package org.nuiton.jaxx.runtime.swing.session;

/*
 * #%L
 * JAXX :: Runtime Swing Session
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.JTabbedPane;

/**
 * State for the JTabbedPane
 *
 * @author poussin
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.16
 */
public class JTabbedPaneState implements State {

    protected int selectedIndex = -1;

    protected int tabCount;

    public JTabbedPaneState() {
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public int getTabCount() {
        return tabCount;
    }

    public void setTabCount(int tabCount) {
        this.tabCount = tabCount;
    }


    protected JTabbedPane checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JTabbedPane)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JTabbedPane) o;
    }

    @Override
    public State getState(Object o) {
        JTabbedPaneState result = new JTabbedPaneState();

        JTabbedPane p = checkComponent(o);
        result.setSelectedIndex(p.getSelectedIndex());
        result.setTabCount(p.getTabCount());

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (state == null) {
            return;
        }
        if (state instanceof JTabbedPaneState) {
            JTabbedPane p = checkComponent(o);
            JTabbedPaneState tps = (JTabbedPaneState) state;
            if (tps.getSelectedIndex() != -1
                    && p.getTabCount() == tps.getTabCount()) {
                p.setSelectedIndex(tps.getSelectedIndex());
            }
        } else {
            throw new IllegalArgumentException("invalid state");
        }
    }
}
