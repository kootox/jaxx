package org.nuiton.jaxx.runtime.swing.session;

/*
 * #%L
 * JAXX :: Runtime Swing Session
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * State for JTable.
 * TODO add support for column order change
 *
 * @author poussin
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.16
 */
public class JTableState implements State {

    private static final Log log = LogFactory.getLog(JTableState.class);

    protected int[] columnWidths = new int[0];

    protected Map<Integer, String> sortKeys;

    public JTableState() {
    }

    public JTableState(int[] columnWidths, Map<Integer, String> sortKeys) {
        this.columnWidths = columnWidths;
        this.sortKeys = sortKeys;
    }

    public int[] getColumnWidths() {
        return columnWidths;
    }

    public void setColumnWidths(int[] columnWidths) {
        this.columnWidths = columnWidths;
    }

    public Map<Integer, String> getSortKeys() {
        return sortKeys;
    }

    public void setSortKeys(Map<Integer, String> sortKeys) {
        this.sortKeys = sortKeys;
    }

    protected JTable checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JTable)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JTable) o;
    }

    @Override
    public State getState(Object o) {
        JTable table = checkComponent(o);

        JTableState result = new JTableState();

        int[] columnWidths = new int[table.getColumnCount()];
        boolean resizableColumnExists = false;

        for (int i = 0; i < columnWidths.length; i++) {
            TableColumn tc = table.getColumnModel().getColumn(i);
            columnWidths[i] = (tc.getResizable()) ? tc.getWidth() : -1;
            if (tc.getResizable()) {
                resizableColumnExists = true;
            }
        }

        if (resizableColumnExists) {
            result.setColumnWidths(columnWidths);
        }

        if (table.getRowSorter() != null) {
            List<? extends RowSorter.SortKey> sortKeys = table.getRowSorter().getSortKeys();
            Map<Integer, String> sortKeysMap = null;
            if (sortKeys != null) {
                sortKeysMap = new HashMap<>();
                for (RowSorter.SortKey sortKey : sortKeys) {
                    sortKeysMap.put(sortKey.getColumn(), String.valueOf(sortKey.getSortOrder()));
                }
            }
            result.setSortKeys(sortKeysMap);
        }

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof JTableState)) {
            throw new IllegalArgumentException("invalid state");
        }

        JTable table = checkComponent(o);
        JTableState jTableState = (JTableState) state;

        int[] columnWidths = jTableState.getColumnWidths();
        if (columnWidths != null
                && table.getColumnCount() == columnWidths.length) {
            for (int i = 0; i < columnWidths.length; i++) {
                if (columnWidths[i] != -1) {
                    TableColumn tc = table.getColumnModel().getColumn(i);
                    if (tc.getResizable()) {
                        tc.setPreferredWidth(columnWidths[i]);
                    }
                }
            }
        }
        Map<Integer, String> sortKeysMap = jTableState.getSortKeys();
        if (sortKeysMap != null) {
            List<RowSorter.SortKey> sortKeys = new ArrayList<>();
            for (Integer index : sortKeysMap.keySet()) {
                SortOrder sortOrder = SortOrder.valueOf(sortKeysMap.get(index));
                RowSorter.SortKey sortKey = new RowSorter.SortKey(index, sortOrder);
                sortKeys.add(sortKey);
            }
            table.getRowSorter().setSortKeys(sortKeys);
        }
    }
}
