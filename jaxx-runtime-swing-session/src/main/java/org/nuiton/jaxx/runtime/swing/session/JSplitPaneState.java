package org.nuiton.jaxx.runtime.swing.session;

/*
 * #%L
 * JAXX :: Runtime Swing Session
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.JSplitPane;

/**
 * State for JSplit
 * FIXME add listener for divider move action
 *
 * @author poussin
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.16
 */
public class JSplitPaneState implements State {

    protected int dividerLocation = -1;

    protected int orientation = JSplitPane.HORIZONTAL_SPLIT;

    public JSplitPaneState() {
    }

    public int getDividerLocation() {
        return dividerLocation;
    }

    public void setDividerLocation(int dividerLocation) {
        this.dividerLocation = dividerLocation;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    protected JSplitPane checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JSplitPane)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JSplitPane) o;
    }

    @Override
    public State getState(Object o) {
        JSplitPane p = checkComponent(o);

        JSplitPaneState result = new JSplitPaneState();
        result.setDividerLocation(p.getUI().getDividerLocation(p));
        result.setOrientation(p.getOrientation());

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (state == null) {
            return;
        }
        JSplitPane p = checkComponent(o);
        if (state instanceof JSplitPaneState) {
            JSplitPaneState sps = (JSplitPaneState) state;
            if (sps.getDividerLocation() != -1
                    && p.getOrientation() == sps.getOrientation()) {
                p.setDividerLocation(sps.getDividerLocation());
            }
        } else {
            throw new IllegalArgumentException("invalid state");
        }
    }
}
