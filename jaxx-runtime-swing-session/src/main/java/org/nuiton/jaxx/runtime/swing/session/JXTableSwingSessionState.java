package org.nuiton.jaxx.runtime.swing.session;

/*
 * #%L
 * JAXX :: Runtime Swing Session
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.table.TableColumn;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.5.16
 */
public class JXTableSwingSessionState extends JTableState {

    protected boolean[] hiddenColumns;

    public JXTableSwingSessionState() {
        super();
    }

    public JXTableSwingSessionState(int[] columnWidths, Map<Integer, String> sortKeys, boolean[] hiddenColumns) {
        super(columnWidths, sortKeys);
        this.hiddenColumns = hiddenColumns;
    }

    public boolean[] getHiddenColumns() {
        return hiddenColumns;
    }

    public void setHiddenColumns(boolean[] hiddenColumns) {
        this.hiddenColumns = hiddenColumns;
    }

    protected JXTable checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof JXTable)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (JXTable) o;
    }

    @Override
    public State getState(Object o) {
        JXTable table = checkComponent(o);
        JXTableSwingSessionState result = new JXTableSwingSessionState();
        JTableState state = (JTableState) super.getState(o);
        if (state != null) {
            result.setColumnWidths(state.getColumnWidths());
            result.setSortKeys(state.getSortKeys());
        }
        DefaultTableColumnModelExt columnModel =
                (DefaultTableColumnModelExt) table.getColumnModel();
        List<TableColumn> columns = columnModel.getColumns(true);
        boolean[] hiddenColumns = new boolean[columns.size()];
        for (int i = 0; i < hiddenColumns.length; i++) {
            TableColumnExt tc = (TableColumnExt) columns.get(i);
            hiddenColumns[i] = !tc.isVisible();
        }
        result.setHiddenColumns(hiddenColumns);

        return result;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof JXTableSwingSessionState)) {
            throw new IllegalArgumentException("invalid state");
        }
        super.setState(o, state);
        JXTable table = checkComponent(o);
        boolean[] hiddenColumns = ((JXTableSwingSessionState) state).getHiddenColumns();
        DefaultTableColumnModelExt columnModel =
                (DefaultTableColumnModelExt) table.getColumnModel();
        List<TableColumn> columns = columnModel.getColumns(true);
        int columnNb = columns.size();
        if (hiddenColumns != null && hiddenColumns.length == columnNb) {
            for (int i = 0; i < columnNb; i++) {
                TableColumnExt tc = (TableColumnExt) columns.get(i);
                tc.setVisible(!hiddenColumns[i]);
            }
        }
    }
}
