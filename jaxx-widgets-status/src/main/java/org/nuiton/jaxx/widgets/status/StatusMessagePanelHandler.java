/*
 * #%L
 * JAXX :: Widgets Status
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.status;

import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.Timer;
import java.awt.Color;
import java.awt.Dimension;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6.0
 */
public class StatusMessagePanelHandler implements UIHandler<StatusMessagePanel> {

    protected StatusMessagePanel ui;

    protected Color statusForeground;

    protected String statusReferenceContent;

    protected Timer timer;

    public void init() {
        if (ui.isShowBusy()) {
            Dimension dim = new Dimension(30, 15);
            //Dimension dim = new Dimension(30, (int) statusLabel.getPreferredSize().getHeight());
            //log.info("dimension of busy = "+ dim);
            //busyWidget.setPreferredSize(dim);
            ui.busyWidget.setMaximumSize(dim);
            ui.busyWidget.setMinimumSize(dim);
        }
    }

    public void clearStatus() {
        stopStatusFader(ui);
        ui.getStatusLabel().setText(StatusMessagePanel.EMPTY_STATUS);
        //getStatusLabel().setString(EMPTY_STATUS);
    }

    public void setStatus(String status) {
        if (status != null) {
            stopStatusFader(ui);
            ui.getStatusLabel().setText(status);
            //getStatusLabel().setString(status);
        }

        if (!ui.isBusy()) {
            startStatusFader(ui);
        }
    }

    protected void fadeStatus(StatusMessagePanel ui) {
        for (int i = 0; i < 8; i++) {
            // synchronized (this) {
            if (!statusReferenceContent.equals(ui.getStatusLabel().getText())) {
//            if (!statusReferenceContent.equals(ui.getStatusLabel().getString())) {
                return;
            }
            Color currentForeground = ui.getStatusLabel().getForeground();
            Color newColor = new Color(currentForeground.getRed(),
                                       currentForeground.getGreen(),
                                       currentForeground.getBlue(),
                                       currentForeground.getAlpha() - 25);
            ui.getStatusLabel().setForeground(newColor);
            ui.getStatusLabel().repaint();
            // }
            // TC-2000311 je comprends pas a quoi ca sert, a part frizzer les ui ?
            // si on utilise un Timer, pourquoi utiliser ça ?
            /*try {
            Thread.sleep(200);
            } catch (InterruptedException eee) {
            eee.printStackTrace();
            }*/
        }
    }

    protected void startStatusFader(StatusMessagePanel ui) {
        statusReferenceContent = ui.getStatusLabel().getText();
//        statusReferenceContent = ui.getStatusLabel().getString();

        int millisecondsPerMinute = 5000;
        timer = new Timer(millisecondsPerMinute, ui);
        timer.setRepeats(false);
        timer.setInitialDelay((int) ((long) millisecondsPerMinute -
                System.currentTimeMillis() %
                        (long) millisecondsPerMinute) + 500);
        timer.start();
    }

    protected void stopStatusFader(StatusMessagePanel ui) {
        if (timer != null) {
            timer.stop();
            ui.getStatusLabel().setForeground(statusForeground);
        }
    }

    @Override
    public void beforeInit(StatusMessagePanel ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(StatusMessagePanel ui) {
        init();
    }
}
