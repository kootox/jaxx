/*
 * #%L
 * JAXX :: Widgets Status
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.status;

import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of ui {@link MemoryStatusWidget}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class MemoryStatusWidgetHandler implements UIHandler<MemoryStatusWidget>{

    private final static String memoryTestStr = "99999/99999Mb";

    private final FontRenderContext frc = new FontRenderContext(null, false, false);

    private final LineMetrics lm = new JLabel().getFont().getLineMetrics(memoryTestStr, frc);

    protected MemoryStatusWidget ui;

    public void paintComponent(Graphics g) {
        Insets insets = new Insets(0, 0, 0, 0);
        Runtime runtime = Runtime.getRuntime();
        int freeMemory = (int) (runtime.freeMemory() / 1024L);
        int totalMemory = (int) (runtime.totalMemory() / 1024L);
        int usedMemory = totalMemory - freeMemory;
        int width = ui.getWidth() - insets.left - insets.right;
        int height = ui.getHeight() - insets.top - insets.bottom - 1;
        float fraction = (float) usedMemory / (float) totalMemory;
        g.setColor(ui.progressBackground);
        g.fillRect(insets.left, insets.top, (int) ((float) width * fraction), height);
        // No i18n string was :
        // String str = usedMemory / 1024 + "/" + totalMemory / 1024 + "Mb";
        String str = t("memorywidget.memory", usedMemory / 1024, totalMemory / 1024);
        //FontRenderContext frc = new FontRenderContext(null, false, false);
        Rectangle2D bounds = g.getFont().getStringBounds(str, frc);
        Graphics g2 = g.create();
        g2.setClip(insets.left, insets.top,
                   (int) ((float) width * fraction), height);
        g2.setColor(ui.progressForeground);
        g2.drawString(str, insets.left
                              + (int) ((double) width - bounds.getWidth()) / 2,
                      (int) ((float) insets.top + lm.getAscent()));
        g2.dispose();
        g2 = g.create();
        g2.setClip(insets.left + (int) ((float) width * fraction),
                   insets.top, ui.getWidth() - insets.left
                           - (int) ((float) width * fraction), height);
        g2.setColor(ui.getForeground());
        g2.drawString(str, insets.left
                              + (int) ((double) width - bounds.getWidth()) / 2,
                      (int) ((float) insets.top + lm.getAscent()));
        g2.dispose();
    }

    @Override
    public void beforeInit(MemoryStatusWidget ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(MemoryStatusWidget ui) {
        ui.setFont(new JLabel().getFont());
        Rectangle2D bounds = ui.getFont().getStringBounds(memoryTestStr, frc);
        Dimension dim = new Dimension((int) bounds.getWidth(), (int) bounds.getHeight());
        ui.setPreferredSize(dim);
        ui.setMaximumSize(dim);
    }
}
