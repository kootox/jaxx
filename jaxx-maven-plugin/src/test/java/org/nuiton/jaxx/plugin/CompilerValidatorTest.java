/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@Ignore
public class CompilerValidatorTest extends JaxxBaseTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CompilerValidatorTest.class);


    @Test
    public void ValidatorOk() throws Exception {
        getMojo().execute();
        assertNumberJaxxFiles(3);

    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void ValidatorErrors() throws Exception {
        // init mojo to get alls files to treate
        GenerateMojo mojo = getMojo();
        mojo.init();
        String[] files = mojo.files;
        assertNumberJaxxFiles(18);
        mojo.setLog(new SystemStreamLog() {

            @Override
            public boolean isErrorEnabled() {
                return false;
            }

            @Override
            public void error(Throwable error) {
                //do nothing
            }

            @Override
            public void error(CharSequence content) {
                //do nothing
            }

            @Override
            public void error(CharSequence content, Throwable error) {
                //do nothing
            }
        });

        // execute mjo on each jaxx file to produce the error
        for (String file : files) {
            log.info("test bad file " + file);
            mojo.files = new String[]{file};
            try {
                mojo.doAction();
                // should never pass
                fail("for file " + file);
            } catch (MojoExecutionException e) {
                // ok jaxx compiler failed
                assertTrue(true);
                assertError(mojo.getEngine(), file, 1);
            }
        }
    }
}
