/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.plugin.compilerValidatorTest.validator.ok;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Identity {

    protected String firstName = "";

    protected String lastName = "";

    protected String email = "dummy@codelutin.com";

    protected int age = 51;


    final PropertyChangeSupport p;

    public Identity() {
        p = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        p.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        p.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.removePropertyChangeListener(propertyName, listener);
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        String oldFirstName = this.firstName;
        this.firstName = firstName;
        p.firePropertyChange("firstName", oldFirstName, firstName);
    }

    public void setLastName(String lastName) {
        String oldLastName = this.lastName;
        this.lastName = lastName;
        p.firePropertyChange("lastName", oldLastName, lastName);
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        p.firePropertyChange("email", oldEmail, email);
    }

    public void setAge(int age) {
        int oldAge = this.age;
        this.age = age;
        p.firePropertyChange("age", oldAge, age);
    }
}
