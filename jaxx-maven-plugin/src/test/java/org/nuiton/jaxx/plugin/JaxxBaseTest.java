/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Rule;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;
import org.nuiton.jaxx.compiler.decorators.DefaultCompiledObjectDecorator;
import org.nuiton.jaxx.runtime.context.DefaultJAXXContext;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.plugin.MojoTestRule;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base test case for a jaxx:generate goal.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see GenerateMojo
 */
public abstract class JaxxBaseTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(JaxxBaseTest.class);

    @Rule
    public final MojoTestRule<GenerateMojo> rule = new MojoTestRule<GenerateMojo>(getClass(), "generate") {

        @Override
        protected void setUpMojo(GenerateMojo mojo, File pomFile) throws Exception {

            try {
                super.setUpMojo(mojo, pomFile);
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error(e);
                }
            }
            mojo.setEncoding("UTF-8");
            mojo.validatorFactoryFQN = SwingValidator.class.getName();
            mojo.jaxxContextFQN = DefaultJAXXContext.class.getName();
            mojo.compilerFQN = JAXXCompiler.class.getName();
            mojo.defaultDecoratorFQN = DefaultCompiledObjectDecorator.class.getName();

        }

    };

//    @Override
//    protected String getGoalName(String methodName) {
//        return "generate";
//    }
//
//    @Override
//    protected void setUpMojo(GenerateMojo mojo, File pomFile) throws Exception {
//        try {
//            super.setUpMojo(mojo, pomFile);
//        } catch (Exception e) {
//            if (log.isErrorEnabled()) {
//                log.error(e);
//            }
//        }
//        mojo.setEncoding("UTF-8");
//        mojo.validatorFactoryFQN= SwingValidator.class.getName();
//        mojo.jaxxContextFQN = DefaultJAXXContext.class.getName();
//        mojo.compilerFQN = JAXXCompiler.class.getName();
//        mojo.defaultDecoratorFQN = DefaultCompiledObjectDecorator.class.getName();
//    }

    protected void checkPattern(GenerateMojo mojo,
                                String pattern,
                                boolean required,
                                String... files) throws IOException {
        if (files.length == 0) {
            files = mojo.files;
        }
        for (String file : files) {
            // check we have a the required/forbidden pattern
            File f = new File(mojo.outJava, file.substring(0, file.length() - 4) + "java");
            if (mojo.isVerbose()) {
                log.info("check generated file " + f);
            }

            assertTrue("generated file " + f + " was not found...", f.exists());
            String content = new String(Files.readAllBytes(f.toPath()), StandardCharsets.UTF_8);

            String errorMessage = required ? "could not find the pattern : " : "should not have found pattern :";
            assertEquals(errorMessage + pattern + " in file " + f, required, content.contains(pattern));
        }
    }

    public GenerateMojo getMojo() {
        return rule.getMojo();
    }

    protected void assertNumberJaxxFiles(int expectedNbFiles) {
        if (expectedNbFiles == 0) {
            assertTrue(getMojo().files == null || getMojo().files.length == 0);
        } else {
            assertEquals(expectedNbFiles, getMojo().files.length);
        }
    }

    @SuppressWarnings("unchecked")
    protected void assertError(JAXXEngine engine, String file, int nbCompiler) {

        JAXXCompilerFile[] compilers = engine.getCompiledFiles();
        assertEquals(nbCompiler, compilers.length);
        List<String> errors = engine.getErrors();
        assertTrue("should have found at least one error for " + file, errors != null && !errors.isEmpty());
    }
}
