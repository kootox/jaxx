/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.junit.Test;

import java.io.IOException;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class DataBindingTest extends JaxxBaseTest {

    @Test
    public void simpleBinding() throws Exception {

        GenerateMojo mojo = getMojo();
        mojo.execute();
        assertNumberJaxxFiles(1);

        // no bindings
        checkPattern(mojo, "BINDING_NOBINDING1_TEXT", false);
        checkPattern(mojo, "BINDING_NOBINDING2_TEXT", false);

        // with bindings
        checkBinding(mojo, "BINDING_BINDING8_TEXT");
    }

    protected void checkBinding(GenerateMojo mojo, String... bindings) throws IOException {
        for (String b : bindings) {
            checkPattern(mojo, "public static final String " + b + " = ", true);
//            checkPattern(mojo, "new DataBindingListener(this, " + b + ");", true);
        }
    }
}
