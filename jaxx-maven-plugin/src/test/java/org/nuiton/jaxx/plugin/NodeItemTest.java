/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class NodeItemTest {

    @Test
    public void testFindChild() {
        System.out.println("findChild");
        String path = "a";
        NodeItem instance = new NodeItem("a", "rootItem text");
        NodeItem expResult = new NodeItem("a", "rootItem text");
        NodeItem result = instance.findChild(path);
        assertTocItemEquals(expResult, result);

        path = "a.b";
        expResult = new NodeItem("b", null);
        result = instance.findChild(path);
        assertTocItemEquals(expResult, result);
        assertTrue(instance.getChild("b") == result);
        assertTrue(instance.findChild("b") == result);

        NodeItem b = result;

        path = "ab";
        expResult = new NodeItem("ab", null);
        result = instance.findChild(path);
        assertTocItemEquals(expResult, result);
        assertTrue(instance.getChild("ab") == result);
        assertTrue(instance.findChild("ab") == result);

        path = "a.b.cd";
        expResult = new NodeItem("cd", null);
        result = instance.findChild(path);
        assertTocItemEquals(expResult, result);
        assertTrue(b.getChild("cd") == result);
        assertTrue(b.findChild("b.cd") == result);
        assertTrue(b.findChild("cd") == result);
        assertTrue(instance.findChild("b.cd") == result);
        assertTrue(instance.findChild("a.b.cd") == result);
    }

    protected void assertTocItemEquals(NodeItem t, NodeItem t2) {
        Assert.assertEquals(t.getTarget(), t2.getTarget());
        Assert.assertEquals(t.getText(), t2.getText());

    }
}
