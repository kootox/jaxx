/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.Test;
import org.nuiton.jaxx.runtime.context.DefaultJAXXContext;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class CompilerTest extends JaxxBaseTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CompilerTest.class);

    @Test
    public void InnerClasses() throws Exception {
        getMojo().execute();
        assertNumberJaxxFiles(1);
    }

    @Test
    public void Icon() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.execute();
        checkPattern(mojo, ".createImageIcon(\"myIcon.png\")", true);
        checkPattern(mojo, ".createActionIcon(\"myActionIcon.png\")", true);
        checkPattern(mojo, ".getUIManagerIcon(\"myIcon.png\")", false);
        checkPattern(mojo, ".getUIManagerActionIcon(\"myActionIcon.png\")", false);
        assertNumberJaxxFiles(1);
        mojo.useUIManagerForIcon = true;
        mojo.execute();
        checkPattern(mojo, ".createImageIcon(\"myIcon.png\")", false);
        checkPattern(mojo, ".createActionIcon(\"myActionIcon.png\")", false);
        checkPattern(mojo, ".getUIManagerIcon(\"myIcon.png\")", true);
        checkPattern(mojo, ".getUIManagerActionIcon(\"myActionIcon.png\")", true);
    }

    @Test
    public void ClientProperty() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.execute();
        checkPattern(mojo, ".putClientProperty(\"testOne\", \"oneTest\")", true);
        checkPattern(mojo, ".putClientProperty(\"testTwo\", \"anotherTest\")", true);
    }

    @Test
    public void SpecialSubclassing() throws Exception {
        getMojo().execute();
        assertNumberJaxxFiles(7);
    }

    @Test
    public void CSSTests() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(7);
    }

    @Test
    public void WithLog() throws Exception {
        GenerateMojo mojo = getMojo();
        executeMojo();
        String[] files = mojo.files;
        assertNumberJaxxFiles(2);

        String withLogFile = files[0];
        checkPattern(mojo, "Log log = LogFactory.getLog(", true, withLogFile);
        checkPattern(mojo, "import org.apache.commons.logging.Log;", true, withLogFile);
        checkPattern(mojo, "import org.apache.commons.logging.LogFactory;", true, withLogFile);

        withLogFile = files[1];
        checkPattern(mojo, "Log log = LogFactory.getLog(", true, withLogFile);
        checkPattern(mojo, "import org.apache.commons.logging.Log;", true, withLogFile);
        checkPattern(mojo, "import org.apache.commons.logging.LogFactory;", true, withLogFile);
    }

    @Test
    public void NoLog() throws Exception {
        GenerateMojo mojo = getMojo();
        executeMojo();
        assertNumberJaxxFiles(2);
        checkPattern(mojo, "Log log = LogFactory.getLog(", false);
        checkPattern(mojo, "import org.apache.commons.logging.Log;", false);
        checkPattern(mojo, "import org.apache.commons.logging.LogFactory;", false);
    }

    @SuppressWarnings({"unchecked"})
    @Test
    public void Errors() throws Exception {
        GenerateMojo mojo = getMojo();
        // init mojo to get alls files to treate
        mojo.init();

        assertNumberJaxxFiles(33);
        mojo.setLog(new SystemStreamLog() {

            @Override
            public boolean isErrorEnabled() {
                return false;
            }

            @Override
            public void error(Throwable error) {
                //do nothing
            }

            @Override
            public void error(CharSequence content) {
                //do nothing
            }

            @Override
            public void error(CharSequence content, Throwable error) {
                //do nothing
            }
        });

        // execute mjo on each jaxx file to produce the error
        for (String file : mojo.files) {
            log.info("test bad file " + file);
            mojo.files = new String[]{file};
            try {
                mojo.doAction();
                // should never pass
                fail();
            } catch (MojoExecutionException e) {
                // ok jaxx compiler failed
                assertTrue(true);
                assertError(mojo.getEngine(), file, 1);
            }
        }
    }


    @SuppressWarnings({"unchecked"})
    @Test
    public void ErrorsCss() throws Exception {
        GenerateMojo mojo = getMojo();
        // init mojo to get alls files to treate
        mojo.init();

        assertNumberJaxxFiles(1);
        mojo.setLog(new SystemStreamLog() {

            @Override
            public boolean isErrorEnabled() {
                return false;
            }

            @Override
            public void error(Throwable error) {
                //do nothing
            }

            @Override
            public void error(CharSequence content) {
                //do nothing
            }

            @Override
            public void error(CharSequence content, Throwable error) {
                //do nothing
            }
        });

        // execute mjo on each jaxx file to produce the error
        for (String file : mojo.files) {
            log.info("test bad file " + file);
            mojo.files = new String[]{file};
            try {
                mojo.doAction();
                // should never pass
                fail();
            } catch (MojoExecutionException e) {
                // ok jaxx compiler failed
                assertTrue(true);
                assertError(mojo.getEngine(), file, 1);
            }
        }
    }

    @Test
    public void Initializers() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(1);
    }


    @Test
    public void ImportTag() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(1);
    }

    @Test
    public void ErrorJaxxContextImplementorClass() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.jaxxContextFQN = null;
        try {
            mojo.init();
            fail();
        } catch (NullPointerException e) {
            assertTrue(true);
        }

        mojo.jaxxContextFQN = String.class.getName();
        try {
            mojo.init();
            fail();
        } catch (MojoExecutionException e) {
            assertTrue(true);
        }
//
//        mojo.jaxxContextFQN = JAXXContext.class.getName();
//        try {
//            mojo.init();
//            fail();
//        } catch (IllegalArgumentException e) {
//            assertTrue(true);
//        }

        mojo.jaxxContextFQN = DefaultJAXXContext.class.getName();
        mojo.init();
        assertTrue(true);

    }

    @Test
    public void Script() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(1);
    }

    @Test
    public void OverridingDataBindings() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(3);
    }

    @Test
    public void ClassReferences() throws Exception {
        executeMojo();
        assertNumberJaxxFiles(6);
    }

    @Test
    public void Force() throws Exception {

        // first round, with force option so will generate theonly JButton.jaxx file
        GenerateMojo mojo = getMojo();
        executeMojo();
        String[] files = mojo.files;
        assertNumberJaxxFiles(1);

        File srcFile = new File(mojo.src, files[0]);

        File dstFile = mojo.updater.getMirrorFile(srcFile);

        long oldTime = dstFile.lastModified();
        // second round, no force so will not the file
        mojo.setForce(false);
        executeMojo();
        assertNumberJaxxFiles(0);

        Thread.sleep(1000);

        assertEquals(oldTime, mojo.updater.getMirrorFile(srcFile).lastModified());

        // three round : modify a source with no force option
        if (!srcFile.setLastModified(System.currentTimeMillis())) {
            log.warn("failed to set LastModified for file " + srcFile);
        }

        executeMojo();
        assertNumberJaxxFiles(1);
        assertTrue(mojo.updater.getMirrorFile(srcFile).lastModified() > oldTime);

        // last round, reforce file generation for an no modify source
        mojo.setForce(true);
        executeMojo();
        assertNumberJaxxFiles(1);
        assertTrue(mojo.updater.getMirrorFile(srcFile).lastModified() > oldTime);
    }

    protected void executeMojo() throws MojoExecutionException, MojoFailureException {

        try {
            getMojo().execute();
        } catch (MojoExecutionException | MojoFailureException e) {
            log.error(e);
            throw e;
        }

    }

}
