/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.junit.Test;

public class I18nTest extends JaxxBaseTest {

    @Test
    public void I18nText() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.i18nable = false;
        mojo.execute();
        checkPattern(mojo, "setText(t(\"test.text\"));", false);

        mojo.i18nable = true;
        mojo.execute();
        checkPattern(mojo, "setText(t(\"test.text\"));", true);
        checkPattern(mojo, "t(\\\"test.text\\\")", false);
    }

    @Test
    public void I18nTitle() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.i18nable = false;
        mojo.execute();
        checkPattern(mojo, "setTitle(t(\"test.title\"));", false);

        mojo.i18nable = true;
        mojo.execute();
        checkPattern(mojo, "setTitle(t(\"test.title\"));", true);
        checkPattern(mojo, "t(\\\"test.title\\\")", false);
    }

    @Test
    public void I18nToolTipText() throws Exception {
        GenerateMojo mojo = getMojo();
        mojo.i18nable = false;
        mojo.execute();
        checkPattern(mojo, "setToolTipText(t(\"test.toolTipText\"));", false);

        mojo.i18nable = true;
        mojo.execute();
        checkPattern(mojo, "setToolTipText(t(\"test.toolTipText\"));", true);
        checkPattern(mojo, "t(\\\"test.toolTipText\\\")", false);
    }
}
