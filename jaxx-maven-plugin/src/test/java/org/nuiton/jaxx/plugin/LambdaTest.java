package org.nuiton.jaxx.plugin;

/*-
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.30
 */
public class LambdaTest extends JaxxBaseTest {

    @Test
    public void Lambda() throws Exception {
        getMojo().execute();
        assertNumberJaxxFiles(1);
        checkPattern(getMojo(), "Raw Body Code Here", true);
        checkPattern(getMojo(), "Raw Body Code Here2", true);
    }

}
