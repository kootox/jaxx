/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.plugin;

import org.junit.Test;

/**
 * Test the bug describe here https://forge.nuiton.org/issues/show/1404
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4.1
 */
public class Bug1404Test extends JaxxBaseTest {

    @Test
    public void Bug_1404() throws Exception {

        GenerateMojo mojo = getMojo();
        mojo.execute();
        assertNumberJaxxFiles(3);

        checkPattern(mojo, "import org.nuiton.jaxx.runtime.swing.SwingUtil;", true);
        checkPattern(mojo, "import org.nuiton.jaxx.runtime.JAXXUtil;", true);
    }
}
