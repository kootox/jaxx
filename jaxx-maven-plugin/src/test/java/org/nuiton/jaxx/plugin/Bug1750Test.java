/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.junit.Test;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.runtime.Base64Coder;
import org.nuiton.jaxx.runtime.JAXXObjectDescriptor;
import org.nuiton.jaxx.runtime.JAXXUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/** @author Tony Chemit - dev@tchemit.fr */
public class Bug1750Test extends JaxxBaseTest {

    @Test
    public void Bug_1750() throws Exception {
        getMojo().execute();
        assertNumberJaxxFiles(1);

        JAXXCompiler compiler = getMojo().getEngine().getJAXXCompiler("org.nuiton.jaxx.plugin.bug1750Test.ComboBox");
        assertNotNull(compiler);

        JAXXObjectDescriptor descriptor = compiler.getJAXXObjectDescriptor();
        assertNotNull(descriptor);

        String data = Base64Coder.serialize(descriptor, false);

        JAXXObjectDescriptor descriptor2 = (JAXXObjectDescriptor) Base64Coder.deserialize(data, false);
        assertNotNull(descriptor2);
        assertEquals(descriptor.toString(), descriptor2.toString());

        descriptor2 = JAXXUtil.decodeJAXXObjectDescriptor(data);
        assertNotNull(descriptor2);
        assertEquals(descriptor.toString(), descriptor2.toString());

        data = Base64Coder.serialize(descriptor, true);

        descriptor2 = (JAXXObjectDescriptor) Base64Coder.deserialize(data, true);
        assertNotNull(descriptor2);
        assertEquals(descriptor.toString(), descriptor2.toString());

        descriptor2 = JAXXUtil.decodeCompressedJAXXObjectDescriptor(data);
        assertNotNull(descriptor2);
        assertEquals(descriptor.toString(), descriptor2.toString());

    }
}
