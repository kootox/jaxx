/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jaxx.runtime.JAXXUtil;

import javax.swing.event.ChangeEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Fix the bug https://forge.nuiton.org/issues/show/1124
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.2.4
 */
public class Bug1124Test {

    protected static final Log log = LogFactory.getLog(Bug1124Test.class);

    int i;

    @Test
    public void testColumnMarginChanged() throws Exception {

        TableColumnModel model = new DefaultTableColumnModel();

        TableColumnModelListener listener;

        listener = JAXXUtil.getEventListener(
                TableColumnModelListener.class,
                "columnAdded",
                this,
                "myColumnAdded"
        );

        Assert.assertNotNull(listener);

        model.addColumnModelListener(listener);

        Assert.assertEquals(0, i);

        model.addColumn(new TableColumn(0));

        Assert.assertEquals(1, i);

        listener = JAXXUtil.getEventListener(
                TableColumnModelListener.class,
                "columnMarginChanged",
                this,
                "myColumnMarginChanged"
        );
        Assert.assertNotNull(listener);

        model.addColumnModelListener(listener);
        model.setColumnMargin(12);
        Assert.assertEquals(2, i);
//        listener.columnMarginChanged(new ChangeEvent(this));
    }

    public void myColumnAdded(TableColumnModelEvent evt) {
        log.info(evt.getSource());
        i++;
    }

    public void myColumnMarginChanged(ChangeEvent evt) {
        log.info(evt.getSource());
        i++;
    }
}
