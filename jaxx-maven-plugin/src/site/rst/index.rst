.. -
.. * #%L
.. * JAXX :: Maven plugin
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=================
jaxx-maven-plugin
=================

.. contents::


Introduction
------------

The plugin permits to generate java files from jaxx files via the **generate** goal.

You can also generate the java help system via the **generate-helpXXX* goals.

for more information, consult the `documentation of each mojo`_.


Default layout
--------------

The plugin defines some directory layout convention, using them is a great
benefit.

  * the jaxx files should be under *src/main/java*
  * the generated java files should be under *target/generated-sources/java*
  * the java help files should be under *src/main/help*
  * the generated help search index should be under *target/generated-sources/help*


.. _documentation of each mojo: ./plugin-info.html
