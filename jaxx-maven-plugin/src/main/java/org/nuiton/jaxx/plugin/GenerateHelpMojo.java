/*
 * #%L
 * JAXX :: Maven plugin
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.plugin;

import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Mojo to generate all the javax help stuff for your project.
 *
 * This mojo will chain all others help mojo required.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
@Mojo(name = "generate-help",
        defaultPhase = LifecyclePhase.PROCESS_SOURCES,
        requiresProject = true,
        requiresDependencyResolution = ResolutionScope.COMPILE)
@Execute(goal = "generate-help-search")
public class GenerateHelpMojo extends AbstractGenerateHelpMojo {

    @Override
    public File getTargetDirectory() {
        return null;
    }

    @Override
    public void setTargetDirectory(File targetDirectory) {
    }

    @Override
    public void doAction() {
    }

    @Override
    protected void preDoAction() {
    }

    @Override
    protected void postDoAction() {
    }

    @Override
    protected void doActionForLocale(Locale locale,
                                     boolean isDefaultLocale,
                                     File localizedTarget,
                                     String localePath) {

    }

}
