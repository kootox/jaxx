<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  JAXX :: Maven plugin
  %%
  Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>io.ultreia.java4all</groupId>
    <artifactId>jaxx</artifactId>
    <version>3.0-alpha-27-SNAPSHOT</version>
  </parent>

  <groupId>io.ultreia.java4all.jaxx</groupId>
  <artifactId>jaxx-maven-plugin</artifactId>
  <packaging>maven-plugin</packaging>

  <name>JAXX :: Maven plugin</name>
  <description>
    Maven 2 plugin to generate java sources from JAXX files.
  </description>

  <properties>
    <javaVersion>1.8</javaVersion>
    <signatureArtifactId>java18</signatureArtifactId>
    <signatureVersion>1.0</signatureVersion>
  </properties>

  <dependencies>

    <!-- sibling dependencies -->
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jaxx-runtime-spi</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jaxx-runtime</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jaxx-validator</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>jaxx-compiler</artifactId>
      <version>${project.version}</version>
    </dependency>

    <!-- maven plugin project dependencies -->

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
      <scope>compile</scope>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-core</artifactId>
    </dependency>

    <!--dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-model</artifactId>
    </dependency-->

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-compat</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugin-tools</groupId>
      <artifactId>maven-plugin-annotations</artifactId>
    </dependency>

    <!-- other dependencies -->

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>helper-maven-plugin-api</artifactId>
    </dependency>

    <dependency>
      <groupId>io.ultreia.java4all.i18n</groupId>
      <artifactId>i18n-api</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <dependency>
      <groupId>javax.help</groupId>
      <artifactId>javahelp</artifactId>
    </dependency>

    <!-- pour acceder aux BeansInfos swing via Introspector -->
    <dependency>
      <groupId>com.sun</groupId>
      <artifactId>dt</artifactId>
      <scope>system</scope>
      <systemPath>/${java.home}/../lib/dt.jar</systemPath>
    </dependency>

    <!-- tests dependencies -->

    <dependency>
      <groupId>org.apache.maven.shared</groupId>
      <artifactId>maven-verifier</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugin-testing</groupId>
      <artifactId>maven-plugin-testing-harness</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>helper-maven-plugin-test-api</artifactId>
    </dependency>

    <dependency>
      <groupId>org.eclipse.sisu</groupId>
      <artifactId>org.eclipse.sisu.plexus</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-utils</artifactId>
    </dependency>

    <!--dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-aether-provider</artifactId>
      <version>3.3.9</version>
    </dependency-->

    <!--dependency>
      <groupId>org.eclipse.aether</groupId>
      <artifactId>aether-api</artifactId>
    </dependency-->

    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>runtime</scope>
    </dependency>

  </dependencies>

  <build>
    <pluginManagement>
      <plugins>

        <plugin>
          <artifactId>maven-plugin-plugin</artifactId>
          <configuration>
            <skipErrorNoDescriptorsFound>true</skipErrorNoDescriptorsFound>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>helpmojo</goal>
                <goal>descriptor</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

      </plugins>
    </pluginManagement>
  </build>

  <profiles>

    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>

          <plugin>
            <artifactId>maven-plugin-plugin</artifactId>
            <version>${plugin.version.plugins}</version>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>${plugin.version.javadoc}</version>
            <configuration>
              <quiet>true</quiet>
              <tagletArtifacts>
                <tagletArtifact>
                  <groupId>org.apache.maven.plugin-tools</groupId>
                  <artifactId>maven-plugin-tools-javadoc</artifactId>
                  <version>${plugin.version.plugins}</version>
                </tagletArtifact>
                <tagletArtifact>
                  <groupId>org.codehaus.plexus</groupId>
                  <artifactId>plexus-component-javadoc</artifactId>
                  <version>1.5.5</version>
                </tagletArtifact>
              </tagletArtifacts>
            </configuration>
            <reportSets>
              <reportSet>
                <reports>
                  <report>javadoc</report>
                  <report>test-javadoc</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>

        </plugins>
      </reporting>

    </profile>

  </profiles>
</project>
