package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;
import org.nuiton.validator.bean.list.BeanListValidatorEvent;
import org.nuiton.validator.bean.list.BeanListValidatorListener;

import javax.swing.JComponent;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The model of the table of errors.
 *
 * The model listens list-validators messages and update his internal model
 * from it.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class SwingListValidatorMessageTableModel extends AbstractTableModel
        implements BeanListValidatorListener {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log =
            LogFactory.getLog(SwingListValidatorMessageTableModel.class);

    public static final String[] columnNames =
            {"validator.scope", "validator.bean",
                    "validator.field", "validator.message"};

    public static final Class<?>[] columnClasses =
            {NuitonValidatorScope.class, Object.class, String.class, String.class};

    /** list of registred validators */
    protected final transient List<BeanListValidator<?>> validators;

    /** list of messages actual displayed */
    protected final List<SwingListValidatorMessage> data;

    public SwingListValidatorMessageTableModel() {
        data = Lists.newArrayList();
        validators = Lists.newArrayList();
    }

    /**
     * Register a validator for this model.
     *
     *
     * Note: a validator can not be register twice in the same model.
     *
     * @param validator the validator to register
     */
    public void registerValidator(BeanListValidator<?> validator) {
        Preconditions.checkState(
                !validators.contains(validator),
                "Validator " + validator + " is already registred in "
                        + this);
        validators.add(validator);
        validator.addBeanListValidatorListener(this);
    }

    public void clear() {
        int i = data.size() + data.size();
        if (i > 0) {
            data.clear();
            fireTableRowsDeleted(0, i - 1);
        }
    }

    public void clearValidators() {
        for (BeanListValidator<?> v : validators) {
            v.removeBeanListValidatorListener(this);
        }
        validators.clear();
    }

    /**
     * Obtain the message for a given row.
     *
     * @param rowIndex the row index
     * @return the message for the given row index
     */
    public SwingListValidatorMessage getRow(int rowIndex) {
        SwingUtil.ensureRowIndex(this, rowIndex);
        return data.get(rowIndex);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        // cells are never editable in this model
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        SwingUtil.ensureColumnIndex(this, columnIndex);
        return columnClasses[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        SwingUtil.ensureColumnIndex(this, column);
        return columnNames[column];
    }

    @Override
    public void onFieldChanged(BeanListValidatorEvent event) {
        String[] toDelete = event.getMessagesToDelete();
        String[] toAdd = event.getMessagesToAdd();
        String field = event.getField();
        Object bean = event.getBean();
        NuitonValidatorScope scope = event.getScope();
        boolean mustAdd = toAdd != null && toAdd.length > 0;
        boolean mustDel = toDelete != null && toDelete.length > 0;

        if (log.isTraceEnabled()) {
            log.trace("----------------------------------------------------------");
            log.trace(field + " - (" + getRowCount() + ") toAdd     " + mustAdd);
            log.trace(field + " - (" + getRowCount() + ") toDelete  " + mustDel);
        }

        BeanListValidator<?> validator = event.getSource();

        if (mustDel) {

            // removes datas and notify if no messages to add
            removeMessages(validator, bean, field, scope, !mustAdd, toDelete);
        }

        if (mustAdd) {

            // add new messages, sort datas and notify
            addMessages(validator, bean, field, scope, true, toAdd);
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SwingUtil.ensureColumnIndex(this, columnIndex);
        SwingUtil.ensureRowIndex(this, rowIndex);

        SwingListValidatorMessage row = data.get(rowIndex);
        if (columnIndex == 0) {
            // the icon
            return row.getScope();
        }
        if (columnIndex == 1) {
            // the bean
            return row.getBean();
        }
        if (columnIndex == 2) {
            // the field
            return row.getField();
        }
        if (columnIndex == 3) {
            // the message
            return row.getMessage();
        }

        // should never come here
        return null;
    }

    protected void addMessages(BeanListValidator<?> validator,
                               Object bean,
                               String fieldName,
                               NuitonValidatorScope scope,
                               boolean sort,
                               String... messages) {

//        JComponent editor = validator == null ?
//                            null :
//                            validator.getFieldRepresentation(fieldName);

        JComponent editor = null;

        // add new errors
        for (String error : messages) {
            SwingListValidatorMessage row =
                    new SwingListValidatorMessage(
                            validator,
                            bean,
                            fieldName,
                            error,
                            scope,
                            editor
                    );
            data.add(row);
            if (!sort) {
                fireTableRowsInserted(data.size() - 1, data.size() - 1);
            }
        }

        if (sort) {

            // resort datas
            Collections.sort(data);

            // notify
            fireTableDataChanged();
        }
    }

    protected void removeMessages(BeanListValidator<?> validator,
                                  Object bean,
                                  String fieldName,
                                  NuitonValidatorScope scope,
                                  boolean notify,
                                  String... messages) {

        List<String> messagesToDel =
                new ArrayList<>(Arrays.asList(messages));

        // do it in reverse mode (only one pass in that way since index
        // will stay coherent while removing them)

        for (int i = getRowCount() - 1; i > -1; i--) {
            SwingListValidatorMessage error = data.get(i);
            if (validator.equals(error.getValidator()) &&
                    error.getScope() == scope &&
                    error.getBean() == bean &&
                    error.getField().equals(fieldName) &&
                    messagesToDel.contains(error.getMessage())) {
                // remove the message
                data.remove(i);
                if (notify) {
                    fireTableRowsDeleted(i, i);
                }
            }
        }
    }

}
