/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * A mouse listener to put on a {@link JList} with a {@link
 * SwingValidatorMessageTableModel} as a model.
 *
 * When a double click occurs, find the selected error in model and then focus
 * to the associated component of error.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SwingValidatorMessageTableMouseListener extends MouseAdapter {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(SwingValidatorMessageTableMouseListener.class);

    public static final String HIGHLIGHT_ERROR_PROPERTY = "highlightError";

    /** delgate property change support */
    protected final PropertyChangeSupport pcs;

    public SwingValidatorMessageTableMouseListener() {
        pcs = new PropertyChangeSupport(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getClickCount() == 2) {

            SwingValidatorMessage entry = getSelectedMessage(e);
            if (entry == null) {
                // no entry found
                return;
            }
            JComponent component = entry.getEditor();
            if (component != null) {
                pcs.firePropertyChange(HIGHLIGHT_ERROR_PROPERTY, null, entry);
                if (component.isVisible()) {
                    component.requestFocus();
                }
            }
        }
    }


    protected SwingValidatorMessage getSelectedMessage(MouseEvent e) {
        JTable table = (JTable) e.getSource();
        if (!(table.getModel() instanceof SwingValidatorMessageTableModel)) {
            if (log.isWarnEnabled()) {
                log.warn("model must be a " +
                                 SwingValidatorMessageTableModel.class +
                                 ", but was " + table.getModel());
            }
            return null;
        }

        SwingValidatorMessageTableModel model =
                (SwingValidatorMessageTableModel) table.getModel();
        int index = table.getSelectionModel().getMinSelectionIndex();
        if (index == -1) {
            // nothing is selected
            return null;
        }
        if (table.getRowSorter() != null) {
            index = table.getRowSorter().convertRowIndexToModel(index);
        }
        SwingValidatorMessage entry = model.getRow(index);
        if (log.isDebugEnabled()) {
            log.debug("selected index: " + index + " : error: " + entry);
        }
        return entry;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

}
