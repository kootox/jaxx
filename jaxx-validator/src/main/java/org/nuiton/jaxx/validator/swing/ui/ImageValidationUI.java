/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing.ui;

import com.google.common.collect.Sets;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.EnumMap;


/**
 * An implementation of {@link AbstractBeanValidatorUI} which paints a icon on
 * top right corner.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ImageValidationUI extends AbstractBeanValidatorUI {

    private static final long serialVersionUID = 1L;

    protected final EnumMap<NuitonValidatorScope, BufferedImage> icons;

    public ImageValidationUI(String fields) {
        this(Sets.newHashSet(fields));
    }

    public ImageValidationUI(Collection<String> fields) {
        super(fields);
        icons = new EnumMap<>(NuitonValidatorScope.class);

        for (NuitonValidatorScope scope : NuitonValidatorScope.values()) {

            BufferedImage image = prepareIcon(SwingValidatorUtil.getIcon(scope));
            icons.put(scope, image);
        }
    }

    protected static BufferedImage prepareIcon(ImageIcon image) {
        BufferedImage icon = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = (Graphics2D) icon.getGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2.drawImage(image.getImage(), 0, 0, null);
        g2.dispose();
        return icon;
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        c.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 3));
    }

    @Override
    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        c.setBorder(null);
    }

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        super.paintLayer(g2, l);
        NuitonValidatorScope scope = getScope();
        if (scope != null) {
            BufferedImage icon = icons.get(scope);
            g2.drawImage(icon, l.getWidth() - icon.getWidth() - 1, 0, null);
        }
    }
}
