/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing.meta;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to put on each field or method linked to a validator.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.3
 */
@Target(value = {ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidatorField {
    /**
     * Obtain the id of the validator used for the field.
     *
     * @return the id of the validator used for the field.
     */
    String validatorId();

    /**
     * Obtain the name of the bean property(ies) to validate.
     *
     * @return the name of the property(ies) to validate
     */
    String[] propertyName();

    /**
     * Obtain the name of the property editor.
     *
     * If empty, then use the {@link #propertyName()}.
     *
     * @return the name of the property editor
     */
    String editorName() default "";
}
