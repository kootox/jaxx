package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Set;

/**
 * Listens a table model which is the editor of a list of bean, add and remove
 * beans to the target validator.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class SwingListValidatorTableEditorModelListener<O> implements TableModelListener {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SwingListValidatorTableEditorModelListener.class);

    private final SwingListValidatorDataLocator<O> dataLocator;

    private final BeanListValidator<O> validator;

    public SwingListValidatorTableEditorModelListener(
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        this.dataLocator = dataLocator;
        this.validator = validator;
    }

    @Override
    public void tableChanged(TableModelEvent e) {

        TableModel model = (TableModel) e.getSource();

        int type = e.getType();

        int firstRow = e.getFirstRow();
        int lastRow = e.getLastRow();

        switch (type) {
            case TableModelEvent.INSERT:

                onRowsInserted(model, firstRow, lastRow);

                break;
            case TableModelEvent.DELETE:

                onRowsDeleted(model);

                break;
            case TableModelEvent.UPDATE:

                if (e.getColumn() == TableModelEvent.ALL_COLUMNS
                        && firstRow == 0
                        && lastRow == Integer.MAX_VALUE) {

                    // fireTableDataChanged

                    onDataChanged(model);

                }
                break;
        }
    }

    protected void onRowsInserted(TableModel model, int firstRow, int lastRow) {

        Set<O> beans = Sets.newHashSet();

        for (int i = firstRow; i <= lastRow; i++) {

            O bean = dataLocator.locateBean(model, i);
            if (log.isDebugEnabled()) {
                log.debug("Add a bean to validator " + bean);
            }
            beans.add(bean);
        }

        validator.addAllBeans(beans);

    }

    protected void onRowsDeleted(TableModel model) {


        // bean are no more existing in editor, must then find out
        // which beans must be deleted from all the beans in the validator

        Set<O> beans = Sets.newHashSet(validator.getBeans());
        for (int i = 0, max = model.getRowCount(); i < max; i++) {
            O bean = dataLocator.locateBean(model, i);
            if (log.isDebugEnabled()) {
                log.debug("Remove a bean from validator " + bean);
            }
            beans.remove(bean);
        }

        validator.removeAllBeans(beans);

    }

    protected void onDataChanged(TableModel model) {


        // remove all beans from validator
        validator.removeAllBeans();

        // add all beans from model

        Set<O> beans = Sets.newHashSet();
        for (int i = 0, max = model.getRowCount(); i < max; i++) {
            O bean = dataLocator.locateBean(model, i);
            if (log.isDebugEnabled()) {
                log.debug("Add a bean to validator " + bean);
            }
            beans.add(bean);
        }
        validator.addAllBeans(beans);

    }
}
