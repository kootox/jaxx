package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * A mouse listener to edit a cell when double clicking on a validation
 * message coming from a {@link SwingValidatorMessageTableModel}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class SwingListValidatorMessageTableMouseListener extends MouseAdapter {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(SwingListValidatorMessageTableMouseListener.class);

    public static final String HIGHLIGHT_ERROR_PROPERTY = "highlightError";

    /**
     * Delegate property change support.
     *
     * @since 2.5.3
     */
    protected final PropertyChangeSupport pcs;

    /**
     * The editor of listened bean.
     *
     * @since 2.5.3
     */
    protected final JTable editor;

    /**
     * The cell data locator.
     *
     * @since 2.5.3
     */
    protected final SwingListValidatorDataLocator dataLocator;

    public SwingListValidatorMessageTableMouseListener(
            JTable editor,
            SwingListValidatorDataLocator dataLocator) {
        this.editor = editor;
        this.dataLocator = dataLocator;
        pcs = new PropertyChangeSupport(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getClickCount() == 2) {

            SwingListValidatorMessage entry = getSelectedMessage(e);
            if (entry == null) {
                // no entry found
                return;
            }

            if (dataLocator.acceptType(entry.getBean().getClass())) {

                Pair<Integer, Integer> cell = dataLocator.locateDataCell(
                        editor.getModel(),
                        entry.getBean(),
                        entry.getField());

                SwingUtil.editCell(editor, cell.getLeft(), cell.getRight());
            }

        }
    }

    protected SwingListValidatorMessage getSelectedMessage(MouseEvent e) {
        JTable table = (JTable) e.getSource();
        if (!(table.getModel() instanceof SwingListValidatorMessageTableModel)) {
            if (log.isWarnEnabled()) {
                log.warn("model must be a " +
                                 SwingValidatorMessageTableModel.class +
                                 ", but was " + table.getModel());
            }
            return null;
        }

        SwingListValidatorMessageTableModel model =
                (SwingListValidatorMessageTableModel) table.getModel();
        int index = table.getSelectionModel().getMinSelectionIndex();
        if (index == -1) {
            // nothing is selected
            return null;
        }
        if (table.getRowSorter() != null) {
            index = table.getRowSorter().convertRowIndexToModel(index);
        }
        SwingListValidatorMessage entry = model.getRow(index);
        if (log.isDebugEnabled()) {
            log.debug("selected index: " + index + " : error: " + entry);
        }
        return entry;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

}
