package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;

import javax.swing.table.TableModel;

/**
 * Object that can locate for a given {@link SwingListValidatorMessage},
 * the cell of this data in a table editor.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public interface SwingListValidatorDataLocator<O> {

    /**
     * Tests if the given type can be managed by this locator.
     *
     * Useful if there is more than one locator used for a same list
     * validation table model.
     *
     * @param beanType the type of bean to test
     * @return {@code true} if this locator can manage this bean type.
     */
    boolean acceptType(Class<?> beanType);

    /**
     * Locate the cell of the given data.
     *
     * @param tableModel the table model where data are edited
     * @param bean       the bean to locate
     * @param fieldName  the field to locate
     * @return the cell where to find data
     */
    Pair<Integer, Integer> locateDataCell(TableModel tableModel,
                                          O bean,
                                          String fieldName);

    /**
     * Locate the index of the row of the given bean.
     *
     * @param tableModel the table model where data are edited
     * @param bean       the bean to find
     * @return the row index of the given bean in the table.
     */
    int locateBeanRowIndex(TableModel tableModel, O bean);

    /**
     * Locate the bean given his row index in the table.
     *
     * @param tableModel the table model where data are edited
     * @param rowIndex   the row index of the bean to find in the editor
     * @return the bean corresponding to the given row index in the editor
     */
    O locateBean(TableModel tableModel, int rowIndex);
}
