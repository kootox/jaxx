package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.JTable;
import java.awt.Component;

/**
 * HighLight predicates for a table editor using {@link BeanListValidator} of
 * his data.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class SwingListValidatorHighlightPredicate<O> implements HighlightPredicate {

    /**
     * Filtering scope.
     *
     * @since 2.5.3
     */
    protected final NuitonValidatorScope scope;

    /**
     * Validator (contains data).
     *
     * @since 2.5.3
     */
    protected final BeanListValidator<O> validator;

    /**
     * Data locator to find back data in editor.
     *
     * @since 2.5.3
     */
    protected final SwingListValidatorDataLocator<O> dataLocator;

    public static <O> SwingListValidatorHighlightPredicate<O> newFatalPredicate(
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        return newPredicate(
                NuitonValidatorScope.FATAL,
                validator,
                dataLocator
        );
    }

    public static <O> SwingListValidatorHighlightPredicate<O> newErrorPredicate(
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        return newPredicate(
                NuitonValidatorScope.ERROR,
                validator,
                dataLocator
        );
    }

    public static <O> SwingListValidatorHighlightPredicate<O> newWarningPredicate(
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        return newPredicate(
                NuitonValidatorScope.WARNING,
                validator,
                dataLocator
        );
    }

    public static <O> SwingListValidatorHighlightPredicate<O> newInfoPredicate(
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        return newPredicate(
                NuitonValidatorScope.INFO,
                validator,
                dataLocator
        );
    }

    public static <O> SwingListValidatorHighlightPredicate<O> newPredicate(
            NuitonValidatorScope scope,
            BeanListValidator<O> validator,
            SwingListValidatorDataLocator<O> dataLocator) {
        return new SwingListValidatorHighlightPredicate<>(
                scope,
                validator,
                dataLocator
        );
    }

    public SwingListValidatorHighlightPredicate(NuitonValidatorScope scope,
                                                BeanListValidator<O> validator,
                                                SwingListValidatorDataLocator<O> dataLocator) {
        this.scope = scope;
        this.validator = validator;
        this.dataLocator = dataLocator;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {

        int columnIndex = adapter.convertColumnIndexToModel(adapter.column);

        String fieldName = adapter.getColumnName(columnIndex);

        int rowIndex = adapter.convertRowIndexToModel(adapter.row);
        JTable component = (JTable) adapter.getComponent();
        O bean = dataLocator.locateBean(component.getModel(), rowIndex);
        boolean result = false;
        if (bean != null && dataLocator.acceptType(bean.getClass())) {
            BeanListValidator.NuitonValidatorContext<O> context = validator.getContext(bean);
            NuitonValidatorScope highestScope = context.getHighestScope(fieldName);
            result = scope == highestScope;
        }
        return result;
    }
}
