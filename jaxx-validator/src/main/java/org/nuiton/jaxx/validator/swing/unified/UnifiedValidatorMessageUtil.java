package org.nuiton.jaxx.validator.swing.unified;

/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.validator.swing.SwingListValidatorDataLocator;
import org.nuiton.jaxx.validator.swing.SwingListValidatorTableEditorModelListener;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableMouseListener;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import java.awt.event.MouseListener;
import java.util.Collections;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 8/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public class UnifiedValidatorMessageUtil {

    /**
     * Prepare the ui where to display the validators messages.
     *
     * @param errorTable the table where to display validators messages
     * @param render     renderer to use
     */
    public static void installUI(JTable errorTable,
                                 UnifiedValidatorMessageTableRenderer render) {

        errorTable.setDefaultRenderer(Object.class, render);
        errorTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));

        SwingUtil.setI18nTableHeaderRenderer(
                errorTable,
                n("validator.scope.header"),
                n("validator.scope.header.tip"),
                n("validator.bean.header"),
                n("validator.bean.header.tip"),
                n("validator.field.header"),
                n("validator.field.header.tip"),
                n("validator.message.header"),
                n("validator.message.header.tip"));
        SwingUtil.fixTableColumnWidth(errorTable, 0, 25);

    }

    /**
     * Prepare the ui where to display the validators messages.
     *
     * @param messageTable the table where to display validators messages
     */
    public static <O> void registerValidator(SwingValidator<O> validator,
                                             JTable messageTable) {

        UnifiedValidatorMessageTableModel messageTableModel = getModel(messageTable);

        // register the validator to the error table model
        messageTableModel.registerValidator(validator);

        // add the mouse listener
        UnifiedValidatorMessageTableMouseListener listener = getUnifiedValidatorMessageTableMouseListener(messageTable);

        if (listener == null) {
            registerErrorTableMouseListener(messageTable, null, null);
        }

    }

    /**
     * Prepare the ui where to display the validators messages.
     *
     * @param validator    validator to register
     * @param messageTable the table where to display validators messages
     * @param dataTable    table with data to validate by the validator
     * @param dataLocator  tool to find data in the data table from the validator messages
     */
    public static <O> void registerValidator(BeanListValidator<O> validator,
                                             JTable messageTable,
                                             JTable dataTable,
                                             SwingListValidatorDataLocator<O> dataLocator) {

        UnifiedValidatorMessageTableModel messageTableModel = getModel(messageTable);

        // register the validator to the error table model
        messageTableModel.registerValidator(validator);

        // add the mouse listener
        registerErrorTableMouseListener(messageTable, dataTable, dataLocator);

        // listen on editor model to add / remove bean into validator
        dataTable.getModel().addTableModelListener(
                new SwingListValidatorTableEditorModelListener<>(validator, dataLocator));
    }

    /**
     * Register for a given validator table ui a validator mouse listener.
     *
     * If a previous such listener was registered, then we will remove it and register a new one.
     *
     * @param messageTable the validator table ui
     * @param dataTable    table with data to validate by the validator
     * @param dataLocator  tool to find data in the data table from the validator messages
     * @see UnifiedValidatorMessageTableMouseListener
     */
    public static <O> void registerErrorTableMouseListener(JTable messageTable,
                                                           JTable dataTable,
                                                           SwingListValidatorDataLocator<O> dataLocator) {
        UnifiedValidatorMessageTableMouseListener listener =
                getUnifiedValidatorMessageTableMouseListener(messageTable);

        if (listener != null) {

            // remove existing listener
            messageTable.removeMouseListener(listener);

        }

        listener = new UnifiedValidatorMessageTableMouseListener(dataTable, dataLocator);
        messageTable.addMouseListener(listener);

    }

    /**
     * @param table the validator table ui
     * @return the validator table mouse listener, or <code>null</code> if not found
     * @see SwingValidatorMessageTableMouseListener
     */
    public static UnifiedValidatorMessageTableMouseListener getUnifiedValidatorMessageTableMouseListener(JTable table) {

        UnifiedValidatorMessageTableMouseListener result = null;

        for (MouseListener listener : table.getMouseListeners()) {

            if (listener instanceof UnifiedValidatorMessageTableMouseListener) {

                result = (UnifiedValidatorMessageTableMouseListener) listener;
                break;

            }
        }

        return result;

    }

    protected static UnifiedValidatorMessageTableModel getModel(JTable messageTable) {

        TableModel model = messageTable.getModel();

        if (!(model instanceof UnifiedValidatorMessageTableModel)) {
            throw new IllegalStateException("messageTable is not using a UnifiedValidatorMessageTableModel model: " + model);
        }

        return (UnifiedValidatorMessageTableModel) model;

    }

}
