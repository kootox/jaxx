package org.nuiton.jaxx.validator.swing.unified;

/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 8/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public class UnifiedValidatorMessageTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected final Map<String, String> fieldNameMap = Maps.newHashMap();

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        JLabel rendererComponent = (JLabel)
                super.getTableCellRendererComponent(
                        table,
                        value,
                        isSelected,
                        hasFocus,
                        row,
                        column
                );

        ImageIcon icon = null;
        String text = null;
        String toolTipText = null;

        column = table.convertColumnIndexToModel(column);
        if (table.getRowSorter() != null) {
            row = table.getRowSorter().convertRowIndexToModel(row);
        }

        if (column == 0) {

            // scope
            NuitonValidatorScope scope = (NuitonValidatorScope) value;
            icon = getIcon(row, scope);
            toolTipText = getIconTip(row, scope);

        } else {

            UnifiedValidatorMessageTableModel tableModel =
                    (UnifiedValidatorMessageTableModel) table.getModel();
            UnifiedValidatorMessage validatorMessage = tableModel.getRow(row);

            switch (column) {

                case 1:

                    // row bean

                    if (validatorMessage.isSimpleValidator()) {

                        text = getSimpleBeanValidatorValue(row, validatorMessage);
                        toolTipText = getSimpleBeanValidatorValueTip(row, text);

                    } else {

                        text = getListBeanValidatorValue(row, validatorMessage);
                        toolTipText = getListBeanValidatorValueTip(row, text);

                    }
                    break;

                case 2:

                    // field name
                    text = getFieldName(row, validatorMessage, (String) value);
                    toolTipText = getFieldNameTip(row, text);
                    break;

                case 3:

                    // message
                    text = getMessage(row, validatorMessage);
                    toolTipText = getMessageTip(row, text);
                    break;

            }

        }

        rendererComponent.setText(text);
        rendererComponent.setToolTipText(toolTipText);
        rendererComponent.setIcon(icon);
        return rendererComponent;
    }

    public ImageIcon getIcon(int row, NuitonValidatorScope scope) {
        return SwingValidatorUtil.getIcon(scope);
    }

    public String getIconTip(int row, NuitonValidatorScope scope) {
        String label = t(scope.getLabel());
        return t("validator.scope.tip", label);
    }

    public String getMessage(int row, UnifiedValidatorMessage model) {
        return SwingValidatorUtil.getMessage(model);
    }

    public String getMessageTip(int row, String message) {
        return t("validator.message.tip", message);
    }

    public String getFieldName(int row, UnifiedValidatorMessage model, String value) {
        return SwingValidatorUtil.getFieldName(model, value, fieldNameMap.get(value));
    }

    public String getFieldNameTip(int row, String fieldName) {
        return t("validator.field.tip", fieldName);
    }

    public String getListBeanValidatorValue(int row, UnifiedValidatorMessage model) {
        return decorateBean(model.getBean());
    }

    public String getListBeanValidatorValueTip(int row, String beanValue) {
        return StringUtils.isEmpty(beanValue) ? null : t("validator.bean.tip", row, beanValue);
    }

    public String getSimpleBeanValidatorValue(int row, UnifiedValidatorMessage model) {
        return decorateBean(((SimpleBeanValidator) model.getValidator()).getBean());
    }

    public String getSimpleBeanValidatorValueTip(int row, String simpleBeanValue) {
        return null;
    }

    protected String decorateBean(Object bean) {
        return bean == null ? "" : bean.toString();
    }

    public void clearFieldNameMap() {
        fieldNameMap.clear();
    }

    public void addFieldName(String field, String name) {
        fieldNameMap.put(field, name);
    }

}
