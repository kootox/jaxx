/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorEvent;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorListener;

import javax.swing.AbstractListModel;
import javax.swing.JComponent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The model of the list of validation's messages
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SwingValidatorMessageListModel
        extends AbstractListModel
        implements SimpleBeanValidatorListener {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(SwingValidatorMessageListModel.class);

    /** list of registred validators */
    protected final transient List<SwingValidator<?>> validators;

    /** list of messages actual displayed */
    protected final List<SwingValidatorMessage> data;

    public SwingValidatorMessageListModel() {
        validators = new ArrayList<>();
        data = new ArrayList<>();
    }

    public boolean isEmpty() {
        return getSize() == 0;
    }

    public void registerValidator(SwingValidator<?> validator) {
        if (validators.contains(validator)) {
            throw new IllegalArgumentException(
                    "the validator " + validator + " is already registred in "
                            + this);
        }
        validators.add(validator);
        validator.addSimpleBeanValidatorListener(this);
    }

    public void clear() {
        int i = data.size();
        if (i > 0) {
            data.clear();
            fireIntervalRemoved(this, 0, i - 1);
        }
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        ensureRowIndex(index);
        return data.get(index);
    }

    @Override
    public void onFieldChanged(SimpleBeanValidatorEvent event) {
        String[] toDelete = event.getMessagesToDelete();
        String[] toAdd = event.getMessagesToAdd();
        String field = event.getField();
        NuitonValidatorScope scope = event.getScope();
        boolean mustAdd = toAdd != null && toAdd.length > 0;
        boolean mustDel = toDelete != null && toDelete.length > 0;

        if (log.isTraceEnabled()) {
            log.trace("----------------------------------------------------------");
            log.trace(field + " - (" + getSize() + ") toAdd     " + mustAdd);
            log.trace(field + " - (" + getSize() + ") toDelete  " + mustDel);
        }

        SwingValidator<?> validator = (SwingValidator<?>) event.getSource();

        if (mustDel) {

            // removes datas and notify if no messages to add
            removeMessages(validator, field, scope, !mustAdd, toDelete);
        }

        if (mustAdd) {

            // add new messages, sort datas and notify
            addMessages(validator, field, scope, true, toAdd);
        }
    }

    protected void ensureRowIndex(int index) throws ArrayIndexOutOfBoundsException {
        if (index < -1 || index >= getSize()) {
            throw new ArrayIndexOutOfBoundsException(
                    "the rowIndex was " + index + ", but should be int [0," +
                            (getSize() - 1) + "]");
        }
    }

    protected void addMessages(SwingValidator<?> validator,
                               String field,
                               NuitonValidatorScope scope,
                               boolean sort,
                               String... messages) {

        JComponent editor = validator.getFieldRepresentation(field);
        // add new errors
        for (String error : messages) {
            SwingValidatorMessage row = new SwingValidatorMessage(
                    validator,
                    field,
                    error,
                    scope,
                    editor
            );
            data.add(row);
            if (!sort) {
                fireIntervalAdded(this, data.size() - 1, data.size() - 1);
            }
        }

        if (sort) {

            // resort datas
            Collections.sort(data);

            // notify
            fireContentsChanged(this, 0, getSize() - 1);
        }
    }

    protected void removeMessages(SwingValidator<?> validator,
                                  String field,
                                  NuitonValidatorScope scope,
                                  boolean notify,
                                  String... messages) {

        List<String> messagesToDel =
                new ArrayList<>(Arrays.asList(messages));

        // do it in reverse mode (only one pass in that way since index
        // will stay coherent while removing them)

        for (int i = getSize() - 1; i > -1; i--) {
            SwingValidatorMessage error = data.get(i);
            if (error.getValidator().equals(validator) &&
                    error.getScope() == scope &&
                    error.getField().equals(field) &&
                    messagesToDel.contains(error.getMessage())) {
                // remove the message
                data.remove(i);
                if (notify) {
                    fireIntervalRemoved(this, i, i);
                }
            }
        }
    }
}
