/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing.ui;

import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorEvent;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorListener;

import javax.swing.JComponent;
import java.util.Collection;
import java.util.Set;

/**
 * Abstract renderer
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class AbstractBeanValidatorUI extends AbstractLayerUI<JComponent> implements SimpleBeanValidatorListener {

    /** Logger */
    private static final Log log = LogFactory.getLog(AbstractBeanValidatorUI.class);

    private static final long serialVersionUID = 1L;

    /**
     * Actual scope to display in the layer.
     *
     * This field will be recomputed each time a new event arrived on this
     * field.
     */
    protected NuitonValidatorScope scope;

    /** Field name in validator. */
    protected final Set<String> fields;

    protected AbstractBeanValidatorUI(String field) {
        this(Sets.newHashSet(field));
    }

    protected AbstractBeanValidatorUI(Collection<String> fields) {
        this.fields = Sets.newHashSet(fields);
        if (log.isDebugEnabled()) {
            log.debug("install " + this + "<fields:" + this.fields + ">");
        }
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    @Override
    public void onFieldChanged(SimpleBeanValidatorEvent event) {
        if (fields.contains(event.getField())) {

            scope = getHighestScope(event);
            if (log.isDebugEnabled()) {
                log.debug("set new scope : " + scope + " to field " + fields);
            }
            // ask to repaint the layer
            setDirty(true);
        }
    }

    protected NuitonValidatorScope getHighestScope(SimpleBeanValidatorEvent event) {
        SimpleBeanValidator<?> source = event.getSource();
        Set<NuitonValidatorScope> scopes = Sets.newHashSet();
        for (String field : fields) {
            NuitonValidatorScope scope = source.getHighestScope(field);
            if (scope != null) {
                scopes.add(scope);
            }
        }
        return scopes.isEmpty() ? null : Ordering.natural().max(scopes);
    }


}
