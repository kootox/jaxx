/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * A simple render of a table of validator's messages, says a table that use a
 * {@link SwingValidatorMessageTableModel} model.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SwingValidatorMessageTableModel
 * @since 1.3
 */
public class SwingValidatorMessageListRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 1L;

    protected String format = "%1$-20s - %2$s";

    protected final String formatTip = "%1$-20s - %2$-20s :  %3$s";

    public SwingValidatorMessageListRenderer() {
    }

    public SwingValidatorMessageListRenderer(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        JLabel rendererComponent = (JLabel)
                super.getListCellRendererComponent(
                        list,
                        value,
                        index,
                        isSelected,
                        cellHasFocus
                );

        SwingValidatorMessage model = (SwingValidatorMessage) value;

        // scope
        ImageIcon icon = SwingValidatorUtil.getIcon(model.getScope());

        // field name
        String fieldName = getFieldName(
                list,
                model.getField(),
                index
        );

        // message
        String message = getMessage(model);

        // text to display
        String text = String.format(format, fieldName, message);

        String label = t(model.getScope().getLabel());
        String tmp = t("validator.scope.tip", label);
        String tmp2 = t("validator.field.tip", fieldName);

        String tooltTipText = String.format(formatTip, tmp, tmp2, message);


        rendererComponent.setText(text);
        rendererComponent.setToolTipText(tooltTipText);
        rendererComponent.setIcon(icon);

        return rendererComponent;
    }

    public ImageIcon getIcon(NuitonValidatorScope scope) {
        return SwingValidatorUtil.getIcon(scope);
    }

    public String getMessage(SwingValidatorMessage model) {
        return SwingValidatorUtil.getMessage(model);
    }

    public String getFieldName(JList list, String value, int row) {
        SwingValidatorMessageListModel tableModel =
                (SwingValidatorMessageListModel) list.getModel();
        SwingValidatorMessage model =
                (SwingValidatorMessage) tableModel.getElementAt(row);
        return SwingValidatorUtil.getFieldName(model, value);
    }
}
