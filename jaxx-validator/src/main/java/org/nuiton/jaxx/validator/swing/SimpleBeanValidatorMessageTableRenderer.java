/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorMessage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * A simple render of a table of validator's messages, says a table that use
 * a {@link SimpleBeanValidatorMessageTableModel} model.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SimpleBeanValidatorMessageTableModel
 * @since 2.6.23
 */
public class SimpleBeanValidatorMessageTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        JLabel rendererComponent = (JLabel)
                super.getTableCellRendererComponent(
                        table,
                        value,
                        isSelected,
                        hasFocus,
                        row,
                        column
                );

        ImageIcon icon = null;
        String text = null;
        String toolTipText = null;

        column = table.convertColumnIndexToModel(column);
        if (table.getRowSorter() != null) {
            row = table.getRowSorter().convertRowIndexToModel(row);
        }

        switch (column) {
            case 0:
                // scope
                NuitonValidatorScope scope = (NuitonValidatorScope) value;
                icon = SwingValidatorUtil.getIcon(scope);
                String label = t(scope.getLabel());
                toolTipText = t("validator.scope.tip", label);
                break;

            case 1:
                // field name
                text = getFieldName(table, (String) value, row);
                toolTipText = t("validator.field.tip", text);
                break;

            case 2:
                // message
                text = getMessage(table, (String) value, row);
                toolTipText = t("validator.message.tip", text);
                break;
        }

        rendererComponent.setText(text);
        rendererComponent.setToolTipText(toolTipText);
        rendererComponent.setIcon(icon);
        return rendererComponent;
    }

    public ImageIcon getIcon(NuitonValidatorScope scope) {
        return SwingValidatorUtil.getIcon(scope);
    }

    public String getMessage(JTable table, String value, int row) {
        SimpleBeanValidatorMessageTableModel tableModel =
                (SimpleBeanValidatorMessageTableModel) table.getModel();
        SimpleBeanValidatorMessage model = tableModel.getRow(row);
        return SwingValidatorUtil.getMessage(model);
    }

    public String getFieldName(JTable table, String value, int row) {
        SimpleBeanValidatorMessageTableModel tableModel =
                (SimpleBeanValidatorMessageTableModel) table.getModel();
        return value;
    }
}
