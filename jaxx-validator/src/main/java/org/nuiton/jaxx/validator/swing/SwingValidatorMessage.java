/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.simple.SimpleBeanValidatorMessage;

import javax.swing.JComponent;

/**
 * The object to box a validation message within an swing ui.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see SimpleBeanValidatorMessage
 * @since 1.3
 */
public class SwingValidatorMessage extends SimpleBeanValidatorMessage<SwingValidatorMessage> {

    private static final long serialVersionUID = 1L;

    /** the optional field's editor */
    protected final JComponent editor;

    public SwingValidatorMessage(SwingValidator<?> validator,
                                 String fieldName,
                                 String message,
                                 NuitonValidatorScope scope,
                                 JComponent editor) {
        super(validator, fieldName, message, scope);
        this.editor = editor;
    }

    public JComponent getEditor() {
        return editor;
    }

    @Override
    public String toString() {
        String s = scope + " - " +
                (field == null ? message : field + " - " + message);
        if (editor != null) {
            s = editor.getName() + " : " + s;
        }
        return s;
    }
}
