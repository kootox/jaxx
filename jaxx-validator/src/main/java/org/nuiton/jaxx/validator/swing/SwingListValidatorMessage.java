package org.nuiton.jaxx.validator.swing;
/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;
import org.nuiton.validator.bean.list.BeanListValidatorMessage;

import javax.swing.JComponent;

/**
 * TODO
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class SwingListValidatorMessage extends BeanListValidatorMessage<SwingListValidatorMessage> {

    private static final long serialVersionUID = 1L;

    /** the optional field's editor */
    protected final JComponent editor;

    public SwingListValidatorMessage(BeanListValidator<?> validator,
                                     Object bean,
                                     String fieldName,
                                     String message,
                                     NuitonValidatorScope scope,
                                     JComponent editor) {
        super(validator, bean, fieldName, message, scope);
        this.editor = editor;
    }

    public JComponent getEditor() {
        return editor;
    }

    @Override
    public String toString() {
        String s = scope + " - " +
                (field == null ? message : field + "[" + getBean() +
                        "] - " + message);
        if (editor != null) {
            s = editor.getName() + " : " + s;
        }
        return s;
    }
}
