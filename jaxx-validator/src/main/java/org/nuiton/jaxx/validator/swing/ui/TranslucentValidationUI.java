/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing.ui;

import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.Collection;

/**
 * An implementation of {@link AbstractBeanValidatorUI} which paints a
 * translucent backgroud color (green for ok, red for error, yellow for
 * warning).
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TranslucentValidationUI extends AbstractBeanValidatorUI {

    private static final long serialVersionUID = 1L;

    public TranslucentValidationUI(String field) {
        super(field);
    }

    public TranslucentValidationUI(Collection<String> fields) {
        super(fields);
    }

    @Override
    protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
        // paints the layer as is
        super.paintLayer(g2, l);

        // to be in sync with the view if the layer has a border
        Insets layerInsets = l.getInsets();
        g2.translate(layerInsets.left, layerInsets.top);

        JComponent view = l.getView();
        // To prevent painting on view's border
        Insets insets = view.getInsets();
        g2.clip(new Rectangle(insets.left, insets.top,
                              view.getWidth() - insets.left - insets.right,
                              view.getHeight() - insets.top - insets.bottom));

        NuitonValidatorScope scope = getScope();

        Color c = scope == null ? Color.WHITE : SwingValidatorUtil.getColor(scope);
        g2.setColor(c);
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .2f));
        g2.fillRect(0, 0, l.getWidth(), l.getHeight());
    }
}
