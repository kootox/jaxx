/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator;


import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JComponent;
import java.util.List;

/**
 * The contract of a validator-able object.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JAXXValidator {

    /**
     * Obtain a validator from his id
     *
     * @param validatorId validator id
     * @return the associated validator, or <code>null</code> if not find
     */
    SwingValidator<?> getValidator(String validatorId);

    /** @return the list of ids of all registred validator */
    List<String> getValidatorIds();

    /**
     * Init the fields representation.
     *
     * This method is generated and should be called each time the context name
     * of a validator has changed, since when using method
     * {@link SwingValidator#setFieldRepresentation(String, JComponent)}
     * is invoked at init, if a field is not watched for the selected context,
     * then after when changing context name, the field will not be notified of any changed...
     *
     * @since 2.2.1
     */
    void registerValidatorFields();

}
