/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.validator.swing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A mouse listener to put on a {@link JList} with a {@link
 * SwingValidatorMessageListModel} as a model.
 *
 * When a double click occurs, find the selected error in model and then focus
 * to the associated component of error.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SwingValidatorMessageListMouseListener extends MouseAdapter {

    /** Logger */
    static private final Log log =
            LogFactory.getLog(SwingValidatorMessageListMouseListener.class);

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getClickCount() == 2) {

            SwingValidatorMessage entry = getSelectedMessage(e);
            if (entry == null) {
                // no entry found
                return;
            }
            JComponent component = entry.getEditor();
            if (component != null) {
                component.requestFocus();
            }
        }
    }

    protected SwingValidatorMessage getSelectedMessage(MouseEvent e) {
        JList list = (JList) e.getSource();
        if (!(list.getModel() instanceof SwingValidatorMessageListModel)) {
            if (log.isWarnEnabled()) {
                log.warn("model must be a " +
                                 SwingValidatorMessageListModel.class + ", but was " +
                                 list.getModel());
            }
            return null;
        }

        SwingValidatorMessageListModel model =
                (SwingValidatorMessageListModel) list.getModel();
        int index = list.getSelectionModel().getMinSelectionIndex();
        if (index == -1) {
            // nothing is selected
            return null;
        }
        SwingValidatorMessage entry =
                (SwingValidatorMessage) model.getElementAt(index);
        if (log.isDebugEnabled()) {
            log.debug("selected index: " + index + " : error: " + entry);
        }
        return entry;
    }

}
