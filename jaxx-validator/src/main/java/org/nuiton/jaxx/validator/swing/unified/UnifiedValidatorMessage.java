package org.nuiton.jaxx.validator.swing.unified;

/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.AbstractValidator;

import javax.swing.JComponent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 8/15/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public class UnifiedValidatorMessage implements Comparable<UnifiedValidatorMessage>, Serializable {

    private static final long serialVersionUID = 1L;

    /** the validator that produce the message */
    protected final AbstractValidator<?> validator;

    /** the bean on which event occurs. */
    protected final Object bean;

    /** the field that produce the message */
    protected final String field;

    /** the label of the message (to be displayed somewhere) */
    protected final String message;

    /** the scope of the message */
    protected final NuitonValidatorScope scope;

    /** the optional field's editor */
    protected final JComponent editor;

    protected final boolean simpleValidator;

    public UnifiedValidatorMessage(AbstractValidator<?> validator,
                                   Object bean,
                                   String field,
                                   String message,
                                   NuitonValidatorScope scope,
                                   JComponent editor) {
        this.field = field;
        this.bean = bean;
        this.validator = validator;
        this.message = message == null ? null : message.trim();
        this.scope = scope;
        this.simpleValidator = validator instanceof SwingValidator<?>;
        this.editor = editor;
    }

    public AbstractValidator<?> getValidator() {
        return validator;
    }

    public String getField() {
        return field;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getMessage() {
        return message;
    }

    public Object getBean() {
        return bean;
    }

    public JComponent getEditor() {
        return editor;
    }

    public boolean isSimpleValidator() {
        return simpleValidator;
    }

    @Override
    public int compareTo(UnifiedValidatorMessage o) {
        // sort on scope
        int result = getScope().compareTo(o.getScope());
        if (result == 0) {

            // sort on bean

            if (simpleValidator) {

                if (o.simpleValidator) {
                    result = 0;
                } else {
                    // this message must go up (simple message)
                    result = -1;
                }
            } else {
                if (o.simpleValidator) {
                    // that message must go up (simple message)
                    result = 1;
                }
            }
            if (result == 0) {

                // sort on field name
                result = field.compareTo(o.field);
                if (result == 0) {
                    // sort on message
                    result = message.compareTo(o.message);
                }
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UnifiedValidatorMessage)) {
            return false;
        }

        UnifiedValidatorMessage that = (UnifiedValidatorMessage) o;

        return field.equals(that.field) &&
                Objects.equals(bean, that.bean) &&
                Objects.equals(message, that.message) &&
                scope == that.scope;
    }

    @Override
    public int hashCode() {
        int result = field.hashCode();
        result = 31 * result + (bean != null ? bean.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        String s = scope + " - " +
                (field == null ? message : field + "[" + getBean() +
                        "] - " + message);
        if (editor != null) {
            s = editor.getName() + " : " + s;
        }
        return s;
    }

    public String getI18nError(String error) {
        String text;
        if (!error.contains("##")) {
            text = t(error);
        } else {
            StringTokenizer stk = new StringTokenizer(error, "##");
            String errorName = stk.nextToken();
            List<String> args = new ArrayList<>();
            while (stk.hasMoreTokens()) {
                args.add(stk.nextToken());
            }
            text = t(errorName, args.toArray());
        }
        return text;
    }
}
