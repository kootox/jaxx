package org.nuiton.jaxx.validator.swing;

/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.ComponentMover;
import org.nuiton.jaxx.runtime.swing.ComponentResizer;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 * @since 2.10
 */
public class SwingListValidatorMessageWidget extends JToggleButton {

    private static final Log log =
            LogFactory.getLog(SwingListValidatorMessageWidget.class);

    private static final long serialVersionUID = 1L;

    public static final String CLOSE_DIALOG_ACTION = "closeDialog";

    protected final SwingListValidatorMessageTableModel messageTableModel = new SwingListValidatorMessageTableModel();

    protected final JDialog popup = new JDialog();

    protected final JTable messageTable = new JTable();

    protected Point popupPosition = null;

    public SwingListValidatorMessageWidget() {
        super(SwingUtil.createActionIcon("alert-none"));
        setToolTipText(t("validator.messageWidget.alert.none"));

        messageTableModel.addTableModelListener(e -> {
            int alerts = messageTableModel.getRowCount();
            String label;
            switch (alerts) {
                case 0:
                    label = n("validator.messageWidget.alert.none");
                    break;
                case 1:
                    label = n("validator.messageWidget.alert.one");
                    break;
                default:
                    label = n("validator.messageWidget.alert.several");
            }

            NuitonValidatorScope maxScope;
            String icon;
            if (alerts == 0) {
                icon = "alert-none";

            } else {
                maxScope = NuitonValidatorScope.INFO;
                for (int i = 0; i < alerts; i++) {
                    NuitonValidatorScope scope = messageTableModel.getRow(i).getScope();
                    int diff = scope.compareTo(maxScope);
                    if (diff < 0) {
                        maxScope = scope;
                    }
                }
                switch (maxScope) {
                    case INFO:
                        icon = "alert-info";
                        break;
                    case WARNING:
                        icon = "alert-warning";
                        break;
                    default:
                        icon = "alert-error";

                }
            }

            setToolTipText(t(label, alerts));
            setIcon(SwingUtil.createActionIcon(icon));
        });

        messageTable.setModel(messageTableModel);
        messageTable.setRowSelectionAllowed(true);
        messageTable.setAutoCreateRowSorter(true);
        messageTable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        messageTable.setCellSelectionEnabled(false);
        messageTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        messageTable.setFillsViewportHeight(true);

        SwingValidatorUtil.installUI(messageTable,
                                     new SwingListValidatorMessageTableRenderer());

        JScrollPane scrollPanel = new JScrollPane(messageTable);
        scrollPanel.setColumnHeaderView(messageTable.getTableHeader());

        JXTitledPanel titledPanel = new JXTitledPanel(t("validator.messageWidget.title"), scrollPanel);
        popup.add(titledPanel);
        popup.setTitle(t("validator.messageWidget.title"));
        popup.setSize(800, 300);
        popup.setAlwaysOnTop(true);
        popup.setUndecorated(true);

        ComponentResizer cr = new ComponentResizer();
        cr.registerComponent(popup);
        ComponentMover cm = new ComponentMover();
        cm.setDragInsets(cr.getDragInsets());
        cm.registerComponent(popup);

        popup.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                setSelected(false);
            }

        });

        popup.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentMoved(ComponentEvent e) {
                Component component = e.getComponent();
                if (component.isShowing()) {
                    popupPosition = component.getLocationOnScreen();
                }
            }

        });

        addActionListener(e -> {
            if (isSelected()) {
                popup.setVisible(true);
            } else {
                popup.dispose();
            }
        });

        addHierarchyBoundsListener(new HierarchyBoundsAdapter() {

            @Override
            public void ancestorMoved(HierarchyEvent e) {
                if (popupPosition == null && isShowing()) {
                    Point point = new Point(getLocationOnScreen());
                    point.translate(-popup.getWidth() + getWidth(), -popup.getHeight());
                    popup.setLocation(point);
                }
            }
        });

        // add a auto-close action
        JRootPane rootPane = popup.getRootPane();

        KeyStroke shortcutClosePopup = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                shortcutClosePopup, CLOSE_DIALOG_ACTION);

        Action closeAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                popup.dispose();
                setSelected(false);
            }
        };

        ImageIcon actionIcon = SwingUtil.createActionIcon("close-dialog");
        closeAction.putValue(Action.SMALL_ICON, actionIcon);
        closeAction.putValue(Action.LARGE_ICON_KEY, actionIcon);
        closeAction.putValue(Action.ACTION_COMMAND_KEY, "close");
        closeAction.putValue(Action.NAME, "close");
        closeAction.putValue(Action.SHORT_DESCRIPTION, t("validator.messageWidget.closeDialog.tip"));

        rootPane.getActionMap().put(CLOSE_DIALOG_ACTION, closeAction);

        JButton closeButton = new JButton(closeAction);
        closeButton.setText(null);
        closeButton.setFocusPainted(false);
        closeButton.setRequestFocusEnabled(false);
        closeButton.setFocusable(false);

        JToolBar jToolBar = new JToolBar();
        jToolBar.setOpaque(false);
        jToolBar.add(closeAction);
        jToolBar.setBorderPainted(false);
        jToolBar.setFloatable(false);
        titledPanel.setRightDecoration(jToolBar);

    }

    public void addTableModelListener(TableModelListener listener) {
        messageTableModel.addTableModelListener(listener);
    }

    public void removeTableModelListener(TableModelListener listener) {
        messageTableModel.removeTableModelListener(listener);
    }

    public void setDefaultRenderer(TableCellRenderer renderer) {
        messageTable.setDefaultRenderer(Object.class, renderer);
    }

    /**
     * Register a list validator.
     *
     * @param validator   validator to register
     * @param dataTable   table with data to validate by the validator
     * @param dataLocator tool to find data in the data table from the validator messages
     */
    public void registerValidator(BeanListValidator validator,
                                  JTable dataTable,
                                  SwingListValidatorDataLocator dataLocator) {

        if (log.isInfoEnabled()) {
            log.info("Register validator: " + validator);
        }
        SwingValidatorUtil.registerListValidator(
                validator,
                messageTableModel,
                dataTable,
                messageTable,
                dataLocator);

        validator.doValidate();
    }

    /** Clear all the validators. */
    public void clearValidators() {
        messageTableModel.clearValidators();
        messageTableModel.clear();
    }

}
