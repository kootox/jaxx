package org.nuiton.jaxx.validator.swing;

/*
 * #%L
 * JAXX :: Validator
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.JComponent;
import java.util.Objects;

/**
 * Created on 9/17/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.13
 */
public class SwingListValidator<B> extends BeanListValidator<B> {

    /** Logger */
    private static final Log log = LogFactory.getLog(SwingListValidator.class);

    /**
     * Obtain a new {@link SwingListValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the default provider of {@link NuitonValidator}
     *
     * @param type    type of bean to validate
     * @param context context of validation
     * @param scopes  authorized scopes (if {@code null}, will use all scopes)
     * @param <O>     type of bean to validate
     * @return the new instanciated {@link BeanListValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getDefaultProviderName()
     */
    public static <O> SwingListValidator<O> newSwingValidator(Class<O> type,
                                                              String context,
                                                              NuitonValidatorScope... scopes) throws NullPointerException {


        // get the provider default name
        String providerName = NuitonValidatorFactory.getDefaultProviderName();

        // get the bean validator with this provider
        return newSwingValidator(providerName,
                                 type,
                                 context,
                                 scopes
        );
    }

    /**
     * Obtain a new {@link SwingListValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the provider of {@link NuitonValidator}
     * defined by the {@code providerName}.
     *
     * @param providerName name of {@link NuitonValidator} to use
     * @param type         type of bean to validate
     * @param context      context of validation
     * @param scopes       authorized scopes (if {@code null}, will use all scopes)
     * @param <O>          type of bean to validate
     * @return the new instanciated {@link BeanListValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getProvider(String)
     */
    public static <O> SwingListValidator<O> newSwingValidator(String providerName,
                                                              Class<O> type,
                                                              String context,
                                                              NuitonValidatorScope... scopes) throws NullPointerException {

        Objects.requireNonNull(type, "type parameter can not be null.");

        // get delegate validator provider
        NuitonValidatorProvider provider =
                NuitonValidatorFactory.getProvider(providerName);

        Preconditions.checkState(
                provider != null,
                "Could not find provider with name " + providerName);

        // create the new instance of bean validator

        return new SwingListValidator<>(
                provider, type, context, scopes
        );
    }

    protected JComponent editor;

    public SwingListValidator(NuitonValidatorProvider validatorProvider, Class<B> beanClass, String context) {
        super(validatorProvider, beanClass, context);
    }

    public SwingListValidator(NuitonValidatorProvider validatorProvider, Class<B> beanClass, String context, NuitonValidatorScope... scopes) {
        super(validatorProvider, beanClass, context, scopes);
    }

    public JComponent getEditor() {
        return editor;
    }

    public void setEditor(JComponent editor) {
        this.editor = editor;
    }

}
