package org.nuiton.jaxx.widgets.text;

/*-
 * #%L
 * JAXX :: Widgets Text
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created by tchemit on 11/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class NormalTextEditorHandler implements UIHandler<NormalTextEditor> {

    private final NormalTextEditor ui;
    private PropertyChangeListener propertyChangeListener;

    NormalTextEditorHandler(NormalTextEditor ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(NormalTextEditor normalTextEditor) {
        ui.addPropertyChangeListener(NormalTextEditor.PROPERTY_MODEL, evt -> {
            JavaBean oldValue = (JavaBean) evt.getOldValue();
            JavaBean newValue = (JavaBean) evt.getNewValue();
            updateListenersFromModel(oldValue, newValue, this.ui.getProperty());
        });
        ui.addPropertyChangeListener(NormalTextEditor.PROPERTY_PROPERTY, evt -> {
            String oldValue = (String) evt.getOldValue();
            String newValue = (String) evt.getNewValue();
            updateListenersFromProperty(oldValue, newValue, this.ui.getModel());
        });
    }

    private void updateListenersFromModel(JavaBean oldModel, JavaBean newModel, String property) {
        removeListener(oldModel, property);
        addListener(newModel, property);
    }

    private void updateListenersFromProperty(String oldProperty, String newProperty, JavaBean model) {
        removeListener(model, oldProperty);
        addListener(model, newProperty);
    }

    private void addListener(JavaBean model, String property) {
        if (property != null && model != null) {
            propertyChangeListener = evt -> {
                Object newValue = evt.getNewValue();
                String oldText = ui.getTextEditor().getText();
                String newText = SwingUtil.getStringValue(newValue);
                if (!Objects.equals(oldText, newText)) {
                    ui.getTextEditor().setText(newText);
                }
            };
            model.addPropertyChangeListener(property, propertyChangeListener);
        }
    }

    private void removeListener(JavaBean model, String property) {
        if (property != null && model != null && propertyChangeListener != null) {
            model.removePropertyChangeListener(property, propertyChangeListener);
            propertyChangeListener = null;
        }
    }

}
