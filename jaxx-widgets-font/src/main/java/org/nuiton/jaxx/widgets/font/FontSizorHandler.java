/*
 * #%L
 * JAXX :: Widgets Font
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.font;

import org.nuiton.jaxx.runtime.binding.SimpleJAXXObjectBinding;
import org.nuiton.jaxx.runtime.spi.UIHandler;

/**
 * Handler of ui {@link FontSizor}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class FontSizorHandler implements UIHandler<FontSizor> {

    protected FontSizor ui;

    public static final String BINDING_FONT_SIZE_CALL_BACK = "fontSize.callBack";

    public static final String BINDING_DEFAULT_FONT_SIZE_CALL_BACK = "defaultFontSize.callBack";

    public void init() {
        if (ui.fontSize == null) {
            ui.setFontSize(ui.defaultFontSize);
        }
    }

    boolean updateDefaultSizeEnabled(Float fontSize, Float defaultFontSize, boolean enabled) {
        return !(fontSize == null || defaultFontSize == null) && !fontSize.equals(defaultFontSize) && enabled;
    }

    public void setCallBack(final Runnable action) {
        ui.removeDataBinding(BINDING_FONT_SIZE_CALL_BACK);
        ui.registerDataBinding(new SimpleJAXXObjectBinding(ui, BINDING_FONT_SIZE_CALL_BACK, true, "fontSize") {

            @Override
            public void processDataBinding() {
                action.run();
            }
        });
        ui.applyDataBinding(BINDING_FONT_SIZE_CALL_BACK);
    }

    @Override
    public void beforeInit(FontSizor ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(FontSizor ui) {
        ui.registerDataBinding(new SimpleJAXXObjectBinding(ui, BINDING_DEFAULT_FONT_SIZE_CALL_BACK, true, "defaultFontSize") {

            @Override
            public void processDataBinding() {
                ui.setFontSize(ui.defaultFontSize);
            }
        });
        //applyDataBinding(BINDING_DEFAULT_FONT_SIZE_CALL_BACK);
    }
}
