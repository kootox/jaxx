package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import java.util.ArrayList;
import java.util.List;

/**
 * A call back with his attached options.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class CallBackEntry {

    protected final String name;
    protected final String description;
    protected final Icon icon;
    protected final Runnable action;
    protected final List<OptionModel> options;

    public CallBackEntry(String name,
                         String description,
                         Icon icon,
                         Runnable action) {
        this.description = description;
        this.icon = icon;
        options = new ArrayList<>();
        this.name = name;
        this.action = action;
    }

    public Runnable getAction() {
        return action;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Icon getIcon() {
        return icon;
    }

    public List<OptionModel> getOptions() {
        // always send a copy
        return new ArrayList<>(options);
    }

    protected void addOption(OptionModel option) {
        options.add(option);

    }
}
