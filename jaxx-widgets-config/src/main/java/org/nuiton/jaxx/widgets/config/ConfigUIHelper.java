package org.nuiton.jaxx.widgets.config;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Supplier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.widgets.config.model.CallBackFinalizer;
import org.nuiton.jaxx.widgets.config.model.CategoryModel;
import org.nuiton.jaxx.widgets.config.model.ConfigUIModel;
import org.nuiton.jaxx.widgets.config.model.ConfigUIModelBuilder;
import org.nuiton.jaxx.widgets.config.model.OptionModel;

import javax.swing.Icon;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Frame;
import java.io.File;
import java.util.Objects;

/**
 * A helper to build a config ui.
 *
 * contains all states as method to build model, then ui and finally display it.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class ConfigUIHelper {

    public static final Log log = LogFactory.getLog(ConfigUIHelper.class);

    /** internal model builder */
    protected final ConfigUIModelBuilder modelBuilder;

    /** internal model after builder flush */
    protected ConfigUIModel model;

    protected ConfigUI ui;

    public ConfigUIHelper(Object configurationBean, ApplicationConfig config, File configFile) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(configurationBean, config, configFile);
    }

    @Deprecated
    public ConfigUIHelper(Supplier<ApplicationConfig> config) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(config, config.get(), config.get().getUserConfigFile());
    }

    @Deprecated
    public ConfigUIHelper(Supplier<ApplicationConfig> config, File configFile) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(config, config.get(), configFile);
    }

    @Deprecated
    public ConfigUIHelper(Object configurationBean, ApplicationConfig config) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(configurationBean, config, config.getUserConfigFile());
    }

    @Deprecated
    public ConfigUIHelper(ApplicationConfig config, File configFile) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(config, config, configFile);
    }

    @Deprecated
    public ConfigUIHelper(ApplicationConfig config) {
        modelBuilder = new ConfigUIModelBuilder();
        modelBuilder.createModel(config, config, config.getUserConfigFile());
    }

    public ConfigUIModel getModel() {
        if (model == null) {
            model = modelBuilder.flushModel();
        }
        return model;
    }

    public ConfigUIModelBuilder addCategory(String categoryName, String categoryLabel)
            throws IllegalStateException, NullPointerException {
        modelBuilder.addCategory(categoryName, categoryLabel);
        return modelBuilder;
    }

    public ConfigUIModelBuilder addCategory(String categoryName, String categoryLabel, String categoryCallback)
            throws IllegalStateException, NullPointerException {
        modelBuilder.addCategory(categoryName, categoryLabel, categoryCallback);
        return modelBuilder;
    }

    public ConfigUIModelBuilder addOption(ConfigOptionDef def)
            throws IllegalStateException, NullPointerException {
        modelBuilder.addOption(def);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOptionPropertyName(String propertyName)
            throws IllegalStateException, NullPointerException {
        modelBuilder.setOptionPropertyName(propertyName);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOptionShortLabel(String shortLabel)
            throws IllegalStateException, NullPointerException {
        modelBuilder.setOptionShortLabel(shortLabel);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOptionEditor(TableCellEditor editor)
            throws IllegalStateException, NullPointerException {
        modelBuilder.setOptionEditor(editor);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOptionRenderer(TableCellRenderer renderer)
            throws IllegalStateException, NullPointerException {
        modelBuilder.setOptionRenderer(renderer);
        return modelBuilder;
    }

    public ConfigUIModelBuilder registerCallBack(String name,
                                                 String description,
                                                 Icon icon,
                                                 Runnable action) {
        modelBuilder.registerCallBack(name, description, icon, action);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOptionCallBack(String name) {
        modelBuilder.setOptionCallBack(name);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setModel(ConfigUIModel model) throws IllegalStateException {
        modelBuilder.setModel(model);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setCategory(CategoryModel categoryModel)
            throws IllegalStateException {
        modelBuilder.setCategory(categoryModel);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setOption(OptionModel optionModel)
            throws IllegalStateException {
        modelBuilder.setOption(optionModel);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setFinalizer(CallBackFinalizer finalizer) {
        modelBuilder.setFinalizer(finalizer);
        return modelBuilder;
    }

    public ConfigUIModelBuilder setCloseAction(Runnable runnable) {
        modelBuilder.setCloseAction(runnable);
        return modelBuilder;
    }

    /**
     * Construire l'ui de configuration (sous forme de panel)
     *
     * @param parentContext   le context applicatif
     * @param defaultCategory la categorie a selectionner
     * @return l'ui instanciate
     */
    public ConfigUI buildUI(JAXXContext parentContext,
                            String defaultCategory) {

        ConfigUIModel model = getModel();

        JAXXContext tx = new JAXXInitialContext().add(parentContext).add(model);

        ui = new ConfigUI(tx);

        ui.init(defaultCategory);

        return ui;
    }

    public void displayUI(Frame parentUI, boolean undecorated) {
        Objects.requireNonNull(ui, "UI was not build, use before the *buildUI* method");
        ui.getHandler().displayUI(parentUI, undecorated);
    }
}
