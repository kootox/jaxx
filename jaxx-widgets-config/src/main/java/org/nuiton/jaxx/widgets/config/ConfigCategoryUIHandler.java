package org.nuiton.jaxx.widgets.config;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.renderer.ClassTableCellRenderer;
import org.nuiton.jaxx.runtime.swing.renderer.ColorCellRenderer;
import org.nuiton.jaxx.widgets.config.model.ConfigTableModel;
import org.nuiton.jaxx.widgets.config.model.OptionModel;

import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseEvent;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Handler of {@link ConfigCategoryUI}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class ConfigCategoryUIHandler implements UIHandler<ConfigCategoryUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ConfigCategoryUIHandler.class);

    private ConfigCategoryUI ui;

    @Override
    public void beforeInit(ConfigCategoryUI ui) {
        this.ui= ui;
    }

    @Override
    public void afterInit(ConfigCategoryUI ui) {

        JTable table = ui.getTable();

        // prepare table
        SwingUtil.setI18nTableHeaderRenderer(
                table,
                n("config.key"),
                n("config.key.tip"),
                n("config.value"),
                n("config.value.tip"),
                n("config.defaultValue"),
                n("config.defaultValue.tip"));

        ConfigTableRenderer renderer = new ConfigTableRenderer();
        SwingUtil.setTableColumnRenderer(table, 0, renderer);
        SwingUtil.setTableColumnRenderer(table, 1, renderer);
        SwingUtil.setTableColumnRenderer(table, 2, renderer);
        Font f = table.getFont().deriveFont(Font.ITALIC | Font.BOLD);
        int width = SwingUtil.computeTableColumnWidth(table, f, 0, "___*");
        SwingUtil.fixTableColumnWidth(table, 0, width);
        SwingUtil.setTableColumnEditor(table, 1, new ConfigTableEditor((ConfigTableModel) table.getModel()));
        table.setDefaultRenderer(Color.class, new ColorCellRenderer());
        table.setDefaultRenderer(Class.class, new ClassTableCellRenderer());
    }

    void updateDescriptionText() {
        OptionModel option;
        JTable table = ui.getTable();
        JTextArea description = ui.getDescription();
        ListSelectionModel selectionModel = ui.getSelectionModel();

        if (selectionModel.isSelectionEmpty()) {
            option = null;
        } else {
            int row = selectionModel.getAnchorSelectionIndex();
            ConfigTableModel m = (ConfigTableModel) table.getModel();
            option = m.getEntry(row);
            if (log.isDebugEnabled()) {
                log.debug(row + " : " + option);
            }
        }
        StringBuilder buffer = new StringBuilder();
        if (option == null) {
            buffer.append(t("config.no.option.selected"));
        } else {
            buffer.append(t("config.option.label", option.getKey(), t(option.getDescription()))).append('\n');
            if (option.isModified()) {
                String oValue = option.toString(option.getOriginalValue());
                String mValue = option.toString(option.getValue());
                buffer.append(t("config.option.modified", oValue, mValue)).append('\n');
            }
            if (option.isFinal()) {
                buffer.append(t("config.option.final")).append('\n');
            }
        }
        description.setText(buffer.toString());
    }

    public void copyCellValue() {

        JXTable table = ui.getTable();

        int[] selectedRows = table.getSelectedRows();

        int selectedRow = selectedRows[0];

        Integer selectedColumn = (Integer) ui.getCopyCellValue().getClientProperty("selectedColumn");

        ConfigTableModel tableModel = ui.getTableModel();
        OptionModel optionModel = tableModel.getEntry(selectedRow);

        Object value = null;
        switch (selectedColumn) {
            case 1:
                if (log.isInfoEnabled()) {
                    log.info("Will copy option value from " + optionModel.getKey());
                }
                value = optionModel.getValue();
                break;
            case 2:
                if (log.isInfoEnabled()) {
                    log.info("Will copy option default value from " + optionModel.getKey());
                }
                value = optionModel.getOriginalValue();
                break;
        }

        String text = "";
        if (value != null) {
            TableCellRenderer cellRenderer = table.getCellRenderer(selectedRow, selectedColumn);
            Component tableCellRendererComponent = cellRenderer.getTableCellRendererComponent(table, value, false, false, selectedRow, selectedColumn);

            text = ((JLabel) tableCellRendererComponent).getText();
        }

        if (log.isInfoEnabled()) {
            log.info("Copy to clipboard cell (" + selectedRow + "-" + selectedColumn + ") value: \"" + text + "\"");
        }

        JAXXUtil.copyToClipBoard(text);

    }

    public void resetOptionValue() {

        JXTable table = ui.getTable();

        int[] selectedRows = table.getSelectedRows();

        int selectedRow = selectedRows[0];

        ConfigTableModel tableModel = ui.getTableModel();
        OptionModel optionModel = tableModel.getEntry(selectedRow);

        if (log.isInfoEnabled()) {
            log.info("Will reset option: " + optionModel.getKey());
        }
        tableModel.setValueAt(optionModel.getDefaultValue(), selectedRow, 1);

    }

    void openTablePopupMenu(MouseEvent e, JPopupMenu popup) {

        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick) {

            JXTable source = (JXTable) e.getSource();

            int[] selectedRows = source.getSelectedRows();
            int[] selectedColumns = source.getSelectedColumns();

            // get the coordinates of the mouse click
            Point p = e.getPoint();

            // get the row index at this point
            int rowIndex = source.rowAtPoint(p);

            // get the column index at this point
            int columnIndex = source.columnAtPoint(p);

            // select row (could empty selection)
            if (rowIndex == -1) {
                source.clearSelection();
            } else if (!ArrayUtils.contains(selectedRows, rowIndex)) {

                // set selection
                source.setRowSelectionInterval(rowIndex, rowIndex);
            }

            // select column (could empty selection)
            if (columnIndex == -1) {
                source.clearSelection();
            } else if (!ArrayUtils.contains(selectedColumns, columnIndex)) {
                source.setColumnSelectionInterval(columnIndex, columnIndex);
            }

            int selectedRowCount = source.getSelectedRowCount();

            boolean enableCopyOptionValue = selectedRowCount == 1;
            boolean enableResetOptionValue = selectedRowCount > 0;

            if (log.isDebugEnabled()) {
                log.debug("At point [" + p + "] found Row " + rowIndex + ", Column " + columnIndex);
            }

            boolean canContinue = true;

            if (source.isEditing()) {

                // stop editing
                boolean stopEdit = source.getCellEditor().stopCellEditing();
                if (!stopEdit) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not stop edit cell...");
                    }
                    canContinue = false;
                }
            }

            int selectedColumn = source.getSelectedColumn();
            ui.getCopyCellValue().putClientProperty("selectedColumn", selectedColumn);

            ui.getCopyCellValue().setEnabled(enableCopyOptionValue);
            ui.getResetOptionValue().setEnabled(enableResetOptionValue);

            if (canContinue) {

                // on right click show popup
                popup.show(source, e.getX(), e.getY());

            }
        }
    }
}
