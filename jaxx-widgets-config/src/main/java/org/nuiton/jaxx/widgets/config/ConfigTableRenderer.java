package org.nuiton.jaxx.widgets.config;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.widgets.config.model.ConfigTableModel;
import org.nuiton.jaxx.widgets.config.model.OptionModel;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour le rendu du tableau des options d'une categorie
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ConfigTableModel
 * @since 2.5.11
 */
public class ConfigTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected static Color col;

    protected static Font font;

    protected static Font font2;

    public ConfigTableRenderer() {
        col = getForeground();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        int modelRow = table.convertRowIndexToModel(row);
        int modelColumn = table.convertColumnIndexToModel(column);
        ConfigTableModel model = (ConfigTableModel) table.getModel();
        OptionModel key = model.getEntry(modelRow);
        boolean isModified = key.isModified();

        boolean isValid = key.isValid();

        if (font == null) {
            font = getFont();
            font2 = font.deriveFont(Font.ITALIC | Font.BOLD);
        }
        Component cellRenderer;
        switch (modelColumn) {
            case 0:
                cellRenderer = getKeyCellRenderer(table, value, isSelected, hasFocus, modelRow, modelColumn, key, isValid, isModified);
                break;
            case 1:
                cellRenderer = getValueCellRenderer(table, value, isSelected, hasFocus, modelRow, modelColumn, key, isValid, isModified);
                break;
            case 2:
                cellRenderer = getValueCellRenderer(table, value, isSelected, hasFocus, modelRow, modelColumn, key, isValid, isModified);
                break;
            default:
                throw new IllegalStateException("no renderer find for column " + modelColumn);
        }
        return cellRenderer;
    }

    protected Component getKeyCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column, OptionModel key, boolean isValid, boolean isModified) {
        String tooltip = t(key.getDescription()) + " (" + key.getKey() + ")";
        String originalValue = key.toString(key.getOriginalValue());
        boolean isFinal = key.isFinal();
        if (isFinal) {
            tooltip += " [" + t("config.unmodifiable") + ']';
        }
        if (isModified) {
            String s = t("config.modified", originalValue);
            value = value + " *";
            tooltip += " [" + s + ']';
        }
        if (!isValid) {
            String s2 = t("config.unvalid", originalValue, key.getType());
            tooltip += " (" + s2 + ")";
            value = value + " !";
        }
        JComponent result = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        result.setToolTipText(tooltip);
        result.setForeground(isValid ? col : Color.RED);
        result.setFont(isModified || !isValid ? font2 : font);
        result.setEnabled(!isFinal);
        return result;
    }

    protected Component getValueCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column, OptionModel key, boolean isValid, boolean isModified) {
        Component result;
        if (key.getRenderer() != null) {
            result = key.getRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        } else {
            TableCellRenderer defaultRenderer = table.getDefaultRenderer(key.getType());
            result = defaultRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        return result;
    }
}
