package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.converter.ConverterUtil;

import javax.swing.table.AbstractTableModel;

/**
 * le modele du tableau d'options pour une categorie donnee.
 *
 * Le modele se base sur le modele d'une categorie d'option.
 *
 * @author tchemit
 * @see CategoryModel
 * @since 2.5.11
 */
public class ConfigTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private static final Class<?>[] columnClass = {String.class, Object.class, String.class};

    /** le modele d'une categorie */
    protected final CategoryModel categoryModel;

    public ConfigTableModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
        // listen of property reload of the category model
        // to known when to refresh table
        this.categoryModel.addPropertyChangeListener(CategoryModel.RELOAD_PROPERTY_NAME, evt -> fireTableDataChanged());
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public OptionModel getEntry(int rowIndex) {
        return categoryModel.getEntries().get(rowIndex);
    }

    @Override
    public int getRowCount() {
        return categoryModel.getEntries().size();
    }

    @Override
    public int getColumnCount() {
        return columnClass.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1 && !getEntry(rowIndex).isFinal();
    }

    @Override
    public Object getValueAt(int row, int column) {
        OptionModel key = getEntry(row);
        Object value = null;
        switch (column) {
            case 0:
                // try first to use the shortLabel
                value = key.getShortLabel();
                if (StringUtils.isBlank((String) value)) {

                    // fallback to key
                    value = key.getKey();
                }
                break;
            case 1:
                value = key.getValue();
                break;
            case 2:
                value = key.getDefaultValue();
                if (value != null) {
                    value = ConverterUtil.convert(key.getType(), value);
                }
                break;
        }
        return value;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        if (column != 1) {
            // seul la colonne 1 est editable (valeur de l'option)
            throw new IllegalArgumentException("can not edit column " + column);
        }
        OptionModel key = getEntry(row);
        Object val;
        if (aValue == null || key.getType().equals(aValue.getClass())) {
            val = aValue;
        } else {
            String valStr = String.valueOf(aValue).trim();
            try {
                val = ConverterUtil.convert(key.getType(), valStr);
                if (val != null && val instanceof Integer) {
                    if (new Integer(0).equals(val) && !valStr.equals("0")) {
                        val = null;
                    }
                }
            } catch (Exception e) {
                val = null;
            }
        }
        categoryModel.setValue(key, val);
        fireTableRowsUpdated(row, row);
    }


    public void destroy() {
        categoryModel.destroy();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }
}
