package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ArrayUtils;
import org.nuiton.config.ConfigOptionDef;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.util.Objects;

/**
 * le modele d'une option de la configuration a editer.
 *
 * @author tchemit
 * @since 2.5.11
 */
public class OptionModel implements ConfigOptionDef {

    private static final long serialVersionUID = 1L;

    /** la definition de l'option (venant de la config) */
    protected final ConfigOptionDef def;

    /** un drapeau pour savoir si l'option est valide (n'est pas utilisé actuellement) */
    protected boolean valid = true;

    /** un drapeau pour savoir si l'option a été sauvée */
    protected boolean saved;

    /** la valeur non modifié de l'option */
    protected Object originalValue;

    /** la valeur actuelle de l'option (peut être la valeur orignal si non modifée) */
    protected Object value;

    /** le nom de la propriété javaBean (peut etre null, si option sans support javaBean) */
    protected String propertyName;

    /**
     * Une description courte à utiliser à la place de la clef i18n.
     *
     * @since 2.5.29
     */
    protected String shortLabel;

    /** l'editeur utilise pour modifier graphiquement l'option */
    protected TableCellEditor editor;

    /** le renderer utilise pour afficher graphiquement l'option */
    protected TableCellRenderer renderer;

    protected OptionModel(ConfigOptionDef def, Object value) {
        this.def = def;
        initValue(value);
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    @Override
    public String getKey() {
        return def.getKey();
    }

    @Override
    public Class<?> getType() {
        return def.getType();
    }

    public boolean isArrayType() {
        return getType().isArray();
    }

    @Override
    public String getDescription() {
        return def.getDescription();
    }

    @Override
    public String getDefaultValue() {
        return def.getDefaultValue();
    }

    @Override
    public boolean isTransient() {
        return def.isTransient();
    }

    @Override
    public boolean isFinal() {
        return def.isFinal();
    }

    public Object getOriginalValue() {
        return originalValue;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        def.setDefaultValue(defaultValue);
    }

    @Override
    public void setTransient(boolean isTransient) {
        def.setTransient(isTransient);
    }

    @Override
    public void setFinal(boolean isFinal) {
        def.setFinal(isFinal);
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean isModified() {
        boolean result;
        if (isArrayType()) {
            result = !Objects.deepEquals(originalValue, value);
        } else {
            result = !Objects.equals(originalValue, value);
        }
        return result;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public void initValue(Object originalValue) {
        this.originalValue = originalValue;
        value = originalValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public TableCellEditor getEditor() {
        return editor;
    }

    protected void setEditor(TableCellEditor editor) {
        this.editor = editor;
    }

    public TableCellRenderer getRenderer() {
        return renderer;
    }

    public void setRenderer(TableCellRenderer renderer) {
        this.renderer = renderer;
    }

    protected void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * Get a string value of the given {@code value} considered as a value of
     * this option, means if option is array type, then we will decorate the
     * value as a array.
     *
     * @param value value to decorate
     * @return decorated value
     * @since 2.5.14
     */
    public String toString(Object value) {
        String result;
        if (value == null) {
            result = null;
        } else {
            if (isArrayType()) {
                result = ArrayUtils.toString(value);
            } else {
                result = String.valueOf(value);
            }
        }
        return result;
    }
}
