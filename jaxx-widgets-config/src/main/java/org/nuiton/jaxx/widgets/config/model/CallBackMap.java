package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.List;

/**
 * A convient map of callback entry for a list of option.
 *
 * This model is used to store callbacks to use before a saving action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class CallBackMap extends LinkedHashMap<CallBackEntry, List<OptionModel>> {
    private static final long serialVersionUID = 1L;

    public boolean containsCallBack(String callBackName) {
        for (CallBackEntry entry : keySet()) {
            if (callBackName.equals(entry.getName())) {
                return true;
            }
        }
        return false;
    }

    public CallBackEntry getCallBack(String callBackName) {
        for (CallBackEntry entry : keySet()) {
            if (callBackName.equals(entry.getName())) {
                return entry;
            }
        }
        return null;
    }
}
