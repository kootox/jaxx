package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * CallBack manager.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class CallBacksManager {

    /** lists of registred callback. */
    protected final List<CallBackEntry> callbacks;

    /** optional callbacks finalizer */
    protected CallBackFinalizer finalizer;

    public CallBacksManager() {
        callbacks = new ArrayList<>();
    }

    /**
     * Obtain the finalizer to use.
     *
     * @return the finalizer (can be {@code null}).
     * @see CallBackFinalizer
     */
    public CallBackFinalizer getFinalizer() {
        return finalizer;
    }

    /**
     * Sets the finalizer.
     *
     * @param finalizer the new finalizer
     * @see CallBackFinalizer
     */
    public void setFinalizer(CallBackFinalizer finalizer) {
        this.finalizer = finalizer;
    }

    /**
     * Registers a new callback.
     *
     * <b>Note:</b> the order of registred callback is used to determine
     * the higher priority of callback to launch if required.
     *
     * @param name        the unique name of a callback
     * @param description the i18n key to describe the action
     * @param icon        icon of callBack (used in ui)
     * @param action      the action of the callback
     */
    public void registerCallBack(String name,
                                 String description,
                                 Icon icon,
                                 Runnable action) {
        if (name == null) {
            throw new NullPointerException(
                    "parameter 'name' can not be null");
        }
        if (action == null) {
            throw new NullPointerException(
                    "parameter 'action' can not be null");
        }
        if (description == null) {
            throw new NullPointerException(
                    "parameter 'description' can not be null");
        }
        if (icon == null) {
            throw new NullPointerException("parameter 'icon' can not be null");
        }
        if (getCallBack(name) != null) {
            throw new IllegalArgumentException(
                    "there is already a callback with name '" + name + "'");
        }
        callbacks.add(new CallBackEntry(name, description, icon, action));
    }

    /**
     * Registers a option into a known callback.
     *
     * @param name   the name of the callback
     * @param option the option to register for the given callback
     */
    public void registerOption(String name, OptionModel option) {
        if (name == null) {
            throw new NullPointerException("parameter 'name' can not be null");
        }
        if (option == null) {
            throw new NullPointerException(
                    "parameter 'option' can not be null");
        }
        CallBackEntry callback = getCallBack(name);
        if (callback == null) {
            throw new IllegalArgumentException(
                    "could not find a callback with name '" + name + "'");
        }
        callback.addOption(option);
    }

    /**
     * Scan a model and grab per callBack the options saved.
     *
     * @param model the model to scan
     * @return the dictionnary of options for each callback to launch
     */
    public CallBackMap getCallBacksForSaved(ConfigUIModel model) {

        return getCallBacks(model, true, false);
    }

    /**
     * Scan a category and grab per callBack the options saved.
     *
     * @param category the category to scan
     * @return the dictionnary of options for each callBack to launch
     */
    public CallBackMap getCallBacksForSaved(CategoryModel category) {

        return getCallBacks(category, true, false);
    }

    /**
     * Scan a model and grab per callBack the options.
     *
     * @param model    the model to scan
     * @param modified {@code true} to include modified options
     * @param saved    {@code true} to include saved options
     * @return the dictionnary of options for each callback to launch
     */
    public CallBackMap getCallBacks(ConfigUIModel model,
                                    boolean saved,
                                    boolean modified) {

        CallBackMap result = new CallBackMap();

        for (CategoryModel categoryModel : model) {
            CallBackMap callBacks =
                    getCallBacks(categoryModel, saved, modified);
            for (Map.Entry<CallBackEntry, List<OptionModel>> entry :
                    callBacks.entrySet()) {
                CallBackEntry key = entry.getKey();
                List<OptionModel> value = entry.getValue();
                if (result.containsKey(key)) {
                    result.get(key).addAll(value);
                } else {
                    result.put(key, value);
                }
            }
            callBacks.clear();
        }

        CallBackFinalizer finalizer = getFinalizer();
        if (finalizer != null) {
            result = finalizer.finalize(result);
        }
        return result;
    }

    /**
     * Scan a category and grab per callBack the options.
     *
     * @param category the category to scan
     * @param modified {@code true} to include modified options
     * @param saved    {@code true} to include saved options
     * @return the dictionnary of options for each callBack to launch
     */
    public CallBackMap getCallBacks(CategoryModel category,
                                    boolean saved,
                                    boolean modified) {

        CallBackMap result = new CallBackMap();

        for (OptionModel optionModel : category) {
            if (modified && optionModel.isModified() ||
                    saved && optionModel.isSaved()) {
                CallBackEntry callBackEntry = getCallBack(optionModel);
                if (callBackEntry != null) {
                    List<OptionModel> models = result.computeIfAbsent(callBackEntry, k -> new ArrayList<>());
                    models.add(optionModel);
                }
            }
        }
        return result;
    }

    /**
     * Get the first callBack for a given option.
     *
     * @param option the option
     * @return the first callBack (so the most important) on which the given
     * option is attacjed. (can be null)
     */
    protected CallBackEntry getCallBack(OptionModel option) {
        for (CallBackEntry callback : callbacks) {
            if (callback.getOptions().contains(option)) {
                return callback;
            }
        }
        return null;
    }

    /**
     * Obtain a registred callBack from his name.
     *
     * @param name the name of the searched callBack
     * @return the callBack for the given name (or {@code null} if not found).
     */
    protected CallBackEntry getCallBack(String name) {
        for (CallBackEntry callback : callbacks) {
            if (callback.getName().equals(name)) {
                return callback;
            }
        }
        return null;
    }

}
