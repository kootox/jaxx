package org.nuiton.jaxx.widgets.config;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.config.model.CallBackEntry;
import org.nuiton.jaxx.widgets.config.model.CallBackMap;
import org.nuiton.jaxx.widgets.config.model.OptionModel;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Component;
import java.awt.Window;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * callBackUI handler
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
class ConfigCallBackUIHandler {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigCallBackUIHandler.class);

    void init(ConfigCallBackUI ui) {

        // build tree model
        DefaultMutableTreeNode root = new DefaultMutableTreeNode();

        CallBackMap forSaved = ui.getContextValue(
                CallBackMap.class, ConfigUIHandler.CALLBACKS_WITH_OPTIONS);

        for (Map.Entry<CallBackEntry, List<OptionModel>> e :
                forSaved.entrySet()) {

            CallBackEntry callBackEntry = e.getKey();
            List<OptionModel> options = e.getValue();

            DefaultMutableTreeNode callBackNode;
            callBackNode = new DefaultMutableTreeNode(callBackEntry, true);

            root.add(callBackNode);
            for (OptionModel o : options) {

                DefaultMutableTreeNode optionkNode;
                optionkNode = new DefaultMutableTreeNode(o, false);

                callBackNode.add(optionkNode);
            }
        }

        JTree tree = ui.getDetectedCallBack();

        tree.setModel(new DefaultTreeModel(root));

        SwingUtil.expandTree(tree);

        tree.setCellRenderer(new DefaultTreeCellRenderer() {
            private static final long serialVersionUID = -6532818343647280782L;

            @Override
            public Component getTreeCellRendererComponent(JTree tree,
                                                          Object value,
                                                          boolean sel,
                                                          boolean expanded,
                                                          boolean leaf,
                                                          int row,
                                                          boolean hasFocus) {
                if (log.isDebugEnabled()) {
                    log.debug("value = " + value.getClass());
                }
                if (value == null) {
                    return super.getTreeCellRendererComponent(
                            tree,
                            value,
                            sel,
                            expanded,
                            leaf,
                            row,
                            hasFocus);
                }

                if (!(value instanceof DefaultMutableTreeNode)) {
                    return super.getTreeCellRendererComponent(
                            tree,
                            value,
                            sel,
                            expanded,
                            leaf,
                            row,
                            hasFocus);
                }

                DefaultMutableTreeNode n = (DefaultMutableTreeNode) value;
                value = n.getUserObject();

                if (value instanceof CallBackEntry) {
                    CallBackEntry v = (CallBackEntry) value;
                    if (log.isDebugEnabled()) {
                        log.debug("callBackEntry detected " + v.getName());
                    }
                    value = t(v.getDescription());
                } else if (value instanceof OptionModel) {

                    OptionModel v = (OptionModel) value;
                    if (log.isDebugEnabled()) {
                        log.debug("option detected " + v.getKey());
                    }
                    value = v.getKey() + " (" + t(v.getDescription()) + ")";
                }

                JLabel rendererComponent;
                rendererComponent = (JLabel)
                        super.getTreeCellRendererComponent(
                                tree,
                                value,
                                sel,
                                expanded,
                                leaf,
                                row,
                                hasFocus);

                value = n.getUserObject();

                if (value instanceof CallBackEntry) {

                    CallBackEntry v = (CallBackEntry) value;
                    rendererComponent.setIcon(v.getIcon());
                }
                return rendererComponent;
            }
        });
    }

    void doAction(final ConfigCallBackUI ui) {
        log.info("Launch callBacks...");
        Window parent = ui.getContextValue(Window.class, "parent");
        if (parent != null) {
            log.info("dispose parent window...");
            parent.dispose();
        }

        SwingUtilities.invokeLater(() -> {

            List<CallBackEntry> callBacks = JAXXUtil.<CallBackEntry>newListContextEntryDef(ConfigUIHandler.CALLBACKS).getContextValue(ui);
            for (CallBackEntry e : callBacks) {
                if (log.isInfoEnabled()) {
                    log.info("launch callBack " + t(e.getDescription()));
                }
                e.getAction().run();
            }
        });
    }
}
