package org.nuiton.jaxx.widgets.config;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.runtime.swing.editor.ClassCellEditor;
import org.nuiton.jaxx.runtime.swing.editor.ColorCellEditor;
import org.nuiton.jaxx.runtime.swing.editor.EnumEditor;
import org.nuiton.jaxx.runtime.swing.editor.KeyStrokeCellEditor;
import org.nuiton.jaxx.runtime.swing.editor.LocaleEditor;
import org.nuiton.jaxx.widgets.config.model.ConfigTableModel;
import org.nuiton.jaxx.widgets.config.model.OptionModel;
import org.nuiton.jaxx.widgets.file.FileCellEditor;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.EventObject;
import java.util.Locale;

/**
 * L'éditeur des valeurs des propriétés d'une configuration
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class ConfigTableEditor implements TableCellEditor {

    protected TableCellEditor delegate;

    protected final ConfigTableModel model;

    public ConfigTableEditor(ConfigTableModel model) {
        this.model = model;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        delegate = findDelegate(table, model.getEntry(row));
        return delegate.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        return !hasDelegate() ? null : delegate.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return !hasDelegate() || delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return hasDelegate() && delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return !hasDelegate() || delegate.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        if (hasDelegate()) {
            delegate.cancelCellEditing();
        }
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        if (hasDelegate()) {
            delegate.addCellEditorListener(l);
        }
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        if (hasDelegate()) {
            delegate.removeCellEditorListener(l);
        }
    }

    protected TableCellEditor findDelegate(JTable table, OptionModel option) {
        Class<?> type = option.getType();
        if (char.class.equals(type)) {
            type = String.class;
        }
        TableCellEditor editor = table.getDefaultEditor(type);
        TableCellEditor defaultEditor = table.getDefaultEditor(Object.class);
        // always Search from option.getEditor() and store editor as cache
        if (option.getEditor() != null) {
            editor = option.getEditor();
            table.setDefaultEditor(type, editor);

        } else if (editor.equals(defaultEditor)) {
            // find not a specialized editor for the type
            if (type.isEnum()) {
                // add a EnumEditor to table
                editor = new DefaultCellEditor(EnumEditor.newEditor((Class<Enum>) type));

            } else if (type.equals(Class.class)) {
                editor = new ClassCellEditor();

            } else if (type.equals(File.class)) {
                editor = new FileCellEditorWithExtDetector();

            } else if (type.equals(KeyStroke.class)) {
                editor = new KeyStrokeCellEditor();

            } else if (type.equals(Locale.class)) {
                editor = new DefaultCellEditor(LocaleEditor.newEditor());

            } else if (type.equals(Color.class)) {
                editor = new ColorCellEditor();

            } else {
                editor = table.getDefaultEditor(String.class);
            }
            table.setDefaultEditor(type, editor);
        }
        if (editor == null) {
            throw new IllegalStateException("could not find a editor for type +" + type);
        }
        return editor;
    }

    protected static class FileCellEditorWithExtDetector extends FileCellEditor {

        private static final long serialVersionUID = 1L;

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

            String fileExtension = value == null ? null : Files.getFileExtension(((File) value).getName());
            if (StringUtils.isNotEmpty(fileExtension)) {
                fileEditor.setExts(fileExtension);
                fileEditor.setAcceptAllFileFilterUsed(true);
            }
            return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
    }

    protected boolean hasDelegate() {
        return delegate != null;
    }
}
