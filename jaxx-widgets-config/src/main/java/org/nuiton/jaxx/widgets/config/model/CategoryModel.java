package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXUtil;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * le modele d'une categorie d'options.
 *
 * Une categorie est un ensemble d'options.
 *
 * @author tchemit
 * @since 2.5.11
 */
public class CategoryModel implements Iterable<OptionModel>, Serializable {

    private static final long serialVersionUID = 1L;

    public static final String RELOAD_PROPERTY_NAME = "reload";

    public static final String MODIFIED_PROPERTY_NAME = "modified";

    public static final String VALID_PROPERTY_NAME = "valid";

    /** category short name (i18n key) */
    protected final String category;

    /** category long name (i18n key) */
    protected final String categoryLabel;

    /** options of the category */
    protected final List<OptionModel> entries;

    /** suport of modification */
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    protected CategoryModel(String category, String categoryLabel) {
        this.category = category;
        this.categoryLabel = categoryLabel;
        entries = new ArrayList<>();
    }

    protected void addOption(OptionModel option) {
        entries.add(option);
    }

    @Deprecated
    public CategoryModel(String category,
                         String categoryLabel,
                         OptionModel[] entries) {
        this.category = category;
        this.categoryLabel = categoryLabel;
        this.entries = Collections.unmodifiableList(Arrays.asList(entries));
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public List<OptionModel> getEntries() {
        return entries;
    }

    public boolean isModified() {
        boolean modified = false;
        for (OptionModel m : this) {
            if (m.isModified()) {
                modified = true;
                break;
            }
        }
        return modified;
    }

    public boolean isValid() {
        boolean valid = true;
        for (OptionModel m : this) {
            if (!m.isValid()) {
                valid = false;
                break;
            }
        }
        return valid;
    }

    public OptionModel getOptionModel(String optionModelKey) {
        OptionModel result = null;
        for (OptionModel optionModel : this) {
            if (optionModelKey.endsWith(optionModel.getKey())) {
                result = optionModel;
                break;
            }
        }
        return result;
    }

    public void setValue(OptionModel key, Object val) {
        boolean wasModified = isModified();
        boolean wasValid = isValid();
        key.setValue(val);
        boolean modified = isModified();
        boolean valid = isValid();
        if (wasModified != modified) {
            // change modified state
            firePropertyChange(MODIFIED_PROPERTY_NAME, wasModified, modified);
        }
        if (wasValid != valid) {
            // change valid state
            firePropertyChange(VALID_PROPERTY_NAME, wasValid, valid);
        }
    }

    @Override
    public Iterator<OptionModel> iterator() {
        return entries.iterator();
    }

    public List<OptionModel> getInvalidOptions() {

        List<OptionModel> result = new ArrayList<>();
        for (OptionModel m : this) {
            if (!m.isValid()) {
                result.add(m);
            }
        }
        return result;
    }

    public List<OptionModel> getModifiedOptions() {

        List<OptionModel> result = new ArrayList<>();
        for (OptionModel m : this) {
            if (m.isModified()) {
                result.add(m);
            }
        }
        return result;
    }

    public List<OptionModel> getSavedOptions() {
        List<OptionModel> result = new ArrayList<>();
        for (OptionModel option : this) {
            if (option.isSaved()) {
                result.add(option);
            }
        }
        return result;
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public boolean hasListeners(String propertyName) {
        return pcs.hasListeners(propertyName);
    }

    public PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        return pcs.getPropertyChangeListeners(propertyName);
    }

    public PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners();
    }

    public void destroy() {
        JAXXUtil.destroy(pcs);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }
}
