package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * A finalizer to mark a special category to eat every others categories
 * when finalizing.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class MainCallBackFinalizer implements CallBackFinalizer {

    /** name of call back which eats everybody */
    protected final String mainCallBack;

    public MainCallBackFinalizer(String mainCallBack) {
        this.mainCallBack = mainCallBack;
    }

    @Override
    public CallBackMap finalize(CallBackMap result) {
        CallBackEntry applicationEntry = result.getCallBack(mainCallBack);
        CallBackMap newResult;
        if (applicationEntry == null) {
            // rien n'a change
            newResult = result;
        } else {
            newResult = new CallBackMap();
            // on passe toutes les options sur ce callback
            List<OptionModel> options = new ArrayList<>();

            for (List<OptionModel> optionModels : result.values()) {
                options.addAll(optionModels);
            }
            newResult.put(applicationEntry, options);
        }
        return newResult;
    }
}
