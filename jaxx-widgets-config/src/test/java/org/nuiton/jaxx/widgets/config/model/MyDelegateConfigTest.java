package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.editor.MyDefaultCellEditor;

import javax.swing.ImageIcon;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.io.File;
import java.util.Collection;

/**
 * To test ConfigUI api on {@link MyDelegateConfig}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class MyDelegateConfigTest {


    public static final String CATEGORY = "cat0";

    protected ConfigUIModelBuilder builder;

    protected MyDelegateConfig config;

    @Before
    public void setup() {
        config = new MyDelegateConfig();
        builder = new ConfigUIModelBuilder();
    }

    @Test(expected = NullPointerException.class)
    public void testCreateModelLimitCase0() throws Exception {
        builder.createModel((ApplicationConfig) null);
    }

    @Test(expected = IllegalStateException.class)
    public void testFlushModelLimitCase0() throws Exception {
        builder.flushModel();
    }

    @Test
    public void testCreateModel() throws Exception {
        ConfigUIModel configModel = builder
                .createModel(config)
                .flushModel();
        Assert.assertNotNull(configModel);
        Assert.assertNotNull(configModel.getApplicationConfig());
    }

    @Test(expected = IllegalStateException.class)
    public void testRegisterCallbackLimitCase() throws Exception {
        builder.registerCallBack(null, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testRegisterCallbackLimitCase0() throws Exception {
        builder.createModel(config)
               .registerCallBack(null, null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testRegisterCallbackLimitCase1() throws Exception {
        builder.createModel(config)
               .registerCallBack("yo", null, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testRegisterCallbackLimitCase2() throws Exception {
        builder.createModel(config)
               .registerCallBack("yo", "yo description", null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testRegisterCallbackLimitCase4() throws Exception {
        Runnable callback = () -> {
        };
        builder.createModel(config)
               .registerCallBack("yo", "yo description", null, callback);
    }

    @Test
    public void testRegisterCallback() throws Exception {
        Runnable callback = () -> {
        };
        ImageIcon icon = SwingUtil.createActionIcon("config-copy");
        ConfigUIModel configModel = builder.createModel(config)
                                           .registerCallBack("yo", "yo description", icon, callback)
                                           .flushModel();
        Assert.assertNotNull(configModel);
        Assert.assertNotNull(configModel.getApplicationConfig());
        CallBackEntry callBackEntry =
                configModel.getCallBacksManager().getCallBack("yo");
        Assert.assertNotNull(callBackEntry);

        Assert.assertEquals(callback, callBackEntry.getAction());
    }

    @Test(expected = IllegalStateException.class)
    public void testAddCategoryLimitCase0() throws Exception {
        builder.addCategory(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testAddCategoryLimitCase1() throws Exception {
        builder.createModel(config)
               .addCategory(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testAddCategoryLimitCase2() throws Exception {
        builder.createModel(config)
               .addCategory("", null);
    }

    @Test
    public void testAddCategory() throws Exception {
        CategoryModel categoryModel = builder
                .createModel(config)
                .addCategory("cat0", "cat0 label")
                .flushCategory();
        Assert.assertNotNull(categoryModel);
        Assert.assertEquals("cat0", categoryModel.category);
        Assert.assertEquals("cat0 label", categoryModel.categoryLabel);
        Assert.assertEquals(0, categoryModel.entries.size());

    }

    @Test(expected = IllegalStateException.class)
    public void testAddOptionLimitCase0() throws Exception {
        builder.addOption(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testAddOptionLimitCase1() throws Exception {
        builder.createModel(config)
               .addOption(null);
    }

    @Test(expected = NullPointerException.class)
    public void testAddOptionLimitCase2() throws Exception {
        builder.createModel(config)
               .addCategory("cat0", "cat0 label")
               .addOption(null);
    }

    @Test
    public void testAddOption() throws Exception {
        OptionModel optionModel = builder
                .createModel(config)
                .addCategory("cat0", "cat0 label")
                .addOption(MyDelegateConfig.Option.LOCALE)
                .flushOption();
        Assert.assertNotNull(optionModel);
        Assert.assertEquals(MyDelegateConfig.Option.LOCALE, optionModel.def);

        CategoryModel categoryModel = builder.flushCategory();
        Assert.assertNotNull(categoryModel);
        Assert.assertEquals("cat0", categoryModel.category);
        Assert.assertEquals("cat0 label", categoryModel.categoryLabel);
        Assert.assertEquals(1, categoryModel.entries.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionPropertyNameLimitCase0() throws Exception {
        builder.setOptionPropertyName(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionPropertyNameLimitCase1() throws Exception {
        builder.createModel(config)
               .setOptionPropertyName(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionPropertyNameLimitCase2() throws Exception {
        builder.createModel(config)
               .addCategory("cat0", "cat0 label")
               .setOptionPropertyName(null);
    }

    @Test(expected = NullPointerException.class)
    public void testSetOptionPropertyNameLimitCase3() throws Exception {
        builder.createModel(config)
               .addCategory("cat0", "cat0 label")
               .addOption(MyDelegateConfig.Option.LOCALE)
               .setOptionPropertyName(null);
    }

    @Test
    public void testSetOptionPropertyName() throws Exception {
        OptionModel optionModel = builder
                .createModel(config)
                .addCategory("cat0", "cat0 label")
                .addOption(MyDelegateConfig.Option.LOCALE)
                .setOptionPropertyName(MyDelegateConfig.PROPERTY_LOCALE)
                .flushOption();
        Assert.assertNotNull(optionModel);
        Assert.assertEquals(MyDelegateConfig.Option.LOCALE, optionModel.def);
        Assert.assertEquals(MyDelegateConfig.PROPERTY_LOCALE, optionModel.propertyName);
        Assert.assertNull(optionModel.editor);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionEditorLimitCase0() throws Exception {
        builder.setOptionEditor(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionEditorLimitCase1() throws Exception {
        builder.createModel(config)
               .setOptionEditor(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetOptionEditorLimitCase2() throws Exception {
        builder.createModel(config)
               .addCategory("cat0", "cat0 label")
               .setOptionEditor(null);
    }


    @Test(expected = NullPointerException.class)
    public void testSetOptionEditorLimitCase3() throws Exception {
        builder.createModel(config)
               .addCategory("cat0", "cat0 label")
               .addOption(MyDelegateConfig.Option.LOCALE)
               .setOptionEditor(null);
    }

    @Test
    public void testSetOptionEditor() throws Exception {
        TableCellEditor cellEditor = MyDefaultCellEditor.newBooleanEditor();
        OptionModel optionModel = builder.createModel(config)
                                         .addCategory("cat0", "cat0 label")
                                         .addOption(MyDelegateConfig.Option.LOCALE)
                                         .setOptionEditor(cellEditor)
                                         .flushOption();
        Assert.assertNotNull(optionModel);
        Assert.assertEquals(MyDelegateConfig.Option.LOCALE, optionModel.def);
        Assert.assertNull(optionModel.propertyName);
        Assert.assertEquals(cellEditor, optionModel.editor);
    }

    @Test
    public void testFlushModel() throws Exception {
        ConfigUIModel configModel = builder
                .createModel(config)
                .flushModel();
        Assert.assertNotNull(configModel);
        Assert.assertNull(builder.model);
    }

    @Test
    public void testSaveClassOption() throws Exception {
        ConfigUIModel configModel = builder
                .createModel(config)
                .addCategory(CATEGORY, "cat0 label")
                .addOption(MyDelegateConfig.Option.DEMO_CLASS)
                .setOptionPropertyName(MyDelegateConfig.PROPERTY_DEMO_CLASS)
                .flushModel();
        configModel.setCategory(CATEGORY);

        CategoryModel categoryModel = configModel.getCategoryModel();
        Assert.assertNotNull(categoryModel);

        OptionModel optionModel = categoryModel.getOptionModel(MyDelegateConfig.Option.DEMO_CLASS.getKey());
        Assert.assertNotNull(optionModel);

        categoryModel.setValue(optionModel, Collection.class);

        Assert.assertEquals(File.class, config.getDemoClass());

        configModel.saveModified();

        Assert.assertEquals(Collection.class, config.getDemoClass());
    }

    @Test
    public void testSaveColorOption() throws Exception {
        ConfigUIModel configModel = builder
                .createModel(config)
                .addCategory(CATEGORY, "cat0 label")
                .addOption(MyDelegateConfig.Option.DEMO_COLOR)
                .flushModel();
        configModel.setCategory(CATEGORY);

        CategoryModel categoryModel = configModel.getCategoryModel();
        Assert.assertNotNull(categoryModel);

        OptionModel optionModel = categoryModel.getOptionModel(MyDelegateConfig.Option.DEMO_COLOR.getKey());
        Assert.assertNotNull(optionModel);

        Color newColor = new Color(0, 0, 0);
        categoryModel.setValue(optionModel, newColor);

        Assert.assertFalse(newColor.equals(config.getDemoColor()));

        configModel.saveModified();
        Assert.assertEquals(newColor, config.getDemoColor());
    }
}
