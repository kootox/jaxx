package org.nuiton.jaxx.widgets.config.model;

/*
 * #%L
 * JAXX :: Widgets Config
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Supplier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * A config to test config ui api when using a delegation
 * on {@link ApplicationConfig}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.11
 */
public class MyDelegateConfig extends AbstractBean implements Supplier<ApplicationConfig> {

    /** Logger */
    private static final Log log = LogFactory.getLog(MyDelegateConfig.class);

    public static final String PROPERTY_FULLSCREEN = "fullscreen";

    public static final String PROPERTY_LOCALE = "locale";

    public static final String PROPERTY_FONT_SIZE = "fontSize";

    public static final String PROPERTY_ADJUSTING = "adjusting";

    public static final String PROPERTY_DEMO_COLOR = "demoColor";

    public static final String PROPERTY_DEMO_CLASS = "demoClass";

    protected final ApplicationConfig applicationConfig;

    public MyDelegateConfig() {
        this.applicationConfig = new ApplicationConfig();

        applicationConfig.setConfigFileName(Option.CONFIG_FILE.defaultValue);

        // chargement de la configuration interne

        for (Option o : Option.values()) {
            applicationConfig.setDefaultOption(o.key, o.defaultValue);
        }

        applicationConfig.setAdjusting(true);
        try {
            addPropertyChangeListener(PROPERTY_FULLSCREEN, saveAction);
            addPropertyChangeListener(PROPERTY_FONT_SIZE, saveAction);
            addPropertyChangeListener(PROPERTY_LOCALE, saveAction);
            addPropertyChangeListener(PROPERTY_DEMO_COLOR, saveAction);
            addPropertyChangeListener(PROPERTY_DEMO_CLASS, saveAction);
        } finally {
            applicationConfig.setAdjusting(false);
        }
    }

    protected final PropertyChangeListener saveAction = evt -> {
        if (isAdjusting()) {
            if (log.isDebugEnabled()) {
                log.debug("skip save while adjusting");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Saving configuration at " + new Date());
            }
            saveForUser();
        }
    };

    public void saveForUser(String... excludeKeys) {
        // never save anything :)
    }

    public boolean isFullScreen() {
        Boolean result = applicationConfig.getOptionAsBoolean(Option.FULL_SCREEN.key);
        return result != null && result;
    }

    public Locale getLocale() {
        return applicationConfig.getOption(Locale.class, Option.LOCALE.key);
    }

    public Float getFontSize() {
        return applicationConfig.getOption(Float.class, Option.FONT_SIZE.key);
    }

    public Color getDemoColor() {
        return applicationConfig.getOptionAsColor(Option.DEMO_COLOR.key);
    }

    public Class<?> getDemoClass() {
        return applicationConfig.getOptionAsClass(Option.DEMO_CLASS.key);
    }

    public void setFullscreen(boolean fullscreen) {
        Object oldValue = null;
        applicationConfig.setOption(Option.FULL_SCREEN.key, fullscreen + "");
        firePropertyChange(PROPERTY_FULLSCREEN, oldValue, fullscreen);
    }

    public void setLocale(Locale newLocale) {
        applicationConfig.setOption(Option.LOCALE.key, newLocale.toString());
        firePropertyChange(PROPERTY_LOCALE, null, newLocale);
    }

    public void setFontSize(Float newFontSize) {
        Float oldValue = getFontSize();
        if (log.isDebugEnabled()) {
            log.debug("changing font-size to " + newFontSize);
        }
        applicationConfig.setOption(Option.FONT_SIZE.key, newFontSize.toString());
        firePropertyChange(PROPERTY_FONT_SIZE, oldValue, newFontSize);
    }

    public void setDemoColor(Color color) {
        Color oldValue = getDemoColor();
        if (log.isDebugEnabled()) {
            log.debug("changing demo-color to " + color);
        }
        applicationConfig.setOption(Option.DEMO_COLOR.key, color.toString());
        firePropertyChange(PROPERTY_DEMO_COLOR, oldValue, color);
    }

    public void setDemoClass(Class<?> newClass) {
        Class<?> oldValue = getDemoClass();
        if (log.isDebugEnabled()) {
            log.debug("changing demo-class to " + newClass);
        }
        applicationConfig.setOption(Option.DEMO_CLASS.key, newClass.getName());
        firePropertyChange(PROPERTY_DEMO_CLASS, oldValue, newClass);
    }

    public boolean isAdjusting() {
        return applicationConfig.isAdjusting();
    }

    public void setAdjusting(boolean adjusting) {
        applicationConfig.setAdjusting(adjusting);
    }

    @Override
    public ApplicationConfig get() {
        return applicationConfig;
    }

    //////////////////////////////////////////////////
    // Toutes les options disponibles
    //////////////////////////////////////////////////

    public enum Option implements ConfigOptionDef {

        CONFIG_FILE(ApplicationConfig.CONFIG_FILE_NAME, t("jaxxdemo.config.configFileName.description"), "jaxxdemo", String.class, true, true),
        FULL_SCREEN("ui.fullscreen", t("jaxxdemo.config.ui.fullscreen"), "false", Boolean.class, false, false),
        LOCALE("ui." + PROPERTY_LOCALE, t("jaxxdemo.config.ui." + PROPERTY_LOCALE), Locale.FRANCE.toString(), Locale.class, false, false),
        FONT_SIZE("ui." + PROPERTY_FONT_SIZE, t("jaxxdemo.config.ui." + PROPERTY_FONT_SIZE), "10f", Float.class, false, false),
        DEMO_COLOR("ui." + PROPERTY_DEMO_COLOR, t("jaxxdemo.config.ui.demoColor"), "#ffffff", Color.class, false, false),
        DEMO_CLASS("ui." + PROPERTY_DEMO_CLASS, t("jaxxdemo.config.ui.demoClass"), "java.io.File", Class.class, false, false);

        public final String key;

        public final String description;

        public String defaultValue;

        public final Class<?> type;

        public boolean _transient;

        public boolean _final;

        Option(String key,
               String description,
               String defaultValue,
               Class<?> type,
               boolean _transient,
               boolean _final) {
            this.key = key;
            this.description = description;
            this.defaultValue = defaultValue;
            this.type = type;
            this._final = _final;
            this._transient = _transient;
        }

        @Override
        public boolean isFinal() {
            return _final;
        }

        @Override
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public void setTransient(boolean _transient) {
            this._transient = _transient;
        }

        @Override
        public void setFinal(boolean _final) {
            this._final = _final;
        }

        @Override
        public boolean isTransient() {
            return _transient;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }
}

