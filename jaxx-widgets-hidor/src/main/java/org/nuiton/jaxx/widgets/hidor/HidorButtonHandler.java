/*
 * #%L
 * JAXX :: Widgets Hidor
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.hidor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.Icon;
import javax.swing.JComponent;

/**
 * Handler of ui {@link HidorButton}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class HidorButtonHandler implements UIHandler<HidorButton> {

    private static final Log log = LogFactory.getLog(HidorButtonHandler.class);

    protected HidorButton ui;

    public void setTarget(JComponent target) {
        JComponent oldValue = ui.target;
        ui.target = target;
        ui.firePropertyChange("target", oldValue, target);
    }

    public void setExpandIcon(Icon icon) {
        ui.putClientProperty("expandIcon", icon);
    }

    public void setHideIcon(Icon icon) {
        ui.putClientProperty("hideIcon", icon);
    }

    protected String updateToolTipText(boolean c) {
        return c ? ui.hideTip : ui.showTip;
    }

    protected String updateText(boolean c) {
        return c ? ui.hideText : ui.showText;
    }

    protected Icon updateIcon(boolean c) {
        return (Icon) ui.getClientProperty(c ? "hideIcon" : "showIcon");
    }

    @Override
    public void beforeInit(HidorButton ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(HidorButton ui) {

        ui.addPropertyChangeListener("targetVisible", evt -> {
            if (log.isDebugEnabled()) {
                log.debug("target visible changed <" + evt.getOldValue() + ":" + evt.getNewValue() + ">");
            }
            boolean newValue = (Boolean) evt.getNewValue();
            if (ui.target != null) {
                ui.target.setVisible(newValue);
            }
        });
    }
}
