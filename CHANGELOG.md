# jaxx changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2018-01-24 00:45.

## Version [3.0-alpha-26](https://gitlab.com/ultreiaio/jaxx/milestones/132)

**Closed at *In progress*.**


### Issues
  * [[Evolution 679]](https://gitlab.com/ultreiaio/jaxx/issues/679) **Be able to configure key stroke of an ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-25](https://gitlab.com/ultreiaio/jaxx/milestones/131)

**Closed at *In progress*.**


### Issues
  * [[Evolution 671]](https://gitlab.com/ultreiaio/jaxx/issues/671) **Improve FilterTreeModel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 672]](https://gitlab.com/ultreiaio/jaxx/issues/672) **Add in JTrees efficient enumeration to walk on a Tree** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 673]](https://gitlab.com/ultreiaio/jaxx/issues/673) **Move JTrees to tree package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 674]](https://gitlab.com/ultreiaio/jaxx/issues/674) **Introduce IterableTreeNode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-24](https://gitlab.com/ultreiaio/jaxx/milestones/130)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-23](https://gitlab.com/ultreiaio/jaxx/milestones/129)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-22](https://gitlab.com/ultreiaio/jaxx/milestones/128)

**Closed at *In progress*.**


### Issues
  * [[Evolution 670]](https://gitlab.com/ultreiaio/jaxx/issues/670) **Add a simple way to load ui resources in from a module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-21](https://gitlab.com/ultreiaio/jaxx/milestones/127)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-20](https://gitlab.com/ultreiaio/jaxx/milestones/126)

**Closed at *In progress*.**


### Issues
  * [[Evolution 669]](https://gitlab.com/ultreiaio/jaxx/issues/669) **Improve generation of handler is ui is abstract but not handler** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-19](https://gitlab.com/ultreiaio/jaxx/milestones/125)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 668]](https://gitlab.com/ultreiaio/jaxx/issues/668) **Fix ApplicationAction.isEnabled() method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-18](https://gitlab.com/ultreiaio/jaxx/milestones/124)

**Closed at *In progress*.**


### Issues
  * [[Evolution 659]](https://gitlab.com/ultreiaio/jaxx/issues/659) **Add yet another Action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 660]](https://gitlab.com/ultreiaio/jaxx/issues/660) **Add new text widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 662]](https://gitlab.com/ultreiaio/jaxx/issues/662) **Improve BeanComboBox widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 664]](https://gitlab.com/ultreiaio/jaxx/issues/664) **Add yet another init API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 665]](https://gitlab.com/ultreiaio/jaxx/issues/665) **Let&#39;s add possible data binding on javaBean attribute content** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 666]](https://gitlab.com/ultreiaio/jaxx/issues/666) **Be able to use ApplicationAction in JCheckBoxMenuItem also** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 667]](https://gitlab.com/ultreiaio/jaxx/issues/667) **Reuse component icon if exists in ApplicationAction** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 663]](https://gitlab.com/ultreiaio/jaxx/issues/663) **Temperature widget requires a Serializable bean, but should not** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 661]](https://gitlab.com/ultreiaio/jaxx/issues/661) **Remove module jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-17](https://gitlab.com/ultreiaio/jaxx/milestones/123)

**Closed at *In progress*.**


### Issues
  * [[Evolution 657]](https://gitlab.com/ultreiaio/jaxx/issues/657) **Use new i18n project APi to generate jaxx getter while generating jaxx files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 658]](https://gitlab.com/ultreiaio/jaxx/issues/658) **Detect any I18n api call inside JAXX files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-16](https://gitlab.com/ultreiaio/jaxx/milestones/122)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 655]](https://gitlab.com/ultreiaio/jaxx/issues/655) **Fix getHandler method generation for abstract ui** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-15](https://gitlab.com/ultreiaio/jaxx/milestones/121)

**Closed at *In progress*.**


### Issues
  * [[Evolution 653]](https://gitlab.com/ultreiaio/jaxx/issues/653) **Improve generation of getHandler method for abstract classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 654]](https://gitlab.com/ultreiaio/jaxx/issues/654) **FilterableDoubleList should not accept only serializable objects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-14](https://gitlab.com/ultreiaio/jaxx/milestones/120)

**Closed at *In progress*.**


### Issues
  * [[Evolution 651]](https://gitlab.com/ultreiaio/jaxx/issues/651) **Introduce jaxx-runtime-spi module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 652]](https://gitlab.com/ultreiaio/jaxx/issues/652) **Improve TabInfo component** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-13](https://gitlab.com/ultreiaio/jaxx/milestones/119)

**Closed at *In progress*.**


### Issues
  * [[Evolution 649]](https://gitlab.com/ultreiaio/jaxx/issues/649) **Be able to detect validator fields using inheritance of jaxx classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 650]](https://gitlab.com/ultreiaio/jaxx/issues/650) **Introduce UIInitializer API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-12](https://gitlab.com/ultreiaio/jaxx/milestones/118)

**Closed at *In progress*.**


### Issues
  * [[Evolution 647]](https://gitlab.com/ultreiaio/jaxx/issues/647) **Introduce jaxx-widgets-temperature module with a temperature editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 646]](https://gitlab.com/ultreiaio/jaxx/issues/646) **When using computeI18n, label should always be tried before toolTipText** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 648]](https://gitlab.com/ultreiaio/jaxx/issues/648) **In select widgets, can&#39;t change any longer the sort type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-11](https://gitlab.com/ultreiaio/jaxx/milestones/117)

**Closed at 2017-08-24.**


### Issues
  * [[Evolution 644]](https://gitlab.com/ultreiaio/jaxx/issues/644) **Generate i18n keys in mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 645]](https://gitlab.com/ultreiaio/jaxx/issues/645) **Add a way to compute i18n labels while using decorator in widgets (select)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-10](https://gitlab.com/ultreiaio/jaxx/milestones/116)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 643]](https://gitlab.com/ultreiaio/jaxx/issues/643) **Do NOT invoke twice the $initialize method when inheritates from a JaxxObject...** (Thanks to ) (Reported by Tony CHEMIT)

## Version [3.0-alpha-9](https://gitlab.com/ultreiaio/jaxx/milestones/115)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 641]](https://gitlab.com/ultreiaio/jaxx/issues/641) **Bad ui handler init in Config widgets** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 642]](https://gitlab.com/ultreiaio/jaxx/issues/642) **Update some libraries** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-8](https://gitlab.com/ultreiaio/jaxx/milestones/114)

**Closed at *In progress*.**


### Issues
  * [[Evolution 640]](https://gitlab.com/ultreiaio/jaxx/issues/640) **Remove usage of nuiton-utils** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 639]](https://gitlab.com/ultreiaio/jaxx/issues/639) **Fix a spanish translation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-7](https://gitlab.com/ultreiaio/jaxx/milestones/113)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 636]](https://gitlab.com/ultreiaio/jaxx/issues/636) **Config ui is not well initialized** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 637]](https://gitlab.com/ultreiaio/jaxx/issues/637) **Use i18n 4.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 638]](https://gitlab.com/ultreiaio/jaxx/issues/638) **Use ultreia.io stack** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-4](https://gitlab.com/ultreiaio/jaxx/milestones/109)
&#10;&#10;*(from redmine: created on 2017-01-18)*

**Closed at *In progress*.**


### Issues
  * [[Evolution 632]](https://gitlab.com/ultreiaio/jaxx/issues/632) **use pom 10.5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 631]](https://gitlab.com/ultreiaio/jaxx/issues/631) **Error in DMD gis editor, minutes are not ok** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-3](https://gitlab.com/ultreiaio/jaxx/milestones/107)
&#10;&#10;*(from redmine: created on 2017-01-01)*

**Closed at 2017-01-01.**


### Issues
  * [[Evolution 541]](https://gitlab.com/ultreiaio/jaxx/issues/541) **Extract a minimal runtime module used in generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 618]](https://gitlab.com/ultreiaio/jaxx/issues/618) **Introduce jaxx-runtime-swing-session module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 619]](https://gitlab.com/ultreiaio/jaxx/issues/619) **Move some api from jaxx-runtime to jaxx-widgets-extra** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 620]](https://gitlab.com/ultreiaio/jaxx/issues/620) **Introduce jaxx-runtime-swing-application module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 621]](https://gitlab.com/ultreiaio/jaxx/issues/621) **Introduce jaxx-runtime-swing-nav module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 622]](https://gitlab.com/ultreiaio/jaxx/issues/622) **Introduce jaxx-runtime-swing-wizard module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 623]](https://gitlab.com/ultreiaio/jaxx/issues/623) **Introduce jaxx-runtime-swing-help module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 624]](https://gitlab.com/ultreiaio/jaxx/issues/624) **Review widgets to use UIHandler and other Jaxx best pratices** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 625]](https://gitlab.com/ultreiaio/jaxx/issues/625) **Generate generic handler if necessary** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 617]](https://gitlab.com/ultreiaio/jaxx/issues/617) **Can&#39;t open links with file ou http protocol in method SwingUtil.openLink** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-2](https://gitlab.com/ultreiaio/jaxx/milestones/106)
&#10;&#10;*(from redmine: created on 2016-12-31)*

**Closed at 2016-12-31.**


### Issues
  * [[Evolution 82]](https://gitlab.com/ultreiaio/jaxx/issues/82) **Add a breadcrumb widget** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 608]](https://gitlab.com/ultreiaio/jaxx/issues/608) **Remove deprecated API and remove jaxx-widgets module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 609]](https://gitlab.com/ultreiaio/jaxx/issues/609) **Review default configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 610]](https://gitlab.com/ultreiaio/jaxx/issues/610) **Introduce jaxx-widgets-file module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 611]](https://gitlab.com/ultreiaio/jaxx/issues/611) **Introduce jaxx-widgets-status** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 612]](https://gitlab.com/ultreiaio/jaxx/issues/612) **Remove jaxx-application modules** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 613]](https://gitlab.com/ultreiaio/jaxx/issues/613) **Introduce jaxx-widgets-font module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 614]](https://gitlab.com/ultreiaio/jaxx/issues/614) **Introduce jaxx-widgets-i18n module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 615]](https://gitlab.com/ultreiaio/jaxx/issues/615) **Introduce jaxx-widgets-error module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 616]](https://gitlab.com/ultreiaio/jaxx/issues/616) **Introduce jaxx-widgets-hidor module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 239]](https://gitlab.com/ultreiaio/jaxx/issues/239) **Some tests are failing with jdk 7 VM Server** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 466]](https://gitlab.com/ultreiaio/jaxx/issues/466) **Review the demo application** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 502]](https://gitlab.com/ultreiaio/jaxx/issues/502) **Reimplements the action API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-1](https://gitlab.com/ultreiaio/jaxx/milestones/87)
&#10;&#10;*(from redmine: created on 2015-04-05)*

**Closed at 2016-12-30.**


### Issues
  * [[Evolution 545]](https://gitlab.com/ultreiaio/jaxx/issues/545) **Migrates package to *org.nuiton.jaxx*** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 606]](https://gitlab.com/ultreiaio/jaxx/issues/606) **Improve initialization of generated UI** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 133]](https://gitlab.com/ultreiaio/jaxx/issues/133) **Cannot generate class extend JaxxObject** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 566]](https://gitlab.com/ultreiaio/jaxx/issues/566) **Make webstart compatible with jdk 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 338]](https://gitlab.com/ultreiaio/jaxx/issues/338) **Update jdk level to 1.8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.43](https://gitlab.com/ultreiaio/jaxx/milestones/1)

**Closed at 2017-04-19.**


### Issues
  * [[Evolution 1]](https://gitlab.com/ultreiaio/jaxx/issues/1) **[TableFilter] Enable to specify a custom header renderer** (Thanks to Kevin Morin) (Reported by Kevin Morin)
  * [[Anomalie 2]](https://gitlab.com/ultreiaio/jaxx/issues/2) **[SwingSession] the window state is not restored if the window is always in full screen** (Thanks to Kevin Morin) (Reported by Kevin Morin)

