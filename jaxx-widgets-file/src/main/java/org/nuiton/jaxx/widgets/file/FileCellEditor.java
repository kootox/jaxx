/*
 * #%L
 * JAXX :: Widgets File
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.file;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.io.File;

/**
 * @author Sylvain Lletellier
 */
public class FileCellEditor extends AbstractCellEditor
        implements TableCellEditor {

    private static final long serialVersionUID = 4728254135566827971L;
    protected FileEditor fileEditor;

    public FileCellEditor() {
        setFileEditor(new FileEditor());
    }

    public FileCellEditor(FileEditor editor) {
        setFileEditor(editor);
    }

    public void setFileEditor(FileEditor fileEditor) {
        this.fileEditor = fileEditor;
        fileEditor.addActionListener(e -> fireEditingStopped());
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        File file = (File) value;
        fileEditor.setSelectedFile(file);
        return fileEditor;
    }

    @Override
    public Object getCellEditorValue() {
        return fileEditor.getSelectedFile();
    }
}
