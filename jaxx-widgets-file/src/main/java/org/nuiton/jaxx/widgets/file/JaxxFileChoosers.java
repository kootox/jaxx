package org.nuiton.jaxx.widgets.file;

/*-
 * #%L
 * JAXX :: Widgets File
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.Component;
import java.io.File;
import java.util.Arrays;
import javax.swing.JOptionPane;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.JOptionPanes;


import static org.nuiton.i18n.I18n.t;

/**
 * Created by tchemit on 11/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JaxxFileChoosers {

    /** Logger. */
    private static final Log log = LogFactory.getLog(JaxxFileChoosers.class);

    /**
     * Choisir un fichier via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre du dialogue de sélection
     * @param buttonLabel le label du boutton d'acceptation
     * @param incoming    le fichier de base à utilier
     * @param filters     les filtres + descriptions sur le sélecteur de
     *                    fichiers
     * @return le fichier choisi ou le fichier incoming si l'opération a été
     * annulée
     */
    public static File chooseFile(Component parent, String title, String buttonLabel, File incoming, String... filters) {

        JaxxFileChooser.ToLoadFile toLoadFile = JaxxFileChooser.forLoadingFile()
                .setParent(parent)
                .setTitle(title)
                .setApprovalText(buttonLabel)
                .setPatternOrDescriptionFilters(Arrays.asList(filters));

        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadFile.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadFile.choose();

        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        return file == null ? incoming : file;
    }

    /**
     * Choisir un répertoire via un sélecteur graphique de fichiers.
     *
     * @param parent      le component swing appelant le controle
     * @param title       le titre de la boite de dialogue de sléection
     * @param buttonLabel le label de l'action d'acceptation
     * @param incoming    le fichier de base à utiliser
     * @return le répertoire choisi ou le répertoire incoming si l'opération a
     * été annulée
     */
    public static File chooseDirectory(Component parent, String title, String buttonLabel, File incoming) {

        JaxxFileChooser.ToLoadDirectory toLoadDirectory = JaxxFileChooser.forLoadingDirectory()
                .setParent(parent)
                .setTitle(title)
                .setApprovalText(buttonLabel);
        File parentDirectoryIfExist = getParentDirectoryIfExist(incoming);
        if (parentDirectoryIfExist != null) {
            toLoadDirectory.setStartDirectory(parentDirectoryIfExist);
        }
        File file = toLoadDirectory.choose();
        if (log.isDebugEnabled()) {
            log.debug(title + " : " + file);
        }
        return file;
    }

    private static File getParentDirectoryIfExist(File incoming) {
        if (incoming != null) {
            File basedir;
            if (incoming.isFile()) {
                basedir = incoming.getParentFile();
            } else {
                basedir = incoming;
            }
            if (basedir.exists()) {
                return basedir;
            }
        }
        return null;
    }

    public static boolean confirmOverwriteFileIfExist(Component parent, File file) {
        boolean write = true;
        if (file.exists()) {
            write = JOptionPanes.askUser(
                    parent,
                    t("jaxx.file.overwrite.title"),
                    t("jaxx.file.overwrite"),
                    JOptionPane.OK_CANCEL_OPTION,
                    new Object[]{
                            t("jaxx.file.overwrite.ok"),
                            t("jaxx.file.overwrite.cancel")},
                    1) == 0;
        }

        return write;
    }
}
