/*
 * #%L
 * JAXX :: Widgets File
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.file;

import com.google.common.io.Files;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.io.File;

/** @author Sylvain Lletellier */
public class FileEditorHandler implements UIHandler<FileEditor> {

    public static final String SEPARATOR_REGEX = "\\s*,\\s*";

    public static File lastSelectedPath;

    protected FileEditor ui;

    /**
     * To set the dialog (see https://forge.nuiton.org/issues/2578).
     *
     * @since 2.5.12
     */
    protected JDialog dialogOwner;

    public void openLocation() {

        // use last selected file
        File startFile = ui.getSelectedFile();
        String startPath = ui.getStartPath();
        if (startFile == null && StringUtils.isNotEmpty(startPath)) {

            // else filed start path
            startFile = new File(startPath);
        } else if (startFile == null) {

            // else start with user home
            startFile = lastSelectedPath;
        }
        JFileChooser fc = new JFileChooser(startFile);

        fc.setDialogTitle(ui.getTitle());
        fc.setAcceptAllFileFilterUsed(ui.getAcceptAllFileFilterUsed());

        // TODO sletellier 14/06/2012 : activate multi selection
//        boolean multiSelectionEnabled = view.isMultiSelectionEnabled();
//        fc.setMultiSelectionEnabled(multiSelectionEnabled);

        // used to enable directory selection
        boolean directoryEnabled = ui.isDirectoryEnabled();
        if (directoryEnabled) {
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }

        // used to enable file selection
        boolean fileEnabled = ui.isFileEnabled();
        if (fileEnabled) {

            if (directoryEnabled) {

                // both
                fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            } else {
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            }
            String extsAsString = ui.getExts();
            if (extsAsString != null) {

                // extentions can be separted by comma
                String[] exts = extsAsString.split(SEPARATOR_REGEX);
                String extsDescription = ui.getExtsDescription();

                String[] descs = new String[0];
                if (extsDescription != null) {
                    descs = extsDescription.split(SEPARATOR_REGEX);
                }
                for (int i = 0; i < exts.length; i++) {

                    // use ext if no desc found
                    String ext = exts[i];
                    String desc = ext;
                    if (descs.length > i) {
                        desc = descs[i];
                    }

                    fc.addChoosableFileFilter(new ExtentionFileFiler(ext, desc));
                }
            }
        }

        // directory or/and file must be enabled
        if (!directoryEnabled && !fileEnabled) {
            throw new IllegalArgumentException("You must enable at least file or directory to open dialog");
        }

        // show dialog
        // see https://forge.nuiton.org/issues/2578
        boolean hackDialog = dialogOwner != null && dialogOwner.isAlwaysOnTop();
        if (hackDialog) {
            dialogOwner.setAlwaysOnTop(false);
        }
        int returnVal = fc.showOpenDialog(ui);
        if (hackDialog) {
            dialogOwner.setAlwaysOnTop(true);
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            // get selected to display in ui
            File file = fc.getSelectedFile();

            setSelectedFile(file);
        }
    }

    public void setSelectedFile(String path) {
        setSelectedFile(new File(path));
    }

    public void setSelectedFile(File file) {
        ui.setSelectedFile(file);
        ui.setStartPath(file.getPath());
        File dir = file;
        if (dir.exists()) {
            if (!dir.isDirectory()) {
                dir = dir.getParentFile();
            }
            lastSelectedPath = dir;
        }
    }

    protected void setDialogOwner(JDialog dialogOwner) {
        this.dialogOwner = dialogOwner;
    }

    @Override
    public void beforeInit(FileEditor ui) {
        this.ui = ui;
        if (lastSelectedPath == null) {
            lastSelectedPath = new File(System.getProperty("user.home"));
        }
    }

    @Override
    public void afterInit(FileEditor ui) {

    }

    public static class ExtentionFileFiler extends FileFilter {
        protected final String ext;
        protected final String desciption;

        public ExtentionFileFiler(String ext, String desciption) {
            this.ext = ext;
            this.desciption = desciption;
        }

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            }
            String fileExtension = Files.getFileExtension(file.getName());
            return ext.equals(fileExtension);
        }

        @Override
        public String getDescription() {
            return desciption;
        }
    }
}
