package org.nuiton.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Widgets Number
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberCellEditor<E extends Number> extends AbstractCellEditor
        implements TableCellEditor, FocusListener, AncestorListener {

    private static final long serialVersionUID = 1L;

    protected final NumberEditor numberEditor;

    /** constructor */
    public NumberCellEditor(Class<E> type, boolean useSign) {
        numberEditor = new NumberEditor();
        numberEditor.getTextField().setHorizontalAlignment(SwingConstants.RIGHT);
        numberEditor.getTextField().setBorder(null);
        numberEditor.getTextField().addFocusListener(this);
        numberEditor.getTextField().addAncestorListener(this);

        numberEditor.setNumberType(type);
        numberEditor.setUseSign(useSign);
        numberEditor.init();

    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {

        Number number = (Number) value;
        numberEditor.setNumberValue(number);
//        numberEditor.setModelText(JAXXUtil.getStringValue(number));
        return numberEditor;
    }

    public NumberEditor getNumberEditor() {
        return numberEditor;
    }

    @Override
    public E getCellEditorValue() {
        NumberEditorModel model = numberEditor.getModel();
        return (E) model.getNumberValue();
    }

    @Override
    public void focusGained(FocusEvent e) {
        SwingUtilities.invokeLater(() -> {
            numberEditor.getTextField().requestFocus();
            numberEditor.getTextField().selectAll();
        });
    }

    @Override
    public void focusLost(FocusEvent e) {
        // commenting the next line fixes https://forge.nuiton.org/issues/3517 and https://forge.nuiton.org/issues/3518
//        cancelCellEditing();
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        SwingUtilities.invokeLater(() -> {
            numberEditor.getTextField().requestFocus();
            numberEditor.getTextField().selectAll();
        });
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
    }

    @Override
    public boolean stopCellEditing() {
        boolean result = super.stopCellEditing();
        // Reset previous data to avoid keeping it on other cell edition
        if (result) {
            numberEditor.setNumberValue(null);
//            // Use empty string, otherwise there is a NPE in NumberEditorHandler
//            numberEditor.setModelText("");
            // force binding, I do not know why the textfield text is not emptied
            // if we do not force it
            numberEditor.applyDataBinding(NumberEditor.BINDING_TEXT_FIELD_TEXT);
        }
        return result;
    }

}
