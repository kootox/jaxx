package org.nuiton.jaxx.widgets.number;

/*-
 * #%L
 * JAXX :: Widgets Number
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.nuiton.jaxx.runtime.resources.UIResourcesProvider;

/**
 * Created by tchemit on 13/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(UIResourcesProvider.class)
public class JaxxWidgetsNumberUIResourcesProvider extends UIResourcesProvider {

    public JaxxWidgetsNumberUIResourcesProvider() {
        super("Jaxx Widgets Number module", "META-INF/ui/jaxx-widgets-number-ui.properties");
    }
}
