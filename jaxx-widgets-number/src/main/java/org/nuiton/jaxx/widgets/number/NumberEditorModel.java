package org.nuiton.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Widgets Number
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.widgets.ModelToBean;

/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberEditorModel extends AbstractSerializableBean implements ModelToBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BEAN = "bean";

    public static final String PROPERTY_TEXT_VALUE = "textValue";

    public static final String PROPERTY_NUMBER_VALUE = "numberValue";

    public static final String PROPERTY_NUMBER_PATTERN = "numberPattern";

    private final NumberEditorConfig config;

    /** Optional bean where to push data. */
    protected Object bean;

    /**
     * Current text representation of the number (this value is always displayed in editor).
     * <p>
     * Meanwhile the value can be different than the string represention of the numberValue, for example we can have as
     * textValue {@code 0.} which represents the number {@code 0}.
     */
    protected String textValue;

    /**
     * Current number value of the editor.
     */
    protected Number numberValue;

    /**
     * Optional pattern to validate input text.
     */
    protected String numberPattern;

    /**
     * To avoid reentrant code while adjusting text value.
     */
    protected boolean textValueIsAdjusting;

    /**
     * To avoid reentrant code while adjusting number value.
     */
    protected boolean numberValueIsAdjusting;

    public NumberEditorModel(NumberEditorConfig config) {
        this.config = config;
    }

    public NumberEditorConfig getConfig() {
        return config;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Object oldValue = getBean();
        this.bean = bean;
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {

        String oldValue = getNumberPattern();
        this.numberPattern = numberPattern;
        firePropertyChange(PROPERTY_NUMBER_PATTERN, oldValue, numberPattern);

    }

    public Number getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Number numberValue) {

        if (!numberValueIsAdjusting) {

            numberValueIsAdjusting = true;

            try {
                Number oldValue = getNumberValue();
                this.numberValue = numberValue;
                firePropertyChange(PROPERTY_NUMBER_VALUE, oldValue, numberValue);
            } finally {

                numberValueIsAdjusting = false;

            }

        }

    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {

        if (!textValueIsAdjusting) {

            textValueIsAdjusting = true;

            try {
                String oldValue = getTextValue();
                this.textValue = textValue;
                firePropertyChange(PROPERTY_TEXT_VALUE, oldValue, textValue);
                firePropertyChange("canUseDot", null, isCanUseDot());
                firePropertyChange("canUseSign", null, isCanUseSign());
                firePropertyChange("canUseZero", null, isCanUseZero());
                firePropertyChange("canClearAll", null, isCanClearAll());
            } finally {

                textValueIsAdjusting = false;

            }

        }

    }

    public boolean isCanUseDot() {
        Boolean useDecimal = config.getUseDecimal();
        return useDecimal != null && useDecimal && !textValue.contains(".");
    }

    public boolean isCanUseSign() {
        return config.isUseSign() && StringUtils.isNotBlank(textValue);
    }

    public boolean isCanUseZero() {
        return StringUtils.isNotBlank(textValue) && "0".equals(textValue);
    }

    public boolean isCanClearAll() {
        return StringUtils.isNotBlank(textValue);
    }

    public boolean isTextValueIsAdjusting() {
        return textValueIsAdjusting;
    }

    public boolean isNumberValueIsAdjusting() {
        return numberValueIsAdjusting;
    }

    protected Predicate<NumberEditorModel> canUpdateBeanNumberValuePredicate() {
        return input -> true;
    }

}
