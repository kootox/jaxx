package org.nuiton.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Widgets Number
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import io.ultreia.java4all.lang.Setters;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.MutateOnConditionalPropertyChangeListener;


/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberEditorHandler implements UIHandler<NumberEditor> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(NumberEditorHandler.class);

    protected final static ImmutableSet<Class<?>> INT_CLASSES = ImmutableSet.copyOf(Sets.<Class<?>>newHashSet(
            byte.class,
            Byte.class,
            short.class,
            Short.class,
            int.class,
            Integer.class,
            BigInteger.class
    ));

    private static final String VALIDATE_PROPERTY = "validate";

    protected NumberEditor ui;

    protected Pattern numberPattern;

    protected NumberParserFormatter numberParserFormatter;

    private static Map<Class<?>, NumberParserFormatter<?>> numberFactories;

    @Override
    public void beforeInit(NumberEditor ui) {
        this.ui = ui;

        NumberEditorModel model = new NumberEditorModel(new NumberEditorConfig());
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(NumberEditor ui) {
        // nothing to do here, everything is done in init method
    }

    /**
     * Ajoute le caractère donné à l'endroit où est le curseur dans la zone de
     * saisie et met à jour le modèle.
     *
     * @param c le caractère à ajouter.
     */
    public void addChar(char c) {
        try {

            ui.getTextField().getDocument().insertString(ui.getTextField().getCaretPosition(), c + "", null);

        } catch (BadLocationException e) {
            log.warn(e);
        }

        setTextValue(ui.getTextField().getText());

    }

    /**
     * Supprime le caractère juste avant le curseur du modèle (textuel) et
     * met à jour la zone de saisie.
     */
    public void removeChar() {

        JTextField textField = ui.getTextField();

        int position = textField.getCaretPosition();
        if (position < 1) {
            if (log.isDebugEnabled()) {
                log.debug("cannot remove when caret on first position or text empty");
            }
            // on est au debut du doc, donc rien a faire
            return;
        }
        try {
            textField.getDocument().remove(position - 1, 1);
        } catch (BadLocationException ex) {
            // ne devrait jamais arrive vu qu'on a fait le controle...
            log.debug(ex);
            return;
        }
        String newText = textField.getText();
        if (log.isDebugEnabled()) {
            log.debug("text updated : " + newText);
        }
        position--;
        textField.setCaretPosition(position);

        setTextValue(newText);

    }

    public void reset() {

//        ui.getModel().setNumberValue(null);

        setTextValue("");

    }

    /**
     * Permute le signe dans la zone de saisie et
     * dans le modèle.
     */
    public void toggleSign() {

        String newValue = ui.getModel().getTextValue();

        if (newValue.startsWith("-")) {
            newValue = newValue.substring(1);
        } else {
            newValue = "-" + newValue;
        }

        setTextValue(newValue);

    }

    public void setTextValue(String newText) {

        NumberEditorModel model = ui.getModel();

        boolean textValid;

        if (StringUtils.isEmpty(newText)) {

            // empty text is always valid
            textValid = true;

        } else {

            if (numberPattern != null) {

                // use given number pattern to check text
                Matcher matcher = numberPattern.matcher(newText);

                textValid = matcher.matches();

                if (!textValid && model.isCanUseSign() && "-".equals(newText)) {
                    // limit case when we have only "-"
                    textValid = true;
                }

            } else {

                // check text validity "by hand"
                //TODO
                textValid = true;

            }

        }

        if (textValid) {

            if (log.isInfoEnabled()) {
                log.info("Text [" + newText + "] is valid, will set it to model");
            }

            model.setTextValue(newText);

        } else {

            String oldText = model.getTextValue();

            if (oldText == null) {

                oldText = "";

            }

            if (log.isInfoEnabled()) {
                log.info("Text [" + newText + "] is not valid, will rollback to previous valid text: " + oldText);
            }

            int caretPosition = ui.getTextField().getCaretPosition() - 1;

            ui.getTextField().setText(oldText);
            if (caretPosition >= 0) {
                ui.getTextField().setCaretPosition(caretPosition);
            }

        }

    }

    /**
     * Affiche ou cache la popup.
     *
     * @param newValue la nouvelle valeur de visibilité de la popup.
     */
    public void setPopupVisible(Boolean newValue) {

        if (log.isTraceEnabled()) {
            log.trace(newValue);
        }

        if (newValue == null || !newValue) {
            ui.getPopup().setVisible(false);
            return;
        }
        SwingUtilities.invokeLater(() -> {
            JComponent invoker =
                    ui.isShowPopupButton() ?
                            ui.getShowPopUpButton() :
                            ui;
            Dimension dim = ui.getPopup().getPreferredSize();
            int x = (int) (invoker.getPreferredSize().getWidth() - dim.getWidth());
            ui.getPopup().show(invoker, x, invoker.getHeight());
            ui.getTextField().requestFocus();
        });
    }

    protected void init() {

        NumberEditorModel model = ui.getModel();

        NumberEditorConfig config = model.getConfig();

        Class<?> numberType = config.getNumberType();

        {
            // init numberType
            Preconditions.checkState(numberType != null, "Required a number type on " + ui);

            numberParserFormatter = getNumberFactory(numberType);

            if (log.isInfoEnabled()) {
                log.info("init numberType: " + numberType + " on " + ui);
            }

        }

        {

            // init canUseDecimal
            Boolean useDecimal = config.getUseDecimal();

            boolean canBeDecimal = !INT_CLASSES.contains(numberType);

            if (useDecimal == null) {

                // guess from type
                useDecimal = canBeDecimal;

                config.setUseDecimal(useDecimal);

            } else {

                // Check this is possible
                Preconditions.checkState(!useDecimal || canBeDecimal, "Can't use decimal with the following number type " + numberType + " on " + ui);

            }

        }

        {

            // init numberPattern
            String numberPatternDef = model.getNumberPattern();
            if (StringUtils.isNotEmpty(numberPatternDef)) {

                setNumberPattern(numberPatternDef);

            }
        }


        {

            // list when number pattern changed to recompute it
            model.addPropertyChangeListener(NumberEditorModel.PROPERTY_NUMBER_PATTERN, evt -> {
                String newPattern = (String) evt.getNewValue();

                setNumberPattern(newPattern);

                if (log.isInfoEnabled()) {
                    log.info("set new numberPattern" + newPattern);
                }
                if (StringUtils.isEmpty(newPattern)) {
                    numberPattern = null;
                } else {
                    numberPattern = Pattern.compile(newPattern);
                }
            });

            // listen when numberValue changed (should be from outside) to convert to textValue
            model.addPropertyChangeListener(NumberEditorModel.PROPERTY_NUMBER_VALUE, evt -> {

                Number newValue = (Number) evt.getNewValue();
                setTextValueFromNumberValue(newValue);


            });

            // listen when textValue changed to convert to number value
            model.addPropertyChangeListener(NumberEditorModel.PROPERTY_TEXT_VALUE, evt -> {

                String newValue = (String) evt.getNewValue();
                setNumberValueFromTextValue(newValue);

            });

        }
        Object bean = model.getBean();

        if (bean != null) {

            String property = config.getProperty();

            if (property != null) {

                Method mutator = Setters.getMutator(bean, property);

                // check mutator exists
                Objects.requireNonNull(mutator, "could not find mutator for " + property);

                // check type is ok
                Class<?>[] parameterTypes = mutator.getParameterTypes();
                Objects.requireNonNull(parameterTypes[0].equals(numberType), "Mismatch mutator type, required a " + numberType + " but was " + parameterTypes[0]);

                // When model number changed, let's push it back in bean
                model.addPropertyChangeListener(
                        NumberEditorModel.PROPERTY_NUMBER_VALUE,
                        new MutateOnConditionalPropertyChangeListener<>(model, mutator, model.canUpdateBeanNumberValuePredicate()));

            }
        }

        {
            // Add some listeners on ui

            ui.addPropertyChangeListener(NumberEditor.PROPERTY_SHOW_POPUP_BUTTON, evt -> {
                if (ui.getPopup().isVisible()) {
                    setPopupVisible(false);
                }
            });

            ui.addPropertyChangeListener(NumberEditor.PROPERTY_AUTO_POPUP, evt -> {
                if (ui.getPopup().isVisible()) {
                    setPopupVisible(false);
                }
            });

            ui.addPropertyChangeListener(NumberEditor.PROPERTY_POPUP_VISIBLE, evt -> setPopupVisible((Boolean) evt.getNewValue()));
            ui.getTextField().addMouseListener(new PopupListener());

        }

        // apply incoming number value
        Number numberValue = model.getNumberValue();
        setTextValueFromNumberValue(numberValue);

    }

    protected void setNumberPattern(String newPattern) {

        try {
            this.numberPattern = Pattern.compile(newPattern);
        } catch (Exception e) {
            throw new IllegalStateException("Could not compute numberPattern " + newPattern + " on " + ui, e);
        }

        if (log.isInfoEnabled()) {
            log.info("init numberPattern: " + numberPattern + " on " + ui);
        }

    }

    protected void setNumberValueFromTextValue(String textValue) {

        if (ui.getModel().isNumberValueIsAdjusting()) {
            // do nothing if number value is already adjusting
            return;
        }

        Number numberValue;

        if (StringUtils.isBlank(textValue)) {

            numberValue = null;

        } else {

            numberValue = numberParserFormatter.parse(textValue);

        }

        if (log.isInfoEnabled()) {
            log.info("Set numberValue " + numberValue + " from textValue " + textValue);
        }
        ui.getModel().setNumberValue(numberValue);
    }

    protected void setTextValueFromNumberValue(Number numberValue) {

        if (ui.getModel().isTextValueIsAdjusting()) {
            // do nothing if text value is already adjusting
            return;
        }

        String textValue = numberParserFormatter.format(numberValue);

        if (log.isInfoEnabled()) {
            log.info("Set textValue " + textValue + " from numberValue " + numberValue);
        }
        ui.getModel().setTextValue(textValue);

    }

    protected void validate() {

        setPopupVisible(false);
        // fire validate property (to be able to notify listeners)
        ui.firePropertyChange(VALIDATE_PROPERTY, null, true);
    }

    protected class PopupListener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        protected void maybeShowPopup(MouseEvent e) {
            if (!e.isPopupTrigger()) {
                return;
            }
            if (ui.isAutoPopup()) {
                if (ui.isPopupVisible()) {
                    if (!ui.getPopup().isVisible()) {
                        setPopupVisible(true);
                    }
                    // popup already visible

                } else {
                    // set popup auto
                    ui.setPopupVisible(true);

                }
            } else {
                if (ui.isPopupVisible()) {
                    setPopupVisible(true);
                }
            }
        }
    }

    interface NumberParserFormatter<N extends Number> {

        N parse(String textValue);

        String format(N numberValue);

    }


    protected static NumberParserFormatter<?> getNumberFactory(final Class<?> numberType) {

        if (numberFactories == null) {

            numberFactories = new HashMap<>();

            NumberParserFormatter<Byte> byteSupport = new NumberParserFormatter<Byte>() {
                @Override
                public String format(Byte numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public Byte parse(String textValue) {
                    Byte v;
                    if (NULL_LIMIT_INTS.contains(textValue)) {
                        v = null;
                    } else {
                        v = Byte.parseByte(textValue);
                    }
                    return v;
                }
            };
            numberFactories.put(byte.class, byteSupport);
            numberFactories.put(Byte.class, byteSupport);

            NumberParserFormatter<Short> shortSupport = new NumberParserFormatter<Short>() {
                @Override
                public String format(Short numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public Short parse(String textValue) {
                    Short v;
                    if (NULL_LIMIT_INTS.contains(textValue)) {
                        v = null;
                    } else {
                        v = Short.parseShort(textValue);
                    }
                    return v;
                }
            };
            numberFactories.put(short.class, shortSupport);
            numberFactories.put(Short.class, shortSupport);

            NumberParserFormatter<Integer> integerSupport = new NumberParserFormatter<Integer>() {
                @Override
                public String format(Integer numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public Integer parse(String textValue) {
                    Integer v;
                    if (NULL_LIMIT_INTS.contains(textValue)) {
                        v = null;
                    } else {
                        v = Integer.parseInt(textValue);
                    }
                    return v;
                }
            };
            numberFactories.put(int.class, integerSupport);
            numberFactories.put(Integer.class, integerSupport);

            NumberParserFormatter<Long> longSupport = new NumberParserFormatter<Long>() {
                @Override
                public String format(Long numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public Long parse(String textValue) {
                    Long v;
                    if (NULL_LIMIT_INTS.contains(textValue)) {
                        v = null;
                    } else {
                        v = Long.parseLong(textValue);
                    }
                    return v;
                }
            };
            numberFactories.put(long.class, longSupport);
            numberFactories.put(Long.class, longSupport);

            NumberParserFormatter<Float> floatSupport = new NumberParserFormatter<Float>() {

                final DecimalFormat df = new DecimalFormat("#.##########");

                @Override
                public String format(Float numberValue) {
                    String format;
                    if (numberValue == null) {
                        format = "";
                    } else {
                        format = String.valueOf(numberValue);
                        if (format.contains("E")) {

                            format = df.format(numberValue);
                            if (format.contains(",")) {
                                format = format.replace(",", ".");
                            }

                        }
                    }
                    return format;
                }

                @Override
                public Float parse(String textValue) {
                    Float v;
                    if (NULL_LIMIT_DECIMALS.contains(textValue)) {
                        v = null;
                    } else {
                        boolean addSign = false;
                        if (textValue.startsWith("-")) {
                            addSign = true;
                            textValue = textValue.substring(1);
                            if (textValue.startsWith(".")) {
                                textValue = "0" + textValue;
                            }
                        }
                        v = Float.parseFloat(textValue);
                        if (addSign) {
                            v = -v;
                        }
                    }
                    return v;
                }
            };
            numberFactories.put(float.class, floatSupport);
            numberFactories.put(Float.class, floatSupport);

            NumberParserFormatter<Double> doubleSupport = new NumberParserFormatter<Double>() {

                final DecimalFormat df = new DecimalFormat("#.##########");

                @Override
                public String format(Double numberValue) {
                    String format;
                    if (numberValue == null) {
                        format = "";
                    } else {
                        format = String.valueOf(numberValue);
                        if (format.contains("E")) {

                            format = df.format(numberValue);
                            if (format.contains(",")) {
                                format = format.replace(",", ".");
                            }

                        }
                    }
                    return format;
                }

                @Override
                public Double parse(String textValue) {
                    Double v;
                    if (NULL_LIMIT_DECIMALS.contains(textValue)) {
                        v = null;
                    } else {

                        boolean addSign = false;
                        if (textValue.startsWith("-")) {
                            addSign = true;
                            textValue = textValue.substring(1);
                            if (textValue.startsWith(".")) {
                                textValue = "0" + textValue;
                            }
                        }
                        v = Double.parseDouble(textValue);
                        if (addSign) {
                            v = -v;
                        }

                    }
                    return v;
                }
            };
            numberFactories.put(double.class, doubleSupport);
            numberFactories.put(Double.class, doubleSupport);
            NumberParserFormatter<BigInteger> bigIntegerSupport = new NumberParserFormatter<BigInteger>() {
                @Override
                public String format(BigInteger numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public BigInteger parse(String textValue) {
                    return new BigInteger(textValue);
                }
            };
            numberFactories.put(BigInteger.class, bigIntegerSupport);
            NumberParserFormatter<BigDecimal> bigDecimalSupport = new NumberParserFormatter<BigDecimal>() {
                @Override
                public String format(BigDecimal numberValue) {
                    return numberValue == null ? "" : String.valueOf(numberValue);
                }

                @Override
                public BigDecimal parse(String textValue) {
                    return new BigDecimal(textValue);
                }
            };
            numberFactories.put(BigDecimal.class, bigDecimalSupport);

        }

        for (Map.Entry<Class<?>, NumberParserFormatter<?>> entry : numberFactories.entrySet()) {

            if (entry.getKey().equals(numberType)) {

                return entry.getValue();

            }

        }

        throw new IllegalArgumentException("Could not find a NumberFactory for type " + numberType);

    }

    protected static final ImmutableSet<String> NULL_LIMIT_DECIMALS = ImmutableSet.copyOf(new String[]{"-", ".", "-."});

    protected static final ImmutableSet<String> NULL_LIMIT_INTS = ImmutableSet.copyOf(new String[]{"-"});

}
