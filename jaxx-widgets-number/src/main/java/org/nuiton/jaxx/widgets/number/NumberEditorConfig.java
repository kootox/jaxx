package org.nuiton.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Widgets Number
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Put here all immutable options used to init the number editor.
 *
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberEditorConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Optional property where to bind the number value in optional bean.
     */
    protected String property;

    /**
     * Should you allowed signed number ?
     */
    protected boolean useSign = true;

    /**
     * Should you allowed decimal number ?
     */
    protected Boolean useDecimal;

    /**
     * Type of number.
     */
    protected Class<?> numberType;

    /**
     * When a error occurs, previous valid value is repush in textField,
     * with this flag setted to true then also reselect this content.
     */
    protected boolean selectAllTextOnError;

    public Class<?> getNumberType() {
        return numberType;
    }

    public void setNumberType(Class<?> numberType) {
        this.numberType = numberType;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public boolean isSelectAllTextOnError() {
        return selectAllTextOnError;
    }

    public void setSelectAllTextOnError(boolean selectAllTextOnError) {
        this.selectAllTextOnError = selectAllTextOnError;
    }

    public boolean isUseSign() {
        return useSign;
    }

    public void setUseSign(boolean useSign) {
        this.useSign = useSign;
    }

    public Boolean getUseDecimal() {
        return useDecimal;
    }

    public void setUseDecimal(Boolean useDecimal) {
        this.useDecimal = useDecimal;
    }
}
