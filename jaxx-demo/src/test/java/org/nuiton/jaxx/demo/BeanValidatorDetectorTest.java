/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jaxx.demo.entities.Identity;
import org.nuiton.jaxx.demo.entities.Model;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.Iterator;
import java.util.SortedSet;

/** @author Tony Chemit - dev@tchemit.fr */
public class BeanValidatorDetectorTest extends AbstractValidatorDetectorTest {

    public BeanValidatorDetectorTest() {
        // prefer do tests on target copy instead of original files
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    @Test
    public void detectAllValidators() {
        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;

        // test with all context and all scopes

        result = detectValidators(Identity.class, Model.class, People.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());


        iterator = result.iterator();
        validator = iterator.next();

        assertValidatorModel(validator, null, Identity.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING, NuitonValidatorScope.INFO);
//        ValidatorTestHelper.assertValidatorEffectiveFields(validator, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME, Person.PROPERTY_FIRSTNAME);
//        ValidatorTestHelper.assertValidatorEffectiveFields(validator, NuitonValidatorScope.WARNING, Person.PROPERTY_PET);

        validator = iterator.next();
        assertValidatorModel(validator, null, Model.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING, NuitonValidatorScope.INFO);
//        ValidatorTestHelper.assertValidatorEffectiveFields(validator, NuitonValidatorScope.ERROR, Pet.PROPERTY_NAME);

        validator = iterator.next();
        assertValidatorModel(validator, null, People.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING, NuitonValidatorScope.INFO);

    }

    @Override
    protected File getRootDirectory(File basedir) {
        return new File(basedir, "target" + File.separator + "classes");
    }
}
