/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.treetable;

import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.feature.nav.NavDemoHandler;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;
import org.nuiton.jaxx.runtime.swing.nav.NavHelper;
import org.nuiton.jaxx.runtime.swing.nav.treetable.NavTreeTableNodeChildLoador;

import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class MoviesTreeTableNodeLoador extends NavTreeTableNodeChildLoador<Movie, Movie, NavDemoTreeTableNode> {

    private static final long serialVersionUID = 1L;

    protected final boolean isTreeTable;

    public MoviesTreeTableNodeLoador() {
        this(false);
    }

    public MoviesTreeTableNodeLoador(boolean isTreeTable) {
        super(Movie.class);
        this.isTreeTable = isTreeTable;
    }

    @Override
    public List<Movie> getData(Class<?> parentClass,
                               String parentId,
                               NavDataProvider dataProvider) {

        DemoDataProvider provider = (DemoDataProvider) dataProvider;

        // Return all movies
        return provider.getMovies();
    }

    @Override
    public NavDemoTreeTableNode createNode(Movie data,
                                           NavDataProvider dataProvider) {

        NavDemoTreeTableNode moviesNode;
        NavDemoTreeTableNode actorsCategoryNode;

        // Create movies static nodes
        moviesNode = new NavDemoTreeTableNode(
                getBeanType(),
                data.getId(),
                null,
                null
        );

        // Create clients category node
        actorsCategoryNode = new NavDemoTreeTableNode(
                String.class,
                n(NavDemoHandler.ACTORS_CATEGORY_NODE),
                null,
                NavHelper.getChildLoador(ActorsTreeTableNodeLoador.class)
        );

        // Add actors nodes to movies node
        moviesNode.add(actorsCategoryNode);

        return moviesNode;
    }
}
