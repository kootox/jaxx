package org.nuiton.jaxx.demo.feature.validation.list;
/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.jaxx.demo.entities.Identity;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

/**
 * Table model of {@link Identity}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class PeopleTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    public static final List<String> columnNames =
            Arrays.asList(People.PROPERTY_ID,
                          People.PROPERTY_FIRST_NAME,
                          People.PROPERTY_LAST_NAME,
                          People.PROPERTY_AGE);

    public static final Class<?>[] columnClasses =
            {String.class, String.class, Integer.class};

    private final List<People> data = Lists.newArrayList();

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public String getColumnName(int column) {
        SwingUtil.ensureColumnIndex(this, column);
        return columnNames.get(column);
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        SwingUtil.ensureColumnIndex(this, columnIndex);
        SwingUtil.ensureRowIndex(this, rowIndex);

        People row = data.get(rowIndex);
        if (columnIndex == 0) {
            return row.getId();
        }
        if (columnIndex == 1) {
            return row.getFirstName();
        }
        if (columnIndex == 2) {
            return row.getLastName();
        }
        if (columnIndex == 3) {
            return row.getAge();
        }

        // should never come here
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        SwingUtil.ensureColumnIndex(this, columnIndex);
        SwingUtil.ensureRowIndex(this, rowIndex);

        People row = data.get(rowIndex);

        if (columnIndex == 0) {
            row.setId(String.valueOf(aValue));
        } else if (columnIndex == 1) {
            row.setFirstName(String.valueOf(aValue));
        } else if (columnIndex == 2) {
            row.setLastName(String.valueOf(aValue));
        } else if (columnIndex == 3) {
            row.setAge(Integer.valueOf(aValue.toString()));
        }
    }


    public int getBeanIndex(People bean) {
        return data.indexOf(bean);
    }

    public People getBean(int row) {
        SwingUtil.ensureRowIndex(this, row);
        return data.get(row);
    }

    public Pair<Integer, Integer> getCell(People bean, String fieldName) {

        int row = getBeanIndex(bean);
        int col = columnNames.indexOf(fieldName);

        return Pair.of(row, col);
    }

    public void removeBean(int selectedRow) {
        SwingUtil.ensureRowIndex(this, selectedRow);
        data.remove(selectedRow);
        fireTableRowsDeleted(selectedRow, selectedRow);
    }

    public void addBean(People bean) {
        data.add(bean);
        int newrowIndex = data.size() - 1;
        fireTableRowsInserted(newrowIndex, newrowIndex);
    }
}
