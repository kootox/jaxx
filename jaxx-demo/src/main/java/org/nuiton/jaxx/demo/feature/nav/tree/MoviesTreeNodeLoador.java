/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.tree;

import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.feature.nav.NavDemoHandler;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;
import org.nuiton.jaxx.runtime.swing.nav.NavHelper;
import org.nuiton.jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;

import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class MoviesTreeNodeLoador extends NavTreeNodeChildLoador<Movie, Movie, NavDemoTreeNode> {

    private static final long serialVersionUID = 1L;

    protected final boolean isTreeTable;

    public MoviesTreeNodeLoador() {
        this(false);
    }

    public MoviesTreeNodeLoador(boolean isTreeTable) {
        super(Movie.class);
        this.isTreeTable = isTreeTable;
    }

    @Override
    public List<Movie> getData(Class<?> parentClass,
                               String parentId,
                               NavDataProvider dataProvider) {

        DemoDataProvider provider = (DemoDataProvider) dataProvider;

        // Return all movies
        return provider.getMovies();
    }

    @Override
    public NavDemoTreeNode createNode(Movie data, NavDataProvider dataProvider) {

        NavDemoTreeNode moviesNode;
        NavDemoTreeNode actorsCategoryNode;

        // Create movies static nodes
        moviesNode = new NavDemoTreeNode(
                getBeanType(),
                data.getId(),
                null,
                null
        );

        // Create clients category node
        actorsCategoryNode = new NavDemoTreeNode(
                String.class,
                n(NavDemoHandler.ACTORS_CATEGORY_NODE),
                null,
                NavHelper.getChildLoador(ActorsTreeNodeLoador.class)
        );

        // Add actors nodes to movies node
        moviesNode.add(actorsCategoryNode);

        return moviesNode;
    }
}
