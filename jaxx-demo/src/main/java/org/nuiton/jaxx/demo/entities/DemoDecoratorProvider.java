/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.entities;

import org.nuiton.decorator.DecoratorProvider;

/**
 * Demo decorator provider.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see DecoratorProvider
 * @since 2.2
 */
public class DemoDecoratorProvider extends DecoratorProvider {
    @Override
    protected void loadDecorators() {

        // load movie decorator
        registerMultiJXPathDecorator(Movie.class,
                                     "${title}$s#${year}$s", "#", " - ");

        // load people decorator
        registerMultiJXPathDecorator(People.class,
                                     "${firstName}$s#${lastName}$s#${age}$s",
                                     "#",
                                     " - "
        );
    }
}
