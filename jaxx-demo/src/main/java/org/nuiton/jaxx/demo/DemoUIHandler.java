package org.nuiton.jaxx.demo;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.demo.tree.DemoNode;
import org.nuiton.jaxx.demo.tree.DemoTreeHelper;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorProviderListCellRenderer;
import org.nuiton.jaxx.widgets.about.AboutUI;
import org.nuiton.jaxx.widgets.about.AboutUIBuilder;
import org.nuiton.jaxx.widgets.config.ConfigUIHelper;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.Arrays;
import java.util.Locale;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le handler de l'ui principale.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see DemoUI
 */
public class DemoUIHandler implements UIHandler<DemoUI> {

    /** Logger */
    static private final Log log = LogFactory.getLog(DemoUIHandler.class);

    static final JAXXContextEntryDef<DemoUI> MAIN_UI_ENTRY_DEF =
            JAXXUtil.newContextEntryDef("mainui", DemoUI.class);

    public static final String OPEN_CONFIG_ACTION = "openConfig";

    public DemoUIHandler() {
        reloadUICallback = () -> {
            if (log.isInfoEnabled()) {
                log.info("will reload ui");
            }
            DefaultApplicationContext context = DemoApplicationContext.get();
            DemoUI ui = getUI(context);
            DemoConfig config = ui.getConfig();
            reloadUI(context, config);
        };
        reloadApplicationCallback = () -> {
            if (log.isInfoEnabled()) {
                log.info("will reload appplication");
            }
            close(DemoApplicationContext.get());
        };
    }

    /**
     * Methode pour initialiser l'ui principale sans l'afficher.
     *
     * @param rootContext le context applicatif
     * @param config      la configuration a utiliser
     * @return l'ui instancie et initialisee mais non visible encore
     */
    public DemoUI initUI(DefaultApplicationContext rootContext, DemoConfig config) {

        if (log.isDebugEnabled()) {
            log.debug("fullscreen ? " + config.isFullScreen());
        }
        DecoratorProvider decoratorProvider =
                rootContext.getContextValue(DecoratorProvider.class);

        if (!config.getLocale().equals(I18n.getDefaultLocale())) {
            if (log.isInfoEnabled()) {
                log.info("re-init I18n with locale " + config.getLocale());
            }
            // change i18n language
            I18n.setDefaultLocale(config.getLocale());
            // reload decorators
            decoratorProvider.reload();
        }

        // create restrict context for ui 
        JAXXInitialContext context = new JAXXInitialContext();

        // share handler
        context.add(this);

        // share tree helper
        context.add(rootContext.getContextValue(DemoTreeHelper.class));

        // share config
        context.add(rootContext.getContextValue(DemoConfig.class));

        // share a unique DecoratorProvider
        context.add(decoratorProvider);

        // share a unique DecoratorProviderListCellRenderer
        context.add(new DecoratorProviderListCellRenderer(decoratorProvider));

        // instanciate ui
        DemoUI ui = new DemoUI(context);

        // add key strokes
        addKeyStrokes(ui, config);

        // keep it in root context
        MAIN_UI_ENTRY_DEF.setContextValue(rootContext, ui);

        // synch to error dialog
        ErrorDialogUI.init(ui);

        // set fullscreen propery on main ui
        ui.getGraphicsConfiguration().getDevice().setFullScreenWindow(config.isFullScreen() ? ui : null);

        return ui;
    }

    protected void addKeyStrokes(final DemoUI ui, DemoConfig config) {

        // Use WHEN_IN_FOCUSED_WINDOW to don't have focus check for binding keys
        JPanel mainPane = ui.getMainPane();
        mainPane.getActionMap().put(OPEN_CONFIG_ACTION, new AbstractAction() {

            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                showConfig(ui);
            }
        });
        final InputMap inputMap = mainPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        config.addPropertyChangeListener(DemoConfig.PROPERTY_KEY_OPEN_CONFIG, evt -> {
            KeyStroke oldValue = (KeyStroke) evt.getOldValue();
            inputMap.remove(oldValue);
            setShowConfigInputMap(inputMap, (KeyStroke) evt.getNewValue());
        });
        setShowConfigInputMap(inputMap, config.getKeyOpenConfig());
    }

    protected void setShowConfigInputMap(InputMap inputMap, KeyStroke keyStroke) {
        inputMap.put(keyStroke, OPEN_CONFIG_ACTION);
    }

    @Override
    public void beforeInit(DemoUI ui) {

    }

    @Override
    public void afterInit(final DemoUI ui) {

        // Creation of selection listener to open ui when tree selection change
        TreeSelectionListener listener = event -> {
            TreePath path = event.getPath();
            DemoNode node = (DemoNode) path.getLastPathComponent();

            if (log.isDebugEnabled()) {
                log.debug("Select node " + node);
            }

            if (node == null || node.isStringNode()) {

                // noeud de présentation, rien a faire
                return;
            }
            showUI(ui, node.getInternalClass());
        };

        JTree tree = ui.getNavigation();

        ui.getTreeHelper().setUI(tree, true, listener);

        // auto-expand node when selected
        SwingUtil.addExpandOnClickListener(tree);

    }

    protected void showUI(DemoUI ui, Class<?> type) {

        JPanel content = ui.getContent();

        String constraints = type.getName();

        log.info("Show for " + constraints);

        // Verify if instance is existing
        DemoTab demoContent = getContentIfExist(ui, constraints);

        if (demoContent == null) {

            log.info("Will instanciate a new " + constraints);
            try {
                // Get constructor
                Constructor<?> constructor =
                        type.getConstructor(JAXXContext.class);

                JAXXInitialContext tx =
                        new JAXXInitialContext().add(ui.getDelegateContext());

                DemoPanel panel = (DemoPanel) constructor.newInstance(tx);

                log.info("Demo panel to use : " + panel);

                demoContent = new DemoTab(tx.add("content", panel));

            } catch (Exception eee) {
                log.error(eee, eee);
                ErrorDialogUI.showError(eee);
            }

            // Add to content panel
            content.add(demoContent, constraints);
        }

        // show ui
        ui.getContentLayout().show(content, constraints);
    }

// Get content if exist in content, else return null

    protected <E extends Component> E getContentIfExist(DemoUI ui, String constraints) {
        if (log.isDebugEnabled()) {
            log.debug("Get content if exist " + constraints);
        }
        if (!ui.getContentLayout().contains(constraints)) {
            return null;
        }
        return (E) ui.getContentLayout().getComponent(ui.getContent(), constraints);
    }

    public void displayUI(final DemoUI ui, final String[] nodePath) {

        // expend all nodes of the demo tree
        SwingUtil.expandTree(ui.getNavigation());

        SwingUtilities.invokeLater(() -> {

            String[] path;
            if (nodePath == null) {
                // take first node
                path = new String[]{"jaxxdemo.tree"};
            } else {
                // take selected node
                path = nodePath;

            }
            log.info("node to re select " + Arrays.toString(path));
            // select node
            ui.getTreeHelper().selectNode(path);

            // use best dimensions
            ui.getSplitPane().resetToPreferredSizes();
        });

        // show ui after all (in another invocation, tu avoid layout adjustement
        // to be seen).

        SwingUtilities.invokeLater(() -> {

            // show ui
            ui.setVisible(true);
        });
    }

    /**
     * Permet de recharger l'ui principale et de changer de le mode d'affichage.
     *
     * @param rootContext le contexte applicatif
     * @param config      la configuration a utiliser
     */
    public void reloadUI(DefaultApplicationContext rootContext, DemoConfig config) {

        // scan main ui
        DemoUI ui = getUI(rootContext);

        String[] node = null;
        if (ui != null) {

            ui.getConfig().removeJaxxPropertyChangeListener();

            node = ui.getTreeHelper().getSelectedIds();

            if (node != null) {
                if (log.isDebugEnabled()) {
                    log.debug("selected node " + Arrays.toString(node));
                }
            }

            ErrorDialogUI.init(null);

            ui.dispose();

            ui.setVisible(false);

            MAIN_UI_ENTRY_DEF.removeContextValue(rootContext);
        }

        ui = initUI(rootContext, config);

        displayUI(ui, node);
    }

    /**
     * Méthode pour changer de mode d'affichage.
     *
     * Si <code>fullscreen</code> est à <code>true</code> alors on passe en
     * mode console (c'est à dire en mode plein écran exclusif), sinon on
     * passe en mode fenetré normal.
     *
     * @param context    l'ui principale de l'application
     * @param fullscreen le nouvel état requis.
     */
    public void changeScreen(JAXXContext context, boolean fullscreen) {

        DemoUI ui = getUI(context);

        // sauvegarde de l'état dans la configuration
        DemoConfig config = ui.getConfig();
        config.setFullscreen(fullscreen);

        // rechargement de l'ui
        reloadUI(DemoApplicationContext.get(), config);
    }

    public void changeLanguage(JAXXContext context, Locale newLocale) {

        DemoUI ui = getUI(context);

        DemoConfig config = ui.getConfig();

        // sauvegarde de la nouvelle locale
        config.setLocale(newLocale);

        // rechargement de l'ui
        reloadUI(DemoApplicationContext.get(), config);
    }

    /**
     * Ferme l'application.
     *
     * @param context l'ui principale de l'application
     */
    public void close(JAXXContext context) {
        log.info("JAXX Demo quitting...");
        try {

            DemoUI ui = getUI(context);
            ui.dispose();
        } finally {
            System.exit(0);
        }
    }

    final Runnable reloadUICallback;

    final Runnable reloadApplicationCallback;

    public void showConfig(JAXXContext context) {
        DemoUI ui = getUI(context);
        DemoConfig config = ui.getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config);

        helper.registerCallBack("ui",
                                n("demo.action.reload.ui"),
                                SwingUtil.createActionIcon("reload-ui"),
                                reloadUICallback);

        helper.registerCallBack("application",
                                n("demo.action.reload.application"),
                                SwingUtil.createActionIcon("reload-application"),
                                reloadApplicationCallback);

        // categorie repertoires

        helper.addCategory(n("jaxxdemo.config.category.directories"),
                           n("jaxxdemo.config.category.directories.description"));

        helper.addOption(DemoConfig.Option.CONFIG_FILE);

        // others
        helper.addCategory(n("jaxxdemo.config.category.other"),
                           n("jaxxdemo.config.category.other.description"));

        helper.addOption(DemoConfig.Option.FULL_SCREEN).
                setOptionPropertyName(DemoConfig.PROPERTY_FULLSCREEN).
                      setOptionCallBack("ui");

        helper.addOption(DemoConfig.Option.FONT_SIZE).
                setOptionPropertyName(DemoConfig.PROPERTY_FONT_SIZE).
                      setOptionCallBack("ui");

        helper.addOption(DemoConfig.Option.LOCALE).
                setOptionPropertyName(DemoConfig.PROPERTY_LOCALE).
                      setOptionCallBack("ui");

        helper.addOption(DemoConfig.Option.DEMO_COLOR).
                setOptionPropertyName(DemoConfig.PROPERTY_DEMO_COLOR).
                      setOptionCallBack("ui");

        helper.addOption(DemoConfig.Option.DEMO_CLASS).
                setOptionPropertyName(DemoConfig.PROPERTY_DEMO_CLASS).
                      setOptionCallBack("ui");

        helper.addOption(DemoConfig.Option.KEY_OPEN_CONFIG).
                setOptionPropertyName(DemoConfig.PROPERTY_KEY_OPEN_CONFIG).
                      setOptionCallBack("ui");

        helper.buildUI(context, "jaxxdemo.config.category.other");

        helper.displayUI(ui, false);
    }

    public void showHelp(DemoUI ui) {

        DemoApplicationContext.get().showHelp(ui, ui.getBroker(), null);
    }

    public void showHelp(JAXXContext context, String helpId) {
        log.info(context + " :: " + helpId);
//        DemoUI mainUI = getUI(context);
//        ObserveHelpBroker helpBroker = context.getContextValue(ObserveHelpBroker.class);
//
//        if (helpId == null) {
//            helpId = helpBroker.getDefaultID();
//        }
//        log.debug("show help " + helpId);
//        mainUI.getHelp().setCurrentID(helpId);
    }

    public void gotoSite(JAXXContext context) {

        DemoUI ui = getUI(context);
        DemoConfig config = ui.getConfig();

        URL siteURL = config.getApplicationSiteUrl();

        log.info(t("jaxxdemo.message.goto.site", siteURL));

        if (log.isDebugEnabled()) {
            log.debug("goto " + siteURL);
        }
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(siteURL.toURI());
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                ErrorDialogUI.showError(ex);
            }
        }
    }

    public void showAbout(DemoUI ui) {

        DemoConfig config = ui.getConfig();

        String iconPath = config.getIconPath();

        String name = "jaxx-demo";
        AboutUI about = AboutUIBuilder.builder(ui)
                                      .setIconPath(config.getIconPath())
                                      .setTitle(t("jaxxdemo.title.about"))
                                      .setBottomText(ui.getConfig().getCopyrightText())
                                      .addAboutTab(t("jaxxdemo.about.message"), true)
                                      .addDefaultLicenseTab(name, false)
                                      .addDefaultThirdPartyTab(name, false)
                                      .addDefaultChangelogTab(name, false)
                                      .build();

        about.display();

    }

    DemoUI getUI(JAXXContext context) {
        if (context instanceof DemoUI) {
            return (DemoUI) context;
        }
        return MAIN_UI_ENTRY_DEF.getContextValue(context);
    }
}
