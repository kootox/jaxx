package org.nuiton.jaxx.demo.component.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.spi.UIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/1/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinatesEditorDemoHandler implements UIHandler<CoordinatesEditorDemo> {

    @Override
    public void beforeInit(CoordinatesEditorDemo ui) {

        CoordinatesEditorDemoModel model = new CoordinatesEditorDemoModel();
        model.setLatitude(-1.5261505f);
        model.setLongitude(47.1963537f);
        model.setQuadrant(1);

        ui.setContextValue(model);

    }

    @Override
    public void afterInit(final CoordinatesEditorDemo ui) {

        ui.getEditor().init();

    }

    public String getQuadrant(Integer quadrant) {
        return t("jaxxdemo.coordinate.result.quadrant", quadrant);
    }

    public String getLatitude(Float latitude) {
        return t("jaxxdemo.coordinate.result.latitude", latitude);
    }

    public String getLongitude(Float longitude) {
        return t("jaxxdemo.coordinate.result.longitude", longitude);
    }

}
