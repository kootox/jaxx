/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;
import org.nuiton.jaxx.runtime.swing.nav.NavNodeChildLoador;
import org.nuiton.jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;

import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class DemoNodeLoador extends NavTreeNodeChildLoador<Object, Object, DemoNode> {

    private static final long serialVersionUID = 1L;

    /** Logger */
    static private final Log log = LogFactory.getLog(NavNodeChildLoador.class);

    public DemoNodeLoador() {
        super(Object.class);
    }

    @Override
    public List<Object> getData(Class<?> parentClass,
                                String packageName,
                                NavDataProvider dataProvider) {

        DemoDataProvider provider = (DemoDataProvider) dataProvider;
        return provider.getImplementations(packageName);
    }

    @Override
    public DemoNode createNode(Object data, NavDataProvider dataProvider) {

        if (log.isDebugEnabled()) {
            log.debug("Creating node for object : " + data);
        }

        DemoNode node = null;

        if (data instanceof String) {
            // package node
            node = new DemoNode((String) data);
        }

        if (data instanceof Class<?>) {
            // demo node
            node = new DemoNode((Class<?>) data);
        }

        if (node == null) {
            throw new IllegalArgumentException("Data [" + data + "] can not be use to build a node");
        }


        return node;
    }
}
