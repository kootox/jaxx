package org.nuiton.jaxx.demo.component.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class TimeEditorDemoHandler implements UIHandler<TimeEditorDemo> {

    protected final DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @Override
    public void beforeInit(TimeEditorDemo ui) {

        TimeEditorDemoModel model = new TimeEditorDemoModel();

        Date now = new Date();

        model.setTime(now);

        ui.setContextValue(model);

    }

    @Override
    public void afterInit(final TimeEditorDemo ui) {

        // init editor
        ui.getEditor().init();

    }

    public String getTime(Date date) {
        return timeFormat.format(date);
    }

}
