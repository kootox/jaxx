/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.feature.databinding;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

import javax.swing.JToggleButton;
import java.io.File;

/**
 * Created: 3 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 */
public class DefaultDemoUIModel extends AbstractSerializableBean implements DemoUIModel {


    private static final long serialVersionUID = 1L;

    protected File file;

    protected File file1;

    protected Boolean f1 = false;

    protected Boolean f0 = false;

    protected JToggleButton t1;

    protected JToggleButton t0;

    public static final String PROPERTY_T1 = "t1";

    public static final String PROPERTY_F1 = "f1";

    public static final String PROPERTY_F0 = "f0";

    public static final String PROPERTY_FILE = "file";

    protected static final Log log = LogFactory.getLog(DefaultDemoUIModel.class);

    /*---------------------------------------------------------------------------------*/
    /*-- public acessor methods -------------------------------------------------------*/
    /*---------------------------------------------------------------------------------*/

    public Boolean getF0() {
        return f0;
    }

    public Boolean getF1() {
        return f1;
    }

    @Override
    public File getFile() {
        return file;
    }

    public File getFile1() {
        return file1;
    }

    @Override
    public JToggleButton getT0() {
        return t0;
    }

    @Override
    public JToggleButton getT1() {
        return t1;
    }

    @Override
    public Boolean isF0() {
        return f0 != null && f0;
    }

    @Override
    public Boolean isF1() {
        return f1 != null && f1;
    }

    /*---------------------------------------------------------------------------------*/
    /*-- public mutator methods -------------------------------------------------------*/
    /*---------------------------------------------------------------------------------*/

    @Override
    public void setF0(Boolean newValue) {
        Boolean oldValue = f0;
        f0 = newValue;
        firePropertyChange(PROPERTY_F0, oldValue, newValue);
    }

    @Override
    public void setF1(Boolean newValue) {
        Boolean oldValue = f1;
        f1 = newValue;
        firePropertyChange(PROPERTY_F1, oldValue, newValue);
    }

    @Override
    public void setFile(File newValue) {
        File oldValue = file;
        file = newValue;
        firePropertyChange(PROPERTY_FILE, oldValue, newValue);
    }


    public void setT1(JToggleButton newValue) {
        JToggleButton oldValue = t1;
        t1 = newValue;
        firePropertyChange(PROPERTY_T1, oldValue, newValue);
    }

}
