/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.tree;

import org.nuiton.jaxx.runtime.swing.nav.tree.NavTreeNode;

/**
 * Basic node of the demo.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class DemoNode extends NavTreeNode<DemoNode> {

    private static final long serialVersionUID = 1L;

    public DemoNode(String id) {
        super(String.class,
              id,
              null,
              DemoTreeHelper.getChildLoador(DemoNodeLoador.class)
        );
    }

    @Override
    public DemoNode getContainerNode() {
        if (isStringNode()) {
            // on est sur un noeud de type String, donc on regarde sur le parent
            return this;
        }

        // fallback (should never comme here since root is Stringnode)
        if (isRoot()) {
            // si on arrive sur le root, quelque chose ne va pas,
            // on bloque par null, a defaut de declancher une exception
            return null;
        }

        // cas final : sur un noeud de donnee + classe interne de donnee
        return getParent().getContainerNode();
    }

    public DemoNode(Class<?> internalClass) {
        super(internalClass, internalClass.getSimpleName(), null, null);
    }
}
