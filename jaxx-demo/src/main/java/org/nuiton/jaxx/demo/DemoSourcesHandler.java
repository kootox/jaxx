/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.font.FontSizor;

import java.awt.event.ItemEvent;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created: 21 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class DemoSourcesHandler implements UIHandler<DemoSources> {

    /** Logger */
    private static final Log log = LogFactory.getLog(DemoSourcesHandler.class);

    protected static Map<String, String> sourcesCache;

    public static String[] addDefaultSources(JAXXObject ui, String... extraSources) {
        List<String> sources = Lists.newArrayList();
        String packageName = ui.getClass().getPackage().getName();
        String simpleName = ui.getClass().getSimpleName();
        addIfExist(ui, sources, packageName, simpleName + ".jaxx");
        addIfExist(ui, sources, packageName, simpleName + ".css");
        addIfExist(ui, sources, packageName, simpleName + "Handler.java");
        addIfExist(ui, sources, packageName, simpleName + "Model.java");
        Collections.addAll(sources, extraSources);
        return sources.toArray(new String[sources.size()]);
    }

    private static void addIfExist(JAXXObject ui,
                                   List<String> sources,
                                   String packageName,
                                   String resourcePath) {
        String path = "/" + packageName.replaceAll("\\.", "/");
        path += "/" + resourcePath;
        InputStream resource = ui.getClass().getResourceAsStream(path);
        try {
            if (resource != null) {
                sources.add(resourcePath);
            }
        } finally {
            IOUtils.closeQuietly(resource);
        }

    }

    public static Map<String, String> getSourcesCache() {
        if (sourcesCache == null) {
            sourcesCache = new TreeMap<>();
        }
        return sourcesCache;
    }

    public String getSourceContent(String name) {
        if (name == null) {
            return "";
        }
        String result = getSourcesCache().get(name);
        if (result == null) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug(name + " from " + ui.getIncomingClass());
                }
                int lastDotIndex = name.lastIndexOf("");
                String path = "/" + name.substring(0, lastDotIndex).replaceAll("\\.", "/");
                path += name.substring(lastDotIndex);
                result = IOUtils.toString(ui.getIncomingClass().getResourceAsStream(path), StandardCharsets.UTF_8);
                if (log.isDebugEnabled()) {
                    log.debug("source [" + name + "], loaded content =\n" + result);
                }
            } catch (Exception e) {
                log.error("could not load file " + name, e);
                result = "could not load file " + name;
            }
            getSourcesCache().put(name, result);
        }
        return result;
    }

    public String getSourceEditingStyle(String source) {

        if (source == null) {
            return "";
        }
        ui.getEditor().setFractionalFontMetricsEnabled(true);
        String s = null;
        if (source.matches(".*\\.jaxx") || source.matches(".*\\.xml")) {
            s = RSyntaxTextArea.SYNTAX_STYLE_XML;
        } else if (source.matches(".*\\.java")) {
            s = RSyntaxTextArea.SYNTAX_STYLE_JAVA;
        } else if (source.matches(".*\\.css")) {
            s = RSyntaxTextArea.SYNTAX_STYLE_CSS;
        }
        if (log.isDebugEnabled()) {
            log.debug("source [" + source + "] style = " + s);
        }
        return s;
    }

    DemoSources ui;

    @Override
    public void beforeInit(DemoSources ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(final DemoSources ui) {

        //fontSizor.setDefaultFontSize(getConfig().getFontSize());
        final FontSizor fontSizor = ui.getFontSizor();
        fontSizor.init();
        fontSizor.setCallBack(() -> {
            log.info("update font size : " + fontSizor.getFontSize());
            ui.getConfig().setFontSize(fontSizor.getFontSize());
        });
    }

    public void updateSource(ItemEvent event) {
        ui.setCurrentSource((String) ui.sourceTabs.getSelectedItem());
        ui.editor.setText(getSourceContent(ui.getCurrentSource()));
        ui.editor.setSyntaxEditingStyle(getSourceEditingStyle(ui.getCurrentSource()));
        ui.editor.setCaretPosition(0);
    }

    public void init(final DemoSources ui) {
        List<String> sources = ui.getSources();
        if (sources == null) {
            return;
        }

        List<String> toFill = new ArrayList<>();
        String prefix = ui.getIncomingClass() == null ? "" :
                ui.getIncomingClass().getPackage().getName() + "";
        for (String source : sources) {
            String path;
            if (source.startsWith("/")) {
                path = source.substring(1);
            } else {
                path = prefix + source;
            }
            if (log.isDebugEnabled()) {
                log.debug("adding resource " + path);
            }
            toFill.add(path);
        }
        SwingUtil.fillComboBox(ui.getSourceTabs(), toFill, null);
        if (!toFill.isEmpty()) {
            ui.getSourceTabs().setSelectedIndex(0);
        }
        toFill.clear();
        ui.getEditor().discardAllEdits();
    }

}
