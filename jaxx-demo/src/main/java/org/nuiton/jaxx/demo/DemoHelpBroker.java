package org.nuiton.jaxx.demo;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.awt.visitor.BuildTreeVisitor;
import org.nuiton.jaxx.runtime.awt.visitor.ComponentTreeNode;
import org.nuiton.jaxx.runtime.awt.visitor.DebugComponentTreeNodeVisitor;
import org.nuiton.jaxx.runtime.awt.visitor.GetCompopentAtPointVisitor;
import org.nuiton.jaxx.runtime.swing.help.JAXXHelpBroker;
import org.nuiton.jaxx.runtime.swing.help.JAXXHelpUI;

import javax.swing.AbstractButton;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Objects;

/**
 * Help broker.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
public class DemoHelpBroker extends JAXXHelpBroker {

    /** Logger */
    private static final Log log = LogFactory.getLog(DemoHelpBroker.class);

    public DemoHelpBroker(String defaultID) {
        super("tutti", "help",
              defaultID,
              DemoApplicationContext.get());
    }

    @Override
    public void prepareUI(JAXXObject c) {

        Objects.requireNonNull(c, "parameter c can not be null!");

        // l'ui doit avoir un boutton showHelp
        AbstractButton help = getShowHelpButton(c);

        if (help != null) {

            // attach context to button
            if (log.isDebugEnabled()) {
                log.debug("attach context to showhelp button " + c);
            }
            help.putClientProperty(JAXX_CONTEXT_ENTRY, c);

            // add tracking action
            ActionListener listener = getShowHelpAction();
            if (log.isDebugEnabled()) {
                log.debug("adding tracking action " + listener);
            }
            help.addActionListener(listener);

            if (log.isDebugEnabled()) {
                log.debug("done for " + c);
            }
        }
    }

    @Override
    public String findHelpId(Component comp) {

        if (comp == null) {
            comp = DemoApplicationContext.get().getMainUI();
        }
        JAXXHelpUI parentContainer = SwingUtil.getParent(comp, JAXXHelpUI.class);

        String result;
        if (parentContainer != null && this != parentContainer.getBroker()) {

            JAXXHelpBroker broker = parentContainer.getBroker();
            result = broker.findHelpId(comp);
        } else {
            result = super.findHelpId(comp);
        }

        if (result == null) {
            result = "ui.main.menu";
        }

        return result;
    }

    @Override
    public Component getDeppestComponent(Component mouseComponent, MouseEvent event) {
        ComponentTreeNode tree = BuildTreeVisitor.buildTree(mouseComponent);

        DebugComponentTreeNodeVisitor debugTree = new DebugComponentTreeNodeVisitor() {
            @Override
            public String getMessage(ComponentTreeNode componentTree) {
                String message = super.getMessage(componentTree);
                Component userObject = componentTree.getUserObject();
                if (userObject.isShowing() && userObject.isVisible()) {
                    Rectangle rectangle = new Rectangle(userObject.getLocationOnScreen(), userObject.getSize());
                    message += " visible - " + rectangle;
                } else {
                    message += " invisible";
                }
                return message;
            }
        };

        debugTree.setDebug(log.isDebugEnabled());

        debugTree.parse(tree);

//        Point point = event.getPoint();
        Point point = event.getLocationOnScreen();

        Component component = GetCompopentAtPointVisitor.get(tree, point);
        if (log.isDebugEnabled()) {
            log.debug("Component at (" + point + "): " + component);
        }
        return component;
    }


}
