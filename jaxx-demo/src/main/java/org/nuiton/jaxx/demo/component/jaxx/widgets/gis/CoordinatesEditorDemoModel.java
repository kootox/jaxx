package org.nuiton.jaxx.demo.component.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created on 9/3/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinatesEditorDemoModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected Integer quadrant;

    protected Float latitude;

    protected Float longitude;

    public Integer getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(Integer quadrant) {
        Object oldValue = getQuadrant();
        this.quadrant = quadrant;
        firePropertyChange("quadrant", oldValue, quadrant);
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        Object oldValue = getLatitude();
        this.latitude = latitude;
        firePropertyChange("latitude", oldValue, latitude);
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        Object oldValue = getLongitude();
        this.longitude = longitude;
        firePropertyChange("longitude", oldValue, longitude);
    }
}
