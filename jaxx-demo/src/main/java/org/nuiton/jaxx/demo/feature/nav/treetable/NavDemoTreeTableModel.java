/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.feature.nav.treetable;

import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.nav.NavNode;
import org.nuiton.jaxx.runtime.swing.nav.treetable.NavTreeTableModel;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class NavDemoTreeTableModel extends NavTreeTableModel.MyDefaultTreeTableModel {

    protected final DemoDataProvider dataProvider;

    public NavDemoTreeTableModel(DemoDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public Object getValueAt(Object o, int i) {
        NavNode node = (NavNode) o;

        // Get node type
        Class<?> editType = node.getInternalClass();
        String id = node.getId();

        // If it's category node
        if (node.isStringNode()) {
            if (i == 0) {
                return t(id);
            }
            return "";

            // People node
        } else if (editType.equals(People.class)) {
            People people = dataProvider.getPeople(id);
            return getPeopleColumn(people, i);

            // Movie node
        } else if (editType.equals(Movie.class)) {
            Movie movie = dataProvider.getMovie(id);
            return getMovieColumn(movie, i);
        }

        // This never append
        return "not found";
    }

    private String getMovieColumn(Movie movie, int i) {
        String result = "";

        switch (i) {
            case 0:
                result = movie.getTitle();
                break;
            case 2:
                result = String.valueOf(movie.getYear());
                break;
        }
        return result;
    }

    protected String getPeopleColumn(People people, int i) {
        String result = "";

        switch (i) {
            case 0:
                result = people.getFirstName();
                break;
            case 1:
                result = people.getLastName();
                break;
            case 2:
                result = String.valueOf(people.getAge());
                break;
        }
        return result;
    }

    @Override
    public String[] getColumnsNames() {
        return new String[]{t("jaxxdemo.common.firstName"),
                t("jaxxdemo.common.lastName"),
                t("jaxxdemo.common.age")};
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return false;
    }
}
