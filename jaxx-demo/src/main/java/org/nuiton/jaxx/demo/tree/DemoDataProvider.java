/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.demo.component.jaxx.BoxedDecoratorDemo;
import org.nuiton.jaxx.demo.component.jaxx.StatusMessagePanelDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.BeanComboBoxDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.BeanDoubleListDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.BeanFilterableComboBoxDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.ComboEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.DatePickerDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.FileEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.editor.I18nEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.widgets.datetime.DateTimeEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.widgets.datetime.TimeEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.widgets.gis.CoordinatesEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.widgets.number.NumberEditorDemo;
import org.nuiton.jaxx.demo.component.jaxx.widgets.select.FilterableDoubleListDemo;
import org.nuiton.jaxx.demo.component.swing.HidorButtonDemo;
import org.nuiton.jaxx.demo.component.swing.JButtonDemo;
import org.nuiton.jaxx.demo.component.swing.JCheckBoxDemo;
import org.nuiton.jaxx.demo.component.swing.JCheckBoxMenuItemDemo;
import org.nuiton.jaxx.demo.component.swing.JComboBoxDemo;
import org.nuiton.jaxx.demo.component.swing.JDialogDemo;
import org.nuiton.jaxx.demo.component.swing.JListDemo;
import org.nuiton.jaxx.demo.component.swing.JMenuItemDemo;
import org.nuiton.jaxx.demo.component.swing.JPasswordFieldDemo;
import org.nuiton.jaxx.demo.component.swing.JProgressBarDemo;
import org.nuiton.jaxx.demo.component.swing.JRadioButtonDemo;
import org.nuiton.jaxx.demo.component.swing.JRadioButtonMenuItemDemo;
import org.nuiton.jaxx.demo.component.swing.JSliderDemo;
import org.nuiton.jaxx.demo.component.swing.JSpinnerDemo;
import org.nuiton.jaxx.demo.component.swing.JSplitPaneDemo;
import org.nuiton.jaxx.demo.component.swing.JTextAreaDemo;
import org.nuiton.jaxx.demo.component.swing.JTextFieldDemo;
import org.nuiton.jaxx.demo.component.swing.JToggleButtonDemo;
import org.nuiton.jaxx.demo.feature.databinding.BeanDataBindingDemo;
import org.nuiton.jaxx.demo.feature.databinding.BindingExtremeDemo;
import org.nuiton.jaxx.demo.feature.nav.NavDemo;
import org.nuiton.jaxx.demo.feature.validation.list.ListBeanValidationDemo;
import org.nuiton.jaxx.demo.feature.validation.simple.SimpleBeanValidationByListDemo;
import org.nuiton.jaxx.demo.feature.validation.simple.SimpleBeanValidationByTableDemo;
import org.nuiton.jaxx.demo.fun.CalculatorDemo;
import org.nuiton.jaxx.demo.fun.CounterDemo;
import org.nuiton.jaxx.demo.fun.LabelStyleDemo;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * Data provider of the navigation tree.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class DemoDataProvider implements NavDataProvider {

    /** Logger */
    static private final Log log = LogFactory.getLog(DemoDataProvider.class);

    /** mapping of demo per package */
    protected final Map<String, List<Object>> mapping;

    public DemoDataProvider() {

        mapping = new HashMap<>();

        if (log.isDebugEnabled()) {
            log.debug("for " + this);
        }

        addMapping("jaxxdemo.tree",
                   n("jaxxdemo.component.swing"),
                   n("jaxxdemo.component.jaxx"),
                   n("jaxxdemo.feature"),
                   n("jaxxdemo.fun")
        );

        addMapping(n("jaxxdemo.component.swing"),
                   n("jaxxdemo.component.swing.buttons"),
                   n("jaxxdemo.component.swing.form"),
                   n("jaxxdemo.component.swing.layout"),
                   n("jaxxdemo.component.swing.menu"),
                   n("jaxxdemo.component.swing.window"),
                   JProgressBarDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.buttons"),
                   JButtonDemo.class,
                   JCheckBoxDemo.class,
                   JRadioButtonDemo.class,
                   JToggleButtonDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.form"),
                   n("jaxxdemo.component.swing.form.text"),
                   JComboBoxDemo.class,
                   JListDemo.class,
                   JSliderDemo.class,
                   JSpinnerDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.form.text"),
                   JPasswordFieldDemo.class,
                   JTextFieldDemo.class,
                   JTextAreaDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.layout"),
                   JSplitPaneDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.menu"),
                   JMenuItemDemo.class,
                   JCheckBoxMenuItemDemo.class,
                   JRadioButtonMenuItemDemo.class
        );

        addMapping(n("jaxxdemo.component.swing.window"),
                   JDialogDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx"),
                   HidorButtonDemo.class,
                   n("jaxxdemo.component.jaxx.widgets"),
                   n("jaxxdemo.component.jaxx.editor"),
                   BoxedDecoratorDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets"),
                   n("jaxxdemo.component.jaxx.widgets.number"),
                   n("jaxxdemo.component.jaxx.widgets.select"),
                   n("jaxxdemo.component.jaxx.widgets.gis"),
                   n("jaxxdemo.component.jaxx.widgets.file"),
                   n("jaxxdemo.component.jaxx.widgets.i18n"),
                   n("jaxxdemo.component.jaxx.widgets.status"),
                   n("jaxxdemo.component.jaxx.widgets.datetime")
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.number"),
                   NumberEditorDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.select"),
                   BeanComboBoxDemo.class,
                   BeanFilterableComboBoxDemo.class,
                   BeanDoubleListDemo.class,
                   FilterableDoubleListDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.gis"),
                   CoordinatesEditorDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.file"),
                   FileEditorDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.i18n"),
                   I18nEditorDemo.class
        );
        addMapping(n("jaxxdemo.component.jaxx.widgets.status"),
                   StatusMessagePanelDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.widgets.datetime"),
                   DateTimeEditorDemo.class,
                   TimeEditorDemo.class,
                   DatePickerDemo.class
        );

        addMapping(n("jaxxdemo.component.jaxx.editor"),
                   ComboEditorDemo.class

        );

        addMapping(n("jaxxdemo.feature"),
                   n("jaxxdemo.feature.databinding"),
                   n("jaxxdemo.feature.validation"),
                   n("jaxxdemo.feature.nav")
        );

        addMapping(n("jaxxdemo.feature.databinding"),
                   BindingExtremeDemo.class,
                   BeanDataBindingDemo.class
        );

        addMapping(n("jaxxdemo.feature.validation"),
                   n("jaxxdemo.feature.simple.validation"),
                   n("jaxxdemo.feature.list.validation")
        );

        addMapping(n("jaxxdemo.feature.simple.validation"),
                   SimpleBeanValidationByListDemo.class,
                   SimpleBeanValidationByTableDemo.class
        );

        addMapping(n("jaxxdemo.feature.list.validation"),
                   ListBeanValidationDemo.class
        );
        addMapping(n("jaxxdemo.feature.nav"),
                   NavDemo.class
        );

        addMapping(n("jaxxdemo.fun"),
                   LabelStyleDemo.class,
                   CounterDemo.class,
                   CalculatorDemo.class
        );
    }

    protected void addMapping(String packageName, Object... classes) {
        List<Object> list = Arrays.asList(classes);
        if (log.isDebugEnabled()) {
            log.debug("Adding mapping [" + packageName + "] : " + list);
        }
        mapping.put(packageName, list);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public List<Object> getImplementations(String packageName) {
        return mapping.get(packageName);
    }


}
