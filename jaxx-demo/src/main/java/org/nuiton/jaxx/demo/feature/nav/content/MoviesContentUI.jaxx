<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<AbstractContentUI superGenericType='List&lt;Movie&gt;'>

  <import>
    java.util.List
    org.nuiton.jaxx.demo.entities.Movie
    org.nuiton.jaxx.widgets.select.BeanListHeader
    org.nuiton.jaxx.runtime.swing.renderer.DecoratorProviderListCellRenderer

    static org.nuiton.i18n.I18n.t
  </import>
  <script><![CDATA[

private void $afterCompleteSetup() {
    getHandler().initUI(this);    
}
 ]]></script>

  <java.util.List id='data' genericType='Movie' javaBean='null'/>

  <JPanel layout='{new BorderLayout()}'>

    <JScrollPane
            border='{new TitledBorder(t("jaxxdemo.navigation.movies.title"))}'
            constraints='BorderLayout.CENTER'
            columnHeaderView='{toolbar}'
            horizontalScrollBarPolicy='{JScrollPane.HORIZONTAL_SCROLLBAR_NEVER}'
            verticalScrollBarPolicy='{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED}'>

      <!-- list of movies -->
      <JList id='list'
             cellRenderer='{getContextValue(DecoratorProviderListCellRenderer.class)}'/>

      <!-- list header -->
      <JToolBar id='toolbar' floatable='false' layout='{new BorderLayout()}'>
        <JPanel layout='{new GridLayout(1,0)}'
                constraints='BorderLayout.CENTER'>

          <!-- to show the selected actor -->
          <JButton text='jaxxdemo.action.show'
                   enabled='{list.getSelectedIndex()!=-1}'
                   onActionPerformed='getHandler().selectChild(this, (Movie) list.getSelectedValue())'/>

          <!-- to show a new actor -->
          <JButton text='jaxxdemo.action.add'/>

          <!-- to delete the selected actor -->
          <JButton text='jaxxdemo.action.remove'
                   enabled='{list.getSelectedIndex()!=-1}'/>
        </JPanel>

        <BeanListHeader id='listHeader' constraints='BorderLayout.EAST'
                        i18nPrefix='jaxxdemo.common.'
                        data='{getData()}' genericType='Movie'
                        list='{list}' showReset='true'/>
      </JToolBar>
    </JScrollPane>

  </JPanel>
</AbstractContentUI>
