/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.entities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.7.2
 */
public class Movie extends AbstractDemoBean {

    static {
        n("jaxxdemo.common.movie");
        n("jaxxdemo.common.title");
        n("jaxxdemo.common.year");
        n("jaxxdemo.common.actors");
    }

    protected String title;

    protected int year;

    protected List<People> actors;

    private static final long serialVersionUID = 1L;

    private static final String PROPERTY_ACTORS = "actors";

    private static final String PROPERTY_TITLE = "title";

    private static final String PROPERTY_YEAR = "year";

    private static final String PROPERTY_IMAGE = "image";

    public Movie(String id, String title, int year, String image) {
        super(id, image);
        this.title = title;
        this.year = year;
        actors = new ArrayList<>();
    }

    public Movie() {
        actors = new ArrayList<>();
    }

    public List<People> getActors() {
        return actors;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public void setActors(List<People> actors) {
        Object oldValue = this.actors;
        this.actors = actors;
        firePropertyChange(PROPERTY_ACTORS, null, actors);
    }

    public void setTitle(String title) {
        Object oldValue = this.title;
        this.title = title;
        firePropertyChange(PROPERTY_TITLE, oldValue, title);
    }

    public void setYear(int year) {
        Object oldValue = this.year;
        this.year = year;
        firePropertyChange(PROPERTY_YEAR, oldValue, year);
    }


    public void addActor(People actor) {
        actors.add(actor);
        firePropertyChange(PROPERTY_ACTORS, null, actors);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Movie other = (Movie) obj;
        return !(id == null ? other.id != null : !id.equals(other.id));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        ToStringBuilder b = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        b.append(PROPERTY_ID, id);
        b.append(PROPERTY_TITLE, title);
        b.append(PROPERTY_IMAGE, image);
        b.append(PROPERTY_YEAR, year);
        b.append(PROPERTY_ACTORS, actors);
        return b.toString();
    }
}
