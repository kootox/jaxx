/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.entities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class DemoDataProvider implements NavDataProvider {

    /** Logger */
    static private final Log log = LogFactory.getLog(DemoDataProvider.class);

    protected final Map<String, Movie> movies;

    protected final Map<String, People> peoples;

    public DemoDataProvider() {

        movies = new HashMap<>();
        peoples = new HashMap<>();

        if (log.isDebugEnabled()) {
            log.debug("for " + this);
        }
        People a = new People("0", "Jack", "Black", 35, "/org/nuiton/jaxx/demo/images/jack.jpg");
        People a2 = new People("1", "Héctor", "Jiménez", 28, "/org/nuiton/jaxx/demo/images/hector.jpg");
        People a3 = new People("2", "Ana", "de la Reguera", 34, "/org/nuiton/jaxx/demo/images/ana.jpg");
        People a4 = new People("3", "Jack", "Nicholson", 76, "/org/nuiton/jaxx/demo/images/joker.jpg");
        People a5 = new People("4", "Jim", "Parsons", 30, "/org/nuiton/jaxx/demo/images/jim.jpg");

        Movie m = new Movie("0", "Nacho libre", 1996, "/org/nuiton/jaxx/demo/images/nacho.jpg");
        m.addActor(a);
        m.addActor(a2);
        m.addActor(a3);

        Movie m2 = new Movie("1", "Nacho 2", 2009, "/org/nuiton/jaxx/demo/images/nacho2.png");
        m2.addActor(a);
        m2.addActor(a2);

        movies.put(m.getId(), m);
        movies.put(m2.getId(), m2);

        peoples.put(a.getId(), a);
        peoples.put(a2.getId(), a2);
        peoples.put(a3.getId(), a3);
        peoples.put(a4.getId(), a4);
        peoples.put(a5.getId(), a5);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Movie getMovie(String id) {
        return movies.get(id);
    }

    public People getPeople(String id) {
        return peoples.get(id);
    }

    public List<Movie> getMovies() {
        return new ArrayList<>(movies.values());
    }

    public List<People> getPeoples() {
        return new ArrayList<>(peoples.values());
    }

    public List<People> getPeoples(Movie m) {
        return m.getActors();
    }

    public List<People> getPeoples(String moviesId) {

        if (log.isDebugEnabled()) {
            log.debug("Get people for movie " + moviesId);
        }

        return movies.get(moviesId).getActors();
    }
}
