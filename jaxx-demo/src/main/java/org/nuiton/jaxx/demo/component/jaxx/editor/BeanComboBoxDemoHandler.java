package org.nuiton.jaxx.demo.component.jaxx.editor;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.DemoDecoratorProvider;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Handlerof {@link BeanComboBoxDemo}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.9
 */
public class BeanComboBoxDemoHandler implements UIHandler<BeanComboBoxDemo> {

    private JXPathDecorator<People> decorator;

    @Override
    public void beforeInit(BeanComboBoxDemo ui) {
        this.decorator = (JXPathDecorator<People>)
                new DemoDecoratorProvider().getDecoratorByType(People.class);
    }

    @Override
    public void afterInit(BeanComboBoxDemo ui) {
        List<People> data = new DemoDataProvider().getPeoples();

        ui.getComboBox().init(decorator, data);
    }

    public String getSelectedResult(People people) {
        String result;

        if (people == null) {
            result = t("jaxxdemo.beancomboboxdemo.no.people.selected");
        } else {
            result = t("jaxxdemo.beancomboboxdemo.selected.people", decorator.toString(people));
        }
        return result;
    }

    static {
        n("jaxxdemo.common.people");
        n("jaxxdemo.common.firstName");
        n("jaxxdemo.common.lastName");
        n("jaxxdemo.common.age");
    }
}
