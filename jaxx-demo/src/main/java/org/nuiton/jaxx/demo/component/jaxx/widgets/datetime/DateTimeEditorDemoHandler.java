package org.nuiton.jaxx.demo.component.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 9/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateTimeEditorDemoHandler implements UIHandler<DateTimeEditorDemo> {

    protected final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    protected final DateFormat dayDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    protected final DateFormat timeDateFormat = new SimpleDateFormat("HH:mm");

    @Override
    public void beforeInit(DateTimeEditorDemo ui) {

        DateTimeEditorDemoModel model = new DateTimeEditorDemoModel();

        Date now = new Date();

        model.setDate(now);
        model.setDayDate(now);
        model.setTimeDate(now);

        ui.setContextValue(model);

    }

    @Override
    public void afterInit(final DateTimeEditorDemo ui) {

        // init editor
        ui.getEditor().init();

    }

    public String getDate(Date date) {
        return dateFormat.format(date);
    }

    public String getDayDate(Date date) {
        return dayDateFormat.format(date);
    }

    public String getTimeDate(Date date) {
        return timeDateFormat.format(date);
    }

}
