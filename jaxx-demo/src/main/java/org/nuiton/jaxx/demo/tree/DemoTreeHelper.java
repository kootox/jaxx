/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.tree;

import org.nuiton.jaxx.runtime.swing.nav.tree.NavTreeHelper;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class DemoTreeHelper extends NavTreeHelper<DemoNode> {

    public DemoTreeHelper(DemoDataProvider provider) {
        setDataProvider(provider);
    }

    @Override
    public DemoDataProvider getDataProvider() {
        return (DemoDataProvider) super.getDataProvider();
    }

    public TreeModel createModel() {

        // Create root static node
        DemoNode root = new DemoNode("jaxxdemo.tree");

        // Create model
        DefaultTreeModel model = createModel(root);

        // load all nodes of model
        loadAllNodes(root, getDataProvider());

        return model;
    }
}
