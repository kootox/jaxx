/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.feature.databinding;

import javax.swing.JToggleButton;
import java.beans.PropertyChangeListener;
import java.io.File;

/**
 * Created: 2 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public interface DemoUIModel {

    Boolean isF0();

    Boolean isF1();

    void setF0(Boolean b);

    void setF1(Boolean b);

    JToggleButton getT0();

    JToggleButton getT1();

    File getFile();

    void setFile(File newValue);

//    /**
//     * All <code>JAXXObject</code> implements are capable of broadcasting <code>PropertyChangeEvent</code>, and
//     * furthermore (for technical reasons) must allow code in outside packages, specifically the JAXX runtime,
//     * to trigger these events.
//     *
//     * @param name     the name of the property which changed
//     * @param oldValue the old value of the property
//     * @param newValue the new value of the property
//     */
//    void firePropertyChange(String name, Object oldValue, Object newValue);

    /**
     * Register a general {@link PropertyChangeListener}.
     *
     * @param listener the listener to register
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Register a  {@link PropertyChangeListener}. for the given {@code propertyName}.
     *
     * @param property the property name to listen
     * @param listener the listener to register
     */
    void addPropertyChangeListener(String property, PropertyChangeListener listener);

    /**
     * Unregister a general {@link PropertyChangeListener}.
     *
     * @param listener the listener to unregister
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Unregister a  {@link PropertyChangeListener}. for the given {@code propertyName}.
     *
     * @param property the property name to listen
     * @param listener the listener to unregister
     */
    void removePropertyChangeListener(String property, PropertyChangeListener listener);

}
