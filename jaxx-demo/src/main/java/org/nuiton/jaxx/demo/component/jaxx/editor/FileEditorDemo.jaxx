<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>
  <import>
    org.nuiton.jaxx.widgets.file.FileEditor
  </import>

  <String id='title' javaBean='"Open file"'/>
  <String id='exts' javaBean='"txt, png"'/>
  <String id='extsDescription' javaBean='"Text (*.txt), Image (*.png)"'/>
  <Boolean id='acceptAllFileFilterUsed' javaBean='Boolean.TRUE'/>
  <Boolean id='directoryEnabled' javaBean='Boolean.TRUE'/>
  <Boolean id='fileEnabled' javaBean='Boolean.FALSE'/>
  <Boolean id='showReset' javaBean='Boolean.TRUE'/>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.titleLbl'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JTextField id='titleField'
                    text='{getTitle()}'
                    onKeyReleased='setTitle(titleField.getText())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.directoryEnabled'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JCheckBox id='directoryEnabledField'
                   selected='{isDirectoryEnabled()}'
                   onActionPerformed='setDirectoryEnabled(directoryEnabledField.isSelected())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.fileEnabled'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JCheckBox id='fileEnabledField'
                   selected='{isFileEnabled()}'
                   onActionPerformed='setFileEnabled(fileEnabledField.isSelected())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.extsLbl'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JTextField id='extsField'
                    enabled='{isFileEnabled()}'
                    text='{getExts()}'
                    onKeyReleased='setExts(extsField.getText())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.extsDescLbl'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JTextField id='extsDescriptionField'
                    enabled='{isFileEnabled()}'
                    text='{getExtsDescription()}'
                    onKeyReleased='setExtsDescription(extsDescriptionField.getText())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.acceptAllFileFilterUsed'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JCheckBox id='acceptAllFileFilterUsedField'
                   selected='{isAcceptAllFileFilterUsed()}'
                   onActionPerformed='setAcceptAllFileFilterUsed(acceptAllFileFilterUsedField.isSelected())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1'>
        <JLabel text='jaxxdemo.fileEditor.showResetLabel'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JCheckBox id='showResetCheckBox'
                   selected='{isShowReset()}'
                   onActionPerformed='setShowReset(showResetCheckBox.isSelected())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1' columns='2'>
        <FileEditor id='fileChooser'
                    title='{getTitle()}'
                    exts='{getExts()}'
                    directoryEnabled='{isDirectoryEnabled()}'
                    showReset='{isShowReset()}'
                    fileEnabled='{isFileEnabled()}'
                    acceptAllFileFilterUsed='{isAcceptAllFileFilterUsed()}'
                    extsDescription='{getExtsDescription()}'/>
      </cell>
    </row>
  </Table>
</org.nuiton.jaxx.demo.DemoPanel>
