package org.nuiton.jaxx.demo;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.nuiton.jaxx.runtime.swing.help.JAXXHelpBroker;
import org.nuiton.jaxx.runtime.swing.help.JAXXHelpUIHandler;

import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class DemoApplicationContext extends DefaultApplicationContext implements JAXXHelpUIHandler {

    static final JAXXContextEntryDef<DemoUI> MAIN_UI =
            JAXXUtil.newContextEntryDef("MainUI", DemoUI.class);

    /** The singleton instance of the main context */
    private static volatile DemoApplicationContext context;

    /** Logger. */
    private static final Log log = LogFactory.getLog(DemoApplicationContext.class);

    /**
     * @return <code>true</code> si le context a été initialisé via la méthode
     * {@link #init(DemoConfig)}, <code>false</code> autrement.
     */
    protected static boolean isInit() {
        return context != null;
    }

    /**
     * Permet l'initialisation du contexte applicatif et positionne
     * l'context partagée.
     *
     * Note : Cette méthode ne peut être appelée qu'une seule fois.
     *
     * @param config application config
     * @return le context partagée
     * @throws IllegalStateException si un contexte applicatif a déja été positionné.
     */
    protected static DemoApplicationContext init(DemoConfig config) throws IllegalStateException {
        if (isInit()) {
            throw new IllegalStateException("there is an already application context registred.");
        }
        context = new DemoApplicationContext(config);
        return context;
    }

    /**
     * Récupération du contexte applicatif.
     *
     * @return l'context partagé du contexte.
     * @throws IllegalStateException si le contexte n'a pas été initialisé via
     *                               la méthode {@link #init(DemoConfig)}
     */
    public static DemoApplicationContext get() throws IllegalStateException {
        if (!isInit()) {
            throw new IllegalStateException("no application context registred.");
        }
        return context;
    }

    public DemoApplicationContext(DemoConfig config) {

        // share config
        setContextValue(config);

        //--------------------------------------------------------------------//
        // init help
        //--------------------------------------------------------------------//

        // load help mapping
        String mappingProperties = "/jaxxdemo-help-fr.properties";
        try {

            InputStream resourceAsStream =
                    getClass().getResourceAsStream(mappingProperties);
            helpMapping = new Properties();
            helpMapping.load(resourceAsStream);

        } catch (Exception eee) {
            log.error("Failed to load help mapping file at '" +
                              mappingProperties + "'", eee);
        }
    }

    public DemoConfig getConfig() {
        return getContextValue(DemoConfig.class);
    }

    private Properties helpMapping;

    @Override
    public void showHelp(JAXXContext context, JAXXHelpBroker broker, String helpId) {

        if (helpId == null) {
            helpId = broker.getDefaultID();
        }

        if (log.isInfoEnabled()) {
            log.info("help-id: " + helpId);
        }

        String value = (String) helpMapping.get(helpId);

        if (value == null) {
            throw new RuntimeException(t("jaxxdemo.context.helpPage.notFound", helpId));
        }

        String helpDirectory = getConfig().getHelpResourceWithLocale(value);
        boolean withFragment = helpDirectory.contains("#");

        String fragment = null;
        if (withFragment) {
            fragment = StringUtils.substringAfter(helpDirectory, "#");
            helpDirectory = StringUtils.substringBefore(helpDirectory, "#");
        }

        URI resolvedUri = URI.create(helpDirectory);
//        try {

        if (withFragment) {
            resolvedUri = URI.create(resolvedUri.toString() + "#" + fragment);
        }
        if (log.isInfoEnabled()) {
            log.info("help-uri: " + resolvedUri);
        }
        getMainUI().getP().setStatus(t("jaxxdemo.openHelp", helpId));
//            SwingUtil.openLink(resolvedUri);
//        } catch (URISyntaxException e) {
//            throw new RuntimeException(t("jaxxdemo.context.helpPage.notFound", resolvedUri));
//        }
    }

    public DemoUI getMainUI() {

        return MAIN_UI.getContextValue(this);
    }

}
