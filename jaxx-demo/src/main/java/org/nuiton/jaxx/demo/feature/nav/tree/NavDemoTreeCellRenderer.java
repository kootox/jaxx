/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Component;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class NavDemoTreeCellRenderer extends AbstractNavTreeCellRenderer<DefaultTreeModel, NavDemoTreeNode> {

    /** Logger */
    protected static final Log log =
            LogFactory.getLog(NavDemoTreeCellRenderer.class);

    protected final DecoratorProvider decoratorProvider;

    public NavDemoTreeCellRenderer(DecoratorProvider decoratorProvider,
                                   DemoDataProvider provider) {
        setDataProvider(provider);
        this.decoratorProvider = decoratorProvider;
    }

    @Override
    public DemoDataProvider getDataProvider() {
        return (DemoDataProvider) super.getDataProvider();
    }

    @Override
    protected String computeNodeText(NavDemoTreeNode node) {

        // Get node type
        Class<?> editType = node.getInternalClass();
        String id = node.getId();

        // get decorator
        Decorator<?> decorator = decoratorProvider.getDecoratorByType(editType);

        Object toDecorate = null;

        // People node
        if (editType.equals(People.class)) {
            toDecorate = getDataProvider().getPeople(id);

            // Movie node
        } else if (editType.equals(Movie.class)) {
            toDecorate = getDataProvider().getMovie(id);
        }

        // Get decorated value
        String decorated = decorator.toString(toDecorate);

        if (log.isDebugEnabled()) {
            log.debug("Compute text for node " +
                              node + " return " +
                              decorated);
        }

        return decorated;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {

        if (!(value instanceof NavDemoTreeNode)) {
            return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }

        // get node
        NavDemoTreeNode node = (NavDemoTreeNode) value;

        // get text for node
        String text = getNodeText(node);

        // Render node
        return super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
    }
}
