/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.entities;

import org.jdesktop.beans.AbstractSerializableBean;

public class Model extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected String text = "text";

    protected String text2 = "text2";

    protected int ratio = 51;

    public String getText() {
        return text;
    }

    public String getText2() {
        return text2;
    }

    public int getRatio() {
        return ratio;
    }

    public void setText(String text) {
        String oldText = this.text;
        this.text = text;
        firePropertyChange("text", oldText, text);
    }

    public void setText2(String text2) {
        String oldText2 = this.text2;
        this.text2 = text2;
        firePropertyChange("text2", oldText2, text2);
    }

    public void setRatio(int ratio) {
        int oldRatio = this.ratio;
        this.ratio = ratio;
        firePropertyChange("ratio", oldRatio, ratio);
    }
}
