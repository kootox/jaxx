package org.nuiton.jaxx.demo.component.jaxx.widgets.select;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.DemoDecoratorProvider;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.select.FilterableDoubleList;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class FilterableDoubleListDemoHandler implements UIHandler<FilterableDoubleListDemo> {

    private JXPathDecorator<People> decorator;

    @Override
    public void beforeInit(FilterableDoubleListDemo ui) {

        decorator = (JXPathDecorator<People>) new DemoDecoratorProvider().getDecoratorByType(People.class);

        FilterableDoubleListDemoModel model = new FilterableDoubleListDemoModel();
        ui.setContextValue(model);

    }

    @Override
    public void afterInit(final FilterableDoubleListDemo ui) {

        FilterableDoubleList<People> doubleList = ui.getDoubleList();

        DemoDataProvider demoDataProvider = new DemoDataProvider();

        List<People> data = demoDataProvider.getPeoples();
        List<People> selected = data.subList(0, 1);

        doubleList.init(decorator, data, selected);

    }

    public String updateResultText(List<People> selected) {

        StringBuilder builder = new StringBuilder();
        if (selected == null || selected.isEmpty()) {

            builder.append(t("jaxxdemo.beandoublelistdemo.noselected"));

        } else {

            builder.append(t("jaxxdemo.beandoublelistdemo.selected"));
            builder.append("<ul>");

            for (People people : selected) {

                String peopleStr = decorator.toString(people);
                builder.append("<li>");
                builder.append(peopleStr);
                builder.append("</li>");

            }

            builder.append("</ul>");

        }

        return builder.toString();

    }
}
