/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.entities;

import org.jdesktop.beans.AbstractSerializableBean;

import static org.nuiton.i18n.I18n.n;

/**
 * Abstract demo bean.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.2
 */
public abstract class AbstractDemoBean extends AbstractSerializableBean {

    static {
        n("jaxxdemo.common.id");
        n("jaxxdemo.common.image");
    }

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_IMAGE = "image";

    private static final long serialVersionUID = 1L;

    protected String id;

    protected String image;

    public AbstractDemoBean() {
    }

    protected AbstractDemoBean(String id, String image) {
        this.id = id;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public void setId(String id) {
        String old = this.id;
        this.id = id;
        firePropertyChange(PROPERTY_ID, old, id);
    }


    public void setImage(String image) {
        Object oldValue = this.image;
        this.image = image;
        firePropertyChange(PROPERTY_IMAGE, oldValue, image);
    }
}
