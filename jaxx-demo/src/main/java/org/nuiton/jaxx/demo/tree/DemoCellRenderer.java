/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class DemoCellRenderer extends AbstractNavTreeCellRenderer<DefaultTreeModel, DemoNode> {

    /** Logger */
    protected static final Log log =
            LogFactory.getLog(DemoCellRenderer.class);


    public DemoCellRenderer(org.nuiton.jaxx.demo.tree.DemoDataProvider provider) {
        setDataProvider(provider);
    }

    @Override
    public org.nuiton.jaxx.demo.entities.DemoDataProvider getDataProvider() {
        return (org.nuiton.jaxx.demo.entities.DemoDataProvider) super.getDataProvider();
    }

    @Override
    protected String computeNodeText(DemoNode node) {

        if (node == null) {
            return "";
        }


        String toDecorate;

        String id = node.getId();

        if (node.isStringNode()) {

            // String node
            toDecorate = t(id);
        } else {

            // Demo node
            toDecorate = id;
        }

        if (log.isDebugEnabled()) {
            log.debug("Compute text for node " + node +
                              " (" + node.getInternalClass() + ") = " + toDecorate);
        }

        return toDecorate;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {

        if (!(value instanceof DemoNode)) {
            return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }

        // get node
        DemoNode node = (DemoNode) value;

        // get text for node
        String text = getNodeText(node);

        // Render node
        return super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
    }
}
