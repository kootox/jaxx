package org.nuiton.jaxx.demo.feature.validation.list;
/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.jdesktop.swingx.JXTable;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.demo.entities.DemoDecoratorProvider;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.validator.swing.SwingListValidatorDataLocator;
import org.nuiton.jaxx.validator.swing.SwingListValidatorMessageTableRenderer;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;

import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import java.util.Collections;
import java.util.UUID;

import static org.nuiton.i18n.I18n.n;

/**
 * Handler of UI {@link ListBeanValidationDemo}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.3
 */
public class ListBeanValidationDemoHandler implements UIHandler<ListBeanValidationDemo> {

    private ListBeanValidationDemo ui;

    @Override
    public void beforeInit(ListBeanValidationDemo ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(ListBeanValidationDemo ui) {

        // customize data table

        JXTable dataTable = ui.getDataTable();

        dataTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));
        SwingUtil.setI18nTableHeaderRenderer(
                dataTable,
                n("jaxx.demo.label.id"),
                n("jaxx.demo.label.id.tip"),
                n("jaxx.demo.label.firstName"),
                n("jaxx.demo.label.firstName.tip"),
                n("jaxx.demo.label.lastName"),
                n("jaxx.demo.label.lastName.tip"),
                n("jaxx.demo.label.age"),
                n("jaxx.demo.label.age.tip"));

        SwingUtil.fixTableColumnWidth(dataTable, 3, 35);

        // register validator
        BeanListValidator<People> validator = ui.getValidator();

        // customize error table

        JTable errorTable = ui.getErrorTable();

        PeopleValidatorDataLocator dataLocator = new PeopleValidatorDataLocator();

        SwingValidatorUtil.installUI(errorTable,
                                     new SwingListValidatorMessageTableRenderer() {

                                         private static final long serialVersionUID = 1L;

                                         final Decorator<People> decorator
                                                 = new DemoDecoratorProvider().getDecoratorByType(People.class);

                                         @Override
                                         protected String decorateBean(Object bean) {
                                             return decorator.toString(bean);
                                         }
                                     });

        SwingValidatorUtil.registerListValidator(
                validator,
                ui.getErrorTableModel(),
                dataTable,
                errorTable,
                dataLocator);

        SwingValidatorUtil.addHightLighterOnEditor(
                validator, dataTable, dataLocator,
                NuitonValidatorScope.ERROR,
                NuitonValidatorScope.WARNING);

        // add some datas in model

        People a = new People("0", "Jack", "Black", 12, "/org/nuiton/jaxx/demo/images/jack.jpg");
        People a2 = new People("1", "Héctor", "Jiménez", 28, "/org/nuiton/jaxx/demo/images/hector.jpg");
        People a3 = new People("2", "Ana", "de la Reguera", 34, "/org/nuiton/jaxx/demo/images/ana.jpg");

        addPeople(a);
        addPeople(a2);
        addPeople(a3);
    }

    public void addPeople() {
        People bean = new People();
        bean.setId(UUID.randomUUID().toString());
        addPeople(bean);
    }


    public void addPeople(People bean) {
        PeopleTableModel model = ui.getModel();
        model.addBean(bean);
    }

    public void removePeople() {
        int selectedRow = ui.getDataTable().getSelectedRow();
        PeopleTableModel model = ui.getModel();
        model.removeBean(selectedRow);
    }

    public void updateOkEnabled() {
        BeanListValidator<People> validator = ui.getValidator();
        boolean valid = !validator.hasErrors();
        ui.getOk().setEnabled(valid);
    }

    private static class PeopleValidatorDataLocator implements SwingListValidatorDataLocator<People> {

        @Override
        public boolean acceptType(Class<?> beanType) {
            return People.class.isAssignableFrom(beanType);
        }

        @Override
        public Pair<Integer, Integer> locateDataCell(TableModel tableModel,
                                                     People bean,
                                                     String fieldName) {
            PeopleTableModel model = (PeopleTableModel) tableModel;

            return model.getCell(bean, fieldName);
        }

        @Override
        public int locateBeanRowIndex(TableModel tableModel, People bean) {
            PeopleTableModel model = (PeopleTableModel) tableModel;
            return model.getBeanIndex(bean);
        }

        @Override
        public People locateBean(TableModel tableModel, int rowIndex) {
            PeopleTableModel model =
                    (PeopleTableModel) tableModel;
            return model.getBean(rowIndex);
        }
    }

}


