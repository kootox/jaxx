/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.entities;

import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;

public class Identity extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected String firstName = "";

    protected String lastName = "";

    protected String email = "dummy@codelutin.com";

    protected int age = 51;

    protected File config = new File("/tmp");

    protected File dir = new File("/tmp");


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    public File getConfig() {
        return config;
    }

    public File getDir() {
        return dir;
    }

    public void setFirstName(String firstName) {
        String oldFirstName = this.firstName;
        this.firstName = firstName;
        firePropertyChange("firstName", oldFirstName, firstName);
    }

    public void setLastName(String lastName) {
        String oldLastName = this.lastName;
        this.lastName = lastName;
        firePropertyChange("lastName", oldLastName, lastName);
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        firePropertyChange("email", oldEmail, email);
    }

    public void setAge(int age) {
        int oldAge = this.age;
        this.age = age;
        firePropertyChange("age", oldAge, age);
    }

    public void setConfig(File config) {
        File oldConfig = this.config;
        this.config = config;
        firePropertyChange("config", oldConfig, config);
    }

    public void setDir(File dir) {
        File oldDir = this.dir;
        this.dir = dir;
        firePropertyChange("dir", oldDir, dir);
    }
}
