/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.demo.feature.nav;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTreeTable;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.jaxx.demo.entities.AbstractDemoBean;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.demo.feature.nav.content.AbstractContentUI;
import org.nuiton.jaxx.demo.feature.nav.content.ActorContentUI;
import org.nuiton.jaxx.demo.feature.nav.content.ActorsContentUI;
import org.nuiton.jaxx.demo.feature.nav.content.MovieContentUI;
import org.nuiton.jaxx.demo.feature.nav.content.MoviesContentUI;
import org.nuiton.jaxx.demo.feature.nav.tree.NavDemoTreeCellRenderer;
import org.nuiton.jaxx.demo.feature.nav.tree.NavDemoTreeNode;
import org.nuiton.jaxx.demo.feature.nav.treetable.NavDemoTreeTableNode;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.CardLayout2;
import org.nuiton.jaxx.runtime.swing.nav.NavNode;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * Handler of all uis in the Nave demo.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.2
 */
public class NavDemoHandler implements UIHandler<NavDemo> {

    private static final Log log = LogFactory.getLog(NavDemoHandler.class);

    protected NavDemo ui;

    public static final String MOVIES_CATEGORY_NODE = n("jaxxdemo.common.movies");

    public static final String ACTORS_CATEGORY_NODE = n("jaxxdemo.common.actors");

    @Override
    public void beforeInit(NavDemo ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(final NavDemo ui) {
        // share in context

        ui.setContextValue(this);
        ui.setContextValue(ui.getTreeHelper(), "treeHelper");
        ui.setContextValue(ui.getTreeTableHelper(), "treeTableHelper");

        // Creation of selection listener to open ui when tree selection change
        TreeSelectionListener treeSelectionListener = event -> {
            TreePath path = event.getPath();
            NavDemoTreeNode demoNode =
                    (NavDemoTreeNode) path.getLastPathComponent();

            if (log.isDebugEnabled()) {
                log.debug("Select demoNode " + demoNode);
            }

            // Do nothing for root
            if (demoNode.isRoot()) {
                return;
            }
            openUI(demoNode);
        };

        // Creation of selection listener to open ui when tree selection change
        TreeSelectionListener treeTableSelectionListener = event -> {
            TreePath path = event.getPath();
            NavDemoTreeTableNode demoNode =
                    (NavDemoTreeTableNode) path.getLastPathComponent();

            if (log.isDebugEnabled()) {
                log.debug("Select demoNode " + demoNode);
            }

            // Do nothing for root
            if (demoNode.isRoot()) {
                return;
            }
            openUI(demoNode);
        };

        final JTree tree = ui.getNavigationTree();
        JXTreeTable table = ui.getNavigationTreeTable();

        // Attach renderer
        NavDemoTreeCellRenderer renderer = new NavDemoTreeCellRenderer(
                ui.getContextValue(DecoratorProvider.class),
                ui.getDataProvider()
        );
        tree.setCellRenderer(renderer);

        // Register tree
        ui.getTreeHelper().setUI(tree, true, treeSelectionListener);

        // Register tree table
        ui.getTreeTableHelper().setUI(table, true, treeTableSelectionListener);

        SwingUtilities.invokeLater(() -> {
            tree.setSelectionInterval(0, 0);
            ui.getSplitPane().resetToPreferredSizes();
        });

        // expand the tree
        SwingUtil.expandTree(tree);
        SwingUtil.expandTreeTable(table);

        // auto-expand demoNode when selected
        SwingUtil.addExpandOnClickListener(tree);
        SwingUtil.addExpandOnClickListener(table);
    }

    public void initUI(MoviesContentUI contentUI) {
        DecoratorProvider provider =
                contentUI.getContextValue(DecoratorProvider.class);
        JXPathDecorator<Movie> decorator =
                (JXPathDecorator<Movie>) provider.getDecoratorByType(Movie.class);
        contentUI.getListHeader().init(decorator, new ArrayList<>());
    }

    public void initUI(ActorsContentUI contentUI) {
        DecoratorProvider provider =
                contentUI.getContextValue(DecoratorProvider.class);
        JXPathDecorator<People> decorator =
                (JXPathDecorator<People>) provider.getDecoratorByType(People.class);
        contentUI.getListHeader().init(decorator, new ArrayList<>());
    }

    public void selectChild(AbstractContentUI<?> contentUI,
                            AbstractDemoBean selected) {
        contentUI.getTreeHelper().selectNode(selected.getId());
        contentUI.getTreeTableHelper().selectNode(selected.getId());
    }

    public String getContent(AbstractDemoBean data) {
        if (data == null) {
            return "no content";
        }
        return String.valueOf(data);
    }

    public ImageIcon getImage(AbstractDemoBean data) {
        return data == null ? null : SwingUtil.createIcon(data.getImage());
    }

    protected void openUI(NavNode<?, ?> demoNode) {

        // Get demoNode type
        Class<?> editType = demoNode.getInternalClass();
        String id = demoNode.getId();

        // If it's category demoNode
        DemoDataProvider provider = ui.getDataProvider();
        if (editType.equals(String.class)) {

            // Actors categorie demoNode
            if (ACTORS_CATEGORY_NODE.equals(id)) {

                List<People> peoples = provider.getPeoples();
                showUI(peoples, ActorsContentUI.class);

                // Movies categorie demoNode
            } else if (MOVIES_CATEGORY_NODE.equals(id)) {

                List<Movie> movies = provider.getMovies();
                showUI(movies, MoviesContentUI.class);
            }

            // People demoNode
        } else if (editType.equals(People.class)) {
            People people = provider.getPeople(id);
            showUI(people, ActorContentUI.class);

            // Movie demoNode
        } else if (editType.equals(Movie.class)) {
            Movie movie = provider.getMovie(id);
            showUI(movie, MovieContentUI.class);
        }
    }

    // Create by introspection content ui

    protected <B> void showUI(B bean,
                              Class<? extends AbstractContentUI<B>> uiClass) {

        // Verify if instance is existing
        AbstractContentUI<B> ui = getContentIfExist(uiClass);

        // Get layout identifier
        String contentName = uiClass.getName();

        JPanel content = this.ui.getContent();
        if (ui == null) {
            try {
                // Get constructor
                Constructor<? extends AbstractContentUI<B>> constructor =
                        uiClass.getConstructor(JAXXContext.class);

                // Invoke instance creation
                ui = constructor.newInstance(this.ui);
            } catch (Exception eee) {
                log.error("Could not create ui of type " + uiClass, eee);
                ErrorDialogUI.showError(eee);
                return;
            }

            // Add to content panel
            content.add(ui, contentName);
        }

        // Attach bean
        ui.setData(bean);

        // show ui
        this.ui.getContentLayout().show(content, contentName);

        // revalidate container
        SwingUtilities.invokeLater(revalidateContent);

    }

    // Get content if exist in content, else return null

    protected <E extends Component> E getContentIfExist(Class<E> uiClass) {
        String contentName = uiClass.getName();
        if (log.isDebugEnabled()) {
            log.debug("Get content if exist " + contentName);
        }
        CardLayout2 layout2 = ui.getContentLayout();
        if (!layout2.contains(contentName)) {
            return null;
        }
        return (E) layout2.getComponent(ui.getContent(), contentName);
    }

    private final Runnable revalidateContent = new Runnable() {

        @Override
        public void run() {
            ui.revalidate();
        }
    };

}
