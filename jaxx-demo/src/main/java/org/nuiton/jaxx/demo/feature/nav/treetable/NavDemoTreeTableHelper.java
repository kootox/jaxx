/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.treetable;

import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.feature.nav.NavDemoHandler;
import org.nuiton.jaxx.runtime.swing.nav.treetable.NavTreeTableHelper;
import org.nuiton.jaxx.runtime.swing.nav.treetable.NavTreeTableModel;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class NavDemoTreeTableHelper extends NavTreeTableHelper<NavDemoTreeTableNode> {

    public NavDemoTreeTableHelper(DemoDataProvider provider) {
        setDataProvider(provider);
    }

    @Override
    public DemoDataProvider getDataProvider() {
        return (DemoDataProvider) super.getDataProvider();
    }


    public NavTreeTableModel createModel() {

        // Create root static node
        NavDemoTreeTableNode root = new NavDemoTreeTableNode(
                String.class,
                "Root node",
                null,
                null
        );

        // Create movies category node
        NavDemoTreeTableNode moviesCategoryNode = new NavDemoTreeTableNode(
                String.class,
                NavDemoHandler.MOVIES_CATEGORY_NODE,
                null,
                getChildLoador(MoviesTreeTableNodeLoador.class)
        );

        // Create peoples category node
        NavDemoTreeTableNode peoplesCategoryNode = new NavDemoTreeTableNode(
                String.class,
                NavDemoHandler.ACTORS_CATEGORY_NODE,
                null,
                getChildLoador(ActorsTreeTableNodeLoador.class)
        );

        // Add to root
        root.add(moviesCategoryNode);
        root.add(peoplesCategoryNode);

        // Create model
        NavDemoTreeTableModel delegate = new NavDemoTreeTableModel(getDataProvider());
        NavTreeTableModel model = createModel(root, delegate);

        // Populate childs nodes
        root.populateChilds(getBridge(), getDataProvider());

        return model;
    }
}
