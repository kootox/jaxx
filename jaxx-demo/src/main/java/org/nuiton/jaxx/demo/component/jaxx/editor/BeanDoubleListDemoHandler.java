/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nuiton.jaxx.demo.component.jaxx.editor;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.DemoDecoratorProvider;
import org.nuiton.jaxx.demo.entities.Movie;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.util.List;

/** @author kmorin */
public class BeanDoubleListDemoHandler implements UIHandler<BeanDoubleListDemo> {

    private BeanDoubleListDemo ui;

    @Override
    public void beforeInit(BeanDoubleListDemo ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(BeanDoubleListDemo ui) {
        DemoDataProvider demoDataProvider = new DemoDataProvider();
        List<People> data = demoDataProvider.getPeoples();
        JXPathDecorator<People> decorator = (JXPathDecorator<People>) new DemoDecoratorProvider().getDecoratorByType(People.class);
        ui.getDoubleList().init(decorator, data, data.subList(0, 1));

        Movie nacho = demoDataProvider.getMovie("0");
        Predicate<People> filter = input -> nacho.getActors().contains(input);
        ui.getFilterOnNachoButton().addChangeListener(e -> {
            if (BeanDoubleListDemoHandler.this.ui.getFilterOnNachoButton().isSelected()) {
                BeanDoubleListDemoHandler.this.ui.getDoubleList().getHandler().addFilter(filter);

            } else {
                BeanDoubleListDemoHandler.this.ui.getDoubleList().getHandler().clearFilters();
            }
        });
    }
}
