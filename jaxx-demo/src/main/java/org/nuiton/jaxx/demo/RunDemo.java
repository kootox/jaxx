/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorProvider;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.jaxx.demo.entities.DemoDecoratorProvider;
import org.nuiton.jaxx.demo.tree.DemoDataProvider;
import org.nuiton.jaxx.demo.tree.DemoTreeHelper;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;
import org.nuiton.util.StringUtil;

import java.util.Arrays;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/** @author Tony Chemit - dev@tchemit.fr */
public class RunDemo {

    /** Logger */
    private static final Log log = LogFactory.getLog(RunDemo.class);

    public static void main(String... args) {

        long startingTime = System.nanoTime();
        log.info("JAXX Demo start at " + new Date() + " args: " + Arrays.toString(args));

        try {

            // init root context
            // to enable javassist on webstart, must remove any securityManager,
            // see if this can be dangerous (should not be since jnlp is signed ?)
            // moreover it speeds up the loading :)
            System.setSecurityManager(null);

            long t0 = System.nanoTime();

            I18n.init(new DefaultI18nInitializer("jaxx-demo-i18n"), null);

            Runtime.getRuntime().addShutdownHook(new ShutdownHook());

            // init config
            DemoConfig config = new DemoConfig(args);

            // init root context
            DemoApplicationContext rootContext = DemoApplicationContext.init(config);

            // share a tree helper
            rootContext.setContextValue(new DemoTreeHelper(new DemoDataProvider()));

            // share the config
            rootContext.setContextValue(config);

            // init decorator provider
            DecoratorProvider p = new DemoDecoratorProvider();

            // share the decorator provider
            rootContext.setContextValue(p);

            long t00 = System.nanoTime();

            // init i18n
            I18n.setDefaultLocale(config.getLocale());

            log.info("language : " + config.getLocale());

            if (log.isDebugEnabled()) {
                log.debug("i18n loading time : " + StringUtil.convertTime(t00, System.nanoTime()));
            }

            log.info(t("jaxxdemo.message.config.loaded", config.getVersion()));

            // prepare ui look&feel and load ui properties
            try {
                SwingUtil.initNimbusLoookAndFeel();
            } catch (Exception e) {
                // could not find nimbus look-and-feel
                log.warn(t("jaxxdemo.warning.nimbus.landf"));
            } catch (Throwable e) {
                log.warn(t("jaxxdemo.warning.no.ui"));

            }

            if (log.isDebugEnabled()) {
                log.debug("init done in " + StringUtil.convertTime(t0, System.nanoTime()));
            }

            log.info(t("jaxxdemo.init.context.done", StringUtil.convertTime(startingTime, System.nanoTime())));

            // on affiche l'ui principale
            DemoUIHandler handler = new DemoUIHandler();

            DemoUI ui = handler.initUI(rootContext, config);

            log.info(t("jaxxdemo.init.ui.done"));

            DemoApplicationContext.MAIN_UI.setContextValue(rootContext, ui);

            handler.displayUI(ui, config.getDemoPath().split("/"));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            ErrorDialogUI.showError(e);
            System.exit(1);
        }
    }

    protected static class ShutdownHook extends Thread {

        public ShutdownHook() {
            super("shutdown JAXXDemo");
        }

        @Override
        public void run() {
            try {
                super.run();

                // force to kill main thread

                log.info(t("jaxxdemo.init.closed", new Date()));
                Runtime.getRuntime().halt(0);
            } catch (Exception ex) {
                log.error("error while closing " + ex.getMessage(), ex);
                Runtime.getRuntime().halt(1);
            }
        }
    }
}
