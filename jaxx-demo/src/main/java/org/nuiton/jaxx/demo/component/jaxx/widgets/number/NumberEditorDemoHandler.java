package org.nuiton.jaxx.demo.component.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.spi.UIHandler;

/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class NumberEditorDemoHandler implements UIHandler<NumberEditorDemo> {

    @Override
    public void beforeInit(NumberEditorDemo ui) {

        NumberEditorDemoModel model = new NumberEditorDemoModel();
        model.setIntegerNumber(10);
        model.setFloatNumber(-10.10f);
        model.setDoubleNumber(-0.0001d);

        ui.setContextValue(model);

    }

    @Override
    public void afterInit(NumberEditorDemo ui) {

        ui.integerEditor.init();
        ui.floatEditor.init();
        ui.doubleEditor.init();

    }

}
