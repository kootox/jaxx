/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.feature.nav.treetable;

import org.nuiton.jaxx.demo.entities.DemoDataProvider;
import org.nuiton.jaxx.demo.entities.People;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;
import org.nuiton.jaxx.runtime.swing.nav.treetable.NavTreeTableNodeChildLoador;

import java.util.List;

/**
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class ActorsTreeTableNodeLoador extends NavTreeTableNodeChildLoador<People, People, NavDemoTreeTableNode> {

    private static final long serialVersionUID = 1L;

    public ActorsTreeTableNodeLoador() {
        super(People.class);
    }

    @Override
    public List<People> getData(Class<?> parentClass,
                                String moviesId,
                                NavDataProvider dataProvider) {

        // Get people for parentId
        DemoDataProvider provider = (DemoDataProvider) dataProvider;

        // If its not root
        if (moviesId != null) {

            // Return peoples for movies id
            return provider.getPeoples(moviesId);
        }

        // Return all peoples
        return provider.getPeoples();
    }

    @Override
    public NavDemoTreeTableNode createNode(People data, NavDataProvider dataProvider) {

        NavDemoTreeTableNode actorNode;

        // Create actor static nodes

        actorNode = new NavDemoTreeTableNode(
                getBeanType(),
                data.getId(),
                null,
                null
        );

        return actorNode;
    }
}
