package org.nuiton.jaxx.demo.component.jaxx.widgets.number;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.17
 */
public class NumberEditorDemoModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected Integer integerNumber;

    protected Float floatNumber;

    protected Double doubleNumber;

    public Integer getIntegerNumber() {
        return integerNumber;
    }

    public void setIntegerNumber(Integer integerNumber) {
        Number oldValue = getIntegerNumber();
        this.integerNumber = integerNumber;
        firePropertyChange("integerNumber", oldValue, integerNumber);
    }

    public Float getFloatNumber() {
        return floatNumber;
    }

    public void setFloatNumber(Float floatNumber) {
        Number oldValue = getFloatNumber();
        this.floatNumber = floatNumber;
        firePropertyChange("floatNumber", oldValue, floatNumber);
    }

    public Double getDoubleNumber() {
        return doubleNumber;
    }

    public void setDoubleNumber(Double doubleNumber) {
        Number oldValue = getDoubleNumber();
        this.doubleNumber = doubleNumber;
        firePropertyChange("doubleNumber", oldValue, doubleNumber);
    }
}
