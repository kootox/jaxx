package org.nuiton.jaxx.demo.component.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Date;

/**
 * Created on 9/10/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateTimeEditorDemoModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    protected Date date;

    protected Date dayDate;

    protected Date timeDate;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Date oldValue = getDate();
        this.date = date;
        firePropertyChange("date", oldValue, date);
    }

    public Date getDayDate() {
        return dayDate;
    }

    public void setDayDate(Date dayDate) {
        Date oldValue = getDayDate();
        this.dayDate = dayDate;
        firePropertyChange("dayDate", oldValue, dayDate);
    }

    public Date getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(Date timeDate) {
        Date oldValue = getTimeDate();
        this.timeDate = timeDate;
        firePropertyChange("timeDate", oldValue, timeDate);
    }
}
