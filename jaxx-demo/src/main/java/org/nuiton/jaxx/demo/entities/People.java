/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.demo.entities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.7.2
 */
public class People extends AbstractDemoBean {

    static {
        n("jaxxdemo.common.people");
        n("jaxxdemo.common.firstName");
        n("jaxxdemo.common.lastName");
        n("jaxxdemo.common.age");
    }

    public static final String PROPERTY_FIRST_NAME = "firstName";

    public static final String PROPERTY_LAST_NAME = "lastName";

    public static final String PROPERTY_AGE = "age";

    protected String firstName;

    protected String lastName;

    protected int age;

    private static final long serialVersionUID = 1L;

    public People() {
    }

    public People(String id,
                  String firstName,
                  String lastName,
                  int age,
                  String image) {
        super(id, image);
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getImage() {
        return image;
    }

    public void setAge(int age) {
        int old = this.age;
        this.age = age;
        firePropertyChange(PROPERTY_AGE, old, age);
    }

    public void setFirstName(String firstName) {
        String old = this.firstName;
        this.firstName = firstName;
        firePropertyChange(PROPERTY_FIRST_NAME, old, firstName);
    }

    public void setLastName(String lastName) {
        String old = this.lastName;
        this.lastName = lastName;
        firePropertyChange(PROPERTY_LAST_NAME, old, lastName);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        People other = (People) obj;
        return !(id == null ? other.id != null : !id.equals(other.id));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        ToStringBuilder b = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        b.append(PROPERTY_ID, id);
        b.append(PROPERTY_FIRST_NAME, firstName);
        b.append(PROPERTY_LAST_NAME, lastName);
        b.append(PROPERTY_IMAGE, image);
        b.append(PROPERTY_AGE, age);
        return b.toString();
    }
}
