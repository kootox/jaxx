/*
 * #%L
 * JAXX :: Runtime Swing Application
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.application;

import java.util.Objects;
import java.util.concurrent.Callable;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import io.ultreia.java4all.lang.Strings;

/**
 * Action worker to execute a incoming action.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class ActionWorker<R, P> extends SwingWorker<R, P> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ActionWorker.class);

    protected final String actionLabel;

    protected Callable<R> target;

    protected ActionStatus status;

    protected Exception error;

    protected long startTime;

    protected long endTime;

    public ActionWorker(String actionLabel) {
        this.actionLabel = actionLabel;
    }

    public ActionWorker(String actionLabel, Runnable target) {
        this.target = new RunnableBridge<>(target);
        this.actionLabel = actionLabel;
    }

    public Callable<R> getTarget() {
        return target;
    }

    public void setTarget(Runnable target) {
        setTarget(new RunnableBridge<>(target));
    }

    /**
     * Set the target to execute.
     *
     * @param target the target to execute
     * @since 2.5.1
     */
    public void setTarget(Callable<R> target) {
        this.target = target;
    }

    @Override
    protected R doInBackground() {
        Objects.requireNonNull(target != null, "Targe field can not be null here.");
        startTime = System.nanoTime();
        if (log.isDebugEnabled()) {
            log.debug("Action [" + getActionLabel() + "] is starting...");
        }
        R result = null;
        try {

            result = getTarget().call();
        } catch (Exception e) {
            error = e;
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Action [" + getActionLabel() + "] is ending...");
            }
        }
        return result;
    }

    public boolean isFailed() {
        return (isDone() || isCancelled()) && error != null;
    }

    public Exception getError() {
        return error;
    }

    public ActionStatus getStatus() {
        return status;
    }

    public String getActionLabel() {
        return actionLabel;
    }

    @Override
    protected void done() {
        super.done();
        endTime = System.nanoTime();
        if (error != null) {
            status = ActionStatus.FAIL;
        } else if (isCancelled()) {
            status = ActionStatus.CANCEL;
        } else {
            status = ActionStatus.OK;
        }
        if (log.isDebugEnabled()) {
            log.debug("Action [" + getActionLabel() + "] ends with status : " + status + " in " + getTime());
        }
    }

    public String getTime() {
        return Strings.convertTime(endTime - startTime);
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    /** State of a running action */
    public enum ActionStatus {
        OK,
        CANCEL,
        FAIL
    }

    /**
     * Transform a {@link Runnable} into a {@link Callable}.
     *
     * @param <R> type of return (used to maintain generic checks in this class).
     * @since 2.5.1
     */
    private static class RunnableBridge<R> implements Callable<R> {

        private final Runnable target;

        public RunnableBridge(Runnable target) {
            this.target = target;
        }

        @Override
        public R call() {
            target.run();
            return null;
        }
    }

}
