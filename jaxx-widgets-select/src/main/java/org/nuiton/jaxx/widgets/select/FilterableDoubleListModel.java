package org.nuiton.jaxx.widgets.select;

/*
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.runtime.swing.model.JaxxDefaultListModel;
import org.nuiton.jaxx.runtime.swing.model.JaxxFilterableListModel;
import org.nuiton.jaxx.widgets.ModelToBean;

/**
 * Created on 11/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class FilterableDoubleListModel<O> extends AbstractSerializableBean implements ModelToBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_ADD_ENABLED = "addEnabled";

    public static final String PROPERTY_REMOVE_ENABLED = "removeEnabled";

    public static final String PROPERTY_SELECTED_UP_ENABLED = "selectedUpEnabled";

    public static final String PROPERTY_SELECTED_DOWN_ENABLED = "selectedDownEnabled";

    public static final String PROPERTY_BEAN = "bean";

    public static final String PROPERTY_SELECTED = "selected";

    /**
     * Can use select action ?
     */
    protected boolean addEnabled;

    /**
     * Can use unselect action ?
     */
    protected boolean removeEnabled;

    /**
     * Can use select up action ?
     */
    protected boolean selectedUpEnabled;

    /**
     * Can use select down action ?
     */
    protected boolean selectedDownEnabled;

    /**
     * Optional bean where to push data.
     */
    protected Object bean;

    /**
     * Internal flag to avoid reentrant code while firing some events.
     */
    protected boolean objectIsAdjusting;

    /**
     * Universe of items useables in not-selected and selected lists.
     */
    protected final List<O> universe = new ArrayList<>();

    /**
     * Model containing the remaining available items.
     */
    protected final JaxxFilterableListModel<O> universeModel = new JaxxFilterableListModel<>();

    /**
     * Model containing the selected items.
     */
    protected final JaxxDefaultListModel<O> selectedModel = new JaxxDefaultListModel<>();

    private final FilterableDoubleListConfig<O> config;

    public FilterableDoubleListModel(FilterableDoubleListConfig<O> config) {
        this.config = config;
        this.selectedModel.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {
                fireSelectedChange();
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                fireSelectedChange();
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                fireSelectedChange();
            }
        });
    }

    private void fireSelectedChange() {

        if (!selectedModel.isValueIsAdjusting()) {

            // only fire when not adjusting model
            List<O> selected = getSelected();
            firePropertyChange(PROPERTY_SELECTED, null /* To force event propagation */, selected);
        }

    }

    public JaxxDefaultListModel<O> getSelectedModel() {
        return selectedModel;
    }

    public JaxxFilterableListModel<O> getUniverseModel() {
        return universeModel;
    }

    //------------------------------------------------------------------------//
    //-- Config delegate methods ---------------------------------------------//
    //------------------------------------------------------------------------//

    public FilterableDoubleListConfig<O> getConfig() {
        return config;
    }

    public Class<O> getBeanType() {
        return config.getBeanType();
    }

    public boolean isUseMultiSelect() {
        return config.isUseMultiSelect();
    }

    public String getProperty() {
        return config.getProperty();
    }

    public String getI18nPrefix() {
        return config.getI18nPrefix();
    }

    //------------------------------------------------------------------------//
    //-- Universe - Selected methods -----------------------------------------//
    //------------------------------------------------------------------------//

    public List<O> getSelected() {

        return selectedModel.toList();

    }

    public int getSelectedListSize() {
        return selectedModel.size();
    }

    public void setUniverse(Collection<O> universe) {

        resetUniverse();

        this.universe.clear();
        if (universe != null) {
            this.universe.addAll(universe);
            resetUniverse();
        }

    }

    public void setSelected(Collection<O> selected) {

        setValueIsAdjustingToTrue();

        try {

            resetUniverse();

            selectedModel.clear();

            if (selected != null) {

                addToSelected(selected);

            }

        } finally {

            setValueIsAdjustingToFalse(true);

        }

    }

    public void addToSelected(Iterable<O> items) {

        setValueIsAdjustingToTrue();

        try {
            for (O item : items) {

                addToSelected(item);

            }
        } finally {

            setValueIsAdjustingToFalse(isUseMultiSelect());

        }

    }

    public void removeFromSelected(Iterable<O> items) {

        setValueIsAdjustingToTrue();

        try {
            for (O item : items) {

                removeFromSelected(item);

            }
        } finally {

            setValueIsAdjustingToFalse(isUseMultiSelect());

        }

    }

    /**
     * Move up a selected item.
     *
     * @param item the selected item
     */
    public void moveUpSelected(O item) {

        int i = selectedModel.indexOf(item);
        selectedModel.removeElement(item);
        selectedModel.insertElementAt(item, i - 1);

    }

    /**
     * Move down a selected item.
     *
     * @param item the selected item
     */
    public void moveDownSelected(O item) {

        int i = selectedModel.indexOf(item);
        selectedModel.removeElement(item);
        selectedModel.insertElementAt(item, i + 1);

    }

    //------------------------------------------------------------------------//
    //-- Bean property methods -----------------------------------------------//
    //------------------------------------------------------------------------//

    public boolean isAddEnabled() {
        return addEnabled;
    }

    public void setAddEnabled(boolean addEnabled) {
        boolean oldValue = isAddEnabled();
        this.addEnabled = addEnabled;
        firePropertyChange(PROPERTY_ADD_ENABLED, oldValue, addEnabled);
    }

    public boolean isRemoveEnabled() {
        return removeEnabled;
    }

    public void setRemoveEnabled(boolean removeEnabled) {
        boolean oldValue = isRemoveEnabled();
        this.removeEnabled = removeEnabled;
        firePropertyChange(PROPERTY_REMOVE_ENABLED, oldValue, removeEnabled);
    }

    public boolean isSelectedUpEnabled() {
        return selectedUpEnabled;
    }

    public void setSelectedUpEnabled(boolean selectedUpEnabled) {
        boolean oldValue = isSelectedUpEnabled();
        this.selectedUpEnabled = selectedUpEnabled;
        firePropertyChange(PROPERTY_SELECTED_UP_ENABLED, oldValue, selectedUpEnabled);
    }

    public boolean isSelectedDownEnabled() {
        return selectedDownEnabled;
    }

    public void setSelectedDownEnabled(boolean selectedDownEnabled) {
        boolean oldValue = isSelectedDownEnabled();
        this.selectedDownEnabled = selectedDownEnabled;
        firePropertyChange(PROPERTY_SELECTED_DOWN_ENABLED, oldValue, selectedDownEnabled);
    }

    @Override
    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Object oldValue = getBean();
        this.bean = bean;
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    private void resetUniverse() {

        universeModel.setAllElements(universe);

    }

    private void addToSelected(O item) {

        selectedModel.addElement(item);
        if (!isUseMultiSelect()) {
            // remove from universe list
            universeModel.removeElement(item);
        }

    }

    private void removeFromSelected(O item) {

        selectedModel.removeElement(item);
        if (!isUseMultiSelect()) {
            // add to universe list
            universeModel.addElement(item);
        }

    }

    int universeSize;

    int selectedSize;

    private void setValueIsAdjustingToTrue() {

        universeSize = universeModel.size();
        selectedSize = selectedModel.size();

        universeModel.setValueIsAdjusting(true);
        selectedModel.setValueIsAdjusting(true);
    }

    private void setValueIsAdjustingToFalse(boolean updateUniverse) {

        universeModel.setValueIsAdjusting(false);
        selectedModel.setValueIsAdjusting(false);

        if (updateUniverse) {

            universeModel.refresh();

        }

        int currentSelectedSize = selectedModel.size();

        if (currentSelectedSize <= selectedSize) {

            selectedModel.refresh(selectedSize);

        } else {

            selectedModel.refresh();

        }

    }
}
