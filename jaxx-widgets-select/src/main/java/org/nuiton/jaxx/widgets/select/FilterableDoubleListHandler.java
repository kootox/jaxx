package org.nuiton.jaxx.widgets.select;

/*
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.decorator.MultiJXPathDecorator;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.model.JaxxFilterableListModel;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.nuiton.jaxx.runtime.swing.renderer.FilteredDecoratorListCellRenderer;
import org.nuiton.jaxx.widgets.MutateOnConditionalPropertyChangeListener;

/**
 * Created on 11/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class FilterableDoubleListHandler<O> implements UIHandler<FilterableDoubleList<O>> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FilterableDoubleListHandler.class);

    private FilterableDoubleList<O> ui;

    /** the decorator of data */
    private MultiJXPathDecorator<O> decorator;

    private I18nLabelsBuilder i18nLabelBuilder;

    public void setI18nLabelBuilder(I18nLabelsBuilder i18nLabelBuilder) {
        this.i18nLabelBuilder = i18nLabelBuilder;
    }

    private final BeanUIUtil.PopupHandler<O> popupHandler = new BeanUIUtil.PopupHandler<O>() {

        @Override
        public I18nLabelsBuilder getI18nLabelsBuilder() {

            if (i18nLabelBuilder == null) {
                i18nLabelBuilder = new I18nLabelsBuilder(ui.getBeanType());
            }
            return i18nLabelBuilder;
        }

        @Override
        public JPopupMenu getPopup() {
            return ui.getPopup();
        }

        @Override
        public JComponent getInvoker() {
            return ui.getChangeDecorator();
        }
    };

    @Override
    public void beforeInit(FilterableDoubleList<O> ui) {

        this.ui = ui;
        FilterableDoubleListConfig<O> config = new FilterableDoubleListConfig<>();
        FilterableDoubleListModel<O> model = new FilterableDoubleListModel<>(config);
        ui.setContextValue(model);

    }

    @Override
    public void afterInit(FilterableDoubleList<O> ui) {
        // nothing to do here, everything is done in init method
    }

    //------------------------------------------------------------------------//
    //-- Filter methods ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public void addFilter(Predicate<O> filter) {
        ui.getModel().getUniverseModel().addFilter(filter);
    }

    public void removeFilter(Predicate<O> filter) {
        ui.getModel().getUniverseModel().removeFilter(filter);
    }

    public void clearFilters() {
        ui.getModel().getUniverseModel().clearFilters();
    }

    public void refreshFilteredElements() {
        ui.getModel().getUniverseModel().refreshFilteredElements();
    }

    //------------------------------------------------------------------------//
    //-- Public methods ------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * When universe list was double clicked, move selected items to selected list.
     *
     * @param event mouse event
     */
    public void onUniverseListClicked(MouseEvent event) {

        JList universeList = ui.getUniverseList();

        if (event.getClickCount() == 2) {
            int index = universeList.locationToIndex(event.getPoint());
            if (index < 0) {
                return;
            }

            O item = ui.getModel().getUniverseModel().getElementAt(index);
            List<O> items = new ArrayList<>();
            items.add(item);
            ui.getModel().addToSelected(items);

//            sortData();
        }

    }

    /**
     * When selected list was double clicked, move selected items to universe list.
     *
     * @param event mouse event
     */
    public void onSelectedListClicked(MouseEvent event) {

        JList selectedList = ui.getSelectedList();

        if (event.getClickCount() == 2) {
            int index = selectedList.locationToIndex(event.getPoint());
            if (index < 0) {
                return;
            }

            List<O> items = new ArrayList<>();
            items.add(ui.getModel().getSelectedModel().getElementAt(index));
            ui.getModel().removeFromSelected(items);

            sortData();
        }

    }

    /** When add button was hit, move selected items (from universe list) to selected list. */
    public void select() {

        List<O> selectedValues = ui.getUniverseList().getSelectedValuesList();
        ui.getModel().addToSelected(selectedValues);
        ui.getUniverseList().clearSelection();

//        sortData();

    }

    /** When remove button was hit, move selected items (from selected list) to universe list. */
    public void unselect() {

        List<O> selectedValues = ui.getSelectedList().getSelectedValuesList();
        ui.getModel().removeFromSelected(selectedValues);
        ui.getSelectedList().clearSelection();

        sortData();

    }

    public void onKeyPressedOnUniverseList(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int lastIndice = ui.getUniverseList().getSelectionModel().getLeadSelectionIndex();
            select();
            JList source = (JList) e.getSource();
            if (source.getModel().getSize() > 0) {
                source.setSelectedIndex(Math.min(lastIndice, source.getModel().getSize() - 1));
            } else {
                ui.getSelectedList().requestFocus();
            }
        }

    }

    public void onKeyPressedOnSelectedList(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            int lastIndice = ui.getSelectedList().getSelectionModel().getLeadSelectionIndex();
            unselect();
            JList source = (JList) e.getSource();
            if (source.getModel().getSize() > 0) {
                source.setSelectedIndex(Math.min(lastIndice, source.getModel().getSize() - 1));
                ui.getSelectedList().requestFocus();
            }
        }

    }

    public void selectFirstRowIfNoSelection(FocusEvent event) {

        JList list = (JList) event.getSource();
        if (list.isSelectionEmpty()) {
            list.setSelectedIndex(0);
        }

    }

    /**
     * Move up a selected item.
     *
     * @param item the selected item
     * @since 2.5.26
     */
    public void moveUpSelected(O item) {

        ui.getModel().moveUpSelected(item);
        ui.getSelectedList().setSelectedValue(item, true);

    }

    /**
     * Move down a selected item.
     *
     * @param item the selected item
     * @since 2.5.26
     */
    public void moveDownSelected(O item) {

        ui.getModel().moveDownSelected(item);
        ui.getSelectedList().setSelectedValue(item, true);

    }

    public JPopupMenu getSelectedListPopup(boolean showIt) {
        JPopupMenu result;
        if (showIt) {
            result = ui.getSelectedListPopup();
        } else {
            result = null;
        }
        return result;
    }

    /** Toggle the popup visible state. */
    public void togglePopup() {
        popupHandler.togglePopup();
    }

    //------------------------------------------------------------------------//
    //-- Init methods --------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * Initializes the handler of the UI
     *
     * @param decorator the decorator to use to display the data nicely
     * @param universe  the list of all the available items
     * @param selected  the list of selected items
     */
    public void init(JXPathDecorator<O> decorator, List<O> universe, List<O> selected) {
        init(decorator, decorator, universe, selected);
    }

    /**
     * Initializes the handler of the UI
     *
     * @param decorator  the decorator to use to display the data nicely
     * @param decorator2 the selected decorator to use to display the selected data nicely (if none, then reuse the first one)
     * @param universe   the list of all the available items
     * @param selected   the list of selected items
     */
    public void init(JXPathDecorator<O> decorator, JXPathDecorator<O> decorator2, List<O> universe, List<O> selected) {

        if (decorator == null) {
            throw new NullPointerException("decorator can not be null (for type " + ui.getBeanType() + ")");
        }

        this.decorator = DecoratorUtil.cloneDecorator(decorator);

        FilterableDoubleListModel<O> model = ui.getModel();

        FilterableDoubleListConfig<O> config = model.getConfig();

        Object bean = model.getBean();

        if (bean != null) {

            String property = config.getProperty();

            if (property != null) {

                Method mutator = Setters.getMutator(bean, property);

                // check mutator exists
                Objects.requireNonNull(mutator, "could not find mutator for " + property);

                // When selected list changed, let's push it back in bean
                model.addPropertyChangeListener(
                        FilterableDoubleListModel.PROPERTY_SELECTED,
                        new MutateOnConditionalPropertyChangeListener<FilterableDoubleListModel>(model, mutator, filterableDoubleListModel -> true));

            }
        }

        if (config.isShowListLabel()) {

            JLabel universeListHeader = new JLabel();
            universeListHeader.setText(config.getUniverseLabel());
            ui.getUniverseListPane().setColumnHeaderView(universeListHeader);

            JLabel selectedListHeader = new JLabel();
            selectedListHeader.setText(config.getSelectedLabel());
            ui.getSelectedListPane().setColumnHeaderView(selectedListHeader);

        }

        ui.getModel().setUniverse(universe);
        ui.getModel().setSelected(selected);

        {
            // Init universe list
            JList<O> universeList = ui.getUniverseList();
            FilteredDecoratorListCellRenderer universeListCellRenderer = new FilteredDecoratorListCellRenderer(this.decorator);
            universeList.setCellRenderer(universeListCellRenderer);
            // When universe list selection model changed, update the add button enabled property
            universeList.getSelectionModel().addListSelectionListener(e -> {
                ListSelectionModel source = (ListSelectionModel) e.getSource();
                ui.getModel().setAddEnabled(!source.isSelectionEmpty());
            });

            final JaxxFilterableListModel<O> filterModel = ui.getModel().getUniverseModel();

            filterModel.setDecorator(this.decorator);

            ui.getFilterField().getDocument().addDocumentListener(new DocumentListener() {

                @Override
                public void insertUpdate(DocumentEvent e) {
                    String text = ui.getFilterField().getText();
                    universeListCellRenderer.setFilterText(text);
                    filterModel.setFilterText(text);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    String text = ui.getFilterField().getText();
                    universeListCellRenderer.setFilterText(text);
                    filterModel.setFilterText(text);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    String text = ui.getFilterField().getText();
                    universeListCellRenderer.setFilterText(text);
                    filterModel.setFilterText(text);
                }
            });
        }

        {
            // Init selected list
            JList<O> selectedList = ui.getSelectedList();
            if (decorator2 != null) {
                decorator2 = DecoratorUtil.cloneDecorator(decorator2);
            } else {
                decorator2 = this.decorator;
            }
            selectedList.setCellRenderer(new DecoratorListCellRenderer<>(decorator2));


            // When selected list selection model changed, update the add button enabled property
            selectedList.getSelectionModel().addListSelectionListener(e -> {
                ListSelectionModel source = (ListSelectionModel) e.getSource();
                FilterableDoubleListModel<O> model1 = ui.getModel();
                if (!e.getValueIsAdjusting()) {
                    boolean removeEnabled = !source.isSelectionEmpty();
                    model1.setRemoveEnabled(removeEnabled);

                    int minSelectionIndex = source.getMinSelectionIndex();
                    int maxSelectionIndex = source.getMaxSelectionIndex();

                    boolean oneSelection = minSelectionIndex == maxSelectionIndex;

                    boolean upEnabled = removeEnabled &&
                            oneSelection &&
                            minSelectionIndex > 0;
                    model1.setSelectedUpEnabled(upEnabled);

                    boolean downEnabled = removeEnabled &&
                            oneSelection &&
                            minSelectionIndex + 1 < model1.getSelectedListSize();
                    model1.setSelectedDownEnabled(downEnabled);
                }
            });
        }

        {
            // Init decorator ui
            JAXXButtonGroup indexes = ui.getIndexes();

            // build popup
            popupHandler.preparePopup(null,
                                      null,
                                      config.getI18nPrefix(),
                                      null,
                                      indexes,
                                      ui.getPopupSeparator(),
                                      ui.getPopupLabel(),
                                      ui.getSortUp(),
                                      ui.getSortDown(),
                                      this.decorator);

            // select sort button
            indexes.setSelectedButton(ui.getIndex());

            sortData();

        }

        {

            // Listen ui changes

            ui.addPropertyChangeListener(FilterableDoubleList.PROPERTY_INDEX, evt -> {

                // decorator index has changed, force reload of data in ui
                setIndex((Integer) evt.getOldValue(),
                         (Integer) evt.getNewValue());

            });

            ui.addPropertyChangeListener(FilterableDoubleList.PROPERTY_REVERSE_SORT, evt -> {
                // sort order has changed, force reload of data in ui
                setSortOrder((Boolean) evt.getOldValue(),
                             (Boolean) evt.getNewValue());
            });

            ui.addPropertyChangeListener(FilterableDoubleList.PROPERTY_HIGHLIGHT_FILTER_TEXT, evt -> setHighlightFilterText((Boolean) evt.getNewValue()));
        }

        // force to reload the showSelectPopup binding
        ui.processDataBinding(FilterableDoubleList.BINDING_SELECTED_LIST_COMPONENT_POPUP_MENU);

    }

    //------------------------------------------------------------------------//
    //-- Internal methods ----------------------------------------------------//
    //------------------------------------------------------------------------//

    private void sortData() {

        // just update UI should do the math of this
        updateUI(ui.getIndex(), ui.isReverseSort());

    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setIndex(Integer oldValue, Integer newValue) {
        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }
        AbstractButton button = ui.getIndexes().getButton(newValue);
        if (button != null) {
            button.setSelected(true);
        }
        updateUI(newValue, ui.isReverseSort());
    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */

    protected void setSortOrder(Boolean oldValue, Boolean newValue) {

        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }

        updateUI(ui.getIndex(), newValue);
    }

    protected void setHighlightFilterText(Boolean newValue) {
        FilteredDecoratorListCellRenderer universeListCellRenderer =
                (FilteredDecoratorListCellRenderer) ui.getUniverseList().getCellRenderer();
        universeListCellRenderer.setHighlightFilterText(newValue);
        ui.getUniverseList().repaint();
    }

    protected void updateUI(int index, boolean reversesort) {

        if (decorator == null) {

            // can't come here right now...
            return;
        }

        // change decorator context
        decorator.setContextIndex(index);

        List<O> data = ui.getModel().getUniverseModel().toList();
        try {
            // Sort data with the decorator jxpath tokens.
            DecoratorUtil.sort(decorator,
                               data,
                               index,
                               reversesort
            );

        } catch (Exception eee) {
            log.warn(eee.getMessage(), eee);
        }

        // reload the model
        SwingUtil.fillList(ui.getUniverseList(), data, null);

        ui.getUniverseList().requestFocus();
    }

    public MultiJXPathDecorator<O> getDecorator() {
        return decorator;
    }
}
