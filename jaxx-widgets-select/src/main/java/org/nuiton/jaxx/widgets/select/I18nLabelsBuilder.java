package org.nuiton.jaxx.widgets.select;

/*-
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import org.nuiton.decorator.MultiJXPathDecorator;


import static org.nuiton.i18n.I18n.t;

/**
 * To build a dictionnary of i18n labels used in ui based on a decorator.
 * <p>
 * Created by tchemit on 24/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
public class I18nLabelsBuilder {

    private final Class beanType;

    public I18nLabelsBuilder(Class beanType) {
        this.beanType = beanType;
    }

    public LinkedHashMap<String, String> computeLabels(String i18nPrefix, MultiJXPathDecorator decorator) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        int nbContext = decorator.getNbContext();
        if (nbContext > 1) {
            for (int i = 0; i < nbContext; i++) {
                String property = getI18nKey(beanType,i18nPrefix, i, decorator);
                String propertyI18n = t(property);
                result.put(property, propertyI18n);
            }
        }
        return result;
    }

    protected String getI18nKey(Class beanType, String i18nPrefix, int i, MultiJXPathDecorator decorator) {
        return i18nPrefix + decorator.getProperty(i);
    }

}
