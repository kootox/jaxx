package org.nuiton.jaxx.widgets.select.actions;

/*-
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.select.BeanComboBox;


import static org.nuiton.i18n.I18n.n;

/**
 * Created by tchemit on 13/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BeanComboBoxShowDecoratorAction extends BeanComboBoxActionSupport {

    public BeanComboBoxShowDecoratorAction() {
        super(null, n("beancombobox.action.sort.tip"), "combobox-sort", SwingUtil.findKeyStroke("beancombobox.showDecorator", "Ctrl pressed F"));
    }

    @Override
    protected void doActionPerformed(ActionEvent e, BeanComboBox beanComboBox) {
        ui.getHandler().togglePopup();
    }
}
