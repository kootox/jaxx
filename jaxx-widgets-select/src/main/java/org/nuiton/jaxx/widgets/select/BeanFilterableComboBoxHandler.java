/*
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.select;

import io.ultreia.java4all.lang.Setters;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.decorator.MultiJXPathDecorator;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.model.JaxxFilterableComboBoxModel;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;

/**
 * Le handler d'un {@link BeanFilterableComboBox}.
 * <p>
 * Note: ce handler n'est pas stateless et n'est donc pas partageable entre
 * plusieurs ui.
 *
 * @param <O> le type des objet contenus dans le modèle du composant.
 * @author Kevin Morin - morin@codelutin.com
 * @see BeanFilterableComboBox
 * @since 2.5.12
 */
public class BeanFilterableComboBoxHandler<O> implements PropertyChangeListener, UIHandler<BeanFilterableComboBox<O>> {

    public static final Log log = LogFactory.getLog(BeanFilterableComboBoxHandler.class);

    protected BeanFilterableComboBox<O> ui;

    /** the mutator method on the property of boxed bean in the ui */
    protected Method mutator;

    /** the decorator of data */
    protected MultiJXPathDecorator<O> decorator;

    protected boolean init;

    protected final FocusListener EDITOR_TEXT_COMP0NENT_FOCUSLISTENER = new FocusListener() {
        @Override
        public void focusGained(FocusEvent e) {
            if (log.isDebugEnabled()) {
                log.debug("close popup from " + e);
            }
            ui.getPopup().setVisible(false);
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    };

    protected final DocumentListener EDITOR_TEXT_COMPONENT_DOCUMENTLISTENER = new DocumentListener() {
        public void insertUpdate(DocumentEvent e) {
            updateFilter();
        }

        public void removeUpdate(DocumentEvent e) {
            updateFilter();
        }

        public void changedUpdate(DocumentEvent e) {
            updateFilter();
        }

        protected void updateFilter() {
            JComboBox comboBox = ui.getCombobox();
            JaxxFilterableComboBoxModel model = (JaxxFilterableComboBoxModel) comboBox.getModel();
            JTextComponent editorComponent = (JTextComponent) comboBox.getEditor().getEditorComponent();
            // hide the popup before setting the filter, otherwise the popup height does not fit
            boolean wasPopupVisible = comboBox.isShowing() && comboBox.isPopupVisible();
            if (wasPopupVisible) {
                comboBox.hidePopup();
            }
            String text = editorComponent.getText();
            if (ui.getSelectedItem() != null) {
                text = "";
            }
            if (log.isDebugEnabled()) {
                log.debug("updateFilter " + text);
            }
            model.setFilterText(text);
            if (wasPopupVisible) {
                comboBox.showPopup();
            }
        }

    };

    private I18nLabelsBuilder i18nLabelBuilder;

    public void setI18nLabelBuilder(I18nLabelsBuilder i18nLabelBuilder) {
        this.i18nLabelBuilder = i18nLabelBuilder;
    }

    private final BeanUIUtil.PopupHandler<O> popupHandler = new BeanUIUtil.PopupHandler<O>() {

        @Override
        public I18nLabelsBuilder getI18nLabelsBuilder() {

            if (i18nLabelBuilder == null) {
                i18nLabelBuilder = new I18nLabelsBuilder(getBeanType());
            }
            return i18nLabelBuilder;
        }

        @Override
        public JPopupMenu getPopup() {
            return ui.getPopup();
        }

        @Override
        public JComponent getInvoker() {
            return ui.getChangeDecorator();
        }
    };

    /**
     * Initialise le handler de l'ui
     *
     * @param decorator le decorateur a utiliser
     * @param data      la liste des données a gérer
     */
    public void init(JXPathDecorator<O> decorator, List<O> data) {

        if (init) {
            throw new IllegalStateException("can not init the handler twice");
        }
        init = true;

        if (decorator == null) {
            throw new NullPointerException("decorator can not be null (for type " + ui.getBeanType() + ")");
        }

        JAXXButtonGroup indexes = ui.getIndexes();

        this.decorator = BeanUIUtil.createDecorator(decorator);

        JComboBox<O> combobox = ui.getCombobox();
        JAXXFilterableComboBoxEditor editor =
                new JAXXFilterableComboBoxEditor(ui.getCombobox().getEditor());
        combobox.setEditor(editor);

        editor.getEditorComponent().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (ui.isEnabled()) {
                    combobox.showPopup();
                }
            }

        });
        editor.getEditorComponent().addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (!ui.isEnabled()) {
                    e.consume();
                    return;
                }
                if (log.isDebugEnabled()) {
                    log.debug("keyPressed: " + e.getKeyCode());
                }
                if (combobox.isPopupVisible() && ui.isEnterToSelectUniqueUniverse()) {
                    if (KeyEvent.VK_ENTER == e.getKeyCode() && combobox.getItemCount() == 1) {
                        // we don't want any other action for this one
                        if (log.isDebugEnabled()) {
                            log.debug("ENTER + only one selected item, consume");
                        }
                        combobox.hidePopup();
                        e.consume();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (log.isDebugEnabled()) {
                    log.debug("keyReleased: " + e.getKeyCode());
                }
                if (combobox.isPopupVisible() && KeyEvent.VK_ESCAPE == e.getKeyCode()) {
                    if (log.isDebugEnabled()) {
                        log.debug("ESC , hide popup");
                    }
                    e.consume();
                    combobox.hidePopup();
                    return;
                }
                if (!combobox.isPopupVisible() && KeyEvent.VK_ESCAPE != e.getKeyCode() && KeyEvent.VK_ENTER != e.getKeyCode()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Will show popup, keycode: " + e.getKeyCode());
                    }
                    combobox.showPopup();
                }
                // if the typed text does not match the selected item,
                // set the selected item to null
                Object selectedItem = ui.getSelectedItem();
                String text = editor.getEditorComponent().getText();
                if (log.isDebugEnabled()) {
                    log.debug("keycode: " + e.getKeyCode() + ", editorText: " + text);
                }
                if (KeyEvent.VK_ENTER == e.getKeyCode() && ui.isEnterToSelectUniqueUniverse() && combobox.getItemCount() == 1) {

                    // auto-select the
                    if (log.isDebugEnabled()) {
                        log.debug("Auto-select unique result with *ENTER* key");
                    }
                    combobox.setSelectedIndex(0);
                    e.consume();
                    // edition is done
                    combobox.hidePopup();
                    return;
                }
                final String selectedItemString;
                if (getBeanType().isInstance(selectedItem)) {
                    selectedItemString = BeanFilterableComboBoxHandler.this.decorator.toString(selectedItem);
                } else {
                    selectedItemString = JAXXUtil.getStringValue(selectedItem);
                }
                if (selectedItem == null || !selectedItemString.equals(text)) {
                    unselectItem();
                }
            }

        });

        // init combobox renderer base on given decorator
        combobox.setRenderer(new DecoratorListCellRenderer<>(this.decorator));
        ((JaxxFilterableComboBoxModel<O>) combobox.getModel()).setDecorator(this.decorator);
        combobox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
        combobox.addItemListener(e -> {
            Object item = e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (log.isDebugEnabled()) {
                    log.debug("itemStateChanged selected " + item + " - " + (item != null ? item.getClass() : null));
                }
                combobox.getEditor().getEditorComponent().setForeground(null);
                ui.setSelectedItem(item);

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("itemStateChanged deselected " + item + " - " + (item != null ? item.getClass() : null));
                }
                combobox.getEditor().getEditorComponent().setForeground(ui.getInvalidComboEditorTextColor());
            }
        });

        // build popup
        popupHandler.preparePopup(ui.getSelectedToolTipText(),
                                  ui.getNotSelectedToolTipText(),
                                  ui.getI18nPrefix(),
                                  ui.getPopupTitleText(),
                                  indexes,
                                  ui.getPopupSeparator(),
                                  ui.getPopupLabel(),
                                  ui.getSortUp(),
                                  ui.getSortDown(),
                                  this.decorator);

        setFilterable(false, ui.getFilterable());

        ui.addPropertyChangeListener(this);

        // set datas
        ui.setData(data);

        // select sort button
        indexes.setSelectedButton(ui.getIndex());
    }

    /** Toggle the popup visible state. */
    public void togglePopup() {
        popupHandler.togglePopup();
    }

    /**
     * @return {@code true} if there is no data in comboBox, {@code false}
     * otherwise.
     */
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(ui.getData());
    }

    /**
     * Add the given items into the comboBox.
     * <p>
     * <strong>Note:</strong> The item will be inserted at his correct following
     * the selected ordering.
     *
     * @param items items to add in comboBox.
     * @since 2.5.28
     */
    public void addItems(Iterable<O> items) {

        List<O> data = ui.getData();

        boolean wasEmpty = CollectionUtils.isEmpty(data);

        for (O item : items) {
            data.add(item);
        }

        updateUI(ui.getIndex(), ui.isReverseSort());

        fireEmpty(wasEmpty);
    }

    /**
     * Remove the given items from the comboBox model.
     * <p>
     * <strong>Note:</strong> If this item was selected, then selection will be
     * cleared.
     *
     * @param items items to remove from the comboBox model
     * @since 2.5.28
     */
    public void removeItems(Iterable<O> items) {

        List<O> data = ui.getData();

        boolean needUpdate = false;
        for (O item : items) {
            boolean remove = data.remove(item);

            if (remove) {

                // item was found in data

                Object selectedItem = ui.getSelectedItem();
                if (item == selectedItem) {

                    // item was selected item, reset selected item then
                    ui.setSelectedItem(null);
                }

                needUpdate = true;
            }
        }

        if (needUpdate) {

            updateUI(ui.getIndex(), ui.isReverseSort());
            fireEmpty(false);
        }

    }

    /**
     * Add the given item into the comboBox.
     * <p>
     * <strong>Note:</strong> The item will be inserted at his correct following
     * the selected ordering.
     *
     * @param item item to add in comboBox.
     * @since 2.5.9
     */
    public void addItem(O item) {

        addItems(Collections.singleton(item));
    }

    /**
     * Remove the given item from the comboBox model.
     * <p>
     * <strong>Note:</strong> If this item was selected, then selection will be
     * cleared.
     *
     * @param item the item to remove from the comboBox model
     * @since 2.5.9
     */
    public void removeItem(O item) {

        removeItems(Collections.singleton(item));
    }

    /** Sort data of the model. */
    public void sortData() {

        // just update UI should do the math of this
        updateUI(ui.getIndex(), ui.isReverseSort());
    }

    /**
     * Reset the combo-box; says remove any selected item and filter text.
     */
    public void reset() {
        if (ui.getSelectedItem() != null) {
            ui.setSelectedItem(null);
        } else {
            JTextComponent editorComponent = (JTextComponent) ui.getCombobox().getEditor().getEditorComponent();
            editorComponent.setText("");
        }

        JComboBox comboBox = ui.getCombobox();
        if (comboBox.isShowing()) {
            comboBox.hidePopup();
        }
    }

    /**
     * Focus combo only if autoFocus ui property is on.
     *
     * @since 2.8.5
     */
    public void focusCombo() {
        if (ui.isAutoFocus()) {
            ui.combobox.requestFocusInWindow();
        }
    }

    /**
     * Modifie l'état filterable de l'ui.
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setFilterable(Boolean oldValue, Boolean newValue) {
        oldValue = oldValue != null && oldValue;
        newValue = newValue != null && newValue;
        if (oldValue.equals(newValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("filterable state : <" + oldValue + " to " + newValue + ">");
        }
        if (!newValue) {
            JTextComponent editorComponent = (JTextComponent) ui.getCombobox().getEditor().getEditorComponent();
            editorComponent.removeFocusListener(EDITOR_TEXT_COMP0NENT_FOCUSLISTENER);
            editorComponent.getDocument().removeDocumentListener(EDITOR_TEXT_COMPONENT_DOCUMENTLISTENER);
            ((JaxxFilterableComboBoxModel) ui.getCombobox().getModel()).setFilterText(null);

        } else {
            JTextComponent editorComponent = (JTextComponent) ui.getCombobox().getEditor().getEditorComponent();
            editorComponent.addFocusListener(EDITOR_TEXT_COMP0NENT_FOCUSLISTENER);
            editorComponent.getDocument().addDocumentListener(EDITOR_TEXT_COMPONENT_DOCUMENTLISTENER);
            EDITOR_TEXT_COMPONENT_DOCUMENTLISTENER.changedUpdate(null);
        }
    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setIndex(Integer oldValue, Integer newValue) {
        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }
        AbstractButton button = ui.getIndexes().getButton(newValue);
        if (button != null) {
            button.setSelected(true);
        }
        updateUI(newValue, ui.isReverseSort());
    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setSortOrder(Boolean oldValue, Boolean newValue) {

        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }

        updateUI(ui.getIndex(), newValue);
    }

    protected void updateUI(int index, boolean reversesort) {

        // change decorator context
        decorator.setContextIndex(index);

        // keep selected item
        Object previousSelectedItem = ui.getSelectedItem();

        // remove autocomplete
        if (previousSelectedItem != null) {
            ui.getCombobox().setSelectedItem(null);
            ui.selectedItem = null;
        }

        List<O> data = ui.getData();

        if (ui.isSortable() && CollectionUtils.isNotEmpty(data)) {
            try {
                // Sort data with the decorator jxpath tokens.
                DecoratorUtil.sort(decorator,
                                   data,
                                   index,
                                   reversesort);

            } catch (Exception eee) {
                log.warn(eee.getMessage(), eee);
            }
        }

        // reload the model
        SwingUtil.fillComboBox(ui.getCombobox(), data, null);

        if (previousSelectedItem != null) {
            ui.setSelectedItem(previousSelectedItem);
        }

        ui.getCombobox().requestFocus();
    }

    protected void unselectItem() {
        if (ui.selectedItem == null) {
            return;
        }

        ui.selectedItem = null;
        BeanUIUtil.invokeMethod(getMutator(),
                                ui.getBean(),
                                (O) null);
    }

    /**
     * Modifie la valeur sélectionnée dans la liste déroulante.
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setSelectedItem(O oldValue, O newValue) {
        if (oldValue == null && newValue == null) {
            return;
        }

        if (!getBeanType().isInstance(newValue)) {
            newValue = null;
        }

        JTextComponent editorComponent = (JTextComponent) ui.getCombobox().getEditor().getEditorComponent();
        editorComponent.setText("");

        if (log.isDebugEnabled()) {
            log.debug(ui.getProperty() + " on " + getBeanType() + " :: " + oldValue + " to " + newValue);
        }

        BeanUIUtil.invokeMethod(getMutator(),
                                ui.getBean(),
                                newValue);
    }

    public MultiJXPathDecorator<O> getDecorator() {
        return decorator;
    }

    /** @return get the type of objects contained in the comboBox model. */
    public Class<O> getBeanType() {
        Class<O> result = ui.getBeanType();
        if (result == null) {
            result = decorator == null ? null : decorator.getType();
        }
        return result;
    }

    /** @return le mutateur a utiliser pour modifier le bean associé. */
    protected Method getMutator() {
        if (mutator == null && ui.getBean() != null && ui.getProperty() != null) {
            mutator = Setters.getMutator(ui.getBean(), ui.getProperty());
        }
        return mutator;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();

        if (BeanFilterableComboBox.PROPERTY_SELECTED_ITEM.equals(propertyName)) {
            setSelectedItem((O) evt.getOldValue(), (O) evt.getNewValue());
            return;
        }

        if (BeanFilterableComboBox.PROPERTY_FILTERABLE.equals(propertyName)) {

            setFilterable((Boolean) evt.getOldValue(),
                          (Boolean) evt.getNewValue());
            return;
        }

        if (BeanFilterableComboBox.PROPERTY_INDEX.equals(propertyName)) {

            // decorator index has changed, force reload of data in ui
            setIndex((Integer) evt.getOldValue(),
                     (Integer) evt.getNewValue());
            return;
        }

        if (BeanFilterableComboBox.PROPERTY_REVERSE_SORT.equals(propertyName)) {

            // sort order has changed, force reload of data in ui
            setSortOrder((Boolean) evt.getOldValue(),
                         (Boolean) evt.getNewValue());
            return;
        }

        if (BeanFilterableComboBox.PROPERTY_DATA.equals(propertyName)) {

            // list has changed, force reload of index
            setIndex(null, ui.getIndex());

            // list has changed, fire empty property
            List list = (List) evt.getOldValue();
            fireEmpty(CollectionUtils.isEmpty(list));
        }
    }

    protected void fireEmpty(boolean wasEmpty) {
        ui.firePropertyChange(BeanComboBox.PROPERTY_EMPTY, wasEmpty,
                              isEmpty());
    }

    @Override
    public void beforeInit(BeanFilterableComboBox<O> ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(BeanFilterableComboBox<O> ui) {

    }

    /** Editor for the Combobox of the UI - uses the decorator */
    class JAXXFilterableComboBoxEditor implements ComboBoxEditor {

        Object oldItem;

        final ComboBoxEditor wrapped;

        public JAXXFilterableComboBoxEditor(ComboBoxEditor wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public JTextComponent getEditorComponent() {
            return (JTextComponent) wrapped.getEditorComponent();
        }

        @Override
        public void setItem(Object anObject) {
            if (log.isDebugEnabled()) {
                log.debug("setItem " + anObject + " - " + (anObject != null ? anObject.getClass() : null));
            }
            Object item = anObject;
            if (anObject != null) {
                if (getBeanType().isInstance(anObject)) {
                    item = decorator.toString(anObject);
                    oldItem = anObject;
                }
                try {
                    wrapped.setItem(item);

                } catch (IllegalStateException e) {
                    // fail silently
                }
            }
        }

        @Override
        public Object getItem() {
            JTextComponent editor = getEditorComponent();
            Object newValue = editor.getText();
            if (log.isDebugEnabled()) {
                log.debug("getItem " + newValue + " - " + (newValue != null ? newValue.getClass() : null));
            }

            if (oldItem != null && getBeanType().isInstance(oldItem)) {
                // The original value is not a string. Should return the value in it's
                // original type.
                if (Objects.equals(newValue, decorator.toString(oldItem))) {
                    newValue = oldItem;
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("getItem 2 " + newValue + " - " + (newValue != null ? newValue.getClass() : null));
            }
            return newValue;
        }

        @Override
        public void selectAll() {
            wrapped.selectAll();
        }

        @Override
        public void addActionListener(ActionListener l) {
            wrapped.addActionListener(l);
        }

        @Override
        public void removeActionListener(ActionListener l) {
            wrapped.removeActionListener(l);
        }
    }
}
