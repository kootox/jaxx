package org.nuiton.jaxx.widgets.select;

/*
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.decorator.MultiJXPathDecorator;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.model.JaxxFilterableListModel;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.nuiton.jaxx.runtime.swing.renderer.FilteredDecoratorListCellRenderer;

/**
 * The handler of a {@link BeanDoubleList}.
 *
 * @param <O> the type of the objects contained in the list.
 * @author Kevin Morin - kmorin@codelutin.com
 * @see BeanDoubleList
 * @since 2.5.8
 */
public class BeanDoubleListHandler<O> implements PropertyChangeListener, UIHandler<BeanDoubleList<O>> {

    private static final Log log = LogFactory.getLog(BeanDoubleListHandler.class);

    /** the mutator method on the property of boxed bean in the ui */
    protected Method mutator;

    protected BeanDoubleList<O> ui;

    /** the decorator of data */
    protected MultiJXPathDecorator<O> decorator;

    private I18nLabelsBuilder i18nLabelBuilder;

    public void setI18nLabelBuilder(I18nLabelsBuilder i18nLabelBuilder) {
        this.i18nLabelBuilder = i18nLabelBuilder;
    }

    private final BeanUIUtil.PopupHandler<O> popupHandler = new BeanUIUtil.PopupHandler<O>() {

        @Override
        public I18nLabelsBuilder getI18nLabelsBuilder() {

            if (i18nLabelBuilder == null) {
                i18nLabelBuilder = new I18nLabelsBuilder(getBeanType());
            }
            return i18nLabelBuilder;
        }

        @Override
        public JPopupMenu getPopup() {
            return ui.getPopup();
        }

        @Override
        public JComponent getInvoker() {
            return ui.getChangeDecorator();
        }
    };

    public JPopupMenu getSelectedListPopup(boolean showIt) {
        JPopupMenu result;
        if (showIt) {
            result = ui.getSelectedListPopup();
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Initializes the handler of the UI
     *
     * @param decorator the decorator to use to display the data nicely
     * @param universe  the list of all the available items
     * @param selected  the list of selected items
     */
    public void init(JXPathDecorator<O> decorator, List<O> universe, List<O> selected) {
        init(decorator, decorator, universe, selected);
    }

    /**
     * Initializes the handler of the UI
     *
     * @param decorator  the decorator to use to display the data nicely
     * @param decorator2 the selected decorator to use to display the selected data nicely (if none, then reuse the first one)
     * @param universe   the list of all the available items
     * @param selected   the list of selected items
     */
    public void init(JXPathDecorator<O> decorator, JXPathDecorator<O> decorator2, List<O> universe, List<O> selected) {

        if (decorator == null) {
            throw new NullPointerException("decorator can not be null (for type " + ui.getBeanType() + ")");
        }

        this.decorator = BeanUIUtil.createDecorator(decorator);

        final BeanDoubleListModel<O> uiModel = ui.getModel();

        // make sure useMultiSelect is set before filling universe and select lists
        uiModel.setUseMultiSelect(ui.getUseMultiSelect());

        uiModel.setUniverse(universe);
        uiModel.setSelected(selected);

        JList<O> universeList = ui.getUniverseList();
        JList<O> selectedList = ui.getSelectedList();

        uiModel.addCanRemoveItemsPredicate(o -> !o.isEmpty());

        FilteredDecoratorListCellRenderer<O> universeListCellRenderer
                = new FilteredDecoratorListCellRenderer<>(this.decorator);
        universeList.setCellRenderer(universeListCellRenderer);

        if (decorator2 != null) {
            decorator2 = BeanUIUtil.createDecorator(decorator2);
        } else {
            decorator2 = this.decorator;
        }
        selectedList.setCellRenderer(new DecoratorListCellRenderer<>(decorator2));

        // When universe list selection model changed, update the add button enabled property
        universeList.getSelectionModel().addListSelectionListener(e -> {
            ListSelectionModel source = (ListSelectionModel) e.getSource();
            uiModel.setAddEnabled(!source.isSelectionEmpty());
        });

        // When selected list selection model changed, update the add button enabled property
        selectedList.getSelectionModel().addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                recomputeButtonStates();
            }
        });

        // When selected list model changed, push back selected list to bean
        selectedList.getModel().addListDataListener(new ListDataListener() {

            @Override
            public void intervalAdded(ListDataEvent e) {
                fireSelectionUpdate();
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                fireSelectionUpdate();
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                fireSelectionUpdate();
            }
        });

        JAXXButtonGroup indexes = ui.getIndexes();

        // build popup
        popupHandler.preparePopup(null,
                                  null,
                                  ui.getI18nPrefix(),
                                  null,
                                  indexes,
                                  ui.getPopupSeparator(),
                                  ui.getPopupLabel(),
                                  ui.getSortUp(),
                                  ui.getSortDown(),
                                  this.decorator);

        ui.addPropertyChangeListener(this);

        JaxxFilterableListModel<O> filterModel = uiModel.getUniverseModel();

        filterModel.setDecorator(this.decorator);

        ui.getFilterField().getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = ui.getFilterField().getText();
                universeListCellRenderer.setFilterText(text);
                filterModel.setFilterText(text);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = ui.getFilterField().getText();
                universeListCellRenderer.setFilterText(text);
                filterModel.setFilterText(text);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                String text = ui.getFilterField().getText();
                universeListCellRenderer.setFilterText(text);
                filterModel.setFilterText(text);
            }
        });

        // select sort button
        indexes.setSelectedButton(ui.getIndex());

        sortData();

        // force to reload the showSelectPopup binding
        ui.processDataBinding(BeanDoubleList.BINDING_SELECTED_LIST_COMPONENT_POPUP_MENU);
    }

    public void recomputeButtonStates() {
        JList<O> selectedList = ui.getSelectedList();
        BeanDoubleListModel<O> uiModel = ui.getModel();

        List<O> selectedItems = selectedList.getSelectedValuesList();
        boolean removeEnabled = uiModel.computeRemoveEnabled(selectedItems);

        ListSelectionModel selectionModel = selectedList.getSelectionModel();
        int minSelectionIndex = selectionModel.getMinSelectionIndex();
        int maxSelectionIndex = selectionModel.getMaxSelectionIndex();

        boolean oneSelection = minSelectionIndex == maxSelectionIndex;

        boolean upEnabled = removeEnabled &&
                oneSelection &&
                minSelectionIndex > 0;
        uiModel.setSelectedUpEnabled(upEnabled);

        boolean downEnabled = removeEnabled &&
                oneSelection &&
                minSelectionIndex + 1 < uiModel.getSelectedListSize();
        uiModel.setSelectedDownEnabled(downEnabled);
    }

    public void setUniverse(List<O> selection) {
        ui.getModel().setUniverse(selection);
        sortData();
    }

    public void setSelected(List<O> selection) {
        ui.getModel().setSelected(selection);
        sortData();
    }

    /** Toggle the popup visible state. */
    public void togglePopup() {
        popupHandler.togglePopup();
    }

    /**
     * Sort data of the model.
     *
     * @since 2.5.10
     */
    public void sortData() {

        // just update UI should do the math of this
        updateUI(ui.getIndex(), ui.isReverseSort());
    }

    /**
     * Move up a selected item.
     *
     * @param item the selected item
     * @since 2.5.26
     */
    public void moveUpSelected(O item) {
        ui.getModel().moveUpSelected(item);
        ui.getSelectedList().setSelectedValue(item, true);
    }

    /**
     * Move down a selected item.
     *
     * @param item the selected item
     * @since 2.5.26
     */
    public void moveDownSelected(O item) {
        ui.getModel().moveDownSelected(item);
        ui.getSelectedList().setSelectedValue(item, true);
    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */
    protected void setIndex(Integer oldValue, Integer newValue) {
        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }
        AbstractButton button = ui.getIndexes().getButton(newValue);
        if (button != null) {
            button.setSelected(true);
        }
        updateUI(newValue, ui.isReverseSort());
    }

    /**
     * Modifie l'index du décorateur
     *
     * @param oldValue l'ancienne valeur
     * @param newValue la nouvelle valeur
     */

    protected void setSortOrder(Boolean oldValue, Boolean newValue) {

        if (newValue == null || newValue.equals(oldValue)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("check state : <" + oldValue + " to " + newValue + ">");
        }

        updateUI(ui.getIndex(), newValue);
    }

    protected void setHighlightFilterText(Boolean newValue) {
        FilteredDecoratorListCellRenderer universeListCellRenderer =
                (FilteredDecoratorListCellRenderer) ui.getUniverseList().getCellRenderer();
        universeListCellRenderer.setHighlightFilterText(newValue);
        ui.getUniverseList().repaint();
    }

    protected void updateUI(int index, boolean reversesort) {

        // change decorator context
        decorator.setContextIndex(index);

        JaxxFilterableListModel<O> universeModel = ui.getModel().getUniverseModel();
        List<O> data = Arrays.asList(universeModel.toArray());
        try {
            // Sort data with the decorator jxpath tokens.
            DecoratorUtil.sort(decorator,
                               data,
                               index,
                               reversesort
            );

        } catch (Exception eee) {
            log.warn(eee.getMessage(), eee);
        }

        // reload the model
        SwingUtil.fillList(ui.getUniverseList(), data, null);

        ui.getUniverseList().requestFocus();
    }

    /**
     * When universe list was double clicked, move selected items to selected list.
     *
     * @param event mouse event
     */
    public void onUniverseListClicked(MouseEvent event) {
        JList<O> universeList = ui.getUniverseList();
        if (event.getClickCount() == 2) {
            int index = universeList.locationToIndex(event.getPoint());
            if (index < 0) {
                return;
            }
            if (!ui.getModel().isAddEnabled()) {
                return;
            }
            O item = universeList.getModel().getElementAt(index);
            ui.getModel().addToSelected(item);

            sortData();
        }
    }

    /**
     * When selected list was double clicked, move selected items to universe list.
     *
     * @param event mouse event
     */
    public void onSelectedListClicked(MouseEvent event) {

        JList<O> selectedList = ui.getSelectedList();

        if (event.getClickCount() == 2) {
            int index = selectedList.locationToIndex(event.getPoint());
            if (index < 0) {
                return;
            }
            if (!ui.getModel().isRemoveEnabled()) {
                return;
            }
            O item = selectedList.getModel().getElementAt(index);
            ui.getModel().removeFromSelected(item);

            sortData();
        }
    }

    /** When add button was hit, move selected items (from universe list) to selected list. */
    public void select() {
        List<O> selection = ui.getUniverseList().getSelectedValuesList();
        ui.getModel().addToSelected(selection);
        ui.getUniverseList().clearSelection();

        sortData();
    }

    /** When remove button was hit, move selected items (from selected list) to universe list. */
    public void unselect() {
        List<O> selection = ui.getSelectedList().getSelectedValuesList();
        ui.getModel().removeFromSelected(selection);
        ui.getSelectedList().clearSelection();

        sortData();
    }

    protected void fireSelectionUpdate() {
        if (ui.getBean() != null) {
            BeanUIUtil.invokeMethod(getMutator(),
                                    ui.getBean(),
                                    ui.getModel().getSelected());
        }
    }

    public Method getMutator() {
        if (mutator == null && ui.getBackground() != null && ui.getProperty() != null) {
            mutator = Setters.getMutator(ui.getBean(), ui.getProperty());
        }
        return mutator;
    }

    public MultiJXPathDecorator<O> getDecorator() {
        return decorator;
    }

    /**
     * @return get the type of objects contained in the comboBox model.
     * @since 2.5.9
     */
    public Class<O> getBeanType() {
        Class<O> result = ui.getBeanType();
        if (result == null) {
            result = decorator == null ? null : decorator.getType();
        }
        return result;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();

        if (BeanListHeader.PROPERTY_INDEX.equals(propertyName)) {

            // decorator index has changed, force reload of data in ui
            setIndex((Integer) evt.getOldValue(),
                     (Integer) evt.getNewValue());

        } else if (BeanDoubleList.PROPERTY_REVERSE_SORT.equals(propertyName)) {

            // sort order has changed, force reload of data in ui
            setSortOrder((Boolean) evt.getOldValue(),
                         (Boolean) evt.getNewValue());

        } else if (BeanDoubleList.PROPERTY_HIGHLIGHT_FILTER_TEXT.equals(propertyName)) {

            setHighlightFilterText((Boolean) evt.getNewValue());
        } else if (BeanDoubleList.PROPERTY_USE_MULTI_SELECT.equals(propertyName)) {

            ui.getModel().setUseMultiSelect((Boolean) evt.getNewValue());
        }
    }

    public void addFilter(Predicate<O> filter) {
        ui.getModel().getUniverseModel().addFilter(filter);
    }

    public void removeFilter(Predicate<O> filter) {
        ui.getModel().getUniverseModel().removeFilter(filter);
    }

    public void clearFilters() {
        ui.getModel().getUniverseModel().clearFilters();
    }

    public void refreshFilteredElements() {
        ui.getModel().getUniverseModel().refreshFilteredElements();
    }

    public void onKeyPressedOnUniverseList(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int lastIndice = ui.getUniverseList().getSelectionModel().getLeadSelectionIndex();
            select();
            JList source = (JList) e.getSource();
            if (source.getModel().getSize() > 0) {
                source.setSelectedIndex(Math.min(lastIndice, source.getModel().getSize() - 1));
            } else {
                ui.getSelectedList().requestFocus();
            }
        }
    }

    public void onKeyPressedOnSelectedList(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            int lastIndice = ui.getSelectedList().getSelectionModel().getLeadSelectionIndex();
            unselect();
            JList source = (JList) e.getSource();
            if (source.getModel().getSize() > 0) {
                source.setSelectedIndex(Math.min(lastIndice, source.getModel().getSize() - 1));
                ui.getSelectedList().requestFocus();
            }
        }
    }

    public void selectFirstRowIfNoSelection(FocusEvent event) {
        JList list = (JList) event.getSource();
        if (list.isSelectionEmpty()) {
            list.setSelectedIndex(0);
        }
    }

    @Override
    public void beforeInit(BeanDoubleList<O> ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(BeanDoubleList<O> ui) {

    }
}
