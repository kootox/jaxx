package org.nuiton.jaxx.widgets.select.session;

/*
 * #%L
 * JAXX :: Widgets Select
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.session.State;
import org.nuiton.jaxx.widgets.select.BeanFilterableComboBox;

/**
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.20
 */
public class BeanFilterableComboBoxState implements State {

    protected int index = 0;

    protected boolean reverseSort = false;

    public BeanFilterableComboBoxState() {
    }

    public BeanFilterableComboBoxState(int index, boolean reverseSort) {
        this.index = index;
        this.reverseSort = reverseSort;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean reverseSort) {
        this.reverseSort = reverseSort;
    }

    protected BeanFilterableComboBox checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof BeanFilterableComboBox)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (BeanFilterableComboBox) o;
    }

    @Override
    public State getState(Object o) {
        BeanFilterableComboBox combo = checkComponent(o);
        return new BeanFilterableComboBoxState(combo.getIndex(), combo.isReverseSort());
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof BeanFilterableComboBoxState)) {
            throw new IllegalArgumentException("invalid state");
        }
        BeanFilterableComboBox combo = checkComponent(o);
        BeanFilterableComboBoxState bfcbState = (BeanFilterableComboBoxState) state;
        combo.setIndex(bfcbState.getIndex());
        combo.setReverseSort(bfcbState.isReverseSort());
    }


}
