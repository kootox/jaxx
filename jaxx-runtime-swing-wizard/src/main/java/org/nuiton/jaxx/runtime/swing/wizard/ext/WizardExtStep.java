/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard.ext;

import org.nuiton.jaxx.runtime.swing.wizard.WizardStep;
import org.nuiton.jaxx.runtime.swing.wizard.WizardStepUI;


/**
 * Extension de {@link WizardStep} avec des états supplémentaires.
 *
 * Chaque étape possède un modèle de type {@link #getModelClass()}.
 * Ce modèle peut être partagé par plusieurs étapes.
 *
 * De plus, on a deux drapeaux {@link #isConfig()} pour savoir si l'étape est une phase de configuration.
 *
 * Le drapeau {@link #isOperation()} quand à lui permet de savoir si l'étape est une opération.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public interface WizardExtStep extends WizardStep {

    /** @return le label de l'opération */
    String getOperationLabel();

    /** @return la description de l'opération */
    String getOperationDescription();

    /** @return le type du modèle de l'action */
    Class<? extends WizardExtStepModel<?>> getModelClass();

    /** @return le type de l'ui de l'opération */
    Class<? extends WizardStepUI<?, ?>> getUiClass();

    /** @return {@code true} si l'étape a une opération associée, {@code false} sinon. */
    boolean isOperation();

    /** @return {@code true} si l'étape est une phase de configuration, {@code false} sinon. */
    boolean isConfig();

    /**
     * Instancie le modèle associée à l'étape.
     *
     * @return le nouveau modèle associé à l'étape
     */
    WizardExtStepModel<?> newModel();

    /**
     * Construit l'ui associée à l'étape.
     *
     * @param ui l'ui principale du wiard
     * @return la noveau ui associé à l'étape
     */
    WizardStepUI<?, ?> newUI(WizardExtUI<?, ?> ui);
}
