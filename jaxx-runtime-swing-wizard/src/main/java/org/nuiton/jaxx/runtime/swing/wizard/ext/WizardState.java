/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard.ext;

/**
 * Pour caractériser l'état d'une opération.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum WizardState {

    /** quand l'opération n'a pas encore été réalisée */
    PENDING,

    /** quand l'opération est en cours */
    RUNNING,

    /** quand l'opération est annulé en cours d'exécution */
    CANCELED,

    /** quand une erreur s'est produite pendant l'exécution */
    FAILED,

    /** quand l'exécution s'est terminée mais requière des corrections */
    NEED_FIX,

    /** quand l'exécution s'est terminée et ne requière pas de correction */
    SUCCESSED
}
