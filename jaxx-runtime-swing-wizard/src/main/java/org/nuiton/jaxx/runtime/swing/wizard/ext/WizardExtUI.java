/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard.ext;

import org.nuiton.jaxx.runtime.swing.wizard.WizardUI;

/**
 * Contrat a respecter pour une ui de wizard.
 *
 * @param <E> le type d'etape
 * @param <M> le type de model
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public interface WizardExtUI<E extends WizardExtStep, M extends WizardExtModel<E>> extends WizardUI<E, M> {

    /** Méthode invoqué lorsque le modèle a été initialisé. */
    void onWasInit();

    /** Méthode invoqué lorsque la première opération du modèlé a été démarrée. */
    void onWasStarted();

    /**
     * Méthode invoquée lorsque l'état interne du modèle a changé.
     *
     * @param newState le nouvelle état du modèle de wizard
     */
    void onModelStateChanged(WizardState newState);

    /**
     * Méthode invoqué lorsque l'état d'une opération a changé.
     *
     * @param step     l'étape dont l'état a changé
     * @param newState le nouvel état pour l'étape donné
     */
    void onOperationStateChanged(E step, WizardState newState);

}
