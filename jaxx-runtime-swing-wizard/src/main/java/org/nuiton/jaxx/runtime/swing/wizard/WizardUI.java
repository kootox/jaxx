/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard;

import javax.swing.JTabbedPane;

/**
 * Contrat a respecter pour une ui de wizard.
 *
 * @param <E> le type d'etape
 * @param <M> le type de model
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public interface WizardUI<E extends WizardStep, M extends WizardModel<E>> {

    /** @return le modèle de wizard */
    M getModel();

    /** @return l'étape courante */
    E getSelectedStep();

    /** @return l'ui de l'étape courante */
    WizardStepUI<E, M> getSelectedStepUI();

    /**
     * @param step l'étape donnée
     * @return l'ui de l'étape donnée
     */
    WizardStepUI<E, M> getStepUI(E step);

    /**
     * @param stepIndex la position de l'étape
     * @return l'ui de l'étape donée
     */
    WizardStepUI<E, M> getStepUI(int stepIndex);

    /** démarre le wizard */
    void start();

    /**
     * //TODO il faudrait supprimer cette méthode
     *
     * @return le conteneur d'ui d'étapes
     */
    JTabbedPane getTabs();

    /**
     * Méthode invoquée lorsque l'univers des étapes a été modifié dans le
     * modèle.
     *
     * @param steps les nouvelles étapes
     */
    void onStepsChanged(E[] steps);

    /**
     * Méthode invoquée lorsque l'étape courante a changé dans le modèle.
     *
     * @param oldStep l'ancienne étape
     * @param newStep la nouvelle étape courante
     */
    void onStepChanged(E oldStep, E newStep);
}
