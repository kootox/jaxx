/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;

import java.awt.Component;
import java.awt.Cursor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * To listen the busy state of a {@link WizardModel}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class BusyChangeListener implements PropertyChangeListener {

    /** Logger */
    private static final Log log = LogFactory.getLog(BusyChangeListener.class);

    protected Cursor busyCursor;

    protected Cursor defaultCursor;

    protected final Component ui;

    protected BlockingLayerUI blockingUI;

    public BusyChangeListener(Component ui) {
        this.ui = ui;
    }

    public Component getUi() {
        return ui;
    }

    public BlockingLayerUI getBlockingUI() {
        return blockingUI;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Boolean value = (Boolean) evt.getNewValue();
        if (log.isDebugEnabled()) {
            log.debug("busy state changed to " + value);
        }
        if (value != null && value) {
            setBusy(ui);

        } else {
            setUnBusy(ui);
        }
    }

    public void setBlockingUI(BlockingLayerUI blockingUI) {
        this.blockingUI = blockingUI;
    }

    protected void setBusy(Component ui) {
        if (ui != null) {
            ui.setCursor(getBusyCursor());
        }
        if (blockingUI != null) {
            blockingUI.setBlock(true);
        }
    }

    protected void setUnBusy(Component ui) {
        if (ui != null) {
            ui.setCursor(getDefaultCursor());
        }
        if (blockingUI != null) {
            blockingUI.setBlock(false);
        }
    }

    protected Cursor getBusyCursor() {
        if (busyCursor == null) {
            busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
        }
        return busyCursor;
    }

    protected Cursor getDefaultCursor() {
        if (defaultCursor == null) {
            defaultCursor = Cursor.getDefaultCursor();
        }
        return defaultCursor;
    }
}
