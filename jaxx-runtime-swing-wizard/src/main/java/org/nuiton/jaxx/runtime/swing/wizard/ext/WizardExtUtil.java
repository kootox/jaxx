/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard.ext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.wizard.WizardModel;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUI;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUtil;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Array;
import java.util.List;

/**
 * Classe de méthodes utiles sur les wizard.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class WizardExtUtil extends WizardUtil {

    /** Logger */
    static private final Log log = LogFactory.getLog(WizardUI.class);

    protected WizardExtUtil() {
    }

    public static boolean acceptStates(WizardState state, WizardState... accepted) {
        for (WizardState s : accepted) {
            if (s == state) {
                return true;
            }
        }
        return false;
    }

    public static boolean rejectStates(WizardState state, WizardState... rejected) {
        for (WizardState s : rejected) {
            if (s == state) {
                return false;
            }
        }
        return true;
    }

    public static <E extends WizardExtStep, M extends WizardExtModel<E>> void installWizardUIListeners(final WizardExtUI<E, M> ui) {


        PropertyChangeListener dispatcher = evt -> {
            String propertyName = evt.getPropertyName();
            if (WizardExtModel.WAS_STARTED_PROPERTY_NAME.equals(propertyName)) {
                ui.onWasStarted();
                return;
            }
            WizardExtModel<E> model = (WizardExtModel<E>) evt.getSource();
            if (WizardModel.STEPS_PROPERTY_NAME.equals(propertyName)) {
                List<E> steps = (List<E>) evt.getNewValue();
                ui.onStepsChanged(
                        steps.toArray((E[]) Array.newInstance(
                                model.getStepClass(), steps.size()))
                );
                return;
            }
            if (WizardModel.STEP_PROPERTY_NAME.equals(propertyName)) {
                ui.onStepChanged((E) evt.getOldValue(), (E) evt.getNewValue());
                return;
            }
            if (WizardModel.VALID_STEP_PROPERTY_NAME.equals(propertyName)) {
                Boolean value = (Boolean) evt.getNewValue();
                if (value == null || !value) {
                    ui.onModelStateChanged(WizardState.NEED_FIX);
                } else {
                    ui.onModelStateChanged(WizardState.PENDING);
                }
                return;
            }
            if (WizardExtModel.MODEL_STATE_PROPERTY_NAME.equals(propertyName)) {
                //TODO should be unicast : only for good stepUI ?
                ui.onModelStateChanged((WizardState) evt.getNewValue());
                return;
            }
            if (WizardExtModel.STEP_STATE_PROPERTY_NAME.equals(propertyName)) {
                IndexedPropertyChangeEvent e = (IndexedPropertyChangeEvent) evt;
                int stepIndex = e.getIndex();
                E step = model.getSteps().get(stepIndex);
                ui.onOperationStateChanged(step, (WizardState) evt.getNewValue());
            }
        };
        M model = ui.getModel();
        log.info("Adding dispatcher " + dispatcher + " to model " + model);
        model.addPropertyChangeListener(dispatcher);
    }

}
