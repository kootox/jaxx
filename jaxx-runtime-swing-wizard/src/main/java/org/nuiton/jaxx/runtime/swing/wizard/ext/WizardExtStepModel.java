/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard.ext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXUtil;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Abstract model of {@link WizardExtStep}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see WizardExtStep
 * @since 2.1
 */
public abstract class WizardExtStepModel<E extends WizardExtStep> {

    /** Logger */
    private static final Log log = LogFactory.getLog(WizardExtStepModel.class);

    /** for properties change support */
    private final PropertyChangeSupport pcs;

    /** step of the model */
    protected final E step;

    /** to store if an error occurs while running */
    protected Exception error;

    protected WizardExtStepModel(E step) {
        this.step = step;
        pcs = new PropertyChangeSupport(this);
    }

    public final E getStep() {
        return step;
    }

    public final Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }


    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public void destroy() {
        if (log.isDebugEnabled()) {
            log.debug("will destroy " + this);
        }

        // remove all listeners
        JAXXUtil.destroy(pcs);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(String propertyName, Object newValue) {
        pcs.firePropertyChange(propertyName, null, newValue);
    }
}
