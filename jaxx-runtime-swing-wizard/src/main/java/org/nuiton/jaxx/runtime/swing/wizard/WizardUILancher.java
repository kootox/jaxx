/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard;

import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;

import javax.swing.ImageIcon;
import java.awt.Window;

/**
 * Une classe pour lancer une ui de wizard.
 *
 * @param <E>  le type des etapes
 * @param <M>  le type de modele
 * @param <UI> le type d'ui
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public abstract class WizardUILancher<E extends WizardStep, M extends WizardModel<E>, UI extends WizardUI<E, M>> {

    /** the jaxx context entry to store the apply action */
    public static final JAXXContextEntryDef<Runnable> APPLY_DEF =
            JAXXUtil.newContextEntryDef("apply", Runnable.class);

    /** the jaxx context entry to store the cancel action */
    public static final JAXXContextEntryDef<Runnable> CANCEL_DEF =
            JAXXUtil.newContextEntryDef("cancel", Runnable.class);

    public static <M extends WizardModel<?>> JAXXContextEntryDef<M> newModelEntry(Class<M> modelType) {
        return JAXXUtil.newContextEntryDef("incoming", modelType);
    }

    protected UI ui;

    public WizardUILancher(JAXXContext context,
                           Class<UI> uiClass,
                           Class<M> modelClass,
                           String title,
                           String tip,
                           ImageIcon icon) {
        this(context, uiClass, modelClass, null, title, tip, icon);
    }

    public WizardUILancher(JAXXContext context,
                           Class<UI> uiClass,
                           Class<M> modelClass,
                           M model,
                           String title,
                           String tip,
                           ImageIcon icon) {
        try {
            ui = createUI(context,
                          uiClass,
                          modelClass,
                          model,
                          title,
                          tip,
                          icon
            );
        } catch (Exception ex) {
            throw new RuntimeException(
                    "could not instanciate launcher for reason " +
                            ex.getMessage(), ex);
        }
    }

    public WizardUILancher(JAXXContext context,
                           Window mainUI,
                           Class<UI> uiClass,
                           Class<M> modelClass,
                           M model) {
        try {
            ui = createUI(context,
                          mainUI,
                          uiClass,
                          modelClass,
                          model
            );
        } catch (Exception ex) {
            throw new RuntimeException(
                    "could not instanciate launcher for reason " +
                            ex.getMessage(), ex);
        }
    }

    public WizardUILancher(JAXXContext context,
                           Window mainUI,
                           Class<UI> uiClass,
                           Class<M> modelClass) {
        this(context, mainUI, uiClass, modelClass, null);
    }

    public void start() {
        init(ui);
        start(ui);
    }

    protected void start(UI ui) {
        ui.start();
    }

    public <T> T getContextValue(Class<T> clazz, String name) {
        if (ui == null) {
            throw new NullPointerException("ui can not be null");
        }
        if (!(ui instanceof JAXXObject)) {
            throw new ClassCastException("ui can not be casted to JAXXObject ");
        }

        return ((JAXXObject) ui).getContextValue(clazz, name);
    }

    public <T> T getContextValue(Class<T> clazz) {
        return getContextValue(clazz, null);
    }

    protected void init(UI ui) {
    }

    protected void doAction(UI ui) {
    }

    protected void doCancel(UI ui) {
    }

    protected void doClose(UI ui, boolean wasCancel) {
    }

    protected UI createUI(JAXXContext context,
                          Window mainUI,
                          Class<UI> uiClass,
                          Class<M> modelClass,
                          M model) throws Exception {
        JAXXInitialContext uiContext = new JAXXInitialContext();
        uiContext.add(mainUI == null ? context : mainUI);
        // parent context model
        uiContext.add(newModel(context, modelClass));
        if (model != null) {
            uiContext.add(newModelEntry(modelClass), model);
        }
        // apply action
        uiContext.add(APPLY_DEF, () -> {
            try {
                doAction(ui);
            } finally {
                doClose(ui, false);
            }
        });
        // cancel action
        uiContext.add(CANCEL_DEF, () -> {
            try {
                doCancel(ui);
            } finally {
                doClose(ui, true);
            }
        });

        // instanciate ui

        return JAXXUtil.invokeConstructor(
                uiClass,
                new Class[]{Window.class, JAXXContext.class},
                mainUI, uiContext
        );
    }

    protected M newModel(JAXXContext context,
                          Class<M> modelClass
                          ) throws Exception {
        return modelClass.newInstance();
    }

    protected UI createUI(JAXXContext context,
                          Class<UI> uiClass,
                          Class<M> modelClass,
                          M model,
                          String title,
                          String tip,
                          ImageIcon icon) throws Exception {
        JAXXInitialContext uiContext = new JAXXInitialContext();
        uiContext.add(context);
        // parent context model
        uiContext.add(newModel(context, modelClass));
        if (model != null) {
            uiContext.add(newModelEntry(modelClass), model);
        }
        // apply action
        uiContext.add(APPLY_DEF, () -> {
            try {
                doAction(ui);
            } finally {
                doClose(ui, false);
            }
        });

        // cancel action
        uiContext.add(CANCEL_DEF, () -> {
            try {
                doCancel(ui);
            } finally {
                doClose(ui, true);
            }
        });

        // instanciate ui
        return JAXXUtil.invokeConstructor(
                uiClass,
                new Class[]{
                        JAXXContext.class,
                        String.class,
                        String.class,
                        ImageIcon.class
                },
                uiContext, title, tip, icon
        );
    }
}
