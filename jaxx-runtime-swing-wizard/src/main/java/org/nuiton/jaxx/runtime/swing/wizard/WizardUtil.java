/*
 * #%L
 * JAXX :: Runtime Swing Wizard
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.wizard;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTabbedPane;
import java.awt.Component;
import java.lang.reflect.Array;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Classe de méthodes utiles sur les wizard.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class WizardUtil {

    /** Logger */
    static private final Log log = LogFactory.getLog(WizardUI.class);

    protected WizardUtil() {
    }

//    public static boolean acceptStates(WizardState state, WizardState... accepted) {
//        for (WizardState s : accepted) {
//            if (s == state) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public static boolean rejectStates(WizardState state, WizardState... rejected) {
//        for (WizardState s : rejected) {
//            if (s == state) {
//                return false;
//            }
//        }
//        return true;
//    }

    public static void addDebugLogListener(final Log log, WizardModel<?> model) {
        if (log.isDebugEnabled()) {
            model.addPropertyChangeListener(evt -> log.debug(evt.getPropertyName() + " <" + evt.getOldValue() + " - " + evt.getNewValue() + ">"));
        }
    }

    public static void addTraceLogListener(final Log log, WizardModel<?> model) {
        if (log.isTraceEnabled()) {
            model.addPropertyChangeListener(evt -> log.trace(evt.getPropertyName() + " <" + evt.getOldValue() + " - " + evt.getNewValue() + ">"));
        }
    }

    public static <E extends WizardStep, M extends WizardModel<E>> void installWizardUIListeners(final WizardUI<E, M> ui) {
        ui.getModel().addPropertyChangeListener(evt -> {
            String propertyName = evt.getPropertyName();
//                if (WizardExtModel.WAS_STARTED_PROPERTY_NAME.equals(propertyName)) {
//                    ui.onWasStarted();
//                    return;
//                }
            if (WizardModel.STEPS_PROPERTY_NAME.equals(propertyName)) {
                List<E> steps = (List<E>) evt.getNewValue();
                ui.onStepsChanged(
                        steps.toArray((E[]) Array.newInstance(
                                ui.getModel().stepClass, steps.size()))
                );
                return;
            }
            if (WizardModel.STEP_PROPERTY_NAME.equals(propertyName)) {
                ui.onStepChanged((E) evt.getOldValue(), (E) evt.getNewValue());
//                    return;
            }
            /*if (WizardModel.VALID_STEP_PROPERTY_NAME.equals(propertyName)) {
                Boolean value = (Boolean) evt.getNewValue();
                if (value == null || !value) {
                    ui.onModelStateChanged(WizardState.NEED_FIX);
                } else {
                    ui.onModelStateChanged(WizardState.PENDING);
                }
                return;
            }
            if (WizardExtModel.MODEL_STATE_PROPERTY_NAME.equals(propertyName)) {
                //TODO should be unicast : only for good stepUI ?
                ui.onModelStateChanged((WizardState) evt.getNewValue());
                return;
            }
            if (WizardExtModel.STEP_STATE_PROPERTY_NAME.equals(propertyName)) {
                IndexedPropertyChangeEvent e = (IndexedPropertyChangeEvent) evt;
                int stepIndex = e.getIndex();
                E step = ui.getModel().getSteps().get(stepIndex);
                ui.onOperationStateChanged(step, (WizardState) evt.getNewValue());
                return;
            }*/
        });
    }

    /**
     * Ajoute un listener sur le modele pour gere la politique d'affichage des
     * onglets.
     *
     * Dans cette implantation, les onglets sont ouverts jusqu'a l'etape
     * courante. Lorsque l'on revient en arrière, les onglets d'etapes
     * superieurs sont fermes.
     *
     * @param <E> le type d'un etape de l'assistant
     * @param <M> le type du modele de l'assistant
     * @param ui  l'ui de l'assitant
     * @since 1.7.1
     */
    public static <E extends WizardStep, M extends WizardModel<E>> void addTabsDisplayUntilStepListener(final WizardUI<E, M> ui) {
        // on écoute les changements d'étapes
        ui.getModel().addPropertyChangeListener(WizardModel.STEP_PROPERTY_NAME, evt -> {
            M model = (M) evt.getSource();
            E oldStep = (E) evt.getOldValue();
            E newStep = (E) evt.getNewValue();
            log.debug("step has changed <old:" + oldStep + ", new:" + newStep + ">");
            int oldStepIndex = oldStep == null ? -1 : model.getStepIndex(oldStep);
            int newStepIndex = model.getStepIndex(newStep);
            JTabbedPane tabs = ui.getTabs();
            if (oldStepIndex + 1 == newStepIndex) {
                // creation d'un nouvel onglet
                WizardStepUI<E, M> c = ui.getStepUI(newStep);
                String title = t(newStep.getLabel());
                String tip = t(newStep.getDescription());
                tabs.addTab(title, null, (Component) c, tip);
                // selection du nouvel onglet
                int index = tabs.indexOfComponent((Component) c);
                if (index > -1) {
                    tabs.setSelectedIndex(index);
                }
            } else if (oldStepIndex > newStepIndex) {
                // il s'agit d'un retour en arrière
                // on supprime tous les onglets obsoletes
                int index = newStepIndex + 1;
                while (tabs.getTabCount() > index) {
                    log.trace("remove tab : " + index);
                    tabs.remove(index);
                }
            } else {
                throw new IllegalStateException("can not go from " + oldStep + " to " + newStep);
            }
        });
    }
}
