package org.nuiton.jaxx.widgets.about;

/*-
 * #%L
 * JAXX :: Widgets About
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import javax.swing.ImageIcon;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Objects;


import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AboutUIBuilder {

    public static final String CLOSE_ACTION = "close";

    protected final AboutUI ui;

    public AboutUIBuilder(Frame parent) {
        Objects.requireNonNull(parent);
        this.ui = new AboutUI(parent, true);

        Action closeAction = new AbstractAction(CLOSE_ACTION) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                ui.dispose();
            }

        };
        closeAction.putValue(Action.NAME, null);
        closeAction.putValue(Action.SMALL_ICON, ui.getClose().getIcon());
        closeAction.putValue(Action.SHORT_DESCRIPTION, ui.getClose().getToolTipText());
        ui.getClose().setAction(closeAction);

        JRootPane rootPane = ui.getRootPane();
        rootPane.setDefaultButton(ui.getClose());
        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), CLOSE_ACTION);
        rootPane.getActionMap().put(CLOSE_ACTION, closeAction);

    }

    public static AboutUIBuilder builder(Frame parent) {
        return new AboutUIBuilder(parent);
    }

    public AboutUIBuilder setTitle(String title) {
        ui.getTitleLabel().setText(title);
        return this;
    }

    public AboutUIBuilder setIconPath(String iconPath) {
        ui.setIconPath(iconPath);
        if (iconPath != null) {
            Icon logoIcon = new ImageIcon(getClass().getResource(iconPath));
            ui.getTopPanel().add(new JLabel(logoIcon));
        }
        return this;
    }

    public AboutUIBuilder setBottomText(String bottomText) {
        ui.getBottomLabel().setText(bottomText);
        return this;
    }

    public AboutUIBuilder addAboutTab(String aboutText, boolean html) {
        return addTab0(t("aboutframe.about"), aboutText, html);
    }

    public AboutUIBuilder addDefaultLicenseTab(String projectName, boolean html) {
        return addLicenseTab("META-INF/" + projectName + "-LICENSE.txt", html);
    }

    public AboutUIBuilder addLicenseTab(String resourceName, boolean html) {
        String tabContent = load(resourceName);
        addTab0(t("aboutframe.license"), tabContent, html);
        return this;
    }

    public AboutUIBuilder addDefaultThirdPartyTab(String projectName, boolean html) {
        return addThirdPartyTab("META-INF/" + projectName + "-THIRD-PARTY.txt", html);
    }

    public AboutUIBuilder addThirdPartyTab(String resourceName, boolean html) {
        String tabContent = load(resourceName);
        return addTab0(t("aboutframe.thirdparty"), tabContent, html);
    }

    public AboutUIBuilder addDefaultChangelogTab(String projectName, boolean html) {
        return addChangelogTab("META-INF/" + projectName + "-CHANGELOG.txt", html);
    }

    public AboutUIBuilder addChangelogTab(String resourceName, boolean html) {
        String tabContent = load(resourceName);
        return addTab0(t("aboutframe.changelog"), tabContent, html);
    }

    public AboutUIBuilder addTab(String tabTitle, String tabContent, boolean html) {
        return addTab0(tabTitle, tabContent, html);
    }

    public AboutUIBuilder addTabFromResource(String tabTitle, String resourceName, boolean html) {
        String tabContent = load(resourceName);
        return addTab0(tabTitle, tabContent, html);
    }

    public AboutUIBuilder addTab(String tabTitle, JComponent component) {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBorder(null);
        ui.tabs.addTab(tabTitle, component);
        return this;
    }

    public AboutUI build() {
        return ui;
    }

    protected AboutUIBuilder addTab0(String tabTitle, String tabContent, boolean html) {

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBorder(null);

        JTabbedPane tabs = ui.getTabs();
        tabs.addTab(tabTitle, scrollPane);

        final JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setBorder(null);
        if (html) {
            // must be done before set in text
            jEditorPane.setContentType("text/html");
        }
        jEditorPane.setText(tabContent);
        jEditorPane.setEditable(false);
        jEditorPane.setFont(jEditorPane.getFont().deriveFont((float) 11));
        jEditorPane.addHyperlinkListener(SwingUtil::openLink);

        scrollPane.getViewport().add(jEditorPane);
        SwingUtilities.invokeLater(() -> jEditorPane.setCaretPosition(0));

        return this;
    }

    protected String load(String resourceName) {
        String result = null;
        try {
            result = Resources.asCharSource(Resources.getResource(resourceName), Charsets.UTF_8).read();

        } catch (IOException ex) {
            // ignore it
        }
        if (result == null) {
            result = "resource " + resourceName + " not found";
        }
        return result;
    }
}
