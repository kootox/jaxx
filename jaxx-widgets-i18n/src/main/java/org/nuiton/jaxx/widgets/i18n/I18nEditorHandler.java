/*
 * #%L
 * JAXX :: Widgets I18n
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.i18n;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

/**
 * Handler of ui {@link I18nEditor}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0
 */
public class I18nEditorHandler implements PropertyChangeListener, ActionListener, UIHandler<I18nEditor> {

    private static final Log log = LogFactory.getLog(I18nEditorHandler.class);

    protected I18nEditor ui;

    public static final String DEFAULT_SELECTED_TOOLTIP = I18n.n("i18neditor.selected");

    public static final String DEFAULT_NOT_SELECTED_TOOLTIP = I18n.n("i18neditor.unselected");

    public static final String LOCALES_PROPERTY = "locales";

    public static final String SELECTED_LOCALE_PROPERTY = "selectedLocale";

    public static final String SHOW_ICON_PROPERTY = "showIcon";

    public static final String SHOW_TEXT_PROPERTY = "showText";

    public static final String SHOW_POPUP_ICON_PROPERTY = "showPopupIcon";

    public static final String SHOW_POPUP_TEXT_PROPERTY = "showPopupText";

    public static final String POPUP_BORDER_PROPERTY = "popupBorder";

    public static final String POPUP_VISIBLE_PROPERTY = "popupVisible";

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String name = evt.getPropertyName();
        if (log.isDebugEnabled()) {
            log.debug(name + " <old:" + evt.getOldValue() + " - new:" + evt.getNewValue() + ">");
        }
        log.info(name + " <old:" + evt.getOldValue() + " - new:" + evt.getNewValue() + ">");
        if (LOCALES_PROPERTY.equals(name)) {
            Collection<?> newLocales = (Collection<?>) evt.getNewValue();
            // mise a jour de la popup
            boolean oldShowText = ui.renderer.isShowText();
            try {
                ui.renderer.setShowText(true);
                ui.popup.removeAll();
                for (Object o : newLocales) {
                    Locale l = (Locale) o;
                    boolean selected = l.equals(ui.selectedLocale);
                    String text = ui.isShowPopupText() ? ui.renderer.getText(l) : null;
                    Icon icon = ui.isShowPopupIcon() ? ui.renderer.getIcon(l) : null;
                    JRadioButtonMenuItem b = new JRadioButtonMenuItem(text, icon, selected);
                    ui.popup.add(b);
                    b.addActionListener(ui);
                    b.putClientProperty("locale", l);
                    b.setToolTipText(getTip(l));
                    b.putClientProperty(JAXXButtonGroup.BUTTON8GROUP_CLIENT_PROPERTY, ui.getIndexes());
                    b.putClientProperty(JAXXButtonGroup.VALUE_CLIENT_PROPERTY, l);
                }
            } finally {
                ui.renderer.setShowText(oldShowText);
                ui.popup.invalidate();
            }
            return;
        }
        if (SHOW_ICON_PROPERTY.equals(name)) {
            ui.renderer.setShowIcon((Boolean) evt.getNewValue());
            ui.processDataBinding("button.icon");
            return;
        }
        if (SHOW_TEXT_PROPERTY.equals(name)) {
            ui.renderer.setShowText((Boolean) evt.getNewValue());
            ui.processDataBinding("button.text");
            return;
        }
        if (SHOW_POPUP_ICON_PROPERTY.equals(name)) {
            rebuildPopup();
            return;
        }
        if (SHOW_POPUP_TEXT_PROPERTY.equals(name)) {
            rebuildPopup();
            return;
        }
        if (POPUP_BORDER_PROPERTY.equals(name)) {
            ui.popup.setBorder((Border) evt.getNewValue());
            return;
        }
        if (POPUP_VISIBLE_PROPERTY.equals(name)) {
            Boolean newValue = (Boolean) evt.getNewValue();
            if (newValue == null || !newValue) {
                if (ui.getPopup() != null && ui.getPopup().isVisible()) {
                    ui.getPopup().setVisible(false);
                }
                return;
            }
            if (!ui.getPopup().isVisible()) {
                SwingUtilities.invokeLater(showPopupRunnable);
            }
            return;
        }
        if (SELECTED_LOCALE_PROPERTY.equals(name)) {
            Locale newLocale = (Locale) evt.getNewValue();
            // mise a jour de la popup
            try {
                for (Component c : ui.popup.getComponents()) {
                    if (c instanceof JRadioButtonMenuItem) {
                        JRadioButtonMenuItem b = (JRadioButtonMenuItem) c;
                        Locale l = (Locale) b.getClientProperty("locale");
                        b.setSelected(newLocale.equals(l));
                    }
                }
            } finally {
                ui.popup.invalidate();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Locale value = (Locale)
                ((JComponent) event.getSource()).getClientProperty("locale");
        if (log.isDebugEnabled()) {
            log.debug("new locale : " + value);
        }
        ui.setSelectedLocale(value);
    }

    public void loadI18nBundles() {
        Locale[] locales = I18n.getStore().getLocales();
        ui.setLocales(Arrays.asList(locales));
    }

    protected void rebuildPopup() {
        log.debug("start rebuild");
        try {
            for (Component c : ui.popup.getComponents()) {
                if (c instanceof JRadioButtonMenuItem) {
                    JRadioButtonMenuItem b = (JRadioButtonMenuItem) c;
                    Locale l = (Locale) b.getClientProperty("locale");
                    String text = ui.isShowPopupText() ? ui.renderer.getSafeText(l) : null;
                    Icon icon = ui.isShowPopupIcon() ? ui.renderer.getSafeIcon(l) : null;
                    b.setIcon(icon);
                    b.setText(text);
                    log.debug("text=" + text);
                    log.debug("icon=" + icon);
                }
            }
        } finally {
            ui.popup.invalidate();
        }
    }

    protected String getTip(Locale l) {
        boolean selected = l.equals(ui.selectedLocale);
        return selected ? getSelectedTip(l) : getNotSelectedTip(l);
    }

    protected String getSelectedTip(Locale l) {
        String selectedTip = ui.getSelectedToolTipText();
        if (selectedTip == null) {
            // use default selected tip text
            selectedTip = DEFAULT_SELECTED_TOOLTIP;
        }
        String tip = ui.renderer.getToolTipText(l);
        tip = I18n.t(selectedTip, tip);
        return tip;
    }

    protected String getNotSelectedTip(Locale l) {
        String selectedTip = ui.getNotSelectedToolTipText();
        if (selectedTip == null) {
            // use default not selected tip text
            selectedTip = DEFAULT_NOT_SELECTED_TOOLTIP;
        }
        String tip = ui.renderer.getToolTipText(l);
        tip = I18n.t(selectedTip, tip);
        return tip;
    }

    protected final Runnable showPopupRunnable = new Runnable() {
        @Override
        public void run() {
            ui.getPopup().pack();
            JToggleButton invoker = ui.getButton();
            Dimension dim = ui.getPopup().getPreferredSize();
            Dimension invokerDim = invoker.getSize();
            ui.getPopup().show(invoker, (int) (invokerDim.getWidth() - dim.getWidth()), invoker.getHeight());
//        getPopup().setVisible(true);
        }
    };

    @Override
    public void beforeInit(I18nEditor ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(I18nEditor ui) {
        ui.addPropertyChangeListener(this);
    }
}
