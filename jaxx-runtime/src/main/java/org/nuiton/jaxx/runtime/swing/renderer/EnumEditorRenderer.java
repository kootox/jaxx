package org.nuiton.jaxx.runtime.swing.renderer;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;

/**
 * Created on 08/03/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class EnumEditorRenderer<E extends Enum<E>> implements ListCellRenderer<E> {

    private final ImmutableMap<E, String> labels;

    private final DefaultListCellRenderer delegate = new DefaultListCellRenderer();

    public EnumEditorRenderer(ImmutableMap<E, String> labels) {
        this.labels = labels;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends E> list, E value, int index, boolean isSelected, boolean cellHasFocus) {
        String realValue = null;
        if (value != null) {
            realValue = labels.get(value);
        }
        return delegate.getListCellRendererComponent(list, realValue, index, isSelected, cellHasFocus);
    }
}
