/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;

/**
 * Panel which uses a {@link GridBagLayout} by default.
 *
 * @author Ethan Nicholas
 */
public class Table extends JPanel {

    private static final long serialVersionUID = 1L;

    public static final Insets DEFAULT_INSETS = new Insets(3, 3, 3, 3);

    private final GridBagConstraints tableConstraints = new GridBagConstraints();

    private GridBagConstraints rowConstraints;

    private GridBagConstraints cellConstraints;

    public Table() {
        super.setLayout(new GridBagLayout());

        tableConstraints.insets = DEFAULT_INSETS;
    }

    @Override
    public void setLayout(LayoutManager layout) {
        // do nothing
    }

    public GridBagConstraints getTableConstraints() {
        return tableConstraints;
    }

    public GridBagConstraints getRowConstraints() {
        return rowConstraints;
    }

    public GridBagConstraints getCellConstraints() {
        return cellConstraints;
    }

    public void newRow() {
        tableConstraints.gridy++;
        rowConstraints = (GridBagConstraints) tableConstraints.clone();
    }

    public void newCell() {
        rowConstraints.gridx++;
        cellConstraints = (GridBagConstraints) rowConstraints.clone();
    }
}
