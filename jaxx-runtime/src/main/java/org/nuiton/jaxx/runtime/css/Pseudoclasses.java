/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.css;

import org.nuiton.jaxx.runtime.JAXXObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class Pseudoclasses {

    public static final String NO_PSEUDOCLASS = "no pseudoclass";

    private static final Map<Object, Map<String, List<PropertyValue>>> properties = new WeakHashMap<>();

    private static class PropertyValue implements Comparable<PropertyValue> {

        private final Object value;

        private final int id;

        public PropertyValue(Object value, int id) {
            this.value = value;
            this.id = id;
        }

        public Object getValue() {
            return value;
        }

        public int getId() {
            return id;
        }

        @Override
        public int compareTo(PropertyValue o) {
            return getId() - o.getId();
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof PropertyValue)) {
                return false;
            }
            PropertyValue that = (PropertyValue) o;
            if (that.getId() != getId()) {
                return false;
            }
            if (that.getValue() == null) {
                return getValue() == null;
            }
            return that.getValue().equals(getValue());
        }

        @Override
        public int hashCode() {
            return (value != null ? value.hashCode() : 0) ^ id;
        }

        @Override
        public String toString() {
            return "PropertyValue[" + value + ", " + id + "]";
        }
    }

    private static List<PropertyValue> getPropertyList(Object object, String property) {
        Map<String, List<PropertyValue>> propertyMap = properties.computeIfAbsent(object, k -> new HashMap<>());

        List<PropertyValue> propertyList = propertyMap.computeIfAbsent(property, k -> new ArrayList<>());

        return propertyList;
    }

    public static boolean isPropertyApplied(Object object, String property, int id) {
        for (PropertyValue aPropertyList : getPropertyList(object, property)) {
            if (aPropertyList.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public static void propertyApplied(Object object, String property, Object value, int id) {
        List<PropertyValue> propertyList = getPropertyList(object, property);
        propertyList.add(new PropertyValue(value, id));
        Collections.sort(propertyList);
    }

    public static void propertyRemoved(Object object, String property, Object value, int id) {
        List<PropertyValue> propertyList = getPropertyList(object, property);
        propertyList.remove(new PropertyValue(value, id));
    }

    public static Object getCurrentValue(Object object, String property) {
        List<PropertyValue> propertyList = getPropertyList(object, property);
        if (propertyList.size() > 0) {
            return propertyList.get(propertyList.size() - 1).getValue();
        }
        return NO_PSEUDOCLASS;
    }

    public static Object applyProperty(JAXXObject parent, Object object, String property, Object newValue, Object currentValue, int id) {
        if (!isPropertyApplied(object, property, id)) {
            Object value = getCurrentValue(object, property);
            if (value == NO_PSEUDOCLASS) {
                propertyApplied(object, property, wrap(currentValue), -1);
            }
            propertyApplied(object, property, wrap(newValue), id);
            value = getCurrentValue(object, property);
            if (value instanceof DataBinding) {
                parent.applyDataBinding(((DataBinding) value).getId());
            }
            return value;
        } else {
            return currentValue;
        }
    }

    public static Object removeProperty(JAXXObject parent, Object object, String property, Object oldValue, Object currentValue, int id) {
        if (isPropertyApplied(object, property, id)) {
            Object value = getCurrentValue(object, property);
            if (value == NO_PSEUDOCLASS) {
                throw new IllegalStateException("found NO_PSEUDOCLASS value for a property which does not have a default value");
            }
            if (value instanceof DataBinding) {
                parent.removeDataBinding(((DataBinding) value).getId());
            }
            propertyRemoved(object, property, wrap(oldValue), id);
            value = getCurrentValue(object, property);
            return value;
        } else {
            return currentValue;
        }
    }

    public static Object wrap(boolean value) {
        return value;
    }

    public static Object wrap(byte value) {
        return value;
    }

    public static Object wrap(short value) {
        return value;
    }

    public static Object wrap(int value) {
        return value;
    }

    public static Object wrap(long value) {
        return value;
    }

    public static Object wrap(float value) {
        return value;
    }

    public static Object wrap(double value) {
        return value;
    }

    public static Object wrap(char value) {
        return value;
    }

    public static Object wrap(Object value) {
        return value;
    }
}
