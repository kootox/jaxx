/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.model;

import com.google.common.collect.Lists;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.Collection;
import java.util.List;

/**
 * @author Sylvain Lletellier
 */
public class GenericListModel<B> extends GenericListSelectionModel<B> implements ComboBoxModel {

    private static final long serialVersionUID = -3663608179800141530L;

    public GenericListModel() {
        super(new DefaultListModel());
    }

    public GenericListModel(Collection<B> values) {
        this();
        setElements(values);
    }

    public void setElements(Collection<B> values) {
        Collection<B> oldValues = getElements();
        Collection<B> oldSelectedValues = getSelectedValues();
        clearSelection();
        fireSelectionRemoved(oldSelectedValues);

        clearElements();
        fireValuesRemoved(oldValues);

        for (B value : values) {
            getListModel().addElement(value);
        }

        fireSelectionAdded(values);
    }

    public void clearElements() {
        Collection<B> elements = getElements();
        getListModel().clear();

        fireValuesRemoved(elements);
    }

    public Collection<B> getElements() {
        int size = getListModel().getSize();
        Collection<B> result = Lists.newArrayList();
        for (int i = 0; i < size; i++) {
            result.add((B) getListModel().get(i));
        }
        return result;
    }

    public void addElement(int index, B valueToAdd) {
        getListModel().add(index, valueToAdd);

        fireValuesAdded(Lists.newArrayList(valueToAdd));
    }

    public void addElement(B valueToAdd) {
        getListModel().addElement(valueToAdd);

        fireValuesAdded(Lists.newArrayList(valueToAdd));
    }

    public void addElements(Collection<B> valuesToAdd) {
        for (B value : valuesToAdd) {
            getListModel().addElement(value);
        }

        fireValuesAdded(valuesToAdd);
    }

    public void removeElements(Collection<B> values) {
        for (B value : values) {
            getListModel().removeElement(value);
        }
        unSelectItems(values);

        fireValuesRemoved(values);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        List<B> oldValue = getSelectedValues();
        fireSelectionRemoved(oldValue);

        setSelectedValues(Lists.newArrayList((B) anItem));

        List<B> newValues = getSelectedValues();
        fireSelectionAdded(newValues);
        fireContentsChanged(this, -1, -1);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, newValues);
    }

    @Override
    public B getSelectedItem() {
        List<B> selectedValues = getSelectedValues();
        if (selectedValues.isEmpty()) {
            return null;
        }
        return selectedValues.get(0);
    }

    @Override
    public int getSize() {
        return getListModel().size();
    }

    @Override
    public Object getElementAt(int index) {
        return getListModel().get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        getListModel().addListDataListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        getListModel().removeListDataListener(l);
    }

    protected void fireContentsChanged(Object source, int index0, int index1) {
        Object[] listeners = getListModel().getListDataListeners();
        ListDataEvent e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ListDataListener.class) {
                if (e == null) {
                    e = new ListDataEvent(source, ListDataEvent.CONTENTS_CHANGED, index0, index1);
                }
                ((ListDataListener) listeners[i + 1]).contentsChanged(e);
            }
        }
    }
}
