/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXObject;

import java.awt.Container;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Une extension de {@link CardLayout2} pour pouvoir automatiquement afficher un
 * contenu à partir de la propriété {@link #selected}.
 *
 * Ainsi, en changeant cette propriété via la méthode {@link #setSelected(String)},
 * le contenu sera changé automatiquement, ce qui permet une utilisation direct
 * dans jaxx sans à avoir à écrire d'écouteur.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see CardLayout2
 * @since 1.3
 */
public class CardLayout2Ext extends CardLayout2 {

    /** log */
    static private Log log = LogFactory.getLog(CardLayout2.class);

    private static final long serialVersionUID = 1L;

    public static final String SELECTED_PROPERTY_NAME = "selected";

    /** pour propager les changements dans le modèle  vers l'ui */
    protected final PropertyChangeSupport pcs;

    /** le contenu sélectionné */
    protected String selected;

    private final String containerName;

    private final JAXXObject ui;

    private Container container;

    public CardLayout2Ext(JAXXObject ui, String containerName) {
        pcs = new PropertyChangeSupport(this);
        this.ui = ui;
        this.containerName = containerName;
    }

    public String getSelected() {
        return selected;
    }

    public String getPreviousSelected() {
        int index = contexts.indexOf(selected);
        if (index < 1) {
            return null;
        }
        return contexts.get(index - 1) + "";
    }

    public String getNextSelected() {
        int index = contexts.indexOf(selected);
        if (index >= contexts.size()) {
            return null;
        }
        return contexts.get(index + 1) + "";
    }

    public void setSelected(String selected) {
        String oldSelected = this.selected;
        this.selected = selected;
        show(getContainer(), selected);
        pcs.firePropertyChange(SELECTED_PROPERTY_NAME, oldSelected, selected);
    }

    public Container getContainer() {
        if (container == null) {
            container = (Container) ui.getObjectById(containerName);
        }
        return container;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListeners() {
        for (PropertyChangeListener l : pcs.getPropertyChangeListeners()) {
            pcs.removePropertyChangeListener(l);
        }
    }
}
