/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import com.google.common.collect.ImmutableMap;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.lang.Objects2;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * New implementation of an {@link Action}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public abstract class ApplicationAction<UI extends JAXXObject> extends AbstractJavaBean implements Action {

    public static final String ACTION_TYPE = "actionType";
    public static final String EDITOR = "editor";
    private static final Log log = LogFactory.getLog(ApplicationAction.class);
    private static final String ENABLED = "enabled";

    private static final ImmutableMap<String, String> PROPERTY_MAPPING = ImmutableMap.<String, String>builder()
            .put(ACTION_COMMAND_KEY, "actionCommandKey")
            .put(NAME, "text")
            .put(SHORT_DESCRIPTION, "tooltipText")
            .put(ACCELERATOR_KEY, "acceleratorKey")
            .put(SMALL_ICON, "icon")
            .put(LARGE_ICON_KEY, "largeIcon")
            .put(MNEMONIC_KEY, "mnemonic")
            .put(SELECTED_KEY, "selectedKey")
            .put(DISPLAYED_MNEMONIC_INDEX_KEY, "displayMnemonicIndexKey")
            .build();
    protected UI ui;
    protected AbstractButton editor;
    private String name;
    private String text;
    private String tooltipText;
    private boolean enabled = true;
    private boolean selectedKey;
    private boolean addKeyStrokeToText = true;
    private boolean addMnemonicAsKeyStroke = true;
    private KeyStroke keyStroke;
    private int mnemonic;
    private int displayMnemonicIndexKey = -1;
    private Icon icon;
    //    private SwingPropertyChangeSupport changeSupport;
    private Icon largeIcon;

    protected ApplicationAction(String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        this(null, label, shortDescription, actionIcon, acceleratorKey);
    }

    protected ApplicationAction(String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        this(actionCommandKey, label, shortDescription, actionIcon);
        this.keyStroke = acceleratorKey;
    }

    protected ApplicationAction(String label, String shortDescription, String actionIcon, char acceleratorKey) {
        this(null, label, shortDescription, actionIcon, acceleratorKey);
    }

    protected ApplicationAction(String actionCommandKey, String label, String shortDescription, String actionIcon, char acceleratorKey) {
        this(actionCommandKey, label, shortDescription, actionIcon);
        this.mnemonic = acceleratorKey;
    }

    private ApplicationAction(String actionCommandKey, String label, String shortDescription, String actionIcon) {
        this.name = actionCommandKey == null ? getClass().getName() : actionCommandKey;
        this.text = t(label);
        this.tooltipText = t(shortDescription);
        if (actionIcon != null) {
            icon = SwingUtil.getUIManagerActionIcon(actionIcon);
        }
    }

    @SuppressWarnings("unchecked")
    public static <U extends JAXXObject, A extends ApplicationAction<U>> A init(U ui, AbstractButton editor) {
        Class<A> actionType = (Class<A>) editor.getClientProperty(ACTION_TYPE);
        return actionType == null ? null : init(ui, editor, actionType);
    }

    public static <U extends JAXXObject, A extends ApplicationAction<U>> A init(U ui, AbstractButton editor, Class<A> actionType) {

        A action = Objects2.newInstance(actionType);

        log.info(String.format("init action: %s", action.getName()));
        action.setUi(ui);
        action.setEditor(editor);
        action.init();
        return action;
    }

    public static <U extends JAXXObject, A extends ApplicationAction<U> & Runnable> void run(U ui, Class<A> actionType) {
        A action = init(ui, null, actionType);
        action.run();
    }

    public abstract void init();

    protected abstract void doActionPerformed(ActionEvent e, UI ui);

    @Override
    public final Object getValue(String key) {
        return get(PROPERTY_MAPPING.getOrDefault(key, key));
    }

    @Override
    public final void putValue(String key, Object value) {
        set(PROPERTY_MAPPING.getOrDefault(key, key), value);
    }

    @Override
    protected Class getJavaBeanType() {
        return ApplicationAction.class;
    }
//    @Override
//    public final synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
//        if (changeSupport == null) {
//            changeSupport = new SwingPropertyChangeSupport(this);
//        }
//        changeSupport.addPropertyChangeListener(listener);
//    }
//    public final boolean isEnabled() { return editor == null ? enabled : editor.isEnabled(); }

//    @Override
//    public final synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
//        if (changeSupport == null) {
//            return;
//        }
//        changeSupport.removePropertyChangeListener(listener);
//    }

    @Override
    public final boolean isEnabled() {
        return enabled;
    }

    @Override
    public final void setEnabled(boolean enabled) {
        boolean oldValue = isEnabled();
        this.enabled = enabled;
        log.debug(String.format("Action [%s] - enabled? %s → %s", getActionCommandKey(), oldValue, enabled));
        firePropertyChange(ENABLED, oldValue, enabled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (canExecuteAction(e)) {
            doActionPerformed(e, ui);
        }
    }

    protected boolean canExecuteAction(ActionEvent e) {
        AbstractButton editor = getEditor();
        Objects.requireNonNull(editor);
        if (!editor.isVisible() || !editor.isEnabled()) {
            return false;
        }
        if (editor instanceof JMenuItem) {
            return ((JMenuItem) editor).isArmed();
        }
        return editor.isShowing();
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final String getText() {
        return text;
    }

    public final void setText(String text) {
        this.text = text;
    }

    public final String getTooltipText() {
        return tooltipText;
    }

    public final void setTooltipText(String tooltipText) {
        this.tooltipText = tooltipText;
    }

    public final KeyStroke getKeyStroke() {
        return keyStroke;
    }

    public final void setKeyStroke(KeyStroke keyStroke) {
        this.keyStroke = keyStroke;
    }

    public final int getMnemonic() {
        return mnemonic;
    }

    public final void setMnemonic(int mnemonic) {
        this.mnemonic = mnemonic;
    }

    public final Icon getIcon() {
        return icon;
    }

    public final void setIcon(Icon icon) {
        this.icon = icon;
    }

    public boolean isSelectedKey() {
        return selectedKey;
    }

    public final void setSelectedKey(boolean selectedKey) {
        this.selectedKey = selectedKey;
    }

    public int getDisplayMnemonicIndexKey() {
        return displayMnemonicIndexKey;
    }

    public final void setDisplayMnemonicIndexKey(int displayMnemonicIndexKey) {
        this.displayMnemonicIndexKey = displayMnemonicIndexKey;
    }

    public boolean isAddMnemonicAsKeyStroke() {
        return addMnemonicAsKeyStroke;
    }

    public void setAddMnemonicAsKeyStroke(boolean addMnemonicAsKeyStroke) {
        this.addMnemonicAsKeyStroke = addMnemonicAsKeyStroke;
    }

    public Icon getLargeIcon() {
        return largeIcon;
    }

    public final void setLargeIcon(Icon largeIcon) {
        this.largeIcon = largeIcon;
    }

    public final KeyStroke getAcceleratorKey() {
        return keyStroke;
    }

    public final String getActionCommandKey() {
        return name;
    }

    public final UI getUi() {
        return ui;
    }

    public final void setUi(UI ui) {
        this.ui = ui;
    }

    public final AbstractButton getEditor() {
        return editor;
    }

    public void setEditor(AbstractButton editor) {
        this.editor = editor;
    }

    public boolean isAddKeyStrokeToText() {
        return addKeyStrokeToText;
    }

    public void setAddKeyStrokeToText(boolean addKeyStrokeToText) {
        this.addKeyStrokeToText = addKeyStrokeToText;
    }

    public final void register(InputMap inputMap, ActionMap actionMap) {
        if (keyStroke != null && inputMap != null && actionMap != null) {
            String actionCommandKey = getActionCommandKey();
            inputMap.put(keyStroke, actionCommandKey);
            actionMap.put(actionCommandKey, this);
        }
    }

//    public final synchronized PropertyChangeListener[] getPropertyChangeListeners() {
//        if (changeSupport == null) {
//            return new PropertyChangeListener[0];
//        }
//        return changeSupport.getPropertyChangeListeners();
//    }
//
//    public final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
//        if (changeSupport == null || Objects.equals(oldValue, newValue)) {
//            return;
//        }
//        changeSupport.firePropertyChange(propertyName, oldValue, newValue);
//    }

    public void displayInfo(String title, String text) {
        JOptionPanes.displayInfo((Component) ui, title, text);
    }

    public void displayWarning(String title, String text) {
        JOptionPanes.displayWarning((Component) ui, title, text);
    }

    public int askUser(String title, String message, int typeMessage, Object[] options, int defaultOption) {
        return JOptionPanes.askUser((Component) ui, title, message, typeMessage, options, defaultOption);
    }

    public int askUser(String title, Object message, int typeMessage, Object[] options, int defaultOption) {
        return JOptionPanes.askUser((Component) ui, title, message, typeMessage, options, defaultOption);
    }

    public int askUser(JOptionPane pane, String title, Object[] options) {
        return JOptionPanes.askUser((Frame) ui, pane, title, options);
    }

    protected void defaultInit(InputMap inputMap, ActionMap actionMap) {
        if (editor != null) {
            log.info("init action " + getActionCommandKey());
            String text = editor.getText();
            if (StringUtils.isNotEmpty(text)) {
                setText(text);
            }
            String tip = editor.getToolTipText();
            if (StringUtils.isNotEmpty(tip)) {
                setTooltipText(tip);
            }
            Icon icon = editor.getIcon();
            if (icon != null) {
                setIcon(icon);
            }
            KeyStroke keyStroke = (KeyStroke) editor.getClientProperty("keyStroke");
            if (keyStroke != null) {
                this.keyStroke = keyStroke;
            }
            if (addMnemonicAsKeyStroke && keyStroke == null && mnemonic != 0) {
                setKeyStroke(KeyStroke.getKeyStroke(mnemonic, InputEvent.ALT_MASK));
            }
            if (addKeyStrokeToText && keyStroke != null) {
                addKeyStrokeToText(getText(), getTooltipText());
            }
            setEnabled(editor.isEnabled());
            editor.setAction(this);
        }
        register(inputMap, actionMap);
    }

    private void addKeyStrokeToText(String label, String shortDescription) {
        if (keyStroke == null) {
            return;
        }
        String acceleratorStr = SwingUtil.keyStrokeToStr(keyStroke);
        setText(label == null ? null : (t(label) + acceleratorStr));
        setTooltipText(shortDescription == null ? null : (t(shortDescription) + acceleratorStr));
    }

}
