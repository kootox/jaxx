package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.JXPathDecorator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * List model for filterable lists.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.11
 */
public class JaxxFilterableListModel<E> extends JaxxDefaultListModel<E> {

    private static final long serialVersionUID = 1L;

    protected final ArrayList<E> filteredItems = Lists.newArrayList();

    protected String filterText;

    /** the decorator of data */
    protected JXPathDecorator<E> decorator;

    protected final List<Predicate<E>> filters = Lists.newArrayList();

    @Override
    public E getElementAt(int index) {
        E result;

        synchronized (this) {
            if (index >= 0 && index < filteredItems.size()) {
                result = filteredItems.get(index);
            } else {
                result = null;
            }
        }
        return result;
    }

    @Override
    public E elementAt(int index) {
        return getElementAt(index);
    }

    @Override
    public void addElement(E o) {
        super.addElement(o);
        refilter();
    }

    @Override
    public boolean removeElement(E o) {
        boolean result = super.removeElement(o);
        refilter();
        return result;
    }

    @Override
    public int getSize() {
        return filteredItems.size();
    }

    @Override
    public int size() {
        return filteredItems.size();
    }

    @Override
    public void trimToSize() {
        super.trimToSize();
        filteredItems.trimToSize();
    }

    @Override
    public void ensureCapacity(int minCapacity) {
        super.ensureCapacity(minCapacity);
        filteredItems.ensureCapacity(minCapacity);
    }

    @Override
    public void setSize(int newSize) {
        super.setSize(newSize);
        filteredItems.ensureCapacity(newSize);
    }

    @Override
    public void setElementAt(E element, int index) {
        super.setElementAt(element, index);
        refilter();
    }

    @Override
    public void removeElementAt(int index) {
        super.removeElementAt(index);
        refilter();
    }

    @Override
    public void insertElementAt(E element, int index) {
        super.insertElementAt(element, index);
        refilter();
    }

    @Override
    public void removeAllElements() {
        super.removeAllElements();
        refilter();
    }

    @Override
    public E set(int index, E element) {
        E rv = super.set(index, element);
        refilter();
        return rv;
    }

    @Override
    public void add(int index, E element) {
        super.add(index, element);
        refilter();
    }

    @Override
    public void setAllElements(Collection<E> objects) {
        super.setAllElements(objects);
        refilter();
    }

    @Override
    public void addAllElements(Collection<E> elements) {
        super.addAllElements(elements);
        refilter();
    }

    @Override
    public void removeAllElements(Collection<E> elements) {
        super.removeAllElements(elements);
        refilter();
    }

    @Override
    public E remove(int index) {
        E rv = super.remove(index);
        refilter();
        return rv;
    }

    @Override
    public void clear() {
        super.clear();
        refilter();
    }

    @Override
    public void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        refilter();
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
        refilter();
    }

    public JXPathDecorator<E> getDecorator() {
        return decorator;
    }

    public void setDecorator(JXPathDecorator<E> decorator) {
        this.decorator = decorator;
    }

    public void addFilter(Predicate<E> filter) {
        filters.add(filter);
        refilter();
    }

    public void removeFilter(Predicate<E> filter) {
        filters.remove(filter);
        refilter();
    }

    public void clearFilters() {
        filters.clear();
        refilter();
    }

    public void refreshFilteredElements() {
        refilter();
    }

    protected void refilter() {
        filteredItems.clear();

        if (StringUtils.isEmpty(StringUtils.remove(filterText, '*'))
                && filters.isEmpty()) {
            filteredItems.addAll(delegate);

        } else {
            Pattern pattern = null;
            if (!StringUtils.isBlank(filterText)) {
                String patternText = Pattern.quote(filterText).replace("*", "\\E.*\\Q") + ".*";
                pattern = Pattern.compile(patternText, Pattern.CASE_INSENSITIVE);
            }
            for (E element : delegate) {
                boolean addElement = true;
                for (Predicate<E> filter : filters) {
                    addElement &= filter.test(element);
                }
                String decoratedElement;
                if (decorator != null) {
                    decoratedElement = decorator.toString(element);
                } else {
                    decoratedElement = String.valueOf(element);
                }
                boolean matches = pattern == null
                        || pattern.matcher(decoratedElement).matches();
                if (matches && addElement) {
                    filteredItems.add(element);
                }
            }
        }

        fireContentsChanged(this, 0, getSize());
    }
}
