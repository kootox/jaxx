/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import org.nuiton.decorator.Decorator;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * A {@link TableCellRenderer} which compute text with the given {@link #decorator}
 * and leave the hand to the {@link #delegate} to perform the visual renderer.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.7.2 (was previously {@code jaxx.runtime.swing.DecoratorTableCellRenderer}).
 */
public class DecoratorTableCellRenderer implements TableCellRenderer {

    /** Delegate cell renderer */
    protected final TableCellRenderer delegate;

    /** Decorator to produce text to render */
    protected final Decorator<?> decorator;

    protected boolean showToolTipText = false;

    public DecoratorTableCellRenderer(Decorator<?> decorator) {
        this(new DefaultTableCellRenderer(), decorator, false);
    }

    public DecoratorTableCellRenderer(Decorator<?> decorator, boolean showToolTipText) {
        this(new DefaultTableCellRenderer(), decorator, showToolTipText);
    }

    public DecoratorTableCellRenderer(TableCellRenderer delegate, Decorator<?> decorator) {
        this(new DefaultTableCellRenderer(), decorator, false);
    }

    public DecoratorTableCellRenderer(TableCellRenderer delegate,
                                      Decorator<?> decorator,
                                      boolean showToolTipText) {
        this.delegate = delegate;
        this.decorator = decorator;
        this.showToolTipText = showToolTipText;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasfocus, int row, int column) {
        String text = null;
        if (value != null) {
            text = decorator.toString(value);
        }
        JComponent result =
                (JComponent) delegate.getTableCellRendererComponent(table,
                                                                    text,
                                                                    isSelected,
                                                                    hasfocus,
                                                                    row,
                                                                    column);
        if (showToolTipText) {
            result.setToolTipText(text);
        }
        return result;
    }
}
