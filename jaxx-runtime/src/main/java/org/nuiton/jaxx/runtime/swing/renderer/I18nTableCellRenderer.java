/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * A simple TableCellRenderer using a delegate TableCellRenderer to render
 * everything elese thant the text : the text is I18nalize.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTableCellRenderer implements TableCellRenderer {

    /** i18n keys of  libelles to display */
    protected final String[] keys;

    /** i18n keys of toolTipTexts to display */
    protected final String[] tips;

    /** the delegate cell renderer */
    protected final TableCellRenderer delegate;

    public I18nTableCellRenderer(TableCellRenderer delegate,
                                 String... keysAndTips) {
        this.delegate = delegate;
        if (keysAndTips.length == 0) {
            throw new IllegalArgumentException(
                    "can not have empty keysAndTips parameters (means no " +
                            "column ?)");
        }
        if (keysAndTips.length % 2 == 1) {
            throw new IllegalArgumentException(
                    "must have some couple (text,tooltTipText), but had an" +
                            " even number of data in keysAndTips parameter");
        }
        int size = keysAndTips.length / 2;
        keys = new String[size];
        tips = new String[size];
        for (int i = 0; i < size; i++) {
            keys[i] = keysAndTips[2 * i];
            tips[i] = keysAndTips[2 * i + 1];
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasfocus,
                                                   int row,
                                                   int column) {
        if (column > keys.length) {
            throw new IndexOutOfBoundsException(
                    "colum can not be greater than " + keys.length);
        }
        TableColumn col = table.getColumn(table.getColumnName(column));
        int index = col.getModelIndex();
        value = t(keys[index]);
        JComponent rendererComponent = (JComponent)
                delegate.getTableCellRendererComponent(
                        table,
                        value,
                        isSelected,
                        hasfocus,
                        row,
                        column
                );
        rendererComponent.setToolTipText(t(tips[index]));
        return rendererComponent;
    }

    public String[] getKeys() {
        return keys;
    }

    public String[] getTips() {
        return tips;
    }

}
