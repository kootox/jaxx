/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.binding;

import org.nuiton.jaxx.runtime.JAXXObject;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public abstract class SimpleJAXXObjectBinding extends DefaultJAXXBinding {

    protected final String[] propertyNames;

    /**
     * Creates a new Data binding which will run the given data binding
     * when it receives a <code>PropertyChangeEvent</code>.
     *
     * @param source         the {@link JAXXObject} source of the binding
     * @param id             the name of the data binding to run
     * @param defaultBinding flag to knwon if binding is coming from a generated jaxx object ({@code true}).
     * @param propertyNames  the name of properties to listen on source
     */
    public SimpleJAXXObjectBinding(JAXXObject source, String id, boolean defaultBinding, String... propertyNames) {
        super(source, id, defaultBinding);
        if (propertyNames == null || propertyNames.length == 0) {
            throw new IllegalArgumentException("must at least have one propertyName ");
        }
        this.propertyNames = propertyNames;
    }

    public boolean canApply() {
        return true;
    }

    public String[] getPropertyNames() {
        return propertyNames;
    }

    @Override
    public void applyDataBinding() {
        if (canApply()) {
            for (String s : propertyNames) {
                source.addPropertyChangeListener(s, this);
            }
        }
    }

    @Override
    public void removeDataBinding() {
        if (canApply()) {
            for (String s : propertyNames) {
                source.removePropertyChangeListener(s, this);
            }
        }
    }
}
