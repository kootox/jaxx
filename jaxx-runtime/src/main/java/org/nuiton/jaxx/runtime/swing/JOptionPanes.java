package org.nuiton.jaxx.runtime.swing;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;
import javax.swing.JDialog;
import javax.swing.JOptionPane;


import static javax.swing.JOptionPane.CLOSED_OPTION;
import static javax.swing.JOptionPane.VALUE_PROPERTY;

/**
 * Created by tchemit on 11/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JOptionPanes {

    public static void displayInfo(Component ui, String title, String text) {

        JOptionPane.showMessageDialog(ui, text, title, JOptionPane.INFORMATION_MESSAGE);

    }

    public static void displayWarning(Component ui, String title, String text) {

        JOptionPane.showMessageDialog(ui, text, title, JOptionPane.WARNING_MESSAGE);

    }

//    public static int askUser(Component ui,
//                              String title,
//                              String message,
//                              int typeMessage,
//                              Object[] options,
//                              int defaultOption) {
//        return askUser(
//                ui,
//                title,
//                message,
//                typeMessage,
//                options,
//                defaultOption
//        );
//    }

    public static int askUser(Component parent,
                              String title,
                              Object message,
                              int typeMessage,
                              Object[] options,
                              int defaultOption) {
        Objects.requireNonNull(parent);
        return JOptionPane.showOptionDialog(
                parent,
                message,
                title,
                JOptionPane.DEFAULT_OPTION,
                typeMessage,
                null,
                options,
                options[defaultOption]
        );
    }

    public static int askUser(Frame ui, JOptionPane pane, String title, Object[] options) {

        JDialog dialog = new JDialog(ui, true);
        dialog.setTitle(title);

        Container contentPane = dialog.getContentPane();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(pane, BorderLayout.CENTER);
        dialog.setResizable(false);

        dialog.pack();
        dialog.setLocationRelativeTo(ui);

        final PropertyChangeListener listener = event -> {
            // Let the defaultCloseOperation handle the closing
            // if the user closed the window without selecting a button
            // (newValue = null in that case).  Otherwise, closeData the dialog.
            if (dialog.isVisible() && event.getSource() == pane &&
                    (event.getPropertyName().equals(VALUE_PROPERTY)) &&
                    event.getNewValue() != null &&
                    event.getNewValue() != JOptionPane.UNINITIALIZED_VALUE) {
                dialog.setVisible(false);
            }
        };

        WindowAdapter adapter = new WindowAdapter() {
            private boolean gotFocus = false;

            public void windowClosing(WindowEvent we) {
                pane.setValue(null);
            }

            public void windowClosed(WindowEvent e) {
                pane.removePropertyChangeListener(listener);
                dialog.getContentPane().removeAll();
            }

            public void windowGainedFocus(WindowEvent we) {
                // Once window gets focus, set initial focus
                if (!gotFocus) {
                    pane.selectInitialValue();
                    gotFocus = true;
                }
            }
        };
        dialog.addWindowListener(adapter);
        dialog.addWindowFocusListener(adapter);
        dialog.addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                // reset value to ensure closing works properly
                pane.setValue(JOptionPane.UNINITIALIZED_VALUE);
            }
        });

        pane.addPropertyChangeListener(listener);

        dialog.setVisible(true);
        Object selectedValue = pane.getValue();

        if (selectedValue == null)
            return CLOSED_OPTION;
        for (int counter = 0, maxCounter = options.length;
             counter < maxCounter; counter++) {
            if (options[counter].equals(selectedValue))
                return counter;
        }
        return CLOSED_OPTION;
    }
}
