/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * A {@link TableCellRenderer} which use decorators from the {@link #provider} to obtain the text to display.
 *
 * The interest of this renderer is to define a unique renderer for your application (put it in JAXXContext)
 * and then use it simply :)
 *
 * User: chemit
 * Date: 29 oct. 2009
 * Time: 03:00:53
 *
 * @see DecoratorProvider
 * @since 2.0.0
 */
public class DecoratorProviderTableCellRenderer implements TableCellRenderer {

    /** Delegate cell renderer */
    protected final TableCellRenderer delegate;

    /** provider of decorators */
    protected final DecoratorProvider provider;

    public DecoratorProviderTableCellRenderer(DecoratorProvider provider) {
        this(new DefaultTableCellRenderer(), provider);
    }

    public DecoratorProviderTableCellRenderer(TableCellRenderer delegate, DecoratorProvider provider) {
        this.delegate = delegate;
        this.provider = provider;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasfocus, int row, int column) {
        if (value != null) {
            Decorator<?> decorator = provider.getDecorator(value);

            if (decorator != null) {
                value = decorator.toString(value);
            }
        }
        return delegate.getTableCellRendererComponent(table, value, isSelected, hasfocus, row, column);
    }
}
