package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.ComboBoxModel;

/**
 * A new comboBox model that add supports for {@link WillChangeSelectedItemListener}.
 *
 * Created on 8/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public interface ComboBoxModel2 extends ComboBoxModel {

    /**
     * Adds a listener to the list that's notified each time the selected item should be changed.
     *
     * @param l the <code>ListDataListener</code> to be added
     */
    void addWillChangeSelectedItemListener(WillChangeSelectedItemListener l);

    /**
     * Adds a listener to the list that's notified each time the selected item should be changed.
     *
     * @param l the <code>ListDataListener</code> to be removed
     */
    void removeWillChangeSelectedItemListener(WillChangeSelectedItemListener l);
}
