package org.nuiton.jaxx.runtime.awt.visitor;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Component;

/**
 * To print a tree node.
 *
 * Created on 1/10/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.7
 */
public class DebugComponentTreeNodeVisitor implements ComponentTreeNodeVisitor {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DebugComponentTreeNodeVisitor.class);

    /**
     * Flag to log in debug level (otherwise will be in info level).
     */
    protected boolean debug;

    protected int level = 0;

    public void parse(ComponentTreeNode componentTree) {

        sb = new StringBuilder();
        componentTree.visit(this);

        String message = sb.toString();
        if (debug) {
            log.debug(message);
        } else {
            log.info(message);
        }
    }

    protected StringBuilder sb = new StringBuilder();

    @Override
    public void startNode(ComponentTreeNode componentTree) {
        String message = StringUtils.leftPad(" ", 2 * level);
        sb.append("\n").append(message).append(getMessage(componentTree));
        level++;
    }

    @Override
    public void endNode(ComponentTreeNode componentTree) {
        level--;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getMessage(ComponentTreeNode componentTree) {
        Component userObject = componentTree.getUserObject();
        return userObject.getClass().getSimpleName() + "::" + userObject.getName();
    }
}
