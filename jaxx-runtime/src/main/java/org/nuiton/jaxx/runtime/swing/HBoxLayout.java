/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

/**
 * Horizontal box layout.  The layout rules followed by this class are quite different than the core BoxLayout class,
 * and in general represent a more useful algorithm.
 *
 * @author Ethan Nicholas
 */
public class HBoxLayout implements LayoutManager {

    private int spacing = 6;

    private int horizontalAlignment = SwingConstants.LEFT;

    private int verticalAlignment = SwingConstants.TOP;

    public int getSpacing() {
        return spacing;
    }

    public void setSpacing(int spacing) {
        this.spacing = spacing;
    }

    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public int getVerticalAlignment() {
        return verticalAlignment;
    }

    public void setVerticalAlignment(int verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();
        int parentHeight = parent.getSize().height - insets.top - insets.bottom;
        int count = parent.getComponentCount();
        Dimension preferredSize = parent.getPreferredSize();
        int x;
        switch (horizontalAlignment) {
            case SwingConstants.LEFT:
                x = insets.left;
                break;
            case SwingConstants.CENTER:
                x = insets.left + (parent.getWidth() - preferredSize.width) / 2;
                break;
            case SwingConstants.RIGHT:
                x = insets.left + (parent.getWidth() - preferredSize.width);
                break;
            default:
                throw new IllegalArgumentException("invalid horizontal alignment: " + horizontalAlignment);
        }

        for (int i = 0; i < count; i++) {
            Component component = parent.getComponent(i);
            Dimension childPreferredSize = component.getPreferredSize();
            int height = Math.min(childPreferredSize.height, parentHeight);
            int y;
            switch (verticalAlignment) {
                case SwingConstants.TOP:
                    y = insets.top;
                    break;
                case SwingConstants.CENTER:
                    y = insets.top + (parentHeight - childPreferredSize.height) / 2;
                    break;
                case SwingConstants.BOTTOM:
                    y = insets.top + (parentHeight - childPreferredSize.height);
                    break;
                default:
                    throw new IllegalArgumentException("invalid vertical alignment: " + verticalAlignment);
            }
            component.setBounds(x, y, childPreferredSize.width, height);
            x += childPreferredSize.width + spacing;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        int width = (parent.getComponentCount() - 1) * spacing;
        int height = 0;
        for (int i = parent.getComponentCount() - 1; i >= 0; i--) {
            Dimension minimumSize = parent.getComponent(i).getMinimumSize();
            width += minimumSize.width;
            height = Math.max(height, minimumSize.height);
        }
        Insets insets = parent.getInsets();
        return new Dimension(width + insets.left + insets.right, height + insets.top + insets.bottom);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        int width = (parent.getComponentCount() - 1) * spacing;
        int height = 0;
        for (int i = parent.getComponentCount() - 1; i >= 0; i--) {
            Dimension preferredSize = parent.getComponent(i).getPreferredSize();
            width += preferredSize.width;
            height = Math.max(height, preferredSize.height);
        }
        Insets insets = parent.getInsets();
        return new Dimension(width + insets.left + insets.right, height + insets.top + insets.bottom);
    }

    @Override
    public void removeLayoutComponent(Component comp) {
    }
}
