/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import java.awt.Color;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.jdesktop.beans.AbstractBean;

public class TabInfo extends AbstractBean {

    public static final String BACKGROUND_PROPERTY = "background";

    public static final String TAB_COMPONENT_PROPERTY = "tabComponent";

    public static final String TAB_COMPONENT_STR_PROPERTY = "tabComponentStr";

    public static final String DISABLED_ICON_PROPERTY = "disabledIcon";

    public static final String DISPLAYED_MNEMONIC_INDEX_PROPERTY = "displayedMnemonicIndex";

    public static final String ENABLED_PROPERTY = "enabled";

    public static final String FOREGROUND_PROPERTY = "foreground";

    public static final String ICON_PROPERTY = "icon";

    public static final String MNEMONIC_PROPERTY = "mnemonic";

    public static final String TITLE_PROPERTY = "title";

    public static final String TOOL_TIP_TEXT_PROPERTY = "toolTipText";

    public static final String VALID_PROPERTY = "valid";

    private String id;

    private Color background;

    private Icon disabledIcon;

    private int displayedMnemonicIndex = -1;

    private boolean enabled = true;

    private Color foreground;

    private Icon icon;

    private int mnemonic = -1;

    private String title;

    private String toolTipText;

    private JComponent tabComponent;

    private String tabComponentStr;

//    private PropertyChangeSupport propertyChangeSupport;

    private Set<String> properties;

    private boolean valid;

    public TabInfo() {
    }

    public TabInfo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Color getBackground() {
        return background;
    }

    public void setBackground(Color background) {
        Color oldValue = this.background;
        this.background = background;
        firePropertyChange(BACKGROUND_PROPERTY, oldValue, background);
    }

    public Icon getDisabledIcon() {
        return disabledIcon;
    }

    public void setDisabledIcon(Icon disabledIcon) {
        Icon oldValue = this.disabledIcon;
        this.disabledIcon = disabledIcon;
        firePropertyChange(DISABLED_ICON_PROPERTY, oldValue, disabledIcon);
    }

    public int getDisplayedMnemonicIndex() {
        return displayedMnemonicIndex;
    }

    public void setDisplayedMnemonicIndex(int displayedMnemonicIndex) {
        int oldValue = this.displayedMnemonicIndex;
        this.displayedMnemonicIndex = displayedMnemonicIndex;
        firePropertyChange(DISPLAYED_MNEMONIC_INDEX_PROPERTY, oldValue, displayedMnemonicIndex);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        boolean oldValue = this.enabled;
        this.enabled = enabled;
        firePropertyChange(ENABLED_PROPERTY, oldValue, enabled);
    }

    public Color getForeground() {
        return foreground;
    }

    public void setForeground(Color foreground) {
        Color oldValue = this.foreground;
        this.foreground = foreground;
        firePropertyChange(FOREGROUND_PROPERTY, oldValue, foreground);
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        Icon oldValue = this.icon;
        this.icon = icon;
        firePropertyChange(ICON_PROPERTY, oldValue, icon);
    }

    public int getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(int mnemonic) {
        int oldValue = this.mnemonic;
        this.mnemonic = mnemonic;
        firePropertyChange(MNEMONIC_PROPERTY, oldValue, mnemonic);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        String oldValue = this.title;
        this.title = title;
        firePropertyChange(TITLE_PROPERTY, oldValue, title);
    }

    public String getToolTipText() {
        return toolTipText;
    }

    public void setToolTipText(String toolTipText) {
        String oldValue = this.toolTipText;
        this.toolTipText = toolTipText;
        firePropertyChange(TOOL_TIP_TEXT_PROPERTY, oldValue, toolTipText);
    }

    public JComponent getTabComponent() {
        return tabComponent;
    }

    public void setTabComponent(JComponent tabComponent) {
        JComponent oldValue = this.tabComponent;
        this.tabComponent = tabComponent;
        firePropertyChange(TAB_COMPONENT_PROPERTY, oldValue, tabComponent);
    }


    public String getTabComponentStr() {
        return tabComponentStr;
    }

    public void setTabComponentStr(String tabComponentStr) {
        String oldValue = this.tabComponentStr;
        this.tabComponentStr = tabComponentStr;
        firePropertyChange(TAB_COMPONENT_STR_PROPERTY, oldValue, tabComponent);
    }

//    private PropertyChangeSupport getPropertyChangeSupport() {
//        if (propertyChangeSupport == null) {
//            propertyChangeSupport = new SwingPropertyChangeSupport(this);
//        }
//        return propertyChangeSupport;
//    }

//    public void addPropertyChangeListener(PropertyChangeListener listener) {
//        getPropertyChangeSupport().addPropertyChangeListener(listener);
//    }
//
//    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
//        getPropertyChangeSupport().addPropertyChangeListener(property, listener);
//    }
//
//    public void removePropertyChangeListener(PropertyChangeListener listener) {
//        getPropertyChangeSupport().removePropertyChangeListener(listener);
//    }
//
//    public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
//        getPropertyChangeSupport().removePropertyChangeListener(property, listener);
//    }
//
//    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
//        if (propertyChangeSupport != null) {
//            getPropertyChangeSupport().firePropertyChange(propertyName, oldValue, newValue);
//        }
//    }

    public Set<String> getProperties() {
        return properties;
    }

    public void setProperties(Set<String> properties) {
        this.properties = properties;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
        firePropertyChange(VALID_PROPERTY, null, valid);
    }
}
