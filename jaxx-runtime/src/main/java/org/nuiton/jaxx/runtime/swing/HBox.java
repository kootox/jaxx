/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.JPanel;
import java.awt.Insets;

/**
 * Panel which uses an {@link HBoxLayout} by default.
 *
 * @author Ethan Nicholas
 */
public class HBox extends JPanel {

    private static final long serialVersionUID = 1L;

    public static final String SPACING_PROPERTY = "spacing";

    public static final String MARGIN_PROPERTY = "margin";

    public static final String HORIZONTAL_ALIGNMENT_PROPERTY = "horizontalAlignment";

    public static final String VERTICAL_ALIGNMENT_PROPERTY = "verticalAlignment";

    private Insets margin;

    public HBox() {
        super(new HBoxLayout());
    }

    /**
     * Returns the spacing between components, in pixels.   Spacing is applied between components only,
     * not to the top or bottom of the container.
     *
     * @return spacing between components
     */
    public int getSpacing() {
        return ((HBoxLayout) getLayout()).getSpacing();
    }

    /**
     * Sets the spacing between components.   Spacing is applied between components only,
     * not to the top or bottom of the container.
     *
     * @param spacing new spacing value
     */
    public void setSpacing(int spacing) {
        int oldValue = getSpacing();
        ((HBoxLayout) getLayout()).setSpacing(spacing);
        firePropertyChange(SPACING_PROPERTY, oldValue, spacing);
        revalidate();
    }

    public int getHorizontalAlignment() {
        return ((HBoxLayout) getLayout()).getHorizontalAlignment();
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        int oldValue = getHorizontalAlignment();
        ((HBoxLayout) getLayout()).setHorizontalAlignment(horizontalAlignment);
        firePropertyChange(HORIZONTAL_ALIGNMENT_PROPERTY, oldValue, horizontalAlignment);
        revalidate();
    }

    public int getVerticalAlignment() {
        return ((HBoxLayout) getLayout()).getVerticalAlignment();
    }

    public void setVerticalAlignment(int verticalAlignment) {
        int oldValue = getVerticalAlignment();
        ((HBoxLayout) getLayout()).setVerticalAlignment(verticalAlignment);
        firePropertyChange(VERTICAL_ALIGNMENT_PROPERTY, oldValue, verticalAlignment);
        revalidate();
    }

    public Insets getMargin() {
        return margin;
    }

    public void setMargin(Insets margin) {
        Insets oldValue = this.margin;
        this.margin = (Insets) margin.clone();
        firePropertyChange(MARGIN_PROPERTY, oldValue, margin);
    }

    @Override
    public Insets getInsets() {
        Insets result = super.getInsets();
        if (margin != null) {
            result.top += margin.top;
            result.left += margin.left;
            result.right += margin.right;
            result.bottom += margin.bottom;
        }
        return result;
    }
}
