/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.context;

import org.nuiton.jaxx.runtime.JAXXContext;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * To qualify an entry in a {@link JAXXContext}.
 *
 * Use the factory methods <code>newContextEntryDef</code> and
 * <code>newListContextEntryDef</code> to obtain new instances.
 *
 * @param <O> type of the entry associated to the definition
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JAXXContextEntryDef<O> implements Serializable {

    /** name of the entry, can be nuill for a unamed entry. */
    protected String name;

    /** class of the entry, can not be null */
    protected Class<O> klass;

    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public Class<O> getKlass() {
        return klass;
    }

    public O getContextValue(JAXXContext context) {
        return context.getContextValue(klass, name);
    }

    public void removeContextValue(JAXXContext context) {
        context.removeContextValue(klass, name);
    }

    public void setContextValue(JAXXContext context, O value) {
        context.setContextValue(value, name);
    }

    @Override
    public String toString() {
        return super.toString() + "<" + klass + ":" + name + ">";
    }

    public JAXXContextEntryDef(Class<O> klass) {
        this(null, klass);
    }

    /**
     * Special constructor for map, otherwise it is not possible to cast to O
     *
     * @param mapClass map class
     * @param name     name of content
     * @since 2.0.2
     */
    @SuppressWarnings({"unchecked"})
    public JAXXContextEntryDef(Class<Map> mapClass, String name) {
        this(name, (Class<O>) mapClass);
    }

    @SuppressWarnings("unchecked")
    public JAXXContextEntryDef(String name, Class<O> klass) {
        if (klass == null) {
            throw new IllegalArgumentException("class can not be null");
        }
        this.name = name;
        if (List.class.isAssignableFrom(klass)) {
            klass = (Class<O>) List.class;
        }
        this.klass = klass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JAXXContextEntryDef<?>)) {
            return false;
        }
        JAXXContextEntryDef<?> that = (JAXXContextEntryDef<?>) o;
        return klass.equals(that.klass) &&
                !(name != null ? !name.equals(that.name) : that.name != null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        return 31 * result + klass.hashCode();
    }

    public boolean accept(Class<?> klass, String name) {
        if (Object.class.equals(klass) && !Object.class.equals(this.klass)) {
            // try on name only
            return this.name != null && name != null && this.name.equals(name);
        }
        return klass.isAssignableFrom(this.klass) && (this.name == null && name == null
                || (this.name != null && name != null && this.name.equals(name)));
    }

    public boolean accept2(Class<?> klass, String name) {
        return !(Object.class.equals(klass) &&
                !Object.class.equals(this.klass)) &&
                this.klass.isAssignableFrom(klass) &&
                (this.name == null && name == null ||
                        (this.name != null && name != null && this.name.equals(name))
                );
    }
}
