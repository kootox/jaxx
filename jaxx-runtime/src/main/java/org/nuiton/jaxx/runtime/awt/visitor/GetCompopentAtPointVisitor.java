package org.nuiton.jaxx.runtime.awt.visitor;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

/**
 * A visitor to get the deepest component given a point in a tree.
 *
 * To obtain the component use the method {@link #get(ComponentTreeNode, Point)}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.14
 */
public class GetCompopentAtPointVisitor implements ComponentTreeNodeVisitor {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(GetCompopentAtPointVisitor.class);

    final int currentX;

    final int currentY;

    final List<ComponentTreeNode> components = Lists.newArrayList();

    public static Component get(ComponentTreeNode node, Point p) {

        GetCompopentAtPointVisitor v = new GetCompopentAtPointVisitor(p);
        node.visit(v);
        Component result = v.getLastComponent();
        v.components.clear();
        return result;
    }

    public GetCompopentAtPointVisitor(Point p) {
        currentX = (int) p.getX();
        currentY = (int) p.getY();
    }

    public Component getLastComponent() {
        Component result = null;
        if (!components.isEmpty()) {
            ComponentTreeNode node = components.get(components.size() - 1);
            result = node.getUserObject();
        }
        return result;
    }

    @Override
    public void startNode(ComponentTreeNode node) {
        Component component = node.getUserObject();

        if (!(component.isVisible() && component.isShowing())) {
            if (log.isDebugEnabled()) {
                log.debug("Skip invisible component: " +
                                  component.getClass().getSimpleName() + "::" +
                                  component.getName());
            }
            return;
        }

        // check parent is ok
        if (node.isRoot() || components.contains(node.getParent())) {

            Point locationOnScreen = component.getLocationOnScreen();
            Rectangle bounds = component.getBounds();
            bounds.setLocation(locationOnScreen);

            boolean containsPoint = bounds.contains(currentX, currentY);

            if (containsPoint) {

                if (log.isDebugEnabled()) {
                    log.debug("Accept component: " + component);
                }

                // keep this node
                components.add(node);
            }
        }
    }

    @Override
    public void endNode(ComponentTreeNode componentTree) {
    }
}
