/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Enumeration;

public class JAXXButtonGroup extends ButtonGroup {

    public static final String SELECTED_VALUE_PROPERTY = "selectedValue";

    public static final String BUTTON8GROUP_CLIENT_PROPERTY = "$buttonGroup";

    public static final String VALUE_CLIENT_PROPERTY = "$value";

    public static final String SELECTED_TIP_CLIENT_PROPERTY = "$selected.toolTipText";

    public static final String NOT_SELECTED_TIP_CLIENT_PROPERTY = "$not.selected.toolTipText";

    protected final EventListenerList listenerList = new EventListenerList();

    private PropertyChangeSupport propertyChangeSupport;

    private transient Object selectedValue;

    protected boolean useToolTipText;

    protected transient ChangeEvent changeEvent = new ChangeEvent(this);

    private final transient ChangeListener changeListener = new ChangeListener() {

        @Override
        public void stateChanged(ChangeEvent e) {
            updateSelectedValue();
            if (useToolTipText) {
                updateToolTipText();
            }
        }
    };

    private static final long serialVersionUID = 1L;

    @Override
    public void add(AbstractButton button) {
        super.add(button);
        button.addChangeListener(changeListener);
        updateSelectedValue();
    }

    @Override
    public void remove(AbstractButton button) {
        super.remove(button);
        button.removeChangeListener(changeListener);
        updateSelectedValue();
    }

    public void updateSelectedValue() {
        Enumeration<AbstractButton> e = getElements();
        while (e.hasMoreElements()) {
            AbstractButton button = e.nextElement();
            if (button.isSelected()) {
                Object buttonValue = button.getClientProperty(VALUE_CLIENT_PROPERTY);
                if (buttonValue != getSelectedValue()) {
                    setSelectedValue(buttonValue);
                }
            }
        }
    }

    public void updateToolTipText() {
        Enumeration<AbstractButton> e = getElements();
        while (e.hasMoreElements()) {
            AbstractButton button = e.nextElement();
            String key = button.isSelected() ? SELECTED_TIP_CLIENT_PROPERTY : NOT_SELECTED_TIP_CLIENT_PROPERTY;
            button.setToolTipText((String) button.getClientProperty(key));
        }
    }

    public boolean isUseToolTipText() {
        return useToolTipText;
    }

    public Object getSelectedValue() {
        return selectedValue;
    }

    public AbstractButton getSelectedButton() {
        Enumeration<AbstractButton> e = getElements();
        while (e.hasMoreElements()) {
            AbstractButton button = e.nextElement();
            if (button.isSelected()) {
                return button;
            }
        }
        return null;
    }

    public AbstractButton getButton(Object value) {
        Enumeration<AbstractButton> e = getElements();
        while (e.hasMoreElements()) {
            AbstractButton button = e.nextElement();
            Object buttonValue = button.getClientProperty(VALUE_CLIENT_PROPERTY);
            if (value.equals(buttonValue)) {
                return button;
            }
        }
        return null;
    }

    public void setSelectedValue(Object value) {
        Object oldValue = getSelectedValue();
        this.selectedValue = value;
        firePropertyChange(oldValue);
    }

    public void setUseToolTipText(boolean useToolTipText) {
        this.useToolTipText = useToolTipText;
    }

    public void setSelectedButton(Object value) {
        setSelectedValue(value);
        if (value == null) {
            Enumeration<AbstractButton> e = getElements();
            while (e.hasMoreElements()) {
                AbstractButton button = e.nextElement();
                setSelected(button.getModel(), false);
            }
            return;
        }

        Enumeration<AbstractButton> e = getElements();
        while (e.hasMoreElements()) {
            AbstractButton button = e.nextElement();
            Object buttonValue = button.getClientProperty(VALUE_CLIENT_PROPERTY);
            if (value.equals(buttonValue)) {
                button.setSelected(true);
                break;
            }
        }
    }

    protected PropertyChangeSupport getPropertyChangeSupport() {
        if (propertyChangeSupport == null) {
            propertyChangeSupport = new PropertyChangeSupport(this);
        }
        return propertyChangeSupport;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(property, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(property, listener);
    }

    private void firePropertyChange(Object oldValue) {
        if (propertyChangeSupport != null) {
            getPropertyChangeSupport().firePropertyChange(SELECTED_VALUE_PROPERTY,
                                                          oldValue, getSelectedValue());
        }
        fireStateChanged();
    }

    /**
     * Adds a <code>ChangeListener</code> to the button.
     *
     * @param l the listener to be added
     */
    public void addChangeListener(ChangeListener l) {
        listenerList.add(ChangeListener.class, l);
    }

    /**
     * Removes a ChangeListener from the button.
     *
     * @param l the listener to be removed
     */
    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }

    /**
     * Returns an array of all the <code>ChangeListener</code>s added
     * to this AbstractButton with addChangeListener().
     *
     * @return all of the <code>ChangeListener</code>s added or an empty
     * array if no listeners have been added
     * @since 1.4
     */
    public ChangeListener[] getChangeListeners() {
        return listenerList.getListeners(ChangeListener.class);
    }

    /**
     * Notifies all listeners that have registered interest for
     * notification on this event type.  The event instance
     * is lazily created.
     *
     * @see EventListenerList
     */
    protected void fireStateChanged() {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ChangeListener.class) {
                // Lazily create the event:
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
            }
        }
    }
}
