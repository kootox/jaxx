/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.css;

import java.util.Map;

public class Rule implements java.io.Serializable, Comparable<Rule> {

    public static final String INLINE_ATTRIBUTE = "<inline attribute>";

    public static final String DATA_BINDING = "<data binding>";

    private final Selector[] selectors;

    private final Map<String, String> properties;

    private static final long serialVersionUID = 1L;

    public Rule(Selector[] selectors, Map<String, String> properties) {
        this.selectors = selectors;
        java.util.Arrays.sort(selectors);
        this.properties = properties;
    }

    public Rule(Selector[] selectors, String[] keys, String[] values) {
        this.selectors = selectors;
        java.util.Arrays.sort(selectors);
        this.properties = new java.util.HashMap<>();
        if (keys.length != values.length) {
            throw new IllegalArgumentException("keys and values must have the same number of entries");
        }
        for (int i = 0; i < keys.length; i++) {
            properties.put(keys[i], values[i]);
        }
    }

    public Selector[] getSelectors() {
        return selectors;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public int compareTo(Rule o) {
        return selectors[0].compareTo(o.selectors[0]); // they are already sorted so we only need to compare the highest-ranked from each one
    }

    @Override
    public String toString() {
        return "Rule[" + java.util.Arrays.asList(selectors) + ", " + properties + "]";
    }
}
