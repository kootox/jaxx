/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * A <code>PropertyChangeListener</code> which processes a data binding when it receives a
 * <code>PropertyChangeEvent</code>.
 */
public class DataBindingListener implements PropertyChangeListener {

    private final JAXXObject object;
    private final String dest;

    /**
     * Creates a new <code>DataBindingListener</code> which will run the given data binding
     * when it receives a <code>PropertyChangeEvent</code>.
     *
     * @param object the object in which the data binding exists
     * @param dest   the name of the data binding to run
     */
    public DataBindingListener(JAXXObject object, String dest) {
        this.object = object;
        this.dest = dest;
    }


    /**
     * Processes the data binding in response to a <code>PropertyChangeEvent</code>.
     *
     * @param e the event which triggered the binding
     */
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        object.processDataBinding(dest);

        // for now, handle dependency changes by always removing & reapplying
        // the binding.  We should be more efficient and only do this when it's
        // actually necessary
        object.removeDataBinding(dest);
        object.applyDataBinding(dest);
    }
}
