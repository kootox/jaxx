/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.runtime.swing.tree;

import javax.swing.tree.TreeModel;

/**
 * Simple API to filter nodes from a tree model using a {@link FilterTreeModel}.
 * <p>
 * Date : 4 mars 2010
 *
 * @author chatellier
 * @author Tony Chemit - dev@tchemit.fr
 * @see FilterTreeModel
 */
public interface TreeFilter {

    /**
     * @param model delegate model from the {@link FilterTreeModel#delegateModel}.
     * @param node  node to test
     * @return {@code true} if the given node is included in the {@link FilterTreeModel}, {@code false} otherwise.
     */
    boolean include(TreeModel model, Object node);
}
