/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.GraphicsConfiguration;

public class Application extends JFrame {

    private static final long serialVersionUID = 1L;
    // Special: jaxxc will automatically add a main() method to any components which
    // extend <Application>

    public Application() {
    }

    public Application(GraphicsConfiguration gc) {
        super(gc);
    }

    public Application(String title) {
        super(title);
    }

    public Application(String title, GraphicsConfiguration gc) {
        super(title, gc);
    }

    public void setLookAndFeel(String lookAndFeel) {
        if (lookAndFeel.equals("system")) {
            lookAndFeel = UIManager.getSystemLookAndFeelClassName();
        } else if (lookAndFeel.equals("cross_platform")) {
            lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
        }
        try {
            UIManager.setLookAndFeel(lookAndFeel);
            if (isDisplayable()) {
                SwingUtilities.updateComponentTreeUI(this);
            }
        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }
}
