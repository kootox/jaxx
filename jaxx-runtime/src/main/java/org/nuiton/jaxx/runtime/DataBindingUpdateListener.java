/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * A <code>PropertyChangeListener</code> which removes and re-applies a data binding
 * when it receives a <code>PropertyChangeEvent</code>.
 */
public class DataBindingUpdateListener implements PropertyChangeListener {

    private final JAXXObject object;
    private final String dest;

    /**
     * Creates a new <code>DataBindingUpdateListener</code> which will remove and re-apply a
     * data binding when it receives a <code>PropertyChangeEvent</code>.
     *
     * @param object the object in which the data binding exists
     * @param dest   the name of the data binding to reapply
     */
    public DataBindingUpdateListener(JAXXObject object, String dest) {
        this.object = object;
        this.dest = dest;
    }

    public String getBindingName() {
        return dest;
    }

    /**
     * Updates the data binding in response to a <code>PropertyChangeEvent</code>.
     *
     * @param e the event which triggered the binding
     */
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        object.removeDataBinding(dest);
        object.applyDataBinding(dest);
    }
}
