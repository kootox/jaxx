package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.JXPathDecorator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * ComboBoxModel which can filter the elements displayed in the popup.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.12
 */
public class JaxxFilterableComboBoxModel<E> extends JaxxDefaultComboBoxModel<E> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(JaxxFilterableComboBoxModel.class);

    public static final Character DEFAULT_WILDCARD_CHARACTER = '*';

    protected final List<E> filteredItems = Lists.newArrayList();

    protected String filterText;

    protected Character wildcardCharacter = DEFAULT_WILDCARD_CHARACTER;

    /** the decorator of data */
    protected JXPathDecorator<E> decorator;

    protected final List<Predicate<E>> filters = Lists.newArrayList();

    public JaxxFilterableComboBoxModel() {
        super();
    }

    public JaxxFilterableComboBoxModel(E... items) {
        delegate = new ArrayList<>(items.length);

        int i, c;
        for (i = 0, c = items.length; i < c; i++)
            delegate.add(items[i]);

    }

    public JaxxFilterableComboBoxModel(Collection<E> v) {
        delegate = new ArrayList<>(v);
    }

    @Override
    public int getIndexOf(E anObject) {
        return filteredItems.indexOf(anObject);
    }

    @Override
    public void addAllElements(Collection<E> objects) {
        super.addAllElements(objects);
        refilter();
    }

    /** Empties the list. */
    @Override
    public void removeAllElements() {
        super.removeAllElements();
        refilter();
    }

    @Override
    public int getSize() {
        return filteredItems.size();
    }

    @Override
    public E getElementAt(int index) {
        E result;

        synchronized (this) {
            if (index >= 0 && index < filteredItems.size()) {
                result = filteredItems.get(index);
            } else {
                result = null;
            }
        }

        return result;
    }

    @Override
    public void addElement(Object anObject) {
        super.addElement(anObject);
        refilter();
    }

    @Override
    public void insertElementAt(Object anObject, int index) {
        super.insertElementAt(anObject, index);
        refilter();
    }

    @Override
    public void removeElementAt(int index) {
        super.removeElementAt(index);
        refilter();
    }

    @Override
    public void removeElement(Object anObject) {
        super.removeElement(anObject);
        refilter();
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
        refilter();
    }

    public Character getWildcardCharacter() {
        return wildcardCharacter;
    }

    public void setWildcardCharacter(Character wildcardCharacter) {
        this.wildcardCharacter = wildcardCharacter;
        refilter();

    }

    public JXPathDecorator<E> getDecorator() {
        return decorator;
    }

    public void setDecorator(JXPathDecorator<E> decorator) {
        this.decorator = decorator;
    }

    public void addFilter(Predicate<E> filter) {
        filters.add(filter);
        refilter();
    }

    public void removeFilter(Predicate<E> filter) {
        filters.remove(filter);
        refilter();
    }

    public void clearFilters() {
        filters.clear();
        refilter();
    }

    public void refreshFilteredElements() {
        refilter();
    }

    protected void refilter() {
        filteredItems.clear();

        if ((StringUtils.isEmpty(filterText)
                || wildcardCharacter != null && StringUtils.isEmpty(StringUtils.remove(filterText, wildcardCharacter)))
                && filters.isEmpty()) {
            filteredItems.addAll(delegate);

        } else {
            Pattern pattern = null;
            if (!StringUtils.isBlank(filterText)) {
                String patternText = Pattern.quote(filterText);

                if (wildcardCharacter == null) {
                    patternText = ".*" + patternText;

                } else {
                    patternText = patternText.replace(wildcardCharacter.toString(), "\\E.*\\Q");
                }
                pattern = Pattern.compile(patternText + ".*", Pattern.CASE_INSENSITIVE);
            }

            for (E element : delegate) {
                boolean addElement = true;
                for (Predicate<E> filter : filters) {
                    addElement &= filter.test(element);
                }
                String decoratedElement;
                if (decorator != null) {
                    decoratedElement = decorator.toString(element);
                } else {
                    decoratedElement = String.valueOf(element);
                }
                boolean matches = pattern == null
                        || pattern.matcher(decoratedElement).matches();
                if (matches && addElement) {
                    filteredItems.add(element);
                }
            }
        }

        if (log.isInfoEnabled()) {
            log.info("After refilter, nb items: " + getSize());
        }

        fireContentsChanged(this, 0, getSize());
    }
}
