package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.EventObject;

/**
 * Created on 8/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public class ComboBoxSelectionEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    /**
     * The current selected item in comboBox.
     */
    protected final Serializable currentSelectedItem;

    /**
     * The item to selected in comboBox.
     */
    protected final Serializable nextSelectedItem;

    /**
     * Constructs a ComboBoxSelectionEvent object.
     *
     * @param source              the Object that originated the event
     *                            (typically <code>this</code>)
     * @param currentSelectedItem the current selected item in comboBox
     * @param nextSelectedItem    the item to selected in comboBox.
     */
    public ComboBoxSelectionEvent(Object source, Serializable currentSelectedItem, Serializable nextSelectedItem) {
        super(source);
        this.currentSelectedItem = currentSelectedItem;
        this.nextSelectedItem = nextSelectedItem;
    }

    public Serializable getCurrentSelectedItem() {
        return currentSelectedItem;
    }

    public Serializable getNextSelectedItem() {
        return nextSelectedItem;
    }
}
