/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link TableCellRenderer} which compute text with the matching decorator
 * from {@link #decorators} based on the type of value and leave the hand to
 * the {@link #delegate} to perform the visual renderer.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class MultiDecoratorTableCelleRenderer implements TableCellRenderer {

    /** Delegate cell renderer */
    protected final TableCellRenderer delegate;

    /** accepted types */
    protected final List<Class<?>> types;

    /** decorators for accepted types */
    protected Decorator<?>[] decorators;

    public MultiDecoratorTableCelleRenderer(JXPathDecorator<?>... decorator) {
        this(new DefaultTableCellRenderer(), decorator);
    }

    public MultiDecoratorTableCelleRenderer(TableCellRenderer delegate,
                                            JXPathDecorator<?>... decorator) {
        this.delegate = delegate;

        types = new ArrayList<>();
        List<Decorator<?>> tmp = new ArrayList<>();
        for (JXPathDecorator<?> d : decorator) {
            if (types.contains(d.getType())) {
                throw new IllegalArgumentException(
                        "can not have twice a decorator of type " +
                                d.getType());
            }
            types.add(d.getType());
            tmp.add(d);
        }
        decorators = tmp.toArray(new Decorator<?>[tmp.size()]);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasfocus,
                                                   int row,
                                                   int column) {
        if (value != null) {
            int i = types.indexOf(value.getClass());
            if (i != -1) {
                Decorator<?> d = decorators[i];
                value = d.toString(value);
            }
        }
        return delegate.getTableCellRendererComponent(table,
                                                      value,
                                                      isSelected,
                                                      hasfocus,
                                                      row,
                                                      column
        );
    }
}
