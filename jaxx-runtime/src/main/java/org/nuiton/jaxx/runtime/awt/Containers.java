package org.nuiton.jaxx.runtime.awt;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.jxlayer.JXLayer;

import java.awt.Component;
import java.awt.Container;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by tchemit on 21/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Containers {

    public static <O extends Component> List<O> findComponents(Container container, Predicate<Component> predicate) {
        List<O> result = new LinkedList<>();
        findComponents(container, predicate, result);
        return result;
    }

    @SuppressWarnings("unchecked")
    private static <O extends Component> void findComponents(Component component, Predicate<Component> predicate, List<O> result) {
        if (predicate.test(component)) {
            result.add((O) component);
            return;
        }
        if (component instanceof JXLayer) {
            findComponents(((JXLayer) component).getView(), predicate, result);
            return;
        }
        if (component instanceof Container) {
            for (Component childComponent : ((Container) component).getComponents()) {
                findComponents(childComponent, predicate, result);
            }
        }
    }

}
