/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.util.EnumSet;

/**
 * A {@link TableCellRenderer} which displays enum values from their ordinal value.
 *
 * @param <E> le type de l'énumération.
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class EnumTableCellRenderer<E extends Enum<E>> implements TableCellRenderer {

    private final TableCellRenderer delegate;

    private final EnumSet<E> enumValues;

    public EnumTableCellRenderer(TableCellRenderer delegate, Class<E> enumClass) {
        this.delegate = delegate;
        this.enumValues = EnumSet.allOf(enumClass);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (value != null) {
            //FIXME : should be also able to read it by name ?
            Integer ordinal = Integer.valueOf(value + "");
            if (ordinal == -1) {
                value = null;
            } else {
                for (E enumValue : enumValues) {
                    if (ordinal == enumValue.ordinal()) {
                        value = enumValue;
                        break;
                    }
                }
            }
        }
        return delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
