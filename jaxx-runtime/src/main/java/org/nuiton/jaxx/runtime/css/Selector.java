/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.css;

public class Selector implements java.io.Serializable, Comparable<Selector> {

    public static final int NEVER_APPLIES = 0;

    public static final int PSEUDOCLASS_APPLIES_INHERIT_ONLY = 1;

    public static final int PSEUDOCLASS_APPLIES = 2;

    public static final int ALWAYS_APPLIES_INHERIT_ONLY = 3;

    public static final int ALWAYS_APPLIES = 4;

    private final String javaClassName;

    private final String styleClass;

    private final String pseudoClass;

    private final String id;

    private final boolean inline;

    private static final long serialVersionUID = 1L;

    public Selector(String javaClassName, String styleClass, String pseudoClass, String id) {
        this(javaClassName, styleClass, pseudoClass, id, false);
    }

    public Selector(String javaClassName, String styleClass, String pseudoClass, String id, boolean inline) {
        this.javaClassName = javaClassName;
        this.styleClass = styleClass;
        this.pseudoClass = pseudoClass;
        this.id = id;
        this.inline = inline;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public String getPseudoClass() {
        return pseudoClass;
    }

    public String getId() {
        return id;
    }

    public boolean isInline() {
        return inline;
    }

    @Override
    public int compareTo(Selector selector) {
        if (inline && !selector.inline) {
            return 1;
        }
        if (!inline && selector.inline) {
            return -1;
        }
        if (pseudoClass != null && selector.pseudoClass == null) {
            return 1;
        }
        if (pseudoClass == null && selector.pseudoClass != null) {
            return -1;
        }
        if (id != null && selector.id == null) {
            return 1;
        }
        if (id == null && selector.id != null) {
            return -1;
        }
        if (styleClass != null && selector.styleClass == null) {
            return 1;
        }
        if (styleClass == null && selector.styleClass != null) {
            return -1;
        }
        if (javaClassName != null && selector.javaClassName == null) {
            return 1;
        }
        if (javaClassName == null && selector.javaClassName != null) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Selector[" + javaClassName + ", " + styleClass + ", " + pseudoClass + ", " + id + "]";
    }
}
