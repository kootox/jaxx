/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.editor;

import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import java.awt.Component;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class MyDefaultCellEditor extends DefaultCellEditor {

    private static final long serialVersionUID = 1L;

    public static TableCellEditor newTextEditor() {
        return new MyDefaultCellEditor(new JTextField());
    }

    public static TableCellEditor newBooleanEditor() {
        return new MyDefaultCellEditor(new JCheckBox());
    }

    public static TableCellEditor newListEditor() {
        return newListEditor(new JComboBox());
    }

    public static TableCellEditor newListEditor(JComboBox editor) {
        return new MyDefaultCellEditor(editor);
    }

    public static TableCellEditor newEnumEditor(EnumEditor<?> editor) {
        return new MyDefaultCellEditor(editor) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object getCellEditorValue() {
                Object value = super.getCellEditorValue();
                if (value != null) {
                    value = ((Enum) value).ordinal();
                } else {
                    value = -1;
                }
                return value;
            }
        };
    }

    public static TableCellEditor newBooleanEditor(Icon icon) {
        return new MyDefaultCellEditor(new JCheckBox(icon));
    }

    public static TableCellEditor newBooleanEditor(Icon icon, boolean requireSelect) {
        TableCellEditor cellEditor = newBooleanEditor(icon);
        ((MyDefaultCellEditor) cellEditor).setRequireSelect(requireSelect);
        return cellEditor;
    }

    public static TableCellEditor newBooleanEditor(boolean requireSelect) {
        TableCellEditor cellEditor = newBooleanEditor();
        ((MyDefaultCellEditor) cellEditor).setRequireSelect(requireSelect);
        return cellEditor;
    }

    protected boolean requireSelect = true;

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (!isSelected && requireSelect) {
            // force to have select the cell before editing, a way to not modify edition for nothing...
            return null;
        }
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    public boolean isRequireSelect() {
        return requireSelect;
    }

    public void setRequireSelect(boolean requireSelect) {
        this.requireSelect = requireSelect;
    }

    protected MyDefaultCellEditor(JTextField textField) {
        super(textField);
        setClickCountToStart(1);
    }

    protected MyDefaultCellEditor(JCheckBox checkBox) {
        super(checkBox);
        setClickCountToStart(1);
    }

    protected MyDefaultCellEditor(JComboBox comboBox) {
        super(comboBox);
        setClickCountToStart(1);
    }
}
