package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created on 8/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public class WillChangeSelectedItemVetoException extends Exception {

    private static final long serialVersionUID = 1L;

    /** The event that the exception was created for. */
    protected final ComboBoxSelectionEvent event;

    /**
     * Constructs an ExpandVetoException object with no message.
     *
     * @param event a ComboBoxSelectionEvent object
     */

    public WillChangeSelectedItemVetoException(ComboBoxSelectionEvent event) {
        this(event, null);
    }

    /**
     * Constructs an ExpandVetoException object with the specified message.
     *
     * @param event   a ComboBoxSelectionEvent object
     * @param message a String containing the message
     */
    public WillChangeSelectedItemVetoException(ComboBoxSelectionEvent event, String message) {
        super(message);
        this.event = event;
    }
}
