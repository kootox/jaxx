/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.runtime.swing.tree;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Filter tree model.
 * <p>
 * Take a delegate {@link TreeModel} filter it with {@link TreeFilter}.
 * <p>
 * Based on : http://forums.sun.com/thread.jspa?forumID=57&amp;threadID=5378510
 *
 * @author chatellier
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FilterTreeModel implements TreeModel {

    /** Full model. */
    private final TreeModel delegateModel;

    /** Keep track of listeners of the Listener for data and structure change notification. */
    private final Set<TreeModelListener> treeModelListeners;

    /** Filter to use (can be null : no filtering). */
    private TreeFilter treeFilter;

    public FilterTreeModel(TreeModel delegateModel) {
        this(delegateModel, null);
    }

    public FilterTreeModel(TreeModel delegateModel, TreeFilter filter) {
        Objects.requireNonNull(delegateModel);
        this.delegateModel = delegateModel;
        this.treeFilter = filter;
        this.treeModelListeners = new LinkedHashSet<>();
        if (delegateModel instanceof DefaultTreeModel) {
            this.treeModelListeners.addAll(Arrays.asList(((DefaultTreeModel) delegateModel).getTreeModelListeners()));
        }

    }

    /**
     * Change filter.
     * <p>
     * Send a {@code treeStructureChanged} event on all registered listeners.
     *
     * @param treeFilter new filter
     */
    public void setFilter(TreeFilter treeFilter) {
        this.treeFilter = treeFilter;
        TreePath path = new TreePath(delegateModel.getRoot());
        fireTreeStructureChanged(path);
    }

    @Override
    public int getChildCount(Object parent) {
        int realCount = delegateModel.getChildCount(parent);
        int filterCount = 0;

        for (int i = 0; i < realCount; i++) {
            Object child = delegateModel.getChild(parent, i);
            if (include(child)) {
                filterCount++;
            }
        }
        return filterCount;
    }

    @Override
    public Object getChild(Object parent, int index) {
        int cnt = -1;
        for (int i = 0, childCount = delegateModel.getChildCount(parent); i < childCount; i++) {
            Object child = delegateModel.getChild(parent, i);

            if (include(child)) {
                cnt++;
            }
            if (cnt == index) {
                return child;
            }
        }
        return null;
    }

    @Override
    public Object getRoot() {
        return delegateModel.getRoot();
    }

    @Override
    public boolean isLeaf(Object node) {
        return delegateModel.isLeaf(node);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        delegateModel.valueForPathChanged(path, newValue);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return delegateModel.getIndexOfChild(parent, child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.add(l);
        delegateModel.addTreeModelListener(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
        delegateModel.removeTreeModelListener(l);
    }

    private void fireTreeStructureChanged(TreePath path) {
        TreeModelEvent e = null;
        for (TreeModelListener current : treeModelListeners) {
            // Lazily create the event:
            if (e == null)
                e = new TreeModelEvent(delegateModel, path);
            current.treeStructureChanged(e);
        }
    }

    private boolean include(Object node) {
        // null filter allowed, no filtering
        return treeFilter == null || treeFilter.include(delegateModel, node);
    }
}
