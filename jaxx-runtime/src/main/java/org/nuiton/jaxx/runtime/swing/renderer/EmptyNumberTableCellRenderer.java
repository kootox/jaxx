/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * A {@link TableCellRenderer} which does not display numbers when they are
 * equals to 0.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class EmptyNumberTableCellRenderer implements TableCellRenderer {

    protected final Integer ZERO = 0;

    protected final Float ZEROF = 0F;

    protected final Double ZEROD = 0D;

    private final TableCellRenderer delegate;

    public EmptyNumberTableCellRenderer() {
        this(new DefaultTableCellRenderer());
    }

    public EmptyNumberTableCellRenderer(TableCellRenderer delegate) {
        this.delegate = delegate;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value == null || ZERO.equals(value) || ZEROF.equals(value) || ZEROD.equals(value)) {
            value = null;
        }
        return delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
