/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.context;

import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXObject;

import java.util.Map.Entry;

/**
 * An initial context to be inject in a {@link JAXXObject}.
 *
 * The method {@link #add(Object)}  register a simple value.
 *
 * The method {@link #add(String, Object)} register a named value.
 *
 * The method {@link #to(JAXXContext)} inject in the {@link JAXXObject} the values registred in the initial context.
 *
 * The  initial context is also a "limited" {@link JAXXContext}, since we can only use the two methods
 *
 * {@link #getContextValue(Class)} or {@link #getContextValue(Class, String)}.
 *
 * @see JAXXContext
 */
public class JAXXInitialContext extends DefaultJAXXContext {

    public JAXXInitialContext() {
        super();
    }

    /**
     * Register a simple (none named) value in the context.
     *
     * @param value the value to be registred in the context
     * @return the instance of the context
     */
    public JAXXInitialContext add(Object value) {
        return add((String) null, value);
    }

    /**
     * Register a named value in the context.
     *
     * @param name  the name of the value
     * @param value the value to registred
     * @return the instance of the context
     */
    public JAXXInitialContext add(String name, Object value) {
        super.setContextValue(value, name);
        return this;
    }

    /**
     * Register a named (or not) value in the context.
     *
     * @param <O>   type of data to add
     * @param def   definition of entry
     * @param value the value to registred
     * @return the instance of the context
     */
    public <O> JAXXInitialContext add(JAXXContextEntryDef<O> def, O value) {
        super.setContextValue(value, def.getName());
        return this;
    }

    /**
     * Inject all the registed values into the {@link JAXXObject}
     *
     * @param dst the object to fill.
     */
    public void to(JAXXContext dst) {
        if (parentContext != null) {
            dst.setContextValue(parentContext);
        }
        for (Entry<JAXXContextEntryDef<?>, Object> entry : data.entrySet()) {
            dst.setContextValue(entry.getValue(), entry.getKey().getName());
        }
    }

    @Override
    public void setContextValue(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setContextValue(Object o, String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> void removeContextValue(Class<T> klazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> void removeContextValue(Class<T> klazz, String name) {
        throw new UnsupportedOperationException();
    }
}
