/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.editor;

import org.nuiton.i18n.I18n;
import org.nuiton.i18n.I18nStore;

import javax.swing.JComboBox;
import java.util.Locale;

/**
 * A {@link Locale} editor.
 *
 * use the static method to have an instance of editor
 * {@link #newEditor(Locale...)}
 *
 * If no locale is given to this method, it will go and seek via
 * {@link I18nStore#getLocales()} all loaded locales in i18n system
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6.0
 */
public class LocaleEditor extends JComboBox {

    /** serialVersionUID */
    private static final long serialVersionUID = -6777873426011538807L;

    protected Locale[] type;

    public static LocaleEditor newEditor(Locale... type) {
        return new LocaleEditor(type);
    }

    public LocaleEditor(Locale... type) {
        super(buildModel(type));
    }

    @Override
    public Locale getSelectedItem() {
        return (Locale) super.getSelectedItem();
    }

    protected static Locale[] buildModel(Locale... type) {
        if (type.length > 0) {
            return type;
        }
        // get availables locales registred in I18n system
        type = I18n.getStore().getLocales();
        return type;
    }

}
