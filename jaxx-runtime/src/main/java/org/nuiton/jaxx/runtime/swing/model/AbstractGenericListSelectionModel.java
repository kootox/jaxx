/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.model;

import com.google.common.collect.Lists;

import javax.swing.DefaultListSelectionModel;
import javax.swing.event.EventListenerList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.List;

/**
 * @author Sylvain Lletellier
 */
public abstract class AbstractGenericListSelectionModel<B> extends DefaultListSelectionModel {

    public static final String PROPERTY_SELECTED_VALUE = "selectedValues";
    private static final long serialVersionUID = -8002767572678647733L;

    protected final EventListenerList listenerList = new EventListenerList();
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    protected List<B> selectedValues;

    public AbstractGenericListSelectionModel() {
        this.selectedValues = Lists.newArrayList();
    }

    public B getSelectedValue() {
        return selectedValues.get(0);
    }

    public List<B> getSelectedValues() {
        return Lists.newArrayList(selectedValues);
    }

    public void setSelectedValues(List<B> selectedValues) {
        this.selectedValues = selectedValues;
    }

    protected void unSelectItems(Collection<B> values) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        for (B value : values) {
            int index = selectedValues.indexOf(value);
            removeSelectionIntervalWithoutFire(index, index);
        }
        fireSelectionRemoved(values);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    protected void unSelectItem(B value) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        int index = selectedValues.indexOf(value);
        removeSelectionIntervalWithoutFire(index, index);

        fireSelectionRemoved(Lists.newArrayList(value));
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    public void addSelectedItem(B toSelect) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        selectedValues.add(toSelect);
        int index = selectedValues.indexOf(toSelect);
        super.addSelectionInterval(index, index);

        fireSelectionAdded(Lists.newArrayList(toSelect));
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    public boolean hasSelectedIndex() {
        return !selectedValues.isEmpty();
    }

    @Override
    public void addSelectionInterval(int index0, int index1) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);

        addSelectionIntervalWithFire(index0, index1);
        super.addSelectionInterval(index0, index1);

        Collection<B> newValue = Lists.newArrayList(selectedValues);
        newValue.removeAll(oldValue);
        fireSelectionAdded(newValue);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    @Override
    public void setSelectionInterval(int index0, int index1) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        selectedValues.clear();
        addSelectionIntervalWithFire(index0, index1);
        super.setSelectionInterval(index0, index1);

        Collection<B> newValue = Lists.newArrayList(selectedValues);
        newValue.removeAll(oldValue);
        fireSelectionAdded(newValue);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    @Override
    public void removeSelectionInterval(int index0, int index1) {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        removeSelectionIntervalWithoutFire(index0, index1);

        Collection<B> newValue = Lists.newArrayList(selectedValues);
        newValue.removeAll(oldValue);
        fireSelectionRemoved(newValue);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    protected void removeSelectionIntervalWithoutFire(int index0, int index1) {
        if (index0 > index1) {
            int tmp = index1;
            index1 = index0;
            index0 = tmp;
        }
        for (int i = index0; i <= index1; i++) {
            if (selectedValues.size() > i && i != -1) {
                selectedValues.remove(i);
            }
        }
        super.removeSelectionInterval(index0, index1);
    }

    protected void addSelectionIntervalWithFire(int index0, int index1) {
        if (index0 > index1) {
            int tmp = index1;
            index1 = index0;
            index0 = tmp;
        }
        for (int i = index0; i <= index1; i++) {

            if (getSize() > i && i != -1) {
                B value = getValueAt(i);
                selectedValues.add(value);
            }
        }
    }

    public abstract int getSize();

    public abstract B getValueAt(int i);

    @Override
    public void clearSelection() {
        Collection<B> oldValue = Lists.newArrayList(selectedValues);
        selectedValues.clear();
        super.clearSelection();

        fireSelectionRemoved(oldValue);
        firePropertyChange(PROPERTY_SELECTED_VALUE, oldValue, selectedValues);
    }

    @Override
    public int getSelectionMode() {
        return MULTIPLE_INTERVAL_SELECTION;
    }

    protected void fireValuesAdded(Collection<B> values) {
        if (values.isEmpty()) {
            return;
        }
        Object[] listeners = listenerList.getListenerList();
        GenericListEvent<B> e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == GenericListListener.class) {
                if (e == null) {
                    e = new GenericListEvent<>(this, values);
                }
                ((GenericListListener) listeners[i + 1]).valuesAdded(e);
            }
        }
    }

    protected void fireValuesRemoved(Collection<B> values) {
        if (values.isEmpty()) {
            return;
        }
        Object[] listeners = listenerList.getListenerList();
        GenericListEvent<B> e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == GenericListListener.class) {
                if (e == null) {
                    e = new GenericListEvent<>(this, values);
                }
                ((GenericListListener) listeners[i + 1]).valuesRemoved(e);
            }
        }
    }

    protected void fireSelectionAdded(Collection<B> selectedValues) {
        if (selectedValues.isEmpty()) {
            return;
        }
        Object[] listeners = listenerList.getListenerList();
        GenericListEvent<B> e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == GenericListListener.class) {
                if (e == null) {
                    e = new GenericListEvent<>(this, selectedValues);
                }
                ((GenericListListener) listeners[i + 1]).selectionAdded(e);
            }
        }
    }

    protected void fireSelectionRemoved(Collection<B> selectedValues) {
        if (selectedValues.isEmpty()) {
            return;
        }
        Object[] listeners = listenerList.getListenerList();
        GenericListEvent<B> e = null;

        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == GenericListListener.class) {
                if (e == null) {
                    e = new GenericListEvent<>(this, selectedValues);
                }
                ((GenericListListener) listeners[i + 1]).selectionAdded(e);
            }
        }
    }

    public void addGenericListListener(GenericListListener l) {
        listenerList.add(GenericListListener.class, l);
    }

    public void removeGenericListListener(GenericListListener l) {
        listenerList.remove(GenericListListener.class, l);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }
}
