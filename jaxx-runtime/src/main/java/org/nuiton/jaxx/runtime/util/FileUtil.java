package org.nuiton.jaxx.runtime.util;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Created by tchemit on 12/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FileUtil {


    public interface FileAction {
        boolean doAction(File f);
    }

    /**
     * Permet de faire une action avant le parcours des fichiers, c-a-d que
     * l'on fera l'action sur les fichiers contenu dans un répertoire
     * après l'action sur le répertoire lui même.
     *
     * @param f          le fichier ou répertoire à partir duquel il faut commencer
     * @param fileAction l'action à effectuer sur chaque fichier
     * @return le résultat des fileAction executé sur les fichiers, chaque
     * résultat de FileAction sont assemblé par un ET logique pour donner
     * le résultat final
     */
    public static boolean walkAfter(File f, FileAction fileAction) {
        boolean result = fileAction.doAction(f);
        if (f.isDirectory()) {
            File list[] = f.listFiles();
            for (File aList : list) {
                result = result && walkAfter(aList, fileAction);
            }
        }
        return result;
    }

    /**
     * Obtain a file from the given {@code rootDirectory}, applying given paths.
     * <p>
     * For example with paths = a, b and c, then result is :
     * <pre>
     * root/a/b/c
     * </pre>
     *
     * @param rootDirectory the root directory
     * @param paths         paths to apply
     * @return the final file
     * @since 2.2
     */
    public static File getFileFromPaths(File rootDirectory, String... paths) {
        File result = rootDirectory;
        for (String path : paths) {
            result = new File(result, path);
        }
        return result;
    }


    /**
     * Obtain a file fro the given {@code rootDirectory}, applying the fqn.
     * <p>
     * For example with fqn = a.b.c, the result is :
     * <pre>
     * root/a/b/c
     * </pre>
     *
     * @param rootDirectory the root directory
     * @param fqn           fqn of searched file
     * @return the final file
     * @since 2.2
     */
    public static File getFileFromFQN(File rootDirectory, String fqn) {
        String[] paths = fqn.split("\\.");

        return getFileFromPaths(rootDirectory, paths);
    }
}
