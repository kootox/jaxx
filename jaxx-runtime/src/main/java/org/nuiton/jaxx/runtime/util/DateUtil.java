package org.nuiton.jaxx.runtime.util;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Calendar;
import java.util.Date;

/**
 * Created by tchemit on 12/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DateUtil {


    /**
     * Créer la date qui utilise uniquement l'heure
     * donnée dans {@code dayTime}.
     *
     * @param dayTime       l'heure a utiliser
     * @param useSecond     FIXME
     * @param useMiliSecond FIXME
     * @return la date donnée avec uniquement l'heure courante
     */
    public static Date getTime(Date dayTime, boolean useSecond, boolean useMiliSecond) {
        Calendar calendar = Calendar.getInstance();

        // recuperation de l'heure
        calendar.setTime(dayTime);
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int s = calendar.get(Calendar.SECOND);
        int ms = calendar.get(Calendar.MILLISECOND);


        // on part d'une date vide
        calendar.setTimeInMillis(0);

        // appliquer l'heure
        calendar.set(Calendar.HOUR_OF_DAY, h);
        calendar.set(Calendar.MINUTE, m);
        if (useSecond) {
            calendar.set(Calendar.SECOND, s);
        }
        if (useMiliSecond) {
            calendar.set(Calendar.MILLISECOND, ms);
        }

        return calendar.getTime();
    }


    /**
     * Enleve les données des heures (hour, minute, second, milli = 0).
     *
     * @param date la date a modifier
     * @return la date d'un jour
     */
    public static Date getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        date = calendar.getTime();
        return date;
    }


    /**
     * Créer la date qui utilise le jour donné dans {@code day} et l'heure
     * donnée dans {@code time}.
     *
     * @param day           le jour à utiliser
     * @param time          l'heure a utiliser
     * @param useSecond     FIXME
     * @param useMiliSecond FIXME
     * @return la date donnée avec l'heure courante
     */
    public static Date getDateAndTime(Date day, Date time, boolean useSecond, boolean useMiliSecond) {

        Calendar calendar = Calendar.getInstance();

        // recuperation de l'heure
        calendar.setTime(time);
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int s = useSecond ? calendar.get(Calendar.SECOND) : 0;
        int ms = useMiliSecond ? calendar.get(Calendar.MILLISECOND) : 0;

        calendar.setTime(day);

        // appliquer l'heure
        calendar.set(Calendar.HOUR_OF_DAY, h);
        calendar.set(Calendar.MINUTE, m);
        calendar.set(Calendar.SECOND, s);
        calendar.set(Calendar.MILLISECOND, ms);

        return calendar.getTime();

    }
}
