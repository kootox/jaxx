/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.Decorator;

import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link ListCellRenderer} which compute text with the given {@link #decorator},
 * highlights a part of the rendered text,
 * and leave the hand to the {@link #delegate} to perform the visual renderer.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 2.5.11
 */
public class FilteredDecoratorListCellRenderer<O> extends DecoratorListCellRenderer<O> {

    /** text to highlight */
    protected String filterText;

    protected Pattern pattern;

    protected boolean highlightFilterText = false;

    public FilteredDecoratorListCellRenderer(Decorator<O> decorator) {
        super(decorator);
    }

    public FilteredDecoratorListCellRenderer(ListCellRenderer<O> delegate,
                                             Decorator<O> decorator) {
        super(delegate, decorator);
    }

    public FilteredDecoratorListCellRenderer(ListCellRenderer<O> delegate,
                                             Decorator<O> decorator,
                                             boolean highlightFilterText) {
        super(delegate, decorator);
        this.highlightFilterText = highlightFilterText;
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
        computePattern();
    }

    public boolean isHighlightFilterText() {
        return highlightFilterText;
    }

    public void setHighlightFilterText(boolean highlightFilterText) {
        this.highlightFilterText = highlightFilterText;
        computePattern();
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        if (!(value instanceof String) && decorator != null) {
            value = decorator.toString(value);
        }

        String stringValue = String.valueOf(value);
        if (pattern != null) {
            Matcher matcher = pattern.matcher(stringValue);
            if (matcher.find()) {
                // for each group caught, add the text between the previous group
                // and the current group and surround the group with the highlighter
                StringBuilder sb = new StringBuilder();
                int i = 0;
                for (int g = 1; g <= matcher.groupCount(); g++) {
                    String match = matcher.group(g);
                    int indexOfMatch = stringValue.indexOf(match, i);
                    sb.append(stringValue.substring(i, indexOfMatch))
                      .append("<span style='background:#FFFF00'>")
                      .append(match)
                      .append("</span>");
                    i = indexOfMatch + match.length();
                }
                sb.append(stringValue.substring(i));
                stringValue = "<html>" + sb.toString() + "</html>";
            }
        }
        return delegate.getListCellRendererComponent(list,
                                                     (O) stringValue,
                                                     index,
                                                     isSelected,
                                                     cellHasFocus
        );
    }

    protected void computePattern() {
        if (highlightFilterText
                && !StringUtils.isEmpty(StringUtils.remove(filterText, '*'))) {
            // add the groups in the pattern
            String patternText = "(" + filterText.replace("*", ").*(") + ").*";
            patternText = StringUtils.remove(patternText, "()");
            pattern = Pattern.compile(patternText, Pattern.CASE_INSENSITIVE);

        } else {
            pattern = null;
        }
    }
}
