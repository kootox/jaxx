/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

// This needs to be split into two classes, Item and TreeItem

/**
 * An item in a component such as <code>JComboBox</code> or <code>JTree</code>.  The <code>Item</code>
 * class corresponds to the <code>&lt;item&gt;</code> tag in JAXX source files.
 */
public class Item {

    public static final String LABEL_PROPERTY = "label";

    public static final String VALUE_PROPERTY = "value";

    public static final String SELECTED_PROPERTY = "selected";

    private final String id;

    private String label;

    private Object value;

    private boolean selected;

    private List<Item> children;

    private Item parent;

    private PropertyChangeSupport propertyChangeSupport;

    /**
     * Creates a new Item.  This should only be called from compiled JAXX files.
     *
     * @param id       the item's ID
     * @param label    the string that should be used to represent the item visually
     * @param value    the item's actual value
     * @param selected <code>true</code> if the item should be selected by default
     */
    public Item(String id, String label, Object value, boolean selected) {
        this.id = id;
        this.label = label;
        this.value = value;
        this.selected = selected;
    }

    /**
     * Returns this item's ID.
     *
     * @return the JAXX ID attribute
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the string that should be used to represent the item at display time.  If <code>null</code>,
     * <code>String.valueOf(getValue())</code> will be used instead.
     *
     * @return this item's display string
     * @see #setLabel
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the item's display string.  If <code>null, String.valueOf(getValue())</code> will be used instead.
     *
     * @param label the new display string
     * @see #getLabel
     */
    public void setLabel(String label) {
        String oldLabel = this.label;
        this.label = label;
        firePropertyChange(LABEL_PROPERTY, oldLabel, label);
    }

    /**
     * Returns the item's actual value as it appears in the component's model.  The <code>Item</code> itself is not
     * visible from the model, only the value.
     *
     * @return the item's value
     * @see #setValue
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the item's value as it appears in the component's model.  The <code>Item</code> itself is not
     * visible from the model, only the value.
     *
     * @param value the new value
     * @see #getValue
     */
    public void setValue(Object value) {
        Object oldValue = this.value;
        this.value = value;
        firePropertyChange(VALUE_PROPERTY, oldValue, value);
    }

    /**
     * Returns <code>true</code> if this item is currently selected.  This is a bound property.
     *
     * @return <code>true</code> if item is selected
     * @see #setSelected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets the item's selection state.  This is a bound property.
     *
     * @param selected the new selection state
     * @see #isSelected
     */
    public void setSelected(boolean selected) {
        boolean oldSelected = this.selected;
        this.selected = selected;
        firePropertyChange(SELECTED_PROPERTY, oldSelected, selected);
    }

    /**
     * Adds a new child node (Items can be nested in trees).
     *
     * @param item the new child item
     */
    public void addChild(Item item) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(item);
        item.parent = this;
    }

    /**
     * Remove child node a new child node
     *
     * @param item to remove
     */
    public void removeChild(Item item) {
        if (children != null) {
            children.remove(item);
        }
    }

    /**
     * Remove all childs nodes
     *
     * @param items list of items to remove
     */
    public void removeChilds(List<Item> items) {
        if (children != null) {
            children.removeAll(items);
        }
    }

    /**
     * Returns a list of this item's children.
     *
     * @return a list of all nested child nodes
     */
    public List<Item> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }

    /**
     * Returns the <code>Item</code> containing this <code>Item</code>, or <code>null</code> for a top-level
     * <code>Item</code>.
     *
     * @return the item parent (or null)
     */
    public Item getParent() {
        return parent;
    }

    /**
     * Set the parent of this item
     *
     * @param parent the item parent (or null)
     */
    public void setParent(Item parent) {
        this.parent = parent;
    }

    private PropertyChangeSupport getPropertyChangeSupport() {
        if (propertyChangeSupport == null) {
            propertyChangeSupport = new SwingPropertyChangeSupport(this);
        }
        return propertyChangeSupport;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(property, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(property, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (propertyChangeSupport != null) {
            getPropertyChangeSupport().firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    @Override
    public String toString() {
        return getClass().getName() + "[" + value + "]";
    }
}
