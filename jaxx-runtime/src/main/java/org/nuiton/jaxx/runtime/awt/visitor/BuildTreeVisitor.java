package org.nuiton.jaxx.runtime.awt.visitor;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.jxlayer.JXLayer;

import javax.swing.JRootPane;
import java.awt.Component;
import java.awt.Container;

/**
 * A visitor to build the tree from a given component.
 *
 * To obtain a tree from a compoent use the method {@link #buildTree(Component)}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.14
 */
public class BuildTreeVisitor implements ComponentTreeNodeVisitor {

    public static ComponentTreeNode buildTree(Component component) {
        BuildTreeVisitor v = new BuildTreeVisitor();
        ComponentTreeNode rootNode = new ComponentTreeNode(component);
        rootNode.visit(v);
        return rootNode;
    }

    @Override
    public void startNode(ComponentTreeNode node) {
        Component component = node.getUserObject();
        if (component instanceof Container) {
            Container container = (Container) component;
            //TODO-TC-2013-03-17 Should use some rules to add this.
            for (Component child : container.getComponents()) {
                if (child instanceof JXLayer<?>) {
                    child = ((JXLayer<?>) child).getView();
                } else if (child instanceof JRootPane) {
                    child = ((JRootPane) child).getLayeredPane();
                }
                ComponentTreeNode childNode = new ComponentTreeNode(child);
                node.add(childNode);
            }
        }
    }

    @Override
    public void endNode(ComponentTreeNode componentTree) {
    }
}
