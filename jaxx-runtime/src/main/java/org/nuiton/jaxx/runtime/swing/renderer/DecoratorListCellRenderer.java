/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import org.nuiton.decorator.Decorator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;

/**
 * A {@link ListCellRenderer} which compute text with the given {@link #decorator}
 * and leave the hand to the {@link #delegate} to perform the visual renderer.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.7.2
 */
public class DecoratorListCellRenderer<O> implements ListCellRenderer<O> {

    /** Delegate cell renderer */
    protected final ListCellRenderer<O> delegate;

    /** Decorator to produce text to render */
    protected final Decorator<O> decorator;

    public DecoratorListCellRenderer(Decorator<O> decorator) {
        this((ListCellRenderer<O>) new DefaultListCellRenderer(), decorator);
    }

    public DecoratorListCellRenderer(ListCellRenderer<O> delegate,
                                     Decorator<O> decorator) {
        this.delegate = delegate;
        this.decorator = decorator;
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
        value = decorateValue(value, index);
        return delegate.getListCellRendererComponent(list,
                                                     (O) value,
                                                     index,
                                                     isSelected,
                                                     cellHasFocus
        );
    }

    protected Object decorateValue(Object value, int index) {
        if (value == null) {
            value = " ";
        }
        if (!(value instanceof String) && decorator != null) {
            value = decorator.toString(value);
        }
        return value;
    }
}
