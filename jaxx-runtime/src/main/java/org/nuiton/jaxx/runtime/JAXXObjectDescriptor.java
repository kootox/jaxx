/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import java.io.Serializable;
import org.nuiton.jaxx.runtime.css.Stylesheet;

public class JAXXObjectDescriptor implements Serializable {

    private static final long serialVersionUID = 1L;

    private final ComponentDescriptor[] descriptors;
    private final Stylesheet stylesheet;

    public JAXXObjectDescriptor(ComponentDescriptor[] descriptors, Stylesheet stylesheet) {
        this.descriptors = descriptors;
        this.stylesheet = stylesheet;
    }

    public ComponentDescriptor[] getComponentDescriptors() {
        return descriptors;
    }

    public Stylesheet getStylesheet() {
        return stylesheet;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        for (ComponentDescriptor descriptor : descriptors) {
            buffer.append("\n").append(descriptor);
        }
        buffer.append("\n").append(stylesheet);
        return buffer.toString();
    }
}
