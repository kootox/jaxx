package org.nuiton.jaxx.runtime.swing.tree;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Enumerations;
import org.jdesktop.swingx.painter.Painter;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIDefaults;
import javax.swing.tree.TreeNode;
import java.awt.Color;
import java.util.Collections;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Stream;

/**
 * Created by tchemit on 11/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JTrees {

    public static void initUI(JScrollPane selectedTreePane, JTree tree) {

        // customize tree selection colors
        UIDefaults defaults = new UIDefaults();

        Painter<JComponent> painter = (g, c, w, h) -> g.fillRect(0, 0, w, h);
        defaults.put("Tree:TreeCell[Enabled+Selected].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Enabled+Focused].backgroundPainter", painter);
        defaults.put("Tree:TreeCell[Focused+Selected].backgroundPainter", painter);

        tree.putClientProperty("Nimbus.Overrides", defaults);
        // Fix http://forge.codelutin.com/issues/2781
        selectedTreePane.getViewport().setBackground(Color.WHITE);
    }

    /**
     * Creates a stream of tree node using the Depth-first search (DSF) algorithm with preorder vertex ordering
     * starting on given {@code node}.
     * <p>
     * <b>Note:</b> the stream is not parallel.
     *
     * @param node node ot walk through
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> preorderStream(TreeNode node) {
        return preorderStream(node, false);
    }

    /**
     * Creates a stream of tree node using the Depth-first search (DSF) algorithm with preorder vertex ordering
     * starting on given {@code node}.
     *
     * @param node     node ot walk through
     * @param parallel to set parallel on the stream
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> preorderStream(TreeNode node, boolean parallel) {
        return Enumerations.stream(new PreorderEnumeration(node), parallel);
    }

    /**
     * Creates a stream of tree node using the Depth-first search (DSF) algorithm with postorder vertex ordering
     * starting on given {@code node}.
     * <p>
     * <b>Note:</b> the stream is not parallel.
     *
     * @param node node ot walk through
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> postorderStream(TreeNode node) {
        return postorderStream(node, false);
    }

    /**
     * Creates a stream of tree node using the Depth-first search (DSF) algorithm with postorder vertex ordering
     * starting on given {@code node}.
     *
     * @param node     node ot walk through
     * @param parallel to set parallel on the stream
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> postorderStream(TreeNode node, boolean parallel) {
        return Enumerations.stream(new PostorderEnumeration(node), parallel);
    }

    /**
     * Creates a stream of tree node using the Breadth-first search (BFS) algorithm with preorder vertex ordering
     * starting on given {@code node}.
     * <p>
     * <b>Note:</b> the stream is not parallel.
     *
     * @param node node ot walk through
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> breadthFirstStream(TreeNode node) {
        return breadthFirstStream(node, false);
    }

    /**
     * Creates a stream of tree node using the Breadth-first search (BFS) algorithm with preorder vertex ordering
     * starting on given {@code node}.
     *
     * @param node     node ot walk through
     * @param parallel to set parallel on the stream
     * @return stream of nodes in correct order.
     */
    public static Stream<TreeNode> breadthFirstStream(TreeNode node, boolean parallel) {
        return Enumerations.stream(new BreadthFirstEnumeration(node), parallel);
    }

    private static final class PreorderEnumeration implements Enumeration<TreeNode> {
        private final Stack<Enumeration> stack = new Stack<>();

        public PreorderEnumeration(TreeNode rootNode) {
            stack.push(Enumerations.singleton(rootNode));
        }

        public boolean hasMoreElements() {
            return (!stack.empty() && stack.peek().hasMoreElements());
        }

        public TreeNode nextElement() {
            Enumeration currentEnumeration = stack.peek();
            TreeNode node = (TreeNode) currentEnumeration.nextElement();
            if (!currentEnumeration.hasMoreElements()) {
                stack.pop();
            }
            Enumeration children = node.children();
            if (children.hasMoreElements()) {
                stack.push(children);
            }
            return node;
        }
    }

    private static final class PostorderEnumeration implements Enumeration<TreeNode> {
        private TreeNode root;
        private Enumeration<TreeNode> children;
        private Enumeration<TreeNode> subtree;

        @SuppressWarnings("unchecked")
        public PostorderEnumeration(TreeNode rootNode) {
            Objects.requireNonNull(rootNode);
            root = rootNode;
            children = root.children();
            subtree = Collections.emptyEnumeration();
        }

        @Override
        public boolean hasMoreElements() {
            return root != null;
        }

        @Override
        public TreeNode nextElement() {
            TreeNode returnValue;
            if (subtree.hasMoreElements()) {
                returnValue = subtree.nextElement();
            } else if (children.hasMoreElements()) {
                subtree = new PostorderEnumeration(children.nextElement());
                returnValue = subtree.nextElement();
            } else {
                returnValue = root;
                root = null;
            }
            return returnValue;
        }
    }

    private static final class BreadthFirstEnumeration implements Enumeration<TreeNode> {
        private Queue queue;

        public BreadthFirstEnumeration(TreeNode rootNode) {
            Objects.requireNonNull(rootNode);
            queue = new Queue();
            queue.enqueue(Enumerations.singleton(rootNode));
        }

        @Override
        public boolean hasMoreElements() {
            return (!queue.isEmpty() && queue.peek().hasMoreElements());
        }

        @Override
        public TreeNode nextElement() {
            Enumeration currentEnumeration = queue.peek();
            TreeNode node = (TreeNode) currentEnumeration.nextElement();
            if (!currentEnumeration.hasMoreElements()) {
                queue.pop();
            }
            Enumeration children = node.children();
            if (children.hasMoreElements()) {
                queue.enqueue(children);
            }
            return node;
        }

        // A simple queue with a linked list data structure.
        public final class Queue {
            Queue.QNode head; // null if empty
            Queue.QNode tail;

            public void enqueue(Enumeration anObject) {
                if (head == null) {
                    head = tail = new Queue.QNode(anObject, null);
                } else {
                    tail.next = new Queue.QNode(anObject, null);
                    tail = tail.next;
                }
            }

            public void pop() {
                if (head == null) {
                    throw new NoSuchElementException("No more elements");
                }
                Queue.QNode oldHead = head;
                head = head.next;
                if (head == null) {
                    tail = null;
                } else {
                    oldHead.next = null;
                }
            }

            public Enumeration peek() {
                if (head == null) {
                    throw new NoSuchElementException("No more elements");
                }
                return head.object;
            }

            public boolean isEmpty() {
                return head == null;
            }

            final class QNode {
                public Enumeration object;
                public Queue.QNode next;   // null if end

                public QNode(Enumeration object, Queue.QNode next) {
                    this.object = object;
                    this.next = next;
                }
            }

        }
    }

}
