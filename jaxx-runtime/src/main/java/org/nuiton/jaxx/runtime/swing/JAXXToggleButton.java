/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.JToggleButton;

public class JAXXToggleButton extends JToggleButton {

    private static final long serialVersionUID = 1L;

    protected String glueText;

    protected String normalText;

    protected String glueTooltipText;

    protected String normalTooltipText;

    protected int normalMnemonic;

    protected int glueMnemonic;

    protected boolean _init;

    public String getGlueText() {
        return glueText;
    }

    public String getNormalText() {
        return normalText;
    }

    public String getGlueTooltipText() {
        return glueTooltipText;
    }

    public String getNormalTooltipText() {
        return normalTooltipText;
    }

    public void setGlueText(String glueText) {
        this.glueText = glueText;

    }

    public void setNormalText(String normalText) {
        this.normalText = normalText;

    }

    public void setGlueTooltipText(String glueTooltipText) {
        this.glueTooltipText = glueTooltipText;
    }

    public int getNormalMnemonic() {
        return normalMnemonic;
    }

    public void setNormalMnemonic(int normalMnemonic) {
        this.normalMnemonic = normalMnemonic;
    }

    public int getGlueMnemonic() {
        return glueMnemonic;
    }

    public void setGlueMnemonic(int glueMnemonic) {
        this.glueMnemonic = glueMnemonic;
    }

    public void setNormalTooltipText(String normalTooltipText) {
        this.normalTooltipText = normalTooltipText;
        if (!_init) {
            init();
            _init = true;
        }
    }

    @Override
    public void setSelected(boolean b) {
        super.setSelected(b);
        if (isSelected()) {
            setText(getGlueText());
            setToolTipText(getGlueTooltipText());
            setMnemonic(getGlueMnemonic());
        } else {
            setText(getNormalText());
            setToolTipText(getNormalTooltipText());
            setMnemonic(getNormalMnemonic());
        }
        revalidate();
    }

    public void init() {
        setSelected(false);
    }

    /* end raw body code */
    public JAXXToggleButton() {
        super();
        _init = false;
    }
}
