package org.nuiton.jaxx.runtime.swing.renderer;
/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * Table cell renderer for a {@link Class}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.4
 */
public class ClassTableCellRenderer implements TableCellRenderer {

    protected final TableCellRenderer defaultDelegate;

    public ClassTableCellRenderer() {
        this.defaultDelegate = new DefaultTableCellRenderer();
    }

    public ClassTableCellRenderer(TableCellRenderer defaultDelegate) {
        this.defaultDelegate = defaultDelegate;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        return defaultDelegate.getTableCellRendererComponent(
                table,
                value == null ? null : ((Class<?>) value).getName(),
                isSelected,
                hasFocus,
                row,
                column);
    }
}
