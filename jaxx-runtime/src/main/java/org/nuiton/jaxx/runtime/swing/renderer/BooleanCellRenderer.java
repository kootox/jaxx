/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.renderer;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.function.Predicate;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class BooleanCellRenderer extends JPanel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected final TableCellRenderer defaultDelegate;

    protected final JCheckBox checkBox;

    private final Predicate predicate;

    private static final Predicate<?> DEFAULT_PREDICATE = input -> (input != null && (Boolean) input);

    public BooleanCellRenderer(TableCellRenderer delegate, Predicate<?> predicate) {
        this(delegate, new JCheckBox(), predicate);
    }

    public BooleanCellRenderer(TableCellRenderer delegate) {
        //super(new BorderLayout());
        this(delegate, DEFAULT_PREDICATE);

    }

    public BooleanCellRenderer(TableCellRenderer delegate, Icon icon, Predicate<?> predicate) {
        this(delegate, new JCheckBox(icon), predicate);
    }

    public BooleanCellRenderer(TableCellRenderer delegate, Icon icon) {
        //super(new BorderLayout());
        this(delegate, icon, DEFAULT_PREDICATE);
        checkBox.setVerticalTextPosition(JLabel.TOP);

    }

    public BooleanCellRenderer(TableCellRenderer delegate, JCheckBox checkBox, Predicate<?> predicate) {
        this.defaultDelegate = delegate;
        this.predicate = predicate;
        this.checkBox = checkBox;
        this.checkBox.setBorderPainted(true);
        this.checkBox.setHorizontalAlignment(JLabel.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        ((JComponent) defaultDelegate).setBackground(null);
        JComponent render = (JComponent) defaultDelegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(render.getForeground());
            setBackground(render.getBackground());
            //fixme make this works... and remove the test
            if (row % 2 == 1) {
                setBackground(Color.WHITE);
            }
        }

        boolean selectCheckBox = predicate.test(value);
        checkBox.setSelected(selectCheckBox);

        setBorder(render.getBorder());
        return checkBox;
    }
}
