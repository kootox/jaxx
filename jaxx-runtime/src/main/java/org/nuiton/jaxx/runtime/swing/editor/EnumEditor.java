/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* 
 * *##% 
 * JAXX Runtime
 * Copyright (C) 2008 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%* */
package org.nuiton.jaxx.runtime.swing.editor;

import java.lang.reflect.Array;
import java.util.EnumSet;
import javax.swing.JComboBox;

/**
 * Une éditeur d'enum.
 *
 * @param <E> le type d'enumeration a editer.
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.6.0
 */
public class EnumEditor<E extends Enum<E>> extends JComboBox<E> {

    private static final long serialVersionUID = 2L;

    /** Type of enumeration */
    protected Class<E> type;

    /**
     * Creates a {@link EnumEditor} for the given enumeration {@code type}, with
     * all values of enumeration.
     *
     * @param type type of enumeration
     * @param <E>  generci type of enumeration
     * @return the instanciated editor
     */
    public static <E extends Enum<E>> EnumEditor<E> newEditor(Class<E> type) {
        return new EnumEditor<>(type);
    }


    /**
     * Creates a {@link EnumEditor} for the given enumeration {@code type}, with
     * all values of enumeration which {@code ordinal} is strictly lower than
     * the given {@code maxOrdinal}.
     *
     * @param type       type of enumeration
     * @param maxOrdinal the upper (strict) bound of ordinal values allowed
     * @param <E>        generic type of enumeration
     * @return the instanciated editor
     */
    public static <E extends Enum<E>> EnumEditor<E> newEditor(Class<E> type,
                                                              int maxOrdinal) {
        return new EnumEditor<>(type, maxOrdinal);
    }

    /**
     * Creates a {@link EnumEditor} for the given enumeration {@code type}, with
     * all given {@code universe} values of enumeration.
     *
     * @param universe enumerations to put in editor
     * @param <E>      generci type of enumeration
     * @return the instanciated editor
     */
    public static <E extends Enum<E>> EnumEditor<E> newEditor(E... universe) {
        return new EnumEditor<>(universe);
    }

    public EnumEditor(Class<E> type) {
        super(buildModel(type));
        this.type = type;
    }

    public EnumEditor(Class<E> type, int maxOrdinal) {
        super(buildModel(type, maxOrdinal));
        this.type = type;
    }

    public EnumEditor(E... universe) {
        super(universe);
    }

    @Override
    public E getSelectedItem() {
        return (E) super.getSelectedItem();
    }

    public Class<E> getType() {
        return type;
    }

    protected static <E extends Enum<E>> E[] buildModel(Class<E> type) {
        EnumSet<E> all = EnumSet.allOf(type);
        E[] result = (E[]) Array.newInstance(type, all.size());
        System.arraycopy(all.toArray(), 0, result, 0, all.size());
        return result;
    }


    protected static <E extends Enum<E>> E[] buildModel(Class<E> type,
                                                        int maxOrdinal) {
        EnumSet<E> all = EnumSet.allOf(type);
        all.removeIf(e -> e.ordinal() > maxOrdinal);
        E[] result = (E[]) Array.newInstance(type, all.size());
        System.arraycopy(all.toArray(), 0, result, 0, all.size());
        return result;
    }
}
