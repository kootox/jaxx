package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.EventListener;

/**
 * A listener to be able to stop the change of a selected item in a
 * Created on 8/7/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.10
 */
public interface WillChangeSelectedItemListener extends EventListener {

    /**
     * Invoked whenever the selected item is about to change in the comboBox.
     */
    void selectedItemWillChanged(ComboBoxSelectionEvent event);

}
