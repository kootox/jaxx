/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.editor;

import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.event.KeyEvent;

/**
 * Custom text field to disable default key events
 *
 * @author Sylvain Lletellier
 */
public class KeyStrokeEditor extends JTextField {

    private static final long serialVersionUID = 3559019483882085443L;
    protected KeyStroke keyStroke;

    public KeyStrokeEditor() {
        enableEvents(KeyEvent.KEY_EVENT_MASK);
        setFocusTraversalKeysEnabled(false);
    }

    @Override
    protected void processKeyEvent(KeyEvent e) {
        if (e.getID() == KeyEvent.KEY_PRESSED) {
            int keyCode = e.getKeyCode();
            if (keyCode == KeyEvent.VK_SHIFT ||
                    keyCode == KeyEvent.VK_ALT ||
                    keyCode == KeyEvent.VK_CONTROL ||
                    keyCode == KeyEvent.VK_ALT_GRAPH ||
                    keyCode == KeyEvent.VK_META) {

                return;
            }

            setKeyStroke(KeyStroke.getKeyStroke(keyCode, e.getModifiers()));
        }
    }

    public KeyStroke getKeyStroke() {
        return keyStroke;
    }

    public void setKeyStroke(KeyStroke keyStroke) {
        KeyStroke oldValue = getKeyStroke();
        this.keyStroke = keyStroke;
        firePropertyChange("keyStroke", oldValue, keyStroke);
        fireActionPerformed();
        SwingUtilities.invokeLater(() -> setText(getKeyStroke().toString()));
    }
}
