/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing;

import javax.swing.Icon;
import javax.swing.JTabbedPane;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TabInfoPropertyChangeListener implements PropertyChangeListener {

    private final JTabbedPane tabs;

    private final int tabIndex;

    public TabInfoPropertyChangeListener(JTabbedPane tabs, int tabIndex) {
        this.tabs = tabs;
        this.tabIndex = tabIndex;
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        String name = e.getPropertyName();
        if (name.equals(TabInfo.TITLE_PROPERTY)) {
            tabs.setTitleAt(tabIndex, (String) e.getNewValue());
        } else if (name.equals(TabInfo.TOOL_TIP_TEXT_PROPERTY)) {
            tabs.setToolTipTextAt(tabIndex, (String) e.getNewValue());
        } else if (name.equals(TabInfo.FOREGROUND_PROPERTY)) {
            tabs.setForegroundAt(tabIndex, (Color) e.getNewValue());
        } else if (name.equals(TabInfo.BACKGROUND_PROPERTY)) {
            tabs.setBackgroundAt(tabIndex, (Color) e.getNewValue());
        } else if (name.equals(TabInfo.MNEMONIC_PROPERTY)) {
            tabs.setMnemonicAt(tabIndex, (Integer) e.getNewValue());
        } else if (name.equals(TabInfo.DISPLAYED_MNEMONIC_INDEX_PROPERTY)) {
            tabs.setDisplayedMnemonicIndexAt(tabIndex, (Integer) e.getNewValue());
        } else if (name.equals(TabInfo.ICON_PROPERTY)) {
            tabs.setIconAt(tabIndex, (Icon) e.getNewValue());
        } else if (name.equals(TabInfo.DISABLED_ICON_PROPERTY)) {
            tabs.setDisabledIconAt(tabIndex, (Icon) e.getNewValue());
        } else if (name.equals(TabInfo.TAB_COMPONENT_PROPERTY)) {
            tabs.setTabComponentAt(tabIndex, (Component) e.getNewValue());
        } else if (name.equals(TabInfo.ENABLED_PROPERTY)) {
            tabs.setEnabledAt(tabIndex, (Boolean) e.getNewValue());
        }
    }
}    
