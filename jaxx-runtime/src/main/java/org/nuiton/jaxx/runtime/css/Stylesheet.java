/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.css;

public class Stylesheet implements java.io.Serializable {

    private Rule[] rules;

    private static final long serialVersionUID = 1L;

    public Stylesheet() {
        rules = new Rule[0];
    }

    public Stylesheet(Rule[] rules) {
        this.rules = rules;
        java.util.Arrays.sort(rules);
    }

    public Rule[] getRules() {
        return rules;
    }

    public void add(Rule newRule) {
        Rule[] oldRules = rules;
        rules = new Rule[oldRules.length + 1];
        System.arraycopy(oldRules, 0, rules, 0, oldRules.length);
        rules[rules.length - 1] = newRule;
        java.util.Arrays.sort(rules);
    }

    public void add(Rule[] newRules) {
        Rule[] oldRules = rules;
        rules = new Rule[oldRules.length + newRules.length];
        System.arraycopy(oldRules, 0, rules, 0, oldRules.length);
        System.arraycopy(newRules, 0, rules, oldRules.length, newRules.length);
        java.util.Arrays.sort(rules);
    }

    @Override
    public String toString() {
        return "Stylesheet" + java.util.Arrays.asList(rules);
    }
}
