package org.nuiton.jaxx.runtime.awt;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 11/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Clipboards {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Clipboards.class);

    /**
     * Copy to clipBoard the content of the given text.
     *
     * @param text text to copy to clipboard
     */
    public static void copyToClipBoard(String text) {

        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (log.isDebugEnabled()) {
            log.debug("Put in clipboard :\n" + text);
        }
        StringSelection selection = new StringSelection(text);
        clipboard.setContents(selection, selection);
    }
}
