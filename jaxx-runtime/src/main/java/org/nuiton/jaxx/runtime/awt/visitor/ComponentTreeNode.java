package org.nuiton.jaxx.runtime.awt.visitor;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.iterators.EnumerationIterator;

import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.Component;
import java.awt.Container;
import java.util.Iterator;

/**
 * A node where userObject is a {@link Component}.
 *
 * If the component is a {@link Container}, then his children are the components
 * of the container.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.14
 */
public class ComponentTreeNode extends DefaultMutableTreeNode implements Iterable<ComponentTreeNode> {

    private static final long serialVersionUID = 1L;

    public ComponentTreeNode(Component userObject) {
        super(userObject, true);
    }

    /**
     * To visit a node.
     *
     * @param visitor the visitor
     */
    public void visit(ComponentTreeNodeVisitor visitor) {
        visitor.startNode(this);
        for (ComponentTreeNode child : this) {
            child.visit(visitor);
        }
        visitor.endNode(this);
    }

    @Override
    public Component getUserObject() {
        return (Component) super.getUserObject();
    }

    @Override
    public ComponentTreeNode getParent() {
        return (ComponentTreeNode) super.getParent();
    }

    @Override
    public ComponentTreeNode getNextLeaf() {
        return (ComponentTreeNode) super.getNextLeaf();
    }

    @Override
    public ComponentTreeNode getNextNode() {
        return (ComponentTreeNode) super.getNextNode();
    }

    @Override
    public ComponentTreeNode getNextSibling() {
        return (ComponentTreeNode) super.getNextSibling();
    }

    @Override
    public ComponentTreeNode getPreviousLeaf() {
        return (ComponentTreeNode) super.getPreviousLeaf();
    }

    @Override
    public ComponentTreeNode getPreviousNode() {
        return (ComponentTreeNode) super.getPreviousNode();
    }

    @Override
    public ComponentTreeNode getPreviousSibling() {
        return (ComponentTreeNode) super.getPreviousSibling();
    }

    @Override
    public ComponentTreeNode getRoot() {
        return (ComponentTreeNode) super.getRoot();
    }

    @Override
    public Iterator<ComponentTreeNode> iterator() {
        return new EnumerationIterator(children());
    }
}
