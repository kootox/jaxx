package org.nuiton.jaxx.runtime.swing.model;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;

import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.MutableComboBoxModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 * To replace the {@link DefaultComboBoxModel} which used {@link Vector}.
 *
 * Add some new methods to {@link #addAllElements(Collection)} or
 * {@link #setAllElements(Collection)}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.10
 */
public class JaxxDefaultComboBoxModel<E> extends AbstractListModel implements MutableComboBoxModel, Serializable, ComboBoxModel2 {

    private static final long serialVersionUID = 1L;

    protected List<E> delegate;

    protected E selectedObject;

    public JaxxDefaultComboBoxModel() {
        delegate = new ArrayList<>();
    }

    public JaxxDefaultComboBoxModel(E... items) {
        delegate = new ArrayList<>(items.length);

        int i, c;
        for (i = 0, c = items.length; i < c; i++)
            delegate.add(items[i]);

        if (getSize() > 0) {
            selectedObject = getElementAt(0);
        }
    }

    public JaxxDefaultComboBoxModel(Collection<E> v) {
        delegate = new ArrayList<>(v);

        if (getSize() > 0) {
            selectedObject = getElementAt(0);
        }
    }

    @Override
    public void setSelectedItem(Object anObject) {
        if ((selectedObject != null && !selectedObject.equals(anObject)) ||
                selectedObject == null && anObject != null) {

            fireSelectedItemWillChanged(selectedObject, anObject);
            selectedObject = (E) anObject;
            fireContentsChanged(this, -1, -1);
        }
    }

    public int getIndexOf(E anObject) {
        return delegate.indexOf(anObject);
    }


    public void setAllElements(Collection<E> objects) {
        removeAllElements();
        addAllElements(objects);
    }

    public void addAllElements(Collection<E> objects) {
        if (CollectionUtils.isNotEmpty(objects)) {
            int firstIndex = delegate.size();
            this.delegate.addAll(objects);
            int lastIndex = delegate.size() - 1;
            if (lastIndex > -1) {
                fireIntervalAdded(this, firstIndex, lastIndex);
            }
        }
    }

    /** Empties the list. */
    public void removeAllElements() {
        if (delegate.size() > 0) {
            int firstIndex = 0;
            int lastIndex = delegate.size() - 1;
            delegate.clear();
            selectedObject = null;
            fireIntervalRemoved(this, firstIndex, lastIndex);
        } else {
            selectedObject = null;
        }
    }

    @Override
    public E getSelectedItem() {
        return selectedObject;
    }

    @Override
    public int getSize() {
        return delegate.size();
    }

    @Override
    public E getElementAt(int index) {
        E result;

        if (index >= 0 && index < delegate.size()) {
            result = delegate.get(index);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public void addElement(Object anObject) {
        delegate.add((E) anObject);
        fireIntervalAdded(this, delegate.size() - 1, delegate.size() - 1);
        if (delegate.size() == 1 && selectedObject == null && anObject != null) {
            setSelectedItem(anObject);
        }
    }

    @Override
    public void insertElementAt(Object anObject, int index) {
        delegate.add(index, (E) anObject);
        fireIntervalAdded(this, index, index);
    }

    @Override
    public void removeElementAt(int index) {
        if (getElementAt(index) == selectedObject) {
            if (index == 0) {
                setSelectedItem(getSize() == 1 ? null : getElementAt(index + 1));
            } else {
                setSelectedItem(getElementAt(index - 1));
            }
        }

        delegate.remove(index);

        fireIntervalRemoved(this, index, index);
    }

    @Override
    public void removeElement(Object anObject) {
        int index = delegate.indexOf(anObject);
        if (index != -1) {
            removeElementAt(index);
        }
    }

    @Override
    public void addWillChangeSelectedItemListener(WillChangeSelectedItemListener l) {
        listenerList.add(WillChangeSelectedItemListener.class, l);
    }

    @Override
    public void removeWillChangeSelectedItemListener(WillChangeSelectedItemListener l) {
        listenerList.remove(WillChangeSelectedItemListener.class, l);
    }

    /**
     * Notifies all listeners that have registered interest for
     * notification on this event type.  The event instance
     * is lazily created using the <code>currentSelectedItem</code> and <code>newSelectedItem</code> parameter.
     */
    public void fireSelectedItemWillChanged(Object currentSelectedItem,
                                            Object newSelectedItem) {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        ComboBoxSelectionEvent e = null;
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == WillChangeSelectedItemListener.class) {
                // Lazily create the event:
                if (e == null)
                    e = new ComboBoxSelectionEvent(this, (Serializable) currentSelectedItem, (Serializable) newSelectedItem);
                ((WillChangeSelectedItemListener) listeners[i + 1]).
                                                                           selectedItemWillChanged(e);
            }
        }
    }
}
