/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.editor;

import org.jdesktop.swingx.JXTable;

import javax.swing.AbstractCellEditor;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class BooleanCellEditor extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {

    private static final long serialVersionUID = 1L;

    protected final TableCellRenderer rendererDelegate;
    protected final TableCellEditor editorDelegate;

    protected final Icon icon;

    public BooleanCellEditor(TableCellRenderer delegate) {
        this(delegate, null);
    }

    public BooleanCellEditor(TableCellRenderer delegate, Icon icon) {
        this.rendererDelegate = delegate;
        this.editorDelegate = new JXTable.BooleanEditor();
        this.icon = icon;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JComponent rendered = (JComponent) rendererDelegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (rendered instanceof JCheckBox) {
            JCheckBox checkBox = (JCheckBox) rendered;
            checkBox.setHorizontalAlignment(JLabel.CENTER);
            checkBox.setVerticalTextPosition(JLabel.TOP);
            checkBox.setBorderPainted(true);
            checkBox.setIcon(icon);
        }
        return rendered;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return editorDelegate.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        return editorDelegate.getCellEditorValue();
    }
}
