package org.nuiton.jaxx.runtime.swing.tree;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.stream.Stream;

/**
 * Created by tchemit on 04/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JTreesTest {

    DefaultMutableTreeNode rootNode;

    @Before
    public void setUp() {
        rootNode = new DefaultMutableTreeNode("a");
        DefaultMutableTreeNode b = new DefaultMutableTreeNode("b");
        rootNode.add(b);
        b.add(new DefaultMutableTreeNode("c"));
        b.add(new DefaultMutableTreeNode("d"));
        DefaultMutableTreeNode c = new DefaultMutableTreeNode("e");
        rootNode.add(c);
        c.add(new DefaultMutableTreeNode("f"));
        c.add(new DefaultMutableTreeNode("g"));
    }

    @Test
    public void preorderStream() {
        Stream<TreeNode> stream = JTrees.preorderStream(rootNode);
        assertStream(stream, "a", "b", "c", "d", "e", "f", "g");
    }

    @Test
    public void postorderStream() {
        Stream<TreeNode> stream = JTrees.postorderStream(rootNode);
        assertStream(stream, "c", "d", "b", "f", "g", "e", "a");
    }

    @Test
    public void breadthFirstStream() {
        Stream<TreeNode> stream = JTrees.breadthFirstStream(rootNode);
        assertStream(stream, "a", "b", "e", "c", "d", "f", "g");
    }

    private void assertStream(Stream<TreeNode> stream, String... values) {
        MutableInt index = new MutableInt(-1);
        stream.forEach(n -> {
            String expectedValue = values[index.incrementAndGet()];
            Assert.assertNotNull(n);
            Assert.assertEquals("Bad node at position " + index.intValue(), expectedValue, n.toString());
        });
    }
}
