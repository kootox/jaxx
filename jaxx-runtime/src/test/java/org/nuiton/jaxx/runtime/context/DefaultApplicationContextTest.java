/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext.AutoLoad;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext.MethodAccess;

import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/** @author Tony Chemit - dev@tchemit.fr */
public class DefaultApplicationContextTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(DefaultApplicationContextTest.class);

    static int helloCount;

    static int helloGetCount;

    static final JAXXContextEntryDef<String> STRING_ENTRY =
            JAXXUtil.newContextEntryDef("myStringEntryKey", String.class);

    static final JAXXContextEntryDef<List<String>> LIST_STRING_ENTRY =
            JAXXUtil.newListContextEntryDef("myListEntryKey");

    @AutoLoad
    @MethodAccess(methodName = "hello", target = String.class)
    public static class Hello {

        public Hello() {
            helloCount++;
        }

        public String hello(String name) {
            helloGetCount++;
            return "hello " + name;
        }
    }

    DefaultApplicationContext context;

    @BeforeClass
    public static void setUpClass() throws Exception {
        helloCount = 0;
        helloGetCount = 0;
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        context = new DefaultApplicationContext();
        assertEquals(0, helloCount);
        assertEquals(0, helloGetCount);
    }

    @After
    public void tearDown() {
        context = null;
        helloCount = helloGetCount = 0;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAutoLoadNamed() {
        context.getContextValue(Hello.class, "fakeName");
    }

    @Test
    public void testAutoLoad() {
        Hello hello = context.getContextValue(Hello.class);
        assertNotNull(hello);
        assertEquals(1, helloCount);

        Hello hello2 = context.getContextValue(Hello.class);
        assertNotNull(hello2);
        assertEquals(1, helloCount);
        assertEquals(hello, hello2);
    }

    @Test
    public void testForward() {
        context.getContextValue(Hello.class);
        assertEquals(1, helloCount);
        String response = context.getContextValue(String.class, "John");
        assertNotNull(response);
        assertEquals(1, helloGetCount);
        assertEquals(new Hello().hello("John"), response);
    }

    @Test
    public void testRemove() {
        String response;

        context.getContextValue(Hello.class);
        assertEquals(1, helloCount);
        assertEquals(1, context.forwards.size());
        response = context.getContextValue(String.class, "John");
        assertNotNull(response);

        context.removeContextValue(Hello.class);
        assertEquals(0, context.forwards.size());
        response = context.getContextValue(String.class, "John");
        assertEquals(1, helloCount);
        assertNull(response);

        // reinstanciate the service
        context.getContextValue(Hello.class);
        assertEquals(2, helloCount);
        assertEquals(1, context.forwards.size());

        // no effect with a name
        context.removeContextValue(Hello.class, "fake");
        assertEquals(1, context.forwards.size());
        context.getContextValue(Hello.class);
        assertEquals(2, helloCount);
    }

    static int yoCount;

    @Test
    public void testEntryListener() {

        PropertyChangeListener listener = evt -> {
            if (log.isInfoEnabled()) {
                log.info("changed detected on " + evt.getSource());
            }
            yoCount++;
        };
        context.addPropertyChangeListener(STRING_ENTRY, "myKey", listener);

        STRING_ENTRY.setContextValue(context, "myValue");

        Assert.assertEquals(1, yoCount);

        STRING_ENTRY.removeContextValue(context);

        Assert.assertEquals(2, yoCount);

        context.removePropertyChangeListener(STRING_ENTRY, "myKey", listener);

        // test that nothing changed now

        STRING_ENTRY.setContextValue(context, "myValue");

        Assert.assertEquals(2, yoCount);

        STRING_ENTRY.removeContextValue(context);

        Assert.assertEquals(2, yoCount);

        // test with a list entry

        context.addPropertyChangeListener(LIST_STRING_ENTRY, "myKey2", listener);

        LIST_STRING_ENTRY.setContextValue(context, Collections.singletonList("myValue"));

        Assert.assertEquals(3, yoCount);

        LIST_STRING_ENTRY.removeContextValue(context);

        Assert.assertEquals(4, yoCount);


        // test that nothing changed now

        context.removePropertyChangeListener(LIST_STRING_ENTRY, "myKey2", listener);

        LIST_STRING_ENTRY.setContextValue(context, Collections.singletonList("myValue2"));

        Assert.assertEquals(4, yoCount);

        LIST_STRING_ENTRY.removeContextValue(context);

        Assert.assertEquals(4, yoCount);


    }


}
