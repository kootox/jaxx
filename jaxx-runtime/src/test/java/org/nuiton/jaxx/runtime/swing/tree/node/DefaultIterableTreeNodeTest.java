package org.nuiton.jaxx.runtime.swing.tree.node;

/*-
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.jaxx.runtime.swing.tree.DefaultIterableTreeNode;
import org.nuiton.jaxx.runtime.swing.tree.IterableTreeNode;

import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 05/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DefaultIterableTreeNodeTest {

    private StringTreeNode rootNode;

    @Before
    public void setUp() {
        rootNode = new StringTreeNode("a", true);
        IterableTreeNode b = new StringTreeNode("b", true);
        rootNode.addChild(b);
        b.addChild(new StringTreeNode("c"));
        b.addChild(new StringTreeNode("d"));
        IterableTreeNode c = new StringTreeNode("e", true);
        rootNode.addChild(c);
        c.addChild(new StringTreeNode("f"));
        c.addChild(new StringTreeNode("g"));
    }

    @Test
    public void stream() {
        Iterator<IterableTreeNode<?>> iterator = rootNode.preorderIterator();
        Stream<IterableTreeNode<?>> stream = IterableTreeNode.stream(iterator);
        Assert.assertNotNull(stream);
        assertStream(stream.filter(n -> ((StringTreeNode) n).getUserObject().contains("a")).collect(Collectors.toList()).iterator(), "a");
    }

    @Test
    public void preorderIterator() {
        Iterator<IterableTreeNode<?>> iterator = rootNode.preorderIterator();
        assertStream(iterator, "a", "b", "c", "d", "e", "f", "g");
    }

    @Test
    public void postorderIterator() {
        Iterator<IterableTreeNode<?>> iterator = rootNode.postorderIterator();
        assertStream(iterator, "c", "d", "b", "f", "g", "e", "a");
    }

    @Test
    public void breadthFirstIterator() {
        Iterator<IterableTreeNode<?>> iterator = rootNode.breadthFirstIterator();
        assertStream(iterator, "a", "b", "e", "c", "d", "f", "g");
    }

    private void assertStream(Iterator<IterableTreeNode<?>> iterator, String... values) {
        MutableInt index = new MutableInt(-1);
        while (iterator.hasNext()) {
            IterableTreeNode<?> n = iterator.next();
            String expectedValue = values[index.incrementAndGet()];
            Assert.assertNotNull(n);
            Assert.assertEquals("Bad node at position " + index.intValue(), expectedValue, n.getUserObject());
        }
        Assert.assertEquals(values.length, index.incrementAndGet());
    }

    private static class StringTreeNode extends DefaultIterableTreeNode<String> {

        public StringTreeNode(String userObject) {
            super(userObject, false);
        }

        public StringTreeNode(String userObject, boolean allowsChildren) {
            super(userObject, allowsChildren);
        }
    }
}
