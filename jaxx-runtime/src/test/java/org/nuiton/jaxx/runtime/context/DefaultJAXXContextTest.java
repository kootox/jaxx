/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.context;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.jaxx.runtime.JAXXContext;

/** @author Tony Chemit - dev@tchemit.fr */
public class DefaultJAXXContextTest {

    DefaultJAXXContext ctxt;

    @Before
    public void initContext() throws Exception {

        // instanciate a new empty context
        ctxt = new DefaultJAXXContext();
    }

    @Test
    public void testGetParentContext() throws Exception {
        JAXXContext expected, result;
        expected = null;
        result = ctxt.getContextValue(JAXXContext.class);
        Assert.assertEquals(expected, result);

        DefaultJAXXContext parentContext = new DefaultJAXXContext();

        ctxt.setContextValue(parentContext);

        expected = parentContext;
        result = ctxt.getContextValue(JAXXContext.class);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSetGetContextValue() throws Exception {
        String expected;
        String result;

        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);

        expected = "yo";
        ctxt.setContextValue(expected);
        result = ctxt.getContextValue(String.class);
        Assert.assertEquals(expected, result);

        expected = "ya";
        ctxt.setContextValue(expected, "second");
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);

        expected = "yi";
        ctxt.setContextValue(expected, "second");
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSetGetContextValueInParentContext() throws Exception {

        // attach parent context
        JAXXContext parentContext = new DefaultJAXXContext();
        ctxt.setContextValue(parentContext);

        String expected;
        String result;

        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);

        expected = "yo";
        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);
        parentContext.setContextValue(expected);
        result = ctxt.getContextValue(String.class);
        Assert.assertEquals(expected, result);

        expected = "ya";
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertNull(result);
        parentContext.setContextValue(expected, "second");
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);

        expected = "yi";
        result = ctxt.getContextValue(String.class, "second");
        parentContext.setContextValue(expected, "second");
        Assert.assertEquals("ya", result);
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSetGetContextValue2() throws Exception {

        // attach parent context
        JAXXContext parentContext = new DefaultJAXXContext();
        ctxt.setContextValue(parentContext);

        String expected;
        String result;

        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);
        result = ctxt.getContextValue(String.class, "yo");
        Assert.assertNull(result);

        expected = "yo";
        ctxt.setContextValue(expected, "yo");

        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);
        result = ctxt.getContextValue(String.class, "yo");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSetGetContextValueInParentParentContext() throws Exception {

        // attach parent parent context
        JAXXContext parentParentContext = new DefaultJAXXContext();
        JAXXContext parentContext = new DefaultJAXXContext();
        parentContext.setContextValue(parentParentContext);
        ctxt.setContextValue(parentContext);

        String expected;
        String result;

        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);

        expected = "yo";
        result = ctxt.getContextValue(String.class);
        Assert.assertNull(result);
        parentParentContext.setContextValue(expected);
        result = ctxt.getContextValue(String.class);
        Assert.assertEquals(expected, result);

        expected = "ya";
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertNull(result);
        parentParentContext.setContextValue(expected, "second");
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);

        expected = "yi";
        result = ctxt.getContextValue(String.class, "second");
        parentParentContext.setContextValue(expected, "second");
        Assert.assertEquals("ya", result);
        result = ctxt.getContextValue(String.class, "second");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testEntrySet() throws Exception {
        Object o = new Object();

        ctxt.setContextValue(o);

        Assert.assertEquals(1, ctxt.data.size());
        Assert.assertEquals(o, ctxt.getContextValue(Object.class));
        Assert.assertEquals(null, ctxt.getContextValue(Object.class, "named"));

        ctxt.setContextValue(o, "named");

        Assert.assertEquals(2, ctxt.data.size());
        Assert.assertEquals(o, ctxt.getContextValue(Object.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object.class, "named"));

        ctxt.removeContextValue(Object.class);
        Assert.assertEquals(1, ctxt.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object.class, "named"));

        ctxt.removeContextValue(Object.class);
        Assert.assertEquals(1, ctxt.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object.class, "named"));

        ctxt.removeContextValue(Object.class, "named");
        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object.class));
        Assert.assertEquals(null, ctxt.getContextValue(Object.class, "named"));

    }

    @Test
    public void testEntrySetWithParent() throws Exception {

        DefaultJAXXContext parentContext = new DefaultJAXXContext();

        ctxt.setContextValue(parentContext);

        class Object2 {

        }
        Object o = new Object2();

        parentContext.setContextValue(o);

        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(1, parentContext.data.size());
        Assert.assertEquals(o, ctxt.getContextValue(Object2.class));
        Assert.assertEquals(null, ctxt.getContextValue(Object2.class, "named"));

        parentContext.setContextValue(o, "named");

        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(2, parentContext.data.size());
        Assert.assertEquals(o, ctxt.getContextValue(Object2.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object2.class, "named"));

        parentContext.removeContextValue(Object2.class);
        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(1, parentContext.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object2.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object2.class, "named"));

        parentContext.removeContextValue(Object2.class);
        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(1, parentContext.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object2.class));
        Assert.assertEquals(o, ctxt.getContextValue(Object2.class, "named"));

        ctxt.removeContextValue(Object2.class, "named");
        Assert.assertEquals(0, ctxt.data.size());
        Assert.assertEquals(0, parentContext.data.size());
        Assert.assertEquals(null, ctxt.getContextValue(Object2.class));
        Assert.assertEquals(null, ctxt.getContextValue(Object2.class, "named"));

    }

}
