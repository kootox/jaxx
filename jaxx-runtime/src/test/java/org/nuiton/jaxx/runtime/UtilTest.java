/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import org.junit.Assert;
import org.junit.Test;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class UtilTest {

    int count;

    @Test
    public void testGetEventListener() {
        count = 0;
        DocumentListener listener = JAXXUtil.getEventListener(DocumentListener.class, this, "incCount");
        listener.insertUpdate(null);
        Assert.assertEquals(count, 1);
        DocumentListener listener2 = JAXXUtil.getEventListener(DocumentListener.class, this, "incCount");
        listener2.removeUpdate(null);
        Assert.assertEquals(count, 2);
        //assertTrue("Received two different event listeners despite using identical parameters", listener == listener2);
    }


    public void incCount(DocumentEvent e) {
        count++;
    }
}
