package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
class TimeSliderInitializer {

    public void init(JSlider slider) {

        // create slider labels
        Map<Integer, JLabel> labelTable = new Hashtable<>();
        for (int i = 0; i < 25; i += 2) {
            labelTable.put(i * 60, new JLabel(i + ""));
        }
        slider.setLabelTable((Dictionary<?, ?>) labelTable);

        slider.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {

                JSlider component = (JSlider) e.getComponent();

                if (component.isEnabled()) {

                    component.setValueIsAdjusting(true);

                    try {

                        int value = getSliderValue(e);
                        component.setValue(value);

                    } finally {
                        component.setValueIsAdjusting(false);
                    }

                }

                showToolTip(e);
                e.consume();

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                showToolTip(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                showToolTip(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        slider.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                showToolTip(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                showToolTip(e);
            }
        });
        slider.addMouseWheelListener(e -> {

            JSlider component = (JSlider) e.getComponent();

            if (component.isEnabled()) {

                // compute new value
                int nb = e.getWheelRotation();
                int value = component.getValue() - nb;

                // set the value
                component.setValueIsAdjusting(true);
                try {
                    component.setValue(value);
                } finally {
                    component.setValueIsAdjusting(false);
                }

            }
            e.consume();
        });
    }

    int getSliderValue(MouseEvent e) {
        JSlider slider = (JSlider) e.getSource();
        int value = -1;
        if (slider.getUI() instanceof BasicSliderUI) {
            BasicSliderUI ui = (BasicSliderUI) slider.getUI();
            value = slider.getOrientation() == JSlider.HORIZONTAL
                    ? ui.valueForXPosition(e.getX())
                    : ui.valueForYPosition(e.getY());
        }
        return value;
    }

    void showToolTip(MouseEvent e) {

        int value = getSliderValue(e);
        if (value == -1) {
            return;
        }
        int h = value / 60;
        int m = value % 60;

        String text = "";
        if (h < 10) {
            text = "0";
        }
        text += h + " : ";
        if (m < 10) {
            text += "0";
        }
        text += m;

        JSlider source = (JSlider) e.getSource();
        source.setToolTipText(text);

    }
}
