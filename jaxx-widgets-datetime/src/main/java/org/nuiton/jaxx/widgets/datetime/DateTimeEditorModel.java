package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Predicate;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.runtime.util.DateUtil;
import org.nuiton.jaxx.widgets.ModelToBean;

/**
 * Created on 9/9/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DateTimeEditorModel extends AbstractSerializableBean implements ModelToBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DAY_DATE = "dayDate";

    public static final String PROPERTY_TIME_DATE = "timeDate";

    public static final String PROPERTY_TIME_IN_MINUTES = "timeInMinutes";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_TIME_EDITABLE = "timeEditable";

    public static final String PROPERTY_DATE_EDITABLE = "dateEditable";

    public static final String PROPERTY_VALUE_IS_ADJUSTING = "valueIsAdjusting";

    /**
     * Optional bean where to push back dates.
     */
    protected Object bean;

    /**
     * Optional bean property where to push back the day date.
     */
    protected String propertyDayDate;

    /**
     * Optional bean property where to push back the time date.
     */
    protected String propertyTimeDate;

    /**
     * Optional bean property where to push back the full date.
     */
    protected String propertyDate;

    /**
     * Is date editable ?
     */
    protected boolean dateEditable = true;

    /**
     * Is time editable ?
     */
    protected boolean timeEditable = true;


    /**
     * Full date (date + time)
     */
    protected Date date;

    /**
     * State to be able to custom the model. will be pass to {@code false} by the {@link DateTimeEditorHandler#init(DateTimeEditor)}.
     */
    protected final boolean fillState = true;

    /**
     * To stop propagate events when we are doing some modifications on the model.
     */
    protected boolean valueIsAdjusting;

    protected final Calendar calendar = new GregorianCalendar();

    public String getPropertyDayDate() {
        return propertyDayDate;
    }

    public void setPropertyDayDate(String propertyDayDate) {
        Preconditions.checkState(fillState, "cant change *propertyDayDate* property once the fillState is off.");
        this.propertyDayDate = propertyDayDate;
    }

    public String getPropertyTimeDate() {
        return propertyTimeDate;
    }

    public void setPropertyTimeDate(String propertyTimeDate) {
        Preconditions.checkState(fillState, "cant change *propertyTimeDate* property once the fillState is off.");
        this.propertyTimeDate = propertyTimeDate;
    }

    public String getPropertyDate() {
        return propertyDate;
    }

    public void setPropertyDate(String propertyDate) {
        Preconditions.checkState(fillState, "cant change *propertyDate* property once the fillState is off.");
        this.propertyDate = propertyDate;
    }

    @Override
    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Preconditions.checkState(fillState, "cant change *bean* property once the fillState is off.");
        this.bean = bean;
    }

    public boolean isDateEditable() {
        return dateEditable;
    }

    public void setDateEditable(boolean dateEditable) {
        this.dateEditable = dateEditable;
        firePropertyChange(PROPERTY_DATE_EDITABLE, null, dateEditable);
    }

    public boolean isTimeEditable() {
        return timeEditable;
    }

    public void setTimeEditable(boolean timeEditable) {
        this.timeEditable = timeEditable;
        firePropertyChange(PROPERTY_TIME_EDITABLE, null, timeEditable);
    }

    public boolean isValueIsAdjusting() {
        return valueIsAdjusting;
    }

    public void setValueIsAdjusting(boolean valueIsAdjusting) {
        boolean oldValue = isValueIsAdjusting();
        this.valueIsAdjusting = valueIsAdjusting;
        fireValueIsAdjusting(oldValue);
    }

    public void setHour(Integer hour) {

        if (isValueIsAdjusting()) {

            // avoid reentrant code
            return;
        }

        Date oldTimeDate = getTimeDate();

        if (oldTimeDate != null) {

            Integer minute = getMinute(oldTimeDate);

            setTime(oldTimeDate, hour, minute);

        }

    }

    public void setMinute(Integer minute) {

        if (isValueIsAdjusting()) {

            // avoid reentrant code
            return;
        }

        Date oldTimeDate = getTimeDate();

        if (oldTimeDate != null) {

            Integer hour = getHour(oldTimeDate);

            setTime(oldTimeDate, hour, minute);

        }

    }

    public Integer getTimeInMinutes() {

        Integer result = 0;

        Date timeDate = getTimeDate();

        if (timeDate != null) {

            int hour = getHour(timeDate);
            int minute = getMinute(timeDate);

            result = hour * 60 + minute;

        }

        return result;

    }

    public void setTimeInMinutes(Integer time) {

        if (isValueIsAdjusting()) {

            // avoid reentrant code
            return;
        }

        Date oldTimeDate = getTimeDate();

        if (oldTimeDate != null) {

            int hour = time / 60;
            int minute = time % 60;
            setTime(oldTimeDate, hour, minute);

        }

    }

    public Date getTimeDate() {
        return date == null ? null : DateUtil.getTime(date, false, false);
    }

    public void setTimeDate(Date timeDate) {
        if (date != null) {

            Date oldDate = date;
            Date oldTimeDate = getTimeDate();
            Integer oldTimeInMinutes = getTimeInMinutes();

            this.date = DateUtil.getDateAndTime(date, timeDate, true, false);
            fireDate(oldDate);
            fireTimeDate(oldTimeDate);
            fireTimeInMinutes(oldTimeInMinutes);

        }
    }

    public Date getDayDate() {
        return date == null ? null : DateUtil.getDay(date);
    }

    public void setDayDate(Date dayDate) {
        if (date != null) {

            Date oldDate = date;
            Date oldDayDate = getDayDate();
            this.date = DateUtil.getDateAndTime(dayDate, date, true, false);
            fireDate(oldDate);
            fireDayDate(oldDayDate);

        }
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {

        Date oldDayDate = getDayDate();
        Date oldTimeDate = getTimeDate();
        Date oldValue = getDate();
        Integer oldTimeInMinutes = getTimeInMinutes();

        this.date = date;

        fireDate(oldValue);
        fireDayDate(oldDayDate);
        fireTimeDate(oldTimeDate);
        fireTimeInMinutes(oldTimeInMinutes);

    }

    protected Integer getDay(Date date) {

        Integer hour = null;
        if (date != null) {
            calendar.setTime(date);
            hour = calendar.get(Calendar.DAY_OF_MONTH);
        }
        return hour;

    }

    protected Integer getHour(Date date) {

        Integer hour = null;
        if (date != null) {
            calendar.setTime(date);
            hour = calendar.get(Calendar.HOUR_OF_DAY);
        }
        return hour;

    }

    protected Integer getMinute(Date date) {

        Integer hour = null;
        if (date != null) {
            calendar.setTime(date);
            hour = calendar.get(Calendar.MINUTE);
        }
        return hour;

    }

    protected void setTime(Date oldTimeDate, Integer hour, Integer minute) {

        if (oldTimeDate != null) {

            Date oldFullDate = getDate();
            Integer oldTimeInMinutes = getTimeInMinutes();

            setValueIsAdjusting(true);

            try {

                calendar.setTime(oldTimeDate);
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);

                Date newTimeDate = calendar.getTime();

                setTimeDate(newTimeDate);

            } finally {

                setValueIsAdjusting(false);

                fireDate(oldFullDate);
                fireTimeDate(oldTimeDate);
                fireTimeInMinutes(oldTimeInMinutes);

            }

        }

    }

    protected void fireTimeDate(Date oldDate) {
        firePropertyChange(PROPERTY_TIME_DATE, oldDate, getTimeDate());
    }

    protected void fireTimeInMinutes(Integer oldDate) {
        firePropertyChange(PROPERTY_TIME_IN_MINUTES, oldDate, getTimeInMinutes());
    }

    protected void fireDayDate(Date oldDate) {
        firePropertyChange(PROPERTY_DAY_DATE, oldDate, getDayDate());
    }

    protected void fireDate(Date oldDate) {
        firePropertyChange(PROPERTY_DATE, oldDate, getDate());
    }

    protected void fireValueIsAdjusting(boolean oldValue) {
        firePropertyChange(PROPERTY_VALUE_IS_ADJUSTING, oldValue, isValueIsAdjusting());
    }

    protected Predicate<DateTimeEditorModel> canUpdateBeanValuePredicate() {
        return input -> !isValueIsAdjusting();
    }
}
