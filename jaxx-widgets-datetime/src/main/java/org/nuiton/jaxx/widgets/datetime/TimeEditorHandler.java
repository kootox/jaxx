package org.nuiton.jaxx.widgets.datetime;

/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.function.Predicate;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.MutateOnConditionalPropertyChangeListener;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class TimeEditorHandler implements UIHandler<TimeEditor> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TimeEditorHandler.class);

    protected TimeEditor ui;

    @Override
    public void beforeInit(TimeEditor ui) {

        TimeEditorModel model = new TimeEditorModel();
        ui.setContextValue(model);

        this.ui = ui;

    }

    @Override
    public void afterInit(TimeEditor ui) {

        ui.getMinuteEditor().setEditor(new JSpinner.DateEditor(ui.getMinuteEditor(), "mm"));
        ui.getHourEditor().setEditor(new JSpinner.DateEditor(ui.getHourEditor(), "HH"));

        JSlider slider = ui.getSlider();

        TimeSliderInitializer timeSliderInitializer = new TimeSliderInitializer();
        timeSliderInitializer.init(slider);

    }

    public void init(TimeEditor ui) {

        TimeEditorModel model = ui.getModel();

        Object bean = model.getBean();

        if (bean != null) {

            Predicate<TimeEditorModel> predicate = model.canUpdateBeanValuePredicate();

            if (model.getPropertyTime() != null) {

                Method mutator = Setters.getMutator(bean, model.getPropertyTime());
                Objects.requireNonNull(mutator, "could not find mutator for " + model.getPropertyTime());

                // When model time date changed, let's push it back in bean
                model.addPropertyChangeListener(
                        TimeEditorModel.PROPERTY_TIME,
                        new MutateOnConditionalPropertyChangeListener<>(model, mutator, predicate));

            }

        }

    }

    protected final Calendar calendarMinute = new GregorianCalendar();

    protected final Calendar calendarHour = new GregorianCalendar();

    public Date getMinuteModelValue(Date incomingDate) {
        if (incomingDate == null) {
            incomingDate = new Date();
        }
        calendarMinute.setTime(incomingDate);
        calendarMinute.set(Calendar.HOUR_OF_DAY, 0);
        incomingDate = calendarMinute.getTime();
        return incomingDate;
    }

    public Date getHourModelValue(Date incomingDate) {
        if (incomingDate == null) {
            incomingDate = new Date();
        }
        calendarHour.setTime(incomingDate);
        calendarHour.set(Calendar.MINUTE, 0);
        incomingDate = calendarHour.getTime();
        return incomingDate;
    }

    public void setHours(Date hourDate) {

        TimeEditorModel model = ui.getModel();

        Date oldTime = model.getTime();

        if (oldTime == null) {
            return;
        }

        calendarHour.setTime(hourDate);
        int newHour = calendarHour.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarHour.get(Calendar.MINUTE);

        int oldHour = model.getHour(oldTime);
        int oldMinute = model.getMinute(oldTime);

        if (oldHour == newHour && oldMinute == newMinute) {

            // do nothing, same data
            if (log.isDebugEnabled()) {
                log.debug("Do not update time model , stay on same time = " + oldHour + ":" + oldMinute);
            }
            return;
        }

        // by default stay on same hour

        // by default, use the new minute data

        if (log.isDebugEnabled()) {
            log.debug("hh:mm (old from dateModel)   = " + oldHour + ":" + oldMinute);
            log.debug("hh:mm (new from hourModel) = " + newHour + ":" + newMinute);
        }

        // change time
        model.setTimeInMinutes(newHour * 60 + oldMinute);

    }

    public void setMinutes(Date minuteDate) {

        TimeEditorModel model = ui.getModel();

        Date oldTime = model.getTime();

        if (oldTime == null) {
            return;
        }

        calendarMinute.setTime(minuteDate);
        int newHour = calendarMinute.get(Calendar.HOUR_OF_DAY);
        int newMinute = calendarMinute.get(Calendar.MINUTE);

        int oldHour = model.getHour(oldTime);
        int oldMinute = model.getMinute(oldTime);

        if (oldHour == newHour && oldMinute == newMinute) {

            // do nothing, same data
            if (log.isDebugEnabled()) {
                log.debug("Do not update time model , stay on same time = " + oldHour + ":" + oldMinute);
            }
            return;
        }

        // by default stay on same hour
        int hour = oldHour;

        // by default, use the new minute data

        if (log.isDebugEnabled()) {
            log.debug("hh:mm (old from dateModel)   = " + oldHour + ":" + oldMinute);
            log.debug("hh:mm (new from minuteModel) = " + newHour + ":" + newMinute);
        }

        if (newMinute == 0) {

            // minute pass to zero (check if a new hour is required)
            if (newHour == 1) {

                hour = (oldHour + 1) % 24;

            }
        } else if (newMinute == 59) {

            // minute pass to 59 (check if a new hour is required)

            if (newHour == 23) {

                // decrease hour
                hour = (oldHour - 1) % 24;

            }
        }

        // time has changed
        if (log.isDebugEnabled()) {
            log.debug("Update time model to hh:mm = " + hour + ":" + newMinute);
        }

        model.setTimeInMinutes(hour * 60 + newMinute);
    }

}
