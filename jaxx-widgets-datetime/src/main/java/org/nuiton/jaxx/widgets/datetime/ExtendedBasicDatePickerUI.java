/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.datetime;

import org.jdesktop.swingx.plaf.basic.BasicDatePickerUI;

import javax.swing.JButton;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Date;

/**
 * @author Sylvain Lletellier
 */
public class ExtendedBasicDatePickerUI extends BasicDatePickerUI {

    public static final String PROPERTY_SHOW_POPUP_BUTTON = "showPopupButton";

    protected JButton popupButton;
    protected final PropertyChangeSupport p;
    protected boolean showPopupButton;

    public ExtendedBasicDatePickerUI() {
        p = new PropertyChangeSupport(this);
        p.addPropertyChangeListener(PROPERTY_SHOW_POPUP_BUTTON, evt -> {

            // dont do this on init
            if (datePicker != null) {
                installComponents();
                installListeners();
            }
        });
    }

    public boolean isShowPopupButton() {
        return showPopupButton;
    }

    public void setShowPopupButton(boolean showPopupButton) {
        boolean oldValue = isShowPopupButton();
        this.showPopupButton = showPopupButton;
        p.firePropertyChange("showPopupButton", oldValue, showPopupButton);
    }

    @Override
    protected JButton createPopupButton() {

        if (datePicker != null && popupButton != null) {
            datePicker.remove(popupButton);
        }
        if (isShowPopupButton()) {
            popupButton = super.createPopupButton();
            return popupButton;
        }
        return null;
    }

    @Override
    public Date getSelectableDate(Date date) throws PropertyVetoException {
        if (date == null) {
            // one place to interrupt the update spiral
            throw new PropertyVetoException("date not selectable", null);
        }
        return date;
    }
}
