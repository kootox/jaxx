package org.nuiton.jaxx.widgets.datetime.session;

/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.session.State;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class TimeEditorState implements State {

    protected Boolean showTimeEditorSlider = false;

    public boolean getShowTimeEditorSlider() {
        return showTimeEditorSlider;
    }

    public void setShowTimeEditorSlider(boolean showTimeEditorSlider) {
        this.showTimeEditorSlider = showTimeEditorSlider;
    }

    @Override
    public State getState(Object o) {
        TimeEditor list = checkComponent(o);
        TimeEditorState state = new TimeEditorState();
        state.setShowTimeEditorSlider(list.getShowTimeEditorSlider());
        return state;
    }

    @Override
    public void setState(Object o, State state) {
        if (!(state instanceof TimeEditorState)) {
            throw new IllegalArgumentException("invalid state");
        }

        TimeEditor list = checkComponent(o);
        TimeEditorState beanDoubleListState = (TimeEditorState) state;
        list.setShowTimeEditorSlider(beanDoubleListState.getShowTimeEditorSlider());

    }

    protected TimeEditor checkComponent(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("null component");
        }
        if (!(o instanceof TimeEditor)) {
            throw new IllegalArgumentException("invalid component");
        }
        return (TimeEditor) o;
    }

}
