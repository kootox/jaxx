/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.Insets;

public class InsetsConverterTest {

    InsetsConverter converter;

    @Before
    public void setUp() {
        converter = new InsetsConverter();
    }

    @Test
    public void testSingleValue() {
        Insets value = (Insets) converter.convertFromString("3", Insets.class);
        Assert.assertEquals(value, new Insets(3, 3, 3, 3));
    }

    @Test
    public void testFourValues() {
        Insets value = (Insets) converter.convertFromString("3, 0, 12, 1000000", Insets.class);
        Assert.assertEquals(value, new Insets(3, 0, 12, 1000000));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTwoValues() {
        converter.convertFromString("0, 4", Insets.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThreeValues() {
        converter.convertFromString("0, 4, 9", Insets.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidNumber() {
        converter.convertFromString("0, 4, 9, A", Insets.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadFormatting() {
        converter.convertFromString("0 - 1 - 2 - 3", Insets.class);
    }
}
