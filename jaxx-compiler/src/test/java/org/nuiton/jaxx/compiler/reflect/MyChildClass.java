/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.reflect;

/**
 * A child class to test the bug https://forge.nuiton.org/issues/show/1470
 *
 * To test if a child class obtains properties from his super class.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4.2
 */
public class MyChildClass extends MyClass implements Cloneable {

    private static final long serialVersionUID = 2L;

    protected String childProperty;

    public String getChildProperty() {
        return childProperty;
    }

    public void setChildProperty(String childProperty) {
        this.childProperty = childProperty;
    }
}
