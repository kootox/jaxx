/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.reflect.resolvers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.reflect.MyAbstractClass;
import org.nuiton.jaxx.compiler.reflect.MyChildClass;
import org.nuiton.jaxx.compiler.reflect.MyChildClass2;
import org.nuiton.jaxx.compiler.reflect.MyClass;
import org.nuiton.jaxx.compiler.reflect.MyEnum;
import org.nuiton.jaxx.compiler.reflect.MyInterface;
import org.nuiton.jaxx.compiler.reflect.MyInterface2;
import org.nuiton.jaxx.compiler.reflect.MyInterface3;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.nuiton.jaxx.runtime.util.FileUtil;

/**
 * Tests the {@link ClassDescriptorResolverFromJavaFile} resolver.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class ClassDescriptorResolverFromJavaFileTest {

    /** log */
    protected static final Log log =
            LogFactory.getLog(ClassDescriptorResolverFromJavaFileTest.class);


    /** test source root directory */
    public static File testSourceRoot;

    @BeforeClass
    public static void initBaseDir() {
        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        testSourceRoot = FileUtil.getFileFromPaths(new File(basedir),
                                                   "src",
                                                   "test",
                                                   "java");
    }

    protected ClassDescriptor getDescriptor(Class<?> klass) throws Exception {

        String javaFilePath =
                klass.getName().replaceAll("\\.", File.separator) + ".java";
        File src = new File(testSourceRoot, javaFilePath);
        Assert.assertTrue(src.exists());

        if (log.isDebugEnabled()) {
            log.debug("file to parse " + src);
        }

        ClassDescriptorResolverFromJavaFile resolver =
                new ClassDescriptorResolverFromJavaFile();

        ClassDescriptor descriptor =
                resolver.resolvDescriptor(klass.getName(), src.toURI().toURL());

        Assert.assertNotNull(descriptor);
        if (log.isDebugEnabled()) {
            log.debug("loaded " + descriptor);
        }
        Assert.assertEquals(klass.getName(), descriptor.getName());
        return descriptor;
    }

    @Test
    public void parseClassDescriptorResolverFromJavaFileTest() throws Exception {

        ClassDescriptor descriptor =
                getDescriptor(ClassDescriptorResolverFromJavaFileTest.class);

        assertInterfaces(descriptor);
        assertSuperClass(descriptor, Object.class);
    }


    @Test
    public void parseMyEnum() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyEnum.class);

        assertInterfaces(descriptor, Serializable.class, MyInterface.class);
        assertInterfaces(descriptor, MyEnum.class.getInterfaces());
        assertSuperClass(descriptor, Enum.class);

        assertIsAssignableFrom(
                descriptor,
                Serializable.class,
                MyInterface.class,
                Enum.class
        );
    }

    @Test
    public void parseMyInterface() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyInterface.class);

        assertInterfaces(descriptor);
        assertSuperClass(descriptor, null);
    }

    @Test
    public void parseMyInterface2() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyInterface2.class);

        assertInterfaces(descriptor, MyInterface2.class.getInterfaces());
        assertInterfaces(descriptor, ConfigOptionDef.class);
        assertSuperClass(descriptor, null);
    }


    @Test
    public void parseMyInterface3() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyInterface3.class);

        assertInterfaces(descriptor, MyInterface3.class.getInterfaces());
        assertInterfaces(descriptor, ConfigOptionDef.class,
                         Iterable.class);
        assertSuperClass(descriptor, null);
    }

    @Test
    public void parseMyAbstractClass() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyAbstractClass.class);

        assertInterfaces(descriptor, MyAbstractClass.class.getInterfaces());
        assertInterfaces(descriptor, MyInterface.class);
        assertSuperClass(descriptor, Object.class);
        assertIsAssignableFrom(descriptor, MyInterface.class, Object.class);
    }

    @Test
    public void parseMyClass() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyClass.class);

        assertInterfaces(descriptor, MyClass.class.getInterfaces());
        assertInterfaces(descriptor, Serializable.class);
        assertSuperClass(descriptor, MyAbstractClass.class);
        assertIsAssignableFrom(descriptor,
                               Serializable.class,
                               MyInterface.class,
                               MyAbstractClass.class
        );

        assertDeclaredField(
                descriptor,
                "serialVersionUID",
                long.class,
                Modifier.PRIVATE | Modifier.FINAL | Modifier.STATIC
        );

        assertDeclaredField(
                descriptor,
                "myPrivateStringField",
                String.class,
                Modifier.PRIVATE | Modifier.FINAL
        );

        assertDeclaredField(
                descriptor,
                "myProtectedStringField",
                String.class,
                Modifier.PROTECTED
        );

        assertField(
                descriptor,
                "myPublicStringField",
                String.class,
                Modifier.PUBLIC
        );
    }

    @Test
    public void parseMyChildClass() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyChildClass.class);

        assertInterfaces(descriptor, MyChildClass.class.getInterfaces());
        assertInterfaces(descriptor, Cloneable.class);
        assertSuperClass(descriptor, MyClass.class);
        assertIsAssignableFrom(descriptor,
                               Serializable.class,
                               MyInterface.class,
                               MyAbstractClass.class,
                               MyClass.class
        );

        assertDeclaredField(
                descriptor,
                "serialVersionUID",
                long.class,
                Modifier.PRIVATE | Modifier.FINAL | Modifier.STATIC
        );

        assertDeclaredField(
                descriptor,
                "myPrivateStringField",
                String.class,
                Modifier.PRIVATE | Modifier.FINAL
        );

        assertDeclaredField(
                descriptor,
                "myProtectedStringField",
                String.class,
                Modifier.PROTECTED
        );

        assertDeclaredField(
                descriptor,
                "childProperty",
                String.class,
                Modifier.PROTECTED
        );

        assertField(
                descriptor,
                "myPublicStringField",
                String.class,
                Modifier.PUBLIC
        );

    }

    @Test
    public void parseMyChildClass2() throws Exception {

        ClassDescriptor descriptor = getDescriptor(MyChildClass2.class);

        assertInterfaces(descriptor, MyChildClass2.class.getInterfaces());
        assertSuperClass(descriptor, MyClass.class);
        assertIsAssignableFrom(descriptor,
                               Serializable.class,
                               MyInterface.class,
                               MyAbstractClass.class,
                               MyClass.class
        );

        assertDeclaredField(
                descriptor,
                "serialVersionUID",
                long.class,
                Modifier.PRIVATE | Modifier.FINAL | Modifier.STATIC
        );

        assertDeclaredField(
                descriptor,
                "myPrivateStringField",
                String.class,
                Modifier.PRIVATE | Modifier.FINAL
        );

        assertDeclaredField(
                descriptor,
                "myProtectedStringField",
                String.class,
                Modifier.PROTECTED
        );

        assertField(
                descriptor,
                "myPublicStringField",
                String.class,
                Modifier.PUBLIC
        );

    }

    public static void assertField(ClassDescriptor descriptor,
                                   String fieldName,
                                   Class<?> fieldType,
                                   int fieldModifiers) throws NoSuchFieldException {
        FieldDescriptor fieldDescriptor =
                descriptor.getFieldDescriptor(fieldName);
        Assert.assertNotNull(fieldDescriptor);
        ClassDescriptor type = fieldDescriptor.getType();
        Assert.assertNotNull(type);
        Assert.assertEquals(fieldType.getName(), type.getName());
        int modifiers = fieldDescriptor.getModifiers();
        Assert.assertEquals(fieldModifiers, modifiers);
    }

    public static void assertDeclaredField(ClassDescriptor descriptor,
                                           String fieldName,
                                           Class<?> fieldType,
                                           int fieldModifiers) throws NoSuchFieldException {
        FieldDescriptor fieldDescriptor =
                descriptor.getDeclaredFieldDescriptor(fieldName);
        Assert.assertNotNull(fieldDescriptor);
        ClassDescriptor type = fieldDescriptor.getType();
        Assert.assertNotNull(type);
        Assert.assertEquals(fieldType.getName(), type.getName());
        int modifiers = fieldDescriptor.getModifiers();
        Assert.assertEquals(fieldModifiers, modifiers);
    }


    public static void assertIsAssignableFrom(ClassDescriptor descriptor,
                                              Class<?>... interfaces) {

        for (Class<?> anInterface : interfaces) {
            ClassDescriptor descriptor2 =
                    ClassDescriptorHelper.getClassDescriptor(anInterface);
            Assert.assertNotNull(descriptor2);
            boolean value = descriptor2.isAssignableFrom(descriptor);
            Assert.assertTrue(
                    anInterface + " should be assignable from " + descriptor,
                    value
            );
        }
    }

    public static void assertInterfaces(ClassDescriptor descriptor,
                                        Class<?>... interfaces) {
        ClassDescriptor[] descriptors = descriptor.getInterfaces();
        Assert.assertEquals(interfaces.length, descriptors.length);

        List<String> doFind = new ArrayList<>();
        for (Class<?> anInterface : interfaces) {
            doFind.add(anInterface.getName());
        }

        for (ClassDescriptor descriptor1 : descriptors) {
            String name = descriptor1.getName();
            Assert.assertTrue(doFind.contains(name));
            doFind.remove(name);
        }
        Assert.assertTrue(
                "The follwing interfaces were not find found : " + doFind,
                doFind.isEmpty()
        );
    }

    public static void assertSuperClass(ClassDescriptor descriptor,
                                        Class<?> superClass) {
        ClassDescriptor superDescriptor = descriptor.getSuperclass();
        if (superClass == null) {
            Assert.assertNull(
                    "Should be null but was " + superDescriptor,
                    superDescriptor
            );
        } else {
            Assert.assertNotNull(superDescriptor);
            Assert.assertEquals(superClass.getName(), superDescriptor.getName());
        }

    }

}
