/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.java;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jaxx.compiler.java.JavaMethod.MethodOrder;

import java.lang.reflect.Modifier;
import java.util.EnumSet;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JavaMethodTest {
    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(JavaMethodTest.class);

    @Test
    public void testGetMethodOrderScope() {

        EnumSet<MethodOrder> allConstants = EnumSet.allOf(MethodOrder.class);
        if (log.isDebugEnabled()) {
            for (MethodOrder allConstant : allConstants) {
                log.debug("\n" + allConstant.getHeader());
            }
        }

        EnumSet<MethodOrder> constants;

        constants = JavaMethod.getMethodOrderScope(allConstants, Modifier.STATIC);

        Assert.assertEquals(1, constants.size());
        Assert.assertTrue(constants.contains(MethodOrder.statics));

        constants = JavaMethod.getMethodOrderScope(allConstants, Modifier.PUBLIC);

        Assert.assertEquals(8, constants.size());
        Assert.assertTrue(constants.contains(MethodOrder.constructors));
        Assert.assertTrue(constants.contains(MethodOrder.JAXXObject));
        Assert.assertTrue(constants.contains(MethodOrder.JAXXContext));
        Assert.assertTrue(constants.contains(MethodOrder.JAXXValidation));
        Assert.assertTrue(constants.contains(MethodOrder.events));
        Assert.assertTrue(constants.contains(MethodOrder.publicGetters));
        Assert.assertTrue(constants.contains(MethodOrder.publicSetters));
        Assert.assertTrue(constants.contains(MethodOrder.otherPublic));

        constants = JavaMethod.getMethodOrderScope(allConstants, Modifier.PROTECTED);

        Assert.assertEquals(4, constants.size());
        Assert.assertTrue(constants.contains(MethodOrder.protectedGetters));
        Assert.assertTrue(constants.contains(MethodOrder.protecteds));
        Assert.assertTrue(constants.contains(MethodOrder.createMethod));
        Assert.assertTrue(constants.contains(MethodOrder.internalMethod));

        constants = JavaMethod.getMethodOrderScope(allConstants, Modifier.PRIVATE);
        Assert.assertEquals(3, constants.size());

        Assert.assertTrue(constants.contains(MethodOrder.createMethod));
        Assert.assertTrue(constants.contains(MethodOrder.packageLocal));
        Assert.assertTrue(constants.contains(MethodOrder.privates));

    }
}
