/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.Color;

public class ColorConverterTest {

    ColorConverter converter;

    @Before
    public void setUp() {
        converter = new ColorConverter();
    }

    @Test
    public void testHexValue() {
        Color value = (Color) converter.convertFromString("#3000FF", Color.class);
        Assert.assertEquals(value, new Color(48, 0, 255));
    }

    @Test
    public void testUpperCaseConstant() {
        Color value = (Color) converter.convertFromString("RED", Color.class);
        Assert.assertEquals(value, Color.RED);
    }

    @Test
    public void testLowerCaseConstant() {
        Color value = (Color) converter.convertFromString("blue", Color.class);
        Assert.assertEquals(value, Color.blue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMissingHash() {
        converter.convertFromString("ABCDEF", Color.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidNumber() {
        converter.convertFromString("#ABCDEG", Color.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidConstant() {
        converter.convertFromString("rEd", Color.class);
    }
}
