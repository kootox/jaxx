/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.jaxx.compiler.CompilerConfiguration;
import org.nuiton.jaxx.compiler.DefaultCompilerConfiguration;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXFactory;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;

import javax.swing.JPopupMenu;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class TagManagerTest {

    protected static CompilerConfiguration configuration;

    protected JAXXCompiler compiler;

    public static class TestHandler extends DefaultObjectHandler {

        public TestHandler(ClassDescriptor beanClass) {
            super(beanClass);
        }
    }

    @BeforeClass
    public static void init() throws Exception {
        if (configuration == null) {
            configuration = new DefaultCompilerConfiguration();
        }
//        TagManager.reset();
        JAXXFactory.setConfiguration(configuration);
        JAXXFactory.initFactory();

    }

    @Before
    public void setUp() {
        JAXXFactory.newDummyEngine();
        compiler = JAXXFactory.newDummyCompiler(JAXXCompiler.class.getClassLoader());
//        compiler = new JAXXCompiler(JAXXCompiler.class.getClassLoader());
        compiler.addImport("javax.swing.*");

    }

    @Test
    public void testRegisterBean() {
        TagManager.registerBean(ClassDescriptorHelper.getClassDescriptor(InputStream.class), TestHandler.class);

        Assert.assertTrue(TagManager.getTagHandler(ClassDescriptorHelper.getClassDescriptor(InputStream.class)) instanceof TestHandler);
        Assert.assertTrue(TagManager.getTagHandler(ClassDescriptorHelper.getClassDescriptor(FileInputStream.class)) instanceof TestHandler);
    }

    @Test
    public void testRegisterDefaultNamespace() {

        TagManager.registerBean(ClassDescriptorHelper.getClassDescriptor(OutputStream.class), TestHandler.class);

        TagManager.registerDefaultNamespace("OutputStream", "java.io.*");
        Assert.assertTrue("Could not find handler for OutputStream despite default namespace", TagManager.getTagHandler(null, "OutputStream", compiler) instanceof TestHandler);

        PrintStream oldErr = System.err;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        System.setErr(new PrintStream(buffer));
        TagManager.registerDefaultNamespace("OutputStream", "java.dummy.*");
        Assert.assertNull("Found handler for OutputStream despite ambiguous default namespace", TagManager.getTagHandler(null, "OutputStream", compiler));
        System.setErr(oldErr);
        Assert.assertTrue("No errors were produced with an ambiguous default namespace", buffer.size() > 0);
        Assert.assertTrue(buffer.size() > 0);
    }

    @Test
    public void testResolveClassName() {
        Assert.assertEquals("Could not resolve class name 'Object'", TagManager.resolveClassName("Object", compiler), "java.lang.Object");
        Assert.assertEquals("Could not resolve class name 'java.lang.Object'", TagManager.resolveClassName("java.lang.Object", compiler), "java.lang.Object");
        Assert.assertNull("Unexpectedly resolved class name 'java.awt.Object'", TagManager.resolveClassName("java.awt.Object", compiler));
    }

    @Test
    public void testPackages() {
        Assert.assertNull("Unexpectedly found handler for java.awt.JButton", TagManager.getTagHandler(null, "java.awt.JButton", compiler));
        Assert.assertNotNull("Did not find handler for JButton with default namespace of java.awt.*", TagManager.getTagHandler("java.awt.*", "JButton", compiler));
        Assert.assertNull("Unexpectedly found handler for java.awt.*:JButton", TagManager.getTagHandler("java.awt.*", "JButton", true, compiler));
        Assert.assertNotNull("Did not find handler for javax.swing.JButton", TagManager.getTagHandler(null, "javax.swing.JButton", compiler));
        Assert.assertNotNull("Did not find handler for JButton with default namespace of java.swing.*", TagManager.getTagHandler("java.swing.*", "JButton", compiler));
        Assert.assertNotNull("Did not find handler for javax.swing.*:JButton", TagManager.getTagHandler("javax.swing.*", "JButton", true, compiler));
    }

    @Test
    public void testImport() throws Exception {
        Assert.assertNull("Found handler for ActionListener despite no java.awt.event.* import", TagManager.getTagHandler(null, "ActionListener", compiler));

        compiler.addImport("java.awt.event.*");

        Assert.assertNotNull("Did not find ActionListener with java.awt.event.* import", TagManager.getTagHandler(null, "ActionListener", compiler));
    }

    @Test
    public void testAmbiguousImport() throws Exception {
        compiler.addImport("java.sql.*");
        Assert.assertNotNull("Did not find java.sql.Date with only java.sql.* imported", TagManager.getTagHandler(null, "Date", compiler));

        PrintStream oldErr = System.err;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        System.setErr(new PrintStream(buffer));
        compiler.addImport("java.util.*");
        init();
        Assert.assertNull("Still found a handler for Date with an ambiguous import", TagManager.getTagHandler(null, "Date", compiler));
        System.setErr(oldErr);
        Assert.assertTrue("No errors were produced with an ambiguous import", buffer.size() > 0);

        compiler.addImport("java.util.Date");
        Assert.assertNotNull("Did not find java.util.Date with a disambiguating import", TagManager.getTagHandler(null, "Date", compiler));
    }

    @Test
    public void testInnerClass() {
        TagHandler handler = TagManager.getTagHandler(null, "JPopupMenu.Separator", compiler);
        Assert.assertTrue("Unable to resolve tag <JPopupMenu.Separator>", handler instanceof DefaultComponentHandler);
        Assert.assertTrue(((DefaultComponentHandler) handler).getBeanClass().getName().equals(JPopupMenu.Separator.class.getName()));

        handler = TagManager.getTagHandler(null, "javax.swing.JPopupMenu.Separator", compiler);
        Assert.assertTrue("Unable to resolve tag <javax.swing.JPopupMenu.Separator>", handler instanceof DefaultComponentHandler);
        Assert.assertTrue(((DefaultComponentHandler) handler).getBeanClass().getName().equals(JPopupMenu.Separator.class.getName()));
    }

    @Test
    public void testWrongCase() {
        Assert.assertNull("Unexpectedly found handler for 'object'", TagManager.getTagHandler(null, "object", compiler));
        Assert.assertNull("Unexpectedly found handler for 'tagmanagertest'", TagManager.getTagHandler(null, "tagmanagertest", compiler));
    }

    @Test
    public void testAliasing() {
        Assert.assertEquals("ButtonGroup is not aliased to org.nuiton.jaxx.runtime.swing.JAXXButtonGroup", "org.nuiton.jaxx.runtime.swing.JAXXButtonGroup", TagManager.resolveClassName("ButtonGroup", compiler));
        Assert.assertEquals("javax.swing.ButtonGroup is not aliased to org.nuiton.jaxx.runtime.swing.JAXXButtonGroup", "org.nuiton.jaxx.runtime.swing.JAXXButtonGroup", TagManager.resolveClassName("javax.swing.ButtonGroup", compiler));
    }
}
