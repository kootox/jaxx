/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.java;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

/**
 * Created: 3 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class JavaFieldTest {
    /** Logger */
    private static final Log log = LogFactory.getLog(JavaFieldTest.class);

    @Test
    public void testGetFieldOrderScope() {

        EnumSet<JavaField.FieldOrder> allConstants = EnumSet.allOf(JavaField.FieldOrder.class);
        if (log.isDebugEnabled()) {
            for (JavaField.FieldOrder allConstant : allConstants) {
                log.debug("\n" + allConstant.getHeader());
            }
        }

        EnumSet<JavaField.FieldOrder> constants;

        constants = JavaField.getFieldOrderScope(allConstants, Modifier.STATIC);

        Assert.assertEquals(4, constants.size());
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsBean));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsOthers));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsPublicBindings));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsPrivateBindings));

        constants = JavaField.getFieldOrderScope(allConstants, Modifier.PUBLIC);

        Assert.assertEquals(3, constants.size());
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsBean));
//        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsOthers));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsPublicBindings));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.publicFields));

        constants = JavaField.getFieldOrderScope(allConstants, Modifier.PROTECTED);

        Assert.assertEquals(2, constants.size());
//        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsOthers));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.protectedFields));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.internalFields));


        constants = JavaField.getFieldOrderScope(allConstants, Modifier.PRIVATE);
        Assert.assertEquals(3, constants.size());
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsPrivateBindings));
//        Assert.assertTrue(constants.contains(JavaField.FieldOrder.staticsOthers));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.privateFields));
        Assert.assertTrue(constants.contains(JavaField.FieldOrder.internalFields));

    }

    /**
     * To test https://forge.nuiton.org/issues/2154.
     *
     * @since 2.5.1
     */
    @Test
    public void testCompare() {

        String[] names = new String[]{
                "cf0",
                "cf1",
                "cf0O1",
                "cf0A1",
                "ct0O1", "ct0A1",
                "cgett0",
                "cgett1", "cgett0O1",
                "cgett0A1", "cfile", "cgetfile", "t0", "t1",
                "cmt0", "cmt1", "cmt0O1", "cmt0A1", "cmgett0",
                "cmgett1", "cmgett0O1", "cmgett0A1",
                "cMfile", "cMgetfile",
                "Mt0", "Mt1", "cit0", "cit1", "cit0O1", "cit0A1", "cigett0",
                "bindings"
        };
        List<JavaField> theFileds = Lists.newArrayList();
        for (String name : names) {
            JavaField field = new JavaField(Modifier.PROTECTED, "String", name, false);
            theFileds.add(field);
        }
        Collections.sort(theFileds);

    }
}
