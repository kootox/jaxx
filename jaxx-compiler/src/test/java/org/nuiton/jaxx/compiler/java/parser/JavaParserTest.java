package org.nuiton.jaxx.compiler.java.parser;

/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.compiler.reflect.resolvers.ClassDescriptorResolverFromJavaFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.nuiton.jaxx.runtime.util.FileUtil;

/**
 * Created on 4/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.24
 */
public class JavaParserTest {

    public static final List<String> DUMMY_LIST = new ArrayList<>();
    public static final List<String> DUMMY_LIST2 = new ArrayList<>();

    @Test
    public void testParseJava8() throws IOException, ClassNotFoundException{

        File file = new File("").getAbsoluteFile();
        Path rootSourceDirectory = file.toPath().resolve("src").resolve("test").resolve("java");

        File thisClassJavaFile = new File(FileUtil.getFileFromFQN(rootSourceDirectory.toFile(), getClass().getPackage().getName()), "Java8Interface.java");

        Assert.assertTrue("Could not find java file " + thisClassJavaFile, thisClassJavaFile.exists());

        ClassDescriptorResolverFromJavaFile resolver = new ClassDescriptorResolverFromJavaFile();

        ClassDescriptor descriptor = resolver.resolvDescriptor("yo", thisClassJavaFile.toURI().toURL());
        Assert.assertNotNull(descriptor);
        MethodDescriptor[] methodDescriptors = descriptor.getMethodDescriptors();
        Assert.assertEquals(2, methodDescriptors.length);
        Assert.assertEquals("defaultMethod", methodDescriptors[0].getName());
        Assert.assertEquals("normalMethod", methodDescriptors[1].getName());
    }

    @Test
    public void testParseClassButNotMethodBody() throws IOException, ClassNotFoundException, NoSuchMethodException {

        File file = new File("").getAbsoluteFile();
        Path rootSourceDirectory = file.toPath().resolve("src").resolve("test").resolve("java");

        File thisClassJavaFile = new File(FileUtil.getFileFromFQN(rootSourceDirectory.toFile(), getClass().getPackage().getName()), getClass().getSimpleName() + ".java");

        Assert.assertTrue("Could not find java file " + thisClassJavaFile, thisClassJavaFile.exists());

        ClassDescriptorResolverFromJavaFile resolver = new ClassDescriptorResolverFromJavaFile();

        ClassDescriptor yo = resolver.resolvDescriptor("yo", thisClassJavaFile.toURI().toURL());
        MethodDescriptor[] methodDescriptors = yo.getMethodDescriptors();
        Assert.assertTrue(methodDescriptors.length > 3);

        {
            MethodDescriptor method = yo.getMethodDescriptor("testParseClassButNotMethodBody");
            Assert.assertNotNull(method);
        }

        {
            MethodDescriptor method = yo.getMethodDescriptor("methodWithDiamondInside");
            Assert.assertNotNull(method);
        }

        {
            MethodDescriptor method = yo.getMethodDescriptor("methodWithTryResourceInside");
            Assert.assertNotNull(method);
        }

    }


    @Test
    public void testParseClassAndMethodBody() throws IOException, ClassNotFoundException, NoSuchMethodException {

        File file = new File("").getAbsoluteFile();
        Path rootSourceDirectory = file.toPath().resolve("src").resolve("test").resolve("java");

        File thisClassJavaFile = new File(FileUtil.getFileFromFQN(rootSourceDirectory.toFile(), getClass().getPackage().getName()), getClass().getSimpleName() + ".java");

        Assert.assertTrue("Could not find java file " + thisClassJavaFile, thisClassJavaFile.exists());

        ClassDescriptorResolverFromJavaFile resolver = new ClassDescriptorResolverFromJavaFile(true);

        try {
            resolver.resolvDescriptor("yo", thisClassJavaFile.toURI().toURL());
            Assert.fail("Can't compile body method (there is some jdk7 syntaxes inside them)...");
        } catch (RuntimeException e) {
            Assert.assertTrue(e.getCause() instanceof CompilerException);
        }

    }

    public void methodWithDiamondInside() {

        System.out.println(new ArrayList<>());
        System.out.println(DUMMY_LIST);
        System.out.println(DUMMY_LIST2);

    }

    public void methodWithTryResourceInside() {

        try (BufferedReader reader = Files.newBufferedReader(new File("").toPath(), Charset.forName("UTF-8"))) {

            System.out.println(reader);
        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }


    }

}
