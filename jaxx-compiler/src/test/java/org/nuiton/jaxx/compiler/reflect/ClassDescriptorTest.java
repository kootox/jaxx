/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.reflect;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ClassDescriptorTest {

    @BeforeClass
    public static void before() {
        ClassDescriptorHelper.setShowLoading(true);
    }

    @AfterClass
    public static void after() {
        ClassDescriptorHelper.setShowLoading(true);
    }

    /*@Test
    public void testGetClassDescriptor() throws Exception {
    ClassDescriptorHelper.getClassDescriptor("jaxx.runtime.swing.navigation.tree.NavigationTreeModel.NavigationTreeNode");
    }*/
    @Test
    public void testBuiltInClassName() throws ClassNotFoundException, NoSuchMethodException {
        ClassDescriptor object = ClassDescriptorHelper.getClassDescriptor("java.lang.Object");
        MethodDescriptor toString = object.getMethodDescriptor("toString");
        assertEquals(toString.getName(), "toString");
        assertEquals(toString.getParameterTypes().length, 0);

        MethodDescriptor equals = object.getMethodDescriptor("equals", object);
        assertEquals(equals.getName(), "equals");
        assertEquals(equals.getParameterTypes().length, 1);
        assertEquals(equals.getParameterTypes()[0], object);
    }

    @Test
    public void testBuiltInClass() throws ClassNotFoundException, NoSuchMethodException {
        ClassDescriptor object1 = ClassDescriptorHelper.getClassDescriptor("java.lang.Object");
        ClassDescriptor object2 = ClassDescriptorHelper.getClassDescriptor(Object.class);
        assertEquals(object1, object2);
    }

    @Test
    public void testUserClassName() throws ClassNotFoundException, NoSuchMethodException {
        ClassDescriptor me = ClassDescriptorHelper.getClassDescriptor(ClassDescriptorTest.class.getName(), getClass().getClassLoader());
        MethodDescriptor testUserClassName = me.getMethodDescriptor("testUserClassName");
        assertEquals(testUserClassName.getName(), "testUserClassName");
        assertEquals(testUserClassName.getParameterTypes().length, 0);
    }

    @Test(expected = ClassNotFoundException.class)
    public void testWrongCase() throws ClassNotFoundException {
        ClassDescriptorHelper.getClassDescriptor("jaxx.junit.classdescriptortest", getClass().getClassLoader());
    }

    @Test
    public void testArrays() throws ClassNotFoundException {
        ClassDescriptor intArray = ClassDescriptorHelper.getClassDescriptor(int[].class);
        assertNotNull(intArray);
        ClassDescriptor objectArray = ClassDescriptorHelper.getClassDescriptor(Object[].class);
        assertNotNull(objectArray);
    }

    @Test
    public void testConstructorFromClass() throws ClassNotFoundException, MalformedURLException {
        String className = MyClass.class.getName();
        ClassDescriptor descriptor = ClassDescriptorHelper.getClassDescriptor(className);
        assertNotNull(descriptor);
        ClassDescriptorHelper.ResolverType resolverType = descriptor.getResolverType();

        //FIXME-tchemit find out why ? (https://forge.nuiton.org/issues/2203)
        // using jdk 7 VM Server it found a FILE instead of a CLASS ? Wonder why?
        // Need to find out why
//        assertEquals(ClassDescriptorHelper.ResolverType.JAVA_CLASS, resolverType);
        assertTrue(Arrays.asList(ClassDescriptorHelper.ResolverType.JAVA_CLASS, ClassDescriptorHelper.ResolverType.JAVA_FILE).contains(resolverType));
        MethodDescriptor[] constructorDescriptors = descriptor.getConstructorDescriptors();
        assertNotNull(constructorDescriptors);
        assertEquals(2, constructorDescriptors.length);
    }

    @Test
    public void testConstructorFromJavaFile() throws ClassNotFoundException, MalformedURLException {
        String className = MyClass.class.getName();
        ClassLoader classLoader = getClass().getClassLoader();


        File testSourceDir = new File(getBasedir(), "src" + File.separator + "test" + File.separator + "java");

        ClassLoader myClassLoader = new URLClassLoader(new URL[]{testSourceDir.toURI().toURL()}, classLoader);

        URL javaFileUrl = ClassDescriptorHelper.getURL(myClassLoader, className, "java");
        assertNotNull(javaFileUrl);

        ClassDescriptor descriptorFromFile = ClassDescriptorHelper.getClassDescriptor0(
                ClassDescriptorHelper.ResolverType.JAVA_FILE,
                className,
                javaFileUrl,
                myClassLoader
        );
        assertNotNull(descriptorFromFile);
        MethodDescriptor[] constructorDescriptorsfromFile = descriptorFromFile.getConstructorDescriptors();
        assertNotNull(constructorDescriptorsfromFile);
        assertEquals(2, constructorDescriptorsfromFile.length);
    }


    static File basedir;

    public static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);

        }
        return basedir;
    }

}
