/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.jaxx.compiler.java.parser.JavaParser;
import org.nuiton.jaxx.compiler.java.parser.SimpleNode;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class JavaParserUtilTest {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(JavaParserUtilTest.class);

    final Map<SimpleNode, List<SimpleNode>> store = new LinkedHashMap<>();
    final Map<SimpleNode, List<SimpleNode>> casts = new LinkedHashMap<>();
    final List<SimpleNode> literals = new ArrayList<>();
    Set<String> requirements;
    Iterator<SimpleNode> simpleNodeIterator;
    Iterator<String> requirementsIterator;
    SimpleNode node;
    String source;

    @Test
    public void testGetExpressionsWithLiterals() throws Exception {

        parseSourceAndGetExpressions("1", 1, 1);
        assertNextNode(source, 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("true", 1, 1);
        assertNextNode(source, 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("\"1\"", 1, 1);
        assertNextNode(source, 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("\"1\" + \"2\"", 2, 2);
        assertNextNode("\"1\"", 0);
        assertLiteralNode(0);
        assertNextNode("\"2\"", 0);
        assertLiteralNode(1);

        parseSourceAndGetExpressions("1.1 + 2", 2, 2);
        assertNextNode("1.1", 0);
        assertLiteralNode(0);
        assertNextNode("2", 0);
        assertLiteralNode(1);
    }

    @Test
    public void testGetExpressionsWithNoLiterals() throws Exception {

        parseSourceAndGetExpressions("a", 0, 1);
        assertNextNode(source, 0);

        parseSourceAndGetExpressions("a.getText()", 0, 1);
        assertNextNode(source, 0);

        parseSourceAndGetExpressions("a.getText().getLength()", 0, 1);
        assertNextNode(source, 0);

        parseSourceAndGetExpressions("getText()", 0, 1);
        assertNextNode(source, 0);

        parseSourceAndGetExpressions("getText(a)", 0, 2);
        assertNextNode(source, 1);
        assertNextNode("a", 0);

        parseSourceAndGetExpressions("getText(a, b, c)", 0, 4);
        assertNextNode(source, 3);
        assertNextNode("a", 0);
        assertNextNode("b", 0);
        assertNextNode("c", 0);

        parseSourceAndGetExpressions("getText(a + b, c)", 0, 4);
        assertNextNode(source, 3);
        assertNextNode("a", 0);
        assertNextNode("b", 0);
        assertNextNode("c", 0);

        parseSourceAndGetExpressions("getText(a + b + c)", 0, 4);
        assertNextNode(source, 3);
        assertNextNode("a", 0);
        assertNextNode("b", 0);
        assertNextNode("c", 0);

        parseSourceAndGetExpressions("(Hum)a", 0, 1);
        assertNextNode("a", 0);

        parseSourceAndGetExpressions("(Hum)  a", 0, 1);
        assertNextNode("a", 0);

        parseSourceAndGetExpressions("((Hum)  a)", 0, 2);
        assertNextNode(source, 1);
        assertNextNode("a", 0);

        parseSourceAndGetExpressions("((Hum)  a).getText()", 0, 2);
        assertNextNode(source, 1);
        assertNextNode("a", 0);
    }

    @Test
    public void testGetExpressions() throws Exception {

        parseSourceAndGetExpressions("getText(\"a\")", 1, 2);
        assertNextNode(source, 1);
        assertNextNode("\"a\"", 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("SwingUtil.getText(\"a\")", 1, 2);
        assertNextNode(source, 1);
        assertNextNode("\"a\"", 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("getText2() && getText(\"a.b\")", 1, 3);
        assertNextNode("getText2()", 0);
        assertNextNode("getText(\"a.b\")", 1);
        assertNextNode("\"a.b\"", 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("SwingUtil.getText2() && getText(\"a.b\")", 1, 3);
        assertNextNode("SwingUtil.getText2()", 0);
        assertNextNode("getText(\"a.b\")", 1);
        assertNextNode("\"a.b\"", 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("SwingUtil.getText2() && SwingUtil2.getText(\"a.b\")", 1, 3);
        assertNextNode("SwingUtil.getText2()", 0);
        assertNextNode("SwingUtil2.getText(\"a.b\")", 1);
        assertNextNode("\"a.b\"", 0);
        assertLiteralNode(0);

        parseSourceAndGetExpressions("SwingUtil.get().getText2() && SwingUtil2.getText(\"a.b\")", 1, 3);
        assertNextNode("SwingUtil.get().getText2()", 0);
        assertNextNode("SwingUtil2.getText(\"a.b\")", 1);
        assertNextNode("\"a.b\"", 0);
        assertLiteralNode(0);
    }

    @Test
    public void testGetMethodInvocationParameters() throws Exception {
        getMethodInvocationParameters("a", null);
        getMethodInvocationParameters("a(", null);
        getMethodInvocationParameters("a(   ", null);
        getMethodInvocationParameters("a)", null);
        getMethodInvocationParameters("a   )", null);
        getMethodInvocationParameters("a()", "");
        getMethodInvocationParameters("a( )", "");
        getMethodInvocationParameters("a( yo )", "yo");
        getMethodInvocationParameters("SwingUtil.a( yo )", "yo");
        getMethodInvocationParameters("SwingUtil.a( yo, ya )", "yo, ya");

    }

    @Test
    public void testGetRequirementsWithLiterals() throws Exception {

        parseSourceAndGetRequirements("1");

        parseSourceAndGetRequirements("true");

        parseSourceAndGetRequirements("\"1\"");

        parseSourceAndGetRequirements("\"1\" + \"2\"");

        parseSourceAndGetRequirements("1.1 + 2");
    }


    @Test
    public void testGetRequirementsWithNoLiterals() throws Exception {

        parseSourceAndGetRequirements("a");

        parseSourceAndGetRequirements("a.getText()", "a");

        parseSourceAndGetRequirements("a.getText().getLength()", "a", "a.getText()");

        parseSourceAndGetRequirements("getText()");

        parseSourceAndGetRequirements("getText(a)");

        parseSourceAndGetRequirements("getText(a, b, c)");

        parseSourceAndGetRequirements("getText(a + b, c)");

        parseSourceAndGetRequirements("getText(a + b + c)");

        parseSourceAndGetRequirements("(Hum)a");

        parseSourceAndGetRequirements("((Hum)a)");

        parseSourceAndGetRequirements("((Hum)a).getText()", "a");

        parseSourceAndGetRequirements("((Hum)a).getB().getText()", "a", "a.getB()");

        parseSourceAndGetRequirements("((Hum)a.getB()).getText()", "a", "a.getB()");
    }

    @Test
    public void testGetRequirements() throws Exception {

        parseSourceAndGetRequirements("getText(\"a\")");

        parseSourceAndGetRequirements("SwingUtil.getText(\"a\")", "SwingUtil");

        parseSourceAndGetRequirements("getText2() && getText(\"a.b\")");

        parseSourceAndGetRequirements("SwingUtil.getText2() && getText(\"a.b\")", "SwingUtil");

        parseSourceAndGetRequirements("SwingUtil.getText2() && SwingUtil2.getText(\"a.b\")", "SwingUtil", "SwingUtil2");

        parseSourceAndGetRequirements("SwingUtil.get().getText2() && SwingUtil2.getText(\"a.b\")", "SwingUtil2", "SwingUtil", "SwingUtil.get()");

    }

    protected void parseSourceAndGetExpressions(String source, int expectedNbLiterals, int expectedNbExpressions) {

        // clean stores
        literals.clear();

        parseSource(source);

        Assert.assertEquals(expectedNbLiterals, literals.size());
        Assert.assertEquals(expectedNbExpressions, store.size());

        simpleNodeIterator = store.keySet().iterator();
    }

    protected void parseSourceAndGetRequirements(String source, String... expected) {

        parseSource(source);

        requirements = JavaParserUtil.getRequired(store.keySet(), casts);
        Assert.assertEquals(expected.length, requirements == null ? 0 : requirements.size());
        if (expected.length > 0) {

            requirementsIterator = requirements.iterator();
            for (String s : expected) {
                Assert.assertTrue(requirementsIterator.hasNext());
                Assert.assertEquals(s, requirementsIterator.next());
            }
        }
    }

    protected void getMethodInvocationParameters(String code, String expected) {
        String invocationParameters = JavaParserUtil.getMethodInvocationParameters(code);
        Assert.assertEquals(expected, invocationParameters);
    }

    protected void assertNextNode(String expectedText, int nbDep) {

        Assert.assertTrue(simpleNodeIterator.hasNext());
        node = simpleNodeIterator.next();
        Assert.assertNotNull(node);
        Assert.assertEquals(expectedText, node.getText().trim());
        Assert.assertEquals(nbDep, store.get(node).size());
    }

    private void parseSource(String source) {

        this.source = source;
        // clean stores
        store.clear();
        literals.clear();
        casts.clear();

        if (log.isDebugEnabled()) {
            log.debug(source);
        }

        JavaParser p;
        SimpleNode node;
        p = new JavaParser(new StringReader(source));
        while (!p.Line()) {
            node = p.popNode();
            JavaParserUtil.getExpressions(node, store, literals, casts);
        }
    }

    protected void assertLiteralNode(int nbDep) {
        Assert.assertNotNull(node);
        Assert.assertTrue(nbDep <= literals.size());
        Assert.assertEquals(node, literals.get(nbDep));
    }

}
