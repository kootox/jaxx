/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.types;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created: 29 nov. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class TypeManagerTest {

    @Test
    public void testHexValue() {

        String actual;
        String expected;

        actual = TypeManager.convertVariableNameToConstantName("azerty");
        expected = "AZERTY";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("azertyQwerty");
        expected = "AZERTY_QWERTY";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("1azertyQwerty");
        expected = "1AZERTY_QWERTY";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("$1azertyQwerty");
        expected = "$1AZERTY_QWERTY";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("binding_$hum");
        expected = "BINDING_$HUM";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("BINding_$hum");
        expected = "BINDING_$HUM";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("BINding_$Hum");
        expected = "BINDING_$HUM";
        Assert.assertEquals(expected, actual);

        actual = TypeManager.convertVariableNameToConstantName("!BINding_$Hum");
        expected = "NOT_BINDING_$HUM";
        Assert.assertEquals(expected, actual);
    }

}
