/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrimitiveConverterTest {

    PrimitiveConverter converter;


    @Before
    public void setUp() {
        converter = new PrimitiveConverter();
    }

    @Test
    public void testBoolean() {
        Boolean value = (Boolean) converter.convertFromString("true", Boolean.class);
        Assert.assertTrue(value);

        value = (Boolean) converter.convertFromString("false", Boolean.class);
        Assert.assertFalse(value);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testBoolean2() {
        converter.convertFromString("yes", Boolean.class);
    }

    @Test
    public void testByte() {
        Byte value = (Byte) converter.convertFromString(String.valueOf(Byte.MAX_VALUE), Byte.class);
        Assert.assertEquals(value.byteValue(), Byte.MAX_VALUE);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testByte2() {

        converter.convertFromString(String.valueOf(1 + Byte.MAX_VALUE), Byte.class);
    }

    @Test
    public void testShort() {
        Short value = (Short) converter.convertFromString(String.valueOf(Short.MAX_VALUE), Short.class);
        Assert.assertEquals(value.shortValue(), Short.MAX_VALUE);


    }

    @Test(expected = IllegalArgumentException.class)
    public void testShort2() {

        converter.convertFromString(String.valueOf(1 + Short.MAX_VALUE), Short.class);
    }

    @Test
    public void testInteger() {
        Integer value = (Integer) converter.convertFromString(String.valueOf(Integer.MAX_VALUE), Integer.class);
        Assert.assertEquals(value.intValue(), Integer.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInteger2() {

        converter.convertFromString(String.valueOf(1L + Integer.MAX_VALUE), Integer.class);
    }

    @Test
    public void testLong() {
        Long value = (Long) converter.convertFromString(String.valueOf(Long.MAX_VALUE), Long.class);
        Assert.assertEquals(value.longValue(), Long.MAX_VALUE);
    }

    @Test
    public void testFloat() {
        Float value = (Float) converter.convertFromString("3.1415", Float.class);
        Assert.assertTrue(value == 3.1415f);
    }

    @Test
    public void testDouble() {
        Double value = (Double) converter.convertFromString("3.1415", Double.class);
        Assert.assertTrue(value == 3.1415);
    }

    @Test
    public void testCharacter() {
        Character value = (Character) converter.convertFromString("A", Character.class);
        Assert.assertEquals(value.charValue(), 'A');
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCharacter2() {

        converter.convertFromString("12", Character.class);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCharacter3() {
        converter.convertFromString("", Character.class);
    }

    @Test
    public void testString() {
        String value = (String) converter.convertFromString("Test", String.class);
        Assert.assertEquals(value, "Test");
    }
}
