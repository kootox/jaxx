/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import org.junit.Assert;
import org.junit.Test;

import java.beans.Introspector;

/** @author Tony Chemit - dev@tchemit.fr */
public class BeanIntoUtilTest {

    @Test
    public void testExtraBeanInfoPath() {
        BeanInfoUtil.reset();
        String[] searchPath0 = Introspector.getBeanInfoSearchPath();

        BeanInfoUtil.addJaxxBeanInfoPath("org.nuiton.jaxx.runtime.swing");

        String[] searchPath = Introspector.getBeanInfoSearchPath();
        Assert.assertEquals(searchPath0.length + 1, searchPath.length);

        BeanInfoUtil.reset();
        Assert.assertEquals(searchPath0.length, Introspector.getBeanInfoSearchPath().length);

        String packageName = getClass().getPackage().getName() + ".dummy";
        BeanInfoUtil.addJaxxBeanInfoPath("jaxx.beaninfos", packageName);

        searchPath = Introspector.getBeanInfoSearchPath();
        Assert.assertEquals(searchPath0.length + 2, searchPath.length);
        Assert.assertEquals(packageName, searchPath[searchPath.length - 1]);

        BeanInfoUtil.reset();
        Assert.assertEquals(searchPath0.length, Introspector.getBeanInfoSearchPath().length);
    }
}
