/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding.writers;

import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.binding.DataBinding;
import org.nuiton.jaxx.compiler.binding.DataListener;
import org.nuiton.jaxx.compiler.finalizers.DefaultFinalizer;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;
import org.nuiton.jaxx.compiler.java.JavaMethod;
import org.nuiton.jaxx.runtime.binding.DefaultJAXXBinding;

import java.util.List;

import static java.lang.reflect.Modifier.PUBLIC;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class DefaultJAXXBindingWriter extends AbstractJAXXBindingWriter<DefaultJAXXBinding> {

    public DefaultJAXXBindingWriter() {
        super(DefaultJAXXBinding.class);
    }

    @Override
    public boolean accept(DataBinding binding) {
        return true;
    }

    @Override
    public void write(DataBinding binding,
                      JavaFileGenerator generator,
                      StringBuilder buffer) {
        DataListener[] trackers = binding.getTrackers();
        String eol = JAXXCompiler.getLineSeparator();

        StringBuilder addBuffer = new StringBuilder();
        StringBuilder removeBuffer = new StringBuilder();

        for (DataListener tracker : trackers) {
            boolean needTest = tracker.getObjectCode() != null;
            if (needTest) {
                addBuffer.append("if (");
                addBuffer.append(tracker.getObjectCode());
                addBuffer.append(" != null) {");
                addBuffer.append(eol);

                removeBuffer.append("if (");
                removeBuffer.append(tracker.getObjectCode());
                removeBuffer.append(" != null) {");
                removeBuffer.append(eol);
            }
            int indentLevel = needTest ? 4 : 0;
            addBuffer.append(JavaFileGenerator.indent(
                    tracker.getAddListenerCode(), indentLevel, false, eol));
            removeBuffer.append(JavaFileGenerator.indent(
                    tracker.getRemoveListenerCode(), indentLevel, false, eol));
            if (needTest) {
                addBuffer.append(eol).append("}");
                removeBuffer.append(eol).append("}");
            }
            addBuffer.append(eol);
            removeBuffer.append(eol);
        }

        List<JavaMethod> bMethods = binding.getMethods();
        bMethods.add(0, JavaElementFactory.newMethod(
                PUBLIC,
                JAXXCompilerFinalizer.TYPE_VOID,
                DefaultFinalizer.METHOD_NAME_REMOVE_DATA_BINDING,
                removeBuffer.toString(),
                true)
        );
        bMethods.add(0, JavaElementFactory.newMethod(
                PUBLIC,
                JAXXCompilerFinalizer.TYPE_VOID,
                DefaultFinalizer.METHOD_NAME_PROCESS_DATA_BINDING,
                binding.getProcessDataBinding(),
                true)
        );
        bMethods.add(0, JavaElementFactory.newMethod(
                PUBLIC,
                JAXXCompilerFinalizer.TYPE_VOID,
                DefaultFinalizer.METHOD_NAME_APPLY_DATA_BINDING,
                addBuffer.toString(),
                true)
        );

        writeInvocationMethod(binding, trackers, generator, buffer, bMethods);
    }

    @Override
    protected String getConstructorParams(DataBinding binding,
                                          DataListener[] trackers) {

        String params = "this, " + binding.getConstantId() + ", true";
        if (trackers.length > 1) {
            //FIXME tchemit-2011-04-21 Must improve this : only need a complex binding
            // when chaining properties : example getA().getB() but not getA() || getB()
            // with a complex binding, we will need to reload de binding after each fire...
            params += ", true";

        }
        return params;
    }
}
