/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Represents a file to be treated by the {@link JAXXCompiler}.
 * <p>
 * It contains informations about jaxx file, ident css, class name,...
 * <p>
 * <b>Note:</b> This class will be more used in next version (will have the
 * compiler it-self, symbols table,...).
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class JAXXCompilerFile {

    /** Logger */
    private static final Log log = LogFactory.getLog(JAXXCompilerFile.class);

    /** root directory of the source roots. */
    protected final File basedir;

    /** relative path from {@link #basedir} to {@link #jaxxFile}. */
    private final String relativePath;

    /** location of the jaxx file */
    protected File jaxxFile;

    /** location of the ident css file */
    protected File cssFile;

    /** full qualified name of the jaxx file class */
    protected String className;

    /** compiler associated to the file */
    protected JAXXCompiler compiler;

    /**
     * Css file extension.
     *
     * @see CompilerConfiguration#getCssExtension()
     */
    protected final String cssExtension;

    public JAXXCompilerFile(File basedir, File jaxxFile, String cssExtension) {
        this.basedir = basedir;
        this.jaxxFile = jaxxFile;
        this.cssExtension = cssExtension;
        String absolutePath = jaxxFile.getAbsolutePath();
        String baseAbsolutePath = basedir.getAbsolutePath();
        if (!absolutePath.startsWith(baseAbsolutePath)) {
            throw new IllegalStateException(
                    "Jaxx file " + jaxxFile + " is not in basedir " + basedir);
        }

        relativePath = absolutePath.substring(baseAbsolutePath.length() + 1);
        if (log.isDebugEnabled()) {
            log.debug("relativePath = " + relativePath);
        }
    }

    public JAXXCompilerFile(File jaxxFile, String className, String cssExtension) {

        this.jaxxFile = jaxxFile;
        this.className = className;
        this.cssExtension = cssExtension;

        String extension = FilenameUtils.getExtension(jaxxFile.getName());
        String[] paths = className.split("\\.");
        File basedir = jaxxFile;
        for (int i = paths.length - 1; i > -1; i--) {
            if (basedir == null) {
                throw new IllegalStateException("Could not find base dir for " + jaxxFile + " according to fqn " + className);
            }

            String path = paths[i];
            if (basedir.equals(jaxxFile)) {
                // first in loop
                path += "." + extension;
            }

            // check path = base filename
            if (!path.equals(basedir.getName())) {
                throw new IllegalStateException("Should have found directory " + path + ", but was " + basedir.getName());
            }

            basedir = basedir.getParentFile();
        }

        if (log.isDebugEnabled()) {
            log.debug("basedir = " + basedir);
        }

        // must guess the base directory and relative path

        String relativePath = jaxxFile.getAbsolutePath().substring(basedir.getAbsolutePath().length() + 1);

        if (log.isDebugEnabled()) {
            log.debug("relative path = " + relativePath);
        }

        this.basedir = basedir;
        this.relativePath = relativePath;
    }

    public File getBasedir() {
        return basedir;
    }

    public JAXXCompiler getCompiler() {
        return compiler;
    }

    public File getJaxxFile() {
        if (jaxxFile == null) {
            jaxxFile = new File(basedir, relativePath);
        }
        return jaxxFile;
    }

    public URL getJAXXFileURL() {
        File file = getJaxxFile();
        try {
            return file.toURI().toURL();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Url of the jaxx file is malformed... " + file);
        }
    }

    public File getCssFile() {
        if (cssFile == null) {
            File file = getJaxxFile();
            String extension = FilenameUtils.getExtension(file.getName());
            String fileName = file.getName();
            int length = fileName.length() - extension.length();
            String identCssFilename = fileName.substring(0, length) + cssExtension;
            cssFile = new File(file.getParentFile(), identCssFilename);
        }
        return cssFile;
    }

    public String getClassName() {
        if (className == null) {

            className = relativePath.substring(0, relativePath.lastIndexOf("."));
            className = className.replace(File.separatorChar, '.');
            className = className.replace('/', '.');
            className = className.replace('\\', '.');
            className = className.replace(':', '.');

        }
        return className;
    }

    public void clear() {
        if (compiler != null) {
            compiler.clear();
        }
    }

    public void setCompiler(JAXXCompiler compiler) {
        this.compiler = compiler;
    }
}
