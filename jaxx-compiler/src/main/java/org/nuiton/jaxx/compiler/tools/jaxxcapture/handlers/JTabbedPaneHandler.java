/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tools.jaxxcapture.handlers;

import org.nuiton.jaxx.compiler.tools.jaxxcapture.ContextNode;
import org.nuiton.jaxx.compiler.tools.jaxxcapture.JAXXCapture;
import org.nuiton.jaxx.compiler.tools.jaxxcapture.MethodNode;
import org.w3c.dom.Element;

import java.util.Arrays;
import java.util.Stack;

public class JTabbedPaneHandler extends ObjectHandler {

    @Override
    protected void evaluateMethod(Element tag, Stack<ContextNode> context, JAXXCapture capture) {
        String methodName = tag.getAttribute("method");
        if (methodName.equals("addTab")) {
            MethodNode addTab = new MethodNode(methodName);
            context.push(addTab);
            processChildren(tag, context, capture);
            context.pop();
            System.err.println(Arrays.asList(addTab.getArguments()));
        } else {
            super.evaluateMethod(tag, context, capture);
        }
    }
}
