/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompiledObjectDecorator;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.I18nHelper;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.beans.JAXXBeanInfo;
import org.nuiton.jaxx.compiler.beans.JAXXEventSetDescriptor;
import org.nuiton.jaxx.compiler.beans.JAXXIntrospector;
import org.nuiton.jaxx.compiler.beans.JAXXPropertyDescriptor;
import org.nuiton.jaxx.compiler.css.StylesheetHelper;
import org.nuiton.jaxx.compiler.java.parser.ParseException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.ComponentDescriptor;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.JAXXObjectDescriptor;
import org.nuiton.jaxx.runtime.bean.BeanTypeAware;
import org.nuiton.jaxx.runtime.css.Stylesheet;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Default handler for class tags.  Class tags are tags which represent instances of Java classes,
 * such as <code>&lt;JButton label='Close'/&gt;</code>.  <code>DefaultObjectHandler</code>
 * provides support for attributes and events which adhere to JavaBeans naming conventions as
 * well as basic JAXX features like the <code>id</code> attribute and data binding by means of
 * curly braces.
 * <p>
 * Throughout this class, the word "member" refers to the name of a field or method (e.g.
 * <code>"getDocument"</code>) and the word "property" refers to the JavaBeans-style simple
 * name of a property (e.g. <code>"document"</code>).
 */
public class DefaultObjectHandler implements TagHandler {

    public static final String HANDLER_ATTRIBUTE = "handler";
    public static final String ABSTRACT_ATTRIBUTE = "abstract";
    public static final String CONSTRAINTS_ATTRIBUTE = "constraints";
    public static final String CONSTRUCTOR_PARAMS_ATTRIBUTE = "constructorParams";
    public static final String DECORATOR_ATTRIBUTE = "decorator";
    public static final String DISPLAYED_MNEMONIC_ATTRIBUTE = "displayedMnemonic";
    public static final String DISPLAYED_MNEMONIC_INDEX_ATTRIBUTE = "displayedMnemonicIndex";
    public static final String GENERIC_TYPE_ATTRIBUTE = "genericType";
    public static final String ID_ATTRIBUTE = "id";
    public static final String IMPLEMENTS_ATTRIBUTE = "implements";
    public static final String INITIALIZER_ATTRIBUTE = "initializer";
    public static final String JAVA_BEAN_ATTRIBUTE = "javaBean";
    public static final String LAYOUT_ATTRIBUTE = "layout";
    public static final String MNEMONIC_ATTRIBUTE = "mnemonic";
    public static final String STYLE_CLASS_ATTRIBUTE = "styleClass";
    public static final String SUPER_GENERIC_TYPE_ATTRIBUTE = "superGenericType";
    public static final String I18N_FORMAT_ATTRIBUTE = "i18nFormat";
    public static final String COMPUTE_I18N_ATTRIBUTE = "computeI18n";
    public static final String GRID_LAYOUT_PREFIX = GridLayout.class.getSimpleName() + "(";
    public static final String BORDER_LAYOUT_PREFIX = BorderLayout.class.getSimpleName() + "(";
    /** Maps XML tags to the CompiledObjects created from them. */
    protected static final Map<Element, CompiledObject> objectMap = new WeakHashMap<>();
    /** Logger. */
    private static final Log log = LogFactory.getLog(DefaultObjectHandler.class);
    /** The class that this handler provides support for. */
    private final ClassDescriptor beanClass;
    /** The JAXXBeanInfo for the beanClass. */
    protected JAXXBeanInfo jaxxBeanInfo;
    /** Maps property names to their respective JAXXPropertyDescriptors. */
    private Map<String, JAXXPropertyDescriptor> properties;
    /** Maps event names to their respective JAXXEventSetDescriptors. */
    private Map<String, JAXXEventSetDescriptor> events;
    /** Maps property names to their respective ProxyEventInfos. */
    private Map<String, ProxyEventInfo> eventInfos;

    /**
     * Creates a new <code>DefaultObjectHandler</code> which provides support for the specified class.  The
     * class is not actually introspected until the {@link #compileFirstPass} method is invoked.
     *
     * @param beanClass the class which this handler supports
     */
    public DefaultObjectHandler(ClassDescriptor beanClass) {
        this.beanClass = beanClass;
    }

    /**
     * Returns the <code>JAXXBeanInfo</code> for the specified class.
     *
     * @param beanClass the bean class for which to retrieve <code>JAXXBeanInfo</code>
     * @return the class' <code>JAXXBeanInfo</code>
     * @throws IntrospectionException if any pb
     */
    public static JAXXBeanInfo getJAXXBeanInfo(ClassDescriptor beanClass) {
        return JAXXIntrospector.getJAXXBeanInfo(beanClass);
    }

    public static ClassDescriptor getEventClass(ClassDescriptor listenerClass) {
        return listenerClass.getMethodDescriptors()[0].getParameterTypes()[0];
    }

    public ProxyEventInfo getEventInfo(String memberName) {
        return eventInfos != null ? eventInfos.get(memberName) : null;
    }

    /**
     * Performs introspection on the beanClass and stores the results.
     *
     * @throws IntrospectionException if any pb
     */
    protected void init() throws IntrospectionException {
        if (jaxxBeanInfo == null) {
            // perform introspection & cache the results
            jaxxBeanInfo = getJAXXBeanInfo(beanClass);

            JAXXPropertyDescriptor[] propertiesArray =
                    jaxxBeanInfo.getJAXXPropertyDescriptors();
            properties = new HashMap<>();
            for (int i = propertiesArray.length - 1; i >= 0; i--) {
                properties.put(propertiesArray[i].getName(), propertiesArray[i]);
            }

            JAXXEventSetDescriptor[] eventsArray =
                    jaxxBeanInfo.getJAXXEventSetDescriptors();
            events = new HashMap<>();
            for (int i = eventsArray.length - 1; i >= 0; i--) {
                MethodDescriptor[] methods = eventsArray[i].getListenerMethods();
                for (MethodDescriptor method : methods) {
                    events.put(method.getName(), eventsArray[i]);
                }
            }

            configureProxyEventInfo();
        }
    }

    /** @return the class which this <code>DefaultObjectHandler</code> supports. */
    public ClassDescriptor getBeanClass() {
        return beanClass;
    }

    /**
     * @return the <code>JAXXBeanInfo</code> for the class which this <code>DefaultObjectHandler</code>
     * supports.
     */
    public JAXXBeanInfo getJAXXBeanInfo() {
        try {
            init();
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        return jaxxBeanInfo;
    }

    /**
     * Returns the type of the named property.  This is the return type of the property's <code>get</code> method;
     * for instance <code>JLabel</code>'s <code>text</code> property is a <code>String</code>.
     *
     * @param object       the object being compiled
     * @param propertyName the simple JavaBeans-style name of the property
     * @param compiler     the current <code>JAXXCompiler</code>
     * @return the property's type
     * @throws CompilerException if the type cannot be determined
     */
    public ClassDescriptor getPropertyType(CompiledObject object,
                                           String propertyName,
                                           JAXXCompiler compiler) {
        safeInit();

        JAXXPropertyDescriptor property = properties.get(propertyName);
        if (property != null) {
            return property.getPropertyType();
        }
        throw new UnsupportedAttributeException(
                "property '" + propertyName + "' not found in " + object);
    }

    /**
     * @param name name of the property
     * @return <code>true</code> if the named member is <i>bound</i> (fires <code>PropertyChangeEvent</code>
     * when modified).  Members are either fields (represented by the simple name of the field) or <code>get/is</code>
     * methods (represented by the simple name of the method, <b>not</b> the simplified JavaBeans-style name).
     * Methods which are not actually bound in their native class, but for which proxy events have been
     * configured (such as <code>JTextField.getText</code>, return <code>true</code>.
     * @throws UnsupportedAttributeException if attribute is not supported
     */
    public boolean isMemberBound(String name) throws UnsupportedAttributeException {
        safeInit();

        if (eventInfos != null && eventInfos.containsKey(name)) {
            return true;
        }

        if (name.equals("getClass")) {
            return false;
        }

        String propertyName = null;
        if (name.startsWith("get")) {
            propertyName = Introspector.decapitalize(name.substring("get".length()));
        } else if (name.startsWith("is")) {
            propertyName = Introspector.decapitalize(name.substring("is".length()));
        }
        JAXXPropertyDescriptor property = propertyName != null ?
                properties.get(propertyName) : null;
        if (property != null) {
            return property.isBound();
        }
        try {
            FieldDescriptor field = getBeanClass().getFieldDescriptor(name);
            return Modifier.isFinal(field.getModifiers()); // final fields might as well be considered bound -- they can't be modified anyway
        } catch (NoSuchFieldException e) {
            throw new UnsupportedAttributeException("cannot find property '" + name + "' of " + getBeanClass());
        }
    }

    /**
     * Configures the event handling for members which do not fire <code>PropertyChangeEvent</code> when
     * modified.  The default implementation does nothing.  Subclasses should override this method to call
     * <code>addProxyEventInfo</code> for each member which requires special handling.
     */
    protected void configureProxyEventInfo() {
    }

    /**
     * Configures a proxy event handler which fires <code>PropertyChangeEvents</code> when a non-bound
     * member is updated.  This is necessary for all fields (which cannot be bound) and for methods that are
     * not bound property <code>get</code> methods.  The proxy event handler will attach the specified kind
     * of listener to the class and fire a <code>PropertyChangeEvent</code> whenever the listener receives
     * any kind of event.
     * <p>
     * Even though this method can theoretically be applied to fields (in addition to methods), it would be an
     * unusual situation in which that would actually work -- as fields cannot fire events when modified, it would
     * be difficult to have a listener that was always notified when a field value changed.
     *
     * @param memberName    the name of the field or method being proxied
     * @param listenerClass the type of listener which receives events when the field or method is updated
     */
    public void addProxyEventInfo(String memberName, Class<?> listenerClass) {
        addProxyEventInfo(memberName, listenerClass, null);
    }

    /**
     * Configures a proxy event handler which fires <code>PropertyChangeEvents</code> when a non-bound
     * member is updated.  This is necessary for all fields (which cannot be bound) and for methods that are
     * not bound property <code>get</code> methods.  This variant attaches a listener to a property of the
     * object (such as <code>model</code>) and not the object itself, which is useful when there is a model
     * that is the "real" container of the information.  The proxy event handler will attach the specified kind
     * of listener to the property's value (retrieved using the property's <code>get</code> method) and fire
     * a <code>PropertyChangeEvent</code> whenever the listener receives any kind of event.
     * <p>
     * If the property is itself bound (typically the case with models), any updates to the property's value will
     * cause the listener to be removed from the old property value and reattached to the new property value,
     * as well as cause a <code>PropertyChangeEvent</code> to be fired.
     * <p>
     * Even though this method can theoretically be applied to fields (in addition to methods), it would be an
     * unusual situation in which that would actually work -- as fields cannot fire events when modified, it would
     * be difficult to have a listener that was always notified when a field value changed.
     *
     * @param memberName    the name of the field or method being proxied
     * @param listenerClass the type of listener which receives events when the field or method is updated
     * @param modelName     the JavaBeans-style name of the model property
     */
    public void addProxyEventInfo(String memberName,
                                  Class<?> listenerClass,
                                  String modelName) {
        String listenerName = listenerClass.getName();
        listenerName =
                listenerName.substring(listenerName.lastIndexOf(".") + 1);
        addProxyEventInfo(memberName,
                          listenerClass,
                          modelName,
                          "add" + listenerName,
                          "remove" + listenerName
        );
    }

    public void addProxyEventInfo(String memberName,
                                  Class<?> listenerClass,
                                  String modelName,
                                  String addMethod,
                                  String removeMethod) {
        try {
            ClassDescriptor classDescriptor =
                    ClassDescriptorHelper.getClassDescriptor(
                            listenerClass.getName()
                    );

            addProxyEventInfo(memberName,
                              classDescriptor,
                              modelName,
                              addMethod,
                              removeMethod
            );
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    // TODO: remove this temporary method, complete switchover to ClassDescriptors

    /**
     * Configures a proxy event handler which fires <code>PropertyChangeEvents</code> when a non-bound
     * member is updated.  This is necessary for all fields (which cannot be bound) and for methods that are
     * not bound property <code>get</code> methods.  This variant attaches a listener to a property of the
     * object (such as <code>model</code>) and not the object itself, which is useful when there is a model
     * that is the "real" container of the information.  The proxy event handler will attach the specified kind
     * of listener to the property's value (retrieved using the property's <code>get</code> method) and fire
     * a <code>PropertyChangeEvent</code> whenever the listener receives any kind of event.
     * <p>
     * If the property is itself bound (typically the case with models), any updates to the property's value will
     * cause the listener to be removed from the old property value and reattached to the new property value,
     * as well as cause a <code>PropertyChangeEvent</code> to be fired.
     * <p>
     * This variant of <code>addProxyEventInfo</code> allows the names of the methods that add and remove
     * the event listener to be specified, in cases where the names are not simply <code>add&lt;listenerClassName&gt;</code>
     * and <code>remove&lt;listenerClassName&gt;</code>.
     * <p>
     * Even though this method can theoretically be applied to fields (in addition to methods), it would be an
     * unusual situation in which that would actually work -- as fields cannot fire events when modified, it would
     * be difficult to have a listener that was always notified when a field value changed.
     *
     * @param memberName    the name of the field or method being proxied
     * @param listenerClass the type of listener which receives events when the field or method is updated
     * @param modelName     the JavaBeans-style name of the model property
     * @param addMethod     add method name
     * @param removeMethod  remove method name
     */
    public void addProxyEventInfo(String memberName,
                                  ClassDescriptor listenerClass,
                                  String modelName,
                                  String addMethod,
                                  String removeMethod) {
        ProxyEventInfo info = new ProxyEventInfo();
        info.memberName = memberName;
        info.listenerClass = listenerClass;
        info.modelName = modelName;
        info.addMethod = addMethod;
        info.removeMethod = removeMethod;
        if (eventInfos == null) {
            eventInfos = new HashMap<>();
        }
        eventInfos.put(memberName, info);
    }

    @Override
    public void compileFirstPass(Element tag,
                                 JAXXCompiler compiler) throws CompilerException, IOException {
        scanAttributesForDependencies(tag, compiler);
        compileChildrenFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag,
                                  JAXXCompiler compiler) throws CompilerException, IOException {
        safeInit();
        CompiledObject object = objectMap.get(tag);
        if (object == null) {
            throw new IllegalStateException(
                    "unable to find CompiledObject associated with tag <" +
                            tag.getTagName() + ">;  should have been registered " +
                            "before second pass"
            );
        }
        compiler.checkOverride(object);
        String constructorParams =
                tag.getAttribute(CONSTRUCTOR_PARAMS_ATTRIBUTE);
        if (StringUtils.isNotEmpty(constructorParams)) {
            object.setConstructorParams(
                    compiler.getScriptManager().trimScript(constructorParams));
        }

        setDefaults(object, tag, compiler);
        setAttributes(object, tag, compiler);
        //TC-20091105, can apply genericType everyWhere, should just test that class is generic (not possible actually)
//        if (object.getGenericTypesLength() > 0 && !(object == compiler.getRootObject() || object.isJavaBean())) {
//            // can ony be apply to root object or javaBean object
//            compiler.reportWarning("'genericType' attribute can only be found on root, or a javaBean object tag but was found on tag " + tag);
//            object.setGenericTypes(null);
//            return;
//        }
        compileChildrenSecondPass(tag, compiler);
    }

    public void registerCompiledObject(Element tag, JAXXCompiler compiler) {
        String id = tag.getAttribute(ID_ATTRIBUTE);
        if (id == null || id.length() == 0) {
            id = compiler.getAutoId(getBeanClass().getSimpleName());
//            id = compiler.getAutoId(getBeanClass());
        }
        CompiledObject object = createCompiledObject(id, compiler);
        objectMap.put(tag, object);
        String styleClass = tag.getAttribute(STYLE_CLASS_ATTRIBUTE).trim();
        if (styleClass.length() > 0) {
            object.setStyleClass(styleClass);
        }
        compiler.registerCompiledObject(object);
    }

    /**
     * Creates the <code>CompiledObject</code> which will represent the object
     * created by this <code>TagHandler</code>.
     *
     * @param id       the <code>CompiledObject's</code> ID.
     * @param compiler compiler to use
     * @return the <code>CompiledObject</code> to use
     */
    protected CompiledObject createCompiledObject(String id,
                                                  JAXXCompiler compiler) {
        return new CompiledObject(id, getBeanClass(), compiler);
    }

    /**
     * Initializes the default settings of the object, prior to setting its
     * attribute values.  The default implementation does nothing.
     *
     * @param object   the object to initialize
     * @param tag      the tag being compiled
     * @param compiler the current <code>JAXXCompiler</code>
     */
    protected void setDefaults(CompiledObject object,
                               Element tag,
                               JAXXCompiler compiler) {
    }

    /**
     * @param property property name to test
     * @return <code>true</code> if the specified property should be inherited by child components when specified
     * via CSS.
     * @throws UnsupportedAttributeException if attribute is not supported
     */
    public boolean isPropertyInherited(String property) throws UnsupportedAttributeException {
        return false;
    }

    /**
     * @param name name of event
     * @return <code>true</code> if the specified name has the form of an event handler attribute
     * (e.g. "onActionPerformed").
     */
    public boolean isEventHandlerName(String name) {
        return name.length() > 2 &&
                name.startsWith("on") &&
                Character.isUpperCase(name.charAt(2));
    }

    /**
     * Scans all attributes for any dependency classes and adds them to the current compilation
     * set.  Called by <code>compileFirstPass()</code> (it is an error to add dependencies after
     * pass 1 is complete).
     *
     * @param tag      tag to scan
     * @param compiler compiler to use
     */
    protected void scanAttributesForDependencies(Element tag,
                                                 JAXXCompiler compiler) {
        List<Attr> attributes = new ArrayList<>();
        NamedNodeMap children = tag.getAttributes();
        for (int i = 0; i < children.getLength(); i++) {
            attributes.add((Attr) children.item(i));
        }
        attributes.sort(getAttributeComparator());

        for (Attr attribute : attributes) {
            String name = attribute.getName();
            String value = attribute.getValue();
            if (name.equals(JAVA_BEAN_ATTRIBUTE)) {
                I18nHelper.tryToRegisterI18nInvocation(compiler, value);
                continue;
            }
            if (name.equals(CONSTRAINTS_ATTRIBUTE) ||
                    isEventHandlerName(name)) {
                // adds dependencies as a side effect
                compiler.preprocessScript(value);
            } else if (name.equals(CONSTRUCTOR_PARAMS_ATTRIBUTE)) {
                //fix bug 178 : if a constructor parameter contains a comma
                // the split will not works.
                // using this hack will always works :) (but still a hack)
                compiler.preprocessScript("java.util.Arrays.toString(" + value + ")");
//                for (String param : value.split("\\s*,\\s*")) {
//                    compiler.preprocessScript(param);
//                }
            } else if (value.startsWith("{") && value.endsWith("}")) {
                compiler.preprocessScript(value.substring(1, value.length() - 1));
            }
        }
    }

    /**
     * Processes the attributes of an XML tag.  Four kinds of attributes are supported: simple property values (of any
     * datatype  supported by {@link #convertFromString}), data binding expressions (attributes containing curly-brace
     * pairs), event listeners (attributes starting with 'on', such as 'onActionPerformed'), and JAXX-defined properties
     * such as 'id'.
     *
     * @param object   the object to be modified
     * @param tag      the tag from which to pull attributes
     * @param compiler the current <code>JAXXCompiler</code>
     */
    public void setAttributes(CompiledObject object,
                              Element tag,
                              JAXXCompiler compiler) {
        List<Attr> attributes = new ArrayList<>();
        NamedNodeMap children = tag.getAttributes();

        for (int i = 0; i < children.getLength(); i++) {
            attributes.add((Attr) children.item(i));
        }
        attributes.sort(getAttributeComparator());

        CompiledObject rootObject = compiler.getRootObject();

        Attr genericTypeAttribute = null;
        for (Attr attribute : attributes) {
            String name = attribute.getName();
            String value = attribute.getValue().trim();
            if (name.equals(ID_ATTRIBUTE) ||
                    name.equals(CONSTRAINTS_ATTRIBUTE) ||
                    name.equals(CONSTRUCTOR_PARAMS_ATTRIBUTE) ||
                    name.equals(STYLE_CLASS_ATTRIBUTE) ||
                    name.startsWith(XMLNS_ATTRIBUTE) ||
                    JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                // ignore, already handled
                continue;
            }

            if (compiler.isUseHandler()) {
                if (name.startsWith(HANDLER_ATTRIBUTE)) {
                    // ignore, already handled
                    continue;
                }
            }
            if (name.equals(JAVA_BEAN_ATTRIBUTE)) {
                object.setJavaBean(true);
                if (!value.isEmpty()) {
                    if (value.startsWith("{")) {
                        String binding = compiler.processDataBindings(value);
                        boolean withBinding = binding != null;

                        if (!withBinding) {
                            compiler.reportError(tag, "Should have a databinding while using braced notation in javaBean attribute");
                        }
                        String setPropertyCode = String.format("%s(%s);", object.getSetterName(),binding);

                        compiler.getBindingHelper().registerDataBinding(
                                compiler.getRootObject().getId() + "." + object.getId(),
                                binding, setPropertyCode);
                        object.setJavaBeanInitCode(binding);
                    } else {
                        object.setJavaBeanInitCode(value);
                    }
                }
                continue;
            }
            if (name.equals(INITIALIZER_ATTRIBUTE)) {
                if (!value.isEmpty()) {
                    I18nHelper.tryToRegisterI18nInvocation(compiler, value);
                    object.setInitializer(value);
                } else {
                    object.setInitializer("null");
                }
                continue;
            }

            if (name.equals(IMPLEMENTS_ATTRIBUTE)) {
                if (object != rootObject) {
                    // can ony be apply to root object
                    compiler.reportError("'" + IMPLEMENTS_ATTRIBUTE + "' attribute can only be found on root tag but was found on tag " + tag);
                    return;
                }
                //tchemit 2011-01-29 reuse what was filled in symbols table
                String[] interfaces = compiler.getSymbolTable().getInterfaces();
//                String[] interfaces = value.split(",");
                compiler.setExtraInterfaces(interfaces);
                continue;
            }

            if (name.equals(ABSTRACT_ATTRIBUTE)) {
                if (object != rootObject) {
                    // can ony be apply to root object
                    compiler.reportError("'" + ABSTRACT_ATTRIBUTE + "' attribute can only be found on root tag but was found on tag " + tag);
                    return;
                }
                compiler.setAbstractClass(true);
                continue;
            }

            if (name.equals(GENERIC_TYPE_ATTRIBUTE)) {
                //TC-20090313 check after all attributes been processed
                genericTypeAttribute = attribute;
                continue;
            }

            if (name.equals(SUPER_GENERIC_TYPE_ATTRIBUTE)) {
                if (object != rootObject) {
                    // can ony be apply to root object
                    compiler.reportError("'" + SUPER_GENERIC_TYPE_ATTRIBUTE + "' attribute can only be found on root tag but was found on tag " + tag);
                    return;
                }
                compiler.setSuperGenericType(value);
                continue;
            }

            if (name.equals(I18N_FORMAT_ATTRIBUTE)) {
                if (object != rootObject) {
                    // can ony be apply to root object
                    compiler.reportError("'" + I18N_FORMAT_ATTRIBUTE + "' attribute can only be found on root tag but was found on tag " + tag);
                    return;
                }
                compiler.setI18nFormat(value);
                compiler.addImport(I18n.class);
                continue;
            }

            if (name.equals(DECORATOR_ATTRIBUTE)) {
                if (!value.isEmpty()) {
                    CompiledObjectDecorator decorator =
                            compiler.getEngine().getDecorator(value);
                    object.setDecorator(decorator);
                }
                continue;
            }

            if (isEventHandlerName(name)) {
                // event handler
                if (!value.endsWith(";") && !value.endsWith("}")) {
                    value += ";";
                }
                addEventHandler(object,
                                Introspector.decapitalize(name.substring(2)),
                                value,
                                compiler
                );
                continue;
            }
            // simple property
            setAttribute(object, name, value, true, compiler);
        }

        if (genericTypeAttribute != null) {
            String name = genericTypeAttribute.getName();
            String value = genericTypeAttribute.getValue().trim();
            if (object == rootObject) {
                compiler.setGenericType(value);
            } else {
                object.setGenericTypes(value.split(","));
                if (object.getSimpleType() != null) {

                    // reload the simpleType
                    object.setSimpleType(object.getSimpleType() + "<" + value + ">");
                }
            }
            if (ClassDescriptorHelper.getClassDescriptor(BeanTypeAware.class).isAssignableFrom(object.getObjectClass())) {
                String fqn = compiler.getImportedTypeForSimpleName(value);
                if (fqn != null) {
                    // add beanType from genericType
                    if (log.isDebugEnabled()) {
                        log.debug("Add beanType property: " + value + " to " + object);
                    }

                    setAttribute(object, BeanTypeAware.PROPERTY_BEAN_TYPE, "{" + value + ".class}", true, compiler);
                }


            }
        }

    }

    /**
     * Returns a <code>Comparator</code> which defines the ordering in which the tag's attributes should be processed.  The
     * default implementation sorts the attributes according to the order defined by the {@link #getAttributeOrdering} method.
     *
     * @return a <code>Comparator</code> defining the order of attribute processing
     */
    protected Comparator<Attr> getAttributeComparator() {
        return (a, b) -> {
            int aOrder = getAttributeOrdering(a);
            int bOrder = getAttributeOrdering(b);

            return aOrder - bOrder;
        };
    }

    /**
     * Returns the priority with which a particular attribute should be processed.  Lower numbers should be processed before
     * higher numbers.  This value is used by the {@link #getAttributeComparator} method to define the sort ordering.
     *
     * @param attr the attribute to treate
     * @return the attribute's priority
     */
    protected int getAttributeOrdering(Attr attr) {
        if (attr.getName().equals(DISPLAYED_MNEMONIC_INDEX_ATTRIBUTE) ||
                attr.getName().equals(DISPLAYED_MNEMONIC_ATTRIBUTE) ||
                attr.getName().equals(MNEMONIC_ATTRIBUTE)) {
            return 1;
        }
        return 0;
    }

    /**
     * Set a single property on an object.  The value may be either a simple value or contain data binding expressions.
     * Simple values are first converted to the property's type using {@link #convertFromString(String, String, Class)}.
     *
     * @param object       the object on which to set the property
     * @param propertyName the name of the property to set
     * @param stringValue  the raw string value of the property from the XML
     * @param compiler     the current <code>JAXXCompiler</code>
     */
    public void setAttributeFromCss(CompiledObject object,
                                    String propertyName,
                                    String stringValue,
                                    JAXXCompiler compiler) {

        try {

            object.addProperty(propertyName, stringValue);
            ClassDescriptor type = getPropertyType(object, propertyName, compiler);
            String binding = compiler.processDataBindings(stringValue);
            boolean withBinding = binding != null;

            if (!withBinding) {
                // no bindings, convert from string

                // add support for i18n attributes (otherwise already done in DefaultComponentHandler)
                if (I18nHelper.isI18nableAttribute(propertyName, compiler)) {
                    stringValue = I18nHelper.addI18nInvocation(object.getId(), propertyName, stringValue, compiler);
                }

                try {
                    Class<?> typeClass = type != null ?
                            ClassDescriptorHelper.getClass(type.getName(), type.getClassLoader()) :
                            null;
                    Object value = convertFromString(propertyName,
                                                     stringValue,
                                                     typeClass
                    );
                    setProperty(object, propertyName, value, compiler);
                    return;
                } catch (IllegalArgumentException e) {
                    compiler.reportError("could not convert literal string '" + stringValue + "' to type " + type.getName());
                } catch (ClassNotFoundException e) {
                    compiler.reportError("could not find class " + type.getName());
                }
            }
            String setPropertyCode = getSetPropertyCode(object.getJavaCode(), propertyName, binding, compiler);
            if (propertyName.equals(LAYOUT_ATTRIBUTE)) {

                // try to add the layout class in imports
                if (setPropertyCode.contains(BORDER_LAYOUT_PREFIX)) {
                    compiler.addImport(BorderLayout.class);
                } else if (setPropertyCode.contains(GRID_LAYOUT_PREFIX)) {
                    compiler.addImport(GridLayout.class);
                }
                // handle containerDelegate (e.g. contentPane on JFrame)
                // have to set layout early, before children are added
                object.appendInitializationCode(setPropertyCode);
            } else if (propertyName.equals(COMPUTE_I18N_ATTRIBUTE)) {
                setAttribute(object, propertyName, stringValue, true, compiler);
            }
            compiler.getBindingHelper().registerDataBinding(
                    object.getId() + "." + propertyName,
                    binding,
                    setPropertyCode
            );
        } catch (UnsupportedAttributeException e) {
            compiler.reportError("class " + object.getObjectClass().getName() + " does not support attribute '" + propertyName + "'");
        }
    }

    /**
     * Set a single property on an object.  The value may be either a simple value or contain data binding expressions.
     * Simple values are first converted to the property's type using {@link #convertFromString(String, String, Class)}.
     *
     * @param object       the object on which to set the property
     * @param propertyName the name of the property to set
     * @param stringValue  the raw string value of the property from the XML
     * @param inline       <code>true</code> if the value was directly specified as an inline class tag attribute, <code>false</code> otherwise (a default value, specified in CSS, etc.)
     * @param compiler     the current <code>JAXXCompiler</code>
     */
    public void setAttribute(CompiledObject object,
                             String propertyName,
                             String stringValue,
                             boolean inline,
                             JAXXCompiler compiler) {

        if (propertyName.equals(COMPUTE_I18N_ATTRIBUTE)) {

            if ("skip".equals(stringValue)) {
                return;
            }
            String i18nKey = compiler.computeI18n(object.getId(), stringValue);
            String i18nProperty = compiler.getI18nProperty(object);
            setAttribute(object, i18nProperty, i18nKey, false, compiler);
            return;
        }

        try {
            //---------------------------------------------------------------------
            // BE WARE, Test if removing this code hurts..., anyway we have a bug on it:
            // For some component (example :  jaxx.runtime.swing.editor.NumberEditor) we do not have one component
            // This is disturbing, must find how why... And this happens only on a mvn clean install on all project
            // Everything is fine otherwise...
            //
            // With new binding design, we will know if there is a binding on the property
            // of the jaxxObject, and could add it only if required...
            //---------------------------------------------------------------------
            //FIXME TC-20091105 we should to this later when all binding are compiled
            // because we don't know yet if this is a databinding
//            if (ClassDescriptorHelper.getClassDescriptor(JAXXObject.class).isAssignableFrom(object.getObjectClass())) {
//                // check for data binding & remove if found
//                JAXXObjectDescriptor jaxxObjectDescriptor = object.getObjectClass().getJAXXObjectDescriptor();
//                if (jaxxObjectDescriptor.getComponentDescriptors().length == 0) {
//                    compiler.reportWarning("JAXXObject component " + object.getObjectClass() + " should have at least one component!");
////                    throw new IllegalStateException("JAXXObject component " + object.getObjectClass() + " should have at least one component!");
//                } else {
//                    ComponentDescriptor root = jaxxObjectDescriptor.getComponentDescriptors()[0];
//                    //TC-20091026 do not prefix binding by object id if on root object
//                    String prefix;
//                    if (object == compiler.getRootObject()) {
//                        prefix = "";
//                    } else {
//                        prefix = object.getJavaCode() + ".";
//                    }
//                    object.appendInitializationCode(prefix + "removeDataBinding(" + compiler.getJavaCode(root.getId() + "." + propertyName) + ");");
//                }
//            }
            object.addProperty(propertyName, stringValue);
            ClassDescriptor type = getPropertyType(object, propertyName, compiler);
            String binding = compiler.processDataBindings(stringValue);
            boolean withBinding = binding != null;

            if (inline) {
                compiler.addInlineStyle(object, propertyName, withBinding);
            }
            if (!withBinding) {
                // no bindings, convert from string
                try {
                    Class<?> typeClass = type != null ?
                            ClassDescriptorHelper.getClass(type.getName(), type.getClassLoader()) :
                            null;
                    Object value = convertFromString(propertyName,
                                                     stringValue,
                                                     typeClass
                    );
                    setProperty(object, propertyName, value, compiler);
                    return;
                } catch (IllegalArgumentException e) {
                    compiler.reportError("could not convert literal string '" + stringValue + "' to type " + type.getName());
                } catch (ClassNotFoundException e) {
                    compiler.reportError("could not find class " + type.getName());
                }
            }
            String setPropertyCode = getSetPropertyCode(object.getJavaCode(), propertyName, binding, compiler);
            if (propertyName.equals(LAYOUT_ATTRIBUTE)) {

                // try to add the layout class in imports
                if (setPropertyCode.contains(BORDER_LAYOUT_PREFIX)) {
                    compiler.addImport(BorderLayout.class);
                } else if (setPropertyCode.contains(GRID_LAYOUT_PREFIX)) {
                    compiler.addImport(GridLayout.class);
                }
                // handle containerDelegate (e.g. contentPane on JFrame)
                // have to set layout early, before children are added
                object.appendInitializationCode(setPropertyCode);
            }
            compiler.getBindingHelper().registerDataBinding(
                    object.getId() + "." + propertyName,
                    binding,
                    setPropertyCode
            );
        } catch (UnsupportedAttributeException e) {
            compiler.reportError("class " + object.getObjectClass().getName() + " does not support attribute '" + propertyName + "'");
        }
    }

    public void applyStylesheets(CompiledObject object, JAXXCompiler compiler) {
        applyStylesheets(object, compiler, null);
    }

    private void applyStylesheets(CompiledObject object,
                                  JAXXCompiler compiler,
                                  Stylesheet overrides) {
        applyStylesheets(object, compiler, overrides, true);
    }

    private void applyStylesheets(CompiledObject object,
                                  JAXXCompiler compiler,
                                  Stylesheet overrides,
                                  boolean recurse) {

        Stylesheet stylesheet = compiler.getStylesheet();

        ClassDescriptor objectClass = object.getObjectClass();
        ClassDescriptor jaxxObjectClassDescriptor =
                ClassDescriptorHelper.getClassDescriptor(JAXXObject.class);

        // to apply styleSheet to a jaxx object,
        // since 2.0.2, this process can be skip if configuration
        // autoRecurseInCss is set to false since this does not work
        boolean applyInside =
                recurse &&
                        jaxxObjectClassDescriptor.isAssignableFrom(objectClass) &&
                        // new since 2.0.2 to skip old buggy mode
                        compiler.getConfiguration().isAutoRecurseInCss();

        try {

            if (!applyInside) {

                // this is the safe mode to use, just apply styleSheet to object
                if (stylesheet != null) {
                    StylesheetHelper.applyTo(object,
                                             compiler,
                                             stylesheet,
                                             overrides
                    );
                }
                return;
            }

            //FIXME TC-20100430 This is an old mode which try to apply inside a
            // detected jaxx object styleSheets (cascade) :
            // It does not work in fact and must be repair...

            JAXXObjectDescriptor jaxxObjectDescriptor =
                    objectClass.getJAXXObjectDescriptor();
            ComponentDescriptor[] descriptors =
                    jaxxObjectDescriptor.getComponentDescriptors();
            for (ComponentDescriptor descriptor : descriptors) {
                ClassDescriptor classDescriptor =
                        ClassDescriptorHelper.getClassDescriptor(
                                descriptor.getJavaClassName()
                        );
                boolean isRoot = classDescriptor != objectClass;
                String id = isRoot ? object.getId() + ' ' + descriptor.getId() : "( " + object.getId() + " ) " + descriptor.getId();
                CompiledObject child = new CompiledObject(id,
                                                          "((" + JAXXCompiler.getCanonicalName(classDescriptor) + ") " +
                                                                  object.getJavaCode() + ".getObjectById(" + TypeManager.getJavaCode(descriptor.getId()) + "))",
                                                          classDescriptor,
                                                          compiler,
                                                          true);
                ComponentDescriptor parentDescriptor = descriptor.getParent();
                CompiledObject currentObject = child;
                while (parentDescriptor != null) {
                    CompiledObject parent = new CompiledObject("internal", ClassDescriptorHelper.getClassDescriptor(parentDescriptor.getJavaClassName()), compiler);
                    currentObject.setParent(parent);
                    currentObject = parent;
                    parentDescriptor = parentDescriptor.getParent();
                }
                currentObject.setParent(object);
                String styleClass = object.getStyleClass();
                if (styleClass == null) {
                    styleClass = descriptor.getStyleClass();
                }
                child.setStyleClass(styleClass);
                Stylesheet mergedStylesheet = overrides;
                Stylesheet childOverrides = jaxxObjectDescriptor.getStylesheet();
                if (childOverrides != null) {
                    if (mergedStylesheet == null) {
                        mergedStylesheet = childOverrides;
                    } else {
                        mergedStylesheet.add(childOverrides.getRules());
                    }
                }
                TagManager.getTagHandler(objectClass).applyStylesheets(child, compiler, mergedStylesheet, isRoot);
                object.appendInitializationCode(child.getInitializationCode(compiler));
            }
        } catch (ParseException e) {
            compiler.reportError("Java parser exception on " + object + " in his css rules: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new CompilerException(e);
        } catch (IllegalArgumentException e) {
            compiler.reportError(e.getMessage());
        }
    }

    /**
     * Adds the necessary Java code to a <code>CompiledObject</code> to add an event listener at runtime.
     *
     * @param object   the <code>CompiledObject</code> to which the event listener should be added
     * @param name     the name of the event listener, such as <code>"actionPerformed"</code>
     * @param value    the Java code snippet to execute when the event is fired
     * @param compiler the current <code>JAXXCompiler</code>
     */
    public void addEventHandler(CompiledObject object,
                                String name,
                                String value,
                                JAXXCompiler compiler) {
        JAXXEventSetDescriptor descriptorSet = events.get(name);
        if (descriptorSet != null) {
            MethodDescriptor[] listenerMethods =
                    descriptorSet.getListenerMethods();
            MethodDescriptor listenerMethod = null;
            for (MethodDescriptor listenerMethod1 : listenerMethods) {
                if (listenerMethod1.getName().equals(name)) {
                    listenerMethod = listenerMethod1;
                    break;
                }
            }
            if (listenerMethod == null) {
                throw new RuntimeException("expected to find method '" + name + "' in JAXXEventSetDescriptor.getListenerMethods()");
            }
            try {
                value = compiler.preprocessScript(value);
                object.addEventHandler(
                        name,
                        descriptorSet.getAddListenerMethod(),
                        listenerMethod,
                        value,
                        compiler
                );
            } catch (CompilerException e) {
                compiler.reportError("While parsing event handler for '" + name + "': " + e.getMessage());
            }
        } else {
            compiler.reportError("could not find event '" + name + "' for object " + object);
        }
    }

    /**
     * Returns a snippet of Java code which will retrieve an object property at runtime.  Typically the code is
     * just a call to the property's <code>get</code> method, but it can be arbitrarily complex.
     *
     * @param javaCode Java code for the object whose property is being retrieved
     * @param name     the name of the property to retrieve
     * @param compiler the current <code>JAXXCompiler</code>
     * @return the snippet
     * @throws CompilerException if a compilation error occurs
     */
    public String getGetPropertyCode(String javaCode,
                                     String name,
                                     JAXXCompiler compiler) {
        safeInit();

        JAXXPropertyDescriptor property = properties.get(name);
        if (property != null) {
            if (property.getReadMethodDescriptor() != null) {
                return javaCode + '.' + property.getReadMethodDescriptor().getName() + "()";
            }
            throw new UnsupportedAttributeException("property '" + name + "' of " + getBeanClass().getName() + " has no read method");
        }
        throw new UnsupportedAttributeException("property '" + name + "' could not be found in class " + getBeanClass().getName());
    }

    /**
     * Returns a snippet of Java code which will set an object property at runtime.  Typically the code is
     * just a call to the property's <code>set</code> method, but it can be arbitrarily complex.
     *
     * @param javaCode  Java code for the object whose property is being set
     * @param name      the name of the property to set
     * @param valueCode Java expression representing the value to set the property to
     * @param compiler  the current <code>JAXXCompiler</code>
     * @return the snippet
     * @throws CompilerException if a compilation error occurs
     */
    public String getSetPropertyCode(String javaCode,
                                     String name,
                                     String valueCode,
                                     JAXXCompiler compiler) {
        // ajout du support i18n
        if (I18nHelper.isI18nableAttribute(name, compiler)) {
            if (compiler.getCompiledObject(valueCode) == null) {
                valueCode = I18nHelper.addI18nInvocation(name, name, valueCode, compiler);
            }
        }
        JAXXPropertyDescriptor property = properties.get(name);
        if (property != null) {
            if (property.getWriteMethodDescriptor() != null) {
                //TC-20091026 do not prefix by javaCode if on root object
                String prefix;
                if (compiler.getRootObject().getJavaCode().equals(javaCode)) {
                    prefix = "";
                } else {
                    prefix = javaCode + ".";
                }
                return prefix + property.getWriteMethodDescriptor().getName() + '(' + valueCode + ");";
            }
            throw new UnsupportedAttributeException("property '" + name + "' of " + getBeanClass().getName() + " is read-only");
        }
        throw new UnsupportedAttributeException("property '" + name + "' could not be found in class " + getBeanClass().getName());
    }

    /**
     * Appends Java code to a <code>CompiledObject</code> in order to implement a property assignment.
     * <code>setProperty</code> is invoked in response to most XML attributes (those which are not more
     * complicated cases, like data bindings or event handlers).
     * <p>
     * By the time it reaches this method, the <code>value</code> has already been converted from its XML
     * string representation to the appropriate destination type for the property (i.e. if
     * <code>JLabel.foreground</code> is being set, <code>value</code> will be a <code>Color</code>).
     *
     * @param object   the object being modified
     * @param name     the name of the property to set
     * @param value    the value to set the property to
     * @param compiler the current <code>JAXXCompiler</code>
     * @throws CompilerException if a compilation error occurs
     */
    public void setProperty(CompiledObject object,
                            String name,
                            Object value,
                            JAXXCompiler compiler) {
        object.appendInitializationCode(
                getSetPropertyCode(object.getJavaCodeForProperty(name),
                                   name,
                                   TypeManager.getJavaCode(value),
                                   compiler
                )
        );
    }

    /**
     * Maps string values onto integers, so that int-valued enumeration properties can be specified by strings.  For
     * example, when passed a key of 'alignment', this method should normally map the values 'left', 'center', and
     * 'right' onto SwingConstants.LEFT, SwingConstants.CENTER, and SwingConstants.RIGHT respectively.
     * <p>
     * You do not normally need to call this method yourself; it is invoked by {@link #convertFromString} when an
     * int-valued property has a value which is not a valid number.  By default, this method looks at the
     * <code>enumerationValues</code> value of the <code>JAXXPropertyDescriptor</code>.
     *
     * @param key   the name of the int-typed property
     * @param value the non-numeric value that was specified for the property
     * @return the constant integer value
     * @throws IllegalArgumentException if the property is an enumeration, but the value is not valid
     * @throws NumberFormatException    if the property is not an enumeration
     */
    protected int constantValue(String key, String value) {
        JAXXBeanInfo JAXXBeanInfo = getJAXXBeanInfo();
        JAXXPropertyDescriptor[] props =
                JAXXBeanInfo.getJAXXPropertyDescriptors();
        String lowercaseValue = value.toLowerCase();
        for (JAXXPropertyDescriptor property : props) {
            if (property.getName().equals(key)) {
                Object[] values = (Object[])
                        property.getValue("enumerationValues");
                if (values != null) {
                    for (int j = 0; j < values.length - 2; j += 3) {
                        if (((String) values[j]).toLowerCase().equals(lowercaseValue)) {
                            return (Integer) values[j + 1];
                        }
                    }

                    StringBuilder message =
                            new StringBuilder("value of '" +
                                                      key + "' must be one of: [");
                    for (int j = 0; j < values.length - 2; j += 3) {
                        if (j != 0) {
                            message.append(", ");
                        }
                        message.append(((String) values[j]).toLowerCase());
                    }
                    message.append("] (found '").append(value).append("')");
                    throw new IllegalArgumentException(message.toString());
                }
            }
        }
        throw new NumberFormatException(value);
    }

    /**
     * As {@link TypeManager#convertFromString(String, Class)}, except that it additionally supports constant names
     * for <code>int</code>-valued types.
     *
     * @param key   the name of the property whose value is being converted
     * @param value the raw string value of the property as it appears in the XML
     * @param type  the datatype to convert the string into
     * @return the converted object
     * @see #constantValue
     */
    protected Object convertFromString(String key, String value, Class<?> type) {
        if (type == null || Object.class.equals(type)) {
            return value;
        }

        try {
            return TypeManager.convertFromString(value, type);
        } catch (NumberFormatException e) {
            if (int.class.equals(type) || Integer.class.equals(type)) {
                return constantValue(key, value);
            }
            throw e;
        }
    }

    /**
     * Compiles the child tags of the current tag.  The default implementation invokes {@link #compileChildTagFirstPass}
     * for each child tag.
     *
     * @param tag      the tag whose children to run
     * @param compiler the current <code>JAXXCompiler</code>
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    protected void compileChildrenFirstPass(Element tag,
                                            JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagFirstPass(child, compiler);
            }
        }
    }

    /**
     * Compiles the child tags of the current tag.  The default implementation invokes {@link #compileChildTagFirstPass}
     * for each child tag.
     *
     * @param tag      the tag whose children to run
     * @param compiler the current <code>JAXXCompiler</code>
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    protected void compileChildrenSecondPass(Element tag,
                                             JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    /**
     * Compiles a child of the current tag.  The default implementation calls {@link JAXXCompiler#compileFirstPass
     * JAXXCompiler.compileFirstPass}.
     *
     * @param tag      the child tag to run
     * @param compiler the current <code>JAXXCompiler</code>
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    protected void compileChildTagFirstPass(Element tag,
                                            JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    /**
     * Compiles a child of the current tag.  The default implementation calls {@link JAXXCompiler#compileFirstPass
     * JAXXCompiler.compileSecondPass}.
     *
     * @param tag      the child tag to run
     * @param compiler the current <code>JAXXCompiler</code>
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    protected void compileChildTagSecondPass(Element tag,
                                             JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }

    @Override
    public String toString() {
        return getClass().getName() + "[" + getBeanClass().getName() + "]";
    }

    protected void safeInit() {
        try {
            init();
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Encapsulates information about a "proxy event handler", which is an event handler that
     * fires PropertyChangeEvents when it is triggered.  ProxyEventInfos simplify the data binding
     * system by allowing all dependencies to fire the same kind of event even if they would
     * normally throw something else, like <code>DocumentEvent</code>.
     */
    public static class ProxyEventInfo {

        /** The name of the method or field being proxied, e.g. "getText". */
        String memberName;

        /** The "actual" event listener for the property in question, e.g. DocumentListener. */
        ClassDescriptor listenerClass;

        /**
         * In cases where a different object (such as a model) is more directly responsible for
         * managing the property, this is the name of the property where that object can be
         * found, e.g. "document" (which is turned into a call to "getDocument()").  This property
         * is also treated as a dependency of the data binding expression, and any updates to it
         * (assuming it is bound) will cause the listener to be removed from the old value and
         * attached to the new value, and the data binding to be processed.
         */
        String modelName;

        /** The name of the method used to add the "native" event listener, e.g. "addDocumentListener". */
        String addMethod;

        /** The name of the method used to remove the "native" event listener, e.g. "removeDocumentListener". */
        String removeMethod;

        public String getAddMethod() {
            return addMethod;
        }

        public ClassDescriptor getListenerClass() {
            return listenerClass;
        }

        public String getMemberName() {
            return memberName;
        }

        public String getModelName() {
            return modelName;
        }

        public String getRemoveMethod() {
            return removeMethod;
        }
    }
}
