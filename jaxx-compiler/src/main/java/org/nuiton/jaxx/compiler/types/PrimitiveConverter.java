/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import com.google.auto.service.AutoService;
import org.nuiton.jaxx.compiler.JAXXCompiler;

@AutoService(TypeConverter.class)
public class PrimitiveConverter implements TypeConverter {

    @Override
    public Class<?>[] getSupportedTypes() {

        return new Class<?>[]{
                boolean.class,
                Boolean.class,
                byte.class,
                Byte.class,
                short.class,
                Short.class,
                int.class,
                Integer.class,
                long.class,
                Long.class,
                float.class,
                Float.class,
                double.class,
                Double.class,
                char.class,
                Character.class,
                String.class
        };
    }

    @Override
    public String getJavaCode(Object object) {
        if (object instanceof Boolean) {
            return String.valueOf(((Boolean) object).booleanValue());
        }
        if (object instanceof Byte) {
            return String.valueOf(((Byte) object).byteValue());
        }
        if (object instanceof Short) {
            return String.valueOf(((Short) object).shortValue());
        }
        if (object instanceof Integer) {
            return String.valueOf(((Integer) object).intValue());
        }
        if (object instanceof Long) {
            return String.valueOf(((Long) object).longValue()) + "L";
        }
        if (object instanceof Float) {
            return String.valueOf(((Float) object).floatValue()) + "F";
        }
        if (object instanceof Double) {
            return String.valueOf(((Double) object).doubleValue());
        }
        if (object instanceof String) {
            return '"' + JAXXCompiler.escapeJavaString((String) object) + '"';
        }
        throw new IllegalArgumentException("unsupported object: " + object);
    }

    @Override
    public Object convertFromString(String string, Class<?> type) {
        if (String.class.equals(type) || Object.class.equals(type) || type == null) {
            return string;
        }
        if (int.class.equals(type) || Integer.class.equals(type)) {
            return Integer.valueOf(string);
        }
        if (boolean.class.equals(type) || Boolean.class.equals(type)) {
            if (string.toLowerCase().equals("true")) {
                return Boolean.TRUE;
            }
            if (string.toLowerCase().equals("false")) {
                return Boolean.FALSE;
            }
            throw new IllegalArgumentException("expected 'true' or 'false', found '" + string + "'");
        }
        if (byte.class.equals(type) || Byte.class.equals(type)) {
            return Byte.valueOf(string);
        }
        if (short.class.equals(type) || Short.class.equals(type)) {
            return Short.valueOf(string);
        }
        if (long.class.equals(type) || Long.class.equals(type)) {
            return Long.valueOf(string);
        }
        if (float.class.equals(type) || Float.class.equals(type)) {
            return Float.valueOf(string);
        }
        if (double.class.equals(type) || Double.class.equals(type)) {
            return Double.valueOf(string);
        }
        if (char.class.equals(type) || Character.class.equals(type)) {
            if (string.length() == 1) {
                return string.charAt(0);
            }
            throw new IllegalArgumentException("expected a single character, found '" + string + "'");
        }
        throw new IllegalArgumentException("unsupported type: " + type);
    }
}
