/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class DataListener {

    /**
     * Unique id of the data listener (should be something like objectId.propertyName
     */
    protected final String symbol;

    /**
     * the nullity test to do before to add or remove the listener
     */
    protected final String objectCode;

    /**
     * code of the add listener
     */
    protected String addListenerCode;

    /**
     * code of the remove listener
     */
    protected String removeListenerCode;

    public DataListener(String symbol,
                        String objectCode,
                        String addListenerCode,
                        String removeListenerCode) {
        this.symbol = symbol;
        this.objectCode = objectCode;
        this.addListenerCode = addListenerCode;
        this.removeListenerCode = removeListenerCode;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public String getAddListenerCode() {
        return addListenerCode;
    }

    public String getRemoveListenerCode() {
        return removeListenerCode;
    }

    @Override
    public String toString() {
        ToStringBuilder b = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        b.append("symbol", symbol);
        b.append("objectCode", objectCode);
        b.append("addListenerCode", addListenerCode.trim());
        b.append("removeListenerCode", removeListenerCode.trim());
        return b.toString();
    }
}
