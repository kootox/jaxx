/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;

/**
 * Task to execute the Second round of compile.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class CompileSecondPassTask extends JAXXEngineTask {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(CompileSecondPassTask.class);

    /** Task name */
    public static final String TASK_NAME = "CompileSecondPass";

    public CompileSecondPassTask() {
        super(TASK_NAME);
    }

    @Override
    public boolean perform(JAXXEngine engine) throws Exception {
        boolean success = true;
        boolean isVerbose = engine.isVerbose();

        // check all files are attached to a compiler
        checkAllFilesCompiled(engine);

        JAXXCompilerFile[] files = engine.getCompiledFiles();

        for (JAXXCompilerFile jaxxFile : files) {

            String className = jaxxFile.getClassName();
            if (isVerbose) {
                log.info("start " + className);
            }

            JAXXCompiler compiler = jaxxFile.getCompiler();

            addStartProfileTime(engine, compiler);
            if (log.isDebugEnabled()) {
                log.debug("runInitializers for " + className);
            }
            if (!compiler.isFailed()) {
                compiler.runInitializers();
            }
            if (log.isDebugEnabled()) {
                log.debug("compile second pass for " + className);
            }
            compiler.compileSecondPass();
            addEndProfileTime(engine, compiler);
            if (log.isDebugEnabled()) {
                log.debug("done with result [" + !compiler.isFailed() +
                                  "] for " + className);
            }
            if (compiler.isFailed()) {
                success = false;
            }
        }

        return success;
    }

}
