/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.Item;
import org.nuiton.jaxx.runtime.swing.JAXXComboBox;
import org.w3c.dom.Element;

import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;

public class JAXXComboBoxHandler extends DefaultComponentHandler {

    public JAXXComboBoxHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JAXXComboBox.class);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getSelectedIndex", ItemListener.class);
        addProxyEventInfo("getSelectedItem", ItemListener.class);
    }

    @Override
    protected CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledItemContainer(id, getBeanClass(), compiler);
    }

    @Override
    public void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileChildrenSecondPass(tag, compiler);
        CompiledItemContainer list = (CompiledItemContainer) compiler.getOpenComponent();
        List<Item> items = list.getItems();
        if (items != null && !items.isEmpty()) {
            String listName = list.getId() + "$items";
            list.appendAdditionCode("java.util.List<org.nuiton.jaxx.runtime.swing.Item> " + listName + " = new java.util.ArrayList<org.nuiton.jaxx.runtime.swing.Item>();");
            for (Item item : items) {
                String id = item.getId();
                CompiledObject compiledItem = new CompiledObject(id, ClassDescriptorHelper.getClassDescriptor(Item.class), compiler);
                compiledItem.setConstructorParams(TypeManager.getJavaCode(id) + ", " + TypeManager.getJavaCode(item.getLabel()) + ", " + TypeManager.getJavaCode(item.getValue()) + ", " + item.isSelected());
                compiler.registerCompiledObject(compiledItem);
                list.appendAdditionCode(listName + ".add(" + id + ");");
            }
            list.appendAdditionCode(list.getId() + ".setItems(" + listName + ");");
        }
    }
}



