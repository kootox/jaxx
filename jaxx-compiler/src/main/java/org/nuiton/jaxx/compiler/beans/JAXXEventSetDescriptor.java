/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;

/**
 * Mirrors the class <code>java.beans.EventSetDescriptor</code>.  JAXX uses its own introspector rather than the built-in
 * <code>java.beans.Introspector</code> so that it can introspect {@link ClassDescriptor},
 * not just <code>java.lang.Class</code>.
 */
public class JAXXEventSetDescriptor extends JAXXFeatureDescriptor {

    private final MethodDescriptor addListenerMethod;

    private final MethodDescriptor removeListenerMethod;

    private final MethodDescriptor[] listenerMethods;

    public JAXXEventSetDescriptor(ClassDescriptor classDescriptor, String name, MethodDescriptor addListenerMethod,
                                  MethodDescriptor removeListenerMethod, MethodDescriptor[] listenerMethods) {
        super(classDescriptor, name);
        this.addListenerMethod = addListenerMethod;
        this.removeListenerMethod = removeListenerMethod;
        this.listenerMethods = listenerMethods;
    }

    public MethodDescriptor getAddListenerMethod() {
        return addListenerMethod;
    }

    public MethodDescriptor getRemoveListenerMethod() {
        return removeListenerMethod;
    }

    public MethodDescriptor[] getListenerMethods() {
        return listenerMethods;
    }
}
