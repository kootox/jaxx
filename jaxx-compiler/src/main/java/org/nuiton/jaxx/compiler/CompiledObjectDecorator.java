/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.nuiton.jaxx.compiler.java.JavaFile;

/**
 * Contract of compiled object decorator at generation time.
 *
 * Note : The implementation of this class must be stateless.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public interface CompiledObjectDecorator {

    /**
     * @return the name of the decorator (match the content of a decorator
     * attribute value in a JAXX file)
     */
    String getName();

    /**
     * Obtain the setup code of the given {@code object} from the
     * {@code compiler} to be inject in the generated method
     * {@code $completeSetup}.
     *
     * @param compiler the compiler to use
     * @param object   the compiled object to treate
     * @param javaFile the file to treate
     * @return the code to inject in $completeSetup method in file
     */
    String createCompleteSetupMethod(JAXXCompiler compiler,
                                     CompiledObject object,
                                     JavaFile javaFile);

    /**
     * Create the initializer code for the given compiled {@code object} and
     * inject it in the {@code code}.
     *
     * @param compiler          the compiler to use
     * @param root              the root compiled object of the jaxx file
     * @param object            the compiled object to treate
     * @param code              where to store code to inject
     * @param lastWasMethodCall {@code true} if last instruction call was a method call
     * @return {@code true} if the last instruction was a method call
     */
    boolean createInitializer(JAXXCompiler compiler,
                              CompiledObject root,
                              CompiledObject object,
                              StringBuilder code,
                              boolean lastWasMethodCall);

    /**
     * Finalize the given compiled object before any generation in the {@code file}.
     *
     * @param compiler      the compiler to use
     * @param root          the root compiled object of the jaxx file
     * @param object        the compiled object to treate
     * @param javaFile      the file to treate
     * @param packageName   the package name
     * @param className     the (simple ?) class name
     * @param fullClassName the fully class name
     * @throws ClassNotFoundException if a class is not found
     */
    void finalizeCompiler(JAXXCompiler compiler,
                          CompiledObject root,
                          CompiledObject object,
                          JavaFile javaFile,
                          String packageName,
                          String className,
                          String fullClassName) throws ClassNotFoundException;

    /**
     * Obtain the creation code of the given {@code object} from the {@code compiler} to inject in generate method
     * {@code $initialize} or {@code createXXX} in not inline.
     *
     * @param compiler the compiler to use
     * @param object   the compiled object to treate
     * @return the code to inject in $initialize (or in the createXXX in not inline) method to create the given object
     * @throws CompilerException if any pb
     */
    String getCreationCode(JAXXCompiler compiler,
                           CompiledObject object) throws CompilerException;
}
