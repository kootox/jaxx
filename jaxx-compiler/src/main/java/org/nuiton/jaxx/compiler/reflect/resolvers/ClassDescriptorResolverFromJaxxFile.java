/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.reflect.resolvers;

import java.util.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXEngine;
import org.nuiton.jaxx.compiler.JAXXFactory;
import org.nuiton.jaxx.compiler.SymbolTable;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorResolver;
import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.JAXXObjectDescriptor;

import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * To obtain a class descriptor from a java source file.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class ClassDescriptorResolverFromJaxxFile extends ClassDescriptorResolver {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(ClassDescriptorResolverFromJaxxFile.class);

    public ClassDescriptorResolverFromJaxxFile() {
        super(ClassDescriptorHelper.ResolverType.JAXX_FILE);
    }

    @Override
    public ClassDescriptor resolvDescriptor(String className, URL source) throws ClassNotFoundException {

        JAXXEngine engine = JAXXFactory.getEngine();

        JAXXCompiler compiler = engine.getJAXXCompiler(className);

        SymbolTable symbolTable = compiler.getSymbolTable();

        if (symbolTable == null) {
            throw new CompilerException("Internal error: no symbol table was generated for class '" + className + "'");
        }

        if (log.isDebugEnabled()) {
            log.debug("for compiler " + compiler.getOutputClassName());
        }

        ClassLoader classLoader = getClassLoader();

        ClassDescriptor superclass =
                ClassDescriptorHelper.getClassDescriptor(
                        symbolTable.getSuperclassName(), classLoader);
        // tchemit 2011-02-18 Be ware!!!
        // this is nearly impossible to obtain consturctors from symbol table.
        // let says for the moment this data is not available here and must be found in other places...
        // Anyway, the only reason of finding constructor is when we want to generate a jaxx file
        // If it inheritate from another jaxx file, we will mkae sure order is ok for this...

        List<MethodDescriptor> publicMethods = symbolTable.getScriptMethods();
        List<FieldDescriptor> publicFields = symbolTable.getScriptFields();
        //List<MethodDescriptor> declaredMethods = new ArrayList<MethodDescriptor>(publicMethods);
        //List<FieldDescriptor> declaredFields = new ArrayList<FieldDescriptor>(publicFields);
        publicMethods.removeIf(method -> !Modifier.isPublic(method.getModifiers()));
        publicFields.removeIf(field -> !Modifier.isPublic(field.getModifiers()));
        Set<String> interfaces = new HashSet<>();
        if (symbolTable.getInterfaces() != null) {
            // having interfaces
            for (String anInterface : symbolTable.getInterfaces()) {
                int genericIndex = anInterface.indexOf("<");
                if (genericIndex > -1) {
                    // remove generic type
                    anInterface = anInterface.substring(0, genericIndex);
                }
                if (log.isDebugEnabled()) {
                    log.debug("getting interface " + anInterface + " descriptor for class " + className);
                }
                interfaces.add(anInterface);
                ClassDescriptor interfaceclass = ClassDescriptorHelper.getClassDescriptor(anInterface, classLoader);
                publicMethods.addAll(Arrays.asList(interfaceclass.getMethodDescriptors()));
                publicFields.addAll(Arrays.asList(interfaceclass.getFieldDescriptors()));
            }
        }
        publicMethods.addAll(Arrays.asList(superclass.getMethodDescriptors()));
        publicFields.addAll(Arrays.asList(superclass.getFieldDescriptors()));
        int dotPos = className.lastIndexOf(".");
        String packageName = dotPos != -1 ? className.substring(0, dotPos) : null;

        ClassDescriptor[] superclassInterfaces = superclass.getInterfaces();
        for (ClassDescriptor superclassInterface : superclassInterfaces) {
            interfaces.add(superclassInterface.getName());
        }
        interfaces.add(JAXXObject.class.getName());
        return new JaxxFileClassDescriptor(
                compiler,
                className,
                packageName,
                interfaces,
                classLoader,
                new MethodDescriptor[0],
                publicMethods.toArray(new MethodDescriptor[publicMethods.size()]),
                publicFields.toArray(new FieldDescriptor[publicFields.size()])
        );
    }

    public class JaxxFileClassDescriptor extends ClassDescriptor {

        private final JAXXCompiler compiler;

        public JaxxFileClassDescriptor(JAXXCompiler compiler,
                                       String className,
                                       String packageName,
                                       Set<String> interfaces,
                                       ClassLoader classLoader,
                                       MethodDescriptor[] constructors,
                                       MethodDescriptor[] publicMethods,
                                       FieldDescriptor[] publicFields) {
            super(ClassDescriptorResolverFromJaxxFile.this.getResolverType(),
                  className,
                  packageName,
                  compiler.getSymbolTable().getSuperclassName(),
                  interfaces.toArray(new String[interfaces.size()]),
                  false,
                  false,
                  null,
                  null,
                  classLoader,
                  constructors,
                  publicMethods,
                  publicFields
            );
            this.compiler = compiler;
        }

        @Override
        public FieldDescriptor getDeclaredFieldDescriptor(String name) throws NoSuchFieldException {
            String type = compiler.getSymbolTable().getClassTagIds().get(name);
            if (type != null) {
                return new FieldDescriptor(name, Modifier.PROTECTED, type, compiler.getClassLoader());
            }
            throw new NoSuchFieldException(name);
        }

        @Override
        public Optional<MethodDescriptor> tryToGetDeclaredMethodDescriptor(String name, ClassDescriptor... parameterTypes) {
            return Optional.empty();
        }

        @Override
        public MethodDescriptor getDeclaredMethodDescriptor(String name, ClassDescriptor... parameterTypes) throws NoSuchMethodException {
            throw new NoSuchMethodException(name);
        }

        @Override
        public JAXXObjectDescriptor getJAXXObjectDescriptor() {
            return compiler.getJAXXObjectDescriptor();
        }
    }
}
