/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.runtime.swing.Table;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.awt.GridBagConstraints;
import java.io.IOException;

public class RowHandler implements TagHandler {

    public static final String TAG_NAME = "row";

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compileChildrenFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        if (!ClassDescriptorHelper.getClassDescriptor(Table.class).isAssignableFrom(compiler.getOpenComponent().getObjectClass())) {
            compiler.reportError("row tag may only appear within Table tag");
            return;
        }

        TableHandler.CompiledTable table = (TableHandler.CompiledTable) compiler.getOpenComponent();
        table.newRow();
        GridBagConstraints c = table.getRowConstraints();
        CellHandler.setAttributes(c, tag);
        compileChildrenSecondPass(tag, compiler);
    }

    public void compileChildrenFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                if (!child.getLocalName().equals(CellHandler.TAG_NAME)) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may only contain cell tags as children");
                }
                compileChildTagFirstPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    public void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                if (!child.getLocalName().equals(CellHandler.TAG_NAME)) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may only contain cell tags as children");
                }
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    protected void compileChildTagSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }
}
