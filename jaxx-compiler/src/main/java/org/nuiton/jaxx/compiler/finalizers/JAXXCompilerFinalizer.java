/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.finalizers;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaFile;

/**
 * Contract of any object to interact with a {@link JAXXCompiler} before the
 * generation pass.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public interface JAXXCompilerFinalizer {

    String TYPE_STRING = "String";

    String TYPE_VOID = "void";

    String TYPE_BOOLEAN = "boolean";

    String TYPE_OBJECT = "Object";

    /**
     * Test if the finalizer must be apply on the given {@code compiler}.
     *
     * @param compiler the compiler
     * @return {@code true} if the finalizer must be apply of compiler
     */
    boolean accept(JAXXCompiler compiler);

    /**
     * Finalize compiler for a given compiler on the finalizer pass before any
     * generation.
     *
     * @param root        the root object
     * @param compiler    the current compiler
     * @param javaFile    the java file to generate
     * @param packageName the package name of the file to generate
     * @param className   the class name of the file to generate
     * @throws Exception if any pb
     */
    void finalizeCompiler(CompiledObject root,
                          JAXXCompiler compiler,
                          JavaFile javaFile,
                          String packageName,
                          String className) throws Exception;

    /**
     * Prepare java file after any compiler finalizer pass, says the last
     * action before generation.
     *
     * @param root        the root object
     * @param compiler    the current compiler
     * @param javaFile    the java file to generate
     * @param packageName the package name of the file to generate
     * @param className   the class name of the file to generate
     * @throws Exception if any pb
     */
    void prepareJavaFile(CompiledObject root,
                         JAXXCompiler compiler,
                         JavaFile javaFile,
                         String packageName,
                         String className) throws Exception;
}
