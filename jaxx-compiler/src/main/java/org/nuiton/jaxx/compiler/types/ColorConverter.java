/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import com.google.auto.service.AutoService;

import java.awt.Color;
import java.lang.reflect.Field;

@AutoService(TypeConverter.class)
public class ColorConverter implements TypeConverter {

    @Override
    public Class<?>[] getSupportedTypes() {
        return new Class<?>[]{
                Color.class
        };
    }

    @Override
    public String getJavaCode(Object object) {
        Color color = (Color) object;
        return "new Color(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ")";
    }

    @Override
    public Object convertFromString(String string, Class<?> type) {
        if (!Color.class.equals(type)) {
            throw new IllegalArgumentException("unsupported type: " + type);
        }
        if (string.length() == 7 && string.charAt(0) == '#') {
            return new Color(Integer.parseInt(string.substring(1), 16));
        }
        try {
            Field color = Color.class.getField(string);
            return color.get(null);
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("colors must be of the form #xxxxxx ('#' followed by six hexadecimal digits), or the name of a constant field in java.awt.Color (found: '" + string + "')");
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
