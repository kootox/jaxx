/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* Generated By:JJTree&JavaCC: Do not edit this line. CSSParser.java */

package org.nuiton.jaxx.compiler.css.parser;

public class CSSParser/*@bgen(jjtree)*/ implements CSSParserTreeConstants, CSSParserConstants {/*@bgen(jjtree)*/

    protected final JJTCSSParserState jjtree = new JJTCSSParserState();

    public SimpleNode popNode() {
        if (jjtree.nodeArity() > 0)  // number of child nodes
            return (SimpleNode) jjtree.popNode();
        else
            return null;
    }

    void jjtreeOpenNodeScope(Node n) {
        ((SimpleNode) n).firstToken = getToken(1);
    }

    void jjtreeCloseNodeScope(Node n) {
        ((SimpleNode) n).lastToken = getToken(0);
    }

    public static void main(String args[]) {
        System.out.println("Reading from standard input...");
        CSSParser css = new CSSParser(System.in);
        try {
            SimpleNode n = css.Stylesheet();
            n.dump("");
            System.out.println("Thank you.");
        } catch (Exception e) {
            System.out.println("Oops.");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    final public SimpleNode Stylesheet() throws ParseException {
        /*@bgen(jjtree) Stylesheet */
        SimpleNode jjtn000 = new SimpleNode(JJTSTYLESHEET);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            label_1:
            while (true) {
                switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                    case IDENTIFIER:
                    case PSEUDOCLASS_COLON:
                    case 29:
                    case 30:
                    case 31:
                        break;
                    default:
                        jj_la1[0] = jj_gen;
                        break label_1;
                }
                Rule();
            }
            jjtree.closeNodeScope(jjtn000, true);
            jjtc000 = false;
            jjtreeCloseNodeScope(jjtn000);
            {
                if (true) return jjtn000;
            }
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
        throw new Error("Missing return statement in function");
    }

    final public void Rule() throws ParseException {
        /*@bgen(jjtree) Rule */
        SimpleNode jjtn000 = new SimpleNode(JJTRULE);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            Selectors();
            jj_consume_token(LEFT_BRACE);
            Declaration();
            label_2:
            while (true) {
                switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                    case SEMICOLON:
                        break;
                    default:
                        jj_la1[1] = jj_gen;
                        break label_2;
                }
                jj_consume_token(SEMICOLON);
                switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                    case IDENTIFIER:
                        Declaration();
                        break;
                    default:
                        jj_la1[2] = jj_gen;
                }
            }
            jj_consume_token(RIGHT_BRACE);
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Selectors() throws ParseException {
        /*@bgen(jjtree) Selectors */
        SimpleNode jjtn000 = new SimpleNode(JJTSELECTORS);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            Selector();
            label_3:
            while (true) {
                switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                    case 28:
                        break;
                    default:
                        jj_la1[3] = jj_gen;
                        break label_3;
                }
                jj_consume_token(28);
                Selector();
            }
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Selector() throws ParseException {
        /*@bgen(jjtree) Selector */
        SimpleNode jjtn000 = new SimpleNode(JJTSELECTOR);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case IDENTIFIER:
                case 29:
                    JavaClass();
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case 30:
                            Id();
                            break;
                        default:
                            jj_la1[4] = jj_gen;
                    }
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case 31:
                            Class();
                            break;
                        default:
                            jj_la1[5] = jj_gen;
                    }
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case PSEUDOCLASS_COLON:
                            PseudoClass();
                            break;
                        default:
                            jj_la1[6] = jj_gen;
                    }
                    break;
                case 30:
                    Id();
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case 31:
                            Class();
                            break;
                        default:
                            jj_la1[7] = jj_gen;
                    }
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case PSEUDOCLASS_COLON:
                            PseudoClass();
                            break;
                        default:
                            jj_la1[8] = jj_gen;
                    }
                    break;
                case 31:
                    Class();
                    switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                        case PSEUDOCLASS_COLON:
                            PseudoClass();
                            break;
                        default:
                            jj_la1[9] = jj_gen;
                    }
                    break;
                case PSEUDOCLASS_COLON:
                    PseudoClass();
                    break;
                default:
                    jj_la1[10] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void JavaClass() throws ParseException {
        /*@bgen(jjtree) JavaClass */
        SimpleNode jjtn000 = new SimpleNode(JJTJAVACLASS);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case IDENTIFIER:
                    jj_consume_token(IDENTIFIER);
                    break;
                case 29:
                    jj_consume_token(29);
                    break;
                default:
                    jj_la1[11] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Id() throws ParseException {
        /*@bgen(jjtree) Id */
        SimpleNode jjtn000 = new SimpleNode(JJTID);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(30);
            jj_consume_token(IDENTIFIER);
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Class() throws ParseException {
        /*@bgen(jjtree) Class */
        SimpleNode jjtn000 = new SimpleNode(JJTCLASS);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(31);
            jj_consume_token(IDENTIFIER);
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void PseudoClass() throws ParseException {
        /*@bgen(jjtree) PseudoClass */
        SimpleNode jjtn000 = new SimpleNode(JJTPSEUDOCLASS);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(PSEUDOCLASS_COLON);
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case PSEUDOCLASS_IDENTIFIER:
                    jj_consume_token(PSEUDOCLASS_IDENTIFIER);
                    break;
                case PROGRAMMATIC_PSEUDOCLASS:
                    jj_consume_token(PROGRAMMATIC_PSEUDOCLASS);
                    break;
                default:
                    jj_la1[12] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case 32:
                    AnimationProperties();
                    break;
                default:
                    jj_la1[13] = jj_gen;
            }
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void AnimationProperties() throws ParseException {
        /*@bgen(jjtree) AnimationProperties */
        SimpleNode jjtn000 = new SimpleNode(JJTANIMATIONPROPERTIES);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(32);
            AnimationProperty();
            label_4:
            while (true) {
                switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                    case 28:
                        break;
                    default:
                        jj_la1[14] = jj_gen;
                        break label_4;
                }
                jj_consume_token(28);
                AnimationProperty();
            }
            jj_consume_token(33);
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void AnimationProperty() throws ParseException {
        /*@bgen(jjtree) AnimationProperty */
        SimpleNode jjtn000 = new SimpleNode(JJTANIMATIONPROPERTY);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(IDENTIFIER);
            jj_consume_token(34);
            jj_consume_token(DECIMAL_LITERAL);
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case IDENTIFIER:
                    jj_consume_token(IDENTIFIER);
                    break;
                default:
                    jj_la1[15] = jj_gen;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Declaration() throws ParseException {
        /*@bgen(jjtree) Declaration */
        SimpleNode jjtn000 = new SimpleNode(JJTDECLARATION);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            Property();
            jj_consume_token(COLON);
            Expression();
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Property() throws ParseException {
        /*@bgen(jjtree) Property */
        SimpleNode jjtn000 = new SimpleNode(JJTPROPERTY);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(IDENTIFIER);
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Expression() throws ParseException {
        /*@bgen(jjtree) Expression */
        SimpleNode jjtn000 = new SimpleNode(JJTEXPRESSION);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            switch ((jj_ntk == -1) ? jj_ntk() : jj_ntk) {
                case DECIMAL_LITERAL:
                    jj_consume_token(DECIMAL_LITERAL);
                    break;
                case STRING:
                    jj_consume_token(STRING);
                    break;
                case IDENTIFIER:
                    jj_consume_token(IDENTIFIER);
                    break;
                case HEXCOLOR:
                    jj_consume_token(HEXCOLOR);
                    break;
                case EMS:
                    jj_consume_token(EMS);
                    break;
                case EXS:
                    jj_consume_token(EXS);
                    break;
                case LENGTH:
                    jj_consume_token(LENGTH);
                    break;
                case JAVA_CODE_START:
                    JavaCode();
                    break;
                default:
                    jj_la1[16] = jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } catch (Throwable jjte000) {
            if (jjtc000) {
                jjtree.clearNodeScope(jjtn000);
                jjtc000 = false;
            } else {
                jjtree.popNode();
            }
            if (jjte000 instanceof RuntimeException) {
                {
                    if (true) throw (RuntimeException) jjte000;
                }
            }
            if (jjte000 instanceof ParseException) {
                {
                    if (true) throw (ParseException) jjte000;
                }
            }
            {
                if (true) throw (Error) jjte000;
            }
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void JavaCode() throws ParseException {
        /*@bgen(jjtree) JavaCode */
        SimpleNode jjtn000 = new SimpleNode(JJTJAVACODE);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(JAVA_CODE_START);
            jj_consume_token(JAVA_CODE);
            jj_consume_token(JAVA_CODE_END);
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    final public void Identifier() throws ParseException {
        /*@bgen(jjtree) Identifier */
        SimpleNode jjtn000 = new SimpleNode(JJTIDENTIFIER);
        boolean jjtc000 = true;
        jjtree.openNodeScope(jjtn000);
        jjtreeOpenNodeScope(jjtn000);
        try {
            jj_consume_token(IDENTIFIER);
        } finally {
            if (jjtc000) {
                jjtree.closeNodeScope(jjtn000, true);
                jjtreeCloseNodeScope(jjtn000);
            }
        }
    }

    public CSSParserTokenManager token_source;

    SimpleCharStream jj_input_stream;

    public Token token, jj_nt;

    private int jj_ntk;

    private int jj_gen;

    final private int[] jj_la1 = new int[17];

    static private int[] jj_la1_0;

    static private int[] jj_la1_1;

    static {
        jj_la1_0();
        jj_la1_1();
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{0xe0002200, 0x8000, 0x200, 0x10000000, 0x40000000, 0x80000000, 0x2000, 0x80000000, 0x2000, 0x2000, 0xe0002200, 0x20000200, 0x201000, 0x0, 0x10000000, 0x200, 0xec40280,};
    }

    private static void jj_la1_1() {
        jj_la1_1 = new int[]{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0,};
    }

    public CSSParser(java.io.InputStream stream) {
        this(stream, null);
    }

    public CSSParser(java.io.InputStream stream, String encoding) {
        try {
            jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
        } catch (java.io.UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        token_source = new CSSParserTokenManager(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    public void ReInit(java.io.InputStream stream) {
        ReInit(stream, null);
    }

    public void ReInit(java.io.InputStream stream, String encoding) {
        try {
            jj_input_stream.ReInit(stream, encoding, 1, 1);
        } catch (java.io.UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        token_source.ReInit(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jjtree.reset();
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    public CSSParser(java.io.Reader stream) {
        jj_input_stream = new SimpleCharStream(stream, 1, 1);
        token_source = new CSSParserTokenManager(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    public void ReInit(java.io.Reader stream) {
        jj_input_stream.ReInit(stream, 1, 1);
        token_source.ReInit(jj_input_stream);
        token = new Token();
        jj_ntk = -1;
        jjtree.reset();
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    public CSSParser(CSSParserTokenManager tm) {
        token_source = tm;
        token = new Token();
        jj_ntk = -1;
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    public void ReInit(CSSParserTokenManager tm) {
        token_source = tm;
        token = new Token();
        jj_ntk = -1;
        jjtree.reset();
        jj_gen = 0;
        for (int i = 0; i < 17; i++) jj_la1[i] = -1;
    }

    final private Token jj_consume_token(int kind) throws ParseException {
        Token oldToken;
        if ((oldToken = token).next != null) token = token.next;
        else token = token.next = token_source.getNextToken();
        jj_ntk = -1;
        if (token.kind == kind) {
            jj_gen++;
            return token;
        }
        token = oldToken;
        jj_kind = kind;
        throw generateParseException();
    }

    final public Token getNextToken() {
        if (token.next != null) token = token.next;
        else token = token.next = token_source.getNextToken();
        jj_ntk = -1;
        jj_gen++;
        return token;
    }

    final public Token getToken(int index) {
        Token t = token;
        for (int i = 0; i < index; i++) {
            if (t.next != null) t = t.next;
            else t = t.next = token_source.getNextToken();
        }
        return t;
    }

    final private int jj_ntk() {
        if ((jj_nt = token.next) == null)
            return (jj_ntk = (token.next = token_source.getNextToken()).kind);
        else
            return (jj_ntk = jj_nt.kind);
    }

    private java.util.Vector jj_expentries = new java.util.Vector();

    private int[] jj_expentry;

    private int jj_kind = -1;

    public ParseException generateParseException() {
        Token errortok = token.next;
        int line = errortok.beginLine, column = errortok.beginColumn;
        String mess = (errortok.kind == 0) ? tokenImage[0] : errortok.image;
        return new ParseException("Parse error.  Encountered: " + mess, line, column);
    }

    final public void enable_tracing() {
    }

    final public void disable_tracing() {
    }

}
