/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.nuiton.jaxx.compiler.tags.TagHandler;

/**
 * Thrown by <code>TagHandler</code> when an unsupported attribute is encountered.
 *
 * @see TagHandler
 */
public class UnsupportedAttributeException extends CompilerException {

    private static final long serialVersionUID = -6919583037172920343L;

    /** Creates a new <code>UnsupportedAttributeException</code>. */
    public UnsupportedAttributeException() {
    }

    /**
     * Creates a new <code>UnsupportedAttributeException</code> with the specified detail message.
     *
     * @param msg the exception's detail message
     */
    public UnsupportedAttributeException(String msg) {
        super(msg);
    }

    /**
     * Creates a new <code>UnsupportedAttributeException</code> with the specified cause.
     *
     * @param initCause the exception's initCause
     */
    public UnsupportedAttributeException(Throwable initCause) {
        super(initCause);
    }

    /**
     * Creates a new <code>UnsupportedAttributeException</code> with the specified detail message and cause.
     *
     * @param msg       the exception's detail message
     * @param initCause the exception's initCause
     */
    public UnsupportedAttributeException(String msg, Throwable initCause) {
        super(msg, initCause);
    }
}
