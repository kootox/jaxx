/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;

import javax.swing.JToolBar;

public class JToolBarHandler extends DefaultComponentHandler {

    public static final String ATTRIBUTE_ORIENTATION = "orientation";

    enum Orientation {
        horizontal(JToolBar.HORIZONTAL),
        vertical(JToolBar.VERTICAL);

        private final int intValue;

        Orientation(int intValue) {
            this.intValue = intValue;
        }

        public int getIntValue() {
            return intValue;
        }
    }

    public JToolBarHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(),
                                                beanClass,
                                                JToolBar.class
        );
    }

    /**
     * Add support for <code>orientation="vertical"</code> and <code>orientation="horizontal"</code>.  These values should
     * have been supported without any special effort on my part, but JToolBar's BeanInfo doesn't contain the enum attribute
     * for the orientation property.
     */
    @Override
    protected int constantValue(String key, String value) {
        if (ATTRIBUTE_ORIENTATION.equals(key)) {
            value = value.trim().toLowerCase();
            Orientation fill = Orientation.valueOf(value);
            if (fill == null) {
                throw new CompilerException(
                        "invalid value for orientation attribute: '" + value +
                                "'");
            }
            return fill.getIntValue();
        }
        return super.constantValue(key, value);
    }
}
