/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.java;

/**
 * Represents an argument to a <code>JavaMethod</code>.
 *
 * @see JavaMethod
 */
public class JavaArgument extends JavaElement {

    private String type;

    private final boolean isFinal;

    /**
     * Creates a new <code>JavaArgument</code> with the specified name and type.  For example, the method <code>main()</code>
     * might have a <code>JavaArgument</code> with a name of <code>"arg"</code> and a type of <code>"java.lang.String[]"</code>.
     *
     * @param type the argument's type, as it would appear in Java source code
     * @param name the argument's name
     */
    JavaArgument(String type, String name) {
        this(type, name, false);
    }

    /**
     * Creates a new <code>JavaArgument</code> with the specified name, type, and finality.  For example, the method <code>main()</code>
     * might have a <code>JavaArgument</code> with a name of <code>"arg"</code> and a type of <code>"java.lang.String[]"</code>.  The
     * <code>isFinal</code> parameter allows the presence of the <code>final</code> keyword on the argument to be controlled.
     *
     * @param type    the argument's type, as it would appear in Java source code
     * @param name    the argument's name
     * @param isFinal <code>true</code> if the argument should be marked final
     */
    JavaArgument(String type, String name, boolean isFinal) {
        super(0, name);
        this.type = type;
        this.isFinal = isFinal;
    }

    /**
     * Returns the argument's type as it would be represented in Java source code.
     *
     * @return the argument's type
     */
    public String getType() {
        return type;
    }

    /**
     * Returns <code>true</code> if the <code>final</code> keyword should appear before the argument.
     *
     * @return <code>true</code> if the argument is final
     */
    public boolean isFinal() {
        return isFinal;
    }

    public void setType(String type) {
        this.type = type;
    }
}
