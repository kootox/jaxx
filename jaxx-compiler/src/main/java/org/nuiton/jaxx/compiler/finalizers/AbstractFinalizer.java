/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.finalizers;

import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaField;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.java.JavaMethod;

/**
 * Base implementation of a {@link JAXXCompilerFinalizer}.
 *
 * Contains commons methods and constants.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
public abstract class AbstractFinalizer implements JAXXCompilerFinalizer {

    /**
     * Clones the given {@code field} and adds it to the {@code file} as a
     * property via the method {@link JavaFile#addField(JavaField)}.
     *
     * @param file  the file where to add the cloned field
     * @param field the field to clone
     * @since 2.4
     */
    protected void addField(JavaFile file, JavaField field) {

        JavaField clonedField = JavaElementFactory.cloneField(field);
        file.addField(clonedField);
    }

    /**
     * Clones the given {@code method} and adds it to the {@code file} as a
     * simple method using the method {@link JavaFile#addMethod(JavaMethod)}.
     *
     * @param file  the file where to add the cloned field
     * @param field the field to clone
     * @since 2.4
     */
    protected void addSimpleField(JavaFile file, JavaField field) {

        JavaField clonedField = JavaElementFactory.cloneField(field);
        file.addSimpleField(clonedField);
    }

    /**
     * Clones the given {@code field} and adds it to the {@code file} as a
     * simple field using the method {@link JavaFile#addSimpleField(JavaField)}.
     *
     * @param file   the file where to add the cloned method
     * @param method the method to clone
     * @param types  optional types to use to simplify the body of the method
     * @since 2.4
     */
    protected void addMethod(JavaFile file, JavaMethod method, String... types) {

        JavaMethod clonedMethod = JavaElementFactory.cloneMethod(method);
        if (types.length > 0) {
            String body = clonedMethod.getBody();
            String simplifiedBody = file.simplifyCode(body, types);
            clonedMethod.setBody(simplifiedBody);
        }
        file.addMethod(clonedMethod);
    }

}
