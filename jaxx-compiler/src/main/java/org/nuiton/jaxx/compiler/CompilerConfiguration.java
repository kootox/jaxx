/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.spi.Initializer;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import java.io.File;
import java.net.URL;
import java.util.Map;

/**
 * Configuration of a compiler task.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public interface CompilerConfiguration {

    /**
     * Default css file extension.
     *
     * @since 2.28
     */
    String DEFAULT_CSS_EXTENSION = "jcss";

    /** @return the class loader used by compilers */
    ClassLoader getClassLoader();

    /** @return the type of compiler to use */
    Class<? extends JAXXCompiler> getCompilerClass();

    /** @return the type of default decorator to use if none specified */
    Class<? extends CompiledObjectDecorator> getDefaultDecoratorClass();

    /** @return the type of the default error UI to use for validation */
    Class<?> getDefaultErrorUI();

    /** @return extra imports to add on all generated jaxx object */
    String[] getExtraImports();

    /**
     * @return the FQN of the help broker (can not directly a type since
     * the help broker should (may?) be inside the sources.
     */
    String getHelpBrokerFQN();

    /**
     * @return the FQN of the validator factory used to instanciate new validators.
     * @since 2.6
     */
    String getValidatorFactoryFQN();


    /** @return the type of context to use in each generated jaxx object */
    Class<? extends JAXXContext> getJaxxContextClass();

    /**
     * Returns whether or not optimization should be performed.
     *
     * @return whether or not optimizations should be performed
     */
    boolean getOptimize();

    /**
     * Returns whether or not should recurse in css for existing JAXX Object.
     *
     * <b>Note:</b> This functionnality was here at the beginning of the project
     * but offers nothing very usefull, will be remove probably in JAXX 3.0.
     *
     * @return {@code true} if a css file with same name as jaxx file should
     * be included in jaxx file if found.
     * @since 2.0.2
     */
    boolean isAutoRecurseInCss();

    /**
     * Returns the target directory.
     *
     * @return the target directory
     */
    File getTargetDirectory();

    /** @return {@code true} if a logger must add on each generated jaxx object */
    boolean isAddLogger();

    /** @return {@code true} if we are generating help */
    boolean isGenerateHelp();

    /** @return {@code true} if we use the i18n system */
    boolean isI18nable();

    /** @return {@code true} to generate optimized code */
    boolean isOptimize();

    /** @return {@code true} if a profile pass must be done */
    boolean isProfile();

    /** @return {@code true} if states must be reset after the compilation */
    boolean isResetAfterCompile();

    /** @return {@code true} if UIManager is used to retrieve icons */
    boolean isUseUIManagerForIcon();

    /** @return {@code true} if compiler is verbose */
    boolean isVerbose();

    /**
     * @return {@code true} to trace the Class descriptor loading.
     * @since 2.4
     */
    boolean isShowClassDescriptorLoading();

    /**
     * @return {@code true} to detect and add {@link UIHandler} if found in class-path.
     * @since 2.6
     */
    boolean isAddAutoHandlerUI();

    /**
     * @param addAutoHandlerUI new value of the state
     * @since 2.6
     */
    void setAddAutoHandlerUI(boolean addAutoHandlerUI);

    /**
     * @return {@code true} to generate missing ids and style classes in the CSS files
     * @since 2.29
     */
    boolean isGenerateMissingIdsAndStyleClassesInCss();

    /**
     * @param generateMissingIdsAndStyleClassesInCss new value of the state
     * @since 2.29
     */
    void setGenerateMissingIdsAndStyleClassesInCss(boolean generateMissingIdsAndStyleClassesInCss);

    /** @return the encoding to use to write files */
    String getEncoding();

    Map<String, CompiledObjectDecorator> getDecorators();

    Map<String, JAXXCompilerFinalizer> getFinalizers();

    Map<String, Initializer> getInitializers();

    /**
     * @return the optional common css to inject in all JaxxCompiler.
     * @since 2.13
     */
    URL getCommonCss();

    /**
     * @return the extension of css files (by default use {@link #DEFAULT_CSS_EXTENSION} as before).
     * @since 2.28
     */
    String getCssExtension();

    File getI18nGettersDirectory();

    void setI18nGettersDirectory(File i18nGettersDirectory);
}
