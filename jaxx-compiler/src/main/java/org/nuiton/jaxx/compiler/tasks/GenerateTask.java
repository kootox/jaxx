/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;
import org.nuiton.jaxx.compiler.java.JavaArgument;
import org.nuiton.jaxx.compiler.java.JavaConstructor;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Last task to generate java files.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class GenerateTask extends JAXXEngineTask {

    /** Logger */
    private static final Log log = LogFactory.getLog(GenerateTask.class);

    /** Task name */
    public static final String TASK_NAME = "Generate";

    public GenerateTask() {
        super(TASK_NAME);
    }

    @Override
    public boolean perform(JAXXEngine engine) throws Exception {

        // check all files are attached to a compiler
        checkAllFilesCompiled(engine);

        boolean success = true;

        boolean verbose = engine.isVerbose();

        JavaFileGenerator generator =
                new JavaFileGenerator(JAXXCompiler.getLineSeparator(), verbose);

        JAXXCompilerFile[] files = engine.getCompiledFiles();

        for (JAXXCompilerFile jaxxFile : files) {
            if (verbose) {
                log.info("start " + jaxxFile.getClassName());
            }

            JAXXCompiler compiler = jaxxFile.getCompiler();

            addStartProfileTime(engine, compiler);

            try {
                consumeCompiler(compiler, generator);
            } finally {
                compiler.getJavaFile().clear();
            }
            addEndProfileTime(engine, compiler);
            if (compiler.isFailed()) {
                success = false;
            }
        }
        return success;
    }

    public void consumeCompiler(JAXXCompiler compiler,
                                JavaFileGenerator generator) throws IOException {

        JavaFile javaFile = compiler.getJavaFile();

        String packageName = javaFile.getPackageName();

        // optimize imports
        List<String> imports = optimizeImports(javaFile, packageName);

        String packageToExclude = packageName + ".*";

        // push back previous imports
        for (String importFQN : javaFile.getImports()) {
            if (!packageToExclude.equals(importFQN) &&
                    !imports.contains(importFQN)) {
                imports.add(importFQN);
            }
        }

        // set them to the file to generate
        javaFile.setImports(imports);


        // generate java file
        compiler.generate(generator);
    }

    public List<String> optimizeImports(JavaFile f,
                                        String packageName) {

        ImportsManager importsManager = f.getImportManager();

        List<String> result;

        for (String importFQN : f.getImports()) {
            if (log.isDebugEnabled()) {
                log.debug("Add import : " + importFQN);
            }
            importsManager.addImport(importFQN);
        }

        if (f.getSuperClass() != null) {
            String superClass = f.getSuperClass();
            try {
                String type = importsManager.getType(superClass);
                f.setSuperClass(type);
            } catch (Exception e) {
                log.error("file [" + f.getName() +
                                  "] Could not determine simple name of super class " +
                                  superClass);
            }
        }

        List<String> interfaces = f.getInterfaces();
        List<String> interfaces2 = new ArrayList<>(interfaces.size());
        for (String anInterface : interfaces) {
            try {
                String newInterface = importsManager.getType(anInterface);
                interfaces2.add(newInterface);
            } catch (Exception e) {
                log.error("file [" + f.getName() +
                                  "] Could not get interface " + anInterface + " :: " +
                                  e.getMessage());
                interfaces2.add(anInterface);
            }
        }
        f.setInterfaces(interfaces2);

        // optimize constructors parameters
        optimizeConstructorParameters(f, importsManager);
//        importsManager.addImport(JAXXUtil.class);
//        if (!f.isSuperclassIsJAXXObject() || needSwingUtil) {
//
//            // while implementing JAXXObject contract we sure need the
//            // SwingUtil class
//            importsManager.addImport(SwingUtil.class);
//        }
        result = importsManager.getImports(packageName);
        result.remove(packageName + ".*");
        if (log.isDebugEnabled()) {
            log.debug("[" + f.getName() + "] Detect " + result.size() +
                              " imports to add.");
            for (String s : result) {
                log.debug(" " + s);
            }
        }

        return result;
    }

    protected void optimizeConstructorParameters(JavaFile f,
                                                 ImportsManager importsManager) {

        List<JavaConstructor> constructors = f.getConstructors();
        for (JavaConstructor constructor : constructors) {
            JavaArgument[] arguments = constructor.getArguments();
            for (JavaArgument argument : arguments) {
                String type = argument.getType();
                try {
                    String simpleType = importsManager.getType(type);
                    argument.setType(simpleType);
                } catch (Exception e) {
                    log.error("file [" + f.getName() +
                                      "] Could not get simple type of constructor paramter  " + type + " :: " +
                                      e.getMessage());
                }

            }
        }
    }
}
