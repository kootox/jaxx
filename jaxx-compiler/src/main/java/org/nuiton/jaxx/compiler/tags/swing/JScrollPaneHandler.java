/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;

import javax.swing.JScrollPane;

public class JScrollPaneHandler extends DefaultComponentHandler {

    public static class JScrollPaneCompiledObject extends CompiledObject {

        boolean hasChild;

        boolean hasColumnViewHeader;

        public static final String COLUMN_HEADER_VIEW = "columnHeaderView";

        public JScrollPaneCompiledObject(String id,
                                         ClassDescriptor beanclass,
                                         JAXXCompiler compiler) {
            super(id, beanclass, compiler);
        }

        @Override
        public void addChild(CompiledObject child,
                             String constraints,
                             JAXXCompiler compiler) throws CompilerException {
            if (constraints != null) {
                compiler.reportError("JScrollPane does not accept constraints");
            }

            if (!hasChild) {
                // first child is always the view port component
                super.addChild(child, constraints, compiler);
                hasChild = true;
                return;
            }

            if (!hasColumnViewHeader) {

                // try to add a column view header
                String property =
                        (String) getProperties().get(COLUMN_HEADER_VIEW);

                if (log.isDebugEnabled()) {
                    log.info("property to match " + property + " against child " + child.getId());
                }
                if (!StringUtils.isEmpty(property) &&
                        ("{" + child.getId() + "}").equals(property)) {
                    hasColumnViewHeader = true;
                    return;
                }
            }

            compiler.reportError(
                    "JScrollPane may only have one child (found another child : " + child.getId() + ").");

        }
    }

    public JScrollPaneHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(),
                                                beanClass,
                                                JScrollPane.class
        );
    }

    @Override
    public CompiledObject createCompiledObject(String id,
                                               JAXXCompiler compiler) throws CompilerException {
        return new JScrollPaneCompiledObject(id, getBeanClass(), compiler);
    }
}
