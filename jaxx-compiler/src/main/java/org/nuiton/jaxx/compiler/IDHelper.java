/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * A usefull class to generate Ids.
 *
 * Created: 27 nov. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 */
public class IDHelper {
    /**
     * Logger
     */
    protected static final Log log = LogFactory.getLog(IDHelper.class);

    /**
     * Counter by type
     */
    protected final Map<String, Integer> autoGenIds = new TreeMap<>();

    /**
     * Maps of uniqued id for objects used in compiler
     */
    protected final Map<Object, String> uniqueIds = new HashMap<>();

    /**
     * Optimized Counter
     */
    protected int optimizedAutogenId = 0;

    /**
     * Flag to use optimized id
     */
    protected final boolean optimize;

    public IDHelper(boolean optimize) {
        this.optimize = optimize;
    }

    public String nextId(String name) {

        if (optimize) {
            return "$" + Integer.toString(optimizedAutogenId++, 36);
        }

        Integer integer = autoGenIds.get(name);

        if (integer == null) {
            integer = 0;
        }
        name = name.substring(name.lastIndexOf(".") + 1);
        String result = "$" + name + integer;
        autoGenIds.put(name, ++integer);
        if (log.isTraceEnabled()) {
            log.trace("new id = " + result);
        }
        return result;
    }

    public String getUniqueId(Object object) {
        String result = uniqueIds.computeIfAbsent(object, k -> "$u" + uniqueIds.size());
        if (log.isTraceEnabled()) {
            log.trace("new uniqueid = " + result);
        }
        return result;
    }

    public void clear() {
        autoGenIds.clear();
        uniqueIds.clear();
    }
}
