/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.spi.Initializer;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.UIManager;
import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.TreeMap;

/** Options of the {@link JAXXCompiler} and {@link JAXXEngine}. */
public class DefaultCompilerConfiguration implements CompilerConfiguration {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(DefaultCompilerConfiguration.class);

    /** where to generate */
    private File targetDirectory;

    /** flag to optimize generated code */
    private boolean optimize;

    /** verbose flag */
    private boolean verbose;

    /** to do a profile pass after generation */
    private boolean profile;

    /** a flag to enable or disable i18n generation */
    private boolean i18nable;

    /** a flag to add or not logger on generated jaxx files */
    private boolean addLogger;

    /** a flag to not reset compiler after a compile */
    private boolean resetAfterCompile;

    /**
     * the name of implementation of {@link JAXXContext}
     * to be used on {@link JAXXObject}.
     */
    protected Class<? extends JAXXContext> jaxxContextClass;

    /** list of fqn of class to import for all generated jaxx files */
    protected String[] extraImports;

    /** default error ui */
    private Class<?> defaultErrorUI;

    /** class loader to use in compiler */
    private ClassLoader classLoader = getClass().getClassLoader();

    /** the compiler class to use */
    private Class<? extends JAXXCompiler> compilerClass;

    /**
     * the default compiled object decorator to use if none specifed via
     * decorator attribute
     */
    private Class<? extends CompiledObjectDecorator> defaultDecoratorClass;

    /** a flag to use {@link UIManager} to retreave icons. */
    private boolean useUIManagerForIcon;

    /** a flag to generate javax help for any */
    private boolean generateHelp;

    /**
     * Fully qualified name of help broker, can not use a class here
     * since this class should be in sources (so not yet compiled)
     */
    private String helpBrokerFQN;

    /**
     * Fully qualified name of validator factory.
     *
     * @since 2.6
     */
    private String validatorFactoryFQN;

    /** Encoding to use to write files */
    private String encoding;

    private boolean autoRecurseInCss;

    private File i18nGettersDirectory;

    /** decorators available in engine */
    protected Map<String, CompiledObjectDecorator> decorators;

    /** finalizers available in engine */
    protected Map<String, JAXXCompilerFinalizer> finalizers;

    /** initializes availables */
    protected Map<String, Initializer> initializers;

    /**
     * To trace class descriptor loading.
     *
     * @since 2.4
     */
    private boolean showClassDescriptorLoading;

    /**
     * To detect {@link UIHandler} if found.
     *
     * @since 2.6
     */
    private boolean addAutoHandlerUI;

    /**
     * To generate missing ids and style classes in the CSS files
     *
     * @since 2.29
     */
    private boolean generateMissingIdsAndStyleClassesInCss;

    /**
     * Optional common css.
     *
     * @since 2.13
     */
    private URL commonCss;

    @Override
    public File getTargetDirectory() {
        return targetDirectory;
    }

    @Override
    public boolean getOptimize() {
        return optimize;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public boolean isShowClassDescriptorLoading() {
        return showClassDescriptorLoading;
    }

    @Override
    public boolean isAddAutoHandlerUI() {
        return addAutoHandlerUI;
    }

    @Override
    public void setAddAutoHandlerUI(boolean addAutoHandlerUI) {
        this.addAutoHandlerUI = addAutoHandlerUI;
    }

    @Override
    public boolean isGenerateMissingIdsAndStyleClassesInCss() {
        return generateMissingIdsAndStyleClassesInCss;
    }

    @Override
    public void setGenerateMissingIdsAndStyleClassesInCss(boolean generateMissingIdsAndStyleClassesInCss) {
        this.generateMissingIdsAndStyleClassesInCss = generateMissingIdsAndStyleClassesInCss;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public boolean isI18nable() {
        return i18nable;
    }

    @Override
    public boolean isUseUIManagerForIcon() {
        return useUIManagerForIcon;
    }

    @Override
    public boolean isAddLogger() {
        return addLogger;
    }

    @Override
    public Class<? extends JAXXContext> getJaxxContextClass() {
        return jaxxContextClass;
    }

    @Override
    public String[] getExtraImports() {
        return extraImports;
    }

    @Override
    public boolean isResetAfterCompile() {
        return resetAfterCompile;
    }

    @Override
    public boolean isOptimize() {
        return optimize;
    }

    @Override
    public Class<?> getDefaultErrorUI() {
        return defaultErrorUI;
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    @Override
    public Class<? extends JAXXCompiler> getCompilerClass() {
        return compilerClass;
    }

    @Override
    public Class<? extends CompiledObjectDecorator> getDefaultDecoratorClass() {
        return defaultDecoratorClass;
    }

    @Override
    public boolean isProfile() {
        return profile;
    }

    @Override
    public boolean isGenerateHelp() {
        return generateHelp;
    }

    @Override
    public String getHelpBrokerFQN() {
        return helpBrokerFQN;
    }

    @Override
    public String getValidatorFactoryFQN() {
        return validatorFactoryFQN;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public Map<String, CompiledObjectDecorator> getDecorators() {
        if (decorators == null) {
            decorators = new TreeMap<>();
            ClassLoader classloader =
                    Thread.currentThread().getContextClassLoader();
            if (log.isInfoEnabled()) {
                log.info("with cl " + classloader);
            }

            // load decorators
            ServiceLoader<CompiledObjectDecorator> services =
                    ServiceLoader.load(CompiledObjectDecorator.class,
                                       classloader);
            for (CompiledObjectDecorator decorator : services) {
                if (log.isInfoEnabled()) {
                    log.info("detected " + decorator);
                }
                decorators.put(decorator.getName(), decorator);
            }
        }
        return decorators;
    }

    @Override
    public Map<String, JAXXCompilerFinalizer> getFinalizers() {
        if (finalizers == null) {
            finalizers = new TreeMap<>();

            ClassLoader classloader =
                    Thread.currentThread().getContextClassLoader();
            if (log.isInfoEnabled()) {
                log.info("with cl " + classloader);
            }

            ServiceLoader<JAXXCompilerFinalizer> services =
                    ServiceLoader.load(JAXXCompilerFinalizer.class,
                                       classloader);
            for (JAXXCompilerFinalizer finalizer : services) {
                if (log.isInfoEnabled()) {
                    log.info("detected " + finalizer);
                }
                finalizers.put(finalizer.getClass().getName(), finalizer);
            }
        }
        return finalizers;
    }

    @Override
    public Map<String, Initializer> getInitializers() {
        if (initializers == null) {
            initializers = new TreeMap<>();
            ClassLoader classloader =
                    Thread.currentThread().getContextClassLoader();
            if (log.isInfoEnabled()) {
                log.info("with cl " + classloader);
            }

            ServiceLoader<Initializer> loader =
                    ServiceLoader.load(Initializer.class, classloader);

            for (Initializer initializer : loader) {
                if (log.isInfoEnabled()) {
                    log.info("detected " + initializer);
                }
                initializers.put(initializer.getClass().getName(), initializer);
            }
        }
        return initializers;
    }

    @Override
    public URL getCommonCss() {
        return commonCss;
    }

    @Override
    public String getCssExtension() {
        return DEFAULT_CSS_EXTENSION;
    }

    public void setCommonCss(URL commonCss) {
        this.commonCss = commonCss;
    }

    @Override
    public boolean isAutoRecurseInCss() {
        return autoRecurseInCss;
    }

    @Override
    public File getI18nGettersDirectory() {
        return i18nGettersDirectory;
    }

    @Override
    public void setI18nGettersDirectory(File i18nGettersDirectory) {
        this.i18nGettersDirectory = i18nGettersDirectory;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(
                this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
