/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;

/**
 * Mirrors the class <code>java.beans.PropertyDescriptor</code>.  JAXX uses its own introspector rather than the built-in
 * <code>java.beans.Introspector</code> so that it can introspect {@link ClassDescriptor},
 * not just <code>java.lang.Class</code>.
 */
public class JAXXPropertyDescriptor extends JAXXFeatureDescriptor {

    private ClassDescriptor propertyType;

    private MethodDescriptor readMethod;

    private MethodDescriptor writeMethod;

    private boolean bound;

    public JAXXPropertyDescriptor(ClassDescriptor classDescriptor, String propertyName) {
        this(classDescriptor, propertyName, null, null);
    }

    public JAXXPropertyDescriptor(ClassDescriptor classDescriptor, String propertyName,
                                  MethodDescriptor readMethod, MethodDescriptor writeMethod) {
        this(classDescriptor, propertyName, readMethod, writeMethod, false);
    }

    public JAXXPropertyDescriptor(ClassDescriptor classDescriptor, String propertyName,
                                  MethodDescriptor readMethod, MethodDescriptor writeMethod,
                                  boolean bound) {
        super(classDescriptor, propertyName);
        this.readMethod = readMethod;
        this.writeMethod = writeMethod;
        this.bound = bound;
    }

    public MethodDescriptor getReadMethodDescriptor() {
        if (readMethod == null) {
            try {
                readMethod = getClassDescriptor().getMethodDescriptor("get" + capitalize(getName()));
            } catch (NoSuchMethodException e) {
                try {
                    readMethod = getClassDescriptor().getMethodDescriptor("is" + capitalize(getName()));
                } catch (NoSuchMethodException e2) {
                }
            }
        }
        return readMethod;
    }

    public MethodDescriptor getWriteMethodDescriptor() {
        if (writeMethod == null) {
            try {
                String methodName = "set" + capitalize(getName());
                MethodDescriptor read = getReadMethodDescriptor();
                if (read != null) {
                    writeMethod = getClassDescriptor().getMethodDescriptor(methodName, read.getReturnType());
                } else {
                    throw new CompilerException("Internal error: requesting 'set' method for property of unknown type: '" + getName() + "' (in " + getClassDescriptor() + ")");
                }
            } catch (NoSuchMethodException e) {
            }
        }
        return writeMethod;
    }

    public ClassDescriptor getPropertyType() {
        if (propertyType == null) {
            MethodDescriptor read = getReadMethodDescriptor();
            if (read != null) {
                propertyType = read.getReturnType();
            } else {
                MethodDescriptor write = getWriteMethodDescriptor();
                propertyType = write.getParameterTypes()[0];
            }
        }
        return propertyType;
    }

    public boolean isBound() {
        return bound;
    }

    public void setBound(boolean bound) {
        this.bound = bound;
    }
}
