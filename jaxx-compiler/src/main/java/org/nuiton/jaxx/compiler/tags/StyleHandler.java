/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.css.StylesheetHelper;
import org.nuiton.jaxx.runtime.css.Stylesheet;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

/**
 * Handles the <code>&lt;style&gt;</code> tag.
 *
 * @author Ethan Nicholas
 */
public class StyleHandler implements TagHandler {

    public static final String TAG_NAME = "style";

    public static final String SOURCE_ATTRIBUTE = "source";

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException {
        boolean source = false;
        NamedNodeMap attributes = tag.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Attr attribute = (Attr) attributes.item(i);
            String name = attribute.getName();
            String attrValue = attribute.getValue();
            if (name.equals(SOURCE_ATTRIBUTE)) {
                source = true;
                File styleFile = new File(compiler.getBaseDir(), attrValue.replace('/', File.separatorChar));
                if (!styleFile.exists()) {
                    compiler.reportError("stylesheet file not found: " + styleFile);
                } else {
                    compiler.registerStyleSheetFile(styleFile, true);
                }
            } else if (!name.startsWith(XMLNS_ATTRIBUTE) &&
                    !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                throw new UnsupportedAttributeException(name);
            }
        }

        StringBuilder style = new StringBuilder();
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            switch (child.getNodeType()) {
                case Node.ELEMENT_NODE:
                    compiler.reportError("<style> tag may not contain child elements: " + tag);
                case Node.TEXT_NODE: // fall through
                case Node.CDATA_SECTION_NODE:
                    style.append(((Text) child).getData());
            }
        }

        String styleString = style.toString().trim();
        if (styleString.length() > 0) {
            if (source) {
                compiler.reportError("<style> tag has both a source attribute and an inline stylesheet");
            }
            try {
                Stylesheet stylesheet = StylesheetHelper.processStylesheet(style.toString());
                compiler.registerStylesheet(stylesheet);
            } catch (CompilerException e) {
                String message = "Inline Css content is not valid";
                if (e instanceof org.nuiton.jaxx.compiler.css.parser.ParseException) {
                    org.nuiton.jaxx.compiler.css.parser.ParseException parseException = (org.nuiton.jaxx.compiler.css.parser.ParseException) e;

                    message += " (line: " + parseException.getLine() + " - col:" + parseException.getColumn() + ") ";
                }
                compiler.reportError(message, e);
            }
        }
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException {
    }

}
