/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* Generated By:JJTree: Do not edit this line. .\CSSParserTreeConstants.java */


package org.nuiton.jaxx.compiler.css.parser;

public interface CSSParserTreeConstants {
    int JJTSTYLESHEET = 0;

    int JJTRULE = 1;

    int JJTSELECTORS = 2;

    int JJTSELECTOR = 3;

    int JJTJAVACLASS = 4;

    int JJTID = 5;

    int JJTCLASS = 6;

    int JJTPSEUDOCLASS = 7;

    int JJTANIMATIONPROPERTIES = 8;

    int JJTANIMATIONPROPERTY = 9;

    int JJTDECLARATION = 10;

    int JJTPROPERTY = 11;

    int JJTEXPRESSION = 12;

    int JJTJAVACODE = 13;

    int JJTIDENTIFIER = 14;


    String[] jjtNodeName = {
            "Stylesheet",
            "Rule",
            "Selectors",
            "Selector",
            "JavaClass",
            "Id",
            "Class",
            "PseudoClass",
            "AnimationProperties",
            "AnimationProperty",
            "Declaration",
            "Property",
            "Expression",
            "JavaCode",
            "Identifier",
    };
}
