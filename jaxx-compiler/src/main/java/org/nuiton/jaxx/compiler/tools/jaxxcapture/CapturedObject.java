/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tools.jaxxcapture;

import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.tools.jaxxcapture.handlers.ObjectHandler;

import java.awt.Component;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CapturedObject extends AbstractContextNode {
    private final String className;

    private final ObjectHandler handler;

    /** Maps children to their constraints. */
    private final Map<CapturedObject, ContextNode> children = new LinkedHashMap<>();

    private CapturedObject parent;

    private final Map<String, String> properties = new LinkedHashMap<>();

    private final Map<String, Object> additionalData = new HashMap<>();

    private final StringBuilder innerXML = new StringBuilder();

    private final StringBuilder script = new StringBuilder();

    private boolean inlineable = true;

    private final JAXXCapture capture;

    public CapturedObject(ObjectHandler handler, String className, JAXXCapture capture) {
        this.handler = handler;
        this.className = className;
        this.capture = capture;
    }


    public ObjectHandler getObjectHandler() {
        return handler;
    }


    public void addChild(CapturedObject child, ContextNode constraints) {
        children.put(child, constraints);
        child.setParent(this);
    }


    public CapturedObject[] getChildren() {
        return children.keySet().toArray(new CapturedObject[children.size()]);
    }


    public CapturedObject getParent() {
        return parent;
    }


    public void setParent(CapturedObject parent) {
        this.parent = parent;
    }


    public ContextNode getConstraints(CapturedObject child) {
        return children.get(child);
    }


    public String getClassName() {
        return className;
    }


    public String getProperty(String key) {
        return properties.get(key);
    }


    public void setProperty(String key, String value) {
        properties.put(key, value);
    }


    public Map<String, String> getProperties() {
        return properties;
    }


    public Object getAdditionalData(String key) {
        return additionalData.get(key);
    }


    public void setAdditionalData(String key, Object value) {
        additionalData.put(key, value);
    }


    public Map<String, Object> getAdditionalData() {
        return additionalData;
    }


    public void setInlineable(boolean inlineable) {
        this.inlineable = inlineable;
    }


    public boolean isInlineable() {
        try {
            return script.length() == 0 && !Component.class.isAssignableFrom(Class.forName(className, true, capture.getClassLoader())) && inlineable;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    public void appendInnerXML(String xml) {
        if (this.innerXML.length() > 0) {
            this.innerXML.append(JAXXCompiler.getLineSeparator());
        }
        this.innerXML.append(xml);
    }


    public String getInnerXML() {
        return innerXML.toString();
    }


    public void appendScriptCode(String script) {
        if (this.script.length() > 0) {
            this.script.append(JAXXCompiler.getLineSeparator());
        }
        this.script.append(script);
    }


    public String getScriptCode() {
        return script.toString();
    }


    public String getXML(JAXXCapture capture) {
        return getObjectHandler().getXML(this, capture);
    }

    @Override
    public String toString() {
        return "CapturedObject[" + getProperty("id") + ", " + className + "]";
    }
}
