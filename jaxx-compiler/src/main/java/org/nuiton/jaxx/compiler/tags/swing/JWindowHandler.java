/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.w3c.dom.Element;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JWindow;
import java.io.IOException;
import java.util.Map;

public class JWindowHandler extends DefaultComponentHandler {

    public JWindowHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JWindow.class, JFrame.class, JDialog.class);
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledObject(id, getBeanClass(), compiler) {

            @Override
            public void addChild(CompiledObject child, String constraints, JAXXCompiler compiler) throws CompilerException {
                if (ClassDescriptorHelper.getClassDescriptor(JMenuBar.class).isAssignableFrom(child.getObjectClass())) {
                    appendAdditionCode(getId() + ".setJMenuBar(" + child.getId() + ");");
                } else {
                    super.addChild(child, constraints, compiler);
                }
            }
        };
    }

    @Override
    protected void openComponent(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        if (compiler.getOpenComponent() != null) {
            compiler.openInvisibleComponent(object);
        } else {
            super.openComponent(object, tag, compiler);
        }
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileSecondPass(tag, compiler);
        CompiledObject object = objectMap.get(tag);
        Map<?, ?> properties = object.getProperties();
        if (!properties.containsKey("width") && !properties.containsKey("height")) {
            compiler.appendLateInitializer(object.getId() + ".pack();\n");
        }
    }
}
