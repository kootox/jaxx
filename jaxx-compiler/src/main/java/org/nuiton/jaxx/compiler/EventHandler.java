/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;

public class EventHandler {

    private final String eventId;

    private final String objectCode;

    private final ClassDescriptor listenerClass;

    private final MethodDescriptor addMethod;

    private final MethodDescriptor listenerMethod;

    private final String javaCode;

    public EventHandler(String eventId,
                        String objectCode,
                        MethodDescriptor addMethod,
                        ClassDescriptor listenerClass,
                        MethodDescriptor listenerMethod,
                        String javaCode) {
        this.eventId = eventId;
        this.objectCode = objectCode;
        this.addMethod = addMethod;
        this.listenerClass = listenerClass;
        this.listenerMethod = listenerMethod;
        this.javaCode = javaCode;
    }

    public String getEventId() {
        return eventId;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public MethodDescriptor getAddMethod() {
        return addMethod;
    }

    public ClassDescriptor getListenerClass() {
        return listenerClass;
    }

    public MethodDescriptor getListenerMethod() {
        return listenerMethod;
    }

    public String getJavaCode() {
        return javaCode;
    }

    @Override
    public String toString() {
        return "EventHandler[" + eventId + ", " +
                listenerClass.getName() + ", " +
                objectCode + ", " + javaCode + "]";
    }
}
