/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Symbol table constructed during the first pass of compilation. */
public class SymbolTable {

    private String superclass;
    // maps ID strings to class names -- we can't map directly to CompiledObjects, because we
    // can't create those until after the first pass

    private final Map<String, String> ids = new HashMap<>();

    private final List<FieldDescriptor> scriptFields = new ArrayList<>();

    private final List<MethodDescriptor> scriptMethods = new ArrayList<>();

    private String[] interfaces;

    /** @return the fully-qualified name of the superclass of the class described by this symbol table. */
    public String getSuperclassName() {
        return superclass;
    }

    public String[] getInterfaces() {
        return interfaces;
    }

    public void setSuperclassName(String superclass) {
        this.superclass = superclass;
    }

    /**
     * @return a map of IDs to class names.  Each entry in the map corresponds to a class tag with an
     * <code>id</code> attribute.  The <code>id</code> is the key, and the fully-qualified class name
     * of the tag is the value.
     */
    public Map<String, String> getClassTagIds() {
        return ids;
    }

    /** @return a list of <code>FieldDescriptors</code> for fields defined in &lt;script&gt; tags. */
    public List<FieldDescriptor> getScriptFields() {
        return scriptFields;
    }

    /** @return a list of <code>MethodDescriptors</code> for methods defined in &lt;script&gt; tags. */
    public List<MethodDescriptor> getScriptMethods() {
        return scriptMethods;
    }

    public void setInterfaces(String[] interfaces) {
        this.interfaces = interfaces;
    }

    public void clear() {
        ids.clear();
        scriptFields.clear();
        scriptMethods.clear();
    }
}
