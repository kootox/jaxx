/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.spi;

import javax.swing.JButton;
import javax.swing.JMenuItem;

import com.google.auto.service.AutoService;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.beans.BeanInfoUtil;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.tags.ImportHandler;
import org.nuiton.jaxx.compiler.tags.ScriptHandler;
import org.nuiton.jaxx.compiler.tags.StyleHandler;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.compiler.tags.TagManager;
import org.nuiton.jaxx.compiler.tags.swing.AbstractButtonHandler;
import org.nuiton.jaxx.compiler.tags.swing.ApplicationHandler;
import org.nuiton.jaxx.compiler.tags.swing.CellHandler;
import org.nuiton.jaxx.compiler.tags.swing.ItemHandler;
import org.nuiton.jaxx.compiler.tags.swing.JAXXComboBoxHandler;
import org.nuiton.jaxx.compiler.tags.swing.JAXXListHandler;
import org.nuiton.jaxx.compiler.tags.swing.JAXXTabHandler;
import org.nuiton.jaxx.compiler.tags.swing.JAXXTreeHandler;
import org.nuiton.jaxx.compiler.tags.swing.JCheckBoxHandler;
import org.nuiton.jaxx.compiler.tags.swing.JComboBoxHandler;
import org.nuiton.jaxx.compiler.tags.swing.JInternalFrameHandler;
import org.nuiton.jaxx.compiler.tags.swing.JListHandler;
import org.nuiton.jaxx.compiler.tags.swing.JMenuHandler;
import org.nuiton.jaxx.compiler.tags.swing.JPasswordFieldHandler;
import org.nuiton.jaxx.compiler.tags.swing.JPopupMenuHandler;
import org.nuiton.jaxx.compiler.tags.swing.JProgressBarHandler;
import org.nuiton.jaxx.compiler.tags.swing.JRadioButtonHandler;
import org.nuiton.jaxx.compiler.tags.swing.JScrollPaneHandler;
import org.nuiton.jaxx.compiler.tags.swing.JSliderHandler;
import org.nuiton.jaxx.compiler.tags.swing.JSpinnerHandler;
import org.nuiton.jaxx.compiler.tags.swing.JSplitPaneHandler;
import org.nuiton.jaxx.compiler.tags.swing.JTabbedPaneHandler;
import org.nuiton.jaxx.compiler.tags.swing.JTextComponentHandler;
import org.nuiton.jaxx.compiler.tags.swing.JToolBarHandler;
import org.nuiton.jaxx.compiler.tags.swing.JTreeHandler;
import org.nuiton.jaxx.compiler.tags.swing.JWindowHandler;
import org.nuiton.jaxx.compiler.tags.swing.JXLayerHandler;
import org.nuiton.jaxx.compiler.tags.swing.RowHandler;
import org.nuiton.jaxx.compiler.tags.swing.TabHandler;
import org.nuiton.jaxx.compiler.tags.swing.TableHandler;
import org.nuiton.jaxx.compiler.tags.validator.BeanValidatorHandler;
import org.nuiton.jaxx.compiler.tags.validator.ExcludeFieldValidatorHandler;
import org.nuiton.jaxx.compiler.tags.validator.FieldValidatorHandler;
import org.nuiton.jaxx.runtime.swing.Application;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.runtime.swing.JAXXComboBox;
import org.nuiton.jaxx.runtime.swing.JAXXList;
import org.nuiton.jaxx.runtime.swing.JAXXTab;
import org.nuiton.jaxx.runtime.swing.JAXXTree;
import org.nuiton.jaxx.runtime.swing.Table;
import org.nuiton.jaxx.runtime.swing.editor.EnumEditor;
import org.nuiton.jaxx.runtime.swing.editor.LocaleEditor;
import org.nuiton.jaxx.validator.swing.SwingValidator;

import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.JWindow;
import javax.swing.text.JTextComponent;
import java.awt.Component;

/**
 * Initializes support provided from JAXX (java, swing and validation).
 *
 * @plexus.component role-hint="default" role="org.nuiton.jaxx.compiler.spi.Initializer"
 */
@AutoService(Initializer.class)
public class DefaultInitializer implements Initializer {

    @Override
    public void initialize() {

        BeanInfoUtil.addJaxxBeanInfoPath("org.nuiton.jaxx.runtime.swing");

        registerDefaultNamespace("javax.swing.*",
                                 JEditorPane.class,
                                 JFormattedTextField.class,
                                 JPasswordField.class,
                                 JTextArea.class,
                                 JTextField.class,
                                 JTextPane.class
        );

        //
        // Register tags
        //

        registerTag(JAXXCompiler.JAXX_NAMESPACE, ScriptHandler.TAG_NAME, new ScriptHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, StyleHandler.TAG_NAME, new StyleHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, ImportHandler.TAG_NAME, new ImportHandler());

        //TC-20091005 why the namespace was on awt ? ButtonGroup is from javax.swing
        registerTag("javax.swing.*", "ButtonGroup", new DefaultObjectHandler(ClassDescriptorHelper.getClassDescriptor(JAXXButtonGroup.class)));

        registerTag(JAXXCompiler.JAXX_NAMESPACE, TabHandler.TAG_NAME, new TabHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, RowHandler.TAG_NAME, new RowHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, CellHandler.TAG_NAME, new CellHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, ItemHandler.TAG_NAME, new ItemHandler());


        registerTag(JAXXCompiler.JAXX_NAMESPACE, BeanValidatorHandler.TAG, new BeanValidatorHandler(ClassDescriptorHelper.getClassDescriptor(SwingValidator.class)));
        registerTag(JAXXCompiler.JAXX_NAMESPACE, FieldValidatorHandler.TAG, new FieldValidatorHandler());
        registerTag(JAXXCompiler.JAXX_NAMESPACE, ExcludeFieldValidatorHandler.TAG, new FieldValidatorHandler());

        //
        // Register beans
        //

        registerBean(Object.class, DefaultObjectHandler.class);
        registerBean(Component.class, DefaultComponentHandler.class);

        // check boxes
        registerBean(JCheckBox.class, JCheckBoxHandler.class);
        registerBean(JCheckBoxMenuItem.class, JCheckBoxHandler.class);

        // combo boxes
        registerBean(JComboBox.class, JComboBoxHandler.class);
        registerBean(EnumEditor.class, JComboBoxHandler.class);
        registerBean(LocaleEditor.class, JComboBoxHandler.class);
        registerBean(JAXXComboBox.class, JAXXComboBoxHandler.class);

        // radio boxes
        registerBean(JRadioButton.class, JRadioButtonHandler.class);
        registerBean(JRadioButtonMenuItem.class, JRadioButtonHandler.class);
        registerBean(JToggleButton.class, JRadioButtonHandler.class);

        // buttons
        registerBean(JButton.class, AbstractButtonHandler.class);
        registerBean(JMenuItem.class, AbstractButtonHandler.class);

        // Lists
        registerBean(JList.class, JListHandler.class);
        registerBean(JAXXList.class, JAXXListHandler.class);

        // Trees
        registerBean(JTree.class, JTreeHandler.class);
        registerBean(JAXXTree.class, JAXXTreeHandler.class);

        // Windows
        registerBean(JDialog.class, JWindowHandler.class);
        registerBean(JFrame.class, JWindowHandler.class);
        registerBean(JWindow.class, JWindowHandler.class);

        // JXLayer
        registerBean(JXLayer.class, JXLayerHandler.class);

        registerBean(Application.class, ApplicationHandler.class);

        registerBean(JInternalFrame.class, JInternalFrameHandler.class);
        registerBean(JMenu.class, JMenuHandler.class);
        registerBean(JPasswordField.class, JPasswordFieldHandler.class);
        registerBean(JPopupMenu.class, JPopupMenuHandler.class);
        registerBean(JProgressBar.class, JProgressBarHandler.class);
        registerBean(JScrollPane.class, JScrollPaneHandler.class);
        registerBean(JSlider.class, JSliderHandler.class);
        registerBean(JSpinner.class, JSpinnerHandler.class);
        registerBean(JSplitPane.class, JSplitPaneHandler.class);
        registerBean(JTabbedPane.class, JTabbedPaneHandler.class);
        registerBean(JTextComponent.class, JTextComponentHandler.class);
        registerBean(JToolBar.class, JToolBarHandler.class);

        registerBean(Table.class, TableHandler.class);
        registerBean(JAXXTab.class, JAXXTabHandler.class);

        registerBean(SwingValidator.class, BeanValidatorHandler.class);

    }

    protected void registerBean(Class<?> beanClass, Class<? extends TagHandler> handlerClass) {
        ClassDescriptor classDescriptor = ClassDescriptorHelper.getClassDescriptor(beanClass);

        TagManager.registerBean(classDescriptor, handlerClass);
    }

    protected void registerTag(String namespace, String tagName, TagHandler handler) {
        TagManager.registerTag(namespace, tagName, handler);
    }

    protected void registerDefaultNamespace(String namespace, Class<?>... beanClass) {

        for (Class<?> c : beanClass) {
            TagManager.registerDefaultNamespace(c.getSimpleName(), namespace);
        }
    }
}
