/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import javax.swing.JTextArea;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

public class JTextComponentHandler extends DefaultComponentHandler {

    private static final int DEFAULT_COLUMNS = 15;

    public static final String ATTRIBUTE_LINE_WRAP = "lineWrap";

    public static final String ATTRIBUTE_WRAP_STYLE_WORD = "wrapStyleWord";

    public static final String ATTRIBUTE_COLUMNS = "columns";

    public static final String ATTRIBUTE_TEXT = "text";

    public JTextComponentHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(),
                                                beanClass,
                                                JTextComponent.class);
    }

    @Override
    protected void setDefaults(CompiledObject object,
                               Element tag,
                               JAXXCompiler compiler) throws CompilerException {
        super.setDefaults(object, tag, compiler);
        try {
            object.getObjectClass().getMethodDescriptor("setColumns",
                                                        ClassDescriptorHelper.getClassDescriptor(int.class)
            );
            setAttribute(object,
                         ATTRIBUTE_COLUMNS,
                         String.valueOf(DEFAULT_COLUMNS),
                         false,
                         compiler
            );
        } catch (NoSuchMethodException e) {
            // ignore ?
        }

        if (ClassDescriptorHelper.getClassDescriptor(JTextArea.class).isAssignableFrom(object.getObjectClass())) {
            setAttribute(object, ATTRIBUTE_LINE_WRAP, "true", false, compiler);
            setAttribute(object, ATTRIBUTE_WRAP_STYLE_WORD, "true", false, compiler);
        }
    }

    @Override
    public String getSetPropertyCode(String id,
                                     String name,
                                     String valueCode,
                                     JAXXCompiler compiler) throws CompilerException {
        if (name.equals(ATTRIBUTE_TEXT)) {
            String prefix = compiler.getImportedType(SwingUtil.class);
            return prefix + ".setText(" +
                    id + ", " + valueCode + ");" +
                    JAXXCompiler.getLineSeparator();
        }
        return super.getSetPropertyCode(id, name, valueCode, compiler);
    }

    @Override
    protected int getAttributeOrdering(Attr attr) {
        // delay text in case other attributes affect how it's processed, as is the case
        // with JEditorPane's contentType
        if (ATTRIBUTE_TEXT.equals(attr.getName())) {
            return 1;
        }
        return super.getAttributeOrdering(attr);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getText", DocumentListener.class, "document");
    }
}
