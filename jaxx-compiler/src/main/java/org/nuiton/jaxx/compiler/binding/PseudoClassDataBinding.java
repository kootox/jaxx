/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.binding;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;
import org.nuiton.jaxx.compiler.java.parser.JavaParser;
import org.nuiton.jaxx.compiler.java.parser.JavaParserTreeConstants;
import org.nuiton.jaxx.compiler.java.parser.SimpleNode;

import java.io.StringReader;

/**
 * Represents a data binding in a JAXX file.  <code>DataBinding</code> uses
 * {@link DataSource} to track changes to a source expression and update
 * the destination.
 */
public class PseudoClassDataBinding extends DataBinding {

    /**
     * Logger
     */
    protected static final Log log = LogFactory.getLog(PseudoClassDataBinding.class);

    protected final boolean invert;

    public static PseudoClassDataBinding newPseudoClassDataBinding(String pseudoClass, CompiledObject object, String propertyCode, String methodName, boolean invertTest) {
        PseudoClassDataBinding binding = null;
        if (pseudoClass.startsWith("{")) {
            pseudoClass = pseudoClass.substring(1, pseudoClass.length() - 1).trim();
            pseudoClass = replaceObjectReferences(pseudoClass, object.getJavaCode());
            String id = object.getId() + ".style." + pseudoClass + "." + methodName;

            if (log.isDebugEnabled()) {
                log.debug("will test if databinding : [" + pseudoClass + "] " + id);
            }
            binding = new PseudoClassDataBinding(id, pseudoClass, propertyCode, invertTest);
        }
        return binding;
    }

    protected PseudoClassDataBinding(String id, String source, String assignment, boolean invert) {
        super(id, source, assignment, false);
        this.invert = invert;
    }

    /**
     * Replaces all references to the variable "object" with the actual object ID.
     *
     * @param code ?
     * @param id   ?
     * @return ?
     * @throws CompilerException ?
     */
    public static String replaceObjectReferences(String code, String id) throws CompilerException {
        JavaParser p = new JavaParser(new StringReader(code + ";"));
        p.Expression();
        SimpleNode node = p.popNode();
        scanNode(node, id);
        return node.getText();
    }

    public static void scanNode(SimpleNode node, String id) {
        if (node.getId() == JavaParserTreeConstants.JJTNAME) {
            String name = node.getText();
            if (name.equals("object") ||
                    (name.contains(".") &&
                            name.substring(0, name.indexOf(".")).trim().equals("object"))) {
                node.firstToken.image = id;
            }
        } else {
            int count = node.jjtGetNumChildren();
            for (int i = 0; i < count; i++) {
                scanNode(node.getChild(i), id);
            }
        }
    }

    @Override
    protected String getInitDataBindingCode(JAXXCompiler compiler, DataSource dataSource, boolean isBinding) {
        // nothing to init
        return null;
    }

    @Override
    protected String getProcessDataBindingCode(JAXXCompiler compiler, DataSource dataSource, boolean isBinding) {
        if (!isBinding) {
            return null;
        }

        String eol = JAXXCompiler.getLineSeparator();

        StringBuilder buffer = new StringBuilder();

        String realSource = invert ? invert(getSource()) : getSource();
        buffer.append("if (").append(realSource).append(") {").append(eol);
        buffer.append(JavaFileGenerator.indent(getAssignment(), 4, false, eol)).append(eol);
        buffer.append("}");
        return buffer.toString();
    }

    protected String invert(String javaCode) {
        javaCode = javaCode.trim();
        return javaCode.startsWith("!") ? javaCode.substring(1) : "!(" + javaCode + ")";
    }
}
