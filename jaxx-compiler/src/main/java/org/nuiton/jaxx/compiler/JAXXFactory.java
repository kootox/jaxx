/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.spi.Initializer;
import org.nuiton.jaxx.compiler.tags.TagManager;

import java.io.File;
import java.util.ServiceLoader;

/**
 * Factory of {@link JAXXCompiler} and {@link JAXXEngine}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class JAXXFactory {

    /** Logger */
    private static final Log log = LogFactory.getLog(JAXXFactory.class);

    /** shared instance of engine. */
    protected static JAXXEngine engine;

    /** shared instance of configuration * */
    protected static CompilerConfiguration configuration;

    protected JAXXFactory() {
        // no instance
    }

    public static void setConfiguration(CompilerConfiguration configuration) {
        JAXXFactory.configuration = configuration;
    }

    /**
     * Create a new empty launchor and set it as current launchor accessible
     * via method {@link #getEngine()}.
     *
     * @return the new instanciated launchor
     */
    public static JAXXEngine newDummyEngine() {
        return newEngine(null);
    }

    /**
     * Create a new launchor and set it as current launchor accessible via
     * method {@link #getEngine()}.
     *
     * The launchor will be prepared to run a set of files, expressed as paths
     * relative to a base directory.
     * The class names of the compiled files are derived from the relative path
     * strings (e.g. "example/Foo.jaxx" compiles into a class named
     * "example.Foo").
     *
     * @param basedir       the directory against which to resolve relative paths
     * @param relativePaths a list of relative paths to .jaxx files being compiled
     * @return the new instanciated launchor
     */
    public static JAXXEngine newEngine(File basedir, String... relativePaths) {
        checkConfiguration();
        if (engine != null) {
            engine.reset(true);
        }
        engine = new JAXXEngine(configuration, basedir, relativePaths);
        return engine;
    }

    /**
     * Creates a dummy Compiler for use in unit testing or dettached use of an
     * engine.
     *
     * @param classLoader class loader to use
     * @return the compiler
     */
    public static JAXXCompiler newDummyCompiler(ClassLoader classLoader) {
        JAXXCompiler compiler = new JAXXCompiler();
        if (classLoader != null) {
            compiler.setClassLoader(classLoader);
        }
        return compiler;
    }

    /**
     * @return the current launchor
     * @throws NullPointerException if no launchor was registred via a
     *                              <code>newEngine-like</code> method.
     */
    public static JAXXEngine getEngine() throws NullPointerException {
        checkConfiguration();
        checkEngine();
        return engine;
    }

    /**
     * @return {@code true} if there is an engine registred, {@code false} otherwise.
     */
    public static boolean isEngineRegistred() {
        return engine != null;
    }

    /**
     * Load the {@link Initializer} services found via the{@link ServiceLoader}
     * mecanism.
     */
    public static void initFactory() {

        // must have a configuration
        checkConfiguration();

        TagManager.reset();

        boolean verbose = configuration.isVerbose();
        for (Initializer initializer :
                configuration.getInitializers().values()) {
            if (verbose) {
                log.info("load initializer " + initializer);
            }
            initializer.initialize();
        }

    }

    protected static void checkConfiguration() throws NullPointerException {
        if (configuration == null) {
            throw new NullPointerException("No configuration was registred.");
        }
    }

    protected static void checkEngine() throws NullPointerException {
        if (engine == null) {
            throw new NullPointerException("No engine was registred.");
        }
    }
}
