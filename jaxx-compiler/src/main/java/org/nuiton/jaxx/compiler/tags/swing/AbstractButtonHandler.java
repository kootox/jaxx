/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import javax.swing.AbstractButton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.runtime.swing.ApplicationAction;

public class AbstractButtonHandler extends DefaultComponentHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractButtonHandler.class);

    public AbstractButtonHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, AbstractButton.class);
    }

    @Override
    public void setAttribute(CompiledObject object, String propertyName, String stringValue, boolean inline, JAXXCompiler compiler) {
        if (propertyName.equals(ApplicationAction.ACTION_TYPE)) {

            compiler.addImport(ApplicationAction.class);
            String type = compiler.checkJavaCode(stringValue);
            if (compiler.getConfiguration().isVerbose()) {
                log.info("Found action: " + type + " for " + compiler.getJavaFile().getName());
            }
            compiler.appendLateInitializer(JAXXCompiler.getLineSeparator()+String.format("ApplicationAction.init(this, %s, %s);", object.getId(), type));
        } else {
            super.setAttribute(object, propertyName, stringValue, inline, compiler);
        }
    }

}
