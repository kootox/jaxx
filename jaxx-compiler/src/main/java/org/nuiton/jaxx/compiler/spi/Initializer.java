/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.spi;

import java.util.ServiceLoader;

/**
 * Performs SPI initialization, typically to register new tags, beans and converter.
 *
 * <b>Note:</b> To load such Initializer, we use the {@link ServiceLoader} mecanism.
 *
 * @see DefaultInitializer
 */
public interface Initializer {

    /**
     * Performs SPI initialization, typically to register new
     * tags, beans and converter.
     */
    void initialize();
}
