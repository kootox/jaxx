/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding.writers;

import org.nuiton.jaxx.compiler.binding.DataBinding;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;
import org.nuiton.jaxx.runtime.JAXXBinding;

/**
 * The contract of a writer of {@link JAXXBinding} creation code from a
 * {@link DataBinding}.
 *
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public interface JAXXBindingWriter<B extends JAXXBinding> {

    /**
     * Test if a binding can be treated by the writer.
     *
     * @param binding the binding to test
     * @return {@code true} if this writer can be used to generate binding creation code, {@code false} otherwise.
     */
    boolean accept(DataBinding binding);

    /**
     * Test if the writer was at least used once (says that the method
     * {@link #write(DataBinding, JavaFileGenerator, StringBuilder)} was at least invoked once).
     *
     * @return {@code true} if this writer was used
     */
    boolean isUsed();

    /** @return the type of {@link JAXXBinding} to generate */
    Class<B> getType();

    /**
     * Generate the creation code of the given {@code binding} and push it in the given {@code buffer}.
     *
     * Note: after beean in this method, the {@link #isUsed()} should always return {@code true}.
     *
     * @param binding   the binding to use
     * @param generator common generator to build creation code
     * @param buffer    where to push creation code
     */
    void write(DataBinding binding,
               JavaFileGenerator generator,
               StringBuilder buffer);

    /**
     * Reset internal states.
     *
     * At the moment, there is only the {@code used} property which is
     * internal and must be reset to {@code false} for each compiler in order
     * to know if a type of writer is used for the file.
     *
     * @since 2.4
     */
    void reset();
}
