/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.Table;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

public class TableHandler extends DefaultComponentHandler {

    public static final Insets DEFAULT_INSETS = new Insets(3, 3, 3, 3);

    public TableHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(),
                                                beanClass,
                                                Table.class);
    }

    @Override
    public void setAttribute(CompiledObject object,
                             String propertyName,
                             String stringValue,
                             boolean inline,
                             JAXXCompiler compiler) throws CompilerException {
        try {
            if (object instanceof CompiledTable) {
                CellHandler.setAttribute(
                        ((CompiledTable) object).getTableConstraints(),
                        propertyName,
                        stringValue
                );
            } else {
                super.setAttribute(object,
                                   propertyName,
                                   stringValue,
                                   inline,
                                   compiler);
            }
        } catch (UnsupportedAttributeException e) {
            super.setAttribute(object,
                               propertyName,
                               stringValue,
                               inline,
                               compiler
            );
        }
    }

    class CompiledTable extends CompiledObject {

        private final List<Integer> rowSpans = new ArrayList<>();

        private final GridBagConstraints tableConstraints;

        private GridBagConstraints rowConstraints;

        private GridBagConstraints cellConstraints;

        private boolean emptyCell;

        public CompiledTable(String id,
                             ClassDescriptor objectClass,
                             JAXXCompiler compiler) throws CompilerException {
            super(id, objectClass, compiler);
            tableConstraints = new GridBagConstraints();
            tableConstraints.gridx = -1;
            tableConstraints.gridy = -1;
            tableConstraints.insets = DEFAULT_INSETS;
        }

        @Override
        public void addChild(CompiledObject child,
                             String constraints,
                             JAXXCompiler compiler) throws CompilerException {
            if (constraints != null) {
                compiler.reportError("Table does not accept constraints");
            }
            GridBagConstraints c = getCellConstraints();
            if (c == null) {
                compiler.reportError("Table tag may only contain row tags");
                return;
            }
            if (!emptyCell) {
                compiler.reportError(
                        "Table cells may only have one child component");
            }
            while (rowSpans.size() < c.gridx + c.gridwidth) {
                rowSpans.add(null);
            }
            for (int x = c.gridx; x < c.gridx + c.gridwidth; x++) {
                rowSpans.set(x, c.gridheight);
            }

            super.addChild(child, TypeManager.getJavaCode(c), compiler);

            emptyCell = false;

            compiler.addImport(GridBagConstraints.class);
            compiler.addImport(Insets.class);
        }

        public GridBagConstraints getTableConstraints() {
            return tableConstraints;
        }

        public GridBagConstraints getRowConstraints() {
            return rowConstraints;
        }

        public GridBagConstraints getCellConstraints() {
            return cellConstraints;
        }

        public void newRow() {
            tableConstraints.gridy++;
            tableConstraints.gridx = -1;
            rowConstraints = (GridBagConstraints) tableConstraints.clone();

            for (int x = 0; x < rowSpans.size(); x++) {
                int rowSpan = rowSpans.get(x);
                if (rowSpan > 0) {
                    rowSpans.set(x, rowSpan - 1);
                }
            }
        }

        public void newCell() {
            emptyCell = true;
            rowConstraints.gridx++;
            while (rowConstraints.gridx < rowSpans.size() &&
                    rowSpans.get(rowConstraints.gridx) > 0) {
                rowConstraints.gridx++;
            }
            cellConstraints = (GridBagConstraints) rowConstraints.clone();
        }
    }

    @Override
    public CompiledObject createCompiledObject(String id,
                                               JAXXCompiler compiler) throws CompilerException {
        return new CompiledTable(id, getBeanClass(), compiler);
    }
}
