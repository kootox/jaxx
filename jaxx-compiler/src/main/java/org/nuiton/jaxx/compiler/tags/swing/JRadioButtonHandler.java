/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeListener;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

public class JRadioButtonHandler extends AbstractButtonHandler {

    private static final String VALUE_PROPERTY = JAXXButtonGroup.VALUE_CLIENT_PROPERTY.substring(1);

    private static final String BUTTON_GROUP_PROPERTY = JAXXButtonGroup.BUTTON8GROUP_CLIENT_PROPERTY.substring(1);

    public JRadioButtonHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JRadioButton.class, JRadioButtonMenuItem.class, JToggleButton.class);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("isSelected", ChangeListener.class, "model");
    }

    @Override
    public ClassDescriptor getPropertyType(CompiledObject object, String name, JAXXCompiler compiler) throws CompilerException {
        if (name.equals(BUTTON_GROUP_PROPERTY)) {
            return null; // accepts either a String or a ButtonGroup
        } else if (name.equals(VALUE_PROPERTY)) {
            return ClassDescriptorHelper.getClassDescriptor(Object.class);
        } else {
            return super.getPropertyType(object, name, compiler);
        }
    }

    @Override
    public boolean isMemberBound(String name) throws UnsupportedAttributeException {
        return !(name.equals(BUTTON_GROUP_PROPERTY) || name.equals(VALUE_PROPERTY)) && super.isMemberBound(name);
    }

    // handle buttonGroup assignment in addition block rather than initialization block
    @Override
    public void setProperty(CompiledObject object, String name, Object value, JAXXCompiler compiler) {
        if (name.equals(BUTTON_GROUP_PROPERTY)) {
            object.appendAdditionCode(getSetPropertyCode(object.getJavaCode(), name, TypeManager.getJavaCode(value), compiler));
        } else {
            super.setProperty(object, name, value, compiler);
        }
    }

    @Override
    public String getSetPropertyCode(String id, String name, String valueCode, JAXXCompiler compiler) throws CompilerException {
        if (name.equals(BUTTON_GROUP_PROPERTY)) {
            if (valueCode.startsWith("\"") && valueCode.endsWith("\"")) {
                valueCode = valueCode.substring(1, valueCode.length() - 1);
                CompiledObject buttonGroup = compiler.getCompiledObject(valueCode);
                if (buttonGroup == null) {
                    buttonGroup = new CompiledObject(valueCode, ClassDescriptorHelper.getClassDescriptor(JAXXButtonGroup.class), compiler);
                    compiler.registerCompiledObject(buttonGroup);
                }
            }
            String type = compiler.getImportedType(
                    ButtonGroup.class.getName());

            return "{ " + type + " $buttonGroup = " + valueCode + "; " + id + ".putClientProperty(\"$buttonGroup\", $buttonGroup); $buttonGroup.add(" + id + "); }\n";
        } else if (name.equals(VALUE_PROPERTY)) {
            String type = compiler.getImportedType(
                    JAXXButtonGroup.class.getName());
            return "{ " + id + ".putClientProperty(\"" + JAXXButtonGroup.VALUE_CLIENT_PROPERTY + "\", " + valueCode + ");  Object $buttonGroup = " + id + ".getClientProperty(\"" + JAXXButtonGroup.BUTTON8GROUP_CLIENT_PROPERTY + "\");" +
                    " if ($buttonGroup instanceof " + type + ") { ((" + type + ") $buttonGroup).updateSelectedValue(); } }\n";
        } else {
            return super.getSetPropertyCode(id, name, valueCode, compiler);
        }
    }
}
