/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

/**
 * Handles the <code>&lt;script&gt;</code> tag.
 *
 * @author Ethan Nicholas
 */
public class ScriptHandler implements TagHandler {

    public static final String TAG_NAME = "script";

    public static final String SOURCE_ATTRIBUTE = "source";

    @Override
    public void compileFirstPass(Element tag,
                                 JAXXCompiler compiler) throws CompilerException {
        File scriptFile = null;
        NamedNodeMap attributes = tag.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Attr attribute = (Attr) attributes.item(i);
            String name = attribute.getName();
            String attrValue = attribute.getValue();
            if (name.equals(SOURCE_ATTRIBUTE)) {
                String filename = attrValue.replace('/', File.separatorChar);
                scriptFile = new File(compiler.getBaseDir(), filename);
                String content = compiler.loadFile(scriptFile);
                compiler.registerScript(content, scriptFile);
//                StringWriter scriptBuffer = new StringWriter();
//                FileReader in = new FileReader(scriptFile);
//                try {
//                    char[] readBuffer = new char[2048];
//                    int c;
//                    while ((c = in.read(readBuffer)) > 0) {
//                        scriptBuffer.write(readBuffer, 0, c);
//                    }
//                } catch (FileNotFoundException e) {
//                    compiler.reportError(
//                            "script file not found: " + scriptFile);
//                } finally {
//                    in.close();
//                }
//                compiler.registerScript(scriptBuffer.toString(), scriptFile);
            } else if (!name.startsWith(XMLNS_ATTRIBUTE) &&
                    !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(
                            attribute.getNamespaceURI())) {
                throw new UnsupportedAttributeException(name);
            }
        }

        StringBuilder script = new StringBuilder();
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            switch (child.getNodeType()) {
                case Node.ELEMENT_NODE:
                    compiler.reportError(
                            "<script> tag may not contain child elements: " +
                                    tag);
                case Node.TEXT_NODE: // fall through
                case Node.CDATA_SECTION_NODE:
                    script.append(((Text) child).getData());
            }
        }

        String scriptString = script.toString().trim();
        if (scriptString.length() > 0) {
            if (scriptFile != null) {
                compiler.reportError(
                        "<script> tag has both a source attribute and an " +
                                "inline script");
            }
            compiler.registerScript(script.toString());
        }
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException {
        // nothing to do
    }
}
