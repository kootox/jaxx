/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.reflect;

/**
 * Mirrors the class <code>java.lang.ref.Field</code>.  JAXX uses <code>ClassDescriptor</code> instead of <code>Class</code>
 * almost everywhere so that it can handle circular dependencies (there can't be a <code>Class</code> object for an uncompiled
 * JAXX or Java source file, and a compiler must be allow references to symbols in uncompiled source files in order to handle
 * circular dependencies).
 */
public class FieldDescriptor extends MemberDescriptor {

    private final String type;

    public FieldDescriptor(String name, int modifiers, String type, ClassLoader classLoader) {
        super(name, modifiers, classLoader);
        this.type = type;
    }

    public ClassDescriptor getType() {
        try {
            return ClassDescriptorHelper.getClassDescriptor(type, getClassLoader());
        } catch (Exception e) {
            throw new RuntimeException("Type not found for field " + this, e);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " type : [" + type + "]";
    }
}
