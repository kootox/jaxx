/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import java.io.IOException;

public class JTreeHandler extends DefaultComponentHandler {

    public JTreeHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JTree.class);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getSelectionCount", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionPath", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionPaths", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionRows", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionValue", TreeSelectionListener.class, "selectionModel");
    }

    @Override
    public void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException {
        NodeList children = tag.getChildNodes();
        if (children.getLength() > 0) {
            compiler.reportError("JTree does not accept childs");
            throw new CompilerException("JTree does not accept childs");
        }
    }
}



