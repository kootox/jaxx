/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import java.awt.Color;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.I18nHelper;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.binding.DataBindingHelper;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.TabInfo;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class TabHandler implements TagHandler {

    public static final String TAG_NAME = "tab";

    public static final String ATTRIBUTE_ID = "id";

    public static final String ATTRIBUTE_TITLE = "title";

    public static final String ATTRIBUTE_TOOL_TIP_TEXT = "toolTipText";

    public static final String ATTRIBUTE_ICON = "icon";

    public static final String ATTRIBUTE_ENABLED = "enabled";

    public static final String ATTRIBUTE_DISABLED_ICON = "disabledIcon";

    public static final String ATTRIBUTE_MNEMONIC = "mnemonic";

    public static final String ATTRIBUTE_DISPLAYED_MNEMONIC_INDEX = "displayedMnemonicIndex";

    public static final String ATTRIBUTE_FOREGROUND = "foreground";

    public static final String ATTRIBUTE_BACKGROUND = "background";

    public static final String ATTRIBUTE_TAB_COMPONENT = "tabComponent";

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compileChildrenFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag, final JAXXCompiler compiler) throws CompilerException, IOException {
        if (!ClassDescriptorHelper.getClassDescriptor(JTabbedPane.class).isAssignableFrom(compiler.getOpenComponent().getObjectClass())) {
            compiler.reportError("tab tag may only appear within JTabbedPane tag");
            return;
        }

        JTabbedPaneHandler.CompiledTabbedPane tabs = (JTabbedPaneHandler.CompiledTabbedPane) compiler.getOpenComponent();

        String id = tag.getAttribute(ATTRIBUTE_ID);
        if (id == null || id.length() == 0) {
            id = compiler.getAutoId(TabInfo.class.getSimpleName());
//            id = compiler.getAutoId(ClassDescriptorHelper.getClassDescriptor(TabInfo.class));
        }
        TabInfo tabInfo = new TabInfo(id);
        CompiledObject compiledTabInfo = new CompiledObject(id, ClassDescriptorHelper.getClassDescriptor(TabInfo.class), compiler);
        compiler.registerCompiledObject(compiledTabInfo);
        compiledTabInfo.setConstructorParams("\"" + id + "\"");
        tabs.tabInfo = tabInfo;
        setAttributes(compiledTabInfo, tabs, tag, compiler);
        compileChildrenSecondPass(tag, compiler);
        tabs.tabInfo = null;
    }

    public static void setAttribute(CompiledObject compiledTabInfo, JTabbedPaneHandler.CompiledTabbedPane tabs, String name, String value, JAXXCompiler compiler) throws CompilerException {
        value = value.trim();
        TabInfo tabInfo = tabs.tabInfo;
        String id = tabInfo.getId();
        String binding = compiler.processDataBindings(value);
        if (binding != null) {
            compiler.getBindingHelper().registerDataBinding(id + "." + name, binding, id + ".set" + StringUtils.capitalize(name) + "(" + binding + ");");
            return;
        }

        String valueCode = TypeManager.getJavaCode(value);

        // add i18n support
        if (name.equals(DefaultObjectHandler.COMPUTE_I18N_ATTRIBUTE)) {

            if ("skip".equals(value)) {
                return;
            }
            String i18nKey = compiler.computeI18n(compiledTabInfo.getId(), value);
            String i18nProperty = compiler.getI18nProperty(compiledTabInfo);
            setAttribute(compiledTabInfo, tabs, i18nProperty, i18nKey, compiler);
            return;
        }

        if (I18nHelper.isI18nableAttribute(name, compiler)) {
            value = valueCode = I18nHelper.addI18nInvocation(id, name, valueCode, compiler);
        }
        if (name.equals(ATTRIBUTE_ID)) {
            // ignore, already handled
        } else if (name.equals(ATTRIBUTE_TITLE)) {
            tabInfo.setTitle(value);
            compiledTabInfo.appendInitializationCode(id + ".setTitle(" + valueCode + ");");
            //compiledTabInfo.appendInitializationCode(id + ".setTitle(" + TypeManager.getJavaCode(value) + ");");
        } else if (name.equals(ATTRIBUTE_TOOL_TIP_TEXT)) {
            tabInfo.setToolTipText(value);
            compiledTabInfo.appendInitializationCode(id + ".setToolTipText(" + valueCode + ");");
            //compiledTabInfo.appendInitializationCode(id + ".setToolTipText(" + TypeManager.getJavaCode(value) + ");");
        } else if (name.equals(ATTRIBUTE_ICON)) {
            Icon icon = (Icon) TypeManager.convertFromString(value, Icon.class);
            tabInfo.setIcon(icon);
            compiledTabInfo.appendInitializationCode(id + ".setIcon(" + TypeManager.getJavaCode(icon) + ");");
        } else if (name.equals(ATTRIBUTE_ENABLED)) {
            boolean enabled = (Boolean) TypeManager.convertFromString(value, Boolean.class);
            tabInfo.setEnabled(enabled);
            compiledTabInfo.appendInitializationCode(id + ".setEnabled(" + enabled + ");");
        } else if (name.equals(ATTRIBUTE_DISABLED_ICON)) {
            Icon disabledIcon = (Icon) TypeManager.convertFromString(value, Icon.class);
            tabInfo.setDisabledIcon(disabledIcon);
            compiledTabInfo.appendInitializationCode(id + ".setDisabledIcon(" + TypeManager.getJavaCode(disabledIcon) + ");");
        } else if (name.equals(ATTRIBUTE_MNEMONIC)) {
            int mnemonic = (Character) TypeManager.convertFromString(value, char.class);
            tabInfo.setMnemonic(mnemonic);
            compiledTabInfo.appendInitializationCode(id + ".setMnemonic(" + mnemonic + ");");
        } else if (name.equals(ATTRIBUTE_DISPLAYED_MNEMONIC_INDEX)) {
            int displayedMnemonicIndex = (Integer) TypeManager.convertFromString(value, int.class);
            tabInfo.setDisplayedMnemonicIndex(displayedMnemonicIndex);
            compiledTabInfo.appendInitializationCode(id + ".setDisplayedMnemonicIndex(" + displayedMnemonicIndex + ");");
        } else if (name.equals(ATTRIBUTE_FOREGROUND)) {
            Color foreground = (Color) TypeManager.convertFromString(value, Color.class);
            tabInfo.setForeground(foreground);
            compiledTabInfo.appendInitializationCode(id + ".setForeground(" + TypeManager.getJavaCode(foreground) + ");");
        } else if (name.equals(ATTRIBUTE_BACKGROUND)) {
            Color background = (Color) TypeManager.convertFromString(value, Color.class);
            tabInfo.setBackground(background);
            compiledTabInfo.appendInitializationCode(id + ".setBackground(" + TypeManager.getJavaCode(background) + ");");
        } else if (name.equals(ATTRIBUTE_TAB_COMPONENT)) {
            tabInfo.setTabComponentStr(TypeManager.getJavaCode(value));
            compiledTabInfo.appendInitializationCode(id + ".setTabComponent(" + TypeManager.getJavaCode(value) + ");");
        } else {
            compiler.reportError("The <tab> tag does not support the attribute '" + name + "'");
        }
    }

    public void setAttributes(CompiledObject compiledTabInfo, JTabbedPaneHandler.CompiledTabbedPane tabs, Element tag, JAXXCompiler compiler) throws CompilerException {
        NamedNodeMap children = tag.getAttributes();
        for (int i = 0; i < children.getLength(); i++) {
            Attr attribute = (Attr) children.item(i);
            String name = attribute.getName();
            String value = attribute.getValue();
            if (!name.startsWith("xmlns") && !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                setAttribute(compiledTabInfo, tabs, name, value, compiler);
            }
        }
    }

    protected void compileChildrenFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagFirstPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    protected void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }
}
