/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;

import java.util.HashMap;
import java.util.Map;

/**
 * Mirrors the class <code>java.beans.FeatureDescriptor</code>.  JAXX uses its own introspector rather than the built-in
 * <code>java.beans.Introspector</code> so that it can introspect {@link ClassDescriptor},
 * not just <code>java.lang.Class</code>.
 */
public class JAXXFeatureDescriptor {

    private String name;

    private Map<String, Object> values;

    private ClassDescriptor classDescriptor;

    JAXXFeatureDescriptor(ClassDescriptor classDescriptor, String name) {
        if (name == null || classDescriptor == null) {
            throw new NullPointerException();
        }
        this.name = name;
        this.classDescriptor = classDescriptor;
    }

    public String getName() {
        return name;
    }

    public ClassDescriptor getClassDescriptor() {
        return classDescriptor;
    }

    public Object getValue(String key) {
        return values != null ? values.get(key) : null;
    }

    public void setValue(String key, Object value) {
        if (values == null) {
            values = new HashMap<>();
        }
        values.put(key, value);
    }

    public static String capitalize(String name) {
        if (name.length() == 0) {
            return name;
        }
        return Character.toUpperCase(name.charAt(0)) + name.substring(1);
    }
}
