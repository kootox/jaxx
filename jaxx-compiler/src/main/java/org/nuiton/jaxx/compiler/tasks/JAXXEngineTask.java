/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;

/**
 * Base class to implement a task to be launched by a {@link JAXXEngine}.
 *
 * The {@link #perform(JAXXEngine)} method contains the logic of the task.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public abstract class JAXXEngineTask {

    /** Task name */
    private final String name;

    public JAXXEngineTask(String name) {
        this.name = name;
    }

    /**
     * Performs the task on the given {@code engine}.
     *
     * @param engine the engine to use
     * @return {@code false} if task failed (with no exception), {@code true} otherwise.
     * @throws Exception if any error
     */
    public abstract boolean perform(JAXXEngine engine) throws Exception;

    public String getName() {
        return name;
    }

    /**
     * Checks the engine does not have any more files to discover.
     *
     * @param engine the engine to test
     * @throws IllegalStateException if there is still some files to discover.
     */
    protected void checkAllFilesCompiled(JAXXEngine engine) throws IllegalStateException {
        JAXXCompilerFile[] undone = engine.getFilesToCompile();

        if (undone.length > 0) {

            throw new IllegalStateException("Can not start '" + getName() + "', there is still files to process in '" + CompileFirstPassTask.TASK_NAME);
        }
    }

    protected void addStartProfileTime(JAXXEngine engine,
                                       JAXXCompiler compiler) {
        engine.addProfileTime(compiler, name + "_start");
    }


    protected void addEndProfileTime(JAXXEngine engine,
                                     JAXXCompiler compiler) {
        engine.addProfileTime(compiler, name + "_end");
    }
}
