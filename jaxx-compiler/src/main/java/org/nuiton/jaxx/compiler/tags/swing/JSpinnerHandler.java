/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.w3c.dom.Element;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;

public class JSpinnerHandler extends DefaultComponentHandler {

    public static final String MINIMUM_PROPERTY = "minimum";

    public static final String MAXIMUM_PROPERTY = "maximum";

    public static final String VALUE_PROPERTY = "value";

    public JSpinnerHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JSpinner.class);
    }

    public static class CompiledSpinner extends CompiledObject {

        Integer minimum;

        Integer maximum;

        Integer value;

        public CompiledSpinner(String id, ClassDescriptor objectClass, JAXXCompiler compiler) throws CompilerException {
            super(id, objectClass, compiler);
        }
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledSpinner(id, getBeanClass(), compiler);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getValue", ChangeListener.class, "model");
    }

    @Override
    public ClassDescriptor getPropertyType(CompiledObject object, String propertyName, JAXXCompiler compiler) throws CompilerException {
        if (propertyName.equals(MINIMUM_PROPERTY) || propertyName.equals(MAXIMUM_PROPERTY) ||
                propertyName.equals(VALUE_PROPERTY)) {
            return ClassDescriptorHelper.getClassDescriptor(Integer.class);
        }
        return super.getPropertyType(object, propertyName, compiler);
    }

    @Override
    public void setProperty(CompiledObject object, String name, Object value, JAXXCompiler compiler) throws CompilerException {
        if (name.equals(MINIMUM_PROPERTY)) {
            ((CompiledSpinner) object).minimum = (Integer) value;
        } else if (name.equals(MAXIMUM_PROPERTY)) {
            ((CompiledSpinner) object).maximum = (Integer) value;
        } else if (name.equals(VALUE_PROPERTY)) {
            ((CompiledSpinner) object).value = (Integer) value;
        } else {
            super.setProperty(object, name, value, compiler);
        }
    }

    @Override
    protected void closeComponent(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        CompiledSpinner spinner = (CompiledSpinner) object;
        if (spinner.minimum != null ||
                spinner.maximum != null ||
                spinner.value != null) {
            if (spinner.getConstructorParams() != null) {
                compiler.reportError("constructorParams and minimum/maximum may not both be specified for the same JSpinner");
            }
            if (spinner.minimum == null) {
                spinner.minimum = Math.min(0, spinner.maximum != null ? spinner.maximum : 0);
            }
            if (spinner.maximum == null) {
                spinner.maximum = Math.max(100, spinner.minimum);
            }
            if (spinner.value == null) {
                spinner.value = spinner.minimum;
            }
            String type = compiler.getImportedType(SpinnerNumberModel.class);

            spinner.setConstructorParams("new " + type + "(" + spinner.value + ", " + spinner.minimum + ", " + spinner.maximum + ", 1)");
        }

        super.closeComponent(object, tag, compiler);
    }
}
