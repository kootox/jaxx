/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;

import javax.swing.JSplitPane;

public class JSplitPaneHandler extends DefaultComponentHandler {

    public static final String ATTRIBUTE_ORIENTATION = "orientation";

    public static final String ORIENTATION_VALUE_HORIZONTAL = "horizontal";

    public static final String ORIENTATION_VALUE_VERTICAL = "vertical";

    public static final String ORIENTATION_VALUE_VERTICAL_SPLIT = "vertical_split";

    public static final String ORIENTATION_VALUE_HORIZONTAL_SPLIT = "horizontal_split";

    public JSplitPaneHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JSplitPane.class);
    }

//    protected Component createRawComponent(Element tag) {
//        return new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
//    }

    /**
     * Add support for <code>orientation="vertical"</code> and <code>orientation="horizontal"</code>.  The
     * values required by the JAXXBeanInfo are the unwieldy <code>vertical_split</code> and <code>horizontal_split</code>
     * (which are also recognized).
     */
    @Override
    protected int constantValue(String key, String value) {
        if (key.equals(ATTRIBUTE_ORIENTATION)) {
            value = value.trim().toLowerCase();
            if (value.equals(ORIENTATION_VALUE_HORIZONTAL) ||
                    value.equals(ORIENTATION_VALUE_HORIZONTAL_SPLIT)) {
                return JSplitPane.HORIZONTAL_SPLIT;
            }
            if (value.equals(ORIENTATION_VALUE_VERTICAL) ||
                    value.equals(ORIENTATION_VALUE_VERTICAL_SPLIT)) {
                return JSplitPane.VERTICAL_SPLIT;
            }
            throw new IllegalArgumentException("orientation must be 'horizontal' or 'vertical', found '" + value + "'");
        }
        return super.constantValue(key, value);
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledObject(id, getBeanClass(), compiler) {

            private int count;

            @Override
            public void addChild(CompiledObject child, String constraints, JAXXCompiler compiler) throws CompilerException {
                if (constraints != null) {
                    compiler.reportError("JSplitPane does not accept constraints");
                }
                if (count == 0) {
                    super.addChild(child, "JSplitPane.LEFT", compiler);
                } else if (count == 1) {
                    super.addChild(child, "JSplitPane.RIGHT", compiler);
                } else {
                    compiler.reportError("JSplitPane is limited to two children");
                }
                count++;
            }
        };
    }
}
