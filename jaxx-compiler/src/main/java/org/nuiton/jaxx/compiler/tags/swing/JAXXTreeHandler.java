/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.Item;
import org.nuiton.jaxx.runtime.swing.JAXXTree;
import org.w3c.dom.Element;

import javax.swing.event.TreeSelectionListener;
import java.io.IOException;
import java.util.List;

public class JAXXTreeHandler extends DefaultComponentHandler {

    public JAXXTreeHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JAXXTree.class);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getSelectionCount", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionPath", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionPaths", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionRows", TreeSelectionListener.class, "selectionModel");
        addProxyEventInfo("getSelectionValue", TreeSelectionListener.class, "selectionModel");
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledItemContainer(id, getBeanClass(), compiler);
    }

    private void createItems(CompiledObject tree, List<Item> items, String addMethod, JAXXCompiler compiler) throws CompilerException {
        for (Item item : items) {
            String id = item.getId();
            CompiledObject compiledItem = new CompiledObject(id, ClassDescriptorHelper.getClassDescriptor(Item.class), compiler);
            compiledItem.setConstructorParams(TypeManager.getJavaCode(id) + ", " + TypeManager.getJavaCode(item.getLabel()) + ", " + TypeManager.getJavaCode(item.getValue()) + ", " + item.isSelected());
            compiler.registerCompiledObject(compiledItem);
            tree.appendAdditionCode(addMethod + "(" + id + ");");
            createItems(tree, item.getChildren(), id + ".addChild", compiler);
        }
    }

    @Override
    public void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        super.compileChildrenSecondPass(tag, compiler);
        CompiledItemContainer tree = (CompiledItemContainer) compiler.getOpenComponent();
        List<Item> items = tree.getItems();
        if (items != null && !items.isEmpty()) {
            String listName = tree.getId() + "$items";
            tree.appendAdditionCode("java.util.List<org.nuiton.jaxx.runtime.swing.Item> " + listName + " = new java.util.ArrayList<org.nuiton.jaxx.runtime.swing.Item>();");
            createItems(tree, items, listName + ".add", compiler);
            tree.appendAdditionCode(tree.getId() + ".setItems(" + listName + ");");
        }
    }
}



