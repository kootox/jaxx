/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class TypeManager {

    /**
     * Dictionnary of converters loaded by the {@link ServiceLoader} mecanism
     */
    private static Map<Class<?>, TypeConverter> converters;

    /**
     * @return dictionnary of known converters
     */
    public static Map<Class<?>, TypeConverter> getConverters() {
        if (converters == null) {
            converters = new HashMap<>();
            // load converters
            ServiceLoader<TypeConverter> loader =
                    ServiceLoader.load(TypeConverter.class);
            for (TypeConverter c : loader) {
                // for each supported type, register the converter
                for (Class<?> type : c.getSupportedTypes()) {
                    converters.put(type, c);
                }
            }
        }
        return converters;
    }

    private TypeManager() { /* not instantiable */ }

    public static TypeConverter getTypeConverter(Class<?> type) {
        return getConverters().get(type);
    }

    public static String getJavaCode(Object object) {
        if (object == null) {
            return "null";
        }
        TypeConverter converter = getTypeConverter(object.getClass());
        if (converter == null) {
            throw new IllegalArgumentException("unsupported type: " + object.getClass());
        }
        return converter.getJavaCode(object);
    }

    public static Object convertFromString(String string, Class<?> type) {
        TypeConverter converter = getTypeConverter(type);
        if (converter == null) {
            throw new IllegalArgumentException("unsupported type: " + type);
        }
        return converter.convertFromString(string, type);
    }

    /**
     * Convertit un nom de variable en nom de constante.
     *
     * @param variableName le nom de variable a convertir
     * @return le nom de la constante à partir du nom de la variable
     */
    public static String convertVariableNameToConstantName(String variableName) {
        StringBuilder buffer = new StringBuilder();
        boolean lastCarIsUp = false;
        char lastChar = '$';
        for (int i = 0, j = variableName.length(); i < j; i++) {
            char c = variableName.charAt(i);
            boolean carIsUp;
            if (c == '!') {
                buffer.append("NOT_");
                lastChar = '_';
                lastCarIsUp = true;
                continue;
            }
            if (Character.isLetterOrDigit(c)) {
                carIsUp = Character.isUpperCase(c);
            } else {
                carIsUp = false;
                if (c != '$') {
                    // on remplace tous les caractères non standard par un _
                    if (lastChar == '_' && c != '_') {
                        continue;
                    }
                    c = '_';
                }
            }

            if (i > 0 && !lastCarIsUp && carIsUp) {
                // ajout d'un _
                buffer.append('_');
            }
            if (carIsUp) {
                buffer.append(c);
            } else {
                buffer.append(Character.toUpperCase(c));
            }
            lastCarIsUp = carIsUp || c == '$' || c == '_';
            lastChar = c;
        }
        return buffer.toString();
    }
}
