/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tags.swing;

import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;

import java.awt.Component;

/**
 * To deal with JXLayer, since from version 3.0.4, we can not use any longer
 * the {@link JXLayer#add(Component)} ! but must now use the
 * method {@link JXLayer#setView(Component)}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
public class JXLayerHandler extends DefaultComponentHandler {

    public static final String ATTRIBUTE_ORIENTATION = "orientation";

    public static final String ORIENTATION_VALUE_HORIZONTAL = "horizontal";

    public static final String ORIENTATION_VALUE_VERTICAL = "vertical";

    public static final String ORIENTATION_VALUE_VERTICAL_SPLIT = "vertical_split";

    public static final String ORIENTATION_VALUE_HORIZONTAL_SPLIT = "horizontal_split";

    public JXLayerHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JXLayer.class);
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledObject(id, getBeanClass(), compiler) {

            @Override
            public void addChild(CompiledObject child,
                                 String constraints,
                                 JAXXCompiler compiler) throws CompilerException {

                if (constraints != null) {
                    compiler.reportError("JXLayer does not accept constraints");
                    return;
                }
                if (CollectionUtils.isNotEmpty(getChilds())) {

                    // already one child, authrozied only one child...
                    compiler.reportError("JXLayer is limited to one children");
                    return;
                }
                super.addChild(child, constraints, compiler);

            }

            @Override
            protected ChildRef newChildRef(CompiledObject child, String constraints, String delegateCode) {
                return new ChildRef(child,
                                    constraints,
                                    child.getJavaCode(),
                                    delegateCode) {
                    @Override
                    public void addToAdditionCode(StringBuilder buffer, boolean isRootObject) {
                        //TC-20091026 do not prefix if on root object
                        String prefix;
                        if (isRootObject) {
                            prefix = "";
                        } else {
                            prefix = getJavaCode() + getDelegateCode() + ".";
                        }
                        buffer.append(prefix);
                        buffer.append("setView(");
                        buffer.append(getChildJavaCode());
                        buffer.append(");");
                        buffer.append(JAXXCompiler.getLineSeparator());
                    }
                };
            }
        };
    }
}
