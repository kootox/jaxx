/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.IOException;

public class CellHandler implements TagHandler {

    public static final String TAG_NAME = "cell";

    public static final String ATTRIBUTE_INSETS = "insets";

    public static final String ATTRIBUTE_WEIGHTX = "weightx";

    public static final String ATTRIBUTE_WEIGHTY = "weighty";

    public static final String ATTRIBUTE_COLUMNS = "columns";

    public static final String ATTRIBUTE_ROWS = "rows";

    public static final String ATTRIBUTE_FILL = "fill";

    enum Fill {
        none(GridBagConstraints.NONE),
        horizontal(GridBagConstraints.HORIZONTAL),
        vertical(GridBagConstraints.VERTICAL),
        both(GridBagConstraints.BOTH);

        private final int intValue;

        Fill(int intValue) {
            this.intValue = intValue;
        }

        public int getIntValue() {
            return intValue;
        }
    }

    public static final String ATTRIBUTE_ANCHOR = "anchor";

    enum Anchor {
        north(GridBagConstraints.NORTH),
        northeast(GridBagConstraints.NORTHEAST),
        east(GridBagConstraints.EAST),
        southeast(GridBagConstraints.SOUTHEAST),
        south(GridBagConstraints.SOUTH),
        southwest(GridBagConstraints.SOUTHEAST),
        west(GridBagConstraints.WEST),
        northwest(GridBagConstraints.NORTHWEST),
        center(GridBagConstraints.CENTER);

        private final int intValue;

        Anchor(int intValue) {
            this.intValue = intValue;
        }

        public int getIntValue() {
            return intValue;
        }
    }

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compileChildrenFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        Node parent = tag.getParentNode();
        if (parent.getNodeType() != Node.ELEMENT_NODE || !parent.getLocalName().equals("row")) {
            compiler.reportError("cell tag may only appear within row tag");
            return;
        }

        TableHandler.CompiledTable table = (TableHandler.CompiledTable) compiler.getOpenComponent();
        table.newCell();
        GridBagConstraints c = table.getCellConstraints();
        setAttributes(c, tag);
        compileChildrenSecondPass(tag, compiler);
    }

    public static void setAttribute(GridBagConstraints c, String name, String value) throws CompilerException {
        value = value.trim();
        if (name.equals(ATTRIBUTE_INSETS)) {
            c.insets = (Insets) TypeManager.convertFromString(value, Insets.class);
        } else if (name.equals(ATTRIBUTE_WEIGHTX)) {
            c.weightx = Double.parseDouble(value);
        } else if (name.equals(ATTRIBUTE_WEIGHTY)) {
            c.weighty = Double.parseDouble(value);
        } else if (name.equals(ATTRIBUTE_COLUMNS)) {
            c.gridwidth = Integer.parseInt(value);
        } else if (name.equals(ATTRIBUTE_ROWS)) {
            c.gridheight = Integer.parseInt(value);
        } else if (name.equals(ATTRIBUTE_FILL)) {
            Fill fill = Fill.valueOf(value);
            if (fill == null) {
                throw new CompilerException("invalid value for fill attribute: '" + value + "'");
            }
            c.fill = fill.getIntValue();
        } else if (name.equals(ATTRIBUTE_ANCHOR)) {
            Anchor anchor = Anchor.valueOf(value);
            if (anchor == null) {
                throw new CompilerException("invalid value for anchor attribute: '" + value + "'");
            }
            c.anchor = anchor.getIntValue();
//            if (value.equals(ANCHOR_VALUE_NORTH)) {
//                c.anchor = GridBagConstraints.NORTH;
//            } else if (value.equals(ANCHOR_VALUE_NORTHEAST)) {
//                c.anchor = GridBagConstraints.NORTHEAST;
//            } else if (value.equals(ANCHOR_VALUE_EAST)) {
//                c.anchor = GridBagConstraints.EAST;
//            } else if (value.equals(ANCHOR_VALUE_SOUTHEAST)) {
//                c.anchor = GridBagConstraints.SOUTHEAST;
//            } else if (value.equals(ANCHOR_VALUE_SOUTH)) {
//                c.anchor = GridBagConstraints.SOUTH;
//            } else if (value.equals(ANCHOR_VALUE_SOUTHWEST)) {
//                c.anchor = GridBagConstraints.SOUTHWEST;
//            } else if (value.equals(ANCHOR_VALUE_WEST)) {
//                c.anchor = GridBagConstraints.WEST;
//            } else if (value.equals(ANCHOR_VALUE_NORTHWEST)) {
//                c.anchor = GridBagConstraints.NORTHWEST;
//            } else if (value.equals(ANCHOR_VALUE_CENTER)) {
//                c.anchor = GridBagConstraints.CENTER;
//            } else {
//                throw new IllegalArgumentException("invalid value for anchor attribute: '" + value + "'");
//            }
        } else {
            throw new UnsupportedAttributeException(name);
        }
    }

    public static void setAttributes(GridBagConstraints c, Element tag) throws CompilerException {
        NamedNodeMap children = tag.getAttributes();
        for (int i = 0; i < children.getLength(); i++) {
            Attr attribute = (Attr) children.item(i);
            String name = attribute.getName();
            String value = attribute.getValue();
            if (!name.startsWith("xmlns") && !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                setAttribute(c, name, value);
            }
        }
    }

    protected void compileChildrenFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagFirstPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    protected void compileChildTagSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }
}
