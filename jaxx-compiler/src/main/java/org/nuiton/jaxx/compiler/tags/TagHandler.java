/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.w3c.dom.Element;

import java.io.IOException;

/**
 * Implementations of <code>TagHandler</code> produce Java source code from XML tags.
 * <code>TagHandlers</code> are mapped to particular XML tags (such as &lt;JFrame&gt;) in {@link JAXXCompiler}.
 * There is only one <code>TagHandler</code> for any given XML tag, and therefore implementations must be
 * stateless.
 *
 * @author Ethan Nicholas
 */
public interface TagHandler {

    String XMLNS_ATTRIBUTE = "xmlns";

    /**
     * Performs the first pass of compilation on an XML tag from a JAXX source file.
     * <code>TagHandler</code> implementations affect the generated <code>.java</code>
     * file by calling methods in the <code>JAXXCompiler</code>.
     *
     * @param tag      the XML tag to compile
     * @param compiler the active JAXXCompiler
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    void compileFirstPass(Element tag,
                          JAXXCompiler compiler) throws CompilerException, IOException;

    /**
     * Performs the second pass of compilation on an XML tag from a JAXX source file.
     * <code>TagHandler</code> implementations affect the generated <code>.java</code>
     * file by calling methods in the <code>JAXXCompiler</code>.
     *
     * @param tag      the XML tag to compile
     * @param compiler the active JAXXCompiler
     * @throws CompilerException if a compilation error occurs
     * @throws IOException       if an I/O error occurs
     */
    void compileSecondPass(Element tag,
                           JAXXCompiler compiler) throws CompilerException, IOException;
}
