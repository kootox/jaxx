/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding.writers;

import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.binding.DataBinding;
import org.nuiton.jaxx.compiler.binding.DataListener;
import org.nuiton.jaxx.compiler.finalizers.DefaultFinalizer;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;
import org.nuiton.jaxx.compiler.java.JavaMethod;
import org.nuiton.jaxx.runtime.JAXXBinding;

import java.util.List;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Id$
 */
public abstract class AbstractJAXXBindingWriter<B extends JAXXBinding> implements JAXXBindingWriter<B> {

    private final Class<B> type;

    protected boolean used;

    protected AbstractJAXXBindingWriter(Class<B> type) {
        this.type = type;
    }

    @Override
    public boolean isUsed() {
        return used;
    }

    @Override
    public Class<B> getType() {
        return type;
    }

    @Override
    public void reset() {
        used = false;
    }

    protected abstract String getConstructorParams(DataBinding binding,
                                                   DataListener[] trackers);

    protected void writeInvocationMethod(DataBinding binding,
                                         DataListener[] trackers,
                                         JavaFileGenerator generator,
                                         StringBuilder buffer,
                                         List<JavaMethod> bMethods) {
        used = true;
        String eol = JAXXCompiler.getLineSeparator();
        buffer.append(DefaultFinalizer.METHOD_NAME_REGISTER_DATA_BINDING);
        buffer.append("(new ");
        buffer.append(getType().getSimpleName());
        buffer.append("(");
        buffer.append(getConstructorParams(binding, trackers));
        buffer.append(") {");
        buffer.append(eol);
        for (JavaMethod m : bMethods) {
            buffer.append(eol);
            String source = generator.generateMethod(m);
            buffer.append(JavaFileGenerator.indent(source, 4, false, eol));
            buffer.append(eol);
        }
        buffer.append("});").append(eol);

        if (binding.getInitDataBinding() != null) {
            buffer.append(binding.getInitDataBinding());
        }
    }
}
