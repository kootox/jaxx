/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.tasks;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXCompilerFile;
import org.nuiton.jaxx.compiler.JAXXEngine;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Last task to generate java files.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public class GenerateMissingRulesTask extends JAXXEngineTask {

    /** Logger */
    private static final Log log = LogFactory.getLog(GenerateMissingRulesTask.class);

    /** Task name */
    public static final String TASK_NAME = "GenerateMissingRules";

    public GenerateMissingRulesTask() {
        super(TASK_NAME);
    }

    @Override
    public boolean perform(JAXXEngine engine) throws Exception {
        boolean success = true;
        boolean isVerbose = engine.getConfiguration().isVerbose();

        JAXXCompilerFile[] files = engine.getCompiledFiles();

        for (JAXXCompilerFile jaxxFile : files) {
            String className = jaxxFile.getClassName();
            if (isVerbose) {
                log.info("start " + className);
            }

            JAXXCompiler compiler = jaxxFile.getCompiler();
            addStartProfileTime(engine, compiler);

            if (compiler.isIdentCssFound()) {

                File cssFile = jaxxFile.getCssFile();

                String cssContent = new String(Files.readAllBytes(cssFile.toPath()), StandardCharsets.UTF_8);

                Map<String, CompiledObject> objects = compiler.getObjects();

                Set<String> cssRulesToAdd = new LinkedHashSet<>();

                for (String id : objects.keySet()) {

                    CompiledObject object = objects.get(id);
                    String styleClass = object.getStyleClass();

                    if (isVerbose) {
                        log.info("id : " + id + ", styleClass : " + styleClass);
                    }

                    if (!id.startsWith("$")) {
                        Pattern idPattern = Pattern.compile("#" + id + "\\s*\\{.*\\}", Pattern.DOTALL);
                        Matcher idMatcher = idPattern.matcher(cssContent);

                        if (!idMatcher.find()) {
                            if (log.isInfoEnabled()) {
                                log.info("add css rule for #" + id + " in " + cssFile.getName());
                            }
                            cssRulesToAdd.add("\n\n/* #" + id + " {} */");
                        }
                    }

                    if (styleClass != null) {
                        Pattern classPattern = Pattern.compile("\\." + styleClass + "\\s*\\{.*\\}", Pattern.DOTALL);
                        Matcher classMatcher = classPattern.matcher(cssContent);

                        if (!classMatcher.find()) {
                            if (log.isInfoEnabled()) {
                                log.info("add css rule for ." + styleClass + " in " + cssFile.getName());
                            }
                            cssRulesToAdd.add("\n\n/* ." + styleClass + " {} */");
                        }
                    }

                }

                Files.write(cssFile.toPath(), StringUtils.join(cssRulesToAdd, "").getBytes(), StandardOpenOption.APPEND);
            }

            addEndProfileTime(engine, compiler);

            if (compiler.isFailed()) {
                success = false;
            }
        }
        return success;
    }
}
