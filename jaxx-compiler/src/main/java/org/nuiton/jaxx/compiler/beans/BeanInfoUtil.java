/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** @author Tony Chemit - dev@tchemit.fr */
public class BeanInfoUtil {

    public static String[] originalBeanInfoSearchPath;

    public static void addJaxxBeanInfoPath(String... packageNames) {

        String[] searchPath = Introspector.getBeanInfoSearchPath();
        if (originalBeanInfoSearchPath == null) {
            originalBeanInfoSearchPath = searchPath;
        }
        List<String> listSearchPath = new ArrayList<>(Arrays.asList(searchPath));
        for (String packageName : packageNames) {
            if (!listSearchPath.contains(packageName)) {
                listSearchPath.add(packageName);
            }
        }

        Introspector.setBeanInfoSearchPath(listSearchPath.toArray(new String[listSearchPath.size()]));
    }

    public static void reset() {
        if (originalBeanInfoSearchPath != null) {
            Introspector.setBeanInfoSearchPath(originalBeanInfoSearchPath);
            originalBeanInfoSearchPath = null;
        }
    }

}
