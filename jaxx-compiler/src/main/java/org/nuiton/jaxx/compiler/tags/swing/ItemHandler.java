/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.UnsupportedAttributeException;
import org.nuiton.jaxx.compiler.binding.DataBindingHelper;
import org.nuiton.jaxx.compiler.tags.TagHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.Item;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;

public class ItemHandler implements TagHandler {

    public static final String TAG_NAME = "item";

    private final String DATA_BINDING = "<data binding has not been processed yet>";

    public static final String ATTRIBUTE_ID = "id";

    @Override
    public void compileFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compileChildrenFirstPass(tag, compiler);
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        String id = tag.getAttribute(ATTRIBUTE_ID);
        if (id == null || id.length() == 0) {
            id = compiler.getAutoId(Item.class.getSimpleName());
//            id = compiler.getAutoId(ClassDescriptorHelper.getClassDescriptor(Item.class));
        }
        String label = null;
        String value = null;
        boolean selected = false;
        NamedNodeMap children = tag.getAttributes();

        for (int i = 0; i < children.getLength(); i++) {
            Attr attribute = (Attr) children.item(i);
            String name = attribute.getName();
            String attrValue = attribute.getValue();
            if (name.equals(ATTRIBUTE_ID)) {
                // already handled
                continue;
            }
            DataBindingHelper bindingHelper = compiler.getBindingHelper();
            if (name.equals(Item.LABEL_PROPERTY)) {
                String labelBinding = compiler.processDataBindings(attrValue);
                if (labelBinding != null) {
                    bindingHelper.registerDataBinding(id + ".label", labelBinding, id + ".setLabel(" + labelBinding + ");");
//                    bindingHelper.registerDataBinding(labelBinding, id + ".label", id + ".setLabel(" + labelBinding + ");");
                } else {
                    label = attrValue;
                }
                continue;
            }
            if (name.equals(Item.VALUE_PROPERTY)) {
                String valueBinding = compiler.processDataBindings(attrValue);
                if (valueBinding != null) {
                    value = DATA_BINDING;
                    bindingHelper.registerDataBinding(id + ".value", valueBinding, id + ".setValue(" + valueBinding + ");");
//                    bindingHelper.registerDataBinding(valueBinding, id + ".value", id + ".setValue(" + valueBinding + ");");
                } else {
                    value = attrValue;
                }
                continue;
            }
            if (name.equals(Item.SELECTED_PROPERTY)) {
                String selectedBinding = compiler.processDataBindings(attrValue);
                if (selectedBinding != null) {
                    bindingHelper.registerDataBinding(id + ".selected", selectedBinding, id + ".setSelected(" + selectedBinding + ");");
//                    bindingHelper.registerDataBinding(selectedBinding, id + ".selected", id + ".setSelected(" + selectedBinding + ");");
                } else {
                    selected = (Boolean) TypeManager.convertFromString(attrValue, Boolean.class);
                }
                continue;
            }

            if (!name.startsWith("xmlns") && !JAXXCompiler.JAXX_INTERNAL_NAMESPACE.equals(attribute.getNamespaceURI())) {
                throw new UnsupportedAttributeException(name);
            }
        }

        Item item = new Item(id, label, value, selected);
        CompiledItemContainer list = (CompiledItemContainer) compiler.getOpenComponent();
        if (value == null) {
            compiler.reportError("<item> tag is missing required 'value' attribute");
        } else {
            if (!value.equals(DATA_BINDING)) {
                List<Item> items = list.getItems();
                for (Item item1 : items) {
                    if (item1.getValue().equals(value)) {
                        compiler.reportError("This container already has an <item> tag with the value '" + value + "'");
                        break;
                    }
                }
            }
            list.openItem(item);
            compileChildrenSecondPass(tag, compiler);
            list.closeItem(item);
        }
    }

    protected void compileChildrenFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagFirstPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildrenSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            int nodeType = node.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                Element child = (Element) node;
                compileChildTagSecondPass(child, compiler);
            } else if (nodeType == Node.TEXT_NODE || nodeType == Node.CDATA_SECTION_NODE) {
                String text = ((Text) node).getData().trim();
                if (text.length() > 0) {
                    compiler.reportError("tag '" + tag.getLocalName() + "' may not contain text ('" + ((Text) node).getData().trim() + "')");
                }
            }
        }
    }

    protected void compileChildTagFirstPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileFirstPass(tag);
    }

    protected void compileChildTagSecondPass(Element tag, JAXXCompiler compiler) throws CompilerException, IOException {
        compiler.compileSecondPass(tag);
    }
}
