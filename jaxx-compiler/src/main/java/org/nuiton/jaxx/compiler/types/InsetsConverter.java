/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import com.google.auto.service.AutoService;

import java.awt.Insets;
import java.util.StringTokenizer;

@AutoService(TypeConverter.class)
public class InsetsConverter implements TypeConverter {

    @Override
    public Class<?>[] getSupportedTypes() {
        return new Class<?>[]{Insets.class};
    }

    @Override
    public String getJavaCode(Object object) {
        Insets insets = (Insets) object;
        return "new Insets(" + insets.top + ", " + insets.left + ", " + insets.bottom + ", " + insets.right + ")";
    }

    @Override
    public Object convertFromString(String string, Class<?> type) {
        if (!Insets.class.equals(type)) {
            throw new IllegalArgumentException("unsupported type: " + type);
        }
        StringTokenizer tokenizer = new StringTokenizer(string, ",");
        int count = tokenizer.countTokens();
        if (count == 1) {
            int i = Integer.parseInt(tokenizer.nextToken().trim());
            return new Insets(i, i, i, i);
        }
        if (count == 4) {
            int[] insets = new int[count];
            for (int i = 0; tokenizer.hasMoreTokens(); i++) {
                insets[i] = Integer.parseInt(tokenizer.nextToken().trim());
            }
            return new Insets(insets[0], insets[1], insets[2], insets[3]);
        }
        throw new IllegalArgumentException("unable to convert string '" + string + "' to Insets");
    }
}
