/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.java;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * To mirror a {@link Constructor}.
 *
 * In a constructor we need to keep all parameters types as fqn since when
 * a jaxx object inheritates from anohter one, it is painfull (even impossible?)
 * to find out from a simple name his fqn.
 *
 * So when using constructor, always keep fqn types.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
public class JavaConstructor extends JavaElement {

    /** arguments of the method (can be empty) */
    private final JavaArgument[] arguments;

    /** exceptions thrown by the method (can be empty) */
    private final String[] exceptions;

    /** body of the method (can be empty) */
    private final String body;

    /**
     * Constructs a new <code>JavaMethod</code> containing the specified body code.  The <code>modifiers</code> parameter
     * is a bit mask of the  constants from {@link Modifier}, and the <code>returnType</code> and
     * <code>exceptions</code> of the method should be represented as they would appear in Java source code (<code>null</code>
     * for a constructor).  The method body is initially empty.
     *
     * @param modifiers  the modifier keywords that should appear as part of the method's declaration
     * @param name       the method's name
     * @param arguments  the method's arguments
     * @param exceptions a list of exceptions the methods can throw, as they would be represented in Java source code
     * @param bodyCode   Java source code which should appear in the method body
     */
    JavaConstructor(int modifiers,
                    String name,
                    JavaArgument[] arguments,
                    String[] exceptions,
                    String bodyCode) {
        super(modifiers, name);
        this.arguments = arguments;
        this.exceptions = exceptions;
        body = bodyCode == null ? "" : bodyCode;
    }

    /**
     * Returns a list of the method's arguments.
     *
     * @return the method's arguments
     */
    public JavaArgument[] getArguments() {
        return arguments;
    }

    /**
     * Returns a list of exceptions the method can throw.
     *
     * @return the method's exceptions
     */
    public String[] getExceptions() {
        return exceptions;
    }

    public String getBody() {
        return body;
    }

}
