/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler;

/** Thrown by the compiler when an error occurs. */
public class CompilerException extends RuntimeException {

    private static final long serialVersionUID = -9099889519671482440L;

    /** Creates a new <code>ParseException</code>. */
    public CompilerException() {
    }

    /**
     * Creates a new <code>ParseException</code> with the specified detail message.
     *
     * @param msg the exception's detail message
     */
    public CompilerException(String msg) {
        super(msg);
    }

    /**
     * Creates a new <code>ParseException</code> with the specified cause.
     *
     * @param initCause the exception's initCause
     */
    public CompilerException(Throwable initCause) {
        super(initCause);
    }

    /**
     * Creates a new <code>ParseException</code> with the specified detail message and cause.
     *
     * @param msg       the exception's detail message
     * @param initCause the exception's initCause
     */
    public CompilerException(String msg, Throwable initCause) {
        super(msg, initCause);
    }

//    @Override
//    public void printStackTrace() {
//        super.printStackTrace();
//        System.err.println("CompilerException printed from:");
//        Thread.dumpStack();
//    }
}
