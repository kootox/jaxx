/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tools;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.compiler.DefaultCompilerConfiguration;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.JAXXFactory;
import org.nuiton.jaxx.compiler.beans.JAXXPropertyDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.reflect.MethodDescriptor;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.tags.TagManager;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/** Generates information about a tag for use on the jaxxframework.org web site. */
public class PrintTagInfo {
    /**
     * Displays information about the class name in arg[0].
     *
     * @param arg command-line arguments
     * @throws Exception if an error occurs
     */
    public static void main(String[] arg) throws Exception {
        if (arg.length < 1) {
            throw new IllegalArgumentException("programm needs at least two parameters : the file where to put the result, and at least one fqn class to treate");
        }
        String firstarg = arg[0];
        boolean toFile = false;
        BufferedWriter w;
        if (firstarg.startsWith("file:")) {
            w = new BufferedWriter(new FileWriter(firstarg.substring(5)));
            toFile = true;
        } else {
            w = new BufferedWriter(new OutputStreamWriter(System.out));
        }

        try {
            JAXXFactory.setConfiguration(new DefaultCompilerConfiguration());
            JAXXFactory.initFactory();
            for (int i = toFile ? 1 : 0; i < arg.length; i++) {
                String className = arg[i];
                treateClass(w, className);
            }
        } finally {
            w.flush();
            w.close();
        }

    }

    protected static void treateClass(BufferedWriter w, String className) throws ClassNotFoundException, IOException {

        ClassDescriptor beanClass = ClassDescriptorHelper.getClassDescriptor(className);
        DefaultObjectHandler handler = TagManager.getTagHandler(beanClass);

        DefaultObjectHandler superHandler = TagManager.getTagHandler(beanClass.getSuperclass());

        // dump all bean properties
        w.append("Properties in ").append(String.valueOf(beanClass));
        w.newLine();
        JAXXPropertyDescriptor[] properties = handler.getJAXXBeanInfo().getJAXXPropertyDescriptors();
        JAXXPropertyDescriptor[] superProperties = superHandler.getJAXXBeanInfo().getJAXXPropertyDescriptors();
        for (JAXXPropertyDescriptor property : properties) {
            if (property.getWriteMethodDescriptor() == null) {
                continue;
            }

            boolean found = false;
            String name = property.getName();
            for (JAXXPropertyDescriptor superProperty : superProperties) {
                if (superProperty.getName().equals(name)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (property.getPropertyType() == null) {
                    System.err.println(name + " has null type");
                } else {
                    w.append("{{EquivalentAttribute|");
                    w.append(name);
                    w.append("|");
                    w.append(className.replace('.', '/'));
                    w.append("|set");
                    w.append(StringUtils.capitalize(name));
                    w.append("|");
                    w.append(JAXXCompiler.getCanonicalName(property.getPropertyType()));
                    w.append("}}");
                    w.append("|-");
                    w.newLine();
                }
            }
        }

        w.newLine();
        w.newLine();

        // dump all bound methods
        dumpMethods(w, beanClass, handler);
    }

    protected static void dumpMethods(BufferedWriter w, ClassDescriptor beanClass, DefaultObjectHandler handler) throws IOException {
        MethodDescriptor[] methods = beanClass.getMethodDescriptors();
        w.append("Bound methods in ").append(String.valueOf(beanClass));
        w.newLine();
        for (MethodDescriptor method : methods) {
            try {
                if (handler.isMemberBound(method.getName())) {
                    w.append("* <tt>").append(method.getName()).append("()</tt>");
                    w.newLine();
                }
            } catch (Throwable e) {
                // ignore ?
            }
        }
    }
}
