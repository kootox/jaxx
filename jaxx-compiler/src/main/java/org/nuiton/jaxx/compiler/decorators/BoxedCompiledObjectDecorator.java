/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.decorators;

import com.google.auto.service.AutoService;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompiledObject.ChildRef;
import org.nuiton.jaxx.compiler.CompiledObjectDecorator;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * A decorator to surround a compiled object (should be a component at least)
 * with a JXLayer.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role-hint="boxed" role="org.nuiton.jaxx.compiler.CompiledObjectDecorator"
 * @since 1.2
 */
@AutoService(CompiledObjectDecorator.class)
public class BoxedCompiledObjectDecorator extends DefaultCompiledObjectDecorator {

    @Override
    public String getName() {
        return "boxed";
    }

    @Override
    public void finalizeCompiler(JAXXCompiler compiler,
                                 CompiledObject root,
                                 CompiledObject object,
                                 JavaFile javaFile,
                                 String packageName,
                                 String className,
                                 String fullClassName) throws ClassNotFoundException {
        CompiledObject parent = object.getParent();
        if (parent == null) {
            parent = root;
        }
        for (ChildRef child : parent.getChilds()) {
            if (child.getChild() == object) {
                String javaCode = child.getChildJavaCode();
                String type = compiler.getImportedType(SwingUtil.class);
                child.setChildJavaCode(
                        type +
                                ".boxComponentWithJxLayer(" + javaCode + ")");
                break;
            }
        }
        super.finalizeCompiler(compiler,
                               root,
                               object,
                               javaFile,
                               packageName,
                               className,
                               fullClassName
        );
    }
}
