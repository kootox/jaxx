/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.finalizers;

import com.google.auto.service.AutoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.runtime.swing.Application;

import javax.swing.SwingUtilities;
import java.lang.reflect.Modifier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role-hint="swing" role="org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer"
 */
@AutoService(JAXXCompilerFinalizer.class)
public class SwingFinalizer extends AbstractFinalizer {

    /** Logger. */
    protected static final Log log = LogFactory.getLog(DefaultFinalizer.class);

    @Override
    public boolean accept(JAXXCompiler compiler) {

        ClassDescriptor descriptor =
                ClassDescriptorHelper.getClassDescriptor(Application.class);
        CompiledObject root = compiler.getRootObject();
        return descriptor.isAssignableFrom(root.getObjectClass());
    }

    @Override
    public void finalizeCompiler(CompiledObject root,
                                 JAXXCompiler compiler,
                                 JavaFile javaFile,
                                 String packageName,
                                 String className) {
    }

    @Override
    public void prepareJavaFile(CompiledObject root,
                                JAXXCompiler compiler,
                                JavaFile javaFile,
                                String packageName,
                                String className) {

        if (compiler.isMainDeclared()) {

            // main method was already defined, can not generate this method.
            return;
        }

        javaFile.addImport(SwingUtilities.class);
        String code = "SwingUtilities.invokeLater(new Runnable() { " +
                "public void run() { new " + className +
                "().setVisible(true); } });";
        javaFile.addMethod(JavaElementFactory.newMethod(
                Modifier.PUBLIC | Modifier.STATIC,
                TYPE_VOID,
                "main",
                code,
                false,
                JavaElementFactory.newArgument("String[]", "arg")));
    }
}
