/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.beans;

import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;

/**
 * Mirrors the class <code>java.beans.BeanInfo</code>.  JAXX uses its own introspector rather than the built-in
 * <code>java.beans.Introspector</code> so that it can introspect {@link ClassDescriptor},
 * not just <code>java.lang.Class</code>.
 */
public class JAXXBeanInfo {

    private final JAXXBeanDescriptor beanDescriptor;

    private final JAXXPropertyDescriptor[] propertyDescriptors;

    private final JAXXEventSetDescriptor[] eventSetDescriptors;

    public JAXXBeanInfo(JAXXBeanDescriptor beanDescriptor,
                        JAXXPropertyDescriptor[] propertyDescriptors,
                        JAXXEventSetDescriptor[] eventSetDescriptors) {
        this.beanDescriptor = beanDescriptor;
        this.propertyDescriptors = propertyDescriptors;
        this.eventSetDescriptors = eventSetDescriptors;
    }

    public JAXXBeanDescriptor getJAXXBeanDescriptor() {
        return beanDescriptor;
    }

    public JAXXPropertyDescriptor[] getJAXXPropertyDescriptors() {
        return propertyDescriptors;
    }

    public JAXXEventSetDescriptor[] getJAXXEventSetDescriptors() {
        return eventSetDescriptors;
    }
}
