/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.types;

import com.google.auto.service.AutoService;

import java.awt.GridBagConstraints;

@AutoService(TypeConverter.class)
public class GridBagConstraintsConverter implements TypeConverter {

    @Override
    public Class<?>[] getSupportedTypes() {
        return new Class<?>[]{
                GridBagConstraints.class
        };
    }

    @Override
    public String getJavaCode(Object object) {
        GridBagConstraints g = (GridBagConstraints) object;
        return "new GridBagConstraints(" + g.gridx + ", " + g.gridy + ", " + g.gridwidth + ", " + g.gridheight + ", " +
                g.weightx + ", " + g.weighty + ", " + g.anchor + ", " + g.fill + ", " +
                TypeManager.getJavaCode(g.insets) + ", " + g.ipadx + ", " + g.ipady + ")";
    }

    @Override
    public Object convertFromString(String string, Class<?> type) {
        throw new UnsupportedOperationException("GridBagConstraints must be represented using Java code");
    }
}
