/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import javax.swing.JSlider;
import javax.swing.event.ChangeListener;

public class JSliderHandler extends DefaultComponentHandler {
    public static final String ATTRIBUTE_VALUE = "value";

    public JSliderHandler(ClassDescriptor beanClass) {
        super(beanClass);
        if (!ClassDescriptorHelper.getClassDescriptor(JSlider.class).isAssignableFrom(beanClass)) {
            throw new IllegalArgumentException(getClass().getName() + " does not support the class " + beanClass.getName());
        }
    }

    @Override
    protected int getAttributeOrdering(Attr attr) {
        if (attr.getName().equals(ATTRIBUTE_VALUE)) {
            return 1;
        } else {
            return super.getAttributeOrdering(attr);
        }
    }

    @Override
    protected void setDefaults(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        super.setDefaults(object, tag, compiler);
        setAttribute(object, ATTRIBUTE_VALUE, "0", false, compiler);
    }

    @Override
    protected void configureProxyEventInfo() {
        super.configureProxyEventInfo();
        addProxyEventInfo("getValue", ChangeListener.class, "model");
    }
}
