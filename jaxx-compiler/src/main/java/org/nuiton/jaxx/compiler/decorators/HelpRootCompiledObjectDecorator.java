/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.decorators;

import com.google.auto.service.AutoService;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompiledObjectDecorator;
import org.nuiton.jaxx.compiler.CompilerConfiguration;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.runtime.swing.help.JAXXHelpUI;

import java.awt.Component;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.nuiton.jaxx.compiler.java.JavaElementFactory.newArgument;

/**
 * A decorator to place on a root compiled object to process javaHelp on the file.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role-hint="help" role="org.nuiton.jaxx.compiler.CompiledObjectDecorator"
 * @since 1.2
 */
@AutoService(CompiledObjectDecorator.class)
public class HelpRootCompiledObjectDecorator extends DefaultCompiledObjectDecorator {

    /** the list of discovered helpId */
    protected static final Set<String> helpIds = new HashSet<>();

    @Override
    public String getName() {
        return "help";
    }

    protected String getBrokerFQN(JAXXCompiler compiler) {
        return compiler.getConfiguration().getHelpBrokerFQN();
    }

    protected String getHelpId(CompiledObject o) {
        String helpID = null;
        if (o.hasClientProperties()) {
            helpID = o.getClientProperty("help");
        }
        return helpID;
    }

    @Override
    public void finalizeCompiler(JAXXCompiler compiler,
                                 CompiledObject root,
                                 CompiledObject object,
                                 JavaFile javaFile,
                                 String packageName,
                                 String className,
                                 String fullClassName) throws ClassNotFoundException {
        super.finalizeCompiler(compiler,
                               root,
                               object,
                               javaFile,
                               packageName,
                               className,
                               fullClassName)
        ;
        CompilerConfiguration options = compiler.getConfiguration();

        if (options.isGenerateHelp()) {

            // add JAXXHelpUI interface
            Class<?> validatorInterface = JAXXHelpUI.class;
            String helpBrokerFQN = getBrokerFQN(compiler);

            boolean needInterface = isNeedInterface(compiler,
                                                    validatorInterface);

            if (needInterface) {

                // only add the contract if needed

                if (log.isDebugEnabled()) {
                    log.debug("Add " + validatorInterface + " on " +
                                      javaFile.getName() + " : parent " +
                                      JAXXCompiler.getCanonicalName(
                                              compiler.getRootObject()));
                }

                javaFile.addInterface(validatorInterface.getName() +
                                              "<" + helpBrokerFQN + ">");
            }

            javaFile.addMethod(JavaElementFactory.newMethod(
                    Modifier.PUBLIC,
                    JAXXCompilerFinalizer.TYPE_VOID,
                    "registerHelpId",
                    "broker.installUI(component, helpId);",
                    true,
                    newArgument(helpBrokerFQN, "broker"),
                    newArgument(Component.class.getName(), "component"),
                    newArgument(JAXXCompilerFinalizer.TYPE_STRING, "helpId"))
            );

            javaFile.addMethod(JavaElementFactory.newMethod(
                    Modifier.PUBLIC,
                    JAXXCompilerFinalizer.TYPE_VOID,
                    "showHelp",
                    "getBroker().showHelp(this, helpId);",
                    true,
                    newArgument(JAXXCompilerFinalizer.TYPE_STRING, "helpId"))
            );

            StringBuilder buffer = new StringBuilder();

            String eol = JAXXCompiler.getLineSeparator();

//            if (options.isGenerateHelp()) {

            // add code to init javax help system
            Iterator<CompiledObject> itr = compiler.getObjectCreationOrder();

            for (; itr.hasNext(); ) {
                CompiledObject o = itr.next();
                String helpID = getHelpId(o);
                if (helpID != null) {
                    buffer.append(eol);
                    // detects a helpId to register
                    buffer.append("registerHelpId(_broker, ");
                    buffer.append(o.getJavaCode());
                    buffer.append(", ");
                    buffer.append(helpID);
                    buffer.append(");");
                    //keep the helpID for helpSet generation
                    helpIds.add(helpID);
                }
            }
//            }
            if (buffer.length() > 0) {

                String type = compiler.getImportedType(helpBrokerFQN);

                buffer.append(eol).append("_broker.prepareUI(this);");
                buffer.append(eol);

                // add the calls
                compiler.appendLateInitializer("// help broker setup" + eol);
                compiler.appendLateInitializer(type + " _broker = getBroker();");
                compiler.appendLateInitializer(buffer.toString());
            }
        }
    }

    /**
     * Detects if the given {@code compiler} need the validatorInterface.
     *
     * We need to test it deeply since the interface is added by the decorator
     * and is not present on the symbol table of compiled objects.
     *
     * @param compiler           the compiler to test
     * @param validatorInterface the validator interface to seek for
     * @return {@code true} if we need to add the interface, {@code false} otherwise
     * @throws ClassNotFoundException if could not find a class
     * @since 2.4
     */
    protected boolean isNeedInterface(JAXXCompiler compiler,
                                      Class<?> validatorInterface) throws ClassNotFoundException {
        if (compiler.isSuperClassAware(validatorInterface)) {

            // parent has already the interface
            return false;
        }
        CompiledObject root = compiler.getRootObject();
        ClassDescriptor rootObjectClass = root.getObjectClass();
        String superClassName = JAXXCompiler.getCanonicalName(rootObjectClass);
        JAXXCompiler parentCompiler = compiler.getEngine().getJAXXCompiler(superClassName);
        if (parentCompiler == null) {

            // parent was not compiled
            return true;
        }

        CompiledObjectDecorator decorator = parentCompiler.getRootObject().getDecorator();

        if (decorator != null && decorator instanceof HelpRootCompiledObjectDecorator) {

            // parent is already with help, no need of the interface
            return false;
        }

        // ok must add the interface
        return isNeedInterface(parentCompiler, validatorInterface);
    }

    public static Set<String> getHelpIds() {
        return new HashSet<>(helpIds);
    }
}
