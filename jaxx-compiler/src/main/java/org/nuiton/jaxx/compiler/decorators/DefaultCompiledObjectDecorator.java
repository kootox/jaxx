/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.decorators;

import com.google.auto.service.AutoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompiledObjectDecorator;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaField;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.java.JavaMethod;
import org.nuiton.jaxx.compiler.script.ScriptInitializer;
import org.nuiton.jaxx.compiler.types.TypeManager;

import java.lang.reflect.Modifier;
import java.util.Map.Entry;

/**
 * The default decorator to use on all compiled objects.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role-hint="default" role="org.nuiton.jaxx.compiler.CompiledObjectDecorator"
 * @since 1.2
 */
@AutoService(CompiledObjectDecorator.class)
public class DefaultCompiledObjectDecorator implements CompiledObjectDecorator {


    /** Logger */
    protected static final Log log =
            LogFactory.getLog(DefaultCompiledObjectDecorator.class);

    @Override
    public String getName() {
        return "default";
    }

    @Override
    public void finalizeCompiler(JAXXCompiler compiler,
                                 CompiledObject root,
                                 CompiledObject object,
                                 JavaFile javaFile,
                                 String packageName,
                                 String className,
                                 String fullClassName) throws ClassNotFoundException {

        if (object instanceof ScriptInitializer) {

            // nothing to finalize
            return;
        }

        String fqn = JAXXCompiler.getCanonicalName(object);

        String id = object.getId();

        if (log.isDebugEnabled()) {
            log.debug("finalize " + id);
        }

        boolean override = object.isOverride();

        if (override) {

            if (object.isOverrideType()) {

                // add a specialized getter, only if type has changed

                String methodName = object.getGetterName();

                String type = object.getSimpleType();
                String body = "return (" + type + ") super." + methodName + "();";

                if (log.isDebugEnabled()) {
                    log.debug("Add specialized getter " + methodName + " : " + body);
                }
                JavaMethod getter = JavaElementFactory.newMethod(
                        Modifier.PUBLIC,
                        fqn,
                        methodName,
                        body,
                        true
                );
                javaFile.addMethod(getter);
            }
        } else {

            int access = id.startsWith("$") ? Modifier.PRIVATE :
                    Modifier.PROTECTED;
            if (root.equals(object)) {

                // add the generic type if required
                String type = className + root.getGenericTypes();

                JavaField field = JavaElementFactory.newField(access,
                                                              type,
                                                              id,
                                                              false,
                                                              null
                );

                javaFile.addSimpleField(field);
            } else {

                JavaField field = JavaElementFactory.newField(access,
                                                              fqn,
                                                              id,
                                                              override
                );
                javaFile.addField(field, object.isJavaBean());
            }
        }

        if (compiler.inlineCreation(object) || root.equals(object)) {

            // nothing more to do here
            return;
        }

        String code = getCreationCode(compiler, object);

        // tchemit 20100519 Do not add the method only if overriden and code is null

        if (code != null) {

            JavaMethod javaMethod = JavaElementFactory.newMethod(
                    Modifier.PROTECTED,
                    JAXXCompilerFinalizer.TYPE_VOID,
                    object.getCreationMethodName(),
                    code,
                    override
            );
            javaFile.addMethod(javaMethod);
        }
    }

    @Override
    public String getCreationCode(JAXXCompiler compiler,
                                  CompiledObject object) throws CompilerException {
        if (object instanceof ScriptInitializer) {
            throw new IllegalStateException(
                    "A script initializer can not come in getCreationCode method!");
        }
        String eol = JAXXCompiler.getLineSeparator();

        StringBuilder result = new StringBuilder();
        StringBuilder init = new StringBuilder();

        if (compiler.getRootObject().equals(object) ||
                compiler.inlineCreation(object)) {
            result.append("// inline creation of ").append(object.getId());
        }

        if (object.isJavaBean() && object.getJavaBeanInitCode() != null) {
            init.append(object.getJavaBeanInitCode());
        } else if (object.getInitializer() != null) {
            init.append(object.getInitializer());
        }

        boolean addToObjectMap = true;
        String id = TypeManager.getJavaCode(object.getId());
        String constructorParams = object.getConstructorParams();

        if (object.isOverride()) {

            if (init.length() == 0 && constructorParams == null) {

                // no init code is given, no need to add to objectMap
                addToObjectMap = false;
            }
        }

        if (addToObjectMap && init.length() == 0) {

            // on special init, use constructor
            String canonicalName = JAXXCompiler.getCanonicalName(object);
            String impl = compiler.getImportedType(canonicalName);

            init.append("new ").append(impl).append("(");

            if (constructorParams != null) {
                init.append(constructorParams);
            }
            init.append(")");
        }

        String superCall = "super." + object.getCreationMethodName() + "();";

        if (addToObjectMap) {
            result.append(eol);
            result.append("$objectMap.put(");
            result.append(id);
            result.append(", ");
            result.append(object.getId()).append(" = ");
            result.append(init);
            result.append(");");
            result.append(eol);
        } else {
            if (object.isOverride()) {

                // when override has no special init code, just use the super method

                result.append(superCall);

            }
        }

        String initCode = object.getInitializationCode(compiler);
        if (StringUtils.isNotEmpty(initCode)) {
            result.append(eol).append(initCode);
        }

        // add client properties        
        addClientProperties(object, result, eol);

        String code = result.toString();

        if (!compiler.inlineCreation(object) &&
                object.isOverride() &&
                superCall.equals(code.trim())) {

            // special case : when override but do nothing more
            // method creation can be skipped
            return null;
        }
        return code;
    }

    @Override
    public String createCompleteSetupMethod(JAXXCompiler compiler,
                                            CompiledObject object,
                                            JavaFile javaFile) {
        StringBuilder code = new StringBuilder();
        String eol = JAXXCompiler.getLineSeparator();
        if (object.getId().startsWith("$")) {
            String additionCode = object.getAdditionCode();
            //TC-20091025 only generate the code if not empty
            if (!additionCode.isEmpty()) {
                code.append("// inline complete setup of ");
                code.append(object.getId());
                code.append(eol);
                code.append(additionCode);
            }
        } else {
            //TODO-TC-20091202 should always create the method to make api more consistent ?
            //TODO-TC-20091202 While generating, we deal with this case, it seems not sa natural
            //TODO-TC-20091202 to NOT having the setup method on each public property ?
//            code.append(object.getAdditionMethodName()).append("();").append(eol);
//            if (!additionCode.isEmpty()) {
//                additionCode = "if (!allComponentsCreated) {" + eol + "    return;" + eol + "}" + eol + additionCode;
//            }
//            javaFile.addMethod(JavaFileGenerator.newMethod(Modifier.PROTECTED, "void", object.getAdditionMethodName(), additionCode, false));
            String additionCode = object.getAdditionCode();
            if (additionCode.length() > 0) {
                code.append(object.getAdditionMethodName()).append("();").append(eol);
//                additionCode = "if (!allComponentsCreated) {" + eol + "    return;" + eol + "}" + eol + additionCode;
                javaFile.addMethod(JavaElementFactory.newMethod(
                        Modifier.PROTECTED,
                        JAXXCompilerFinalizer.TYPE_VOID,
                        object.getAdditionMethodName(),
                        additionCode,
                        false)
                );
            }
        }
        return code.toString();
    }

    @Override
    public boolean createInitializer(JAXXCompiler compiler,
                                     CompiledObject root,
                                     CompiledObject object,
                                     StringBuilder code,
                                     boolean lastWasMethodCall) {
        String eol = JAXXCompiler.getLineSeparator();

        if (object instanceof ScriptInitializer) {

            // initializer has special direct treatment : can not be in a method
            // just push code to compiler
            code.append(object.getInitializationCode(compiler));

            // nothing to initialize of a script
            return lastWasMethodCall;
        }

        if (root.equals(object)) {
            String rootCode = root.getInitializationCode(compiler);
            if (rootCode != null && rootCode.length() > 0) {
                code.append("// inline creation of ");
                code.append(object.getId());
                code.append(eol);
                code.append(rootCode);
                //TC-20091025 generate client properties at creation time (not at setup time)
                // in some case can save to create a setup method (when there is only client properties
                // to store)
                addClientProperties(object, code, eol);
                code.append(eol);
            }
        } else {
            if (!object.isOverride()) {
                if (compiler.inlineCreation(object)) {
                    if (lastWasMethodCall) {
                        lastWasMethodCall = false;
                    }
                    code.append(getCreationCode(compiler, object));
                } else {
                    code.append(object.getCreationMethodName()).append("();");
                    code.append(eol);
                    lastWasMethodCall = true;
                }
            }
        }
        return lastWasMethodCall;
    }

    protected void addClientProperties(CompiledObject object,
                                       StringBuilder code,
                                       String eol) {
        //TC-20090327 generate client properties
        if (object.hasClientProperties()) {
            // generate putClientProperty invocations
            for (Entry<String, String> entry :
                    object.getClientProperties().entrySet()) {
                code.append(object.getJavaCode());
                code.append(".putClientProperty(\"");
                code.append(entry.getKey());
                code.append("\", ");
                code.append(entry.getValue());
                code.append(");");
                code.append(eol);
            }
        }
    }
}
