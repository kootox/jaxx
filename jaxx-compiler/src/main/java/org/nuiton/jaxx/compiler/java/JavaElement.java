/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.java;

import org.apache.commons.collections4.CollectionUtils;

import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Base Java element
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class JavaElement {

    //    private String lineSeparator;
    private String name;

    private int modifiers;

    /**
     * List of annoations.
     *
     * @since 2.3
     */
    private Set<String> annotations;

    public JavaElement(int modifiers, String name) {
        this.modifiers = modifiers;
        this.name = name;
    }

    public final int getModifiers() {
        return modifiers;
    }

    public final void setModifiers(int modifiers) {
        this.modifiers = modifiers;
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public boolean isAbstract() {
        return Modifier.isAbstract(modifiers);
    }

    public final String getModifiersText() {
        if (modifiers == 0) {
            return "";
        } else {
            return Modifier.toString(modifiers) + ' ';
        }
    }

    public Set<String> getAnnotations() {
        if (annotations == null) {
            annotations = new HashSet<>();
        }
        return annotations;
    }

    public boolean hasAnnotations() {
        return CollectionUtils.isNotEmpty(annotations);
    }

    public void addAnnotation(String annotation) {
        getAnnotations().add(annotation);
    }

    public static final Comparator<JavaElement> JavaElementComparator = new Comparator<JavaElement>() {

        final Pattern NAME_PATTERN = Pattern.compile("(.+)([0-9]+)");

        @Override
        public int compare(JavaElement o1, JavaElement o2) {

            String n1 = o1.getName();
            String n2 = o2.getName();

            //FIXME-tchemit-2012-07-02 (see https://forge.nuiton.org/issues/2154)
//            Matcher matcher1 = NAME_PATTERN.matcher(n1);
//            Matcher matcher2 = NAME_PATTERN.matcher(n2);
//            if (matcher1.matches() && matcher2.matches()) {
//                // les deux noms finissent par un nombre
//                String p1 = matcher1.group(1);
//                String p2 = matcher1.group(2);
//                int i = p1.compareTo(p2);
//                if (i != 0) {
//                    // on est sur des noms de prefix différents, donc pas de tri sur les suffixes
//                    return i;
//                }
//                // les deux noms doivent être triés sur les suffixes entiers
//                int i1 = Integer.valueOf(matcher1.group(2));
//                int i2 = Integer.valueOf(matcher2.group(2));
//                return i1 - i2;
//            }

            // les deux noms sont simplement comparé en alpha
            return n1.compareTo(n2);
        }
    };
}
