/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.validator;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.beans.JAXXBeanInfo;
import org.nuiton.jaxx.compiler.beans.JAXXPropertyDescriptor;
import org.nuiton.jaxx.compiler.binding.DataBindingHelper;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultObjectHandler;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.jaxx.validator.swing.ui.AbstractBeanValidatorUI;
import org.w3c.dom.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class BeanValidatorHandler extends DefaultObjectHandler {

    public static final String TAG = "BeanValidator";

    public static final String BEAN_ATTRIBUTE = "bean";

    public static final String BEAN_CLASS_ATTRIBUTE = "beanClass";

    public static final String BEAN_INITIALIZER_ATTRIBUTE = "beanInitializer";

    public static final String ERROR_LIST_MODEL_ATTRIBUTE = "errorListModel";

    public static final String ERROR_TABLE_MODEL_ATTRIBUTE = "errorTableModel";

    public static final String ERROR_LIST_ATTRIBUTE = "errorList";

    public static final String ERROR_TABLE_ATTRIBUTE = "errorTable";

    public static final String ERROR_LIST_MODEL_DEFAULT = "errorListModel";

    public static final String ERROR_TABLE_MODEL_DEFAULT = "errorTableModel";

    public static final String ERROR_LIST_DEFAULT = "errorList";

    public static final String ERROR_TABLE_DEFAULT = "errorTable";

    public static final String AUTOFIELD_ATTRIBUTE = "autoField";

    public static final String UI_CLASS_ATTRIBUTE = "uiClass";

    public static final String STRICT_MODE_ATTRIBUTE = "strictMode";

    public static final String CONTEXT_ATTRIBUTE = "context";

//    /**
//     * @deprecated since 2.4.1, now use the {@link #CONTEXT_ATTRIBUTE}.
//     */
//    @Deprecated
//    public static final String CONTEXT_NAME_ATTRIBUTE = "contextName";
    //public static final String SCOPE_ATTRIBUTE = "scope";

    public static final String PARENT_VALIDATOR_ATTRIBUTE = "parentValidator";

    /** Logger */
    static final Log log = LogFactory.getLog(BeanValidatorHandler.class);

    protected static final Map<JAXXCompiler, List<CompiledBeanValidator>> validators =
            new HashMap<>();

    protected static final Map<JAXXCompiler, List<String>> validatedComponents =
            new HashMap<>();

    public BeanValidatorHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(
                getClass(),
                beanClass,
                SwingValidator.class
        );
    }

    @Override
    protected CompiledObject createCompiledObject(String id,
                                                  JAXXCompiler compiler) {
        return new CompiledBeanValidator(id, getBeanClass(), compiler);
    }

    @Override
    protected void compileChildTagFirstPass(
            Element tag,
            JAXXCompiler compiler) throws CompilerException, IOException {
        if (log.isDebugEnabled()) {
            log.debug(tag);
        }
        if (!tag.getLocalName().equals(FieldValidatorHandler.TAG)) {
            compiler.reportError(
                    "tag '" + tag.getParentNode().getLocalName() +
                            "' may only contain " + FieldValidatorHandler.TAG +
                            " as children, but found : " + tag.getLocalName());
        } else {
            compiler.compileFirstPass(tag);
        }
    }

    @Override
    public void compileSecondPass(Element tag,
                                  JAXXCompiler compiler) throws CompilerException, IOException {

        super.compileSecondPass(tag, compiler);

        CompiledBeanValidator info = (CompiledBeanValidator) objectMap.get(tag);

        boolean error = info.addErrorListModel(tag, this, compiler);

        if (!error) {
            error = info.addErrorList(tag, compiler);
        }

        if (!error) {
            error = info.addErrorTableModel(tag, this, compiler);
        }

        if (!error) {
            error = info.addErrorTable(tag, compiler);
        }

        if (!error) {
            error = info.addUiClass(this, compiler);
        }

        if (!error) {
            error = info.addBean(tag, this, compiler);
        }

        if (!error) {
            error = info.addParentValidator(tag, this, compiler);
        }

        if (error) {
            log.warn("error were detected in second compile pass " +
                             "of CompiledObject [" + info + "]");
        }

        // close the compiled object
        compiler.closeComponent(info);
    }

    @Override
    protected void setDefaults(CompiledObject object,
                               Element tag,
                               JAXXCompiler compiler) {
        // open the compiled object
        compiler.openInvisibleComponent(object);
    }

    @Override
    public void setAttribute(CompiledObject object,
                             String propertyName,
                             String stringValue,
                             boolean inline,
                             JAXXCompiler compiler) {
        if (log.isDebugEnabled()) {
            log.debug(propertyName + " : " + stringValue + " for " + object);
        }
//        if (CONTEXT_ATTRIBUTE.equals(propertyName)) {
//            if (stringValue != null && !stringValue.trim().isEmpty()) {
//                // usage of a deprecated contextName, says it to user...
//                compiler.reportWarning("You are using a validator attribute named 'contextName' which is deprecated, prefer use now a context attribute.");
//            }
//        }
        // delegate to the compiled object with is statefull
        // (but not the tag handler)
        object.addProperty(propertyName, stringValue);
    }

    /**
     * The compiled objet representing a BeanValidator to be generated in
     * JAXXObject
     *
     * @author Tony Chemit - dev@tchemit.fr
     */
    public static class CompiledBeanValidator extends CompiledObject {

        /**
         * Map of field to add into validator.
         *
         * Keys are editors, Values are bean properties.
         */
        protected final Multimap<String, String> fields;

        /**
         * Map of field to exclude.
         *
         * Keys are bean properties, Values are editors.
         */
        protected final Map<String, String> excludeFields;

        protected String bean;

        protected String beanClass;

        protected String context;

        protected String uiClass;

        protected String errorListModel;

        protected String errorList;

        protected Boolean autoField;

        protected Boolean strictMode;

        protected JAXXBeanInfo beanDescriptor;

        protected String errorTableModel;

        protected String errorTable;

        protected String parentValidator;

        public CompiledBeanValidator(String id,
                                     ClassDescriptor objectClass,
                                     JAXXCompiler compiler) {
            //TC-20090524 Use the real class descriptor, not the one by default,
            //TC-20090524 otherwise can not override the validator class while generation
            //super(id, objectClass, compiler);
            super(id, getDescriptor(objectClass, compiler), compiler);
            fields = ArrayListMultimap.create();
            excludeFields = new TreeMap<>();
            if (log.isDebugEnabled()) {
                log.debug("validator objectClass " + getObjectClass());
            }
        }

        protected static ClassDescriptor getDescriptor(
                ClassDescriptor objectClass,
                JAXXCompiler compiler) {
            Class<?> validatorClass = SwingValidator.class;
//                    compiler.getConfiguration().getValidatorClass();
            return ClassDescriptorHelper.getClassDescriptor(validatorClass);
        }

        public Multimap<String, String> getFields() {
            return fields;
        }

        public boolean containsFieldEditor(String editorName) {
            return fields.containsKey(editorName);
        }

        public boolean containsFieldPropertyName(String propertyName) {
            return fields.containsValue(propertyName);
        }

        public Set<String> getFieldEditors() {
            return fields.keySet();
        }

//        public Set<String> getFieldIds() {
//            return fields.values();
//        }

        public boolean containsExcludeFieldEditor(String editorName) {
            return excludeFields.containsValue(editorName);
        }

        protected boolean containsExcludeFieldPropertyName(String editorName) {
            return excludeFields.containsKey(editorName);
        }

//        public Map<String, String> getExcludeFields() {
//            return excludeFields;
//        }

//        public Set<String> getExcludeFieldEditors() {
//            return new HashSet<String>(excludeFields.values());
//        }

        public Set<String> getExcludeFieldPropertyNames() {
            return excludeFields.keySet();
        }

        protected void removeFieldPropertyName(String propertyName) {
            //must find the editor for this property
            for (String editor : fields.keySet()) {
                if (fields.containsEntry(editor, propertyName)) {

                    fields.remove(editor, propertyName);
                    break;
                }
            }
        }

        public void addField(String propertyName, String editor) {
            fields.put(editor, propertyName);
        }

        public void addExcludeField(String propertyName, String editor) {
            excludeFields.put(propertyName, editor);
        }

        public Collection<String> getFieldPropertyNames(String editor) {
            return fields.get(editor);
        }

//        public void setFields(Map<String, String> fields) {
//            this.fields = fields;
//        }

//        public void setExcludeFields(Map<String, String> excludeFields) {
//            this.excludeFields = excludeFields;
//        }

        @Override
        public void addProperty(String property, String value) {

            if (BEAN_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    bean = value.trim();
                }
                return;
            }

//            if (CONTEXT_NAME_ATTRIBUTE.equals(property)) {
//                if (value != null && !value.trim().isEmpty()) {
//                    context= value.trim();
//                }
//
//                return;
//            }

            if (CONTEXT_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    context = value.trim();
                }
                return;
            }

            if (BEAN_CLASS_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    beanClass = value.trim();
                }
                return;
            }

            if (ERROR_LIST_MODEL_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorListModel = value.trim();
                }
                return;
            }

            if (ERROR_LIST_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorList = value.trim();
                }
                return;
            }

            if (ERROR_TABLE_MODEL_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorTableModel = value.trim();
                }
                return;
            }

            if (ERROR_TABLE_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    errorTable = value.trim();
                }
                return;
            }

            if (UI_CLASS_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    uiClass = value.trim();
                }
                return;
            }

            if (AUTOFIELD_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    autoField = (Boolean) TypeManager.convertFromString(
                            value.trim(),
                            Boolean.class
                    );
                }
                return;
            }

            if (STRICT_MODE_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    strictMode = (Boolean) TypeManager.convertFromString(
                            value.trim(),
                            Boolean.class
                    );
                }
                return;
            }

            if (PARENT_VALIDATOR_ATTRIBUTE.equals(property)) {
                if (value != null && !value.trim().isEmpty()) {
                    parentValidator = value.trim();
                }
                return;
            }

            throw new CompilerException("property " + property +
                                                " is not allowed on object " + this);
        }

        public String getBean() {
            return bean;
        }

        public String getErrorListModel() {
            return errorListModel;
        }

        public boolean getAutoField() {
            return autoField != null && autoField;
        }

        public boolean getStrictMode() {
            return strictMode != null && strictMode;
        }

        public String getUiClass() {
            return uiClass;
        }

        public String getBeanClass() {
            return beanClass;
        }

        public String getContext() {
            return context;
        }

        public String getParentValidator() {
            return parentValidator;
        }

        public JAXXBeanInfo getBeanDescriptor(JAXXCompiler compiler) {
            if (beanDescriptor == null && foundBean()) {

//                String beanClassName = null;
                try {
                    //TC-20090111 beanClass is mandatory
                    // get the real bean class name (from bean or beanClass)
                    /*if (beanClass != null) {
                    beanClassName = beanClass;
                    } else {
                    beanClassName = compiler.getSymbolTable().getClassTagIds().get(bean);
                    if (beanClassName == null) {
                    compiler.reportError("could not find class of the bean '" + bean + "'");
                    return null;
                    }
                    }*/
                    ClassDescriptor beanClassDescriptor =
                            ClassDescriptorHelper.getClassDescriptor(beanClass);
                    beanDescriptor = DefaultObjectHandler.getJAXXBeanInfo(
                            beanClassDescriptor);

                } catch (Exception e) {
                    compiler.reportError(
                            "could not load class " + beanClass);
                }
            }
            return beanDescriptor;
        }

        @Override
        public void addChild(CompiledObject child,
                             String constraints,
                             JAXXCompiler compiler) throws CompilerException {
            // do nothing
            compiler.reportError("can not add CompiledObject in the tag '" +
                                         TAG + " (only field tags)");
        }

        public boolean foundBean() {
            return !(beanClass == null || beanClass.isEmpty());
        }

        protected boolean addUiClass(BeanValidatorHandler handler,
                                     JAXXCompiler compiler) {
            boolean withError = false;
            if (uiClass == null &&
                    compiler.getConfiguration().getDefaultErrorUI() != null) {
                uiClass = compiler.getConfiguration().getDefaultErrorUI().getName();
            }
            if (uiClass != null) {
                try {
                    ClassDescriptor uiClazz =
                            ClassDescriptorHelper.getClassDescriptor(uiClass);
                    if (!ClassDescriptorHelper.getClassDescriptor(AbstractBeanValidatorUI.class).isAssignableFrom(uiClazz)) {
                        compiler.reportError(
                                "attribute 'ui'  :'" + uiClass +
                                        "' is not assignable from class " +
                                        AbstractBeanValidatorUI.class
                        );
                        withError = true;
                    } else {
                        String prefix = compiler.getImportedType(uiClazz.getName());
                        String code = handler.getSetPropertyCode(
                                getJavaCode(),
                                UI_CLASS_ATTRIBUTE,
                                prefix + ".class",
                                compiler
                        );
                        appendAdditionCode(code);
                    }
                } catch (ClassNotFoundException e) {
                    compiler.reportError("class not found '" + uiClass + "'");
                    withError = true;
                }
            }

            return withError;
        }

        protected boolean addErrorListModel(Element tag,
                                            BeanValidatorHandler handler,
                                            JAXXCompiler compiler) {
            if (errorListModel == null) {
                // try with the default "errors"
                if (!compiler.checkReference(
                        tag,
                        ERROR_LIST_MODEL_DEFAULT,
                        false,
                        ERROR_LIST_MODEL_ATTRIBUTE)) {
                    return false;
                }
                errorListModel = ERROR_LIST_MODEL_DEFAULT;
            } else {
                if (errorListModel.startsWith("{") &&
                        errorListModel.endsWith("}")) {
                    // this is a script, no check here
                    errorListModel = errorListModel.substring(
                            1,
                            errorListModel.length() - 1).trim();
                } else if (!compiler.checkReference(
                        tag,
                        errorListModel,
                        true,
                        ERROR_LIST_MODEL_ATTRIBUTE)) {
                    // errorListModel is not defined
                    return true;
                }
            }

            String code = handler.getSetPropertyCode(
                    getJavaCode(),
                    ERROR_LIST_MODEL_ATTRIBUTE,
                    errorListModel,
                    compiler
            );
            appendAdditionCode(code);

            return false;
        }

        protected boolean addErrorTableModel(Element tag,
                                             BeanValidatorHandler handler,
                                             JAXXCompiler compiler) {
            if (errorTableModel == null) {
                // try with the default "errors"
                if (!compiler.checkReference(
                        tag,
                        ERROR_TABLE_MODEL_DEFAULT,
                        false,
                        ERROR_LIST_MODEL_ATTRIBUTE)) {
                    return false;
                }
                errorTableModel = ERROR_TABLE_MODEL_DEFAULT;
            } else {
                if (errorTableModel.startsWith("{") &&
                        errorTableModel.endsWith("}")) {
                    // this is a script, no check here
                    errorTableModel = errorTableModel.substring(
                            1, errorTableModel.length() - 1).trim();
                } else if (!compiler.checkReference(
                        tag,
                        errorTableModel,
                        true,
                        ERROR_TABLE_MODEL_ATTRIBUTE)) {
                    // errorListModel is not defined
                    return true;
                }
            }

            String code = handler.getSetPropertyCode(
                    getJavaCode(),
                    ERROR_TABLE_MODEL_ATTRIBUTE,
                    errorTableModel,
                    compiler
            );
            appendAdditionCode(code);

            return false;

        }

        protected boolean addParentValidator(Element tag,
                                             BeanValidatorHandler handler,
                                             JAXXCompiler compiler) {
            if (parentValidator != null) {
                String initializer;
                if (parentValidator.startsWith("{") &&
                        parentValidator.endsWith("}")) {

                    // todo : should be able to bind
                    initializer = parentValidator.substring(
                            1,
                            parentValidator.length() - 1
                    );

                } else {
                    // the attribute referes an existing widget
                    if (!compiler.checkReference(
                            tag,
                            parentValidator,
                            true,
                            PARENT_VALIDATOR_ATTRIBUTE)) {
                        // parentValidator is not defined
                        return true;
                    }
                    initializer = parentValidator;
                }
                String code = handler.getSetPropertyCode(
                        getJavaCode(),
                        PARENT_VALIDATOR_ATTRIBUTE,
                        initializer,
                        compiler
                );
                appendAdditionCode(code);
            }
            return false;
        }

        protected boolean addErrorList(Element tag, JAXXCompiler compiler) {

            if (errorList == null) {
                // try with the default "errorList"
                if (!compiler.checkReference(
                        tag,
                        ERROR_LIST_DEFAULT,
                        false,
                        ERROR_LIST_ATTRIBUTE)) {
                    return false;
                }
                errorList = ERROR_LIST_DEFAULT;
            } else {
                if (!compiler.checkReference(
                        tag,
                        errorList,
                        true,
                        ERROR_LIST_ATTRIBUTE)) {
                    return true;
                }
            }

            String prefix = compiler.getImportedType(SwingValidatorUtil.class);

            String code = prefix +
                    ".registerErrorListMouseListener(" + errorList + ");";
            appendAdditionCode(code);

            return false;
        }

        protected boolean addErrorTable(Element tag, JAXXCompiler compiler) {

            if (errorTable == null) {
                // try with the default "errorList"
                if (!compiler.checkReference(tag,
                                             ERROR_TABLE_DEFAULT,
                                             false,
                                             ERROR_TABLE_ATTRIBUTE)) {
                    return false;
                }
                errorTable = ERROR_TABLE_DEFAULT;
            } else {
                if (!compiler.checkReference(tag,
                                             errorTable,
                                             true,
                                             ERROR_TABLE_ATTRIBUTE)) {
                    return true;
                }
            }

            String prefix = compiler.getImportedType(SwingValidatorUtil.class);

            String code = prefix +
                    ".registerErrorTableMouseListener(" + errorTable +
                    ");";
            appendAdditionCode(code);

            return false;
        }

        protected boolean addBean(Element tag,
                                  BeanValidatorHandler handler,
                                  JAXXCompiler compiler) {

            if (beanClass == null || beanClass.isEmpty()) {
                // try to guest beanClass from bean attribute
                if (bean != null && !bean.isEmpty()) {
                    beanClass = compiler.getSymbolTable().getClassTagIds().get(bean);
                    if (beanClass == null) {
                        compiler.reportError(
                                "could not find class of the bean '" + bean +
                                        "', and no beanClass was setted");
                        return true;
                    }
                }
            }
            if (beanClass == null) {
                compiler.reportError(
                        "tag '" + tag + "' requires a 'beanClass' attribute, " +
                                "and could not guest it from 'bean' attribute " +
                                "(no bean attribute setted...)");
                return true;
            }

            JAXXBeanInfo beanInfo = getBeanDescriptor(compiler);
            if (beanInfo == null) {
                compiler.reportError(
                        tag,
                        "could not find descriptor of class " + beanClass
                );
                return true;
            }

            String beanInitializer = null;
            if (bean != null) {

                if (bean.startsWith("{") && bean.endsWith("}")) {

                    String labelBinding =
                            compiler.processDataBindings(bean);
                    if (labelBinding != null) {
                        compiler.getBindingHelper().registerDataBinding(
                                getId() + ".bean",
                                labelBinding,
                                getId() + ".setBean(" + labelBinding + ");"
                        );
                    }
//                    // just has an intializer
//                    beanInitializer = bean.substring(1, bean.length() - 1);
//                    // this is not a real bean, so delete it
                    bean = null;
                } else {

                    if (!compiler.checkReference(tag,
                                                 bean,
                                                 true,
                                                 BEAN_ATTRIBUTE)) {
                        // could not find bean in compiled object
                        return true;
                    }

                    if (isBeanUsedByValidator(compiler, bean)) {
                        compiler.reportError(
                                "the bean '" + bean + "' is already used in " +
                                        "another the validator, can not used it in  '" +
                                        tag + "'"
                        );
                        return true;
                    }

                    /*if (beanInitializer != null) {
                    compiler.reportWarning("tag '" + tag + "' found a 'bean' and a 'beanInitializer' attributes, 'beanInitializer' is skipped");
                    }*/
                    beanInitializer = bean;
                }
            }

            if (beanInitializer != null) {
                String code = handler.getSetPropertyCode(
                        getJavaCode(),
                        BEAN_ATTRIBUTE,
                        compiler.checkJavaCode(beanInitializer),
                        compiler
                );
                appendAdditionCode(code);
            }

            String beanClassName = beanInfo.getJAXXBeanDescriptor().getClassDescriptor().getName();
            String type = compiler.getImportedType(beanClassName);
            // contextName must be in constructor to able to init validator with his correct contextName
            String constructorParams = type + ".class, " +
                    TypeManager.getJavaCode(context);
//            setConstructorParams(constructorParams);
            String validatorFactoryFqn =
                    compiler.getConfiguration().getValidatorFactoryFQN();

            String prefix = compiler.getImportedType(validatorFactoryFqn);
            setInitializer(
                    prefix + ".newValidator(" + constructorParams + ")"
            );

            // add generic type to validator
            setGenericTypes(beanClassName);

            if (getAutoField()) {
                registerAutoFieldBean(tag, compiler, beanInfo);
            }

            if (getBeanDescriptor(compiler) != null) {

                // add fieldrepresentation invocations
//                addFieldRepresentations(tag, compiler);

                // register the validator in compiler
                registerValidator(compiler, this);

            }

            return false;
        }

        private void registerValidator(
                JAXXCompiler compiler,
                CompiledBeanValidator compiledBeanValidator) {
            List<CompiledBeanValidator> vals = validators.computeIfAbsent(compiler, k -> new ArrayList<>());
            vals.add(compiledBeanValidator);
            List<String> ids = validatedComponents.computeIfAbsent(compiler, k -> new ArrayList<>());
            //            ids.addAll(compiledBeanValidator.getFields().values());
            ids.addAll(compiledBeanValidator.getFieldEditors());
        }

//        /**
//         * Register in buffer all field representation to init (and to record in method {@link JAXXValidator#registerValidatorFields()}
//         *
//         * @param compiler the compiler used
//         * @param javaFile generated file
//         * @param buffer   the buffer where to add code
//         * @since 2.2.1
//         */
//        public void addFieldRepresentations(JAXXCompiler compiler,
//                                            JavaFile javaFile,
//                                            StringBuilder buffer) {
//            for (String component : fields.keySet()) {
////                String component = entry.getKey();
//                Collection<String> propertyNames = fields.get(component);
//                List<String> keyCodes =
//                        Lists.newArrayListWithCapacity(propertyNames.size());
//                for (String propertyName : propertyNames) {
//                    if (!checkBeanProperty(compiler, propertyName)) {
//                        // property not find on bean
//                        continue;
//                    }
//                    String keyCode = TypeManager.getJavaCode(propertyName);
//                    keyCodes.add(keyCode);
//                }
//
//                if (keyCodes.isEmpty()) {
//                    // no property
//                    continue;
//                }
//                String keyCode = Joiner.on(", ").join(keyCodes);
//                if (keyCodes.size()>1) {
//                    keyCode = "{ " + keyCode+" }";
//                }
////                String propertyName = entry.getKey();
//
//                String validatorId = TypeManager.getJavaCode(getId());
//
//                String editorCode = TypeManager.getJavaCode(component);
//                JavaField editor = javaFile.getField(component);
//                String annotation = ValidatorField.class.getSimpleName() +
//                                    "( validatorId = " + validatorId + "," +
//                                    "  propertyName = " + keyCode + "," +
//                                    "  editorName = " + editorCode + "" +
//                                    ")";
//                editor.addAnnotation(annotation);
//
////                if (!compiler.checkReference(tag, component, true, null)) {
////                    // editor component not find on ui
////                    continue;
////                }
//
////                buffer.append(getJavaCode());
////                buffer.append(".setFieldRepresentation(");
////                buffer.append(keyCode);
////                buffer.append(", ");
////                buffer.append(component);
////                buffer.append(");\n");
//            }
//        }

        protected void registerAutoFieldBean(Element tag,
                                             JAXXCompiler compiler,
                                             JAXXBeanInfo beanInfo) {
            for (JAXXPropertyDescriptor beanProperty :
                    beanInfo.getJAXXPropertyDescriptors()) {
                String descriptionName = beanProperty.getName();
                if (log.isDebugEnabled()) {
                    log.debug("try to bind on bean " +
                                      beanInfo.getJAXXBeanDescriptor().getName() +
                                      " property " + descriptionName);
                }
                if (beanProperty.getWriteMethodDescriptor() == null) {
                    // read-only property
                    continue;
                }
//                if (fields.containsKey(descriptionName)) {
                if (containsFieldPropertyName(descriptionName)) {
                    // already defined in field
                    continue;
                }
//                if (excludeFields.containsKey(descriptionName)) {
                if (containsExcludeFieldPropertyName(descriptionName)) {
                    // exclude field
                    continue;
                }
                if (!compiler.checkReference(tag,
                                             descriptionName,
                                             getStrictMode(),
                                             null)) {
                    // no editor component found
                    continue;
                }
                // ok add the field mapping
                registerField(descriptionName, descriptionName, compiler);
            }

//            for (Entry<String, String> entry : excludeFields.entrySet()) {
            for (String key : getExcludeFieldPropertyNames()) {
//                String key = entry.getKey();
//                if (fields.containsKey(key)) {
                if (containsFieldPropertyName(key)) {
                    compiler.reportWarning(
                            "field '" + key + "' can not be used and " +
                                    "excluded at same time ! (field is skipped) " +
                                    "for validator " + this
                    );
//                    fields.remove(key);
                    removeFieldPropertyName(key);
                }
            }
        }

        public void registerField(String id,
                                  String component,
                                  JAXXCompiler compiler) {
//            if (fields.containsKey(id)) {
            if (containsFieldPropertyName(id)) {
                compiler.reportError(
                        "duplicate field '" + id + "' for validator " + this);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("add field <" + id + ":" + component + ">");
                }
//                fields.put(id, component);
                addField(id, component);

            }
        }

        public void registerExcludeField(String id,
                                         String component,
                                         JAXXCompiler compiler) {
//            if (excludeFields.containsKey(id)) {
            if (containsExcludeFieldPropertyName(id)) {
                compiler.reportError(
                        "duplicate field '" + id + "' for validator " + this);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("add excludeField <" + id + ":" + component + ">");
                }
//                excludeFields.put(id, component);
                addExcludeField(id, component);
            }
        }

        public boolean checkBeanProperty(JAXXCompiler compiler,
                                         String propertyName) {

            for (JAXXPropertyDescriptor beanProperty :
                    getBeanDescriptor(compiler).getJAXXPropertyDescriptors()) {
                if (beanProperty.getName().equals(propertyName)) {
                    if (beanProperty.getWriteMethodDescriptor() == null) {
                        // read-onlyproperty
                        compiler.reportError(
                                "could not bind the readonly property '" +
                                        propertyName + "' on bean [" + getBean() + "] ");
                        return false;
                    }
                    return true;
                }
            }
            compiler.reportError(
                    "could not find the property '" + propertyName +
                            "' on bean [" + getBean() + "] ");
            return false;
        }
    }

    /**
     * Test if a given bean is attached to a validator.
     *
     * @param compiler current compiler to use
     * @param beanId   the bean to test
     * @return <code>true</code> if the given bean is attached to a validator,
     * <code>false</code> otherwise
     */
    public static boolean isBeanUsedByValidator(JAXXCompiler compiler,
                                                String beanId) {
        List<CompiledBeanValidator> beanValidatorList = validators.get(compiler);
        if (beanValidatorList != null) {
            for (CompiledBeanValidator validator : beanValidatorList) {
                if (beanId.equals(validator.getBean())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param compiler compiler to use
     * @return <code>true</code> if some validators were detected,
     * <code>false</code> otherwise
     */
    public static boolean hasValidator(JAXXCompiler compiler) {
        List<CompiledBeanValidator> beanValidatorList =
                validators.get(compiler);
        return beanValidatorList != null && !beanValidatorList.isEmpty();
    }

    /**
     * Test if a given CompiledObject is attached to a validator.
     *
     * @param compiler    compiler to use
     * @param componentId the compiled object to test
     * @return <code>true</code> if the given compiled object is attached to
     * a validator, <code>false</code> otherwise
     */
    public static boolean isComponentUsedByValidator(JAXXCompiler compiler,
                                                     String componentId) {
        List<String> ids = validatedComponents.get(compiler);
        return ids != null && ids.contains(componentId);
    }

    public static List<CompiledBeanValidator> getValidators(JAXXCompiler compiler) {
        return validators.get(compiler);
    }
}
