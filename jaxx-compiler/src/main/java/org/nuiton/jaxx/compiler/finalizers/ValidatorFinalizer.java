/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.finalizers;

import com.google.auto.service.AutoService;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompiledObject.ChildRef;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.java.JavaElement;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaField;
import org.nuiton.jaxx.compiler.java.JavaFile;
import org.nuiton.jaxx.compiler.reflect.FieldDescriptor;
import org.nuiton.jaxx.compiler.tags.validator.BeanValidatorHandler;
import org.nuiton.jaxx.compiler.tags.validator.BeanValidatorHandler.CompiledBeanValidator;
import org.nuiton.jaxx.compiler.types.TypeManager;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.SwingValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.jaxx.validator.swing.meta.Validator;
import org.nuiton.jaxx.validator.swing.meta.ValidatorField;

/**
 * To finalize validators fields.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @plexus.component role-hint="validators" role="org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer"
 */
@AutoService(JAXXCompilerFinalizer.class)
public class ValidatorFinalizer extends AbstractFinalizer {

    /** Logger. */
    static final Log log = LogFactory.getLog(ValidatorFinalizer.class);

    protected static final JavaField VALIDATOR_IDS_FIELD =
            JavaElementFactory.newField(
                    Modifier.PROTECTED,
                    List.class.getName() + "<String>",
                    "validatorIds",
                    true
            );

    @Override
    public boolean accept(JAXXCompiler compiler) {

        //use this finalizer if compiler is validation aware
        return BeanValidatorHandler.hasValidator(compiler);
    }

    @Override
    public void finalizeCompiler(CompiledObject root,
                                 JAXXCompiler compiler,
                                 JavaFile javaFile,
                                 String packageName,
                                 String className) {

        for (CompiledObject object : compiler.getObjects().values()) {
            List<ChildRef> childs = object.getChilds();
            if (childs == null || childs.isEmpty()) {
                continue;
            }
            for (ChildRef child : childs) {
                String javaCode = child.getChildJavaCode();

                // some validators are defined on this object
                boolean found =
                        BeanValidatorHandler.isComponentUsedByValidator(
                                compiler,
                                child.getChild().getId()
                        );

                if (found) {
//                    compiler.setNeedSwingUtil(true);
                    String type =
                            compiler.getImportedType(SwingUtil.class);
                    // box the child component in a JxLayer
                    child.setChildJavaCode(
                            type +
                                    ".boxComponentWithJxLayer(" + javaCode + ")");
                }
            }
        }
        String eol = JAXXCompiler.getLineSeparator();
        StringBuilder builder = new StringBuilder();
        // register validators
        List<CompiledBeanValidator> validators =
                BeanValidatorHandler.getValidators(compiler);
//        javaFile.addImport(Validator.class);
//        javaFile.addImport(ValidatorField.class);
//        javaFile.addImport(SwingValidatorUtil.class);
        String validatorUtilPrefix =
                compiler.getImportedType(SwingValidatorUtil.class) +
                        ".";

        compiler.getJavaFile().addMethod(JavaElementFactory.newMethod(
                Modifier.PUBLIC,
                TYPE_VOID,
                "registerValidatorFields",
                validatorUtilPrefix + "detectValidatorFields(this);",
                true)
        );
        builder.append("// register ");
        builder.append(validators.size());
        builder.append(" validator(s)");
        builder.append(eol);

        builder.append("validatorIds = ");
        builder.append(validatorUtilPrefix).append("detectValidators(this);");
        builder.append(eol);

        builder.append(validatorUtilPrefix).append("installUI(this);");
        builder.append(eol);
        compiler.appendLateInitializer(builder.toString());

        for (CompiledBeanValidator validator : validators) {

            registerValidator(validator, compiler, javaFile);
        }
    }

    @Override
    public void prepareJavaFile(CompiledObject root,
                                JAXXCompiler compiler,
                                JavaFile javaFile,
                                String packageName,
                                String className) throws ClassNotFoundException {
        Class<?> validatorClass = SwingValidator.class;

        Class<?> validatorInterface = JAXXValidator.class;

        boolean parentIsValidator =
                compiler.isSuperClassAware(validatorInterface);

        if (parentIsValidator) {

            // nothing to generate (use the parent directly)
            return;
        }

        // add JAXXValidator interface
        javaFile.addInterface(JAXXCompiler.getCanonicalName(validatorInterface));

        // implements JAXXValidator
        addField(javaFile, VALIDATOR_IDS_FIELD);

        String type = compiler.getImportedType(validatorClass);

        String initializer = "return (" + type +
                "<?>) (validatorIds.contains(validatorId) ? " +
                "getObjectById(validatorId) : null);";


        javaFile.addMethod(JavaElementFactory.newMethod(
                Modifier.PUBLIC,
                type + "<?>",
                "getValidator",
                initializer,
                true,
                JavaElementFactory.newArgument(TYPE_STRING, "validatorId"))
        );
    }

    public void registerValidator(CompiledBeanValidator validator,
                                  JAXXCompiler compiler,
                                  JavaFile javaFile) {

        JavaField validatorField = javaFile.getField(validator.getId());

        String validatorId = TypeManager.getJavaCode(validator.getId());

        String type = compiler.getImportedType(Validator.class);
        String fieldType = compiler.getImportedType(ValidatorField.class);

        String validatorAnnotation = type +
                "( validatorId = " + validatorId + ")";
        validatorField.addAnnotation(validatorAnnotation);
//        Map<String, String> fields = validator.getFields();

        for (String component : validator.getFieldEditors()) {

            Collection<String> propertyNames = validator.getFieldPropertyNames(component);
            List<String> keyCodes =
                    Lists.newArrayListWithCapacity(propertyNames.size());
            for (String propertyName : propertyNames) {
                if (!validator.checkBeanProperty(compiler, propertyName)) {
                    // property not find on bean
                    continue;
                }
                String keyCode = TypeManager.getJavaCode(propertyName);
                keyCodes.add(keyCode);
            }

            if (keyCodes.isEmpty()) {
                // no property
                continue;
            }
            String keyCode = Joiner.on(", ").join(keyCodes);
            if (keyCodes.size() > 1) {
                keyCode = "{ " + keyCode + " }";
            }

//            String keyCode = TypeManager.getJavaCode(propertyName);
            String editorCode = TypeManager.getJavaCode(component);
            JavaElement editor = javaFile.getField(component);
            if (editor == null) {
                if (log.isDebugEnabled()) {
                    String message = "Could not find editor [" + component +
                            "] for property(ies) [" + propertyNames +
                            "] for file " + javaFile.getName();
                    log.debug(message);
                }

//                // find in the compiler the object
                CompiledObject compiledObject =
                        compiler.getCompiledObject(component);

                String fqn;
                if (compiledObject == null) {
                    FieldDescriptor fieldDescriptor = null;
                    try {
                        fieldDescriptor = compiler.getRootObject().getObjectClass().getDeclaredFieldDescriptor(component);
                    } catch (NoSuchFieldException e) {
                        // can't happen
                    }
                    if (fieldDescriptor == null) {

                        // this is an error, editor is unknown (this case should
                        // never happen)

                        String errorMessage =
                                "Could not find editor [" + component +
                                        "] for property(ies) [" + propertyNames +
                                        "] for file " + javaFile.getName();

                        throw new CompilerException(errorMessage);
                    }
                    fqn = JAXXCompiler.getCanonicalName(fieldDescriptor.getType());
                } else {
                    fqn = JAXXCompiler.getCanonicalName(compiledObject);
                }

                // now must add a getter in the javaFile


                editor = javaFile.addGetterMethod(component,
                                                  Modifier.PUBLIC,
                                                  fqn,
                                                  true,
                                                  true
                );
            }

            String annotation = fieldType +
                    "( validatorId = " + validatorId + "," +
                    "  propertyName = " + keyCode + "," +
                    "  editorName = " + editorCode + "" + ")";
            editor.addAnnotation(annotation);
        }
    }
}
