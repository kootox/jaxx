/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;

/**
 * Handles the <code>&lt;import&gt;</code> tag.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Id$
 * @since 2.1
 */
public class ImportHandler implements TagHandler {

    /** Logger */
    protected static final Log log = LogFactory.getLog(ImportHandler.class);

    public static final String TAG_NAME = "import";

    @Override
    public void compileFirstPass(Element tag,
                                 JAXXCompiler compiler) throws IOException {

        StringBuilder script = new StringBuilder();
        NodeList children = tag.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            switch (child.getNodeType()) {
                case Node.ELEMENT_NODE:
                    compiler.reportError(
                            "<import> tag may not contain child elements: " +
                                    tag);
                case Node.TEXT_NODE: // fall through
                case Node.CDATA_SECTION_NODE:
                    String txt = ((Text) child).getData();
                    if (log.isDebugEnabled()) {
                        log.debug("Will treate imports  :[\n" + txt + "\n]");
                    }
                    String eol = JAXXCompiler.getLineSeparator();
                    try (LineNumberReader reader = new LineNumberReader(new StringReader(txt.trim()))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            line = line.trim();
                            if (line.isEmpty()) {
                                continue;
                            }
                            script.append("import ").append(line);
                            if (!line.endsWith(";")) {
                                script.append(';');
                            }
                            script.append(eol);
                        }
                    }
            }
        }

        String scriptString = script.toString().trim();
        if (!scriptString.isEmpty()) {
            compiler.registerScript(script.toString());
        }
    }

    @Override
    public void compileSecondPass(Element tag, JAXXCompiler compiler) {
        // nothing to do
    }
}
