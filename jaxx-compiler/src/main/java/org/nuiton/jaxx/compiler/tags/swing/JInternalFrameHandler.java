/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.compiler.tags.swing;

import org.nuiton.jaxx.compiler.CompiledObject;
import org.nuiton.jaxx.compiler.CompilerException;
import org.nuiton.jaxx.compiler.JAXXCompiler;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptor;
import org.nuiton.jaxx.compiler.reflect.ClassDescriptorHelper;
import org.nuiton.jaxx.compiler.tags.DefaultComponentHandler;
import org.w3c.dom.Element;

import javax.swing.JInternalFrame;
import javax.swing.JMenuBar;
import javax.swing.WindowConstants;

public class JInternalFrameHandler extends DefaultComponentHandler {

    public JInternalFrameHandler(ClassDescriptor beanClass) {
        super(beanClass);
        ClassDescriptorHelper.checkSupportClass(getClass(), beanClass, JInternalFrame.class);
    }

    @Override
    public CompiledObject createCompiledObject(String id, JAXXCompiler compiler) throws CompilerException {
        return new CompiledObject(id, getBeanClass(), compiler) {

            @Override
            public void addChild(CompiledObject child, String constraints, JAXXCompiler compiler) throws CompilerException {
                if (ClassDescriptorHelper.getClassDescriptor(JMenuBar.class).isAssignableFrom(child.getObjectClass())) {
                    appendAdditionCode(getId() + ".setJMenuBar(" + child.getId() + ");");
                } else {
                    super.addChild(child, constraints, compiler);
                }
            }
        };
    }

    @Override
    protected void setDefaults(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        super.setDefaults(object, tag, compiler);
        setAttribute(object, "visible", "true", false, compiler);
        setAttribute(object, "closable", "true", false, compiler);
        setAttribute(object, "defaultCloseOperation", String.valueOf(WindowConstants.DISPOSE_ON_CLOSE), false, compiler);
    }

    @Override
    public void setAttributes(CompiledObject object, Element tag, JAXXCompiler compiler) throws CompilerException {
        super.setAttributes(object, tag, compiler);
        compiler.appendInitializerCode(object.getId() + ".pack();\n");
    }
}
