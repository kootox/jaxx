/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.reflect;

import java.net.URL;

/**
 * Contract of a resolver of class descriptor.
 *
 * The unique method {@link #resolvDescriptor(String, URL)} will returns the
 * descriptor if can be found.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.2
 */
public abstract class ClassDescriptorResolver {

    private ClassLoader classLoader;

    private final ClassDescriptorHelper.ResolverType resolverType;

    protected ClassDescriptorResolver(ClassDescriptorHelper.ResolverType resolverType) {
        this.resolverType = resolverType;
    }

    public ClassDescriptorHelper.ResolverType getResolverType() {
        return resolverType;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * @param className the fully qualified name of the class
     * @param source    the source of the class (java file, jaxx file, class)
     * @return the descriptor of the given class
     * @throws ClassNotFoundException if class descriptor could not be found.
     */
    public abstract ClassDescriptor resolvDescriptor(String className,
                                                     URL source) throws ClassNotFoundException;


}
