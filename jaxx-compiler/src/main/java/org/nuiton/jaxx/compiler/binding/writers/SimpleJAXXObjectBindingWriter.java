/*
 * #%L
 * JAXX :: Compiler
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.compiler.binding.writers;

import org.nuiton.jaxx.compiler.binding.DataBinding;
import org.nuiton.jaxx.compiler.binding.DataListener;
import org.nuiton.jaxx.compiler.binding.JavaParserUtil;
import org.nuiton.jaxx.compiler.finalizers.DefaultFinalizer;
import org.nuiton.jaxx.compiler.finalizers.JAXXCompilerFinalizer;
import org.nuiton.jaxx.compiler.java.JavaElementFactory;
import org.nuiton.jaxx.compiler.java.JavaFileGenerator;
import org.nuiton.jaxx.compiler.java.JavaMethod;
import org.nuiton.jaxx.runtime.binding.SimpleJAXXObjectBinding;

import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public class SimpleJAXXObjectBindingWriter extends AbstractJAXXBindingWriter<SimpleJAXXObjectBinding> {

    protected boolean used;

    public SimpleJAXXObjectBindingWriter() {
        super(SimpleJAXXObjectBinding.class);
    }

    @Override
    public boolean accept(DataBinding binding) {
        DataListener[] trackers = binding.getTrackers();
        if (trackers.length > 0) {
            for (DataListener tracker : trackers) {
                if (tracker.getObjectCode() != null) {
                    // tracker must be without any requirement
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected String getConstructorParams(DataBinding binding,
                                          DataListener[] trackers) {

        StringBuilder addBuffer = new StringBuilder();
        addBuffer.append("this, ");
        addBuffer.append(binding.getConstantId());
        addBuffer.append(", true");

        for (DataListener tracker : trackers) {
            String symbol = tracker.getSymbol();
            String name = symbol.substring(symbol.indexOf(".") + 1);
            if (JavaParserUtil.getMethodInvocationParameters(name) != null) {
                // obtain the property name from the method name
                name = JavaParserUtil.getPropertyNameFromMethod(name);
            }
            addBuffer.append(" ,\"").append(name).append("\"");
        }
        return addBuffer.toString();
    }

    @Override
    public void write(DataBinding binding,
                      JavaFileGenerator generator,
                      StringBuilder buffer) {
        DataListener[] trackers = binding.getTrackers();
        List<JavaMethod> bMethods = binding.getMethods();

        JavaMethod method = JavaElementFactory.newMethod(
                Modifier.PUBLIC,
                JAXXCompilerFinalizer.TYPE_VOID,
                DefaultFinalizer.METHOD_NAME_PROCESS_DATA_BINDING,
                binding.getProcessDataBinding(),
                true
        );

        bMethods.add(0, method);

        writeInvocationMethod(binding, trackers, generator, buffer, bMethods);
    }
}
