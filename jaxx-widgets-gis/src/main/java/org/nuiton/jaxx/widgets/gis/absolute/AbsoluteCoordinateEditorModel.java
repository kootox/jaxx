package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.DdCoordinate;
import org.nuiton.jaxx.widgets.gis.DmdCoordinate;
import org.nuiton.jaxx.widgets.gis.DmsCoordinate;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class AbsoluteCoordinateEditorModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DD = "dd";

    public static final String PROPERTY_DD_DEGREE = "ddDegree";

    public static final String PROPERTY_DD_DECIMAL = "ddDecimal";

    public static final String PROPERTY_DMS = "dms";

    public static final String PROPERTY_DMS_DEGREE = "dmsDegree";

    public static final String PROPERTY_DMS_MINUTE = "dmsMinute";

    public static final String PROPERTY_DMS_SECOND = "dmsSecond";

    public static final String PROPERTY_DMD = "dmd";

    public static final String PROPERTY_DMD_DEGREE = "dmdDegree";

    public static final String PROPERTY_DMD_MINUTE = "dmdMinute";

    public static final String PROPERTY_DMD_DECIMAL = "dmdDecimal";

    /**
     * To edit as dd format.
     */
    protected final DdCoordinate dd = DdCoordinate.empty();

    /**
     * To edit as dms format.
     */
    protected final DmsCoordinate dms = DmsCoordinate.empty();

    /**
     * To edit as dmd format.
     */
    protected final DmdCoordinate dmd = DmdCoordinate.empty();


    public Float getValue(CoordinateFormat format) {

        Float result = null;

        switch (format) {

            case dd:
                result = dd.toDecimal();
                break;
            case dms:
                result = dms.toDecimal();
                break;
            case dmd:
                result = dmd.toDecimal();
                break;
        }
        return result;

    }

    public Float getValue() {
        return dd.toDecimal();
    }

    public void setValue(Float value) {

        setDd(value);
        setDms(value);
        setDmd(value);

    }

    // --- DD --- //

    public DdCoordinate getDd() {
        return dd;
    }

    public void setDd(Float decimal) {
        DdCoordinate position = DdCoordinate.valueOf(decimal);
        setDdSign(false);
        setDdDegree(position.getDegree());
        setDdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_DD, null, getDd());
    }

    public boolean isDdSign() {
        return false;
    }

    public void setDdSign(boolean sign) {
        //
    }

    public Integer getDdDegree() {
        return dd.getDegree();
    }

    public void setDdDegree(Integer degree) {
        Object oldValue = getDdDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        dd.setDegree(degree);
        firePropertyChange(PROPERTY_DD_DEGREE, oldValue, degree);
    }

    public Integer getDdDecimal() {
        return dd.getDecimal();
    }

    public void setDdDecimal(Integer decimal) {
        Object oldValue = getDdDecimal();
        if (decimal != null) {
            decimal = Math.abs(decimal);
        }
        dd.setDecimal(decimal);
        firePropertyChange(PROPERTY_DD_DECIMAL, oldValue, decimal);
    }

    // --- DMS --- //

    public DmsCoordinate getDms() {
        return dms;
    }

    public void setDms(Float decimal) {
        DmsCoordinate position = DmsCoordinate.valueOf(decimal);
        setDmsSign(false);
        setDmsDegree(position.getDegree());
        setDmsMinute(position.getMinute());
        setDmsSecond(position.getSecond());
        firePropertyChange(PROPERTY_DMS, null, getDms());
    }

    public boolean isDmsSign() {
        return false;
    }

    public void setDmsSign(boolean sign) {
        //
    }

    public Integer getDmsDegree() {
        return dms.getDegree();
    }

    public void setDmsDegree(Integer degree) {
        Object oldValue = getDmsDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        dms.setDegree(degree);
        firePropertyChange(PROPERTY_DMS_DEGREE, oldValue, degree);
    }

    public Integer getDmsMinute() {
        return dms.getMinute();
    }

    public void setDmsMinute(Integer minute) {
        Object oldValue = getDmsMinute();
        dms.setMinute(minute);
        firePropertyChange(PROPERTY_DMS_MINUTE, oldValue, minute);
    }

    public Integer getDmsSecond() {
        return dms.getSecond();
    }

    public void setDmsSecond(Integer second) {
        Object oldValue = getDmsSecond();
        dms.setSecond(second);
        firePropertyChange(PROPERTY_DMS_SECOND, oldValue, second);
    }

    // --- DMD --- //

    public DmdCoordinate getDmd() {
        return dmd;
    }

    public void setDmd(Float decimal) {
        DmdCoordinate position = DmdCoordinate.valueOf(decimal);
        setDmdSign(false);
        setDmdDegree(position.getDegree());
        setDmdMinute(position.getMinute());
        setDmdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_DMD, null, getDmd());
    }

    public boolean isDmdSign() {
        return false;
    }

    public void setDmdSign(boolean sign) {
        //
    }

    public Integer getDmdDegree() {
        return dmd.getDegree();
    }

    public void setDmdDegree(Integer degree) {
        Object oldValue = getDmdDegree();
        dmd.setDegree(degree);
        firePropertyChange(PROPERTY_DMD_DEGREE, oldValue, degree);
    }

    public Integer getDmdMinute() {
        return dmd.getMinute();
    }

    public void setDmdMinute(Integer minute) {
        Object oldValue = getDmdMinute();
        dmd.setMinute(minute);
        firePropertyChange(PROPERTY_DMD_MINUTE, oldValue, minute);
    }

    public Integer getDmdDecimal() {
        return dmd.getDecimal();
    }

    public void setDmdDecimal(Integer decimal) {
        Object oldValue = getDmdDecimal();
        dmd.setDecimal(decimal);
        firePropertyChange(PROPERTY_DMD_DECIMAL, oldValue, decimal);
    }

}
