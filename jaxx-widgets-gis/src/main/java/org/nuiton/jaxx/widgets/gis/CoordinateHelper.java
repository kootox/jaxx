/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.gis;

import java.math.BigDecimal;

/**
 * Helper to deal with coordinates.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinateHelper {

    /**
     * Calcule le quadrant à partir d'une {@code longitude} et {@code latitude}.
     *
     * @param longitude la longitude décimale
     * @param latitude  la latitude décimale
     * @return la valeur du quadrant ou {@code null} si l'une des deux
     * coordonnées est {@code null}.
     */
    public static Integer getQuadrant(Float longitude, Float latitude) {
        if (longitude == null || latitude == null) {
            return null;
        }
        int result;

        if (latitude > 0) {
            result = longitude > 0 ? 1 : 4;
        } else {
            result = longitude > 0 ? 2 : 3;
        }
        return result;
    }

    /**
     * Calcule la valeur signée de la longitude à partir du {@code quadrant} et
     * de la valeur absolue de la {@code longitude}.
     *
     * @param quadrant  la valeur du quandrant (peut être null)
     * @param longitude la longitude décimale (peut être null)
     * @return la valeur signée de la longitude ou {@code null} si l'une des
     * deux données d'entrée est {@code null}.
     */
    public static Float getSignedLongitude(Integer quadrant, Float longitude) {
        if (longitude == null) {
            return null;
        }
        if (quadrant == null) {

            // cas special ou pas encore de quadrant positionne, on conserve
            // juste la valeur de la longitude sans rien faire d'autre
            return longitude;
        }
        int result;
        switch (quadrant) {
            case 1:
            case 2:
                result = 1;
                break;
            default:
                result = -1;
        }
        return result * longitude;
    }

    /**
     * Calcule la valeur signée de la latitude à partir du {@code quadrant} et
     * de la valeur absolue de la {@code latitude}.
     *
     * @param quadrant la valeur du quandrant (peut être null)
     * @param latitude la longitude décimale (peut être null)
     * @return la valeur signée de la latitude ou {@code null} si l'une des
     * deux données d'entrée est {@code null}.
     */
    public static Float getSignedLatitude(Integer quadrant, Float latitude) {
        if (latitude == null) {
            return null;
        }
        if (quadrant == null) {

            // cas special ou pas encore de quadrant positionne, on conserve
            // juste la valeur de la latitude sans rien faire d'autre
            return latitude;
        }
        int result;
        switch (quadrant) {
            case 1:
            case 4:
                result = 1;
                break;
            default:
                result = -1;
        }
        return result * latitude;
    }

    public static Float roundToThreeDecimals(Float aFloat) {

        Float roundFloat;
        if (aFloat == null) {
            roundFloat = null;
        } else {
            roundFloat = new BigDecimal(aFloat).setScale(3, BigDecimal.ROUND_HALF_DOWN).floatValue();
        }
        return roundFloat;

    }

    public static Float roundToFourDecimals(Float aFloat) {

        Float roundFloat;
        if (aFloat == null) {
            roundFloat = null;
        } else {
            roundFloat = new BigDecimal(aFloat).setScale(4, BigDecimal.ROUND_HALF_DOWN).floatValue();
        }
        return roundFloat;

    }

}
