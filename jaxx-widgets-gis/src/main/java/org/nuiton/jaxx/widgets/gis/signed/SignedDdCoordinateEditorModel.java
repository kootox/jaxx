package org.nuiton.jaxx.widgets.gis.signed;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.widgets.gis.DdCoordinate;

import java.io.Serializable;

/**
 * Created on 10/16/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class SignedDdCoordinateEditorModel extends DdCoordinate {

    public static final String PROPERTY_BEAN = "bean";

    public static final String PROPERTY_PROPERTY_SIGN = "propertySign";

    public static final String PROPERTY_PROPERTY_DEGREE = "propertyDegree";

    public static final String PROPERTY_PROPERTY_DECIMAL = "propertyDecimal";

    private static final long serialVersionUID = 1L;

    /** Bean where to push data. */
    protected Object bean;

    /** Name of the property of the bean to fire the change of the {@link #sign}. */
    protected String propertySign;

    /** Name of the property of the bean to fire the change of the {@link #degree}. */
    protected String propertyDegree;

    /** Name of the property of the bean to fire the change of the {@link #decimal}. */
    protected String propertyDecimal;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Object oldValue = getBean();
        this.bean = bean;
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    public String getPropertySign() {
        return propertySign;
    }

    public void setPropertySign(String propertySign) {
        Object oldValue = getPropertySign();
        this.propertySign = propertySign;
        firePropertyChange(PROPERTY_PROPERTY_SIGN, oldValue, propertySign);
    }

    public String getPropertyDegree() {
        return propertyDegree;
    }

    public void setPropertyDegree(String propertyDegree) {
        Object oldValue = getPropertyDegree();
        this.propertyDegree = propertyDegree;
        firePropertyChange(PROPERTY_PROPERTY_DEGREE, oldValue, propertyDegree);
    }

    public String getPropertyDecimal() {
        return propertyDecimal;
    }

    public void setPropertyDecimal(String propertyDecimal) {
        Object oldValue = getPropertyDecimal();
        this.propertyDecimal = propertyDecimal;
        firePropertyChange(PROPERTY_PROPERTY_DECIMAL, oldValue, propertyDecimal);
    }

    public void setValue(DdCoordinate value) {
        setSign(value != null && value.isSign());
        setDegree(value == null ? null : value.getDegree());
        setDecimal(value == null ? null : value.getDecimal());
    }

    public String getStringPattern() {
        return COORDINATE_STRING_PATTERN;
    }
}
