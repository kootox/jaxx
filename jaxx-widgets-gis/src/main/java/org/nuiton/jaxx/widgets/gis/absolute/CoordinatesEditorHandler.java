package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Objects;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinatesEditorHandler implements UIHandler<CoordinatesEditor> {

    private CoordinatesEditor ui;

    @Override
    public void beforeInit(CoordinatesEditor ui) {
        this.ui = ui;

        CoordinatesEditorModel model = new CoordinatesEditorModel();
        model.setFormat(CoordinateFormat.dd);
        model.setQuadrant(0);

        ui.setContextValue(model);

    }

    @Override
    public void afterInit(CoordinatesEditor ui) {

        ui.getLatitudeDd().init(false);
        ui.getLatitudeDms().init(false);
        ui.getLatitudeDmd().init(false);

        ui.getLongitudeDd().init(true);
        ui.getLongitudeDms().init(true);
        ui.getLongitudeDmd().init(true);

    }

    public void resetModel() {

        ui.getLongitudeDd().resetModel();
        ui.getLongitudeDms().resetModel();
        ui.getLongitudeDmd().resetModel();

        ui.getLatitudeDd().resetModel();
        ui.getLatitudeDms().resetModel();
        ui.getLatitudeDmd().resetModel();

    }

    public void resetQuadrant() {

        JAXXButtonGroup quadrantBG = ui.getQuadrantBG();

        // I don't know why but to reset the buttons, we need to remove them from the button group, 
        quadrantBG.remove(ui.getQuadrant1());
        quadrantBG.remove(ui.getQuadrant2());
        quadrantBG.remove(ui.getQuadrant3());
        quadrantBG.remove(ui.getQuadrant4());

        ui.getQuadrant1().setSelected(false);
        ui.getQuadrant2().setSelected(false);
        ui.getQuadrant3().setSelected(false);
        ui.getQuadrant4().setSelected(false);

        quadrantBG.add(ui.getQuadrant1());
        quadrantBG.add(ui.getQuadrant2());
        quadrantBG.add(ui.getQuadrant3());
        quadrantBG.add(ui.getQuadrant4());

        // reset in model at the end
        ui.setQuadrant(null);

        // reset button group after all
        ui.getQuadrantBG().setSelectedValue(null);

    }

    public void init(CoordinatesEditor ui) {

        CoordinatesEditorModel model = ui.getModel();

        Object bean = model.getBean();

        if (bean != null) {

            if (model.getPropertyLatitude() != null) {

                Method latitudeMutator = Setters.getMutator(bean, model.getPropertyLatitude());
                Objects.requireNonNull(latitudeMutator, "could not find mutator for " + model.getPropertyLatitude());
                // When model latitude changed, let's push it back in bean
                model.addPropertyChangeListener(
                        CoordinatesEditorModel.PROPERTY_LATITUDE,
                        new ModelPropertyChangeListener(model, latitudeMutator));

            }

            if (model.getPropertyLatitude() != null) {

                Method longitudeMutator = Setters.getMutator(bean, model.getPropertyLongitude());
                Objects.requireNonNull(longitudeMutator, "could not find mutator for " + model.getPropertyLongitude());

                // When model longitude changed, let's push it back in bean
                model.addPropertyChangeListener(
                        CoordinatesEditorModel.PROPERTY_LONGITUDE,
                        new ModelPropertyChangeListener(model, longitudeMutator));

            }

            if (model.getPropertyQuadrant() != null) {

                Method quadrantMutator = Setters.getMutator(bean, model.getPropertyQuadrant());
                Objects.requireNonNull(quadrantMutator, "could not find mutator for " + model.getPropertyQuadrant());

                // When model quadrant changed, let's push it back in bean
                model.addPropertyChangeListener(
                        CoordinatesEditorModel.PROPERTY_QUADRANT,
                        new ModelPropertyChangeListener(model, quadrantMutator));

            }

        }

    }

    public boolean isQuadrantSelected(Integer value, int requiredValue) {
        return value != null && value == requiredValue;
    }


    private class ModelPropertyChangeListener implements PropertyChangeListener {

        private final CoordinatesEditorModel model;

        private final Method mutator;

        private ModelPropertyChangeListener(CoordinatesEditorModel model, Method mutator) {
            this.model = model;
            this.mutator = mutator;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
//            if (!valueIsAdjusting) {
            Object newValue = evt.getNewValue();

            try {

//                    valueModelIsAdjusting = true;
//                    try {
                mutator.invoke(model.getBean(), newValue);
//                    } finally {
//                        valueModelIsAdjusting = false;
//                    }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

}
