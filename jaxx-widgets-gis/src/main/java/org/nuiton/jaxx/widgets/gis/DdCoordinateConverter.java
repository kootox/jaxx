package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.converter.NuitonConverter;
import org.nuiton.jaxx.runtime.resources.UIResourcesProvider;

import java.util.regex.Matcher;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
@AutoService(NuitonConverter.class)
public class DdCoordinateConverter implements NuitonConverter<DdCoordinate> {

    protected boolean useSign;

    protected boolean forLongitude;

    protected String nullValue = "";

    protected char fillChar = ' ';

    public void setUseSign(boolean useSign) {
        this.useSign = useSign;
    }

    public void setForLongitude(boolean forLongitude) {
        this.forLongitude = forLongitude;
    }

    public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) {
        this.nullValue = displayZeroWhenNull ? "0" : "";
    }

    public void setFillWithZero(boolean fillWithZero) {
        fillChar = fillWithZero ? '0' : ' ';
    }

    @Override
    public <T> T convert(Class<T> aClass, Object value) {

        if (!isEnabled(aClass)) {
            throw new ConversionException(
                    t("jaxx.error.no.convertor.coordinateDd", value));
        }

        Object result = null;

        if (value == null) {

            if (aClass.equals(String.class)) {

                result = String.format(
                        DdCoordinate.COORDINATE_STRING_PATTERN,
                        useSign ? "-" : "",
                        StringUtils.leftPad(nullValue, forLongitude ? 3 : 2, ' '),
                        StringUtils.leftPad(nullValue, 3, ' '));
            }
        } else {

            if (aClass.equals(value.getClass())) {

                // same class, no convertion to do
                result = value;
            } else if (value instanceof String) {

                // String to Value

                Matcher matcher = DdCoordinate.COORDINATE_PATTERN.matcher((String) value);

                if (matcher.matches()) {

                    String degresStr = matcher.group(1).replaceAll("\\s", "");
                    String decimalesStr = matcher.group(2).replaceAll("\\s", "");

                    Integer degre = degresStr.isEmpty() || "-".equals(degresStr) ? null : Math.abs(Integer.valueOf(degresStr));
                    Integer decimal;
                    if (decimalesStr.isEmpty()) {
                        decimal = null;
                    } else {
                        decimalesStr = StringUtils.rightPad(decimalesStr, 3, '0');
                        decimal = Integer.valueOf(decimalesStr);
                    }

                    boolean signed = degresStr.contains("-");
                    result = DdCoordinate.valueOf(signed,
                                                  degre,
                                                  decimal);
                }

            } else if (value instanceof DdCoordinate) {

                // Value to String

                DdCoordinate coordinate = (DdCoordinate) value;

                boolean sign = coordinate.isSign();
                String signStr = sign ? "-" : "";

                Integer degree = coordinate.getDegree();
                String degreeStr = degree == null ? nullValue : degree.toString();

                Integer decimal = coordinate.getDecimal();

                String decimalStr;
                if (decimal == null) {
                    decimalStr = nullValue;
                } else {

                    decimalStr = StringUtils.leftPad(decimal.toString(), 3, '0');
                    while (decimalStr.endsWith("0")) {
                        decimalStr = decimalStr.substring(0, decimalStr.length() - 1);
                    }
                }

                result = String.format(
                        DdCoordinate.COORDINATE_STRING_PATTERN,
                        signStr,
                        StringUtils.leftPad(degreeStr, forLongitude ? 3 : 2, nullValue.equals(degreeStr) ? ' ' : fillChar),
                        StringUtils.rightPad(decimalStr, 3, nullValue.equals(decimalStr) ? ' ' : fillChar));
            }
        }
        return aClass.cast(result);
    }

    protected boolean isEnabled(Class<?> aClass) {
        return String.class.isAssignableFrom(aClass) ||
                DdCoordinate.class.isAssignableFrom(aClass);
    }

    @Override
    public Class<DdCoordinate> getType() {
        return DdCoordinate.class;
    }

}
