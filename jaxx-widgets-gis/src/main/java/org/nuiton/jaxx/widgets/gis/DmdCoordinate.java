package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.util.regex.Pattern;

/**
 * Geo coordinate in degree decimal, minute format.
 *
 * Created on 10/23/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DmdCoordinate extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String COORDINATE_STRING_PATTERN = "%s%s°%s.%s'";

    public static final Pattern COORDINATE_PATTERN = Pattern.compile("(.*)°(.*)\\.(.*)'");

    public static final String PROPERTY_SIGN = "sign";

    public static final String PROPERTY_DEGREE = "degree";

    public static final String PROPERTY_MINUTE = "minute";

    public static final String PROPERTY_DECIMAL = "decimal";

    protected boolean sign;

    protected Integer degree;

    protected Integer minute;

    protected Integer decimal;

    public static DmdCoordinate empty() {
        return new DmdCoordinate();
    }

    /**
     * Methode statique de fabrique de position a partir d'un autre {@link DmdCoordinate}.
     *
     * Note : Si la valeur vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static DmdCoordinate valueOf(DmdCoordinate decimal) {
        DmdCoordinate r = empty();
        if (decimal != null) {
            r.setSign(decimal.isSign());
            r.setDegree(decimal.getDegree());
            r.setMinute(decimal.getMinute());
            r.setDecimal(decimal.getDecimal());
        }
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * decimal.
     *
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimal la valeur au format decimal
     * @return une nouvelle instance de position convertie
     */
    public static DmdCoordinate valueOf(Float decimal) {
        DmdCoordinate r = new DmdCoordinate();
        r.fromDecimal(decimal);
        return r;
    }

    /**
     * Methode statique de fabrique de position a partir d'une valeur du format
     * degre décimale minute.
     *
     * @param d  la valeur des degres
     * @param m  la valeur des minutes
     * @param dc la valeur des décimales de minutes
     * @return une nouvelle instance de position convertie
     */
    public static DmdCoordinate valueOf(boolean sign,
                                        Integer d,
                                        Integer m,
                                        Integer dc) {
        DmdCoordinate r = new DmdCoordinate();
        r.setSign(sign);
        r.setDegree(d);
        r.setMinute(m);
        r.setDecimal(dc);
        return r;
    }

    public boolean isSign() {
        return sign;
    }

    public Integer getDegree() {
        return degree;
    }

    public Integer getMinute() {
        return minute;
    }

    public Integer getDecimal() {
        return decimal;
    }

    public void setSign(boolean sign) {
        Object oldValue = isSign();
        this.sign = sign;
        firePropertyChange(PROPERTY_SIGN, oldValue, sign);
    }

    public void setDegree(Integer degree) {
        Object oldValue = getDegree();
        this.degree = degree;
        firePropertyChange(PROPERTY_DEGREE, oldValue, degree);
    }

    public void setMinute(Integer minute) {
        Object oldValue = getMinute();
        this.minute = minute;
        firePropertyChange(PROPERTY_MINUTE, oldValue, minute);
    }

    public void setDecimal(Integer decimal) {
        Object oldValue = getDecimal();
        this.decimal = decimal;
        firePropertyChange(PROPERTY_DECIMAL, oldValue, decimal);
    }

    /**
     * @return {@code true} si aucune composante n'est renseignée,
     * {@code false} autrement.
     */
    public boolean isNull() {
        return degree == null && minute == null && decimal == null;
    }

    public boolean isDegreeNull() {
        return degree == null || degree == 0;
    }

    public boolean isMinuteNull() {
        return minute == null || minute == 0;
    }

    public boolean isDecimalNull() {
        return decimal == null || decimal == 0;
    }

    /**
     * Mets a jour les composants de la position a partir d'une valeur decimal.
     *
     * Note : Si la valeur (au format decimal) vaut <code>null</code>, alors on
     * reinitialise les composants de la position a <code>null</code> et la
     * methode {@link #isNull()} vaudra alors {@code true}.
     *
     * @param decimalValue la valeur decimale a convertir (qui peut etre nulle).
     */
    public void fromDecimal(Float decimalValue) {
        Integer d = null;
        Integer m = null;
        Integer dc = null;
        boolean si = false;
        if (decimalValue != null) {
            si = decimalValue < 0;

            float absDecimal = Math.abs(decimalValue);

            d = (int) (Math.round(absDecimal + 0.5) - 1);
            int rest = Math.round(100 * 60.0f * (absDecimal - d));
            if (rest > 0) {
                m = rest / 100;
                dc = (rest - m * 100);
            }
        }

        degree = d;
        minute = m;
        decimal = dc;
        sign = si;

        if (decimal != null) {
            removeTrailingZero();
        }
    }

    public Float toDecimal() {
        if (isNull()) {
            return null;
        }
        Integer d = getNotNullDegree();
        Integer m = getNotNullMinute();
        Integer dc = getNotNullDecimal();
        Float result = Float.valueOf(d);
        result += (m + (dc / 100f)) / 60.0f;
        if (sign) {
            result *= -1;
        }
        result = CoordinateHelper.roundToFourDecimals(result);
        return result;
    }

    public void addTrailingZero() {

        if (degree == null) {
            degree = 0;
        }
        if (minute == null) {
            minute = 0;
        }
        if (decimal == null) {
            decimal = 0;
        }
    }

    public void removeTrailingZero() {
        if (degree != null && degree == 0) {
            degree = null;
        }
        if (minute != null && minute == 0) {
            minute = null;
        }
        if (decimal != null && decimal == 0) {
            decimal = null;
        }
    }

    public Integer getSignedDegree() {
        Integer result = null;
        if (!isDegreeNull()) {
            result = degree;
            if (isSign()) {
                result *= -1;
            }
        }
        return result;
    }

    public int getNotNullDegree() {
        return isDegreeNull() ? 0 : degree;
    }

    public int getNotNullMinute() {
        return isMinuteNull() ? 0 : minute;
    }

    public int getNotNullDecimal() {
        return isDecimalNull() ? 0 : decimal;
    }

    public boolean isLatitudeDegreeValid() {
        return isDegreeValid(false);
    }

    public boolean isLongitudeDegreeValid() {
        return isDegreeValid(true);
    }

    public boolean isMinuteValid() {
        boolean result = true;
        if (!isMinuteNull()) {
            if (minute == 60) {

                // can not have decimal
                result = isDecimalNull();
            } else {
                result = 0 <= minute && minute < 60;
            }
        }
        return result;
    }

    public boolean isDecimalValid() {
        return isDecimalNull() || (0 <= decimal && decimal < 100);
    }

    @Override
    public String toString() {
        return "DmdCoordinateComponent{" +
                "sign=" + sign +
                ", degree=" + degree +
                ", minute=" + minute +
                ", decimal=" + decimal +
                '}';
    }

    public void reset() {
        degree = null;
        minute = null;
        degree = null;
    }

    protected boolean isDegreeValid(boolean longitude) {
        boolean result = true;
        if (!isDegreeNull()) {
            int bound = longitude ? 180 : 90;
            if (bound == degree) {

                // can not have minute nor decimal
                result = isMinuteNull() && isDecimalNull();
            } else {
                result = 0 <= degree && degree < bound;
            }
        }
        return result;
    }

}
