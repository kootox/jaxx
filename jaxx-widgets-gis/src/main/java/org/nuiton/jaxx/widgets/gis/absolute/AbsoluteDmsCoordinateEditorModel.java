package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.widgets.gis.DmsCoordinate;

import java.io.Serializable;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class AbsoluteDmsCoordinateEditorModel extends DmsCoordinate {

    public static final String PROPERTY_BEAN = "bean";

    public static final String PROPERTY_PROPERTY_DEGREE = "propertyDegree";

    public static final String PROPERTY_PROPERTY_MINUTE = "propertyMinute";

    public static final String PROPERTY_PROPERTY_SECOND = "propertySecond";

    private static final long serialVersionUID = 1L;

    /** Bean where to push data. */
    protected Object bean;

    /** Name of the property of the bean to fire the change of the {@link #degree}. */
    protected String propertyDegree;

    /** Name of the property of the bean to fire the change of the {@link #minute}. */
    protected String propertyMinute;

    /** Name of the property of the bean to fire the change of the {@link #second}. */
    protected String propertySecond;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        Object oldValue = getBean();
        this.bean = bean;
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    public String getPropertyDegree() {
        return propertyDegree;
    }

    public void setPropertyDegree(String propertyDegree) {
        Object oldValue = getPropertyDegree();
        this.propertyDegree = propertyDegree;
        firePropertyChange(PROPERTY_PROPERTY_DEGREE, oldValue, propertyDegree);
    }

    public String getPropertyMinute() {
        return propertyMinute;
    }

    public void setPropertyMinute(String propertyMinute) {
        Object oldValue = getPropertyMinute();
        this.propertyMinute = propertyMinute;
        firePropertyChange(PROPERTY_PROPERTY_MINUTE, oldValue, propertyMinute);
    }

    public String getPropertySecond() {
        return propertySecond;
    }

    public void setPropertySecond(String propertySecond) {
        Object oldValue = getPropertySecond();
        this.propertySecond = propertySecond;
        firePropertyChange(PROPERTY_PROPERTY_SECOND, oldValue, propertySecond);
    }

    public void setValue(DmsCoordinate value) {
        setDegree(value == null ? null : value.getDegree());
        setMinute(value == null ? null : value.getMinute());
        setSecond(value == null ? null : value.getSecond());
    }

    @Override
    public boolean isSign() {
        return false;
    }

    @Override
    public void setSign(boolean sign) {
        // never use it
    }
}
