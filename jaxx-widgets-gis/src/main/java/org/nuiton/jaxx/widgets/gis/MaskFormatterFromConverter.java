package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;

/**
 * Created on 11/25/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class MaskFormatterFromConverter<O> extends MaskFormatter {

    /** Logger */
    private static final Log log = LogFactory.getLog(MaskFormatterFromConverter.class);

    private static final long serialVersionUID = 1L;

    private final Converter converter;

    private final Class<O> type;

    public static <O> MaskFormatterFromConverter<O> newFormatter(Class<O> type,
                                                                 String pattern,
                                                                 Converter converter) throws ParseException {
        return new MaskFormatterFromConverter<O>(type, pattern, converter);
    }

    protected MaskFormatterFromConverter(Class<O> type,
                                         String pattern,
                                         Converter converter) throws ParseException {
        super(pattern);
        this.type = type;
        this.converter = converter;
    }

    @Override
    public String valueToString(Object value) {
        String result = converter.convert(String.class, value);
        if (log.isInfoEnabled()) {
            log.info(value + " --> " + result);
        }
        return result;
    }

    @Override
    public Object stringToValue(String value) {
        Object result = converter.convert(type, value);
        if (log.isInfoEnabled()) {
            log.info(value + " --> " + result);
        }
        return result;
    }

    protected Converter getConverter() {
        return converter;
    }

    protected Class<O> getType() {
        return type;
    }
}
