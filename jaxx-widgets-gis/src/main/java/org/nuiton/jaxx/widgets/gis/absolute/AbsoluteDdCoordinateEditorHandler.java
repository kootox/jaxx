package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Objects;
import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.gis.DdCoordinate;
import org.nuiton.jaxx.widgets.gis.DdCoordinateConverter;
import org.nuiton.jaxx.widgets.gis.MaskFormatterFromConverter;
import org.nuiton.jaxx.widgets.jformattedtextfield.JFormattedTextFieldNavigationManager;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class AbsoluteDdCoordinateEditorHandler implements UIHandler<AbsoluteDdCoordinateEditor> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbsoluteDdCoordinateEditorHandler.class);

    private AbsoluteDdCoordinateEditor ui;

    protected Method degreeMutator;

    protected Method decimalMutator;

    protected boolean valueIsAdjusting;

    protected boolean valueModelIsAdjusting;

    protected DdCoordinateConverter coordinateConverter;

    @Override
    public void beforeInit(AbsoluteDdCoordinateEditor ui) {
        this.ui = ui;
        // can't use the one from ConverterUtil since we deal with some internal states
        this.coordinateConverter = new DdCoordinateConverter();
    }

    @Override
    public void afterInit(AbsoluteDdCoordinateEditor ui) {
        // nothing special to do here
    }

    public void resetModel() {
        ui.getModel().reset();
    }

    public void init(boolean longitudeEditor) {

        final AbsoluteDdCoordinateEditorModel model = ui.getModel();

        Objects.requireNonNull(model.getBean(), "could not find bean in " + ui);
        Objects.requireNonNull(model.getPropertyDegree(), "could not find propertyDegree in " + ui);
        Objects.requireNonNull(model.getPropertyDecimal(), "could not find propertyDecimal in " + ui);

        Object bean = model.getBean();

        degreeMutator = Setters.getMutator(bean, model.getPropertyDegree());
        Objects.requireNonNull(degreeMutator, "could not find mutator for " + model.getPropertyDegree());

        decimalMutator = Setters.getMutator(bean, model.getPropertyDecimal());
        Objects.requireNonNull(decimalMutator, "could not find mutator for " + model.getPropertyDecimal());

        coordinateConverter.setForLongitude(longitudeEditor);

        // prepare unsigned formatter factory
        String pattern = getMaskFormatterPattern(longitudeEditor);
        MaskFormatterFromConverter<DdCoordinate> maskFormatter;
        try {
            maskFormatter = MaskFormatterFromConverter.newFormatter(
                    DdCoordinate.class,
                    pattern, coordinateConverter);
            maskFormatter.setValidCharacters(" 01234567890");
            maskFormatter.setCommitsOnValidEdit(true);

        } catch (ParseException e) {
            // can't happen here
            throw new RuntimeException(e);
        }

        JFormattedTextField editor = ui.getEditor();

        DefaultFormatterFactory formatterFactory = new DefaultFormatterFactory(maskFormatter);
        editor.setFormatterFactory(formatterFactory);
        editor.setFocusLostBehavior(JFormattedTextField.COMMIT);

        // When editor changes his value, propagate it to model
        editor.addPropertyChangeListener("value", evt -> {
            DdCoordinate newValue = (DdCoordinate) evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("Value has changed: " + newValue);
            }
            model.setValue(newValue);
        });

        JFormattedTextFieldNavigationManager.install(editor);

        // When model degree changed, let's push it back in bean
        model.addPropertyChangeListener(
                AbsoluteDdCoordinateEditorModel.PROPERTY_DEGREE,
                new ModelPropertyChangeListener(model, degreeMutator));

        // When model decimal changed, let's push it back in bean
        model.addPropertyChangeListener(
                AbsoluteDdCoordinateEditorModel.PROPERTY_DECIMAL,
                new ModelPropertyChangeListener(model, decimalMutator));
    }

    public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) {

        coordinateConverter.setDisplayZeroWhenNull(displayZeroWhenNull);
        if (log.isDebugEnabled()) {
            log.debug("setDisplayZeroWhenNull: " + displayZeroWhenNull);
        }
        AbsoluteDdCoordinateEditorModel model = ui.getModel();
        JFormattedTextField editor = ui.getEditor();
        JFormattedTextField.AbstractFormatter formatter = editor.getFormatter();
        if (formatter != null) {

            try {
                String newStringValue = formatter.valueToString(model);
                if (log.isDebugEnabled()) {
                    log.debug("updating string value: " + newStringValue);
                }
                editor.setText(newStringValue);
            } catch (ParseException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not parse new string value", e);
                }
            }

        }

    }


    public void setFillWithZero(boolean fillWithZero) {
        coordinateConverter.setFillWithZero(fillWithZero);
    }

    public void setValue(DdCoordinate value, boolean pushToModel) {

        if (valueModelIsAdjusting) {
            // avoid re-entrant code
            return;
        }

        valueIsAdjusting = !pushToModel;

        try {
            ui.getEditor().setValue(value);
        } finally {
            valueIsAdjusting = false;
        }
    }

    public void resetEditor() {
        // set null value to model
        setValue(null, true);
    }

    protected String getMaskFormatterPattern(boolean longitudeEditor) {
        String pattern = "**.***";
        if (longitudeEditor) {
            // add one more degre
            pattern = "*" + pattern;
        }
        return pattern;
    }

    private class ModelPropertyChangeListener implements PropertyChangeListener {

        private final AbsoluteDdCoordinateEditorModel model;

        private final Method mutator;

        private ModelPropertyChangeListener(AbsoluteDdCoordinateEditorModel model, Method mutator) {
            this.model = model;
            this.mutator = mutator;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (!valueIsAdjusting) {
                Object newValue = evt.getNewValue();

                try {

                    valueModelIsAdjusting = true;
                    try {
                        mutator.invoke(model.getBean(), newValue);
                    } finally {
                        valueModelIsAdjusting = false;
                    }

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
