package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.CoordinateHelper;

import java.io.Serializable;

/**
 * Created on 8/31/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class CoordinatesEditorModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_LATITUDE = "latitude";

    public static final String PROPERTY_LONGITUDE = "longitude";

    public static final String PROPERTY_QUADRANT = "quadrant";

    public static final String PROPERTY_FORMAT = "format";

    public static final String PROPERTY_SHOW_RESET_BUTTON = "showResetButton";
    public static final String PROPERTY_DISPLAY_ZERO_WHEN_NULL = "displayZeroWhenNull";
    public static final String PROPERTY_FILL_WITH_ZERO = "fillWithZero";

    protected final AbsoluteCoordinateEditorModel latitudeModel = new AbsoluteCoordinateEditorModel();

    protected final AbsoluteCoordinateEditorModel longitudeModel = new AbsoluteCoordinateEditorModel();

    /** Bean where to push back data. */
    protected Object bean;

    /** Name of the property of the bean to fire the change of the {@link #latitudeModel}. */
    protected String propertyLatitude;

    /** Name of the property of the bean to fire the change of the {@link #longitudeModel}. */
    protected String propertyLongitude;

    /** Name of the property of the bean to fire the change of the {@link #quadrant}. */
    protected String propertyQuadrant;

    protected Integer quadrant;

    protected CoordinateFormat format;

    protected boolean showResetButton;

    /**
     * Display a zero on each coordinate componant when it is null.
     */
    protected boolean displayZeroWhenNull;

    /**
     * Prefix any none null value with some zero.
     */
    protected boolean fillWithZero;

    private boolean valueIsAdjusting;

    public CoordinatesEditorModel() {

        latitudeModel.addPropertyChangeListener(evt -> {
            if (!valueIsAdjusting) {
                fireLatitude();
            }
        });

        longitudeModel.addPropertyChangeListener(evt -> {
            if (!valueIsAdjusting) {
                fireLongitude();
            }
        });

        addPropertyChangeListener(PROPERTY_QUADRANT, evt -> {
            fireLatitude();
            fireLongitude();
        });

        addPropertyChangeListener(PROPERTY_FORMAT, evt -> {

            valueIsAdjusting = true;

            try {

                CoordinateFormat oldFormat = (CoordinateFormat) evt.getOldValue();

                if (oldFormat != null) {

                    // synchronize latitude value from the previous format
                    Float latitude = latitudeModel.getValue(oldFormat);
                    latitudeModel.setValue(latitude);

                    // synchronize longitude value from the previous format
                    Float longitude = longitudeModel.getValue(oldFormat);
                    longitudeModel.setValue(longitude);

                }

            } finally {

                valueIsAdjusting = false;

                fireLatitude();
                fireLongitude();

            }
        });
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public String getPropertyLatitude() {
        return propertyLatitude;
    }

    public void setPropertyLatitude(String propertyLatitude) {
        this.propertyLatitude = propertyLatitude;
    }

    public String getPropertyLongitude() {
        return propertyLongitude;
    }

    public void setPropertyLongitude(String propertyLongitude) {
        this.propertyLongitude = propertyLongitude;
    }

    public String getPropertyQuadrant() {
        return propertyQuadrant;
    }

    public void setPropertyQuadrant(String propertyQuadrant) {
        this.propertyQuadrant = propertyQuadrant;
    }

    public boolean isDisplayZeroWhenNull() {
        return displayZeroWhenNull;
    }

    public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) {
        boolean oldValue = isDisplayZeroWhenNull();
        this.displayZeroWhenNull = displayZeroWhenNull;
        firePropertyChange(PROPERTY_DISPLAY_ZERO_WHEN_NULL, oldValue, displayZeroWhenNull);
    }

    public boolean isFillWithZero() {
        return fillWithZero;
    }

    public void setFillWithZero(boolean fillWithZero) {
        boolean oldValue = isFillWithZero();
        this.fillWithZero = fillWithZero;
        firePropertyChange(PROPERTY_FILL_WITH_ZERO, oldValue, fillWithZero);
    }

    public boolean isShowResetButton() {
        return showResetButton;
    }

    public void setShowResetButton(boolean showResetButton) {
        boolean oldValue = isShowResetButton();
        this.showResetButton = showResetButton;
        firePropertyChange(PROPERTY_SHOW_RESET_BUTTON, oldValue, showResetButton);
    }

    public AbsoluteCoordinateEditorModel getLatitudeModel() {
        return latitudeModel;
    }

    public AbsoluteCoordinateEditorModel getLongitudeModel() {
        return longitudeModel;
    }

    public void setLatitudeAndLongitude(Float latitude, Float longitude) {

        valueIsAdjusting = true;
        Integer newQuadrant = CoordinateHelper.getQuadrant(longitude, latitude);
        boolean quadrantChanged = newQuadrant != null;

        try {

            if (quadrantChanged) {

                // only change not nul quadrant
                this.quadrant = newQuadrant;

            }

            this.latitudeModel.setValue(latitude);
            this.longitudeModel.setValue(longitude);

        } finally {

            valueIsAdjusting = false;

            fireLatitude();
            fireLongitude();
            if (quadrantChanged) {
                fireQuadrant();
            }

        }

    }

    public Float getLatitude() {

        Float latitude;

        if (format == null) {
            latitude = null;
        } else {
            Float absoluteLatitude = latitudeModel.getValue(format);
            latitude = CoordinateHelper.getSignedLatitude(quadrant, absoluteLatitude);
        }

        return latitude;

    }

    public void setLatitude(Float latitude) {

        valueIsAdjusting = true;

//        Integer newQuadrant = CoordinateHelper.getQuadrant(getLongitude(), latitude);
//        boolean quadrantChanged = newQuadrant != null;

        try {

//            if (quadrantChanged) {
//
//                // only change not nul quadrant
//                this.quadrant = newQuadrant;
//
//            }

            this.latitudeModel.setValue(latitude);

        } finally {

            valueIsAdjusting = false;

            fireLatitude();
//            if (quadrantChanged) {
//                fireQuadrant();
//            }

        }

    }

    public Float getLongitude() {

        Float longitude;

        if (format == null) {
            longitude = null;
        } else {
            Float absoluteLongitude = longitudeModel.getValue(format);
            longitude = CoordinateHelper.getSignedLongitude(quadrant, absoluteLongitude);
        }

        return longitude;

    }

    public void setLongitude(Float longitude) {

        valueIsAdjusting = true;

//        Integer newQuadrant = CoordinateHelper.getQuadrant(longitude, getLatitude());
//        boolean changeQuandrant = newQuadrant != null;

        try {

//            if (changeQuandrant) {
//
//                // only change not null quadrant
//                this.quadrant = newQuadrant;
//
//            }

            this.longitudeModel.setValue(longitude);

        } finally {

            valueIsAdjusting = false;

            fireLongitude();
//            if (changeQuandrant) {
//                fireQuadrant();
//            }

        }

    }

    public Integer getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(Integer quadrant) {
        Object oldValue = getQuadrant();
        this.quadrant = quadrant;
        firePropertyChange(PROPERTY_QUADRANT, oldValue, quadrant);
    }

    public CoordinateFormat getFormat() {
        return format;
    }

    public void setFormat(CoordinateFormat format) {
        Object oldValue = getFormat();
        this.format = format;
        firePropertyChange(PROPERTY_FORMAT, oldValue, format);
    }

    protected void fireLatitude() {
        firePropertyChange(PROPERTY_LATITUDE, null, getLatitude());
    }

    protected void fireLongitude() {
        firePropertyChange(PROPERTY_LONGITUDE, null, getLongitude());
    }

    protected void fireQuadrant() {
        firePropertyChange(PROPERTY_QUADRANT, null, getQuadrant());
    }

    protected void updateFormat(CoordinateFormat format) {

        if (format != null) {

            Float latitude = latitudeModel.getValue(format);
            latitudeModel.setValue(latitude);

            Float longitude = longitudeModel.getValue(format);
            longitudeModel.setValue(longitude);

        }

    }

}
