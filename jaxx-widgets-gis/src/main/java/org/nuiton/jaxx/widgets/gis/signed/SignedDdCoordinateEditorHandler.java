package org.nuiton.jaxx.widgets.gis.signed;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Objects;
import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.gis.DdCoordinate;
import org.nuiton.jaxx.widgets.gis.DdCoordinateConverter;
import org.nuiton.jaxx.widgets.gis.MaskFormatterFromConverter;

/**
 * Created on 10/16/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class SignedDdCoordinateEditorHandler implements UIHandler<SignedDdCoordinateEditor> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SignedDdCoordinateEditorHandler.class);

    private SignedDdCoordinateEditor ui;

    protected Method signMutator;

    protected Method degreMutator;

    protected Method decimalMutator;

    protected boolean valueIsAdjusting;

    protected boolean valueModelIsAdjusting;

    protected DdCoordinateConverter signedConverter;

    protected DdCoordinateConverter unsignedConverter;

    protected DefaultFormatterFactory signedFormatterFactory;

    protected DefaultFormatterFactory unsignedFormatterFactory;

    @Override
    public void beforeInit(SignedDdCoordinateEditor ui) {
        this.ui = ui;
        // can't use the one from ConverterUtil since we deal with some internal states
        this.signedConverter = new DdCoordinateConverter();
        this.signedConverter.setUseSign(true);
        this.unsignedConverter = new DdCoordinateConverter();
    }

    @Override
    public void afterInit(SignedDdCoordinateEditor ui) {
        // nothing special to do here
    }

    public void init(boolean longitudeEditor) {

        final SignedDdCoordinateEditorModel model = ui.getModel();

        Objects.requireNonNull(model.getBean(), "could not find bean in " + ui);
        Objects.requireNonNull(model.getPropertySign(), "could not find propertySign in " + ui);
        Objects.requireNonNull(model.getPropertyDegree(), "could not find propertyDegree in " + ui);
        Objects.requireNonNull(model.getPropertyDecimal(), "could not find propertyDecimal in " + ui);

        Object bean = model.getBean();
        signMutator = Setters.getMutator(bean, model.getPropertySign());
        Objects.requireNonNull(signMutator, "could not find mutator for " + model.getPropertySign());

        degreMutator = Setters.getMutator(bean, model.getPropertyDegree());
        Objects.requireNonNull(degreMutator, "could not find mutator for " + model.getPropertyDegree());

        decimalMutator = Setters.getMutator(bean, model.getPropertyDecimal());
        Objects.requireNonNull(decimalMutator, "could not find mutator for " + model.getPropertyDecimal());

        signedConverter.setForLongitude(longitudeEditor);
        unsignedConverter.setForLongitude(longitudeEditor);

        {
            // prepare unsigned formatter factory
            String pattern = getMaskFormatterPattern(longitudeEditor, false);
            MaskFormatterFromConverter<DdCoordinate> maskFormatter;
            try {
                maskFormatter = MaskFormatterFromConverter.newFormatter(
                        DdCoordinate.class,
                        pattern, unsignedConverter);
                maskFormatter.setValidCharacters(" 01234567890");
                maskFormatter.setCommitsOnValidEdit(true);
                unsignedFormatterFactory = new DefaultFormatterFactory(maskFormatter);
            } catch (ParseException e) {
                // can't happen here
                throw new RuntimeException(e);
            }
        }
        {
            // prepare signed formatter factory
            String pattern = getMaskFormatterPattern(longitudeEditor, true);
            MaskFormatterFromConverter<DdCoordinate> maskFormatter;
            try {
                maskFormatter = MaskFormatterFromConverter.newFormatter(
                        DdCoordinate.class,
                        pattern, signedConverter);
                maskFormatter.setValidCharacters(" 01234567890");
                maskFormatter.setCommitsOnValidEdit(true);
                signedFormatterFactory = new DefaultFormatterFactory(maskFormatter);
            } catch (ParseException e) {
                // can't happen here
                throw new RuntimeException(e);
            }
        }

        JFormattedTextField editor = ui.getEditor();
        editor.setFormatterFactory(model.isSign() ?
                                           signedFormatterFactory :
                                           unsignedFormatterFactory);
        editor.setFocusLostBehavior(JFormattedTextField.COMMIT);

        // When editor changes his value, propagate it to model
        editor.addPropertyChangeListener("value", evt -> {
            DdCoordinate newValue = (DdCoordinate) evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("Value has changed: " + newValue);
            }
            model.setValue(newValue);
        });

        // When model sign changed, let's push it back in bean
        model.addPropertyChangeListener(
                SignedDdCoordinateEditorModel.PROPERTY_SIGN,
                new ModelPropertyChangeListener(model, signMutator));

        // When model degre changed, let's push it back in bean
        model.addPropertyChangeListener(
                SignedDdCoordinateEditorModel.PROPERTY_DEGREE,
                new ModelPropertyChangeListener(model, degreMutator));

        // When model decimal changed, let's push it back in bean
        model.addPropertyChangeListener(
                SignedDdCoordinateEditorModel.PROPERTY_DECIMAL,
                new ModelPropertyChangeListener(model, decimalMutator));
    }

    public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) {

        signedConverter.setDisplayZeroWhenNull(displayZeroWhenNull);
        unsignedConverter.setDisplayZeroWhenNull(displayZeroWhenNull);
        if (log.isDebugEnabled()) {
            log.debug("setDisplayZeroWhenNull: " + displayZeroWhenNull);
        }
        SignedDdCoordinateEditorModel model = ui.getModel();
        JFormattedTextField editor = ui.getEditor();
        JFormattedTextField.AbstractFormatter formatter = editor.getFormatter();
        if (formatter != null) {

            try {
                String newStringValue = formatter.valueToString(model);
                if (log.isDebugEnabled()) {
                    log.debug("updating string value: " + newStringValue);
                }
                editor.setText(newStringValue);
            } catch (ParseException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not parse new string value", e);
                }
            }

        }

    }


    public void setFillWithZero(boolean fillWithZero) {
        signedConverter.setFillWithZero(fillWithZero);
        unsignedConverter.setFillWithZero(fillWithZero);
    }


    public void setValue(DdCoordinate value, boolean pushToModel) {

        if (valueModelIsAdjusting) {
            // avoid re-entrant code
            return;
        }

        valueIsAdjusting = !pushToModel;

        try {
            ui.getEditor().setValue(value);
        } finally {
            valueIsAdjusting = false;
        }
    }

    public void resetEditor() {
        // set null value to model
        setValue(null, true);

        // use back unsigned format
        ui.getEditor().setFormatterFactory(unsignedFormatterFactory);
    }

    public void onKeyReleased(KeyEvent e) {

        JFormattedTextField source = (JFormattedTextField) e.getSource();

        char keyChar = e.getKeyChar();
        int caretPosition = source.getCaretPosition();
        if (log.isDebugEnabled()) {
            log.debug("Key pressed: " + keyChar + " (caret position: " + caretPosition + ")");
        }

        if (keyChar == '-') {

            DdCoordinate value = (DdCoordinate) source.getValue();

            DefaultFormatterFactory newFactory;

            // try to switch unsigned to signed

            boolean useSign = ui.getModel().isSign();

            if (useSign) {

                if (log.isDebugEnabled()) {
                    log.debug("Switch to unsigned");
                }

                newFactory = unsignedFormatterFactory;

                // remove a sign
                caretPosition--;
            } else {
                // switch to signed
                if (log.isDebugEnabled()) {
                    log.debug("Switch to signed");
                }
                newFactory = signedFormatterFactory;

                // add a sign
                caretPosition++;
            }

            DdCoordinate newValue = DdCoordinate.valueOf(value);
            newValue.setSign(!useSign);

            source.setFormatterFactory(newFactory);
            source.setValue(newValue);

            e.consume();

            source.setCaretPosition(caretPosition);
        }
    }

    protected String getMaskFormatterPattern(boolean longitudeEditor, boolean useSign) {
        String pattern = "**.***";
        if (longitudeEditor) {
            // add one more degre
            pattern = "*" + pattern;
        }
        if (useSign) {
            pattern = "-" + pattern;
        }
        return pattern;
    }

    private class ModelPropertyChangeListener implements PropertyChangeListener {

        private final SignedDdCoordinateEditorModel model;

        private final Method mutator;

        private ModelPropertyChangeListener(SignedDdCoordinateEditorModel model,
                                            Method mutator) {
            this.model = model;
            this.mutator = mutator;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (!valueIsAdjusting) {
                Object newValue = evt.getNewValue();

                try {

                    valueModelIsAdjusting = true;
                    try {
                        mutator.invoke(model.getBean(), newValue);
                    } finally {
                        valueModelIsAdjusting = false;
                    }

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
