package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 10/25/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DmdCoordinateTest {

    @Test
    public void testFromDecimal() throws Exception {

        {
            DmdCoordinate coordinate = DmdCoordinate.empty();

            coordinate.fromDecimal(42.7f);

            assertDmdCoordinate(coordinate, false, 42, 42, null);

            coordinate.addTrailingZero();

            assertDmdCoordinate(coordinate, false, 42, 42, 0);

            coordinate.removeTrailingZero();

            assertDmdCoordinate(coordinate, false, 42, 42, null);
        }

        {
            DmdCoordinate coordinate = DmdCoordinate.empty();

            float decimalExcepted = 42.707f;
            coordinate.fromDecimal(decimalExcepted);

            assertDmdCoordinate(coordinate, false, 42, 42, 42);

            Float decimal = coordinate.toDecimal();
            Assert.assertEquals(decimalExcepted, decimal, 0.001);
        }
    }

    @Test
    public void testToDecimal() throws Exception {

        {
            DmdCoordinate coordinate = DmdCoordinate.empty();
            coordinate.setDegree(42);
            coordinate.setMinute(42);
            coordinate.setDecimal(42);
            Float actual = coordinate.toDecimal();
            Float expected = 42.707f;
            Assert.assertEquals(expected, actual, 0.001);
        }

        {
            DmdCoordinate coordinate = DmdCoordinate.empty();
            coordinate.setDegree(12);
            coordinate.setMinute(12);
            Float actual = coordinate.toDecimal();
            Float expected = 12.2f;
            Assert.assertEquals(expected, actual, 0.0001);

            coordinate.fromDecimal(expected);

            assertDmdCoordinate(coordinate, false, 12, 12, null);
        }

        {
            DmdCoordinate component = DmdCoordinate.empty();
            component.setDegree(12);
            component.setMinute(12);
            component.setDecimal(20);
            Float actual = component.toDecimal();
            Float expected = 12.203333f;
            Assert.assertEquals(expected, actual, 0.001);

            component.fromDecimal(expected);

            assertDmdCoordinate(component, false, 12, 12, 20);

        }
    }

    @Test
    public void testValueOf() throws Exception {

        DmdCoordinate coordinate = DmdCoordinate.valueOf(-0.007333f);
        Assert.assertNotNull(coordinate);
        Assert.assertTrue(coordinate.isDegreeNull());
        Assert.assertTrue(coordinate.isMinuteNull());

        Assert.assertTrue(coordinate.isDegreeValid(true));
        Assert.assertTrue(coordinate.isMinuteValid());
        Assert.assertTrue(coordinate.isDecimalValid());

        assertDmdCoordinate(coordinate, true, null, null, 44);
    }

    public static void assertDmdCoordinate(DmdCoordinate coordinate,
                                           boolean expectedSign,
                                           Integer expectedDegree,
                                           Integer expectedMinute,
                                           Integer expectedDecimal) {
        Assert.assertNotNull(coordinate);
        Assert.assertEquals(expectedSign, coordinate.isSign());
        Assert.assertEquals(expectedDegree, coordinate.getDegree());
        Assert.assertEquals(expectedMinute, coordinate.getMinute());
        Assert.assertEquals(expectedDecimal, coordinate.getDecimal());
    }
}
