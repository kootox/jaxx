package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.converter.ConverterUtil;

/**
 * Created on 11/25/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DmsCoordinateConverterTest {


    @Test
    public void testConvert() throws Exception {

        ConverterUtil.initConverters();

        testConversion(false, "-  °  '26''", DmsCoordinate.valueOf(-0.007333f));
        testConversion(false, "  °  '26''", DmsCoordinate.valueOf(0.007333f));
        testConversion(false, "  °  '59''", DmsCoordinate.valueOf(false, null, null, 59));
        testConversion(false, "  °  '  ''", DmsCoordinate.empty());
        testConversion(false, "  °39'59''", DmsCoordinate.valueOf(false, null, 39, 59));
        testConversion(false, "  °39'  ''", DmsCoordinate.valueOf(false, null, 39, null));
        testConversion(false, " 0°39'59''", DmsCoordinate.valueOf(false, 0, 39, 59));
        testConversion(false, " 0°  '  ''", DmsCoordinate.valueOf(false, 0, null, null));

        testConversion(true, "-   °  '26''", DmsCoordinate.valueOf(-0.007333f));
        testConversion(true, "   °  '26''", DmsCoordinate.valueOf(0.007333f));
        testConversion(true, "   °  '  ''", DmsCoordinate.empty());
        testConversion(true, "   °  '59''", DmsCoordinate.valueOf(false, null, null, 59));
        testConversion(true, "   °39'59''", DmsCoordinate.valueOf(false, null, 39, 59));
        testConversion(true, "   °39'  ''", DmsCoordinate.valueOf(false, null, 39, null));
        testConversion(true, "  0°39'59''", DmsCoordinate.valueOf(false, 0, 39, 59));
        testConversion(true, "  0°  '  ''", DmsCoordinate.valueOf(false, 0, null, null));
    }

    protected void testConversion(boolean forLongitude,
                                  String expectedString,
                                  DmsCoordinate expectedCoordinate) {

        DmsCoordinateConverter converter = (DmsCoordinateConverter) ConverterUtil.getConverter(DmsCoordinate.class);
        converter.setForLongitude(forLongitude);
        Assert.assertNotNull(converter);

        String actualStr;
        DmsCoordinate actualCoordinate;

        // String -> DmsCoordinate

        actualCoordinate = converter.convert(DmsCoordinate.class, expectedString);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getMinute(), expectedCoordinate.getMinute());
        Assert.assertEquals(actualCoordinate.getSecond(), expectedCoordinate.getSecond());

        // DmsCoordinate -> String

        actualStr = converter.convert(String.class, expectedCoordinate);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // String -> String

        actualStr = converter.convert(String.class, expectedString);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // DmsCoordinate -> DmsCoordinate

        actualCoordinate = converter.convert(DmsCoordinate.class, expectedCoordinate);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getMinute(), expectedCoordinate.getMinute());
        Assert.assertEquals(actualCoordinate.getSecond(), expectedCoordinate.getSecond());
    }
}
