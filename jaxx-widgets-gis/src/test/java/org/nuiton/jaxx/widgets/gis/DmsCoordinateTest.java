package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 11/25/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DmsCoordinateTest {

    @Test
    public void testFromDecimal() throws Exception {

        {
            DmsCoordinate actual = DmsCoordinate.empty();

            actual.fromDecimal(42.7f);

            assertDmsCoordinate(actual, false, 42, 42, null);
        }

        {
            DmsCoordinate actual = DmsCoordinate.empty();

            float decimalExcepted = 42.711f;
            actual.fromDecimal(decimalExcepted);

            assertDmsCoordinate(actual, false, 42, 42, 39);

            Float decimal = actual.toDecimal();
            Assert.assertEquals(decimalExcepted, decimal, 0.001);
        }
    }

    @Test
    public void testToDecimal() throws Exception {

        {
            DmsCoordinate coordinate = DmsCoordinate.empty();
            coordinate.setDegree(42);
            coordinate.setMinute(42);
            coordinate.setSecond(42);
            Float floatValue = coordinate.toDecimal();
            Float expected = 42.712f;
            Assert.assertEquals(expected, floatValue, 0.001);
        }

        {
            DmsCoordinate coordinate = DmsCoordinate.empty();
            coordinate.setDegree(12);
            coordinate.setMinute(12);

            Float floatValue = coordinate.toDecimal();
            Float expected = 12.2001f;
            Assert.assertEquals(expected, floatValue, 0.001);

            coordinate.fromDecimal(expected);
            assertDmsCoordinate(coordinate, false, 12, 12, null);

        }

        {
            DmsCoordinate coordinate = DmsCoordinate.empty();
            coordinate.setDegree(12);
            coordinate.setMinute(12);
            coordinate.setSecond(20);
            Float floatValue = coordinate.toDecimal();
            Float expected = 12.20569f;
            Assert.assertEquals(expected, floatValue, 0.001);

            coordinate.fromDecimal(expected);

            assertDmsCoordinate(coordinate, false, 12, 12, 20);
        }
    }


    @Test
    public void testValueOf() throws Exception {

        DmsCoordinate coordinate = DmsCoordinate.valueOf(-0.007333f);
        Assert.assertNotNull(coordinate);
        Assert.assertTrue(coordinate.isDegreeNull());
        Assert.assertTrue(coordinate.isMinuteNull());
        Assert.assertFalse(coordinate.isSecondNull());


        Assert.assertTrue(coordinate.isDegreeValid(true));
        Assert.assertTrue(coordinate.isMinuteValid());
        Assert.assertTrue(coordinate.isSecondValid());

        assertDmsCoordinate(coordinate, true, null, null, 26);
    }

    public static void assertDmsCoordinate(DmsCoordinate coordinate,
                                           boolean expectedSign,
                                           Integer expectedDegree,
                                           Integer expectedMinute,
                                           Integer expectedSecond) {
        Assert.assertNotNull(coordinate);
        Assert.assertEquals(expectedSign, coordinate.isSign());
        Assert.assertEquals(expectedDegree, coordinate.getDegree());
        Assert.assertEquals(expectedMinute, coordinate.getMinute());
        Assert.assertEquals(expectedSecond, coordinate.getSecond());
    }
}
