package org.nuiton.jaxx.widgets.gis.signed;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.widgets.gis.DmdCoordinate;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.text.ParseException;

/**
 * To test the {@link SignedDmsCoordinateEditor}.
 *
 * Created on 10/17/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class SignedDmdCoordinateEditorTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SignedDmdCoordinateEditorTest.class);

    public static class EditorBean extends AbstractSerializableBean {

        private static final long serialVersionUID = 1L;

        public static final String PROPERTY_LONGITUDE_SIGN = "longitudeSign";

        public static final String PROPERTY_LONGITUDE_DEGREE = "longitudeDegree";

        public static final String PROPERTY_LONGITUDE_MINUTE = "longitudeMinute";

        public static final String PROPERTY_LONGITUDE_DECIMAL = "longitudeDecimal";

        public static final String PROPERTY_LATITUDE_SIGN = "latitudeSign";

        public static final String PROPERTY_LATITUDE_DEGREE = "latitudeDegree";

        public static final String PROPERTY_LATITUDE_MINUTE = "latitudeMinute";

        public static final String PROPERTY_LATITUDE_DECIMAL = "latitudeDecimal";

        protected final DmdCoordinate longitude = DmdCoordinate.empty();

        protected final DmdCoordinate latitude = DmdCoordinate.empty();

        public DmdCoordinate getLongitude() {
            return longitude;
        }

        public void setLongitudeDegree(Integer degre) {
            Object oldValue = longitude.getDegree();
            longitude.setDegree(degre);
            firePropertyChange(PROPERTY_LONGITUDE_DEGREE, oldValue, degre);
        }

        public void setLongitudeMinute(Integer minute) {
            Object oldValue = longitude.getMinute();
            longitude.setMinute(minute);
            firePropertyChange(PROPERTY_LONGITUDE_MINUTE, oldValue, minute);
        }

        public void setLongitudeDecimal(Integer decimal) {
            Object oldValue = longitude.getDecimal();
            longitude.setDecimal(decimal);
            firePropertyChange(PROPERTY_LONGITUDE_DECIMAL, oldValue, decimal);
        }

        public void setLongitudeSign(boolean sign) {
            Object oldValue = longitude.isSign();
            longitude.setSign(sign);
            firePropertyChange(PROPERTY_LONGITUDE_SIGN, oldValue, sign);
        }

        public DmdCoordinate getLatitude() {
            return latitude;
        }

        public void setLatitudeSign(boolean sign) {
            Object oldValue = latitude.isSign();
            latitude.setSign(sign);
            firePropertyChange(PROPERTY_LATITUDE_SIGN, oldValue, sign);
        }

        public void setLatitudeDegree(Integer degre) {
            Object oldValue = latitude.getDegree();
            latitude.setDegree(degre);
            firePropertyChange(PROPERTY_LATITUDE_DEGREE, oldValue, degre);
        }

        public void setLatitudeMinute(Integer minute) {
            Object oldValue = latitude.getMinute();
            latitude.setMinute(minute);
            firePropertyChange(PROPERTY_LATITUDE_MINUTE, oldValue, minute);
        }

        public void setLatitudeDecimal(Integer decimal) {
            Object oldValue = latitude.getDecimal();
            latitude.setDecimal(decimal);
            firePropertyChange(PROPERTY_LATITUDE_DECIMAL, oldValue, decimal);
        }

        @Override
        public String toString() {
            return "EditorBean{" +
                    "longitude=" + longitude +
                    ", latitude=" + latitude +
                    '}';
        }
    }

    public static void main(String[] args) throws ParseException {


        EditorBean bean = new EditorBean();

        SignedDmdCoordinateEditor longitudeEditor = new SignedDmdCoordinateEditor();
        longitudeEditor.setBean(bean);
        longitudeEditor.setPropertySign(EditorBean.PROPERTY_LONGITUDE_SIGN);
        longitudeEditor.setPropertyDegree(EditorBean.PROPERTY_LONGITUDE_DEGREE);
        longitudeEditor.setPropertyMinute(EditorBean.PROPERTY_LONGITUDE_MINUTE);
        longitudeEditor.setPropertyDecimal(EditorBean.PROPERTY_LONGITUDE_DECIMAL);
        longitudeEditor.setShowReset(true);
        longitudeEditor.init(true);

        SignedDmdCoordinateEditor latitudeEditor = new SignedDmdCoordinateEditor();
        latitudeEditor.setBean(bean);
        latitudeEditor.setPropertySign(EditorBean.PROPERTY_LATITUDE_SIGN);
        latitudeEditor.setPropertyDegree(EditorBean.PROPERTY_LATITUDE_DEGREE);
        latitudeEditor.setPropertyMinute(EditorBean.PROPERTY_LATITUDE_MINUTE);
        latitudeEditor.setPropertyDecimal(EditorBean.PROPERTY_LATITUDE_DECIMAL);
        latitudeEditor.setShowReset(true);
        latitudeEditor.init(false);

        final JLabel latitudeResult = new JLabel();
        final JLabel longitudeResult = new JLabel();

        bean.addPropertyChangeListener(evt -> {
            EditorBean source = (EditorBean) evt.getSource();
            String propertyName = evt.getPropertyName();
            if (log.isInfoEnabled()) {
                log.info("[" + propertyName + "] value changed: " + evt.getNewValue());
            }
            if (propertyName.startsWith("longitude")) {
                longitudeResult.setText(source.getLongitude().toString());
            } else {

                latitudeResult.setText(source.getLatitude().toString());
            }
        });

        JPanel longitudeEditorPanel = new JPanel(new GridLayout());
        longitudeEditorPanel.setBorder(new TitledBorder("Longitude"));
        longitudeEditorPanel.add(BorderLayout.CENTER, longitudeEditor);
        longitudeEditorPanel.add(BorderLayout.EAST, longitudeResult);

        JPanel latitudeEditorPanel = new JPanel(new GridLayout());
        latitudeEditorPanel.setBorder(new TitledBorder("Latitude"));
        latitudeEditorPanel.add(BorderLayout.CENTER, latitudeEditor);
        latitudeEditorPanel.add(BorderLayout.EAST, latitudeResult);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(longitudeEditorPanel);
        panel.add(latitudeEditorPanel);

        final JDialog frame = new JDialog();

        frame.setContentPane(panel);

        SwingUtilities.invokeLater(() -> {
            frame.setSize(800, 200);
            frame.setVisible(true);
        });
    }
}
