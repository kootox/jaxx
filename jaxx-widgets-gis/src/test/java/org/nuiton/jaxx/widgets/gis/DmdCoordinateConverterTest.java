package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.converter.ConverterUtil;

/**
 * Created on 11/25/13.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DmdCoordinateConverterTest {

    @Test
    public void testConvert() throws Exception {

        ConverterUtil.initConverters();

        testConversion(false, "-  °  .44'", DmdCoordinate.valueOf(-0.007333f));
        testConversion(false, "  °  .44'", DmdCoordinate.valueOf(0.007333f));
        testConversion(false, "  °  .99'", DmdCoordinate.valueOf(false, null, null, 99));
        testConversion(false, "  °  .  '", DmdCoordinate.empty());
        testConversion(false, "  °39.99'", DmdCoordinate.valueOf(false, null, 39, 99));
        testConversion(false, "  °39.  '", DmdCoordinate.valueOf(false, null, 39, null));
        testConversion(false, " 0°39.99'", DmdCoordinate.valueOf(false, 0, 39, 99));
        testConversion(false, " 0°  .  '", DmdCoordinate.valueOf(false, 0, null, null));

        testConversion(true, "-   °  .44'", DmdCoordinate.valueOf(-0.007333f));
        testConversion(true, "   °  .44'", DmdCoordinate.valueOf(0.007333f));
        testConversion(true, "   °  .  '", DmdCoordinate.empty());
        testConversion(true, "   °  .99'", DmdCoordinate.valueOf(false, null, null, 99));
        testConversion(true, "   °39.99'", DmdCoordinate.valueOf(false, null, 39, 99));
        testConversion(true, "   °39.  '", DmdCoordinate.valueOf(false, null, 39, null));
        testConversion(true, "  0°39.99'", DmdCoordinate.valueOf(false, 0, 39, 99));
        testConversion(true, "  0°  .  '", DmdCoordinate.valueOf(false, 0, null, null));
    }

    protected void testConversion(boolean forLongitude, String expectedString, DmdCoordinate expectedCoordinate) {

        DmdCoordinateConverter converter = (DmdCoordinateConverter) ConverterUtil.getConverter(DmdCoordinate.class);
        converter.setForLongitude(forLongitude);
        Assert.assertNotNull(converter);

        String actualStr;
        DmdCoordinate actualCoordinate;

        // String -> DmdCoordinate

        actualCoordinate = converter.convert(DmdCoordinate.class, expectedString);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getMinute(), expectedCoordinate.getMinute());
        Assert.assertEquals(actualCoordinate.getDecimal(), expectedCoordinate.getDecimal());

        // DmdCoordinate -> String

        actualStr = converter.convert(String.class, expectedCoordinate);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // String -> String

        actualStr = converter.convert(String.class, expectedString);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // DmdCoordinate -> DmdCoordinate

        actualCoordinate = converter.convert(DmdCoordinate.class, expectedCoordinate);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getMinute(), expectedCoordinate.getMinute());
        Assert.assertEquals(actualCoordinate.getDecimal(), expectedCoordinate.getDecimal());
    }
}
