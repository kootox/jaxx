package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DdCoordinateTest {

    @Test
    public void testFromDecimal() throws Exception {

        {
            DdCoordinate coordinate = DdCoordinate.empty();

            coordinate.fromDecimal(42.7f);

            assertDdCoordinate(coordinate, false, 42, 700);

            coordinate.addTrailingZero();

            assertDdCoordinate(coordinate, false, 42, 700);

            coordinate.removeTrailingZero();

            assertDdCoordinate(coordinate, false, 42, 700);
        }

        {
            DdCoordinate coordinate = DdCoordinate.empty();

            float decimalExcepted = 42.707f;
            coordinate.fromDecimal(decimalExcepted);

            assertDdCoordinate(coordinate, false, 42, 707);

            Float decimal = coordinate.toDecimal();
            Assert.assertEquals(decimalExcepted, decimal, 0.001);
        }
    }

    @Test
    public void testToDecimal() throws Exception {

        {
            DdCoordinate coordinate = DdCoordinate.empty();
            coordinate.setDegree(42);
            coordinate.setDecimal(42);
            Float actual = coordinate.toDecimal();
            Float expected = 42.042f;
            Assert.assertEquals(expected, actual, 0.001);
        }

        {
            DdCoordinate coordinate = DdCoordinate.empty();
            coordinate.setDegree(12);
            coordinate.setDecimal(2);
            Float actual = coordinate.toDecimal();
            Float expected = 12.002f;
            Assert.assertEquals(expected, actual, 0.0001);

            coordinate.fromDecimal(expected);

            assertDdCoordinate(coordinate, false, 12, 2);
        }

        {
            DdCoordinate component = DdCoordinate.empty();
            component.setDegree(12);
            component.setDecimal(20);
            Float actual = component.toDecimal();
            Float expected = 12.020f;
            Assert.assertEquals(expected, actual, 0.001);

            component.fromDecimal(expected);

            assertDdCoordinate(component, false, 12, 20);

        }
    }

    @Test
    public void testValueOf() throws Exception {

        DdCoordinate coordinate = DdCoordinate.valueOf(-0.007333f);
        Assert.assertNotNull(coordinate);
        Assert.assertTrue(coordinate.isDegreeNull());

        Assert.assertTrue(coordinate.isDegreeValid(true));
        Assert.assertTrue(coordinate.isDecimalValid());

        assertDdCoordinate(coordinate, true, null, 7);
    }

    public static void assertDdCoordinate(DdCoordinate coordinate,
                                          boolean expectedSign,
                                          Integer expectedDegree,
                                          Integer expectedDecimal) {
        Assert.assertNotNull(coordinate);
        Assert.assertEquals(expectedSign, coordinate.isSign());
        Assert.assertEquals(expectedDegree, coordinate.getDegree());
        Assert.assertEquals(expectedDecimal, coordinate.getDecimal());
    }
}
