package org.nuiton.jaxx.widgets.gis.absolute;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.gis.DdCoordinate;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.Serializable;

/**
 * Created on 3/20/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class AbsoluteDdCoordinateEditorTest {

    public static void main(String... args) {

        JPanel panel = new JPanel(new BorderLayout());

        JPanel panel2 = new JPanel(new GridLayout());
        panel.add(panel2, BorderLayout.CENTER);

        AbsoluteDdCoordinateEditor editor = new AbsoluteDdCoordinateEditor();

        JButton before = new JButton("Before");
        before.setMnemonic('B');
        JButton after = new JButton("After");
        after.setMnemonic('A');
        panel2.add(before);
        panel2.add(editor);
        panel2.add(after);

        JLabel label = new JLabel();
        panel.add(label, BorderLayout.SOUTH);

        final JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(600, 200);
        frame.add(panel);

        Bean bean = new Bean(label);
        bean.setDegree(42);
        bean.setDecimal(10);

        editor.setBean(bean);
        editor.setPropertyDegree("degree");
        editor.setPropertyDecimal("decimal");

        editor.init(true);
        editor.setValue(bean.getModel());

        SwingUtilities.invokeLater(() -> {
            SwingUtil.center(null, frame);
            frame.setVisible(true);
        });

    }

    static class Bean implements Serializable {

        private static final long serialVersionUID = 1L;

        final DdCoordinate model = new DdCoordinate();

        private final JLabel label;

        public Bean(JLabel label) {

            this.label = label;
        }

        public DdCoordinate getModel() {
            return model;
        }

        public void setDegree(Integer degree) {
            model.setDegree(degree);
            label.setText("Degree modified: " + model.toString());
        }

        public void setDecimal(Integer decimal) {
            model.setDecimal(decimal);
            label.setText("Decimal modified: " + model.toString());
        }


        public void setSign(boolean sign) {
            model.setSign(sign);
        }

        public boolean isSign() {
            return model.isSign();
        }

        public Integer getDegree() {
            return model.getDegree();
        }

        public Integer getDecimal() {
            return model.getDecimal();
        }

    }

}
