package org.nuiton.jaxx.widgets.gis;

/*
 * #%L
 * JAXX :: Widgets Gis
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.converter.ConverterUtil;

/**
 * Created on 9/2/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.12
 */
public class DdCoordinateConverterTest {


    @Test
    public void testConvert() throws Exception {

        ConverterUtil.initConverters();

        testConversion(false, "-  .4  ", DdCoordinate.valueOf(-0.4f));
        testConversion(false, "  .4  ", DdCoordinate.valueOf(0.4f));
        testConversion(false, "-  .04 ", DdCoordinate.valueOf(-0.04f));
        testConversion(false, "  .04 ", DdCoordinate.valueOf(0.04f));
        testConversion(false, "-  .004", DdCoordinate.valueOf(-0.004f));
        testConversion(false, "  .004", DdCoordinate.valueOf(0.004f));
        testConversion(false, "-  .044", DdCoordinate.valueOf(-0.044f));
        testConversion(false, "  .044", DdCoordinate.valueOf(0.044f));
        testConversion(false, "-  .444", DdCoordinate.valueOf(-0.444f));
        testConversion(false, "  .444", DdCoordinate.valueOf(0.444f));
        testConversion(false, "  .099", DdCoordinate.valueOf(false, null, 99));
        testConversion(false, "  .   ", DdCoordinate.empty());
        testConversion(false, "  .099", DdCoordinate.valueOf(false, null, 99));
        testConversion(false, "39.099", DdCoordinate.valueOf(false, 39, 99));
        testConversion(false, " 0.   ", DdCoordinate.valueOf(false, 0, null));


        testConversion(true, "-   .4  ", DdCoordinate.valueOf(-0.4f));
        testConversion(true, "   .4  ", DdCoordinate.valueOf(0.4f));
        testConversion(true, "-   .04 ", DdCoordinate.valueOf(-0.04f));
        testConversion(true, "   .04 ", DdCoordinate.valueOf(0.04f));
        testConversion(true, "-   .004", DdCoordinate.valueOf(-0.004f));
        testConversion(true, "   .004", DdCoordinate.valueOf(0.004f));
        testConversion(true, "-   .044", DdCoordinate.valueOf(-0.044f));
        testConversion(true, "   .044", DdCoordinate.valueOf(0.044f));
        testConversion(true, "-   .444", DdCoordinate.valueOf(-0.444f));
        testConversion(true, "   .444", DdCoordinate.valueOf(0.444f));
        testConversion(true, "   .099", DdCoordinate.valueOf(false, null, 99));
        testConversion(true, "   .   ", DdCoordinate.empty());
        testConversion(true, "   .099", DdCoordinate.valueOf(false, null, 99));
        testConversion(true, " 39.099", DdCoordinate.valueOf(false, 39, 99));
        testConversion(true, "  0.   ", DdCoordinate.valueOf(false, 0, null));

    }

    protected void testConversion(boolean forLongitude, String expectedString, DdCoordinate expectedCoordinate) {

        DdCoordinateConverter converter = (DdCoordinateConverter) ConverterUtil.getConverter(DdCoordinate.class);
        Assert.assertNotNull(converter);
        converter.setForLongitude(forLongitude);

        String actualStr;
        DdCoordinate actualCoordinate;

        // String -> DdCoordinate

        actualCoordinate = converter.convert(DdCoordinate.class, expectedString);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getDecimal(), expectedCoordinate.getDecimal());

        // DdCoordinate -> String

        actualStr = converter.convert(String.class, expectedCoordinate);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // String -> String

        actualStr = converter.convert(String.class, expectedString);

        Assert.assertNotNull(actualStr);
        Assert.assertEquals(expectedString, actualStr);

        // DdCoordinate -> DdCoordinate

        actualCoordinate = converter.convert(DdCoordinate.class, expectedCoordinate);
        Assert.assertNotNull(actualCoordinate);
        Assert.assertEquals(actualCoordinate.isSign(), expectedCoordinate.isSign());
        Assert.assertEquals(actualCoordinate.getDegree(), expectedCoordinate.getDegree());
        Assert.assertEquals(actualCoordinate.getDecimal(), expectedCoordinate.getDecimal());
    }
}
