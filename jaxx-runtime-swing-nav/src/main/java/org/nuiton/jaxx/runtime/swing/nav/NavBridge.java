/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.EventListener;

/**
 * Contract of the bridge used by {@link NavHelper} to hold model and ui.
 *
 * @param <M> Type of model to bridge
 * @param <N> Type of nodes of the model
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public interface NavBridge<M, N extends NavNode<M, N>> {

    //--------------------------------------------------------------------------
    //-- Model Queries
    //--------------------------------------------------------------------------

    M getModel();

    N getRoot();

    boolean isLeaf(Object node);

    int getChildCount(Object parent);

    N getChild(Object parent, int index);

    int getIndexOfChild(Object parent, Object child);

    TreeNode[] getPathToRoot(TreeNode aNode);

    //--------------------------------------------------------------------------
    //-- Model modification
    //--------------------------------------------------------------------------

    void setModel(M delegate);

    void setRoot(N node);

    void insertNodeInto(N newChild, N parent, int index);

    void removeNodeFromParent(N node);

    void reload(N node);

    boolean canLoadChild(N node);

    //--------------------------------------------------------------------------
    //-- Listeners notifications 
    //--------------------------------------------------------------------------

    void valueForPathChanged(TreePath path, Object newValue);

    void nodeStructureChanged(TreeNode node);

    void nodeChanged(TreeNode node);

    void nodesChanged(TreeNode node, int[] childIndices);

    void nodesWereInserted(N parent, int[] indices);

    void nodeWereInserted(N parentNode, int childIndice, N node);

    void nodesWereRemoved(TreeNode node, int[] childIndices, Object[] removedChildren);

    /**
     * Notifies that the {@code node} was inserted.
     *
     * <b>Note:</b> The method recurses on childs (always notify parent before child)
     *
     * @param node node inserted
     */
    void notifyNodeInserted(N node);

    /**
     * Notifies that all childs nodes of {@code node} were
     * inserted.
     *
     * <b>Note:</b> The method recurses on childs (always notify parent before child)
     *
     * @param node node where all childs where inserted
     */
    void notifyChildNodesInserted(N node);

    //--------------------------------------------------------------------------
    //-- TreeModelListener provider
    //--------------------------------------------------------------------------

    void addTreeModelListener(TreeModelListener l);

    void removeTreeModelListener(TreeModelListener l);

    TreeModelListener[] getTreeModelListeners();

    <T extends EventListener> T[] getListeners(Class<T> listenerType);

}
