/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.nav.NavDataProvider;
import org.nuiton.jaxx.runtime.swing.nav.NavNode;

import javax.swing.tree.DefaultTreeCellRenderer;
import java.util.HashMap;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Le renderer abstrait (qui a toutes les methodes qui aident) pour implanter de
 * vrai renderer pour les différents cas d'utilisation de l'abre de navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public abstract class AbstractNavTreeCellRenderer<M, N extends NavNode<M, N>> extends DefaultTreeCellRenderer {

    /** Logger */
    protected static final Log log =
            LogFactory.getLog(AbstractNavTreeCellRenderer.class);
    private static final long serialVersionUID = -2339479996023197627L;

    /** source de donnée */
    protected NavDataProvider dataProvider;

    /** le cache de rendu */
    protected final Map<N, String> renderCache = new HashMap<>();

    /**
     * Determines the text render of a node using the {@link #dataProvider}.
     *
     * @param node the node to render
     * @return the text render of the node
     */
    protected abstract String computeNodeText(N node);

    protected AbstractNavTreeCellRenderer() {
    }

    public NavDataProvider getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(NavDataProvider dataProvider) {
        this.dataProvider = dataProvider;

        // une nouvelle source utilisée, on vide le cache
        clearCache();
    }

    public void clearCache() {
        renderCache.clear();
    }

    public void invalidateCache(N node) {
        renderCache.remove(node);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        clearCache();
    }

    public String getNodeText(N node) {
        if (node == null) {
            return null;
        }
        String text;

        if (node.isDirty() || !renderCache.containsKey(node)) {

            // calculer le rendu du noeud
            if (node.isStringNode()) {
                text = t(node.getId());

            } else {

                text = computeNodeText(node);
            }

            if (log.isDebugEnabled()) {
                log.debug("text for node [" + node + "] = <" + text + ">");
            }

            // sauvegarde dans le cache
            renderCache.put(node, text);

            // le noeud est de nouveau propre
            node.setDirty(false);

        } else {

            // recupération directement du rendu dans le cache
            text = renderCache.get(node);
        }

        return text;
    }
}
