/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.nav.NavBridge;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.Enumeration;
import java.util.EventListener;

/**
 * Delegate model used to switch between tree model and tree table model api
 *
 * @author Sylvain Lletellier
 * @since 2.1
 */
public class NavTreeBridge<N extends NavTreeNode<N>> implements NavBridge<DefaultTreeModel, N> {

    /** Logger */
    static private final Log log = LogFactory.getLog(NavTreeBridge.class);

    /** bridge model */
    protected DefaultTreeModel model;

    /** bridge ui */
//    protected JTree ui;
//    public NavTreeBridge() {
//    }

    //--------------------------------------------------------------------------
    //-- Model Queries
    //--------------------------------------------------------------------------
    @Override
    public DefaultTreeModel getModel() {
        return model;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public N getRoot() {
        return (N) model.getRoot();
    }

    @Override
    public boolean isLeaf(Object node) {
        return model.isLeaf(node);
    }

    @Override
    public int getChildCount(Object parent) {
        return model.getChildCount(parent);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public N getChild(Object parent, int index) {
        return (N) model.getChild(parent, index);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return model.getIndexOfChild(parent, child);
    }

    @Override
    public TreeNode[] getPathToRoot(TreeNode aNode) {
        return model.getPathToRoot(aNode);
    }

    //--------------------------------------------------------------------------
    //-- Model modification
    //--------------------------------------------------------------------------

    @Override
    public void setModel(DefaultTreeModel model) {
        this.model = model;
    }

    @Override
    public void setRoot(N node) {
        model.setRoot(node);
    }

    @Override
    public void insertNodeInto(N newChild, N parent, int index) {
        model.insertNodeInto(newChild, parent, index);
    }

    @Override
    public void removeNodeFromParent(N node) {
        model.removeNodeFromParent(node);
    }

    @Override
    public void reload(N node) {
        model.reload();
    }

    @Override
    public boolean canLoadChild(N node) {
        return true;
    }

    //--------------------------------------------------------------------------
    //-- Model Listeners notifications
    //--------------------------------------------------------------------------

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        model.valueForPathChanged(path, newValue);
    }

    @Override
    public void nodesWereInserted(N parent, int[] indices) {
        model.nodesWereInserted(parent, indices);
    }

    @Override
    public void nodeWereInserted(N parentNode, int childIndice, N node) {
        int[] indices = new int[1];
        indices[0] = childIndice;
        model.nodesWereInserted(parentNode, indices);
    }

    @Override
    public void nodeChanged(TreeNode node) {
        model.nodeChanged(node);
    }

    @Override
    public void nodeStructureChanged(TreeNode node) {
        model.nodeStructureChanged(node);
    }

    @Override
    public void nodesWereRemoved(TreeNode node, int[] childIndices,
                                 Object[] removedChildren) {
        model.nodesWereRemoved(node, childIndices, removedChildren);
    }

    @Override
    public void nodesChanged(TreeNode node, int[] childIndices) {
        model.nodesChanged(node, childIndices);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void notifyChildNodesInserted(N node) {
        int count = node.getChildCount();
        if (count < 1) {
            if (log.isDebugEnabled()) {
                log.debug("Skip for leaf node : " + node);
            }
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("Notify for node : " + node + ", " + count + " child(s) inserted.");
        }
        int[] indices = new int[count];
        for (int i = 0; i < count; i++) {
            indices[i] = i;
        }

        nodesWereInserted(node, indices);

        // recurse notify on childs
        for (Enumeration<? extends NavTreeNode<?>> childs = node.children();
             childs.hasMoreElements(); ) {
            N child = (N) childs.nextElement();
            notifyChildNodesInserted(child);
        }
    }

    @Override
    public void notifyNodeInserted(N node) {

        N parent = node.getParent();
        if (parent != null) {
            int indice = parent.getIndex(node);
            if (log.isDebugEnabled()) {
                log.debug("Notify for node : " + node + ", for parent [" + parent + "] child  " + indice + " inserted.");
            }
            nodesWereInserted(parent, new int[]{indice});
            notifyChildNodesInserted(node);
        }
    }

    //--------------------------------------------------------------------------
    //-- TreeModelListener provider
    //--------------------------------------------------------------------------

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        model.addTreeModelListener(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        model.removeTreeModelListener(l);
    }

    @Override
    public TreeModelListener[] getTreeModelListeners() {
        return model.getTreeModelListeners();
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public <T extends EventListener> T[] getListeners(Class<T> listenerType) {
        return model.getListeners(listenerType);
    }

}
