/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav.tree;

import org.nuiton.jaxx.runtime.swing.nav.NavHelper;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of {@link NavHelper} base on a {@link JTree} component.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class NavTreeHelper<N extends NavTreeNode<N>> extends NavHelper<DefaultTreeModel, JTree, NavTreeBridge<N>, N> {

    public NavTreeHelper() {
        this(new NavTreeBridge<>());
    }

    public NavTreeHelper(NavTreeBridge<N> bridge) {
        super(bridge);
    }

    @Override
    public void scrollPathToVisible(TreePath path) {
        getUI().scrollPathToVisible(path);
    }

    @Override
    public void setSelectionPath(TreePath path) {
        getUI().setSelectionPath(path);
    }

    @Override
    public void addSelectionPaths(TreePath[] paths) {
        getUI().addSelectionPaths(paths);
    }

    @Override
    public void addSelectionPath(TreePath path) {
        getUI().addSelectionPath(path);
    }

    @Override
    public void removeSelectionPaths(TreePath[] path) {
        getUI().removeSelectionPaths(path);
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        getUI().removeSelectionPath(path);
    }

    @Override
    public TreeSelectionModel getSelectionModel() {
        return getUI().getSelectionModel();
    }

    @Override
    public boolean isExpanded(TreePath pathToExpand) {
        return getUI().isExpanded(pathToExpand);
    }

    @Override
    public void expandPath(TreePath pathToExpand) {
        getUI().expandPath(pathToExpand);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public AbstractNavTreeCellRenderer<DefaultTreeModel, N> getTreeCellRenderer() {
        JTree t = getUI();
        if (t == null) {
            return null;
        }
        TreeCellRenderer r = t.getCellRenderer();
        if (r instanceof AbstractNavTreeCellRenderer) {
            return (AbstractNavTreeCellRenderer<DefaultTreeModel, N>) r;
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public N getSelectedNode() {
        JTree tree = getUI();
        if (tree == null) {
            return null;
        }
        TreePath path = tree.getSelectionPath();
        N node = null;
        if (path != null) {
            node = (N) path.getLastPathComponent();
        }
        return node;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public List<N> getSelectedNodes() {
        JTree tree = getUI();
        if (tree == null) {
            return null;
        }
        TreePath[] paths = tree.getSelectionPaths();
        List<N> nodes = new ArrayList<>();
        if (paths != null) {
            for (TreePath path : paths) {
                if (path != null) {
                    nodes.add((N) path.getLastPathComponent());
                }
            }
        }
        return nodes;
    }

    @Override
    public void setUI(JTree tree,
                      boolean addExpandTreeListener,
                      boolean addOneClickSelectionListener,
                      TreeSelectionListener listener,
                      TreeWillExpandListener willExpandListener) {
        setUI(tree);
        if (willExpandListener != null) {
            tree.addTreeWillExpandListener(willExpandListener);
        }
        if (addExpandTreeListener) {
            tree.addTreeWillExpandListener(expandListener);
        }
        if (listener != null) {
            tree.addTreeSelectionListener(listener);
        }
        if (addOneClickSelectionListener) {
            tree.addTreeSelectionListener(selectionListener);
        }
    }

    @Override
    protected DefaultTreeModel createModel(N node, Object... extraArgs) {
        NavTreeBridge<N> bridge = getBridge();
        DefaultTreeModel model = bridge.getModel();
        if (model == null) {
            model = new DefaultTreeModel(node);
            bridge.setModel(model);
            bridge.addTreeModelListener(treeModelListener);
        } else {
            bridge.setRoot(node);
        }

        // notify structure has changed
        bridge.nodeStructureChanged(getRootNode());
        return model;
    }
}
