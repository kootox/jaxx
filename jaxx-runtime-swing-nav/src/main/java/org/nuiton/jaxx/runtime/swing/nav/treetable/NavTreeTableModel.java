/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav.treetable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.tree.TreeModelSupport;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * Model of the tree table used for a jaxx tree table api.
 *
 * @author Sylvain Lletellier
 * @since 2.2
 */
public class NavTreeTableModel implements TreeTableModel {

    /** Logger */
    static private final Log log = LogFactory.getLog(NavTreeTableModel.class);

    /**
     * Hack to acces to the modelSupport
     *
     * @author sletellier
     * @since 2.2
     */
    public static abstract class MyDefaultTreeTableModel extends DefaultTreeTableModel {

        public TreeModelSupport getModelSupport() {
            return modelSupport;
        }

        public abstract String[] getColumnsNames();

    }

    /** the delegate model */
    protected final MyDefaultTreeTableModel delegate;

    public NavTreeTableModel(MyDefaultTreeTableModel delegate) {
        this.delegate = delegate;
    }

    @SuppressWarnings({"SuspiciousSystemArraycopy"})
    public TreeTableNode[] getPathToRoot(TreeTableNode aNode) {
        if (aNode == null) {
            return null;
        }
        TreeNode[] treeNodes = getDelegate().getPathToRoot(aNode);
        NavTreeTableNode<?>[] result = new NavTreeTableNode[treeNodes.length];
        System.arraycopy(treeNodes, 0, result, 0, treeNodes.length);
        return result;
    }

    public void nodeStructureChanged(NavTreeTableNode<?> node) {
        if (node != null) {
            NavTreeTableNode<?> parentNode = node.getParent();
            if (parentNode == null || parentNode.isRoot()) {
                getModelSupport().fireNewRoot();
            } else {
                TreeNode[] treeNodes = getPathToRoot(parentNode);
                if (treeNodes != null) {
                    getModelSupport().fireTreeStructureChanged(new TreePath(treeNodes));
                }
                // FIXME : it's append....
//              else {
//                  log.error("[Node structure changed] Path to root is null !");
//              }
            }
        } else {
            log.error("Node is null !");
        }
    }

    public void nodeChanged(NavTreeTableNode<?> node) {
        if (node != null) {
            NavTreeTableNode<?> parent = node.getParent();
            TreeNode[] treeNodes = getPathToRoot(parent);
            if (treeNodes != null) {
                getModelSupport().fireChildChanged(
                        new TreePath(treeNodes), parent.getIndex(node), node);
                // FIXME : it's append....
//              else {
//                  log.error("[Node changed] Path to root is null !");
//              }
            }
        } else {
            log.error("Node is null !");
        }
    }

    public MyDefaultTreeTableModel getDelegate() {
        return delegate;
    }

    public TreeModelSupport getModelSupport() {
        return delegate.getModelSupport();
    }

    public String[] getColomnsNames() {
        return delegate.getColumnsNames();
    }

    public void setRoot(TreeTableNode root) {
        delegate.setRoot(root);
    }

    //--------------------------------------------------------------------------
    //-- Overrides delegate methode
    //--------------------------------------------------------------------------

    @Override
    public TreeTableNode getRoot() {
        return delegate.getRoot();
    }

    @Override
    public Object getChild(Object parent, int index) {
        return delegate.getChild(parent, index);
    }

    @Override
    public int getChildCount(Object parent) {
        return delegate.getChildCount(parent);
    }

    @Override
    public boolean isLeaf(Object node) {
        return delegate.isLeaf(node);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        delegate.valueForPathChanged(path, newValue);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return delegate.getIndexOfChild(parent, child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        delegate.addTreeModelListener(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        delegate.removeTreeModelListener(l);
    }

    @Override
    public Class<?> getColumnClass(int i) {
        return getDelegate().getColumnClass(i);
    }

    @Override
    public int getColumnCount() {
        return getColomnsNames().length;
    }

    @Override
    public String getColumnName(int column) {
        return getColomnsNames()[column];
    }

    @Override
    public int getHierarchicalColumn() {
        return getDelegate().getHierarchicalColumn();
    }

    @Override
    public Object getValueAt(Object o, int i) {
        return getDelegate().getValueAt(o, i);
    }

    @Override
    public boolean isCellEditable(Object o, int i) {
        return getDelegate().isCellEditable(o, i);
    }

    @Override
    public void setValueAt(Object o, Object o1, int i) {
        getDelegate().setValueAt(o, o1, i);
    }

}
