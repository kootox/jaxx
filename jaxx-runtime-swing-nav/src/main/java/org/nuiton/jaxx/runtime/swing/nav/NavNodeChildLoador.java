/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.List;

/**
 * Object to load childs of a node.
 *
 * It uses {@link NavDataProvider} in method
 * {@link #loadChilds(NavBridge, NavNode, NavDataProvider)} to obtain datas
 * then build childs nodes.
 *
 * A factory of such objects can be found in {@link NavHelper} to make
 * them reusable in other places than inside a {@link NavNode} to auto-load
 * childs.
 *
 * For example when you want to creat by hand a new node, always prefer to reuse
 * a such object rathen than duplicate same code in helper...
 *
 * @param <T> type of data used to create nodes (can be just a String type to use only ids)
 * @param <O> type of data associated with nodes
 * @param <N> type of node to used (to make possible full co-variance and no cast in fal implementations).
 * @author Tony Chemit - dev@tchemit.fr
 * @see NavHelper
 * @see NavNode
 * @since 2.1
 */
public abstract class NavNodeChildLoador<T, O, M, B extends NavBridge<M, N>, N extends NavNode<M, N>> implements Serializable {

    /** Logger. */
    static private final Log log = LogFactory.getLog(NavNodeChildLoador.class);

    private static final long serialVersionUID = 1L;

    /** Type of data of the node */
    protected final Class<O> beanType;

    protected NavNodeChildLoador(Class<O> beanType) {
        this.beanType = beanType;
    }

    /**
     * Obtain the list of data used to create nodes.
     *
     * If type {@code T} is {@code O}, we directly use the data associated with nodes.
     *
     * @param parentClass  type of parent
     * @param parentId     id of parent
     * @param dataProvider the data provider
     * @return the list of data
     * @throws Exception if any problem
     */
    public abstract List<T> getData(Class<?> parentClass,
                                    String parentId,
                                    NavDataProvider dataProvider);

    /**
     * Hook to create a child node given his {@code data}.
     *
     * @param data         the data of the node to create
     * @param dataProvider the data provider
     * @return the created node
     */
    public abstract N createNode(T data, NavDataProvider dataProvider);

    /**
     * Returns the type of data associated with nodes to create.
     *
     * @return the type of data associated with created nodes.
     */
    public Class<O> getBeanType() {
        return beanType;
    }

    /**
     * Load childs of the given {@code parentnode}.
     *
     * @param bridge       the model owner of nodes
     * @param parentNode   the parent node where to insert nodes
     * @param dataProvider data provider
     * @throws Exception pour tout probleme de recuperation de donnees
     */
    public void loadChilds(B bridge,
                           N parentNode,
                           NavDataProvider dataProvider) {

        N containerNode = parentNode.getContainerNode();

        List<T> datas;
        if (containerNode == null) {

            // pas d'ancetre, il doit s'agir d'un premier noeud de données
            // depuis le noeud root

            // recuperation des objets fils (sans connaitre de parent)
            datas = getData(null, null, dataProvider);

        } else {
            if (log.isDebugEnabled()) {
                log.debug("search data for " + containerNode.getInternalClass() +
                                  " : " + containerNode.getId());
            }

            // recuperation des objets fils
            datas = getData(containerNode.getInternalClass(),
                            containerNode.getId(),
                            dataProvider);
        }

        // on charge les fils
        addChildNodes(parentNode, datas, dataProvider);

        // notifie le modele d'un ajout de noeuds
        bridge.notifyChildNodesInserted(parentNode);
    }

    /**
     * Add childs to given {@code parentNode} using retrive {@code datas} from
     * the data provider.
     *
     * This method is invoked by the {@link #loadChilds(NavBridge, NavNode, NavDataProvider)}.
     *
     * @param parentNode   the node where to insert
     * @param datas        the data used to create node
     * @param dataProvider the data provider
     */
    protected void addChildNodes(N parentNode,
                                 List<T> datas,
                                 NavDataProvider dataProvider) {

        // creation des noeuds fils
        if (datas != null) {
            for (T o : datas) {
                if (log.isDebugEnabled()) {
                    log.debug("[" + parentNode + "] Will add child node for " + o);
                }
                N node = createNode(o, dataProvider);
                parentNode.add(node);
            }
        }
    }

}
