/*
 * #%L
 * JAXX :: Runtime Swing Nav
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime.swing.nav.treetable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTreeTable;
import org.nuiton.jaxx.runtime.swing.nav.NavHelper;
import org.nuiton.jaxx.runtime.swing.nav.tree.AbstractNavTreeCellRenderer;

import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of {@link NavHelper} based on a {@link JXTreeTable} component.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.1
 */
public class NavTreeTableHelper<N extends NavTreeTableNode<N>> extends NavHelper<NavTreeTableModel, JXTreeTable, NavTreeTableBridge<N>, N> {

    /** Logger */
    static private final Log log = LogFactory.getLog(NavTreeTableHelper.class);

    public NavTreeTableHelper() {
        super(new NavTreeTableBridge<>());
    }

    @Override
    public void scrollPathToVisible(TreePath path) {
        getUI().scrollPathToVisible(path);
    }

    @Override
    public void setSelectionPath(TreePath path) {
        getUI().getTreeSelectionModel().setSelectionPath(path);
    }

    @Override
    public void addSelectionPath(TreePath path) {
        getUI().getTreeSelectionModel().addSelectionPath(path);
    }

    @Override
    public void addSelectionPaths(TreePath[] paths) {
        getUI().getTreeSelectionModel().addSelectionPaths(paths);
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        getUI().getTreeSelectionModel().removeSelectionPath(path);
    }

    @Override
    public void removeSelectionPaths(TreePath[] paths) {
        getUI().getTreeSelectionModel().removeSelectionPaths(paths);
    }

    @Override
    public TreeSelectionModel getSelectionModel() {
        return getUI().getTreeSelectionModel();
    }

    @Override
    public boolean isExpanded(TreePath pathToExpand) {
        return getUI().isExpanded(pathToExpand);
    }

    @Override
    public void expandPath(TreePath pathToExpand) {
        getUI().expandPath(pathToExpand);
    }

    @Override
    public AbstractNavTreeCellRenderer<NavTreeTableModel, N> getTreeCellRenderer() {
        //FIXME Implements it if possible
        return null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public N getSelectedNode() {
        TreePath path = getSelectionModel().getSelectionPath();
        N node = null;
        if (path != null) {
            node = (N) path.getLastPathComponent();
        }
        return node;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public List<N> getSelectedNodes() {
        JXTreeTable tree = getUI();
        if (tree == null) {
            return null;
        }
        TreePath[] paths = tree.getTreeSelectionModel().getSelectionPaths();
        List<N> nodes = new ArrayList<>();
        if (paths != null) {
            for (TreePath path : paths) {
                if (path != null) {
                    nodes.add((N) path.getLastPathComponent());
                }
            }
        }
        return nodes;
    }

    @Override
    public void setUI(JXTreeTable tree,
                      boolean addExpandTreeListener,
                      boolean addOneClickSelectionListener,
                      TreeSelectionListener listener,
                      TreeWillExpandListener willExpandListener) {
        setUI(tree);
        if (willExpandListener != null) {
            tree.addTreeWillExpandListener(willExpandListener);
        }
        if (addExpandTreeListener) {
            tree.addTreeWillExpandListener(expandListener);
        }
        if (listener != null) {
            tree.addTreeSelectionListener(listener);
        }
        if (addOneClickSelectionListener) {
            tree.addTreeSelectionListener(selectionListener);
        }
    }

    @Override
    protected NavTreeTableModel createModel(N node, Object... extraArgs) {

        // must have a single extra params with delegate model
        if (extraArgs.length != 1) {
            throw new IllegalArgumentException("Should have exactly one extra parameter (delegate model)");
        }
        if (!(extraArgs[0] instanceof NavTreeTableModel.MyDefaultTreeTableModel)) {
            throw new IllegalArgumentException("extra parameter is not instance of " + NavTreeTableModel.MyDefaultTreeTableModel.class.getName());
        }
        NavTreeTableModel.MyDefaultTreeTableModel delegate = (NavTreeTableModel.MyDefaultTreeTableModel) extraArgs[0];
        NavTreeTableBridge<N> bridge = getBridge();
        NavTreeTableModel model = bridge.getModel();
        if (model == null) {
            model = new NavTreeTableModel(delegate);
            bridge.setModel(model);
//            model = new NavTreeTableBridge(tableModel);
            bridge.addTreeModelListener(treeModelListener);
//            ((NavTreeTableModel) model).addTreeModelListener(treeModelListener);
        }
        bridge.setRoot(node);

        // notify structure has changed
        bridge.nodeStructureChanged(getRootNode());
        return model;
    }
}
