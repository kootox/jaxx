/*
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

/**
 * The {@link JAXXContext} contract defines a generic context.
 *
 * A context contains two king of entries :
 *
 * <h2>Unamed entry</h2>
 * a such entry maps filter only on the clas of the object of the entry.
 *
 * To add a <b>unamed</b> entry, use {@link #setContextValue(Object)} and {@link #getContextValue(Class)} to reteave a
 * such entry.
 *
 * <h2>named entry</h2>
 * a such entry filter on class of the object and on the name of the entry.
 *
 * To add a <b>named</b> entry, use {@link #setContextValue(Object, String)} and {@link #getContextValue(Class, String)}
 * to reteave a such entry.
 *
 * @author letellier
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JAXXContext {

    /**
     * Push in the context a new unamed entry.
     *
     * If a previous entry exists in context (unamed  and same class), it will be removed.
     *
     * @param <T> type of data to set in context
     * @param o   the value to push in context
     */
    <T> void setContextValue(T o);

    /**
     * * Push in the context a new amed entry.
     *
     * If a previous entry exists in context (same name and class), it will be removed.
     *
     * @param <T>  type of data to set in context
     * @param o    the value to push in context
     * @param name the name of the new entry
     */
    <T> void setContextValue(T o, String name);

    /**
     * Remove from context the value with the given klazz as an unamed entry
     *
     * @param <T>   type of data to remove from context
     * @param klazz the klazz entry
     */
    <T> void removeContextValue(Class<T> klazz);

    /**
     * Remove from context the value with the given klazz as an unamed (if name is null) or named entry
     *
     * @param <T>   type of data to remove from context
     * @param klazz the klazz entry
     * @param name  extra name of the entry
     */
    <T> void removeContextValue(Class<T> klazz, String name);

    /**
     * Seek for a unamed entry in the context
     *
     * This is an exemple to call a method in JAXX :
     *
     * <code>&lt;JButton onActionPerformed='{getContextValue(Action.class).method(args[])}'/&gt;</code>
     *
     * @param <T>   type of data to obtain from context
     * @param clazz the class of unamed entry to seek in context
     * @return the value of the unamed entry for the given class, or <code>null</code> if no such entry.
     */
    <T> T getContextValue(Class<T> clazz);

    /**
     * Seek for a named entry in the context
     *
     * @param <T>   type of data to obtain from context
     * @param clazz the class of named entry to seek in context
     * @param name  the name of the entry to seek in context
     * @return the value of the named entry for the given class, or <code>null</code> if no such entry.
     */
    <T> T getContextValue(Class<T> clazz, String name);

//    /**
//     * Return parent's container corresponding to the Class clazz
//     *
//     * @param <O> type of container to obtain from context
//     * @param clazz clazz desired
//     * @return parent's container
//     * @deprecated since 2.0.0 : breaks neutral since Swing
//     */
//    @Deprecated
//    public <O extends Container> O getParentContainer(Class<O> clazz);
//
//    /**
//     * Return parent's container corresponding to the Class clazz
//     *
//     * @param <O> type of container to obtain from context
//     * @param top   the top container
//     * @param clazz desired
//     * @return parent's container
//     * @deprecated since 2.0.0 : breaks neutral since Swing
//     */
//    @Deprecated
//    public <O extends Container> O getParentContainer(Object top, Class<O> clazz);
}
