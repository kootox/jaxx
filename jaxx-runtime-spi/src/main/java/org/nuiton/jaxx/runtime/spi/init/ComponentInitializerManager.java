package org.nuiton.jaxx.runtime.spi.init;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.JAXXObject;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created by tchemit on 14/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComponentInitializerManager {

    public static final ComponentInitializerManager INSTANCE = new ComponentInitializerManager();
    private final Set<ComponentInitializer> initializerSet;

    private ComponentInitializerManager() {
        initializerSet = new HashSet<>();
        for (ComponentInitializer componentInitializer : ServiceLoader.load(ComponentInitializer.class)) {
            initializerSet.add(componentInitializer);
        }
    }

    public static ComponentInitializerManager get() {
        return INSTANCE;
    }

    public Set<ComponentInitializer> apply(JAXXObject ui) {
        Set<ComponentInitializer> done = new LinkedHashSet<>();
        for (ComponentInitializer initializer : initializerSet) {
            initializer.reset();
            if (initializer.acceptUi(ui)) {
                initializer.start(ui);
                for (Map.Entry<String, Object> entry : ui.get$objectMap().entrySet()) {
                    Object component = entry.getValue();
                    if (component == null) {
                        continue;
                    }
                    if (initializer.acceptComponent(component)) {
                        initializer.init(ui, component);
                    }
                }
            }
            if (initializer.isTouched()) {
                done.add(initializer);
            }
        }
        for (ComponentInitializer componentInitializer : done) {
            componentInitializer.end(ui);
        }
        return done;
    }

}
