/*
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.runtime;

import java.beans.PropertyChangeListener;

/**
 * Created: 5 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version $Revision$
 *
 *          Mise a jour: $Date$ par :
 *          $Author$
 */
public interface JAXXBinding extends PropertyChangeListener {
    /** @return the unique id of a binding */
    String getId();

    /** @return the {@link JAXXObject} which owns the binding */
    JAXXObject getSource();

    /**
     * This state is not used actually, but will be usefull later...
     *
     * @return {@code true} if binding was registred as a default binding, {@code false} otherwise
     */
    boolean isDefaultBinding();

    /** Apply the binding without processing it (say just install listeners). */
    void applyDataBinding();

    /** Processes the binding. */
    void processDataBinding();

    /** Remove the binding. */
    void removeDataBinding();
}
