package org.nuiton.jaxx.runtime.resources;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created by tchemit on 13/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UIResourcesProviders {

    public static UIResourcesProviders INSTANCE;
    private final Set<UIResourcesProvider> providers;

    private UIResourcesProviders() {
        providers = new HashSet<>();
        for (UIResourcesProvider componentInitializer : ServiceLoader.load(UIResourcesProvider.class)) {
            providers.add(componentInitializer);
        }
    }

    public static void load() {
        get().load0();
    }

    protected static UIResourcesProviders get() {
        if (INSTANCE == null) {
            INSTANCE = new UIResourcesProviders();
        }
        return INSTANCE;
    }

    private void load0() {
        for (UIResourcesProvider provider : providers) {
            try {
                provider.load();
            } catch (IOException e) {
                throw new IllegalStateException("Can't load ui resources", e);
            }
        }
    }


}
