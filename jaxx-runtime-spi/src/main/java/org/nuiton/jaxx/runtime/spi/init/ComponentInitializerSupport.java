package org.nuiton.jaxx.runtime.spi.init;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.TreeMap;
import javax.swing.JComponent;
import org.nuiton.jaxx.runtime.JAXXObject;

/**
 * Created by tchemit on 13/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ComponentInitializerSupport<C> implements ComponentInitializer<C> {

    private final Map<String, C> editorMap = new TreeMap<>();
    private final Class<C> componentType;
    private JAXXObject ui;

    protected ComponentInitializerSupport(Class<C> componentType) {
        this.componentType = componentType;
    }

    @Override
    public boolean acceptComponent(Object component) {
        return componentType.isAssignableFrom(component.getClass());
    }

    @Override
    public boolean acceptUi(JAXXObject ui) {
        return true;
    }

    public void register(String name, C component) {
        editorMap.put(name, component);
    }

    @SuppressWarnings("unchecked")
    public void register(JComponent component) {
        editorMap.put(component.getName(), (C) component);
    }

    @Override
    public void start(JAXXObject ui) {
        this.ui = ui;
        editorMap.clear();
    }

    @Override
    public void reset() {
        editorMap.clear();
    }

    @Override
    public void end(JAXXObject ui) {
    }

    public boolean isTouched() {
        return ui != null && !editorMap.isEmpty();
    }

    public Map<String, C> getEditorMap() {
        return editorMap;
    }
}
