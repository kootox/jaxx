package org.nuiton.jaxx.runtime.resources;

/*-
 * #%L
 * JAXX :: Runtime Spi
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by tchemit on 13/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UIResourcesProvider {

    private static final Log log = LogFactory.getLog(UIResourcesProvider.class);
    private static final String ICON_PREFIX = "icon.";
    private static final String COLOR_PREFIX = "color.";

    private final String name;
    private final String resourcePath;
    private int nbIcons;
    private int nbColors;

    public UIResourcesProvider(String name, String resourcePath) {
        this.name = name;
        this.resourcePath = resourcePath;
    }

    public void load() throws IOException {
        log.info(String.format("%s Loading ui resources", name));
        try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(resourcePath)) {
            Objects.requireNonNull(resourceAsStream,"Could not find ui resources file: "+resourcePath);
            Properties p = new Properties();
            p.load(resourceAsStream);
            load(p);
        }
        log.info(String.format("%s Loaded %d icon(s).", name, nbIcons));
        log.info(String.format("%s Loaded %d color(s).", name, nbColors));
    }

    public int getNbIcons() {
        return nbIcons;
    }

    public int getNbColors() {
        return nbColors;
    }

    public void load(Properties incoming) throws IOException {
        nbIcons = nbColors = 0;
        for (Map.Entry<Object, Object> entry : incoming.entrySet()) {
            String key = (String) entry.getKey();
            Object value = entry.getValue();

            if (key.startsWith(ICON_PREFIX)) {

                String iconKey = key.substring(ICON_PREFIX.length());
                String iconPath = "/icons/" + value;
                log.info(String.format("%s Loading icon %s → %s", name, iconKey, iconPath));
                URL imgURL = getClass().getResource(iconPath);
                if (imgURL == null) {
                    throw new IOException(String.format("%s could not find icon %s", name, iconPath));
                }
                ImageIcon icon = new ImageIcon(imgURL);
                UIManager.put(iconKey, icon);
                nbIcons++;
                continue;
            }
            if (key.startsWith(COLOR_PREFIX)) {
                String colorKey = key.substring(COLOR_PREFIX.length());
                String[] rgb = ((String) value).split(",");
                log.info(String.format("%s Loading color %s → %s", name, colorKey, Arrays.toString(rgb)));
                UIManager.put(
                        colorKey,
                        new Color(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]))
                );
                nbColors++;
            }
        }
    }

}
