.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Programmation par contrat
=========================

Comme tout bon développeur Java, vous utilisez régulièrement la programmation
par contrat et la puissance de Java dans votre code en ajoutant des génériques,
des interfaces,... dès que possible et vous souhaitez retrouver les mêmes
mécanismes dans JAXX.

Ajout de contrats sur le code généré
------------------------------------

Le compilateur JAXX génère des classes à partir de fichiers JAXX mais n'est pas
capable d'ajouter des contrats sur les objets générés, donc interdit en quelque
sorte la programmation par contrat.

Pour palier à cette limitation, vous pouvez utiliser l'attribut spécial
*implements*.

Cet attribut ne doit être placé que sur le tag racine d'un fichier JAXX et son
contenu est le nom qualifié d'un ou plusieurs contrats séparés par des virgules.
::

  <JPanel implements='java.lang.Comparable'>

    <script>public int compareTo(JPanel o) { return getName().compareTo(o.getName()); }</script>

  </JPanel>

La classe générée aura bien le contrat *java.lang.Comparable*.

Attention a bien implémenter toutes les méthodes du contrat dans les scripts ou
votre fichier ne compilera pas.

Générer des classes abstraites
------------------------------

En utilisant l'attribut abstract et en le positionnant à true, la classe générée
est abstraite. ::

  <JPanel abstract='true'/>

Manipuler les types génériques
------------------------------

Il est possible d'utiliser les types génériques dans JAXX en utilisant les
attributs *genericType* et *superGenericType*.

L'attribut genericType précise le type générique d'une classe. Exemple (fichier
Parent.jaxx) : ::

  <JPanel genericType='E'
          implements='java.lang.Comparable&lt;E&gt;'
          abstract='true' />

La classe générée sera de la forme : ::

  public abstract Parent<E> implements java.lang.Comparable<E> {
     ...
  }

Pour surcharger une telle classe, on utilise l'attibut *superGenericType* en lui
donnant le type à utiliser. Par exemple (fichier Son.jaxx) : ::

  <Parent superGenericType='String'
          abstract='true'/>

La classe générée sera de la forme : ::

  public Son extends Parent<String> {
     ...
  }
