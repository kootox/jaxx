.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================================================================
Amelioration de la gestion des validateurs dans les interfaces graphiques
=========================================================================

Abstract
~~~~~~~~

Fonctionnement actuel
---------------------

Dans la version 2.2 de JAXX, tout se fait a la generation : chaque composant a
valider est encapsule dans un layer pour pouvoir ensuite afficher l'etat de 
validation de la propriete du bean qu'il represente.

Cela nous force a toujours avoir un couplage fort entre ce qui est a valider
et les composants d'edition, on ne peut pas avoir actuellement un composant
d'edition portant sur plusieurs proprietes d'un bean.

De meme, on ne peut pas avoir des validateurs qui descendent et se greffe sur
des composants complexes (car a la generation des composants on ne sait pas qui
va les valider).

Nouveau fonctionnement
----------------------

L'idee c'est d'utiliser un seul layer par formulaire. Cela a de multiple 
avantages :

- utilisation de moins de resources : un layer au lieu de n.

- ne plus alterer les composants d'edition (le fait d'ajouter un layer perturbre 
  l'affichage, et par exemple si on valider sur des boutons ils sont plus petits
  que les autres et c'est moche).

- ne plus rien fige a la generation

- permettre d'utiliser des composants d'edition portant sur plus d'une propriete

Mise en place
~~~~~~~~~~~~~

- definir un nouveau tag dans JAXX BeanValidatorForm (ou un decorator) qui
  correspond au layer unique du formulaire a valider

- ecrire le layer qui va dessiner ce qu'il faut a l'ecran :

  - il doit connaitre les validateurs qui lui sont attaches et il faut ainsi 
    retrouver tous les composants d'edition (en descendant si besoin dans des 
    sous interfaces graphiques, plus de probleme car non lie a la generation).
  - il faut une methode computeUI pour calculer la position des notifications 
    pour chaque composant.

  - il ecoute le modele des messages de validation pour modifier la vue.
  - il ecoute le composant container pour recalculer la disposition (changement
    de taille, changement de disposition,...).
