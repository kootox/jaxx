.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Utiliser les objets Swing
=========================

Dans JAXX, comme nous générons des interfaces Swing, il est possible d'utiliser
directement les objets Swing. Par exemple, si je veux créer un JPanel Swing,
j'utilise le tag <JPanel> si je veux créer un JButton Swing, j'utilise le tag
<JButton>, et ainsi de suite. Tous les objets Swing sont ainsi disponible
aisément sans avoir besoin de déclarer des imports dans le fichier JAXX.

Pour configurer ces objets Swing, il suffit de leur passer en attribut la valeur
souhaitée. Par exemple, si je veux créer un JButton qui a le texte 'Valider' et
qui appelle une méthode statique MyApplication.validate(), j'écris le code JAXX
suivant ::

  <JButton id='myButton'
           text='Valider'
           onActionPerformed='{org.my.package.MyApplication.validate()}'/>
  </JButton>

Propriétés
----------

Pour généraliser, un attribut myAttribute sera passé en paramètre de la méthode
setMyAttribute. Dans notre cas, le JButton myButton sera créé, puis on
effectuera l'appel de méthode suivant : **myButton.setText("Valider")**.

Evenements
----------

De même, pour les évènements, il est possible de déclarer des méthodes écoutant
les évènements créés par un objet Swing. Ainsi un attribut onMyAction sera
appelé lors d'un appel à la méthode fireMyAction de l'objet Swing. Dans le cas
de notre JButton, la méthode **org.my.package.MyApplication.validate()** sera
appelée lorsqu'un évènement de type ActionEvent sera créé par la méthode
fireActionPerformed du JButton.

Code Java
---------

Dand l'example présenté en début de page, vous avez pu remarquer un appel à du
code java (org.my.package.MyApplication.validate()). Il est possible d'appeler
du code Java dans JAXX depuis un attribut d'un tag en échappant le code Java par
des accolades comme dans l'example précédent.
