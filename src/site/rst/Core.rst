.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

++++
Core
++++

.. contents::


Nouvelles fonctionnalités apportées sur les classes générées.

---------
Interface
---------

Présentation
============

Ajout de contrats sur le code généré dans JAXX.

Mécanisme
=========

Le compilateur JAXX génère des classes à partir de fichiers JAXX mais n'est pas capable d'ajouter des contrats sur
les objets générés, donc interdit en quelque sorte la programmation par contrat.

Pour palier à cette limitation, on a ajouté un attribut spécial *implements*.

Cette attribut ne doit être placé que sur le tag racine d'un fichier JAXX et son contenu est le nom qualifié d'un ou
plusieurs contrats séparaés par des virgules.

::

  <JPanel implements='java.lang.Comparable'>

    <script>public int compareTo(JPanel o) { return getName().compareTo(o.getName()); }</script>

  </JPanel>

La classe générée aura bien le contrat *java.lang.Comparable*.

--------
Abstract
--------

Présentation
============

Ajout de la possibilité de générer des classes abstraites.

Mécanisme
=========

Ajout d'un attribut *abstract*.

::

  <JPanel abstract='true'/>

La classe générée sera abstraite.

--------
Generics
--------

Présentation
============

Ajout de possible de type generique sur les inferfaces et superclass.

Mécanisme
=========

Ajout d'un attribut genericType et superGenericType.

Exemple : (fichier Parent.jaxx)
::

  <JPanel genericType='E'
          implements='java.lang.Comparable&lt;E&gt;' 
          abstract='true' />

La classe générée sera de la forme :

::

  public abstract Parent<E> implements java.lang.Comparable<E> {
     ...
  }

Pour surcharger une telle classe (fichier Son.jaxx) :

::

  <Parent superGenericType='String' 
          abstract='true'/>

La classe générée sera de la forme :

::

  public Son extends Parent<String> {
     ...
  }


TODO
====

Permettre l'utilisation des types génériques dans les scripts.

--------
JavaBean
--------

Présentation
============

Ajout du support complêt des javaBean dans JAXX.

Mécanisme
=========

Il est possible dans JAXX de rajouter des objets quelconques via leur nom qualifié de classe :

::

  <JPanel>
    <java.lang.Boolean id='myState' constructorParams='true'/>
    <JLabel text='text' visible='{isMySate()}'/>
  </JPanel>

Avant l'ajout de la fonctionnalité, le code généré possèdait :

  * une propriété en lecture seul nommé *myState*.

Aucun support javaBean n'était présent et le databinding sur la propriété *visible* du label n'est pas créé. Cela veut
dire que le label sera initialisé avec la valeur initiale du boolean et c'est tout...

Avec l'ajout du support javaBean, on peut maintenant faire ces bindings, pour ce faire il suffit d'ajouter un attribut
*javaBean* sur l'objet :

::

  <JPanel>
    <java.lang.Boolean id='myState' javaBean='true'/>
    <JLabel text='text' visible='{isMySate()}'/>
  </JPanel>

On aura donc en plus :

  * un mutateur sur la propriété *myState* qui déclanchera l'envoie d'un *PropertyChange* sur la propriété lors de modification de valeur.

Ainsi le compilateur JAXX sera capable d'enregistrer un novueau dataBindig sur la propriété *visible* du label et la
modification de l'état *myState* sera automatiquement répercuté sur la propriété.

Note: le contenu de l'attribut *javaBean* est l'initialiteur de la propriété.
