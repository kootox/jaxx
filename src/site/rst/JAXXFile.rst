.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==============================
Qu'est-ce qu'un fichier JAXX ?
==============================

Un fichier JAXX est un fichier XML. Le tag racine de votre fichier JAXX est le
composant qui va être généré. Par exemple, un fichier myJaxxComponent.jaxx comme
ceci::

  <JPanel id='myPanelId'>
  </JPanel>

Ce code va générer une classe myJaxxComponent.java qui étends JPanel et
implémente JAXXObject.

Maintenant, si on souhaite ajouter des composants à l'intérieur de ce panel,
il suffit de rajouter un fils à l'élément JPanel ::

  <JPanel id='myPanelId'>
    <JButton id='myButton'/>
  </JPanel>

Ce code va générer un JPanel qui contient un JButton nommé myButton. On peut
maintenant jouer avec l'arborescence de notre fichier JAXX pour créer simplement
des interfaces complexes.
