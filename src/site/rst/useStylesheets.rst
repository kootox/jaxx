.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===================
Utiliser des styles
===================

Introduction
------------

JAXX vous permet de séparer le contenu de l'apparance, un peu comme en HTML, en
utilisant les feuilles de style CSS. Les feuilles de style JAXX peuvent assigner
n'importe quelle propriété d'un objet, et permettre a n'importe quel nombre
d'objets d'être décorés de la même manière avec des règles simples. De plus, les
feuilles de style vous permettent de créer facilement des comportements
dynamiques.

Appliquer un style
------------------

Pour qu'une application utilise une feuille de style, il lui suffit d'avoir
une balise style à sa racine ::

  <Application title='Example'>
    <style source='example.css'/>
    ...
  </Application>

Les sélecteurs
--------------

Ensuite, la feuille de style en question ressemble à une feuille de style HTML.
Le style d'une classe Java est définit de la manière suivante ::

  JSlider {
    paintTicks: true;
    minorTickSpacing: 10;
    majorTickSpacing: 50;
  }

Ainsi toutes les JSlider de l'application auront ce style. On peut également
définir le style d'une balise ayant un id particulier de cette manière::

  #red {
    background: red;
  }

Ainsi, les balises qui ont un id red auront toute un background rouge.

Pour résumé, il y a 4 types de sélecteur de style.

+-----------------+---------------------------+-------------------------------------------------------------------------------+
| Type            | Syntaxe                   | Description                                                                   |
+=================+===========================+===============================================================================+
| classe Java     | classname                 | Sélectionne toutes les instances de la classe (ou ses sous-classes) spécifiée |
+-----------------+---------------------------+-------------------------------------------------------------------------------+
| ID              | #id                       | Sélectionne les composants ayant l'attribut id='<id>'                         |
+-----------------+---------------------------+-------------------------------------------------------------------------------+
| Classe de style | .styleclass               | Sélectionne les composants avec l'attribut styleClass='<styleClass>'          |
+-----------------+---------------------------+-------------------------------------------------------------------------------+
| Pseudo-classe   | :mouseover, :focused, ... | Sélectionne les composants dynamiquement selon leur état                      |
+-----------------+---------------------------+-------------------------------------------------------------------------------+

Expressions Java
----------------

Beaucoup de style Swing ne peut être déclaré qu'en utilisant des expressions
Java. Ces expressions Java peuvent être insérées dans des feuilles de style si
elles sont échappées par des accolades. Par exemple::

  JSlider {
    border: {BorderFactory.createLineBorder(Color.black, 1)};
  }

Combiner les sélecteurs
-----------------------

Un sélecteur est une expression spécifique qui peux être vraie ou fausse pour
n'importe quel type de composant. Vrai signifie que le composant est affecté par
la règle, faux signifie qu'il ne l'est pas. Les quatres types de sélecteur
basiques peuvent être combinés pour former des expressions composées:

* JButton.cancel: S'applique à toutes les instances de JButton (ou une sous-classe) qui a styleClass='cancel'.
* #display:disabled: S'applique au composant ayant l'ID display quand il est dans le statut disabled.
* AbstractButton.large:selected: S'applique a toutes les sous-classes d'AbstractButton ayant styleClass='large' lorsqu'ils sont dans le statut selected.

Le style
--------

Le support des css JAXX est relativement similaire au HTML. La grosse différence
est qu'au lieu d'appliquer des propriétés spécifiques au HTML comme padding
ou color, vous appliquez des propriétés spécifiques comme border ou foreground.
Les mêmes propriétés que vous pouvez spécifier directement sur les tags. JAXX
utilises aussi les pseudo-classe CSS, une fonctionnalité peu utilisée qui vous
permet de changer le style des liens, et étendue pour fonctionner avec tous les
composants permet de répondre à a peu près tous les évènements.

Les pseudo-classes
------------------

Il peut-être intéressant de changer le style d'un composant lorsqu'il est dans
un état particulier. Dans ce cas, il suffit d'utiliser les pseudo-classes.
Par exemple, si on souhaite que notre élément ayant l'ID red ne devienne rouge
que lorsqu'il est survolé par la souris, il suffit de changer le style à::

  #red:mouseover {
    background: #E7ADAD;
  }

JAXX supporte les pseudo-classes suivantes:

* mouseover
* mouseout
* mousedown
* mouseup
* enabled
* disabled
* focused
* unfocused
* selected
* deselected
* armed
* unarmed

Pseudo-classes programmatiques
------------------------------

Chaque pseudo-classe est, comme tous les sélecteurs, un test vrai/faux. La
souris survole le composant, ou pas ? En plus de toutes les pseudo-classes,
JAXX vous permet de spécifier explicitement un test comme ceci::

    JSlider#powerLevel:{object.getValue() > 100} {
      background: red;
    }

Il apparait au même endroit qu'une pseudo-classe, sauf que c'est une expression
Java échappée par des accolades. Avec ce style, le JSlider avec l'ID powerLevel
deviendra rouge lorsque sa valeur dépassera 100. Cette sorte de pseudo-classe
est appellée pseudo-classe programmatique, et elle vous donne toute la
flexibilité possible pour décider si un style doit être appliqué à un objet.

Le système de data binding de JAXX est utilisé pour suivre les changements d'une
expression, ainsi les pseudo-classes programmatique supportent les mêmes
propriétés que le data-binding. En interne, la plupart des pseudo-classes JAXX
sont implémentées comme des pseudo-classes programmatiques. Ainsi la
pseudo-classe focused, par example, est exactement équivalente à
{object.hasFocus()}, mais beaucoup plus simple à retenir.
