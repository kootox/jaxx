.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

============
Présentation
============

Qu'est-ce que JAXX ?
--------------------

JAXX est un framework de création d'interfaces utilisateur Swing libre en XML
automatisant un certain nombre de mécanismes complexes de Swing. En effet, qui
ne s'est pas arraché les cheveux sur des layouts managers trop complexes, des
classes internes anonymes à répétition et des bidouillages bas-niveau pour
réaliser une interface utilisateur, certes magnifique, mais à quel prix.

JAXX se base sur XML qui permet de décrire les interfaces en gardant le principe
d'arbre et d'inclusion de composants sans se préocupper des classes internes,
du style, des layouts,...

Pourquoi utiliser JAXX au lieu de Swing ?
-----------------------------------------

- Les fichiers XML ont naturellement une structure d'arbre. Les tags contiennent des tags, tout comme les composants contiennent des composants.
- JAXX gère les évènements et le data binding pour vous. Plus besoin de créer de classes anonymes à la main pour gérer les évènements.
- La gestion des CSS vous permet de séparer le fond de la forme, et de changer l'aspect de votre programme en changeant juste de feuille de style.
- Les fichiers JAXX sont bien plus courts et faciles à comprendre que le code Java équivalent.
- La balise <Table> fournit la puissance du GridBagLayout en en simplifiant l'utilisation.
