.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

------------------------------------
Utilisation des java beans dans JAXX
------------------------------------

Il est possible dans JAXX de rajouter des objets quelconques via leur nom
qualifié de classe. Example ::

  <JPanel>
    <java.lang.Boolean id='myState' constructorParams='true'/>
    <JLabel text='text' visible='{isMyState()}'/>
  </JPanel>

Cela permet de faire du data-binding en ajoutant un attribut *javaBean* sur
l'objet, sa valeur initialisant le bean. Exemple ::

  <JPanel>
    <java.lang.Boolean id='myState' javaBean='true'/>
    <JLabel text='text' visible='{isMySate()}'/>
  </JPanel>

ou ::

  <java.lang.String id='myStatus' javaBean='Opening...'/>


Cela permet d'avoir un mutateur sur la propriété *myState* qui déclenchera
l'envoi d'un *PropertyChange* sur la propriété lors de modification de valeur
(ce qui n'est pas possible en utilisant une propriété en lecture seule).

Ainsi le compilateur JAXX sera capable d'enregistrer un nouveau dataBinding sur
la propriété *visible* du label et la modification de l'état *myState* sera
automatiquement répercuté sur la propriété.

