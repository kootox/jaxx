.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======================
Migration vers JAXX 2.0
=======================

.. contents::


Présentation
------------

Ce document énumère les choses à migrer pour passer sur la version 2.0 de JAXX.

Nouvelles fonctionnalités
-------------------------

  * JList, JComboBox et JTree n'acceptent plus de fils Item (utiliser JAXXList,
    JAXXComboBox, JAXXTree).

  * Changement de paquetage de jaxx.runtime.JAXXContext vers jaxx.runtime.JAXXContext

  * Changement de paquetage de jaxx.runtime.Decorator vers jaxx.runtime.decorator.Decorator

  * Le framwork de Navigation n'est plus compatible avec l'ancien...

  * JAXX interprète toutes les interfaces, elles doivent être donc importer

  * Les classe PropertychangeListener et PropertyChangeEvent ne sont plus
    automatiquement importées et il faut le faire manuellement quand requis.

  * L'api des bindings a complètement été revue et permet de les utiliser par
    programmation.

Aide à la migration
-------------------

Pour tout problème rencontré lors d'un passage à la version 2.0, vous pouvez
utiliser les listes de diffusions du projet disponibles ici :
http://maven-site.nuiton.org/jaxx/mail-lists.html  

