.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

-------------
BeanValidator
-------------

.. contents::


Présentation
============

Ajout du support de validation dans JAXX.

La techonologie utilisée est celle de Struts 2 (XWorks 2).


Configuration
=============

La configuration des validateurs se fait via des fichier xml (on peut aussi utiliser des annotations,...).

Ajout d'un validateur
*********************

Pour enregister un nouveau  validateur sur un bean, il suffit de placer dans le même paquetage que le bean un fichier *XXX-validation.xml* où XXX est le nom non qualifié du bean.

On peut de plus affecter un context de validation, dans ce cas le fichier doit s'appeler *XXX-YYY-validation.xml*, où YYY est le nom du context de validation. 

Ainsi on peut valider de différentes manières un bean (par exemple selon son cycle de vie :création, modification, ...).

Ajout d'un nouveau type de validateur
*************************************

Il est aussi possible de définir de nouveau type de validateurs :

  * créer une classe qui étend FieldValidator
  * ajouter un fichier validators.xml (ou ajouter dans un tel fichier la définition du nouveau validator) à la racine du class-path.

I18n
****

Afin de rendre le mécanisme multi-langue, on propose dans les fichiers de validations d'utiliser des clef i18n pour les messages.

Un nouveau parseur dans notre plugin i18n a été ajouté pour détecter ces clefs. (*maven-i18n-plugin:0.7:parserValidation*)


Intégration dans JAXX
=====================

Deux nouveaux tags ont été conçus pour pouvoir décrire un validateur dans les fichiers JAXX.

Ce développement est dans le paquetage *jaxx.tags.validator*. 

tag BeanValidator
*****************

Permet de définir un nouveau validateur dans une classe JAXX.

TODO Refaire cette doc qui n'est plas à jour suite au refactoring de la validation (20090202)

Les attributs autorisés sont les suivants :

  * *id* : le nom du validateur

  * *bean* : l'id d'un bean connu par la classe JAXX. On ne peut pas utiliser ici une expression car on n'est pas sûr de pouvoir déterminer le type de cette expression pendant le parsing ? (a verifier).

  * *beanClass* : le FQN de classe du bean à valider. Peut-être non présent si l'attribut *bean* est renseigné.

  * *beanInitializer* : une expression pour initialiser le bean à valider. TODO a revoir : on devrait utiliser uniquement un seul attribut *bean*...

  * *contextName* : le nom du contexte de validation.

  * *autoField* : flag pour indiquer l'inscription implicite des validateurs de champs du bean. Pour ce faire on parcourt l'ensemble des champs du bean et on ne considère uniquement ceux dont on connait dans la classe JAXX un composent d'édition du champ (id=nom du champ). Il est possible de surcharger ce configuration implicite en rajoutant explicitement des champs (voir le tag *field*).
    
  * *errorList* : le composant graphique pour afficher la liste des erreurs, doit étendre *javax.swing.JList*. Si non présent, on essayera le component d'id *errorList*.

  * *errorListModel* : le modèle qui contient la liste des erreurs (et est liée au composant *errorList*), doit étendre *jaxx.runtime.validator.swing.SwingValidatorErrorListModel*. Si non présent on essayera le composent d'id *errorListModel*.

  * *uiClass* : le FQN de la classe utilisé pour le rendu des erreurs sur les wigets d'édition. La classe doit étendre *org.nuiton.jaxx.validator.swing.ui.AbstractBeanValidatorUI*. Si non présent, on utilise par défaut le render *org.nuiton.jaxx.validator.swing.ui.IconValidationUI*.


Le tag supporte aussi l'ajout de tag *field* comme fils pour définir explicitement des champs à validater.

tag field
*********

Le tag *field* définit une entrée dans le validateur, on définit une correspondance entre le champ à valider et le composant d'édition de ce champ.

Le tag supporte les attributs suivants :

  * *name* : le nom de la propriété du bean associée.
    Cet attribut est obligatoire.

  * *component* : l'id du composant graphique d'édition associé à la propriété du bean.
    Cet attribut peut être omis si le nom de la propriété du bean est identique à celui du composent graphique d'édition.


Les classes du runtime
======================

Ce développement est dans le paquetage *jaxx.runtime.validator* (sauf pour l'interface *org.nuiton.jaxx.validator.JAXXValidator*).

Il s'agit de l'ensemble des classes ajoutées dans le module *jaxx-core* pour encapsuler la validation dans les fichiers java générés à partir des fichiers JAXX.


interface org.nuiton.jaxx.validator.JAXXValidator
************************************

Ce contrat a été ajouté à tous les objets JAXX générés (donc l'interface JAXXObject étend JAXXValidator).

On définit ici uniquement des méthodes d'accès aux validateurs enregistrés dans le JAXXObject.

TODO on pourrait ajouter des méthodes pour savoir l'état de validation d'un validateur ? 


classe jaxx.runtime.validator.swin.SwingValidator
*************************************************

Il s'agit de la classe principale d'encapsulation d'un validateur XWorks 2.

Le principe est simple : le validateur écoute les modifications sur le bean associé et revalide le bean à chaque modification. La validation met à jour la liste des erreurs asociées.

On se base sur les PropertyChangeListener pour écouter les modification des beans. Il faut donc que les beans supporte ces listeners.

Pour les entités de ToPIA, il suffit de positionner un contexte (topia) au bean pour profiter des listeners liés.

Pour les DTO de ToPIA, les générateurs ont été modifiés pour gérer ce support.

Les méthodes à retenir sont :

  * *setBean* pour affecter un bean au validateur (pour déasactiver passer null). Lors de la désactivation d'un bean, les erreurs associés sont retirées de la liste des erreurs.

  * *setContextName* pour affecter un nouveau nom de context de validation (par défaut on utilise pas de context de validation).

  * *validate* pour lancer une validation sur le bean liée (ne sera opérant uniquement si un bean est liée et qu'une liste d'erreur est liée).

  * *isValid* pour connaitre l'état du validateur à un moment donné.

Normalement la méthode *validate* ne devrait pas être appelée directement. : elle est automatiquement invoquée lorsqu'une propriété du bean est modifiée.

classe jaxx.runtime.validator.swing.SwingValidatorErrorModel
************************************************************

Modélisation d'une erreur renvoyé par le validateur, on conserve ici :

  * le validateur qui a provoqué l'erreur

  * la propriété du bean en faute

  * le composent graphique d'édtion de la propriété

classe jaxx.runtime.validator.swing.SwingValidatorErrorListModel
****************************************************************

Le modèle de la liste des erreurs renvoyées par le validateur. Il s'agit d'une extension d'un *javax.swing.DefaultListModel* qui permet de gérer une liste d'erreurs provenant de plusieurs validateurs en même temps.

classe jaxx.runtime.validator.swing.SwingValidatorErrorListMouseListener
************************************************************************

Un listener écoutant les double clics sur une liste d'erreurs et qui donne le focus au composent graphique d'édition associée à la propriété dont l'erreur est sélectionné.
  

Les conversions
===============

Pour l'édition de proriétés qui ne sont pas des chaines de caractères, des erreurs de conversions peuvent survenir (conversion de la valeur de l'édtieur graphique vers le bean), avant que l'on utilise 
réellement la mécanique de la validation.

Pour palier à ce problème on a intégré la gestion des erreurs de conversion dans le validateur.
Pour ce faire, il suffit de faire appel à la méthode suivante pour injecter dans un bean une propriété :

  ::

    jaxx.runtime.JAXXUtil.convert(validator,"nomDeLaPropriété",widget.getText(),TypeDeLaProriete.class);
  

On obtiendra si une erreur de conversion intervient, une erreur dont le libellé est de la forme *error.convertor.XXX*
où XXX est nom simple en minuscule du type de la propruité. (par exemple : *error.convertor.integer*).




