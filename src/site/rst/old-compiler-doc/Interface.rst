.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

---------
Interface
---------

.. contents::


Présentation
============

Ajout de contrats sur le code généré dans JAXX.

Mécanisme
=========

Le compilateur JAXX génère des classes à partir de fichiers JAXX mais n'est pas capable d'ajouter des contrats sur
les objets générés, donc interdit en quelque sorte la programmation par contrat.

Pour palier à cette limitation, on a ajouté un attribut spécial *implements*.

Cette attribut ne doit être placé que sur le tag racine d'un fichier JAXX et son contenu est le nom qualifié d'un ou
plusieurs contrats séparaés par des virgules.

::

  <JPanel implements='java.lang.Comparable'>

    <script>public int compareTo(JPanel o) { return getName().compareTo(o.getName()); }</script>

  </JPanel>

La classe générée aura bien le contrat *java.lang.Comparable*.

TODO
====

Il serait intéressant lors de l'injection de contrats sur un objet jaxx de pouvoir vérifier si toutes les méthodes du
contrat sont bien implantées dans la classe, et si ce n'est pas le cas de rendre la classe abstraite.
