.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

====
Demo
====

Une démonstration de JAXX est disponible en utilisant Java Webstart.

`Demonstration de JAXX`_

..Fonctionne seulement si site déployé

Cette démonstration a été réalisée en utilisant JAXX, et vous pouvez visionner
tout ou partie du code source en cliquant sur l'onglet 'Sources'.

Dans cette démonstration, vous pouvez observer nombre des atouts de JAXX et de
ses composants :

  * Boutons
  * Eléments de formulaire
  * Layouts
  * Menus
  * Fenêtres
  * Editeurs
  * Arbre de navigation
  * Data-binding
  * Validation

.. image::demo1.png
:width:800px

.. image::demo2.png
:width:800px

.. _Demonstration de JAXX: ./jaxx-demo/jnlp/jaxx-demo.jnlp
