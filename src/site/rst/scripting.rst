.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===============================================
Utiliser des scripts pour améliorer l'ordinaire
===============================================

Dans JAXX, le scripting c'est la capacité d'embarquer du code Java directement
dans les fichiers JAXX. Et contrairement à ce qu'on entends par script, le code
n'est pas interprété à l'exécution, mais compilé afin de ne pas souffrir d'un
défaut de performances.

Dans JAXX, les scripts peuvent apparaitre à plein d'endroits :

  * dans des balise script
  * dans le data binding
  * dans les gestionnaires d'évènements
  * dans l'attribut constraints
  * dans les paramètres des constructeurs

Les balises script
------------------

Du code Java peut être embarqué n'importe où dans les fichiers JAXX en utilisant
les balises <script>. Les balises script peuvent définir des méthodes, des
champs, des constructeurs, et peuvent aussi exécuter directement du code Java.

Exemple::

  <script><![CDATA[
  import javax.swing.table.JTableHeader;

  public SearchHandler getSearchHandler() {
    return getContextValue(SearchHandler.class);
  }
  ]]>
  </script>

Le data-binding
---------------

Le code Java peut être placé en valeur d'attributs XML quand il est échappé par
des accolades ({ }). La valeur de l'attribut est ensuite calculée en utilisant
le code Java, et recalculée quand ses dépendances (s'il y en a) changent. Cette
utilisation des script est appelée data-binding_

.. _data-binding: dataBinding.html

Exemple::

  <JPanel layout='{new BorderLayout()}'>

Gestionnaires d'évènements
--------------------------

Dans les balises de classes, les attributs dont le nom commence par 'on' suivi
d'une majuscule (e.g. onMousePressed, onActionPerformed) sont des gestionnaires
d'évènement. La valeur de l'attribut est du code Java qui est exécuté lorsque
l'évènement est lancé.

Exemple::

  <JButton onActionPerformed='myAction()'/>

Attribut constraints
--------------------

Sur les composants, l'attribut constraints spécifie les contraintes du layout
(e.g. BorderLayout.NORTH, GridBagConstraints) qui devraient être utilisées
lorsque le composant est ajouté dans son conteneur parent. La valeur de cet
attribut est du code Java qui est évalué à la création du composant.

Exemple::

  <JButton constraints='BorderLayout.SOUTH'/>

Paramètres des constructeurs
----------------------------

Dans les balises de classe, l'attribut constructorParams spécifie les
paramètres, séparés par des virgules, du constructeur de l'objet. Ces paramètres
sont interprétés comme du code Java.
