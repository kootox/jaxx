.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

====================================
CSS - Utiliser les feuilles de style
====================================

Dans ce troisième tutoriel, nous allons essayer de simplifier notre code JAXX en
utilisant une feuille de style. Nous allons, pour cela, nous baser sur le code
du précédent tutoriel : helloworld2.

Le principe des feuilles de style JAXX
--------------------------------------

Dans JAXX, les feuilles de style permettent de sortir du fichier JAXX
toutes les instructions ayant trait au style de l'application. Cela permet aussi
de mutualiser les déclarations d'éléments graphiques (comme pour les feuilles
de style en HTML). Pour approfondir le sujet, vous pouvez consulter `la
documentation`_

.. _la documentation: ../useStylesheets.html

Enlever le style de notre fichier JAXX
--------------------------------------

Nous allons enlever toutes les informations de style de notre fichier JAXX et
ajouter un tag id à chaque élément afin de l'identifier dans notre feuille de
style. On obtient le code suivant::

  <Application id='tuto'>

      <JLabel id='hello'
              constraints='BorderLayout.NORTH'/>

      <JButton id='button'
               constraints='BorderLayout.SOUTH'
               onActionPerformed='dispose()'/>

  </Application>

Cela simplifie grandement notre code.

Créer la feuille de style
-------------------------

Nous allons créer une feuille de style, située au même niveau de l'arborescence
que notre fichier JAXX. Nous allons référencer la feuille de style dans notre
fichier JAXX en rajoutant le code suivant en fils de notre tag Application::

  <style source='Css.css'/>

Dans la feuille de style, il faut retrouver le même niveau que lors du premier
tutoriel. Nous allons donc commencer par notre Application et lui donner un
titre.

Pour l'identifiant tuto, on veux rajouter l'attribut title et lui donner une
valeur. On écrira donc dans la feuille de style::

  #tuto {
      title:"CSS Tutorial";
  }

Pour notre JLabel, on va décider que le même style et le même texte seront
appliqués à tous les JLabel. On mettra le texte à Hello World, la couleur à
rouge et la taille à 20. On écrira donc::

  JLabel {
      font-size:20;
      foreground:red;
      text:"Hello World";
  }

Nous allons aussi mettre le texte de notre JButton::

  #button {
      text:"Close"
  }

Ajouter du style dynamique
--------------------------

Nous allons maintenant ajouter du style dynamique. Nous allons faire changer la
couleur de notre JLabel lorsqu'il est survolé. Nous utiliserons pour cela le
modifieur mouseover::

  #hello:mouseover {
      foreground:green;
  }

Conclusion
----------

Dans ce tutoriel vous avez vu comment mettre du style sur vos composants depuis
un fichier CSS externe. Cela vous permet de créer un style pour votre
application et de le changer au besoin en changeant juste la feuille de style.
Pas besoin de multiplier les fichiers à impacter.

Sources de ce tutoriel
----------------------

Les sources de ce tutoriel sont disponibles au `telechargement ici`_.

.. _telechargement ici: https://forge.nuiton.org/projects/list_files/jaxx

Lancer ce tutoriel
------------------

.. image::webstart.gif

Pour lancer ce tutoriel via `Java Web Start`_, suivez `ce lien`_.

Pour plus de détail sur `webstart`_.

.. _Java Web Start: http://java.sun.com/products/javawebstart/

.. _ce lien: ../jaxx-tutorial-css/jnlp/launch.jnlp

.. _webstart: ../jaxx-tutorial-css/jnlp-report.html
