.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===========
Hello World
===========

Dans ce tutoriel, nous allons créer une fenêtre qui affiche le message
"Hello World". Jaxx s'utilise par défaut avec le gestionnaire de
dépendances/projet Maven. Nous supposons dans la suite de ce tutoriel que vous
savez utiliser Maven.

Créer un projet Maven/configuration de JAXX
-------------------------------------------

Nous allons commencer par créer un projet Maven simple, qui aura comme unique
source un fichier jaxx.

Dans le pom.xml, il faut configurer les repository nuiton pour pouvoir
bénéficier de JAXX et de son plugin Maven::

    <repositories>

        <!-- nuiton releases repository, needed to get jaxx -->

        <repository>
            <id>nuiton.release</id>
            <name>NuitonReleaseRepository</name>
            <url>http://maven.nuiton.org/release</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </repository>

    </repositories>

    <pluginRepositories>

        <!-- nuiton plugin releases repository, needed to get jaxx plugin -->

        <pluginRepository>

            <id>nuiton.release</id>
            <name>NuitonReleaseRepository</name>
            <url>http://maven.nuiton.org/release</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </pluginRepository>

    </pluginRepositories>

Il est aussi nécessaire d'ajouter la dépendance vers la librairie JAXX pour
la compilation et l'exécution::

    <dependencies>

        <!-- librairie Jaxx -->
        <dependency>
            <groupId>io.ultreia.java4all.jaxx</groupId>
            <artifactId>jaxx-runtime</artifactId>
            <version>2.0.1</version>
            <scope>compile</scope>
        </dependency>

    </dependencies>

Nous allons configurer aussi le plugin Maven pour la génération depuis les
fichiers JAXX::

    <build>
        <plugins>

            <plugin>
                <groupId>io.ultreia.java4all.jaxx</groupId>
                <artifactId>jaxx-maven-plugin</artifactId>
                <version>2.0.1</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

Le plugin JAXX doit être dans la même version que jaxx-runtime. Il faut
spécifier l'exécution du goal generate du plugin pour que le fichier JAXX soit
transformé en fichier Java.

Pour les autres plugins, je vous laisse les configurer seuls, il s'agit juste
de configurer maven-compiler plugins pour compiler les sources en version 1.5,
et maven-jar-plugin et maven-dependency-plugin pour rendre le jar exécutable.
Pour ceux qui ne sauraient pas comment faire cela, je vous laisse regarder un
oeil aux sources disponibles au téléchargement en bas de cette page.

Le fichier JAXX
---------------

Maintenant que le projet est configuré.Nous allons commencer par créer un
fichier JAXX nommé helloworld.jaxx que nous placerons dans le package de notre
choix (dans l'exemple, c'est org.nuiton.jaxx.tutos.helloworld mais libre à vous
d'adapter) ::

  <Application title='Hello World'>
    <JLabel text='Hello World'/>
  </Application>

Ce fichier Jaxx est très simple, il va créer une Application helloworld qui
étends une JFrame qui aura pour titre "Hello World" et placera dedans un
JLabel "Hello World".

Une fois que tout est créé, on se place à la racine du projet et on le build
avec la commande ::

  mvn compile

Cela aura pour effet de créer notre jar exécutable si tous les plugins ont bien
été configurés.

On peut alors le lancer et on obtient le résultat suivant:

.. image::helloworld.png

Conclusion
----------

Ce tutoriel vous a appris comment créer simplement une petite application Swing,
très simple au demeurant, en utilisant seulement 3 lignes de code Jaxx. Dans les
tutoriels suivant vous allez découvrir comment réaliser des applications
dynamiques beaucoup plus complexes.

Sources de ce tutoriel
----------------------

Les sources de ce tutoriel sont disponibles au `telechargement ici`_.

.. _telechargement ici: https://forge.nuiton.org/projects/list_files/jaxx

Lancer ce tutoriel
------------------

.. image::webstart.gif

Pour lancer ce tutoriel via `Java Web Start`_, suivez `ce lien`_.

Pour plus de détail sur `webstart`_.

.. _Java Web Start: http://java.sun.com/products/javawebstart/

.. _ce lien: ../jaxx-tutorial-helloworld/jnlp/launch.jnlp

.. _webstart: ../jaxx-tutorial-helloworld/jnlp-report.html
