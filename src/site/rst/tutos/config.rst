.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==========================
Interface de Configuration
==========================

Dans ce tutoriel, nous allons apprendre à utiliser l'api de construction
d'interface graphique de configuration.

Sources de ce tutoriel
----------------------

Les sources de ce tutoriel sont disponibles au `telechargement ici`_.

.. _telechargement ici: https://forge.nuiton.org/projects/list_files/jaxx

Lancer ce tutoriel
------------------

.. image::webstart.gif

Pour lancer ce tutoriel via `Java Web Start`_, suivez `ce lien`_.

Pour plus de détail sur `webstart`_.

.. _Java Web Start: http://java.sun.com/products/javawebstart/

.. _ce lien: ../jaxx-tutorial-config/jnlp/launch.jnlp

.. _webstart: ../jaxx-tutorial-config/jnlp-report.html

