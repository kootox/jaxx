.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=============
Hello World 2
=============

Dans ce deuxième tutoriel, nous allons un petit peu complexifier les choses et
améliorer l'application réalisée dans le premier Hello World. Nous allons
ajouter un peu de style à notre exemple et ajouter un bouton pour fermer la
fénêtre.

Donner du style à notre texte
-----------------------------

Dans le premier tutoriel, le texte était déclaré comme ceci::

  <JLabel text='Hello World'/>

Quand l'interface utilisateur est créée, la méthode setText() du JLabel est
appelée avec l'attribut "Hello World". Nous souhaitons maintenant mettre ce
texte en rouge. La méthode correspondant, sur on objet JLabel, est
setForeground(). Donc nous allons ajouter un attribut foreground à notre JLabel
et il aura la valeur red (toutes les couleurs basiques sont implémentées)::

  <JLabel text='Hello World'
          foreground='red'/>

Si nous compilons et exécutons le code, on obtient :

.. image::helloworld2-red.png

Si vous souhaitez utiliser une couleur spécifique, vous pouvez aussi très bien
utiliser le code couleur HTML. Par example, en remplaçant red par #FF0000 vous
obtiendrez le même résultat.

Maintenant, cherchons à augmenter la taille de la police. Nous allons utiliser
l'attribut font-size (ce n'est pas un attribut standard de JLabel) et lui
donner, par example, l'attribut '24'::

  <JLabel text='Hello World'
        foreground='red'
        font-size='24'/>

Si nous compilons et exécutons le code, on obtient :

.. image::helloworld2-red-big.png

Ajouter un bouton qui ferme la fenêtre
--------------------------------------

Nous allons ajouter un bouton avec le texte "Close". Notre code devient::

  <Application title='Hello World'>

      <JLabel text='Hello World'
              foreground='red'
              font-size='24'
              constraints='BorderLayout.NORTH'/>

      <JButton text='Close'
               constraints='BorderLayout.SOUTH'/>

  </Application>

Si nous compilons et exécutons le code, on obtient :

.. image::helloworld2-red-big-button.png

Mais le bouton ne fait rien, il faut appeler la méthode dispose() quand une
action est réalisée sur le bouton pour fermer la fenêtre. Pour le réaliser,
nous allons donner la valeur dispose() à l'attribut onActionPerformed de notre
bouton. ::

      <JButton text='Close'
               constraints='BorderLayout.SOUTH'
               onActionPerformed='dispose()'/>

Les attributs commençant par on sont en fait des callbacks qui sont appelés
quand un évènement est lancé par l'objet en question. Ainsi dans notre exemple
la méthode dispose() est appelée quand un évènement ActionPerformed est lancé.

Nous allons revenir sur l'attribut constraints que l'on a passé à nos JLabel et
JButton. L'attribut constraints spécifie les contraintes de layout à utiliser
quand un composant est ajouté à son parent. Ici, nous utilisons le BorderLayout
pour placer les éléments les uns en dessous des autres. A noter que l'attribut
constraints n'est applicable que sur les composants descendants de la classe
Component. La valeur prise pas l'attribut constraints est forcément du code
Java qui doit retourner un Object ou null.

Conclusion
----------

Dans ce tutoriel vous avez vu comment mettre du style sur vos composants et
créer un bouton qui lance une méthode. Vous avez également vu comment placer
(très rapidement, il est vrai) des éléments dans un conteneur.

Sources de ce tutoriel
----------------------

Les sources de ce tutoriel sont disponibles au `telechargement ici`_.

.. _telechargement ici: https://forge.nuiton.org/projects/list_files/jaxx

Lancer ce tutoriel
------------------

.. image::webstart.gif

Pour lancer ce tutoriel via `Java Web Start`_, suivez `ce lien`_.

Pour plus de détail sur `webstart`_.

.. _Java Web Start: http://java.sun.com/products/javawebstart/

.. _ce lien: ../jaxx-tutorial-helloworld2/jnlp/launch.jnlp

.. _webstart: ../jaxx-tutorial-helloworld2/jnlp-report.html
