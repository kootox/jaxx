.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

============
Data-binding
============

Dans ce tutoriel, nous allons voir comment utiliser le `data-binding`_ pour
rendre votre application dynamique très simplement. Dans cet exemple, nous
réaliserons une petite application comprenant un champ texte à remplir, un
slider qui change de couleur en fonction de sa valeur et un boutton permettant
de quitter l'application, mais qui est disponible uniquement lorsque du texte
est entré dans le champ à remplir.

Textfield et bouton
-------------------

Nous allons laisser de côté pour l'instant notre slider et nous focaliser sur
le textfield et le bouton. Créons tout d'abord notre vue ::

  <Application id='tuto'>

      <style source='data-binding.css'/>

      <JTextField id='textField'
                  constraints='BorderLayout.NORTH'/>

      <JButton text='Button'
               constraints='BorderLayout.SOUTH'
               onActionPerformed='dispose()'/>

  </Application>

Si vous lancez l'application, le boutton est toujours valide.

.. image::alwaysEnabledButton.png

Nous allons donc le modifier et lui rajouter l'attribut ::

  enabled='{textField.getText().length() != 0}'

Ce que cela va faire c'est rajouter les listeners adéquats pour qu'à chaque fois
que le résultat de textField.getText() change, la propriété enabled du boutton
est réévaluée. En l'occurence, dès que la taille du texte est différente de 0,
le boutton est activé. Essayez pour voir.

.. image::disabledButton.png

.. image::enabledButton.png

Rajoutons le slider
-------------------

Nous allons rajouter un slider entre notre textField et notre bouton. On
utilisera pour cela le code suivant ::

  <JSlider maximum='200'/>

Nous souhaitons que lorsque l'on dépasse la moitié du slider (la valeur 100), la
couleur du fond passe à rouge. Nous allons rajouter cela au style de
l'application grace au data-binding. Dans notre feuille de style, rajoutons (ici
est décrit uniquement le style nous intéressant)::

JSlider:{object.getValue() > 100} {
  background: red;
}

Cela donne :

.. image::sliderBefore100.png

.. image::sliderAfter100.png

Dans le style, il est possible d'utiliser le data-binding sur object, qui sera
remplacé par l'objet en question dans l'application. Ici, comme pour notre
bouton précédemment, nous changeons une propriété (ici background) lorsque la
propriété value dépasse une certaine valeur.

Conclusion
----------

Vous avez découvert comment créer des interfaces dynamiques en utilisant le
data-binding. Vous avez-vu la simplicité ? Vous sentez votre créativité
augmenter ? Avec les bases que vous venez d'acquérir, vous pouvez simplement
créer des applications au look très évolué et dynamique. Il ne reste plus que
votre patte créative à mettre en route.


Sources de ce tutoriel
----------------------

Les sources de ce tutoriel sont disponibles au `telechargement ici`_.

.. _telechargement ici: https://forge.nuiton.org/projects/list_files/jaxx

Lancer ce tutoriel
------------------

.. image::webstart.gif

Pour lancer ce tutoriel via `Java Web Start`_, suivez `ce lien`_.

Pour plus de détail sur `webstart`_.

.. _Java Web Start: http://java.sun.com/products/javawebstart/

.. _ce lien: ../jaxx-tutorial-databinding/jnlp/launch.jnlp

.. _webstart: ../jaxx-tutorial-databinding/jnlp-report.html
