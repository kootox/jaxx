.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

====
JAXX
====

.. contents::

Presentation
------------

JAXX is a framework that allows you to build Swing User Interface in XML.

JAXX documentation is in French, we are planning to update it and make an
english translation at the same time. To help you wait, you can have a look to
the demo_.

Nouveautés de la version 2.8.6
------------------------------

Meilleure gestion des exceptions dans SwingSession
__________________________________________________

Voir le ticket https://forge.nuiton.org/issues/3221

Attention l'API a changée!


Nouveautés de la version 2.8.5
------------------------------

Ajout d'un bouton pour fermer les onglets
_________________________________________

L'interface de *TabContentModel* qui gère les onglets a une nouvelle méthode *isCloseable* qui retourne un *booléen*.
Si la méthode retourne *true*, un bouton est ajouté à l'onglet pour pouvoir le fermer.


Nouveautés de la version 2.8.2
------------------------------

Ajout du module jaxx-widgets-extra
__________________________________

Il s'agit de la reprise du projet https://forge.nuiton.org/projects/nuiton-widgets


Nouveautés de la version 2.6
----------------------------

Ajout de l'option addAutoUIHandler
__________________________________

Cette option permet de détecter un handler associé à chaque fichier jaxx.

Le handler doit porter le nom du fichier java associé au fichier jaxx suffixé par Handler.

Ce handler doit de plus implanter un nouveau contract *org.nuiton.jaxx.runtime.spi.UIHandler*.

Le contrat contient deux méthodes

- *beforeInit* appelée au tout début de la méthode *$initialize*
- *afterInit* appelée tout à la fin de la méthode *$initialize*

En résumé, pendant la méthode *beforeInit* il ne faut pas toucher aux UIs,
elles ne sont pas encore construites.

Voir https://forge.nuiton.org/issues/2946.

Ajout de nouveaux éditeurs
__________________________

De nouveaux éditeurs sont intégrés dans JAXX, à savoir :

- éditeur de temps (https://forge.nuiton.org/issues/2924)
- éditeurs de coordonnées spatiales (https://forge.nuiton.org/issues/2929)

Ils sont intégrés dans la démo de JAXX.

Nouveautés de la version 2.5
----------------------------

Changement de l'artifactId du plugin
____________________________________

Avant la version 2.4, l'artifactId du plugin était **maven-jaxx-plugin**, mais
Maven depuis la version 3 demande que nous utilisions plutôt
**jaxx-maven-plugin** car à terme la première forme d'artifactId ne sera plus
acceptée que pour les projets de maven.

Pour plus d'information sur comment utiliser le plugin, veuillez visiter la
`page des goals`_

Suppression des tutoriels
_________________________

La maintenance des tutoriels est compliquée, nous les désactivons dans cette version, il seront remplacés
bientôt par des articles sur notre blog (voir page `tutoriels`_).

Nouveautés de la version 2.4
----------------------------

Amélioration des imports
________________________

La version 2.4 fait le ménage dans le code généré par JAXX en se rapprochant du
projet eugene_.

Toute la génération n'est pas encore basée sur Eugene mais cela viendra avant la
version 3.

JAXX n'utilise plus des imports de paquetaques (import avec *), il est donc
désormais (enfin...) possible d'utiliser dans les scripts directement un import
sur *java.util.List*.

Le fait de ne plus importer des paquetages a des impacts et pourrait rendre
votre code non compilable.

Pour reproduire la génération comme en version *2.3*, il suffit de renseigner
dans la configuration du plugin la propriété *extraImportList* avec les valeurs
suivantes :

::

  <extraImportList>
  java.awt.*
  javax.swing.*
  javax.swing.event.*
  java.util.*
  </extraImportList>

Il est cependant déconseillé de laisser une telle configuration car le fait
d'utiliser des paquetages au lieu de noms de classes précises alourdit les
temps du compilateur JAXX et génère du code de moins bonne qualité...

D'ici la version *3.0* on écrira une documentation précise de fonctionnement du
compilateur et on va faire une différence entre les namespaces utilisés pour
trouver les objets correspondants au tags d'un fichier jaxx et les imports d'une
classe java à générer. Actuellement les deux concepts sont mélangés.

Utilisation de JXLayer 3.0.4
____________________________

L'utilisation de cette mise à jour (à partir de la 3.0.3) a des impacts car
l'api a été modifiée et n'est plus compatible... Il s'agit en fait de
modification des generics sur JXLayer, vous devrez peut-être modifier votre code
pour qu'il compile.

Nouveautés de la version 2.3
----------------------------

Validation
__________

JAXX se base désormais sur la librarie nuiton-validator_ (qui a été extraite de
jaxx).

Cela a entrainé un certain nombre de modification dans l'api de jaxx. Notamment :

- Les validateurs ont changé de packages et sont dans l'autre librairie : il
  faut donc modifier dans son validators.xml les fqn des validateurs en question.
  Un exemple est donné dans le module **jaxx-demo** ou
  **jaxx-tutorial-validation**.

- Réécriture de l'api de validation (on travaille désormais par annotation pour
  spécifier les validateurs d'un GUI ainsi que les champs à valider).

- L'api SwingValidator a été modifiée sans conserver les anciennes signatures
  car les propriétés en question (contextName devient context) sont des
  propriétés de type JavaBean et la maintenance de l'ancienne et la nouvelle
  api aurait été trop compliquée et confuse.
    
Dans la version 2.3.x ou 2.4 une documentation complète va être écrite
concernant l'utilisation de la validation avec JAXX, en attendant un nouveau
tutorial jaxx-tutorial-validation a été écrit pour aider un peu (seul
l'application est écrite, sans documentation pour le moment...) et la page
suivante : BeanValidator_

Amélioration du générateur
__________________________

Le compilateur JAXX est désormais capable plus facilement de générer des
annotations sur les champs et les méthodes.

Montées de version
__________________

Un certain nombre de librairies ont été montées de version, notamment
**nuiton-i18n** en version 2.2. Veuillez bien utiliser au moins cette version
car cela peut casser l'éxécution au runtime sinon.


Nouveautés de la version 2.2.4
------------------------------

Nouveau module de validation
____________________________

Un nouveau module a été crée **jaxx-validator** qui regroupe tout ce qui
concerne le framework de validation proposé par JAXX (anciennement quand **jaxx-runtime**).

Toute l'api de neutre non lié à JAXX a été dépréciée et sera supprimer en version
*2.3* pour utiliser celle de nuiton-validator (qui récupère le code).

A noter que le module **jaxx-runtime** n'est plus exposé en classifier **tests**
mais **jaxx-validator** l'est (pour pouvoir utiliser les tests abstraits sur
les validateurs).

Utilisation de i18n 2.0
_______________________

En utilisant **i18n 2.0**, on a des bundles de traductions compatible
**ResourceBundle**, ceci étant dit il faut que vous utilisiez aussi cette
version de i18n sinon il ne pourra pas utiliser les traductions offertes par
jaxx. 

Nouveautés de la version 2.1
----------------------------

Introduction d'un nouveau tag **import**
________________________________________

Ce tag simplifie la gestion des imports.

Chaque ligne correspond à un import à effectuer.

Exemple :

::
  <import>
  java.io.File
  static java.io.File.separatorChar
  </import>

Voir https://forge.nuiton.org/issues/show/685

Meilleure gestion de l'héritage
_______________________________

La gestion de l'héritage a été amélioré.

On peut désormais surcharger un tag dans un fichier jaxx fils, le getter surchargé
sera bien généré.

Si aucun initializer n'est renseigné (constructorParams, javabean, initializer)
alors rien de plus ne sera généré.

De plus pour les bindings, on doit utiliser la propriété surchargée et non pas
l'accesseur sur la propriété.

Lors de la génération, l'accesseur sera utilisé à la place de la propriété.

WARNING::
  Cette évolution admet une restriction : si on veut surcharger un
  component swing, on DOIT alors toujours lui préciser un initializer, sinon le
  component surchargé ne sera pas généré et à l'exécution on risque d'avoir un
  *ClassCastException* si le type n'est pas le même.

Une documentation plus détaillée sur le mécanisme d'héritage sera écrite pour la
version 2.2.


Voir

  * https://forge.nuiton.org/issues/show/625
  * https://forge.nuiton.org/issues/show/626

Nouvelle api d'arbre
____________________

Le package *jaxx.runtime.swing.tree** contient une api simplifié pour créer des
arbres qui savent se charger tout seul.

Cette api remplace celle du package **jaxx.runtime.swing.navigation**.

Voir https://forge.nuiton.org/issues/show/666

Nouvelle api d'assistant
________________________

Le package *org.nuiton.jaxx.runtime.swing.wizard.ext** contient une nouvelle api simplifié
pour créer des assitants avec des modèles attachés aux étapes.

Cette api remplace celle du **org.nuiton.jaxx.runtime.swing.wizard.WizardOperationXXX**.

Voir https://forge.nuiton.org/issues/show/665

Ajout de tutoriaux
__________________

JAXX intègre désormais des tutoriaux.

Voir

  * https://forge.nuiton.org/issues/show/640
  * https://forge.nuiton.org/issues/show/641
  * https://forge.nuiton.org/issues/show/642

Présentation
------------

JAXX est un framework qui vous permet de créer des interfaces utilisateur Swing
à partir de fichiers de description en XML. La documentation n'est plus à jour.
En attendant la mise à jour de la documentation (en cours), vous pouvez vous
reporter à la demo_ .

.. TODO A faire car plus a jour...

Le projet JAXX ...


Depuis la version 1.0, et en prévision de nouveaux générateurs s'appuyant sur Jaxx,on a revu l'architecture du projet.

Désormais, une séparation a été effectuée entre le code de compilation et le code d'exécution.

Tout le code de compilation est en dépendance du plugin maven et vous ne devriez pas à avoir à vous en servir.

Ce dont vous avez besoin dans vos dépendances sont uniquement les modules jaxx-runtime-xxx.

.. TODO A finir la présentation de la nouvelle architecture (dans la version 1.2).

**Veuillez consulter la JavaDoc pour de plus ample détails sur les différentes
librairies.**

Migration vers JAXX 2.0
-----------------------

La version 2.0 de JAXX n'est pas compatible avec les versions antérieures.

Pour plus de détail consulter la page `Migration`_.

Nouvelles fonctionnalités 2.0
-----------------------------

  * Core_

  * I18n_

  * JAXXContext_

  * BeanValidator_

  * NavigationModel_

Qui utilise JAXX ?
------------------

Voici une liste de projets utilisant JAXX :

 * Isis-fish_ - Logiciel de simulation de pêcheries complexes - GPL

 * simExplorer-si_

 * ObServe_ - Logiciel de saisie de données concernant la pèche thonière - GPL

 * Tutti_ - Logiciel de saisie de données concernant la pèche - GPL

 * Sammoa_ - Logiciel de saisie de données d'observation de mannifères marins en vol - GPL

 * Lima_ - Logiciel de comptabilité française adaptée aux PME - GPL

 * Vradi_ - Logiciel de traitement de flux XML - GPL

 * Nuiton-i18n-editor

.. TODO Finish this list and add icons

.. _Isis-fish: http://www.isis-fish.org/

.. _simExplorer-si: http://www.simexplorer.org

.. _ObServe: http://doc.codelutin.com/observe/

.. _Tutti: http://doc.codelutin.com/tutti/

.. _Sammoa: http://doc.codelutin.com/sammoa/

.. _Lima: http://doc.chorem.org/lima/

.. _Vradi: http://maven-site.forge.codelutin.com/vradi

.. _Migration: ./migration.html

.. _Core: ./Core.html

.. _demo: ./jaxx-demo/index.html

.. _I18n: ./I18n.html

.. _JAXXContext: ./JAXXContext.html

.. _BeanValidator: ./BeanValidator.html

.. _NavigationModel: ./NavigationModel.html

.. _nuiton-validator: http://doc.nuiton.org/nuiton-validator

.. _eugene: http://doc.nuiton.org/eugene

.. _page des goals: ./jaxx-maven-plugin/plugin-info.html

.. _tutoriels: ./tutoriels.html
