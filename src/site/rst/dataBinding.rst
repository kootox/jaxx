.. -
.. * #%L
.. * JAXX
.. * %%
.. * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========================
Utiliser le data-binding
========================

Dans un fichier JAXX, Les attributs peuvent prendre la valeur d'expressions Java
évaluées au runtime. Cela semble censé pour des expressions comme new
GridLayout(1, 0, 6, 0), mais que se passe-t-il quand vous utilisez une
expression dont la valeur change ?

En fait, c'est ce que nous appelons data-binding. A la compilation, JAXX parse
les expressions Java pour trouver toutes leurs dépendances et ajouter les
listeners d'évènement pour détecter quand elles changent. Quand la valeur
de l'expression change, la nouvelle value est automatiquement changée. A peu
près toutes les propriétés de tous les tags peuvent être gérées de cette
manière. Par example::

    <JTextField id='nameField'/>

    <JButton text='Button'
       enabled='{nameField.getText().length() != 0}'/>

Ainsi, le JButton ne sera activé que si un texte est rentré dans le JTextField.

En utilisant ainsi le data-binding, vous pouvez aussi :

* Afficher une vue détaillée des éléments sélectionnés  d'une liste.
* Appliquer les préférences utilisateur comme une taille de police.
* Afficher automatiquement un curseur d'attente dès que l'application passe dans un état occupé.
* Mettre à jour la barre de titre pour refléter le fichier actif.
* Mettre à jour automatiquement une barre de progression.
* Pré-remplir des éléments de formulaire.
* ...

Virtuellement, à chaque fois qu'un évènement change la valeur d'une propriété,
vous pouvez remplacer la gestion d'évènements Java par une expression de
data-binding plus simple et plus facile à lire. Bien sûr, JAXX n'est pas devin.
Non seulement la propriété à suivre doit lancer un évènement quand elle est
modifiée, mais JAXX doit savoir quel évènement est lancé pour ajouter le
listener approprié. Heureusement JAXX sait comment traquer presque toutes les
propriétés de tous les composants Swing, et peut automatiquement traquer les
propriétés attachées (les propriétés qui lancent des PropertyChangeEvent quand
elles sont modifiées), même sur des classes qu'il n'a jamais vu auparavant.

Vous pouvez aussi complexifier la gestion de vos évènements en utilisant des
attributs de classe. JAXX détecte les changements que vous pouvez y apporter et
instrumente le code en conséquence. JAXX reconnait que l'on met à jour un
attribut et ajoute des PropertyChangeEvent synthétiques. L'impact sur les
performances est le plus souvent négligeable.

Limitations
-----------

Le data-binding parse le code Java cherchant des méthodes et attributs dont il
sait écouter les modifications. Si une méthode représente une propriété qui
lance un PropertyChangeEvent, JAXX le découvre via l'introspection.

 Limitations

.. Data binding parses through Java code looking for methods and fields that it
.. knows how to listen for changes to. If a method represents a property which
.. fires PropertyChangeEvent (a bound property), JAXX discovers this via
.. introspection. For non-bound properties or other types of methods altogether,
.. the TagHandler for the tag must know how to deal with the method. The TagHandler
.. for JTextField, for instance, knows that getText() must be handled via a
.. DocumentListener on the Document.

.. If a method is not 'known' as described above, JAXX has no idea how to listen to it or if it even makes sense to listen to.

.. As field updates are handled by instrumenting the JAXX code, changing fields outside of JAXX (i.e. from ordinary Java code) bypasses JAXX's listeners and the changes will not be detected.

