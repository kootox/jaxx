package io.ultreia.java4all.lang;

/*-
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.model.CommentDto;
import io.ultreia.java4all.lang.model.Dto;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tchemit on 04/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SettersTest {

    @Test
    public void getWriteableProperties() throws Exception {

        {
            Set<String> writeableProperties = Setters.getWriteableProperties(Dto.class);
            Assert.assertEquals(1, writeableProperties.size());
            Assert.assertTrue(writeableProperties.contains("id"));
        }

        {
            Set<String> writeableProperties = Setters.getWriteableProperties(CommentDto.class);
            Assert.assertEquals(2, writeableProperties.size());
            Assert.assertTrue(writeableProperties.contains("id"));
            Assert.assertTrue(writeableProperties.contains("comment"));
        }
    }

}
