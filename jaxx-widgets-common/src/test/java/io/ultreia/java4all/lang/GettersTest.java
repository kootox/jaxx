package io.ultreia.java4all.lang;

/*-
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.model.CommentDto;
import io.ultreia.java4all.lang.model.Dto;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tchemit on 04/10/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GettersTest {

    @Test
    public void getReadableProperties() throws Exception {

        {
            Set<String> readableProperties = Getters.getReadableProperties(Dto.class);
            Assert.assertEquals(1, readableProperties.size());
            Assert.assertTrue(readableProperties.contains("id"));
        }

        {
            Set<String> readableProperties = Getters.getReadableProperties(CommentDto.class);
            Assert.assertEquals(2, readableProperties.size());
            Assert.assertTrue(readableProperties.contains("id"));
            Assert.assertTrue(readableProperties.contains("comment"));
        }
    }

    @Test
    public void isNestedReadableProperty() throws Exception {
    }

    @Test
    public void getReadableType() throws Exception {
        Assert.assertEquals(String.class, Getters.getReadableType(Dto.class, "id"));
        Assert.assertEquals(null, Getters.getReadableType(Dto.class, "comment"));
        Assert.assertEquals(String.class, Getters.getReadableType(CommentDto.class, "id"));
        Assert.assertEquals(String.class, Getters.getReadableType(CommentDto.class, "comment"));
    }

}
