package io.ultreia.java4all.lang;

/*-
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Created by tchemit on 25/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Getters {

    /**
     * Obtains all readable properties from a given type.
     *
     * @param beanType the type to seek
     * @return the set of all readable properties for the given type
     */
    public static Set<String> getReadableProperties(Class<?> beanType) {
        Objects.requireNonNull(beanType);
        Set<String> result = new HashSet<>();

        Objects2.walk(new Objects2.ClassWalkVisitor() {
            @Override
            public void onVisit(Class<?> beanType) {
                for (PropertyDescriptor descriptor : PropertyUtils.getPropertyDescriptors(beanType)) {
                    if (Objects2.IS_READ_DESCRIPTOR.test(descriptor)) {
                        result.add(descriptor.getName());
                    }
                }
            }

            @Override
            public boolean canContinue() {
                return true;
            }
        }, beanType);

        // the special getClass will never be a JavaBean property...
        result.remove("class");

        return result;
    }

    /**
     * Obtains all readable properties from a given type.
     *
     * @param beanType     the type to seek
     * @param propertyName the property name to seek
     * @return the set of all readable properties for the given type
     */
    public static boolean isNestedReadableProperty(Class<?> beanType, String propertyName) {
        Objects.requireNonNull(beanType);
        Objects.requireNonNull(propertyName);
        boolean result = propertyName.contains(".");
        if (result) {
            int dotIndex = propertyName.indexOf(".");
            String firstLevelProperty = propertyName.substring(0, dotIndex);

            Class<?> nestedType = getReadableType(beanType, firstLevelProperty);
            if (nestedType == null) {

                result = false;
            } else {

                String rest = propertyName.substring(dotIndex + 1);
                result = isNestedReadableProperty(nestedType, rest);
            }

        } else {

            // not a nested property check it directly
            Class<?> nestedType = getReadableType(beanType, propertyName);
            result = nestedType != null;
        }

        return result;
    }

    public static Class<?> getReadableType(Class<?> beanType, String propertyName) {
        Objects.requireNonNull(beanType);
        Objects.requireNonNull(propertyName);
        List<Class<?>> finalResult = new LinkedList<>();
        Objects2.walk(new Objects2.ClassWalkVisitor() {

            private Class<?> result = null;

            @Override
            public void onVisit(Class<?> beanType) {

                for (PropertyDescriptor descriptor : PropertyUtils.getPropertyDescriptors(beanType)) {
                    String name = descriptor.getName();
                    if (Objects2.IS_READ_DESCRIPTOR.test(descriptor) && propertyName.equals(name)) {
                        result = descriptor.getReadMethod().getReturnType();
                        finalResult.add(result);
                        break;
                    }
                }
            }

            @Override
            public boolean canContinue() {
                return result == null;
            }
        }, beanType);
        return finalResult.isEmpty() ? null : finalResult.get(0);
//
//        PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(beanType);
//
//        Class<?> result = null;
//        for (PropertyDescriptor descriptor : descriptors) {
//            String name = descriptor.getName();
//            if (IS_READ_DESCRIPTOR.test(descriptor) &&
//                    propertyName.equals(name)) {
//                result = descriptor.getReadMethod().getReturnType();
//                break;
//            }
//        }
//
//        if (result == null) {
//
//            // try with super-class
//            if (beanType.getSuperclass() != null) {
//
//                // get properties fro super-class
//                result = getReadableType(beanType.getSuperclass(), propertyName);
//            }
//        }
//
//        if (result == null) {
//
//            // try it with interfaces
//            Class<?>[] interfaces = beanType.getInterfaces();
//            for (Class<?> anInterface : interfaces) {
//
//                result = getReadableType(anInterface, propertyName);
//
//                if (result != null) {
//
//                    // found it
//                    break;
//                }
//            }
//        }
//        return result;
    }

}
