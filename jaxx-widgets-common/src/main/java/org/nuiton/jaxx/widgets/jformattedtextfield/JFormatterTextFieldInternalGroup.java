package org.nuiton.jaxx.widgets.jformattedtextfield;

/*
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

/**
 * Represent a group of the mask inside a {@link JFormattedTextField} using a  {@link MaskFormatter}.
 *
 * Created on 3/20/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.23
 */
public class JFormatterTextFieldInternalGroup {

    /**
     * index where the group starts.
     */
    private final int startIndex;

    /**
     * index where the group ends.
     */
    private final int endIndex;

    /**
     * index where the group + his symbols ends.
     */
    private final int endGroupIndex;

    private JFormatterTextFieldInternalGroup previousGroup;

    private JFormatterTextFieldInternalGroup nextGroup;

    public JFormatterTextFieldInternalGroup(int startIndex, int endIndex, int endGroupIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.endGroupIndex = endGroupIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getEndGroupIndex() {
        return endGroupIndex;
    }

    public boolean containsPosition(int position) {
        return startIndex <= position && position <= endGroupIndex;
    }

    public boolean isFirstGroup() {
        return previousGroup == null;
    }

    public boolean isLastGroup() {
        return nextGroup == null;
    }

    public JFormatterTextFieldInternalGroup getPreviousGroup() {
        return previousGroup;
    }

    public JFormatterTextFieldInternalGroup getNextGroup() {
        return nextGroup;
    }

    public void setPreviousGroup(JFormatterTextFieldInternalGroup previousGroup) {
        this.previousGroup = previousGroup;
    }

    public void setNextGroup(JFormatterTextFieldInternalGroup nextGroup) {
        this.nextGroup = nextGroup;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("startIndex", startIndex)
                .append("endIndex", endIndex)
                .append("endGroupIndex", endGroupIndex)
                .toString();
    }
}
