package org.nuiton.jaxx.widgets.jformattedtextfield;

/*
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 3/20/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.23
 */
public class JFormatterTextFieldInternalGroups implements Iterable<JFormatterTextFieldInternalGroup> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(JFormatterTextFieldInternalGroups.class);

    public static JFormatterTextFieldInternalGroups create(String mask) {

        // We need to double this caracter for MaskFormatter creation, but here we don't
        // TODO See which other caracters must be especad by MaskFormatter
        mask = mask.replaceAll("''", "'");

        Set<JFormatterTextFieldInternalGroup> groups = new LinkedHashSet<>();

        JFormatterTextFieldInternalGroup previousGroup = null;
        int length = mask.length();

        for (int i = 0; i < length; i++) {
            char c = mask.charAt(i);
            if (c != '*') {

                int endIndex = i - 1;
                int endGroupIndex = getGroupLastIndex(mask, i, length);

                i = endGroupIndex + 1;

                // new group detected
                previousGroup = addGroup(groups, previousGroup, endIndex, endGroupIndex);

            }

        }

        if (previousGroup != null && previousGroup.getEndGroupIndex() < length - 1) {

            // adding the last remaining group
            addGroup(groups, previousGroup, length, length);

        }

        return new JFormatterTextFieldInternalGroups(groups);

    }

    protected static int getGroupLastIndex(String mask, int i, int length) {

        do {

            char c2 = mask.charAt(i);
            if (c2 == '*') {
                break;
            }
            i++;

        } while (i < length);

        return i - 1;

    }

    protected static JFormatterTextFieldInternalGroup addGroup(Set<JFormatterTextFieldInternalGroup> componentPositions, JFormatterTextFieldInternalGroup previousGroup, int endIndex, int endSymbolIndex) {

        boolean withPreviousGroup = previousGroup != null;

        JFormatterTextFieldInternalGroup newGroup;

        if (withPreviousGroup) {

            newGroup = new JFormatterTextFieldInternalGroup(previousGroup.getEndGroupIndex() + 1, endIndex, endSymbolIndex);

        } else {

            newGroup = new JFormatterTextFieldInternalGroup(0, endIndex, endSymbolIndex);

        }

        if (log.isDebugEnabled()) {
            log.debug(String.format("New component (%d): %d - %d - %d", componentPositions.size(), newGroup.getStartIndex(), newGroup.getEndIndex(), newGroup.getEndGroupIndex()));
        }
        componentPositions.add(newGroup);
        if (withPreviousGroup) {
            previousGroup.setNextGroup(newGroup);
            newGroup.setPreviousGroup(previousGroup);
        }

        return newGroup;

    }

    private final Set<JFormatterTextFieldInternalGroup> componentPositions;

    public JFormatterTextFieldInternalGroup getGroupAtPosition(int position) {

        JFormatterTextFieldInternalGroup result = null;
        for (JFormatterTextFieldInternalGroup componentPosition : this) {
            if (componentPosition.containsPosition(position)) {
                result = componentPosition;
                break;
            }
        }
        return result;

    }

    protected JFormatterTextFieldInternalGroups(Set<JFormatterTextFieldInternalGroup> componentPositions) {
        this.componentPositions = ImmutableSet.copyOf(componentPositions);
    }

    @Override
    public Iterator<JFormatterTextFieldInternalGroup> iterator() {
        return componentPositions.iterator();
    }

}
