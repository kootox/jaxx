package org.nuiton.jaxx.widgets;

/*
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * A property change listener to mutate to a bean after a predicate was applied.
 *
 * Created on 11/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class MutateOnConditionalPropertyChangeListener<M extends ModelToBean> implements PropertyChangeListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MutateOnConditionalPropertyChangeListener.class);

    private final M model;

    private final Method mutator;

    private final Predicate<M> canMutatePredicate;

    private final String propertyName;

    public MutateOnConditionalPropertyChangeListener(M model, Method mutator, Predicate canMutatePredicate) {

        Objects.requireNonNull(model);
        Objects.requireNonNull(mutator);
        Objects.requireNonNull(canMutatePredicate);

        this.model = model;
        this.mutator = mutator;
        this.canMutatePredicate = canMutatePredicate;
        this.propertyName = mutator.getName();

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        if (canMutatePredicate.test(model)) {

            Object newValue = evt.getNewValue();


            if (log.isDebugEnabled()) {
                log.debug("Mutates property " + propertyName + " with value " + newValue + " on " + model);
            }

            try {

                mutator.invoke(model.getBean(), newValue);

            } catch (Exception e) {
                throw new JaxxWidgetRuntimeException("Can't mutate property " + propertyName + " on " + model, e);
            }

        }

    }
}
