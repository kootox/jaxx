package org.nuiton.jaxx.widgets;

/*
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * A none checked exception to use in widgets.
 *
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public class JaxxWidgetRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -2963740979386945340L;

    public JaxxWidgetRuntimeException() {
    }

    public JaxxWidgetRuntimeException(String message) {
        super(message);
    }

    public JaxxWidgetRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public JaxxWidgetRuntimeException(Throwable cause) {
        super(cause);
    }
}
