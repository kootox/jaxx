# JAXX

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/jaxx.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22jaxx%22)
![Build Status](https://gitlab.com/ultreiaio/jaxx/badges/develop/build.svg)
[![The Less GNU General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-green.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

# Resources

* [Changelog](https://gitlab.com/ultreiaio/jaxx/blob/develop/CHANGELOG.md)
* [Documentation](https://ultreiaio.gitlab.io/jaxx)

# Community

* [Mailing-list (Users)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-users)
* [Mailing-list (Devel)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-devel)
* [Mailing-list (Commits)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-commits)
