package org.nuiton.jaxx.widgets.temperature;

/*-
 * #%L
 * JAXX :: Widgets Temperature
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Setters;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Objects;
import javax.swing.JLabel;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.number.NumberEditorModel;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TemperatureEditorHandler implements UIHandler<TemperatureEditor> {

    private TemperatureEditor ui;

    @Override
    public void beforeInit(TemperatureEditor ui) {
        this.ui = ui;
        TemperatureEditorModel model = new TemperatureEditorModel();
        model.setFormat(TemperatureFormat.C);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(TemperatureEditor ui) {
    }

    public void init(JLabel label) {

        Objects.requireNonNull(label, "No label, can't init.");

        TemperatureEditorModel model = ui.getModel();
        TemperatureEditorConfig config = model.getConfig();
        Objects.requireNonNull(config, "No config in editor, can't init.");

        String property = config.getProperty();
        Object bean = config.getBean();

        ui.getEditor().init();

        ui.getEditor().getModel().addPropertyChangeListener(NumberEditorModel.PROPERTY_NUMBER_VALUE, evt -> model.setTemperature((Float) evt.getNewValue()));

        Method mutator = Setters.getMutator(bean, property);
        Objects.requireNonNull(mutator, "could not find mutator for " + property);
        // When model changed, let's push it back in bean
        model.addPropertyChangeListener(TemperatureEditorModel.PROPERTY_STORAGE_TEMPERATURE, new ModelPropertyChangeListener(bean, mutator));
        model.addPropertyChangeListener(TemperatureEditorModel.PROPERTY_FORMAT, evt -> label.setText(model.getLabel()));
        label.setText(model.getLabel());

    }

    private class ModelPropertyChangeListener implements PropertyChangeListener {

        private final Object bean;
        private final Method mutator;

        private ModelPropertyChangeListener(Object bean, Method mutator) {
            this.bean = bean;
            this.mutator = mutator;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            try {
                mutator.invoke(bean, evt.getNewValue());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

}
