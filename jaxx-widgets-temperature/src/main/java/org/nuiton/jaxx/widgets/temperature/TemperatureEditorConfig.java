package org.nuiton.jaxx.widgets.temperature;

/*-
 * #%L
 * JAXX :: Widgets Temperature
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import java.util.Objects;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.0
 */
public class TemperatureEditorConfig {

    /**
     * Format used in storage (always use this format to send back to bean).
     * This format will never changed.
     */
    private final TemperatureFormat storageFormat;
    /**
     * Bean where to push back value.
     */
    private Object bean;
    /**
     * Label to display.
     */
    private String label;
    /**
     * Property of the bean where to push back value.
     */
    private String property;

    public TemperatureEditorConfig(TemperatureFormat storageFormat, Object bean, String label, String property) {
        Objects.requireNonNull(storageFormat);
//        Objects.requireNonNull(bean);
//        Objects.requireNonNull(label);
//        Objects.requireNonNull(property);
        this.storageFormat = storageFormat;
        this.bean = bean;
        this.label = label;
        this.property = property;
    }

    TemperatureFormat getStorageFormat() {
        return storageFormat;
    }

    public Object getBean() {
        return bean;
    }

    String getLabel() {
        return label;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setBean(JavaBean bean) {
        this.bean = bean;
    }
}
