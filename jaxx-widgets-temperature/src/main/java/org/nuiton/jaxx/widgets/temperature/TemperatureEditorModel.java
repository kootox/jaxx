package org.nuiton.jaxx.widgets.temperature;

/*-
 * #%L
 * JAXX :: Widgets Temperature
 * %%
 * Copyright (C) 2008 - 2018 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created by tchemit on 25/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TemperatureEditorModel extends AbstractSerializableBean {

    static final String PROPERTY_FORMAT = "format";
    static final String PROPERTY_STORAGE_TEMPERATURE = "storageTemperature";
    static final String PROPERTY_TEMPERATURE = "temperature";
    private static final long serialVersionUID = -3883079504284754230L;

    private TemperatureEditorConfig config;

    /**
     * Format used to display value.
     */
    private TemperatureFormat format;
    /**
     * Temperature to display (using the format in {@link #format}.
     */
    private Float temperature;
    /**
     * Temperature to send back to bean (using the format in {@link TemperatureEditorConfig#storageFormat}.
     */
    private Float storageTemperature;

    public TemperatureEditorConfig getConfig() {
        return config;
    }

    public void setConfig(TemperatureEditorConfig config) {
        this.config = config;
    }

    public String getLabel() {
        return config.getLabel() + " (" + I18nEnumHelper.getLabel(format) + ")";
    }

    public TemperatureFormat getFormat() {
        return format;
    }

    public void setFormat(TemperatureFormat format) {
        Object oldValue = getFormat();
        this.format = format;
        firePropertyChange(PROPERTY_FORMAT, oldValue, format);
        // reload storage temperature (will update then temperature)
        adjusting = true;
        try {
            setStorageTemperature(storageTemperature);
        } finally {
            adjusting = false;
        }
    }

    public Float getTemperature() {
        return temperature;
    }

    private boolean adjusting;

    public void setTemperature(Float temperature) {
        Float oldTemperature = getTemperature();
        this.temperature = temperature;
        firePropertyChange(PROPERTY_TEMPERATURE, oldTemperature, temperature);
        if (!adjusting) {
            Object oldStorageTemperature = getStorageTemperature();
            this.storageTemperature = format.convert(temperature, config.getStorageFormat());
            firePropertyChange(PROPERTY_STORAGE_TEMPERATURE, oldStorageTemperature, storageTemperature);
        }
    }

    private Float getStorageTemperature() {
        return storageTemperature;
    }

    void setStorageTemperature(Float storageTemperature) {
        this.storageTemperature = storageTemperature;
        if (config != null) {
            Float oldTemperature = getTemperature();
            this.temperature = config.getStorageFormat().convert(storageTemperature, format);
            firePropertyChange(PROPERTY_TEMPERATURE, oldTemperature, temperature);
        }
    }
}
